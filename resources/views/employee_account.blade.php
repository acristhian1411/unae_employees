<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Reporte de salarios</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #ddd;
        }
    </style>
</head>

<body>
    <h1>Reporte de salarios</h1>
    <div class="subtitle">Filtros:</div>
    <div class="filters">
        @if(isset($data['filter']['till'])) <div>Caja: {{ $data['filter_label']['till'] }}</div> @endif
        @if(isset($data['filter']['month'])) <div>Mes: {{ $data['filter_label']['month'] }}</div> @endif
        @if(isset($data['filter']['year'])) <div>Año: {{ $data['filter']['year'] }}</div> @endif
        @if(isset($data['filter']['paid'])) <div>Pagado: {{ $data['filter_label']['paid'] }}</div> @endif
        @if(isset($data['filter']['comp_det'])) <div>Entidad: {{ $data['filter_label']['comp_det'] }}</div> @endif
        @if(isset($data['filter']['partial']) and $data['filter']['partial'] != 'null') <div>Pago Parcial: {{ $data['filter']['partial'] == 'true' ? 'si' : 'no' }}</div> @endif

    </div>
    </div>
    <table>
        <thead>
            <tr>
                <th>N°</th>
                <th>ID</th>
                <th>Funcionario</th>
                <th>Cédula</th>
                <th>Entidad</th>
                <th>Salario</th>
                <th>% IPS</th>
                <th>Prestamo</th>
                <th>Hs extra</th>
                <th>Total</th>
                <th>Pagado</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>

            <?php
            $totalAmount = 0;
            foreach ($data['data'] as $key => $salario) :
                $totalAmount += $salario->emplo_salary + $salario->extra_hours - ($salario->amount_loan ?? 0);
            ?>
                <tr>
                    <td><?= $key + 1 ?></td>
                    <td><?= $salario->person_id ?></td>
                    <td><?= $salario->person_lastname . ' ' . $salario->person_fname  ?></td>
                    <td><?= $salario->person_idnumber ?></td>
                    <td><?= $salario->comp_det_bussiness_name ?></td>
                    <td><?= number_format($salario->emplo_salary, 0, '', '.') ?></td>
                    <td><?= $salario->has_ips ? $salario->porcent_ips . '%' : 0 ?></td>
                    <td><?= number_format($salario->amount_loan, 0, '', '.') ?? 0 ?></td>
                    <td><?= number_format($salario->extra_hours, 0, '', '.') ?? 0 ?></td>
                    <td><?= number_format(($salario->emplo_salary + $salario->extra_hours) - ($salario->amount_loan ?? 0), 0, '', '.') ?></td>
                    <td><?= number_format($salario->emp_acc_amountpaid, 0, '', '.') ?? 0 ?></td>
                    <td><?= $salario->emp_acc_status ? 'Pagado' : 'Por pagar' ?> </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="10">Total: <?= NumerosEnLetras::convertir($totalAmount) ?></td>
                <td colspan="2"><strong><?= number_format($totalAmount, 0, '', '.') ?></strong></td>
            </tr>
        </tbody>
    </table>
</body>

</html>