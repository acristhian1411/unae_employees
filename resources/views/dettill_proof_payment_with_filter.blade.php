<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Detalles de pago</title>
  <style>
    /*Estilos CSS para la tabla*/
    table {
      border-collapse: collapse;
      border: 1px solid #CCC;
      width: 100%;
    }

    th,
    td {
      text-align: left;
      border: 1px solid black;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    th {
      background-color: #4CAF50;
      color: white;
    }

    /*Estilos CSS para la cabecera*/
    .header {
      text-align: center;
      margin-bottom: 30px;
    }

    .title {
      font-size: 24px;
      margin-bottom: 10px;
    }

    .subtitle {
      font-size: 18px;
      margin-bottom: 20px;
    }

    .filters {
      font-size: 14px;
    }
  </style>
</head>

<body>
  <div class="header">
    <div class="title">Comprobantes de pago</div>
    <div class="subtitle">Filtros:</div>
    <div class="filters">
      @if(isset($data['filter']['person_id'])) <div>Estudiante: {{ $data['filter_label']['person_name'] }}</div> @endif
      @if(isset($data['filter']['pay_type_id'])) <div>Tipo de pago: {{ $data['filter_label']['pay_type_desc'] }}</div> @endif
      @if(isset($data['filter']['dettills_desc'])) <div>Descripción: {{ $data['filter']['dettills_desc'] }}</div> @endif
      @if(isset($data['filter']['date_start'])) <div>Fecha de inicio: {{ $data['filter']['date_start'] }}</div> @endif
      @if(isset($data['filter']['date_end'])) <div>Fecha fin: {{ $data['filter']['date_end'] }}</div> @endif
    </div>
  </div>
  <table>
    <thead>
      <tr>
        <th>Tipo de pago</th>
        <th>Descripción</th>
        <th>Monto</th>
        <th>Fecha</th>
        <th>Estudiante</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['data'] as $d)
      <tr>
        <td>{{ $d->pay_type_desc }}</td>
        <td>{{ $d->dettills_pr_pay_desc }}</td>
        <td>{{ $d->person_name }}</td>
        <td>{{ number_format($d->amount, 0, '', '.') }}</td>
        <td>{{ $d->date }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</body>

</html>