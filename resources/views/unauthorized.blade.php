<!DOCTYPE html>
  <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <script src="https://momentjs.com/downloads/moment.min.js"></script>

      <title>UNAE</title>
      <!-- Styles -->
      <link href="css/app.css" rel="stylesheet">
  </head>
  <style>
    html, body {
          height: 100%;
          width: 100%;
          padding: 0;
          margin: 0;
          font-family: Gadugi !important;
      }
  </style>
  <body>
        <div id="app">No tiene permisos para ver este reporte, contacte con el administrador del sistema</div>
  </body>
  </html>
