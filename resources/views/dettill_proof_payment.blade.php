<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Detalle de pagos</title>
  <style>
    table {
      border-collapse: collapse;
      width: 100%;
    }

    th,
    td {
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    th {
      background-color: #4CAF50;
      color: white;
    }
  </style>
</head>

<body>
  <h1>Detalle de pagos</h1>
  <table>
    <thead>
      <tr>
        <th>Descripción de pago</th>
        <th>Fecha de pago</th>
        <th>Monto</th>
        <th>Nombre del cliente</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $pago)
      <tr>
        <td>{{$pago->dettills_pr_pay_desc}}</td>
        <td>{{$pago->date}}</td>
        <td>{{$pago->amount}}</td>
        <td>{{$pago->person_name}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</body>

</html>