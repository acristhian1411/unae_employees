  <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://momentjs.com/downloads/moment.min.js"></script>
        <link rel="icon" href="img/favicon.jpeg" >
        <title>AIUS</title>

        <!-- Styles -->
        <link href="css/app.css" rel="stylesheet">
    </head>
    <style>
      html, body {
            height: 100%;
            width: 100%;
            padding: 0;
            margin: 0;
            font-family: Gadugi !important;
            /* scrollbar-width: none; */
        }
        body::-webkit-scrollbar{
          display: none;
        }
    </style>
    <body>


                <div id="app"></div>

          <script src="js/app.js"></script>
    </body>
    </html>
