<!DOCTYPE html>
  <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <script src="http://momentjs.com/downloads/moment.min.js"></script>

      <title>Unae</title>
      <!-- Styles -->
      <link href="../css/app.css" rel="stylesheet">
  </head>
  <style>
    html, body {
          height: 100%;
          width: 100%;
          padding: 0;
          margin: 0;
      }
      .report-table{
        border-collapse:collapse;
      }
      .report-thead{
        font-size: 12px;
        font-family: serif;
        color: #222;
        text-align: center;
      }
      .report-thead th{
        border: 1px solid #9e9e9e;
        border-top: 0px;
        padding: 5px;
      }
      .report-tbody{
        font-size: 11px;
        font-family: serif;
        color: #222;
      }
      .report-tbody td{
        border: 1px solid #9e9e9e;
        border-top: 0px;
        padding: 8px;
        /* border-spacing: 0; */
      }
      .report-container{
        margin: 10px;
      }
      .report-date{
        font-size: 13px;
        color: #222;
      }
  </style>
  <body>
    <div class="report-container">
      <div class="report-header">
        <h1>Ingreso por fecha</h1>
        <div class="report-date">
          <p>Del <span>25 de noviembre de 2021</span> al <span>25 de noviembre de 2021</span></p>
        <div>
      <div>
      <table class="report-table">
        <thead class="report-thead">
          <tr>
            <th>
              <p>Persona/Empresa</p>
            </th>
            <th>
              <p>C.I/R.U.C</p>
            </th>
            <th>
              <p>Concepto</p>
            </th>
            <th>
              <p>Institucion</p>
            </th>
            <th>
              <p>Carrera</p>
            </th>
            <th>
              <p>Fecha y Hora</p>
            </th>
            <th>
              <p>Tipo de pago</p>
            </th>
            <th>
              <p>Monto</p>
            </th>
            <th>

            </th>
          </tr>
        </thead>
        <tbody class="report-tbody">
          @foreach($data as $row)
            <tr>
              <td>
                {{ $row->person_fname ?? '' }}
              </td>
              <td>
                {{ $row->person_idnumber ?? '' }}
              </td>
              <td>
                {{ $row->tickdet_desciption ?? '' }}
              </td>
              <td>
                {{ $row->unit_faculty ?? '' }}
              </td>
              <td>
                {{ $row->unit_faculty ?? '' }}
              </td>
              <td>
                {{ $row->ticket_date ?? '' }}
              </td>
              <td>
                Efectivo
              </td>
              <td>
                {{ $row->tickdet_monto ?? '' }}
              </td>
              <td>
                {{ $row->comp_name ?? '' }}
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>



              <!-- <div id="app"></div>

        <script src="js/app.js"></script> -->
  </body>
  </html>
