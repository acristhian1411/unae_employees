import React from 'react';

const getAuth = React.createContext();
const getPerson = React.createContext();
const getTillStart = React.createContext();

export {getAuth, getPerson, getTillStart};
