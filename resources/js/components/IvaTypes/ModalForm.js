import React, {Component} from 'react';
import {Modal, Backdrop, Fade, InputLabel, TextField, Checkbox,
        Button, Snackbar} from '@material-ui/core';
import Select from 'react-select';
import SaveIcon from '@material-ui/icons/Save';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Alert } from '@material-ui/lab';

 class ModalForm extends Component  {

  constructor (props) {
    super(props)
    this.state = {
      snack_open: false,
      open: true,
      setOpen: true,
      ivatype_id: null,
      ivatype_description: '',
      ivatype_valor: 0,
      edit: false,
      errors: [],
      validator: {
        ivatype_description:{
          message:'',
          error: false,
        },
        ivatype_valor:{
          message:'',
          error: false
        }
      }

    }

    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.onChangeField1 = this.onChangeField1.bind(this)
    this.onChangedSelect1 = this.onChangedSelect1.bind(this)
    this.handleCreateObject = this.handleCreateObject.bind(this)
    this.handleUpdateObject = this.handleUpdateObject.bind(this)
    this.hasErrorFor = this.hasErrorFor.bind(this)
    this.renderErrorFor = this.renderErrorFor.bind(this)
    this.closeSnack = this.closeSnack.bind(this);
  }

  componentDidMount () {
if (this.props.edit) {
  var id = this.props.ivatype.data.ivatype_id
  axios.get('api/ivaType/'+id).then(response => {
    this.setState({
      ivatype_id: response.data.data.ivatype_id,
      ivatype_description: response.data.data.ivatype_description,
      ivatype_valor: response.data.data.ivatype_valor,
    })
  })
}

  }



  closeSnack(){
    this.setState({snack_open: false});
  }
  handleOpen () {
    this.setState({
      setOpen: true
    });
  };

  handleClose () {
    this.setState({
      open: false
    });
    this.props.onHandleSubmit();
  };

  onChangeField1 (e) {
   this.setState({
     ivatype_description: e.target.value
   });
   this.props.changeAtribute(true);
 };


  //funcion para capturar inputs tipo int
  onChangedSelect1 (e) {
    this.setState({ivatype_valor: e.target.value});
    this.props.changeAtribute(true);
};



handleCreateObject (e) {
  e.preventDefault();
  var {ivatype_description, ivatype_valor} = this.state;
  var validator = {ivatype_description: {error: false,  message: ''},
                   ivatype_valor: {error: false,  message: ''}};
  var object_error = {};
  const object = {
    ivatype_valor: this.state.ivatype_valor,
    ivatype_description: this.state.ivatype_description.toUpperCase(),
    }
  const { history } = this.props
    axios.post('api/ivaType', {ivatype_description:object.ivatype_description, ivatype_valor:object.ivatype_valor })
      .then(res => {
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        this.handleClose();      }
    ).catch(error => {
      // console.log(Object.keys(error.response.data.errors));
      this.setState({snack_open: true});
      var errores = Object.keys(error.response.data.errors);
      for (var i = 0; i < errores.length; i++) {
        object_error = {
          ...object_error,
          [errores[i]]: {
            error: true,
            message: error.response.data.errors[errores[i]][0]
          }
        }
      }
      this.setState({
        validator:{
          ...validator,
          ...object_error
        }
      });
    });
  }


  handleUpdateObject(e) {
    e.preventDefault();
    var {ivatype_description, ivatype_valor} = this.state;
    var validator = {ivatype_description: {error: false,  message: ''},
                     ivatype_valor: {error: false,  message: ''}};
    var object_error = {};
    const object = {
      ivatype_id: this.state.ivatype_id,
      ivatype_valor: this.state.ivatype_valor,
      ivatype_description: this.state.ivatype_description.toUpperCase(),
    }
    const id = object.ivatype_id
    axios.put(`api/ivaType/${id}`, {ivatype_description:object.ivatype_description, ivatype_valor:object.ivatype_valor })
      .then((res) => {
        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        this.handleClose();
      }).catch(error => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({snack_open: true});
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0]
            }
          }
        }
        this.setState({
          validator:{
            ...validator,
            ...object_error
          }
        });
      });
  }

  hasErrorFor (field) {
    return !!this.state.errors[field]
  }

  renderErrorFor (field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className='invalid-feedback'>
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      )
    }
  }

  botonEdit(){
    if (this.props.edit) {
      return (<Button name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleUpdateObject}>Actualizar</Button>);
    }
    else {
      return (<Button name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleCreateObject}>Guardar</Button>);

    }
  }


render(){
  const { classes } = this.props;
  const {validator, snack_open} = this.state;
  var showSnack;
  if (snack_open){
        showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                       <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="error">
                         {validator['ivatype_description']['error']?<li>Error: {validator['ivatype_description']['message']}</li>:''}
                         {validator['ivatype_valor']['error']?<li>Error: {validator['ivatype_valor']['message']}</li>:''}
                       </Alert>
                     </Snackbar>
      }
  return (
    <div>
          <h3>Formulario de tipos de iva</h3>
          <hr />
                <form>
                  <div className='form-group'>
                    <TextField
                      name="ivatype_description"
                     error={this.state.validator.ivatype_description.error}
                     helperText={this.state.validator.ivatype_description.message}
                     id="outlined-full-width"
                     label="Descripcion"
                     style={{ margin: 8 }}
                     value={this.state.ivatype_description}
                     onChange={this.onChangeField1}
                     fullWidth
                     margin="normal"
                     variant="outlined"
                   />
                  </div>

                  <div className='form-group'>
                    <TextField
                      name="ivatype_valor"
                     error={this.state.validator.ivatype_valor.error}
                     helperText={this.state.validator.ivatype_valor.message}
                     id="outlined-full-width"
                     label="Valor"
                     type='number'
                     style={{ margin: 8 }}
                     value={this.state.ivatype_valor}
                     onChange={this.onChangedSelect1}
                     fullWidth
                     margin="normal"
                     variant="outlined"
                   />
                  </div>


                        {this.botonEdit()}
                        {showSnack}

                        <Button variant="outlined" color="secondary" startIcon={<ExitToAppIcon />} onClick={this.handleClose} onClick={this.handleClose}>Cancelar</Button>
                </form>
            </div>
  );
  }
}
export default ModalForm;
