import React, {Component} from 'react';
// import {Button, Dialog, DialogActions, DialogTitle} from '@material-ui/core';
import {Modal, Backdrop, Fade, Button, IconButton, TextField} from '@material-ui/core';
import { Autocomplete } from "@material-ui/lab";
import {RoutesList} from '../ArraysCalls/RoutesList';
/*
Este componente recibe las props de:
 onClose() <- esto para desabilitar el open del Dialog
 onHandleAgree() <- esto para manejar si el usuario esta de acuerdo, generalmente activa la funcion
  close modal;
 ejemplo:
 <DialogSearchRoute onClose={this.handleClose} onHandleAgree={this.destroy}/>
*/
export default class DialogSearchRoute extends Component{
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    }
    this.handleClose = this.handleClose.bind(this);
    // this.handleAgree = this.handleAgree.bind(this);
  }
  handleClose(){
    this.setState({open:false});
    this.props.onClose();
  }
  // handleAgree(){
  //   this.setState({open:false});
  //   this.props.onHandleAgree();
  // }
  componentDidMount(){
    console.log(this.props.rol);

  }
  render(){
    return(
      <div>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className=""
          open={this.state.open}
          onClose={this.handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
            invisible: true
          }}
        >
         <Fade in={this.state.open}>
             <div style={{padding: 5, margin: 'auto', marginTop: '50px', width: '60%'}}  className="paper">
             <Autocomplete
               id="combo-box-demo"
               className="buscador"
               disableClearable
               options={RoutesList(this.props.rol.role_name)}
               getOptionLabel={(option) => option.label}
               groupBy={(option)=>option.group}
               onChange={(e, values) =>(
                  window.location.replace(`/#${values.value}`),
                  this.handleClose()
                )
               }
               // value={this.selectValue("facu_name", "faculty_id")}
               getOptionSelected={(option, value) => {
                 if (value === option.value) {
                   return option.label;
                 } else {
                   return false;
                 }
               }}
               // style={{ margin: 8 }}
               fullWidth
               renderInput={(params) => (
                 <TextField
                   {...params}
                   autoFocus
                   label="Busqueda de rutas *"
                   //error={this.state.validator.faculty_id.error}
                   className="textField"
                   variant="outlined"
                 />
               )}
             />
             </div>
          </Fade>
        </Modal>
      </div>
    );
  }
}
