import React, {Component} from 'react';
import {Button, Dialog, DialogActions, DialogTitle} from '@material-ui/core';
/*
Este componente recibe las props de:
 onClose() <- esto para desabilitar el open del Dialog
 onHandleAgree() <- esto para manejar si el usuario esta de acuerdo, generalmente activa la funcion
  getDataEdit del componente padre;
 index <- aqui se le pasa el dato de enrolled para recuperar el edit
 ejemplo:
 <DialogRecoveryEdit index={this.state.last_enroll} onClose={this.handleClose} onHandleAgree={this.recoverFunction}/>
*/
export default class DialogRecoveryEdit extends Component{
  constructor(props) {
    super(props);
    this.state = {
      open: true,
    }
    this.handleClose = this.handleClose.bind(this);
    this.handleAgree = this.handleAgree.bind(this);
  }
  handleClose(){
    this.setState({open:false});
    this.props.onClose();
  }
  handleAgree(){
    this.setState({open:false});
    this.props.onHandleAgree(this.props.index);
  }
  componentDidMount(){
    // console.log(this.props);
  }
  render(){
    return(
      <Dialog
        open={this.state.open}
        onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
      <DialogTitle id="alert-dialog-title">{`Esta persona ya cuenta con una inscripcion este año. ¿Desea recuperar para editarla?`}</DialogTitle>
      <DialogActions>
        <Button color="default" onClick={this.handleClose}>
          No recuperar
        </Button>
        <Button color="primary" autoFocus onClick={this.handleAgree}>
          Recuperar
        </Button>
      </DialogActions>
      </Dialog>
    );
  }
}
