/*
*desplegar la lista de transfers en una tabla, que muestre la persona,
                                    el numero de transfers, monto total
*/
import React, { Component } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Grid,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import {format, disformat} from '../GlobalFunctions/Format';
export default class TillTransferShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transfers: [],
      ticketDetails: [],
      url: `/api/tillsTransfers/${this.props.transfer}`,
      // url: `/api/tillsTransfers/6`,
      permissions: {
        ticket_index: null,
        ticket_show: null,
        ticket_store: null,
        ticket_update: null,
        ticket_destroy: null,
      },
    };

    this.getObject = async () => {
      let res;
      res = await axios.get(`${this.state.url}`);
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          transfers: data.data,
          ticketDetails: data.data.ticketDetails,
        });
      }
    };
  }

  componentDidMount() {
    this._ismounted = true;
    var permissions = [
      "ticket_index",
      "ticket_show",
      "ticket_store",
      "ticket_update",
      "ticket_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject();
        }
      });
  }

  componentWillUnmount() {
    this._ismounted = false;
  }

  render() {
    return (
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2>Fecha: {this.state.transfers.tilltrans_date}</h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={6}>
              <h2>Caja de origen: {this.state.transfers.origin_name}</h2>
            </Grid>

            <Grid item xs={6}>
              <h2>Caja de destino: {this.state.transfers.destiny_name}</h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2>Monto: {format(Math.round(this.state.transfers.tilltrans_amount))}Gs.</h2>
            </Grid>
          </Grid>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2>Observacion: </h2>
              <p>
                {this.state.transfers.tilltrans_observation === null
                  ? "--"
                  : this.state.transfers.tilltrans_observation}
              </p>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}
