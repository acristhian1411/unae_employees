import React, { Component } from "react";
import {
  Modal,
  Backdrop,
  Fade,
  Grid,
  InputLabel,
  CircularProgress,
  TextField,
  Checkbox,
  Button,
  Snackbar,
} from "@material-ui/core";
import { Autocomplete, Alert } from "@material-ui/lab";
import Select from "react-select";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      open: true,
      setOpen: true,
      origin_till_id: 0,
      destiny_till_id: 0,
      cajaOriginAmount: 0,
      cajaDestinyAmount: 0,
      message_success: "",
      amount: 0,
      tills: [],
      obs: "",
      origin_name: "",
      destiny_name: "",
      unit_id: null,
      unit_name: "",
      edit: false,
      time_out: false,
      units: [],
      errors: [],
      validator: {
        amount: {
          message: "",
          error: false,
        },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onChangeField1 = this.onChangeField1.bind(this);
    this.onChangeField2 = this.onChangeField2.bind(this);
    this.onChangedSelect1 = this.onChangedSelect1.bind(this);
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.handleUpdateObject = this.handleUpdateObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  componentDidMount() {
    // if (this.props.edit) {
    axios.get("/api/tills-starting").then((response) => {
      this.setState({
        tills: response.data.data,
      });
    });
    axios.get("/api/tills/"+this.props.till_id).then((response) => {
      this.setState({
        origin_till_id: response.data.data.till_id,
        origin_name: response.data.data.till_name,
      });
    });
    axios.get("/api/tillDetails-sum/" + this.props.till_id).then((res) => {
      this.setState({ cajaOriginAmount: res.data.suma });
    });
    // }
  }

  selectReturn() {
    return this.state.tills.map((data) => ({
      label: data.till_name,
      value: data.till_id,
    }));
  }

  handleOpen() {
    this.setState({
      setOpen: true,
    });
  }

  handleClose() {
    this.setState({
      open: false,
    });
    this.props.onHandleSubmit();
  }

  onChangeField1(e) {
    if (parseInt(e.target.value) > this.state.cajaOriginAmount) {
      var a = {
        amount: {
          message: "Monto mayor",
          error: true,
        },
      };
      this.setState({ validator: a });
    } else {
      var a = {
        amount: {
          message: "",
          error: false,
        },
      };
      this.setState({ validator: a });
    }
    this.setState({
      amount: e.target.value,
    });
    this.props.changeAtribute(true);
  }

  onChangeField2(e) {
    this.setState({
      obs: e.target.value,
    });
    this.props.changeAtribute(true);
  }
  //funcion para capturar inputs tipo int
  onChangedSelect1(e, values) {
    this.setState({ unit_id: values.value });
    this.setState({ unit_name: values.label });
    this.props.changeAtribute(true);
  }

  selectsChange(e, values, nameValue, nameLabel) {
    if (nameValue === "origin_till_id") {
      axios.get("/api/tillDetails-sum/" + values.value).then((res) => {
        this.setState({ cajaOriginAmount: res.data.suma });
      });
    }

    if (nameValue === "destiny_till_id") {
      axios.get("/api/tillDetails-sum/" + values.value).then((res) => {
        this.setState({ cajaDestinyAmount: res.data.suma });
      });
    }

    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    this.props.changeAtribute(true);
  }

  handleCreateObject(e) {
    e.preventDefault();
    var validator = {
      amount: { error: false, message: "" },
    };
    var object_error = {};

    axios
      .post("/api/tillsTransfers", {
        origin_till_id: this.state.origin_till_id,
        destiny_till_id: this.state.destiny_till_id,
        tilltrans_amount: this.state.amount,
        tilltrans_observation: this.state.tilltrans_observation,
      })
      .then((res) => {
        if (res.data.dato === false) {
          this.setState({
            snack_open: true,
            message_success: res.data.message,
          });
        } else if (res.data.dato === true) {
          this.props.onSuccess(res.data.message);
          this.props.changeAtribute(false);
          this.handleClose();
        }
      });
    // .catch((error) => {
    //   // console.log(Object.keys(error.response.data.errors));
    //   this.setState({ snack_open: true });
    //   var errores = Object.keys(error.response.data.errors);
    //   for (var i = 0; i < errores.length; i++) {
    //     object_error = {
    //       ...object_error,
    //       [errores[i]]: {
    //         error: true,
    //         message: error.response.data.errors[errores[i]][0],
    //       },
    //     };
    //   }
    //   this.setState({
    //     validator: {
    //       ...validator,
    //       ...object_error,
    //     },
    //   });
    // });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var { till_name, unit_id } = this.state;
    var validator = {
      till_name: { error: false, message: "" },
      unit_id: { error: false, message: "" },
    };
    var object_error = {};
    const object = {
      till_id: this.state.till_id,
      unit_id: this.state.unit_id,
      till_name: this.state.till_name.toUpperCase(),
    };
    const id = object.till_id;
    axios
      .put(`/api/tills/${id}`, {
        till_name: object.till_name,
        unit_id: object.unit_id,
      })
      .then((res) => {
        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  botonEdit() {
    if (this.props.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleUpdateObject}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
      );
    }
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    this.setState({destiny_name: e.target.value})

    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        var url = ''
        if (e.target.value === "") {
          url = `/api/tills-starting`
          this.setState({
            time_out: false,
            url: url,
          });
          axios.get(`${url}`).then((res) => {
            this.setState({
              tills: res.data.data,
              time_out: false,
            });
          }).catch((error) => {
            this.setState({time_out: false});
            console.log("Ingrese un valor valido");
          });

          } else {
          url = `/api/tills?till_name=${e.target.value}`
          this.setState({
            url: url,
          });
          axios.get(`${url}`).then((res) => {
              this.setState({
                tills: res.data.data,
                time_out: false,
              });
            }).catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  render() {
    const { classes } = this.props;
    const { validator, snack_open, message_success } = this.state;
    var showSnack;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3>Formulario de Cajas</h3>
        <hr />
        <form>
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={6}>
                {/* <Autocomplete
                  id="combo-box-demo"
                  disableClearable
                  options={this.selectReturn()}
                  getOptionLabel={(option) => option.label}
                  onChange={(e, values) =>
                    this.selectsChange(
                      e,
                      values,
                      "origin_till_id",
                      "origin_name"
                    )
                  }
                  value={{
                    label: this.state.origin_name,
                    value: this.state.origin_till_id,
                  }}
                  getOptionSelected={(option, value) => {
                    if (value === option.value) {
                      return option.label;
                    } else {
                      return false;
                    }
                  }}
                  style={{ margin: 8 }}
                  fullWidth
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      // error={this.state.validator.unit_id.error}
                      // helperText={this.state.validator.unit_id.message}
                      label="Selecione la caja de origen *"
                      variant="outlined"
                      fullWidth
                    />
                  )}
                /> */}
                <h3> Caja de origen: {" "+this.state.origin_name}</h3>
                <p>Monto en caja{" "+this.state.cajaOriginAmount}</p>
              </Grid>
              <Grid item xs={6}>
                <Autocomplete
                  id="combo-box-demo"
                  disableClearable
                  options={[
                    { label: this.state.destiny_name, value: this.state.destiny_id },
                    ...this.selectReturn(),
                  ]}
                  getOptionLabel={(option) => option.label}
                  onChange={(e, values) =>
                    this.selectsChange(
                      e,
                      values,
                      "destiny_till_id",
                      "destiny_name"
                    )
                  }
                  value={{
                    label: this.state.destiny_name,
                    value: this.state.destiny_till_id,
                  }}
                  getOptionSelected={(option, value) => {
                    if (value === option.value) {
                      return option.label;
                    } else {
                      return false;
                    }
                  }}
                  style={{ margin: 8 }}
                  fullWidth
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      // error={this.state.validator.unit_id.error}
                      // helperText={this.state.validator.unit_id.message}
                      label="Selecione la caja de destino *"
                      variant="outlined"
                      fullWidth
                      onKeyDown={(e) =>{
                        if(e.key === 'Enter'){
                          e.preventDefault();
                          this.search(e)
                        }
                      }}
                      InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                          <React.Fragment>
                            {this.state.time_out === true ? (
                              <CircularProgress color="inherit" size={20} />
                            ) : null}
                            {params.InputProps.endAdornment}
                          </React.Fragment>
                        ),
                      }}
                    />
                  )}
                />
                <p>Monto en caja{" "+this.state.cajaDestinyAmount}</p>
              </Grid>
            </Grid>
          </Grid>

          <div className="form-group">
            <TextField
              id="outlined-full-width"
              label="Monto"
              type="number"
              error={this.state.validator.amount.error}
              helperText={this.state.validator.amount.message}
              style={{ margin: 8 }}
              value={this.state.amount}
              onChange={this.onChangeField1}
              fullWidth
              margin="normal"
              variant="outlined"
            />
          </div>
          <TextField
            id="outlined-full-width"
            label="Observasion"
            type="text"
            multiline={true}
            rows="3"
            style={{ margin: 8 }}
            value={this.state.obs}
            onChange={this.onChangeField2}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          {this.botonEdit()}
          {showSnack}

          <Button
            variant="outlined"
            color="secondary"
            startIcon={<ExitToAppIcon />}
            onClick={this.handleClose}
          >
            Cancelar
          </Button>
        </form>
      </div>
    );
  }
}
export default ModalForm;
