import ModalForm from "./ModalForm";
import React, { Component, lazy } from "react";
import DialogDestroy from "../Dialogs/DialogDestroy";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
  IconButton,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import {format, disformat} from '../GlobalFunctions/Format';

class TillTransfer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      tills: [],
      till_id: 0,
      units: [],
      till: [],
      from_date: "",
      to_date: "",
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredtills: [],
      prevActiveStep: 1,
      rowsPerPage: 10,
      paginator: [],
      order: "asc",
      orderBy: "till_name",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: `/api/tillsTransfers-till/${this.props.till}?filter=false`,
      busqueda: false,
      permissions: {
        till_transfers_index: null,
        till_transfers_show: null,
        till_transfers_store: null,
        till_transfers_update: null,
        till_transfers_destroy: null,
      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order,
      route
    ) => {
      let res;
      if (route) {
        res = await axios.get(
          `${route}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          tills: data,
          paginator: data,
          filteredtills: data.data,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    //funciones
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    // this.clickSortByName = this.clickSortByName.bind(this)
    // this.clickSortByCode = this.clickSortByCode.bind(this)
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, till: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };

    //fin constructor
  }

  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "till_transfers_index",
      "till_transfers_show",
      "till_transfers_store",
      "till_transfers_update",
      "till_transfers_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({
          permissions: res.data,
          from_date: this.getCurrentDate(),
          to_date: this.getCurrentDate(),
        });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  //
  // clickSortByName(){
  //    this.setState({prevActiveStep: 1});
  //     this.getObject(this.state.rowsPerPage, 1);
  //  }
  //
  //  clickSortByCode(){
  //     this.setState({prevActiveStep: 1});
  //      this.getObject(this.state.rowsPerPage, 1);
  //   }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredtills: this.state.tills.data,
            paginator: this.state.tills,
            page_lenght: this.state.tills.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/tills`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/tills?till_name=${e.target.value}`,
          });
          axios
            .get(`${this.state.url}`)
            .then((res) => {
              this.setState({
                filteredtills: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  deleteObject() {
    axios.delete(`api/tills/${this.state.till_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data.message });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      till: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  changeFields(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      filteredtills,
      till,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.new) {
      // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: { onSuccess: this.openSnack, till_id: this.props.till },
          }}
        </MidModal>
      );
    }
    // else if (this.state.edit) {
    //   // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <ModalForm />,
    //         props_form: {
    //           edit: true,
    //           till: this.state.till,
    //           onSuccess: this.openSnack,
    //         },
    //       }}
    //     </MidModal>
    //   );
    // }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={till.data.till_name}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filteredtills.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body">
        {/*<Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillsTransfers-till/${this.props.till}?filter=today`
            );
          }}
        >
          De Hoy
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillsTransfers-till/${this.props.till}?filter=week`
            );
          }}
        >
          De Esta semana
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillsTransfers-till/${this.props.till}?filter=month`
            );
          }}
        >
          De Este mes
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillsTransfers-till/${this.props.till}?filter=false`
            );
          }}
        >
          Todo
        </Button>*/}
        <TextField
          id="date"
          label="Desde"
          type="date"
          name="from_date"
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e) => {
            this.changeFields(e);
          }}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="date"
          label="Hasta"
          type="date"
          name="to_date"
          // defaultValue="2017-05-24"
          value={this.state.to_date}
          onChange={(e) => {
            this.changeFields(e);
          }}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <IconButton
          aria-label="search"
          onClick={() =>
            {this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillsTransfers-till/${this.props.till}?from_date=${this.state.from_date}&to_date=${this.state.to_date}`
            ),
              this.setState({url:`/api/tillsTransfers-till/${this.props.till}?from_date=${this.state.from_date}&to_date=${this.state.to_date}`})
          }
          }
        >
          <SearchIcon />
        </IconButton>
        <hr />
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "origin_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "origin_name"}
                    direction={orderBy === "origin_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("origin_name");
                    }}
                  >
                    <h3>Caja Origen</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "destiny_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "destiny_name"}
                    direction={orderBy === "destiny_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("destiny_name");
                    }}
                  >
                    <h3>Caja Destino</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "tilltrans_date" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "tilltrans_date"}
                    direction={orderBy === "tilltrans_date" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tilltrans_date");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "tilltrans_amount" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "tilltrans_amount"}
                    direction={orderBy === "tilltrans_amount" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tilltrans_amount");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2">
                  {permissions.till_transfers_store === true && ["Administrar", "Cobrar"].includes(this.props.rol) &&(
                    <Tooltip title="Agregar">
                      <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredtills.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredtills.map((data) => {
        return (
          <TableBody key={data.tilltrans_id}>
            <TableRow>
              <TableCell>{data.origin_name}</TableCell>
              <TableCell>{data.destiny_name}</TableCell>
              <TableCell>{data.tilltrans_date}</TableCell>
              <TableCell>{format(Math.round(data.tilltrans_amount))}</TableCell>

              {/*<TableCell>
                {permissions.till_transfers_show === true && (
                  <Link
                    className="navbar-brand"
                    to={"/cajas/show/" + data.till_id}
                  >
                    <Tooltip title="Mostrar">
                      <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<VisibilityIcon />}
                        type="submit"
                        // onClick={() => {
                        //   this.clickEditar({ data: data });
                        // }}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  </Link>
                )}
              </TableCell>

              <TableCell>
                {permissions.till_transfers_update === true && (
                  <Tooltip title="Editar">
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<EditIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
              <TableCell>
                {permissions.till_transfers_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ till_id: data.till_id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>*/}
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}

export default TillTransfer;
