import React from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Drawer,
  CssBaseline,
  AppBar,
  Toolbar,
  List,
  Typography,
  Divider,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Button,
  MenuItem,
  Menu,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorder";
import Collapse from "@material-ui/core/Collapse";
import PersonIcon from "@material-ui/icons/Person";
import HomeIcon from "@material-ui/icons/Home";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import ListIcon from "@material-ui/icons/List";
import SchoolIcon from "@material-ui/icons/School";
import MailIcon from "@material-ui/icons/Mail";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import AssignmentIcon from "@material-ui/icons/Assignment";
import AccountCircle from "@material-ui/icons/AccountCircle";
import clsx from "clsx";
import Routes from "./Routes";
import { getAuth } from "../contexts";
import { Capitalize } from "./GlobalFunctions/LetterCase";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: "#e6e6e6",
    color: "#333",
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

function Header() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [open4, setOpen4] = React.useState(false);
  const [open5, setOpen5] = React.useState(false);
  const [open6, setOpen6] = React.useState(false);
  const [open7, setOpen7] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const handleClick = () => {
    setOpen2(!open2);
  };

  const handleClick2 = () => {
    setOpen3(!open3);
  };

  const handleClick3 = () => {
    setOpen4(!open4);
  };
  const handleClick4 = () => {
    setOpen5(!open5);
  };
  const handleClick5 = () => {
    setOpen7(!open7);
  };
  // const handleProfileMenuOpen = () => {
  //   setOpen6(!open6);
  // };
  const handleProfileMenuOpen = (event) => {
    // console.log(event.currentTarget)
    setAnchorEl(event.currentTarget);
    setOpen6(!open6);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
    // handleMobileMenuClose();
  };

  return (
    <getAuth.Consumer>
      {(context) => {
        var name = `${context.user.name}`;
        var role = "";
        // var roles = context.user.roles;
        if (context.user.roles !== undefined) {
          role = context.user.roles[0];
        }
        return (
          <div className={classes.root}>
            <CssBaseline />
            <AppBar
              position="fixed"
              className={clsx(classes.appBar, {
                [classes.appBarShift]: open,
              })}
            >
              <Toolbar>
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  onClick={handleDrawerOpen}
                  edge="start"
                  className={clsx(classes.menuButton, open && classes.hide)}
                >
                  <MenuIcon />
                </IconButton>
                  <Typography variant="h6" noWrap className={classes.title}>
                    <Link
                      style={{ margin:0, textDecoration: "none" }}
                      className="navbar-brand"
                      to="/"
                    >
                       <p className="title-app">
                        <img style={{width: '170px', marginTop: '8px'}} src={`img/Maius.png`} />
                       </p>
                    </Link>
                  </Typography>
                  <div className="title-page">{context.title_page}</div>
                <div className="user-section">
                  <span style={{ float: "left", textAlign: "right" }}>
                    <div
                      className="user-name"
                    >
                      {Capitalize(name)}
                      <p className="user-role">{role.role_name}</p>
                    </div>

                  </span>
                  <IconButton
                    edge="end"
                    aria-label="account of current user"
                    // aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleProfileMenuOpen}
                    color="inherit"
                    padding={0}
                  >
                    {(context.user.person_photo !== "-" && context.user.person_photo) ? (
                      <span className="MuiIconButton-label">
                        <img
                          className="MuiSvgIcon-root"
                          style={{ borderRadius: "50%", width: 40, height: 40 }}
                          src={`/img/${context.user.person_photo}`}
                        />
                      </span>
                    ) : (
                      <AccountCircle style={{width: 40, height: 40}}/>
                    )}
                    {open6 && (
                      <Menu
                        anchorEl={anchorEl}
                        anchorOrigin={{ vertical: "top", horizontal: "center" }}
                        id="primary-search-account-menu"
                        keepMounted
                        transformOrigin={{
                          vertical: "bottom",
                          horizontal: "right",
                        }}
                        open={open6}
                        onClose={handleMenuClose}
                      >
                        <MenuItem
                          onClick={() => {
                            location.replace(
                              "#/personas/show/" + context.user.person_id
                            );
                          }}
                        >
                          Perfil
                        </MenuItem>
                        <MenuItem onClick={context.logout}>
                          <ExitToAppIcon /> Cerrar Sesion
                        </MenuItem>
                      </Menu>
                    )}
                  </IconButton>
                </div>

                {/*<Button variant="contained" edge="end" color="default" startIcon={<ExitToAppIcon />} onClick={context.logout}>Cerrar Sesión</Button>*/}
              </Toolbar>
            </AppBar>
            <Drawer
              className={classes.drawer}
              variant="persistent"
              anchor="left"
              open={open}
              classes={{
                paper: classes.drawerPaper,
              }}
            >
              <div className={classes.drawerHeader}>
                <IconButton onClick={handleDrawerClose}>
                  {theme.direction === "ltr" ? (
                    <ChevronLeftIcon />
                  ) : (
                    <ChevronRightIcon />
                  )}
                </IconButton>
              </div>
              <Divider />
              <List>
                <Link className="navbar-brand" to="/">
                  <ListItem button>
                    <ListItemIcon>{<HomeIcon />}</ListItemIcon>
                    Inicio
                  </ListItem>
                </Link>

                {(context.permissions.units_index === true ||
                  context.permissions.faculties_index === true ||
                  context.permissions.careers_index === true ||
                  context.permissions.subjects_index === true) && (
                  <ListItem button onClick={handleClick}>
                    <ListItemIcon>
                      <SchoolIcon />
                    </ListItemIcon>

                    <ListItemText primary="Academica" />
                    {open2 ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                )}

                <Collapse in={open2} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {context.permissions.units_index === true && (
                      <Link className="navbar-brand" to="/instituciones">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Instituciones
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.faculties_index === true && (
                      <Link className="navbar-brand" to="/facultades">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Facultades
                        </ListItem>
                      </Link>
                    )}

                    {/*  <Link className='navbar-brand' to='/facuCareers'>
           <ListItem button className={classes.nested}>
             <ListItemIcon>
              {<ListIcon />}
              </ListItemIcon>
              Carreras por facultad
            </ListItem>
            </Link>
*/}
                    {context.permissions.careers_index === true && (
                      <Link className="navbar-brand" to="/carreras">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Carreras
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.careers_index === true && (
                      <Link className="navbar-brand" to="/congresos">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Congresos
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.subjects_index === true && (
                      <Link className="navbar-brand" to="/materias">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Materias
                        </ListItem>
                      </Link>
                    )}
                  </List>
                </Collapse>

                {(context.permissions.scholarship_index === true ||
                  context.permissions.tariffs_index === true ||
                  context.permissions.tills_index === true ||
                  context.permissions.operations_index === true) && (
                  <ListItem button onClick={handleClick3}>
                    <ListItemIcon>
                      <TrendingUpIcon />
                    </ListItemIcon>
                    <ListItemText primary="Administrativa" />
                    {open4 ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                )}

                <Collapse in={open4} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {context.permissions.scholarship_index === true && (
                      <Link className="navbar-brand" to="/comprobantes">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Comprobantes
                        </ListItem>
                      </Link>
                    )}
               
                    {context.permissions.checks_index === true && (
                      <Link className="navbar-brand" to="/cheques">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Cheques
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.paymentTypes_index === true && (
                      <Link className="navbar-brand" to="/formas_pago">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Metodos de Pago
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.tills_index === true && (
                      <Link className="navbar-brand" to="/cajas">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Cajas
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.operations_index === true && (
                      <Link className="navbar-brand" to="/gestiones">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Gestiones
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.operations_index === true && (
                      <Link className="navbar-brand" to="/inscripciones">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Inscripciones
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.studentsmonitoring_index === true && (
                      <Link className="navbar-brand" to="/planilla/congreso">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Mesas de congresos
                        </ListItem>
                      </Link>
                    )}
                  </List>
                </Collapse>

                {(context.permissions.persons_index === true ||
                  context.permissions.students_index === true ||
                  context.permissions.providers_index === true ||
                  context.permissions.employees_index === true ||
                  context.permissions.professors_index === true ||
                  context.permissions.appointments_index === true ||
                  context.permissions.users_index === true ||
                  context.permissions.roles_index === true) && (
                  <ListItem button onClick={handleClick2}>
                    <ListItemIcon>
                      <PersonIcon />
                    </ListItemIcon>
                    <ListItemText primary="Personas" />
                    {open3 ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                )}

                <Collapse in={open3} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {context.permissions.persons_index === true && (
                      <Link className="navbar-brand" to="/personas">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Personas
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.students_index === true && (
                      <Link className="navbar-brand" to="/estudiantes">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Estudiantes
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.employees_index === true && (
                      <Link className="navbar-brand" to="/funcionarios">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Funcionarios
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.professors_index === true && (
                      <Link className="navbar-brand" to="/profesores">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Profesores
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.providers_index === true && (
                      <Link className="navbar-brand" to="/proveedores">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Proveedores
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.appointments_index === true && (
                      <Link className="navbar-brand" to="/cargos">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Cargos
                        </ListItem>
                      </Link>
                    )}

                    {context.permissions.users_index === true && (
                      <Link className="navbar-brand" to="/usuarios">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Usuarios
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.roles_index === true && (
                      <Link className="navbar-brand" to="/roles">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Roles
                        </ListItem>
                      </Link>
                    )}
                  </List>
                </Collapse>
                {(context.permissions.workplaces_index === true ||
                  context.permissions.schools_index === true ||
                  context.permissions.requiredDocs_index === true ||
                  context.permissions.hstitle_index === true ||
                  context.permissions.healtCares_index === true ||
                  context.permissions.healtCares_index === true ||
                  context.permissions.countries_index === true ||
                  context.permissions.departments_index === true ||
                  context.permissions.cities_index === true ||
                  context.permissions.careertypes_index === true ||
                  context.permissions.evaluationTypes_index === true ||
                  context.permissions.ivaType_index === true ||
                  context.permissions.contactTypes_index === true ||
                  context.permissions.tills_index === true ||
                  context.permissions.operations_index === true ||
                  context.permissions.profile_types_index === true ||
                  context.permissions.retentionQuestion_index === true ||
                  context.permissions.retentionAnswer_index === true ||
                  context.permissions.retentionParameter_index === true) && (
                  <ListItem button onClick={handleClick4}>
                    <ListItemIcon>
                      <AssignmentIcon />
                    </ListItemIcon>
                    <ListItemText primary="Definiciones" />
                    {open5 ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                )}

                <Collapse in={open5} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {context.permissions.products_index === true && (
                      <Link className="navbar-brand" to="/productos">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Productos
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.careertypes_index === true && (
                      <Link className="navbar-brand" to="/tipo_carrera">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Tipos de Carreras
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.evaluationTypes_index === true && (
                      <Link className="navbar-brand" to="/tipos_evaluacion">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Tipos de evaluacion
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.ivaType_index === true && (
                      <Link className="navbar-brand" to="/tipos_iva">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Tipos de iva
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.contactTypes_index === true && (
                      <Link className="navbar-brand" to="/tipo_contacto">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Tipos de Contactos
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.workplaces_index === true && (
                      <Link className="navbar-brand" to="/lugares_trabajo">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Lugares de trabajo
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.schools_index === true && (
                      <Link className="navbar-brand" to="/colegios">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Colegios
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.profile_types_index === true && (
                      <Link className="navbar-brand" to="/tipos_perfil">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Tipos de perfil
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.requiredDocs_index === true && (
                      <Link
                        className="navbar-brand"
                        to="/documentos_requeridos"
                      >
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Documentos requidos
                        </ListItem>
                      </Link>
                    )}

                    {context.permissions.hstitle_index === true && (
                      <Link className="navbar-brand" to="/bachillerato">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Titulos de Bachillerato
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.proffesorstype_index === true && (
                      <Link className="navbar-brand" to="/tipo_profesor">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Tipo de profesores
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.healtCares_index === true && (
                      <Link className="navbar-brand" to="/seguros_medicos">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Seguros Medicos
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.countries_index === true && (
                      <Link className="navbar-brand" to="/paises">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Paises
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.departments_index === true && (
                      <Link className="navbar-brand" to="/departamentos">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Departamentos
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.retentionQuestion_index === true && (
                      <Link className="navbar-brand" to="/preguntas_retencion">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Retención - Preguntas
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.cities_index === true && (
                      <Link className="navbar-brand" to="/ciudades">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Ciudades
                        </ListItem>
                      </Link>
                    )}
                  </List>
                </Collapse>
                {(context.permissions.scholarship_index === true ||
                  context.permissions.tariffs_index === true ||
                  context.permissions.tills_index === true ||
                  context.permissions.operations_index === true ||
                  context.permissions.products_index === true) && (
                  <ListItem button onClick={handleClick5}>
                    <ListItemIcon>
                      <TrendingUpIcon />
                    </ListItemIcon>
                    <ListItemText primary="Reportes" />
                    {open7 ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                )}

                <Collapse in={open7} timeout="auto" unmountOnExit>
                  <List component="div" disablePadding>
                    {context.permissions.tills_index === true && (
                      <Link className="navbar-brand" to="/reportes/balances">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                          Balance
                        </ListItem>
                      </Link>
                    )}
                    {context.permissions.products_index === true && (
                      <Link className="navbar-brand" to="/informe_eventos">
                        <ListItem button className={classes.nested}>
                          <ListItemIcon>{<ListIcon />}</ListItemIcon>
                            Informe de eventos
                        </ListItem>
                      </Link>
                    )}
                  </List>
                </Collapse>
              </List>
              <Divider />
            </Drawer>
            <main
              className={clsx(classes.content, {
                [classes.contentShift]: open,
              })}
            >
              <div className={classes.drawerHeader} />
              <Routes changeTitlePage={context.changeTitlePage} />
            </main>
          </div>
        );
      }}
    </getAuth.Consumer>
  );
}

export default Header;
