// import regeneratorRuntime from "regenerator-runtime";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import { HashRouter, Route, Switch } from "react-router-dom";
import Header from "./Header";
import Routes from "./Routes";
import Login from "./Login";
// import Reports from '../Reports';
// import ReactPDF, {PDFDownloadLink} from '@react-pdf/renderer';
import ResetPassword from "./Login/ResetPassword";
import DialogSearchRoute from "./Dialogs/DialogSearchRoute";
import { Snackbar, CircularProgress } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { getAuth } from "../contexts";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title_page: '',
      snack_open: false,
      authenticated: null,
      change_password: false,
      search_route: false,
      validator: {
        email: { error: false, message: "" },
        password: { error: false, message: "" },
      },
      user: [],
      permisos_menu: {
        professor_categories_index:null,
        professor_controls_index:null,
        professor_assistances_index:null,
        checks_index: null,
        provider_account_index: null,
        accounting_plan_index: null,
        paymentTypes_index: null,
        consult_index: null,
        retentionAnswer_index: null,
        products_index: null,
        reserve_index: null,
        holiday_index: null,
        edifice_index: null,
        classroom_index: null,
        section_index:null,
        objectives_index:null,
        promos_index: null,
        classroomtype_index:null,
        providers_index: null,
        providers_show: null,
        users_index: null,
        roles_index: null,
        unit_types_index: null,
        units_index: null,
        careertypes_index: null,
        faculties_index: null,
        subjects_index: null,
        prereqs_index: null,
        careers_index: null,
        scholarship_index: null,
        titles_index: null,
        hstitle_index: null,
        facuCarAdm_index: null,
        semesters_index: null,
        evaluationTypes_index: null,
        studentScholarship_index: null,
        countries_index: null,
        departments_index: null,
        requiredDocs_index: null,
        healtCares_index: null,
        operations_index: null,
        workplaces_index: null,
        documentPerson_index: null,
        persons_index: null,
        students_index: null,
        employees_index: null,
        cities_index: null,
        contactTypes_index: null,
        facuCareers_index: null,
        profile_types_index: null,
        payplans_index: null,
        ivaType_index: null,
        appointments_index: null,
        schools_index: null,
        tills_index: null,
        tills_show: null,
        contactPersons_index: null,
        tariffs_index: null,
        facucars_index: null,
        carsubjs_index: null,
        enrolleds_index: null,
        professors_index: null,
        subevals_index: null,
        assistances_index: null,
        studenthealthcares_index: null,
        studentsmonitoring_index: null,
        proffesorstype_index: null,
        retentionQuestion_index: null,
        retentionParameter_index: null,
        retentionAnswer_index: null,
        professors_show: null,
        employees_show: null,
        faculties_show: null,
        subjects_show: null,
        subjects_store: null,
        careers_show: null,
        contactTypes_show: null,
        persons_show: null,
        students_show: null,
        users_show: null,
        roles_show: null,
        tariffs_show: null,
        retentionQuestion_show: null,
        retentionParameter_show: null,
        retentionAnswer_show: null,
        gen_studyplan: null,
        ticket_index: null,
        category_index: null
      },
    };
    // this.updateState= () =>{
    //   this.setState({
    //     authenticated: true,
    //   });
    // }
    this.login = this.login.bind(this);
    this.resetPassword = this.resetPassword.bind(this);
    this.logout = this.logout.bind(this);
    this.hasPermission = this.hasPermission.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.changeTitlePage = this.changeTitlePage.bind(this);
    this.keyEvent = this.keyEvent.bind(this);
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  componentDidMount() {
    // axios.get('/sanctum/csrf-cookie').then(response => {
    var permissions = [
      "professor_categories_index",
      "professor_assistances_index",
      "professor_controls_index",
      "checks_index",
      "provider_account_index",
      "accounting_plan_index",
      "products_index",
      "classroom_index",
      "consult_index",
      "objectives_index",
      "edifice_index",
      "section_index",
      "paymentTypes_index",
      "retentionAnswer_index",
      "reserve_index",
      "holiday_index",
      "promos_index",
      "classroomtype_index",
      "providers_index",
      "providers_show",
      "users_index",
      "roles_index",
      "unit_types_index",
      "units_index",
      "careertypes_index",
      "faculties_index",
      "subjects_index",
      "prereqs_index",
      "careers_index",
      "scholarship_index",
      "titles_index",
      "hstitle_index",
      "facuCarAdm_index",
      "semesters_index",
      "evaluationTypes_index",
      "studentScholarship_index",
      "countries_index",
      "departments_index",
      "requiredDocs_index",
      "healtCares_index",
      "operations_index",
      "workplaces_index",
      "documentPerson_index",
      "persons_index",
      "students_index",
      "employees_index",
      "cities_index",
      "contactTypes_index",
      "facuCareers_index",
      "profile_types_index",
      "payplans_index",
      "ivaType_index",
      "appointments_index",
      "schools_index",
      "tills_index",
      "tills_show",
      "contactPersons_index",
      "tariffs_index",
      "facucars_index",
      "carsubjs_index",
      "enrolleds_index",
      "professors_index",
      "subevals_index",
      "assistances_index",
      "studenthealthcares_index",
      "studentsmonitoring_index",
      "proffesorstype_index",
      "retentionQuestion_index",
      "retentionParameter_index",
      "retentionAnswer_index",
      "professors_show ",
      "employees_show",
      "faculties_show",
      "subjects_show ",
      "subjects_store",
      "careers_show",
      "contactTypes_show",
      "persons_show",
      "students_show",
      "users_show",
      "roles_show",
      "tariffs_show",
      "retentionQuestion_show",
      "retentionParameter_show",
      "retentionAnswer_show",
      "gen_studyplan",
      "ticket_index",
      "category_index"
    ];
    axios
      .get("/api/user")
      .then((res) => {
        // console.log(res.data);
        if (res.data.reset_password === true) {
          this.setState({ authenticated: true, user: res.data });
          axios
            .get(`/api/roles_has_permission`, {
              params: {
                permission: permissions,
              },
            })
            .then((res) => {
              // console.log('algo');
              this.setState({ permisos_menu: res.data });
            });
            axios
              .get("/api/users_has_roles", {
                params: {
                  rol: "Docente",
                },
              })
              .then((res) => {
                if (res.data) {
                  window.location.replace("/profesores/#/menu/docente");
                  // window.location.reload();
                }
              });
        } else {
          this.setState({
            authenticated: true,
            change_password: true,
            user: res.data,
          });
          // console.log('se debe cambiar el password');
        }

        // console.log('then');
        // console.log(res);
      })
      .catch((error) => {
        this.setState({ authenticated: false });
        // console.log('catch');
      });
    // });
    var element = document.getElementById('page');
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
  }
  logout(e) {
    axios.post("/auth/logout").then((then) => {
      location.reload();
      // location.replace("/");
      // console.log('sesion cerrada!');
    });
  }
  login(e, params) {
    e.preventDefault();
    const { email, password } = params;
    var validator = {
      email: { error: false, message: "" },
      password: { error: false, message: "" },
    };
    var object_error = {};
    axios.get("/sanctum/csrf-cookie").then((response) => {
      axios
        .post("auth/login", { email, password })
        .then((res) => {
          // console.log(res.data.reset_password);
          if (res.data.reset_password === true) {
            axios
              .get("/api/users_has_roles", {
                params: {
                  rol: "Docente",
                },
              })
              .then((res) => {
                if (res.data) {
                  window.location.replace("/profesores#/menu/docente");
                  window.location.reload();
                } else {
                  window.location.reload();
                }
              });
          } else {
            this.setState({
              authenticated: true,
              change_password: true,
              user: res.data,
            });
            // console.log('se debe cambiar el password');
          }
        })
        .catch((error) => {
          this.setState({ snack_open: true });
          var errores = Object.keys(error.response.data.errors);
          for (var i = 0; i < errores.length; i++) {
            object_error = {
              ...object_error,
              [errores[i]]: {
                error: true,
                message: error.response.data.errors[errores[i]][0],
              },
            };
          }
          this.setState({
            validator: {
              ...validator,
              ...object_error,
            },
          });
        });
    });
  }
  resetPassword(e, params) {
    e.preventDefault();
    console.log(params);
    axios
      .put(`/api/reset-password`, {
         ...params,
      })
      .then((res) => {
        location.reload();
      });
  }
  hasPermission(permission, returnValue){
    var permissions = {};
    if (this.state.user.rol_id !== undefined) {
      axios
        .get(`api/roles/${this.state.user.rol_id}/${permission}`)
        .then((res) => {
          returnValue(res.data, permission);
        });
    }
  }
  changeTitlePage(new_title){
    this.setState({title_page: new_title});
  }
  keyEvent(e){
    if(e.altKey && e.which === 70){
      // console.log("presionaste alt + s");
      e.preventDefault();
      this.setState({search_route: true});
    }else if (e.altKey && e.which === 82) {
      e.preventDefault();
      if (this.state.user.roles[0].role_name === 'Cajas') {
        location.replace('/#/menu/caja');
      }
      else if (this.state.user.roles[0].role_name === 'Secretaria Academica'){
        location.replace('/#/menu/academico');
      }
      else if (this.state.user.roles[0].role_name === 'Administrador de parte Administrativa'){
        location.replace('/#/menu/administracion');
      }
      else if (this.state.user.roles[0].role_name === 'Administrador del sistema'){
        location.replace('/#/menu/sistema');
      }
      else if (this.state.user.roles[0].role_name === 'Recepcion'){
        location.replace('/#/menu/recepcion');
      }
    }
    else {
      // console.log("Yo no escuche nada");
      // console.log(e.which);
      // console.log(e);
    }
  }
  render() {
    var renderizar, showSnack, showDialogRoutes;
    // renderizar = <PDFDownloadLink document={<Reports />} fileName="somename.pdf">
    //   {({ blob, url, loading, error }) => (loading ? 'Loading document...' : 'Download now!')}
    // </PDFDownloadLink>

    const { snack_open, validator } = this.state;
    if (this.state.authenticated === false) {
      renderizar = <Login />;
    } else if (this.state.change_password === true) {
      renderizar = <ResetPassword />;
    } else {
      renderizar = (
        <HashRouter>
          <div
            id="page"
            tabIndex={0}
            onKeyDown={(e)=>this.keyEvent(e)}
          >
            <Header />
          </div>
        </HashRouter>
      );
    }
    if (this.state.search_route) {
      showDialogRoutes = (<DialogSearchRoute rol={this.state.user.roles[0]} onClose={()=>this.setState({search_route: false})}/>);
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["email"]["error"] ? (
              <li>Error: {validator["email"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["password"]["error"] ? (
              <li>Error: {validator["password"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }

    return (
      <getAuth.Provider
        value={{
          handleSubmit: this.login,
          resetPassword: this.resetPassword,
          logout: this.logout,
          user: this.state.user,
          hasPermission: this.hasPermission,
          permissions: this.state.permisos_menu,
          title_page: this.state.title_page,
          changeTitlePage: this.changeTitlePage
        }}
      >
        <div style={{ height: "100%", width: "100%" }}>
          {renderizar}
          {showSnack}
          {showDialogRoutes}
        </div>
      </getAuth.Provider>
    );
  }
}
if (document.getElementById("app")) {
  ReactDOM.render(<App />, document.getElementById("app"));
}
