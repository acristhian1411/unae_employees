import React, { Component, lazy } from "react";
import { Link } from "react-router-dom";
// import ModalCountry from "./ModalCountry";
import SearchIcon from "@material-ui/icons/Search";
import ClearIcon from "@material-ui/icons/Clear";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {format} from '../GlobalFunctions/Format';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import {
  Button,
 
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  Grid,
  Select,
  MenuItem,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Autocomplete,Alert } from "@material-ui/lab";
// const ModalCountry = lazy(()=> import('./ModalCountry'));
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

class DettillProofPayment extends Component {
  constructor() {
    super();
    var d = new Date();
    var m = d.getMonth() + 1
    if(m<10 && d.getDate()>=10){
      var fecha = d.getFullYear()+'-0'+m+'-'+d.getDate()
    }else if (m>=10 && d.getDate()<10) {
      var fecha = d.getFullYear()+'-'+m+'-0'+d.getDate()
    }else if (m<10 && d.getDate()<10) {
      var fecha = d.getFullYear()+'-0'+m+'-0'+d.getDate()
    }else{
      var fecha = d.getFullYear()+'-'+m+'-'+d.getDate()
    }
    this.state = {
      snack_open: false,
      message_success: "",
      countries: [],
      country_id: 0,
      pay_type_id: '',
      student_id: '',
      student_name: '',
      pay_type_desc: '',
      date_start: fecha,
      date_end: fecha,
      country_id: 0,
      country: [],
      edit: false,
      paytype:[],
      students:[],
      new: false,
      time_out_paytype:false,
      time_out_student:false,
      time_out:false,
      open: false,
      dialog: false,
      search: "",
      filteredCountries: [],
      prevActiveStep: 1,
      paginator: [],
      rowsPerPage: 100,
      order: "asc",
      dettills_pr_pay_desc:'',
      orderBy: "dettills_pr_pay_desc",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/dettill_proof",
      busqueda: false,
      permissions: {
        dettill_proofpayment_index: null,
        dettill_proofpayment_show: null,
        dettill_proofpayment_store: null,
        dettill_proofpayment_update: null,
        dettill_proofpayment_destroy: null,
      },
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          countries: data,
          paginator: data,
          filteredCountries: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    this.handleClickOpen = (data) => {
      this.setState({ open: true, country: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    // pagina siguiente
    this.handleNext = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep + 1 });
      var newPage = this.state.prevActiveStep + 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //pagina anterior
    this.handleBack = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep - 1 });
      var newPage = this.state.prevActiveStep - 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //filas por pagina
    this.handleChangeSelect = (event) => {
      this.setState({ rowsPerPage: event.target.value, prevActiveStep: 1 });
      this.getObject(event.target.value, 1);
    };
  }

  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("COMPROBANTES");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "dettill_proofpayment_index",
      "dettill_proofpayment_show",
      "dettill_proofpayment_store",
      "dettill_proofpayment_update",
      "dettill_proofpayment_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {

          axios.get('/api/paymentTypes').then((res) => {
            this.setState({
                paytype:res.data.data
            })
          })
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredCountries: this.state.countries.data,
            paginator: this.state.countries,
            page_lenght: this.state.countries.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/dettill_proof`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/dettill_proof?country_name=${e.target.value}`,
          });
          axios
            .get(
              `${this.state.url}&sort_by=${this.state.orderBy}&order=${
                this.state.order
              }&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filteredCountries: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  deleteObject() {
    axios.delete(`api/dettill_proof/${this.state.country_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  clickEditar(data) {
    console.log("boton editar");
    this.setState({
      edit: true,
      country: data,
    });
  }

  clickAgregar() {
    console.log("boton agregar");
    if(this.state.url == '/api/dettill_proof'){
      open(`/reportes/comprobantes?type=dettill_proof_payment&filters=false`);
    }else{
      open(`/reportes/comprobantes?type=dettill_proof_payment&filters=true&filter[person_id]=${this.state.student_id}&
      filter[person_id]=${this.state.student_id}&
        filter_label[person_name]=${this.state.student_name}&
        filter[pay_type_id]=${this.state.pay_type_id}&
        filter_label[pay_type_desc]=${this.state.pay_type_desc}&
        filter[date_start]=${this.state.date_start}&
        filter[date_end]=${this.state.date_end}&
        filter[dettills_desc]=${this.state.dettills_pr_pay_desc}
      `);
    }
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.prevActiveStep,
      name,
      order
    );
  }

  searchPaytype(event){

    var e = event;
    this.setState({pay_type_desc : e.target.value})
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out_paytype == false) {
      this.setState({ time_out_paytype: true });
      setTimeout(() => {
        if (e.target.value === "") {
          // this.getfacus();
          this.setState({time_out_paytype: false});
        } else {
    axios
    .get(
      `/api/paymentTypes-search/${e.target.value}`
    )
    .then((res) => {

      this.setState({
        paytype: res.data.data,
        time_out_paytype: false,
      });
    })
    .catch((error) => {
      console.log("Ingrese un valor valido");
    });

      }
    })
  }
  }

  searchStudent(event){

    var e = event;
    this.setState({student_name : e.target.value})
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out_student == false) {
      this.setState({ time_out_student: true });
      setTimeout(() => {
        if (e.target.value === "") {
          // this.getfacus();
          this.setState({time_out_student: false});
        } else {
    axios
    .get(
      `/api/students-search?search=${e.target.value}&all=true`
    )
    .then((res) => {

      this.setState({
        students: res.data.data,
        time_out_student: false,
      });
    })
    .catch((error) => {
      this.setState({time_out_student: false});
      console.log("Ingrese un valor valido");
    });

      }
    })
  }
  }

  searchIndex(event){

    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          // this.getfacus();
          this.setState({time_out: false});
        } else {
    axios
    .get(
      `/api/dettill_proof-search?
      filter[person_id]=${this.state.student_id}&
      filter_label[person_name]=${this.state.student_name}&
      filter[pay_type_id]=${this.state.pay_type_id}&
      filter_label[pay_type_desc]=${this.state.pay_type_desc}&
      filter[date_start]=${this.state.date_start}&
      filter[date_end]=${this.state.date_end}&
      filter[dettills_desc]=${this.state.dettills_pr_pay_desc}
      &per_page=${this.state.rowsPerPage}
      &sort_by=${this.state.orderBy}
      &order=${this.state.order}`
    )
    .then((res) => {

      this.setState({
        filteredCountries: res.data.data,
        time_out: false,
        page_lenght: res.data.last_page,
        page: res.data.current_page,
        url:`/api/dettill_proof-search?
        filter[person_id]=${this.state.student_id}&
        filter_label[person_name]=${this.state.student_name}&
        filter[pay_type_id]=${this.state.pay_type_id}&
        filter_label[pay_type_desc]=${this.state.pay_type_desc}&
        filter[date_start]=${this.state.date_start}&
        filter[date_end]=${this.state.date_end}&
        filter[dettills_desc]=${this.state.dettills_pr_pay_desc}
        &per_page=${this.state.rowsPerPage}
        &sort_by=${this.state.orderBy}
        &order=${this.state.order}`
      });
    })
    .catch((error) => {
      this.setState({time_out: false});
      console.log("Ingrese un valor valido");
    });

      }
    })
  }
  }

  fieldsChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }
  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }

  Paytype() {
    return this.state.paytype.map((data) => ({
      label: data.pay_type_desc,
      value: data.pay_type_id,
    }));
  }
  Students() {
    return this.state.students.map((data) => ({
      label: data.person_fname + ' ' + data.person_lastname,
      value: data.person_id,
    }));
  }

  cleanfilter() {
    this.setState({
      page: 1,
      pay_type_id: '',
      student_id: '',
      student_name: '',
      pay_type_desc: '',
      dettills_pr_pay_desc:''
    });
    this.getObject(this.state.rowsPerPage, this.state.page);
  }
  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      country,
      orderBy,
      order,
      permissions,
    } = this.state;
    // if (this.state.new) {
    //   // showModal = <ModalCountry edit={false} onHandleSubmit={this.updateState}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <ModalCountry />,
    //         props_form: { onSuccess: this.openSnack },
    //       }}
    //     </MidModal>
    //   );
    // } else if (this.state.edit) {
    //   // showModal = <ModalCountry edit={true} onHandleSubmit={this.updateState} pais={this.state.country}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <ModalCountry />,
    //         props_form: {
    //           edit: true,
    //           pais: this.state.country,
    //           onSuccess: this.openSnack,
    //         },
    //       }}
    //     </MidModal>
    //   );
    // }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={country.data.country_name}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (this.state.filteredCountries.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body" style={{textAlign: "center"}}>
          <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
          <Grid item xs={3}>
          <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[this.selectValue("pay_type_desc", "pay_type_id"), ...this.Paytype()]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>{
                      this.setState({pay_type_id: values.value, pay_type_desc: values.label})
                    }
                    }
                    value={{label: this.state.pay_type_desc, value:this.state.pay_type_id}}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Forma pago "
                        className="textField"
                        onChange={(e)=>{this.searchPaytype(e)}}
                      size="small"
                      id="outlined-search"
                      variant="outlined"
                        name="pay_type_id"
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {this.state.time_out_paytype === true ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                      />
                    )}
                  />
        </Grid>
            <Grid item xs={3}>
            <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[this.selectValue("student_name", "student_id"), ...this.Students()]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>{
                      this.setState({student_id: values.value, student_name: values.label})
                    }
                    }
                    value={{label: this.state.student_name, value:this.state.student_id}}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Estudiante "
                        className="textField"
                        onChange={(e)=>{this.searchStudent(e)}}
                      size="small"
                      id="outlined-search"
                      variant="outlined"
                        name="student_id"
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {this.state.time_out_student === true ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                      />
                    )}
                  />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Comprobante"
                type="search"
                variant="outlined"
                size="small"
                name="dettills_pr_pay_desc"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.dettills_pr_pay_desc}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Fecha inicio"
                type="date"
                variant="outlined"
                size="small"
                name="date_start"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.date_start}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Fecha fin"
                type="date"
                variant="outlined"
                size="small"
                name="date_end"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.date_end}
              />
            </Grid>
            <Grid item xs={3}>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SearchIcon />}
                onClick={this.searchIndex.bind(this)}
              >
                Buscar
              </Button>
              <Tooltip title="Limpiar filtro">
                <Button
                  variant="contained"
                  color="secondary"
                  size="small"
                  startIcon={<ClearIcon />}
                  onClick={() => {
                    this.cleanfilter();
                  }}
                >
                  {" "}
                </Button>
              </Tooltip>
            </Grid>
          </Grid>

        </Grid>
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
              <TableCell
                  sortDirection={orderBy === "pay_type_desc" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "pay_type_desc"}
                    direction={orderBy === "pay_type_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("pay_type_desc");
                    }}
                  >
                    <h3>Forma de pago</h3>
                  </TableSortLabel>
                </TableCell>


                <TableCell
                  sortDirection={orderBy === "dettills_pr_pay_desc" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "dettills_pr_pay_desc"}
                    direction={orderBy === "dettills_pr_pay_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("dettills_pr_pay_desc");
                    }}
                  >
                    <h3>Comprobante</h3>
                  </TableSortLabel>
                </TableCell>
                
                <TableCell
                  sortDirection={orderBy === "person_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_name"}
                    direction={orderBy === "person_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_name");
                    }}
                  >
                    <h3>Estudiante</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "date" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "date"}
                    direction={orderBy === "date" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("date");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "amount" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "amount"}
                    direction={orderBy === "amount" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("amount");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="3">
                  {permissions.dettill_proofpayment_store === true && this.state.url != '/api/dettill_proof' &&(
                    <Tooltip title="Exportar">
                      <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<PictureAsPdfIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>

            {this.renderList()}
          </Table>
        </TableContainer>
       

        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}

        {this.state.dialog === true && (
                <Dialog
                  open={this.state.dialog}
                  onClose={() => {
                    this.setState({ dialog: false });
                  }}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="alert-dialog-title">{`Comprobante`}</DialogTitle>
                  <DialogContent>
                  
                        <div>
                          <div className="total_title">{this.state.country.dettills_pr_pay_desc}</div>             
                  
                  <img
                     src={`/img/${this.state.country.dettills_pr_pay_image}`}
                  />
                            
                        </div>

                  </DialogContent>
                  <DialogActions>
                    <Button
                      color="secondary"
                      autoFocus
                      onClick={() => {
                        this.setState({ dialog: false });
                      }}
                    >
                      Cancelar
                    </Button>
                    <Button
                      color="primary"
                      variant="outlined"
                      onClick={() => {
                        this.setState({ dialog: false })
                      }}
                    >
                      Guardar
                    </Button>
                  </DialogActions>
                </Dialog>
              )}


      </div>
    );
  }


  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredCountries.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredCountries.map((data) => {
        return (
          <TableBody key={data.dettills_id}>
            <TableRow>
              <TableCell>{data.pay_type_desc}</TableCell>
              <TableCell>{data.dettills_pr_pay_desc}</TableCell>
              <TableCell>{data.person_name}</TableCell>
              <TableCell>{data.date}</TableCell>
              <TableCell>{format(data.amount)}</TableCell>

              <TableCell>
                {/* {permissions.dettill_proofpayment_update === true && ()} */}
                  <Tooltip title="Mostrar">
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<VisibilityIcon />}
                      type="submit"
                      onClick={() => {
                        this.setState({ country: data, dialog:true })
                      }}
                    > 
                      
                    </Button>
                  </Tooltip>
                
              </TableCell>

              <TableCell>
                {permissions.dettill_proofpayment_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ country_id: data.country_id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
export default DettillProofPayment;
