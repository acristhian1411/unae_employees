import React, { Component, Fragment } from "react";
import {
  Button,
  TextField,
  Grid,
  Tooltip,
  FormControlLabel,
  Checkbox,
  Snackbar,
  Fab
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import SaveIcon from "@material-ui/icons/Save";
import AddIcon from "@material-ui/icons/Add";
import InputAdornment from "@material-ui/core/InputAdornment";
import RemoveIcon from "@material-ui/icons/Remove";
import DialogDestroy from "../Dialogs/DialogDestroy";

class FormPaymentType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      snack_openDestroy: false,
      open: false,
      name: "",
      pay_type_id: 0,
      pr_payment: [{
        pay_type_id: 0,
        pr_payment_desc: "Ninguno",
        pr_requires_image: false,
        created: false,
        edited: false,
      }],
      pr_payment_desc: "",
      pr_payment_id: 0,
      sucess: false,
      error: false,
      time_out: false,
      validator: {
        pay_type_desc: {
          message: "",
          error: false,
        },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleCloseAlert = this.handleCloseAlert.bind(this);
    this.fieldChange1 = this.fieldChange1.bind(this);
    this.fieldChange2 = this.fieldChange2.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.selectValue = this.selectValue.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.closeSnackDestroy = this.closeSnackDestroy.bind(this);
    this.openSnack = this.openSnack.bind(this);
  }

  handleOpen(data) {
    //habra que ver si esta funcion y el open sirven para algo en los formularios
    this.setState({ open: true, pr_payment_desc: data });
  }

  closeSnack() {
    this.setState({ snack_open: false });
  }
  closeSnackDestroy() {
    this.setState({ snack_openDestroy: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  handleClose() {
    this.setState({ open: false });
    this.props.onHandleSubmit();
  }
  fieldChange1(e) {
    this.setState({ [e.target.name]: e.target.value });
    this.props.changeAtribute(true);
  }
  fieldChange2(e, values) {
    // console.log(values);
    this.setState({
      pay_type_id: values.value,
      ivatype_description: values.label,
    });
    this.props.changeAtribute(true);
  }
  searchFieldChange(event) {
    var e = event;
    this.setState({ ivatype_description: e.target.value, pay_type_id: null });
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          axios.get("api/paymentTypes").then((res) => {
            this.setState({ iva_types: res.data.data, time_out: false });
          });
        } else {
          axios
            .get(`api/paymentTypes/${e.target.value}`)
            .then((res) => {
              this.setState({
                iva_types: res.data.data,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }
  handleSubmit(e) {
    e.preventDefault();
    var { pay_type_desc } = this.state;

    var object_error = {};
    axios.post("api/paymentTypes", { pay_type_desc }).then((res) => {
      axios.post("api/proofPayments", {
        proof_payment: this.state.pr_payment,
        pay_type_id: res.data.dato
      }).then((res) => {
        this.props.onSuccess(res.data);
      });

      this.props.onSuccess(res.data.message);
      this.props.changeAtribute(false);
      this.setState({
        pay_type_desc: "",
      });
      this.handleClose();
    });

  }

  handleSubmit1(e) {
    e.preventDefault();
    var { pay_type_desc } = this.state;

    axios
      .put(`api/paymentTypes/${this.props.payment_type.data.pay_type_id}`, {
        pay_type_desc,
      })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.handleClose();
      });
    axios
      .put(`api/proofPayments`, {
        proof_payment: this.state.pr_payment,
      })
      .then((res) => {
        this.props.onSuccess(res.data);
      });
    axios.post("api/proofPayments", {
      proof_payment: this.state.pr_payment,
      pay_type_id: this.state.pay_type_id
    }).then((res) => {
      this.props.onSuccess(res.data);
    });
  }
  componentDidMount() {

    if (this.props.edit) {
      const { pay_type_desc, pay_type_id } = this.props.payment_type.data;
      this.setState({
        pay_type_desc,
        pay_type_id
      });
      axios.get("/api/proofPayments/" + this.props.payment_type.data.pay_type_id).then((response) => {
        var a = [];
        if (response.data != false) {
          response.data.data.forEach((item, i) => {
            a.push({
              pr_payment_id: item.pr_payment_id,
              pay_type_id: item.pay_type_id,
              pr_payment_desc: item.pr_payment_desc,
              pr_requires_image: item.pr_requires_image,
              created: true,
              edited: false,
            });
          });
        } else {
          a = [{
            pay_type_id: 0,
            pr_payment_desc: "Ninguno",
            pr_requires_image: false,
            created: false,
            edited: false,
          }]
        }
        this.setState({ pr_payment: a });
      });
    }
  }

  handleInputChange(index, event) {
    const values = [...this.state.pr_payment];
    values[index].pr_payment_desc = event.target.value;
    values[index].edited = true;
    this.setState({
      pr_payment: values,
    });
  }

  handleAddFields() {
    var values = [...this.state.pr_payment];
    values.push({
      pay_type_id: 0,
      pr_payment_desc: "",
      pr_requires_image: "",
      created: false,
      edited: false,
    });
    this.setState({
      pr_payment: values,

    });
  }

  handleRemoveField(index) {
    var array = this.state.pr_payment;
    if (array[index].created === false) {
      array.splice(index, 1);
      this.setState({
        carsub_professor: array,
      });
    } else {
      this.setState({
        index: index,
        pr_payment_id: array[index].pr_payment_id,
      });
      this.handleOpen(array[index].pr_payment_desc);
    }
  }

  destroy() {
    axios
      .delete(`/api/proofPayments/${this.state.pr_payment_id}`)
      .then((res) => {
        var values = [...this.state.pr_payment];
        values.splice(this.state.index, 1);
        this.setState({
          snack_openDestroy: true,
          message_success: res.data,
          pr_payment: values,
        });
      });
  }

  editOrNew() {
    if (this.props.edit) {
      return (
        <Button
          name="save"
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit1}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          name="save"
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit}
        >
          Guardar
        </Button>
      );
    }
  }
  selectValue() {
    return {
      label: this.state.ivatype_description,
      value: this.state.pay_type_id,
    };
  }
  handleCloseAlert() {
    this.setState({
      open: false,
    });
  }
  render() {
    const { validator, snack_open } = this.state;
    var showDialogDestroy;
    var showSnack;
    var showSnackDestoy;
    if (this.state.open === true) {
      showDialogDestroy = (
        <DialogDestroy
          index={`${this.state.pr_payment_desc}`}
          onClose={this.handleCloseAlert}
          onHandleAgree={() => {
            this.destroy();
          }}
        />
      );
    }
    if (this.state.snack_openDestroy) {
      showSnackDestoy = (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={this.state.snack_openDestroy}
          autoHideDuration={6000}
          onClose={this.closeSnackDestroy}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnackDestroy}
            severity="success"
          >
            {this.state.message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["pay_type_id"]["error"] ? (
              <li>Error: {validator["pay_type_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["name"]["error"] ? (
              <li>Error: {validator["pay_type_desc"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }

    return (
      <div>
        <h3>Formulario de Formas de Pago</h3>
        <hr />
        <form>
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={0}></Grid>
          </Grid>
          <TextField
            id="outlined-full-width"
            error={this.state.validator.pay_type_desc.error}
            helperText={this.state.validator.pay_type_desc.message}
            label="Descripción *"
            style={{ margin: 8 }}
            name="pay_type_desc"
            value={this.state.pay_type_desc}
            onChange={this.fieldChange1}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <h3>Comprobantes de pago</h3>
          {this.state.pr_payment.map((inputField, index) => (
            <Fragment key={`${inputField}~${index}`}>
              <Grid container item xs={12} spacing={1}>
                <Grid item xs={6} style={{ textAlign: "center" }}>
                <FormControlLabel
              value="end"
              control={
                <Checkbox
                  color="primary"
                  inputProps={{
                    "aria-label": "secondary checkbox",
                  }}
                  checked={inputField.pr_requires_image}
                  onClick={() => {
                    var c = this.state.pr_payment;
                    c[index].edited = true;
                    c[index].pr_requires_image = !c[index].pr_requires_image;
                    this.setState({
                      pr_payment: c
                    });
                  }}
                />
              }
              label="Requiere imagen"
              labelPlacement="end"
            />
                </Grid>
                <Grid item xs={6} style={{ textAlign: "center" }}>
                  <TextField
                    variant="outlined"
                    required
                    label="Email Address"
                    className="email"
                    size="small"
                    id="pr_payment_desc"
                    name="pr_payment_desc"
                    value={inputField.pr_payment_desc}
                    onChange={(event) =>
                      this.handleInputChange(index, event)
                    }
                    label="Dato "
                    style={{ margin: 8 }}
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    InputProps={{
                      endAdornment: index + 1 <=
                        this.state.pr_payment.length && (
                          <InputAdornment position="start">
                            <Tooltip title="Agregar Comprobante">
                              <Fab
                                color="primary"
                                size="small"
                                onClick={() => this.handleAddFields()}
                              >
                                <AddIcon />
                              </Fab>
                            </Tooltip>
                            <Tooltip title="Eliminar Comprobante">
                              <Fab
                                color="secondary"
                                size="small"
                                onClick={() =>
                                  this.handleRemoveField(index)
                                }
                              >
                                <RemoveIcon />
                              </Fab>
                            </Tooltip>
                          </InputAdornment>
                        ),
                    }}
                  />
                </Grid>
              </Grid>
            </Fragment>
          ))}

          {this.editOrNew()}
          {showSnack}
          {showDialogDestroy}
          {showSnackDestoy}
          {/*<Button variant="outlined" color="secondary" startIcon={<ExitToAppIcon />} onClick={this.handleClose}>Cerrar</Button>
           */}
        </form>
      </div>
    );
  }
}
export default FormPaymentType;
