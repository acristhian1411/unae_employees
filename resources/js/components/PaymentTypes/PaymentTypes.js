import FormPaymentType from "./FormPaymentType";
import MidModal from "../Modals/MidModal";
import React, { Component, lazy } from "react";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
// const FormPaymentType = lazy(()=> import('./FormPaymentType'));
import { Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import Paginator from "../Paginators/Paginator";

class PaymentTypes extends Component {
  constructor() {
    super();
    this.state = {
      snack_open: false,
      message_success: "",
      paymentTypes: [],
      pay_type_id: 0,
      payment_type: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredpaymentTypes: [],
      rowsPerPage: 10,
      paginator: [],
      order: "asc",
      orderBy: "pay_type_desc",
      page_lenght: 0,
      page: 1,
      time_out: false,
      busqueda: false,
      url: "/api/paymentTypes",
      permissions: {
        paymentTypes_index: null,
        paymentTypes_show: null,
        paymentTypes_store: null,
        paymentTypes_update: null,
        paymentTypes_destroy: null,
      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;

      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          paymentTypes: data,
          paginator: data,
          filteredpaymentTypes: data.data,
          open: false,
          page_lenght: res.data.last_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    //funciones
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    // this.clickSortBypay_type_desc = this.clickSortBypay_type_desc.bind(this)
    // this.clickSortByCode = this.clickSortByCode.bind(this)
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);
    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, payment_type: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    //fin constructor
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    this.props.changeTitlePage("METODOS DE PAGO");
    var permissions = [
      "paymentTypes_index",
      "paymentTypes_show",
      "paymentTypes_store",
      "paymentTypes_update",
      "paymentTypes_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }

  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredpaymentTypes: this.state.paymentTypes.data,
            paginator: this.state.paymentTypes,
            page_lenght: this.state.paymentTypes.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/paymentTypes`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/paymentTypes?pay_type_desc=${e.target.value}`,
          });
          axios
            .get(`${this.state.url}&per_page=${this.state.rowsPerPage}`)
            .then((res) => {
              this.setState({
                filteredpaymentTypes: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  deleteObject() {
    axios.delete(`/api/paymentTypes/${this.state.pay_type_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data.message });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      payment_type: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(pay_type_desc) {
    var order = "";
    if (this.state.orderBy === pay_type_desc) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: pay_type_desc, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.page,
      pay_type_desc,
      order
    );
  }

  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      payment_type,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.new) {
      // showModal = <FormPaymentType edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormPaymentType />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormPaymentType />,
            props_form: {
              edit: true,
              payment_type: this.state.payment_type,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={payment_type.data.pay_type_desc}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (this.state.filteredpaymentTypes.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div classpay_type_desc="card-body" style={{textAlign: "center"}}>
          <TextField
            id="outlined-search"
            label="Escriba para buscar"
            type="search"
            name="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "pay_type_desc" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "pay_type_desc"}
                    direction={orderBy === "pay_type_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("pay_type_desc");
                    }}
                  >
                    <h3>Descripcion</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell colSpan="2">
                  {permissions.paymentTypes_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        name="new_item"
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>

        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
        <br />
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    if (this.state.filteredpaymentTypes.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredpaymentTypes.map((data) => {
        return (
          <TableBody key={data.pay_type_id}>
            <TableRow>
              <TableCell>{data.pay_type_desc}</TableCell>

              <TableCell>
                {this.state.permissions.paymentTypes_update === true && (
                  <Tooltip title="Editar">
                    <Button
                      name="edit_item"
                      variant="outlined"
                      color="primary"
                      startIcon={<EditIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
              <TableCell>
                {this.state.permissions.paymentTypes_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      name="delete_item"
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({
                            pay_type_id: data.pay_type_id,
                          });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}

export default PaymentTypes;
