import React, { Component } from "react";
import { Link } from "react-router-dom";
import ModalForm from "./ModalForm";
import Paginator from "../Paginators/Paginator";
import MidModal from "../Modals/MidModal";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

class ContactTypesList extends Component {
  constructor() {
    super();
    this.state = {
      snack_open: false,
      message_success: "",
      contactTypes: [],
      cnttype_id: 0,
      contactType: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredContactTypes: [],
      paginator: [],
      prevActiveStep: 1,
      rowsPerPage: 10,
      order: "asc",
      orderBy: "cnttype_name",
      page_lenght: 0,
      page: 1,
      time_out: false,
      permissions: {
        contactTypes_index: null,
        contactTypes_show: null,
        contactTypes_store: null,
        contactTypes_update: null,
        contactTypes_destroy: null,
      },
    };

    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res = await axios.get(
        `/api/contactTypes?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
      );
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          contactTypes: data,
          paginator: data,
          filteredContactTypes: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };
    this.handleClickOpen = (data) => {
      this.setState({ open: true, contactType: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    // pagina siguiente
    this.handleNext = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep + 1 });
      var newPage = this.state.prevActiveStep + 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //pagina anterior
    this.handleBack = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep - 1 });
      var newPage = this.state.prevActiveStep - 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //filas por pagina
    this.handleChangeSelect = (event) => {
      this.setState({ rowsPerPage: event.target.value, prevActiveStep: 1 });
      this.getObject(event.target.value, 1);
    };
  }

  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("TIPOS DE CONTACTO");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "contactTypes_index",
      "contactTypes_show",
      "contactTypes_store",
      "contactTypes_update",
      "contactTypes_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  // search(e) {
  //   if (e.target.value === "") {
  //     this.setState({
  //       filteredContactTypes: this.state.contactTypes.data,
  //       paginator: this.state.contactTypes,
  //       page_lenght: this.state.contactTypes.last_page,
  //     });
  //   } else {
  //     axios
  //       .get(`/api/contactTypes?cnttype_name=${e.target.value}`)
  //       .then((res) => {
  //         this.setState({
  //           filteredContactTypes: res.data.data,
  //           paginator: res.data,
  //           page_lenght: res.data.last_page,
  //         });
  //       });
  //   }
  // }
  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredContactTypes: this.state.contactTypes.data,
            paginator: this.state.contactTypes,
            page_lenght: this.state.contactTypes.last_page,
            page: 1,
            time_out: false,
          });
        } else {
          axios
            .get(`/api/contactTypes?cnttype_name=${e.target.value}`)
            .then((res) => {
              this.setState({
                filteredContactTypes: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
            });
        }
    //   }, 3000);
    // }
  }

  deleteObject() {
    // console.log(this.state.cnttype_id)
    axios.delete(`/api/contactTypes/${this.state.cnttype_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      contactType: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.prevActiveStep,
      name,
      order
    );
  }
  render() {
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      contactType,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.new) {
      // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState} />
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      // showModal = <ModalForm  edit={true} contactType={this.state.contactType} onHandleSubmit={this.updateState} />
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: {
              edit: true,
              contactType: this.state.contactType,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={contactType.data.cnttype_name}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    const { paginator } = this.state;

    return (
      <div className="card-body" style={{textAlign: "center"}}>
          <TextField
            id="outlined-search"
            label="Escriba para buscar"
            type="search"
            name="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "cnttype_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "cnttype_name"}
                    direction={orderBy === "cnttype_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("cnttype_name");
                    }}
                  >
                    <h3>Nombre</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="3">
                  {permissions.contactTypes_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        name="new_item"
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>

            {this.renderList()}
          </Table>
        </TableContainer>
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />

        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    return this.state.filteredContactTypes.map((data) => {
      return (
        <TableBody key={data.cnttype_id}>
          <TableRow>
            <TableCell>{Capitalize(data.cnttype_name)}</TableCell>

            <TableCell>
              {permissions.contactTypes_update === true && (
                <Tooltip title="Editar">
                  <Button
                    name="edit_item"
                    variant="outlined"
                    color="primary"
                    startIcon={<EditIcon />}
                    type="submit"
                    onClick={() => {
                      this.clickEditar({ data: data });
                    }}
                  >
                    {" "}
                  </Button>
                </Tooltip>
              )}
            </TableCell>

            <TableCell>
              {permissions.contactTypes_destroy === true && (
                <Tooltip title="Eliminar">
                  <Button
                    name="delete_item"
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      this.handleClickOpen({ data: data }),
                        this.setState({ cnttype_id: data.cnttype_id });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </Tooltip>
              )}
            </TableCell>
          </TableRow>
        </TableBody>
      );
    });
  }
}

export default ContactTypesList;
