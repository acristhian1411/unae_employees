import React, { Component } from "react";
import {
  Modal,
  Backdrop,
  Fade,
  InputLabel,
  TextField,
  Button,
  Snackbar,
} from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { Alert } from "@material-ui/lab";

class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      open: true,
      setOpen: true,
      cnttype_id: 0,
      cnttype_name: "",
      edit: false,
      errors: [],
      validator: {
        cnttype_name: {
          message: "",
          error: false,
        },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onChangeContactTypeId = this.onChangeContactTypeId.bind(this);
    this.onChangeContactTypeName = this.onChangeContactTypeName.bind(this);
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.handleUpdateObject = this.handleUpdateObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
  }

  componentDidMount() {
    if (this.props.edit) {
      // console.log(this.props);
      var { cnttype_id, cnttype_name } = this.props.contactType.data;
      this.setState({
        cnttype_id,
        cnttype_name,
      });
      //   axios.get('/api/contactTypes/'+id).then(response =>{
      //     this.setState({
      //       cnttype_id: response.data.cnttype_id,
      //       cnttype_name: response.data.cnttype_name,
      //     })
      //   })
      // }
      // axios.get('api/contactTypes/').then(response => {
      //   this.setState({
      //     contactType: response.data.data
      //   })
      // })
    }
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  handleOpen() {
    this.setState({
      setOpen: true,
    });
    console.log(this.state.setOpen + "esto deberia abrir el modal");
  }

  handleClose() {
    this.setState({
      open: false,
    });
    this.props.onHandleSubmit();
  }

  onChangeContactTypeId(e) {
    this.setState({
      cnttype_id: e.target.value,
    });
  }
  onChangeContactTypeName(e) {
    this.setState({
      cnttype_name: e.target.value,
    });
    this.props.changeAtribute(true);
  }

  handleCreateObject(e) {
    e.preventDefault();
    var { cnttype_name } = this.state;
    var validator = { cnttype_name: { error: false, message: "" } };
    var object_error = {};
    const object = {
      cnttype_id: this.state.cnttype_id,
      cnttype_name: this.state.cnttype_name,
    };
    const { history } = this.props;
    axios
      .post("/api/contactTypes", { cnttype_name: object.cnttype_name })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        // console.log('se guardo con exito');
        // console.log(response.data);
        this.handleClose();
        // this.props.history.push('/contactTypes');
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var { cnttype_name } = this.state;
    var validator = { cnttype_name: { error: false, message: "" } };
    var object_error = {};
    const object = {
      cnttype_id: this.state.cnttype_id,
      cnttype_name: this.state.cnttype_name,
    };

    const id = object.cnttype_id;
    axios
      .put(`/api/contactTypes/${id}`, {
        cnttype_id: object.cnttype_id,
        cnttype_name: object.cnttype_name,
      })
      .then((res) => {
        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        console.log(res.data);
        this.handleClose();
        console.log("contactTypes Type successfully updated");
        this.props.history.push("/contactTypes");
      })
      .catch((error) => {
        this.setState({ snack_open: true });
        // console.log(Object.keys(error.response.data.errors));
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  botonEdit() {
    if (this.props.edit) {
      return (
        <Button
          name="save"
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleUpdateObject}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          name="save"
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
      );
    }
  }

  render() {
    const { classes } = this.props;
    const { validator, snack_open } = this.state;
    var showSnack;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["cnttype_name"]["error"] ? (
              <li>Error: {validator["cnttype_name"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3>Formulario de Tipo de contactos</h3>
        <hr />
        <form encType="multipart/form-data">
          <div className="form-group">
            <TextField
              name="cnttype_name"
              id="outlined-full-width"
              label="Nombre"
              error={this.state.validator.cnttype_name.error}
              helperText={this.state.validator.cnttype_name.message}
              style={{ margin: 8 }}
              placeholder="Ejemplo: Telefono"
              value={this.state.cnttype_name}
              onChange={this.onChangeContactTypeName}
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
            />
          </div>

          {this.botonEdit()}
          {showSnack}
          <Button
            variant="outlined"
            color="secondary"
            startIcon={<ExitToAppIcon />}
            onClick={this.handleClose}
          >
            Cerrar
          </Button>
        </form>
      </div>
    );
  }
}

export default ModalForm;
