import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {AppBar, Tabs, Tab, Typography} from '@material-ui/core';
import { Card } from 'react-bootstrap';
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    height: 140,
  },
});

class ContactTypeShow extends Component{
  constructor(props){
    super(props)
    this.state = {
      contactTypes:{},
      value: 0
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount () {
      const id = this.props.match.params.id
      axios.get(`/api/contactTypes/${id}`).then(response => {
        this.setState({
          contactTypes: response.data
        })
      })
    }

    handleChange (event, value) {
        this.setState({ value });
      };


render(){

  const { classes } = this.props;
  const {contactTypes, value} = this.state;
  return(

    <div className={classes.root}>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab label="Id" />
                <Tab label="Nombre" />

              </Tabs>
            </AppBar>
            {value === 0 && <TabContainer>
              <div >
                            <Card style={{ width: '25rem' }}>
                              <Card.Body>
                                <Card.Title>Id:</Card.Title>
                                <Card.Text>
                                  *{contactTypes.cnttype_id}
                                </Card.Text>
                                <Card.Title>Nombre:</Card.Title>
                                <Card.Text>
                                  *{contactTypes.cnttype_name}
                                </Card.Text>

                            </Card.Body>
                          </Card>
              </div>
              </TabContainer>}
            {value === 1 && <TabContainer>
              <List component="nav" className={classes.root} aria-label="mailbox folders">
              {this.contactTypes()}
              </List>
              <Divider/>


              </TabContainer>}
            {value === 2 && <TabContainer>Item Three</TabContainer>}

          </div>

  )
}


}

ContactTypeShow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContactTypeShow);
