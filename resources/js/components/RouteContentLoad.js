import React,{Component} from 'react';
import {CircularProgress} from '@material-ui/core';

export default class RouteContentLoad extends Component{
  render(){
    return(
      <div style={{display: 'flex',alignItems:'center',justifyContent:'center', flexDirection: 'column'}}>
        <CircularProgress />
      </div>
    );
  }
}
