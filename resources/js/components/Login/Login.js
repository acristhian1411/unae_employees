import React, {Component} from 'react';

import {Button, TextField, InputAdornment, IconButton} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {getAuth} from '../../contexts';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      google_url: null,
      email: '',
      password: '',
      show_password: false,
      validator:{email:{message:'',error: false},
                 password:{message:'',error: false}}
    }
    this.onChangeFields = this.onChangeFields.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.redirectGoogle = this.redirectGoogle.bind(this);
  }
  handleClickShowPassword() {
    this.setState({ show_password: !this.state.show_password });
  };

  handleMouseDownPassword(event){
    event.preventDefault();
    this.setState({ show_password: !this.state.show_password });
  };
  onChangeFields(e){
    this.setState({[e.target.name]: e.target.value});
  }
  componentDidMount() {
    // fetch('/api/auth/google/url', { headers: new Headers({ accept: 'application/json' }) })
    //     .then((response) => {
    //         if (response.ok) {
    //             return response.json();
    //         }
    //         throw new Error('Something went wrong!');
    //     })
    //     .then((url) => this.setState({ google_url: url }))
    //     .catch((error) => console.error(error));
    }
    redirectGoogle(){
      window.location.replace(this.state.google_url);
    }
  handleSubmit(e){
    // const {email, password} = this.state;
    // e.preventDefault();
    // // console.log('email '+email+' password: '+password );
    // axios.get('/sanctum/csrf-cookie').then(response => {
    //   axios.post('/login', {email, password}).then(res=>{
    //     console.log(res);
    //     this.props.onHandleSubmit();
    //   }).catch(error=>{
    //     console.log(error);
    //   });
    // });

  }

  render(){
    var {email, password} = this.state;
    return(
      <getAuth.Consumer>
        {(context)=>{
          return(
            <div className="back-style">
              <div className="image-login-container">
                <img className="image-login" src={`img/Maius.png`} />
                <form onSubmit={(e)=>context.handleSubmit(e, {email, password})}>
                  <TextField
                   id="outlined-full-width"
                   label="Correo"
                   error={this.state.validator.email.error}
                   helperText={this.state.validator.email.message}
                   // style={{ margin: 8 }}
                   name = "email"
                   value={this.state.email}
                   onChange={this.onChangeFields}
                   fullWidth
                   margin="normal"
                   variant="outlined"
                   color="secondary"
                   />
                  <TextField
                    id="outlined-full-width"
                    label="Contraseña"
                    color="secondary"
                    error={this.state.validator.password.error}
                    helperText={this.state.validator.password.message}
                    // style={{ margin: 8 }}
                    type={this.state.show_password ? 'text' : 'password'}
                    name = "password"
                    value={this.state.password}
                    onChange={this.onChangeFields}
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    InputProps={{
                      endAdornment:
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={()=>this.handleClickShowPassword()}
                            // onMouseDown={(e)=>this.handleMouseDownPassword(e)}
                            // onMouseUp={(e)=>this.handleMouseDownPassword(e)}
                          >
                            {this.state.show_password ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>

                    }}

                  />
                  <Button type="submit" variant="outlined" color="primary">Iniciar Sesión</Button>
                  {/*<Button variant="outlined" color="primary" href={"/redirect/google"}>Iniciar sesion con google</Button>*/}
                </form>
              </div>

            </div>

          );
        }}
      </getAuth.Consumer>
    );
  }
}
