import React, {Component} from 'react';

import {Button, TextField, InputAdornment, IconButton} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {getAuth} from '../../contexts';

export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      password_verified: '',
      show_password1: false,
      show_password2: false,
      validator:{email:{message:'',error: false},
                 password:{message:'',error: false}}
    }
    this.onChangeFields = this.onChangeFields.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleClickShowPassword(name) {
    this.setState({ [name]: !this.state[name] });
  };

  handleMouseDownPassword(event){
    event.preventDefault();
  };
  onChangeFields(e){
    this.setState({[e.target.name]: e.target.value});
  }
  componentDidMount() {
    // fetch('/api/auth/google/url', { headers: new Headers({ accept: 'application/json' }) })
    //     .then((response) => {
    //         if (response.ok) {
    //             return response.json();
    //         }
    //         throw new Error('Something went wrong!');
    //     })
    //     .then((url) => this.setState({ google_url: url }))
    //     .catch((error) => console.error(error));
    }
  handleSubmit(e){
    // const {email, password} = this.state;
    // e.preventDefault();
    // // console.log('email '+email+' password: '+password );
    // axios.get('/sanctum/csrf-cookie').then(response => {
    //   axios.post('/login', {email, password}).then(res=>{
    //     console.log(res);
    //     this.props.onHandleSubmit();
    //   }).catch(error=>{
    //     console.log(error);
    //   });
    // });

  }

  render(){
    var {password, password_verified} = this.state;
    return(
      <getAuth.Consumer>
        {(context)=>{
          return(
            <div className="back-style">
              <div className="image-login-container">
                <img className="image-login" src={`img/complejo_educativo.png`} />
                <form onSubmit={(e)=>context.resetPassword(e, {password, password_verified})}>
                  <TextField
                    id="outlined-full-width"
                    label="Nueva contraseña"
                    color="secondary"
                    error={this.state.validator.password.error}
                    helperText={this.state.validator.password.message}
                    style={{ margin: 8 }}
                    type={this.state.show_password1 ? 'text' : 'password'}
                    name = "password"
                    value={this.state.password}
                    onChange={this.onChangeFields}
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    InputProps={{
                      endAdornment:
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={()=>this.handleClickShowPassword('show_password1')}
                          >
                            {this.state.show_password1 ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>

                    }}
                  />
                  <TextField
                   id="outlined-full-width"
                   label="Confirmar contraseña"
                   error={this.state.validator.password.error}
                   helperText={this.state.validator.password.message}
                   style={{ margin: 8 }}
                   type={this.state.show_password2 ? 'text' : 'password'}
                   name = "password_verified"
                   value={this.state.password_verified}
                   onChange={this.onChangeFields}
                   fullWidth
                   margin="normal"
                   variant="outlined"
                   color="secondary"
                   InputProps={{
                     endAdornment:
                       <InputAdornment position="end">
                         <IconButton
                           aria-label="toggle password visibility"
                           onClick={()=>this.handleClickShowPassword('show_password2')}
                         >
                           {this.state.show_password2 ? <Visibility /> : <VisibilityOff />}
                         </IconButton>
                       </InputAdornment>

                   }}
                   />
                  <Button type="submit" variant="outlined" color="primary">Cambiar contraseña</Button>
                  {/*<Button variant="outlined" color="primary" href={"/redirect/google"}>Iniciar sesion con google</Button>*/}
                </form>
              </div>

            </div>

          );
        }}
      </getAuth.Consumer>
    );
  }
}
