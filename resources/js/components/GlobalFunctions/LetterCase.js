function Capitalize(param){
  if (param !== null && param !== undefined) {
    let lower = param.toLowerCase();
    let response = lower.replace(/\w\S*/g, (w) =>
      w.replace(/^\w/, (c) => c.toUpperCase())
    );
    return response;
  }else {
    return param;
  }

}
function CapitalizeFirstLetter(param){
  if (param !== null) {
    let lower = param.toLowerCase();
    return lower.charAt(0).toUpperCase() + lower.slice(1);
  }else {
    return param;
  }

}
function CapitalizeWithRomanNumber(param){
  let lower = param.toLowerCase();
  let roman_number = '';
  let convert_case = '';
  if (lower.indexOf(' ii') !== -1) {
    if(lower.indexOf(' iii')!== -1){
      roman_number = 'iii';
    }
    else {
      roman_number = 'ii';
    }
  }
  else if (lower.indexOf(' iv') !== -1 && lower.length -3 === lower.indexOf(' iv')) {
    roman_number = 'iv';
  }
  else if (lower.indexOf(' v') !== -1 && lower.length -2 === lower.indexOf(' v')) {
    roman_number = 'v';
  }
  else if (lower.indexOf(' vi') !== -1 && lower.length -3 === lower.indexOf(' vi')){
    if (lower.indexOf(' vii') !== -1) {
      if (lower.indexOf(' viii') !== -1) {
        roman_number = 'viii';
      }
      else {
        roman_number = 'vii';
      }
    }
    else {
      roman_number = 'vi';
    }
  }
  else {
    roman_number = '';
  }
  if(roman_number !== ''){
    convert_case = lower.substring(lower.indexOf(' '+roman_number), 0);
    let response = convert_case.replace(/\w\S*/g, (w) =>
      w.replace(/^\w/, (c) => c.toUpperCase())
    );
    return response+' '+roman_number.toUpperCase();
  }
  else{
    let response = lower.replace(/\w\S*/g, (w) =>
      w.replace(/^\w/, (c) => c.toUpperCase())
    );
    return response;
  }
}
export {Capitalize, CapitalizeWithRomanNumber, CapitalizeFirstLetter};
