function format(input) {
  // var num = input.replace(/\./g,'');
  var num = input;
  if (!isNaN(num) && num != null) {
    num = num
      .toString()
      .split("")
      .reverse()
      .join("")
      .replace(/(?=\d*\.?)(\d{3})/g, "$1.");
    num = num.split("").reverse().join("").replace(/^[\.]/, "");
    return num;
  } else {
    // alert("Solo se permiten numeros");
    // console.log("ingrese un valor numerico");
     // return num.replace(/[^\d\.]*/g, "");
  }
  return input;
}
function disformat(input) {
  let text = input;
  while (text.toString().indexOf(".") != -1)
    text = text.toString().replace(".", "");
  return text;
}

export {format, disformat};
