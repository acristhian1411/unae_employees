function getProcess(tp, pl, ne){
    var dif = 0;
    var intervalo = 0;
    var residuo = 0;
    var mincal2 = 0;
    var maxcal2 = 0;
    var mincal3 = 0;
    var maxcal3 = 0;
    var mincal4 = 0;
    var maxcal4 = 0;
    var mincal5 = 0;
    var maxcal5 = 0;
    mincal2 = Math.round((parseFloat(tp)*parseFloat(ne))/parseFloat(100));
    dif = tp - mincal2 +1;
    intervalo = parseInt(dif/4);
    residuo = dif%4;
    if(residuo === 3){
      maxcal2 = mincal2 + intervalo;
      mincal3 = maxcal2 + 1;
      maxcal3 = mincal3 + intervalo;
      mincal4 = maxcal3 + 1;
      maxcal4 = mincal4 + intervalo;
      mincal5 = maxcal4 + 1;
      maxcal5 = mincal5 + intervalo - 1;
    }else if (residuo === 2){
      maxcal2 = mincal2 + intervalo - 1;
      mincal3 = maxcal2 + 1;
      maxcal3 = mincal3 + intervalo;
      mincal4 = maxcal3 + 1;
      maxcal4 = mincal4 + intervalo;
      mincal5 = maxcal4 + 1;
      maxcal5 = mincal5 + intervalo - 1;
    }else if (residuo === 1){
      maxcal2 = mincal2 + intervalo - 1;
      mincal3 = maxcal2 + 1;
      maxcal3 = mincal3 + intervalo;
      mincal4 = maxcal3 + 1;
      maxcal4 = mincal4 + intervalo - 1;
      mincal5 = maxcal4 + 1;
      maxcal5 = mincal5 + intervalo - 1;
    }else{
      maxcal2 = mincal2 + intervalo - 1;
      mincal3 = maxcal2 + 1;
      maxcal3 = mincal3 + intervalo - 1;
      mincal4 = maxcal3 + 1;
      maxcal4 = mincal4 + intervalo - 1;
      mincal5 = maxcal4 + 1;
      maxcal5 = mincal5 + intervalo - 1;
    }
    if(pl >= mincal2 && pl <= maxcal2){
    	return 2;
    }else if(pl >= mincal3 && pl <= maxcal3){
    	return 3;
    }else if(pl >= mincal4 && pl <= maxcal4){
    	return 4;
    }else if(pl >= mincal5 && pl <= maxcal5){
    	return 5;
    }else{
    	return 1;
    }
}

export {getProcess};
