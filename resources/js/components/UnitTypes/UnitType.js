import React, { Component, lazy } from "react";
import { Link } from "react-router-dom";
import ModalUnitType from "./ModalUnitType";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
// const ModalUnitType = lazy(()=> import('./ModalUnitType'));
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

class UnitType extends Component {
  constructor() {
    super();
    this.state = {
      snack_open: false,
      message_success: "",
      unit_types: [],
      unit_type_id: 0,
      unit_type: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredunit_types: [],
      prevActiveStep: 1,
      paginator: [],
      rowsPerPage: 10,
      order: "asc",
      orderBy: "unit_type_description",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/unit_types",
      busqueda: false,
      permissions: {
        unit_types_index: null,
        unit_types_show: null,
        unit_types_store: null,
        unit_types_update: null,
        unit_types_destroy: null,
      },
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          unit_types: data,
          paginator: data,
          filteredunit_types: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    this.handleClickOpen = (data) => {
      this.setState({ open: true, unit_type: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    // pagina siguiente
    this.handleNext = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep + 1 });
      var newPage = this.state.prevActiveStep + 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //pagina anterior
    this.handleBack = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep - 1 });
      var newPage = this.state.prevActiveStep - 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //filas por pagina
    this.handleChangeSelect = (event) => {
      this.setState({ rowsPerPage: event.target.value, prevActiveStep: 1 });
      this.getObject(event.target.value, 1);
    };
  }

  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("TIPOS DE INSTITUCIÓN");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "unit_types_index",
      "unit_types_show",
      "unit_types_store",
      "unit_types_update",
      "unit_types_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredunit_types: this.state.unit_types.data,
            paginator: this.state.unit_types,
            page_lenght: this.state.unit_types.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/unit_types`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/unit_types?unit_type_description=${e.target.value}`,
          });
          axios
            .get(
              `${this.state.url}&sort_by=${this.state.orderBy}&order=${
                this.state.order
              }&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filteredunit_types: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  deleteObject() {
    axios.delete(`api/unit_types/${this.state.unit_type_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      unit_type: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.prevActiveStep,
      name,
      order
    );
  }
  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      unit_type,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.new) {
      // showModal = <ModalUnitType edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalUnitType />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      // showModal = <ModalUnitType edit={true} onHandleSubmit={this.updateState} pais={this.state.unit_type}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalUnitType />,
            props_form: {
              edit: true,
              pais: this.state.unit_type,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={unit_type.data.unit_type_description}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (this.state.filteredunit_types.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body" style={{textAlign: "center"}}>
          <TextField
            id="outlined-search"
            label="Escriba para buscar"
            type="search"
            name="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={
                    orderBy === "unit_type_description" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "unit_type_description"}
                    direction={
                      orderBy === "unit_type_description" ? order : "asc"
                    }
                    onClick={() => {
                      this.createSortHandler("unit_type_description");
                    }}
                  >
                    <h3>Nombre</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell colSpan="3">
                  {permissions.unit_types_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        name="new_item"
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>

            {this.renderList()}
          </Table>
        </TableContainer>

        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredunit_types.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredunit_types.map((data) => {
        return (
          <TableBody key={data.unit_type_id}>
            <TableRow>
              <TableCell>{data.unit_type_description}</TableCell>

              <TableCell>
                {permissions.unit_types_update === true && (
                  <Tooltip title="Editar">
                    <Button
                      name="edit_item"
                      variant="outlined"
                      color="primary"
                      startIcon={<EditIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>

              <TableCell>
                {permissions.unit_types_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      name="delete_item"
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ unit_type_id: data.unit_type_id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
export default UnitType;
