import React, { Component, lazy } from "react";
import { Link } from "react-router-dom";
// import FormSchool from "./FormSchool";
import MidModal from "../Modals/MidModal";
import DialogDestroy from "../Dialogs/DialogDestroy";
import Paginator from "../Paginators/Paginator";
// const FormSchool = lazy(()=> import('./FormSchool'));
// const DialogDestroy = lazy(()=> import('../Dialogs/DialogDestroy'));
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

export default class Staff extends Component {
  constructor(props) {
    super(props);

    this.state = {
      snack_open: false,
      message_success: "",
      staff: [],
      filterstaff: [],
      staff_id: 0,
      school: [],
      edit: false,
      _new: false,
      open: false,
      paginator: [],
      rowsPerPage: 10,
      order: "asc",
      orderBy: "employee_name",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: `/api/facultyStaff/${this.props.faculty}`,
      // url: `/api/facultyStaff/1`,
      busqueda: false,
      permissions: {
        schols_index: null,
        schols_show: null,
        schols_store: null,
        schols_update: null,
        schols_destroy: null,
      },
    };
    // this.handleClickOpen = (data) =>{
    //   this.setState({open: true, school: data});
    // }
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let { data } = await res;
      // console.log(res);
      if (this._ismounted) {
        this.setState({
          staff: data,
          paginator: res.data,
          filterstaff: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
          time_out: false,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        _new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    this.destroy = this.destroy.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    this.getObject(this.state.rowsPerPage, this.state.page);
    // var permissions = [
    //   "schols_index",
    //   "schols_show",
    //   "schols_store",
    //   "schols_update",
    //   "schols_destroy",
    // ];
    // axios
    //   .get(`/api/roles_has_permission`, {
    //     params: {
    //       permission: permissions,
    //     },
    //   })
    //   .then((res) => {
    //     // console.log('algo');
    //     this.setState({ permissions: res.data });
    //     if (res.data !== false) {
    //       this.getObject(this.state.rowsPerPage, this.state.page);
    //     }
    //   });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  handleModal() {
    this.setState({ _new: true });
  }
  handleClickOpen(data) {
    this.setState({ open: true, school: data });
  }
  handleChangePage(event, newPage) {
    this.setState({ page: newPage });
  }
  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  editModal(data) {
    this.setState({ edit: true, school: data });
  }
  destroy() {
    // event.preventDefault();
    axios.delete(`/api/staff/${this.state.staff_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredCities: this.state.staff.data.data,
            paginator: this.state.staff,
            page_lenght: this.state.staff.data.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/staff`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/staff/search/${e.target.value}`,
          });
          axios
            .get(
              `${this.state.url}&sort_by=${this.state.orderBy}&order=${
                this.state.order
              }&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filterstaff: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  render() {
    const {
      snack_open,
      message_success,
      open,
      _new,
      edit,
      school,
      filterstaff,
      rowsPerPage,
      orderBy,
      order,
      permissions,
    } = this.state;
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    // if (_new) {
    //   // showModal = <FormSchool onHandleSubmit={this.updateState}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <FormSchool />,
    //         props_form: { onSuccess: this.openSnack },
    //       }}
    //     </MidModal>
    //   );
    // } else if (edit) {
    //   // showModal = <FormSchool edit ={true} school={school} onHandleSubmit={this.updateState}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <FormSchool />,
    //         props_form: {
    //           edit: true,
    //           school: school,
    //           onSuccess: this.openSnack,
    //         },
    //       }}
    //     </MidModal>
    //   );
    // }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={school.data.school_name}
          onClose={this.handleClose}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filterstaff.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div>
        <h2 align="center">
          Staff
          {/*  <TextField
            id="outlined-search"
            label="Buscar"
            type="search"
            variant="outlined"
            onChange={this.search}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />*/}
        </h2>
        <hr />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "employee_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "employee_name"}
                    direction={orderBy === "employee_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("employee_name");
                    }}
                  >
                    <h3>Funcionario</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={
                    orderBy === "appoin_description" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "appoin_description"}
                    direction={orderBy === "appoin_description" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("appoin_description");
                    }}
                  >
                    <h3>Cargo</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell colSpan="2">
                  {permissions.schols_store === true && (
                    <Button
                      variant="outlined"
                      color="primary"
                      type="submit"
                      onClick={() => {
                        this.handleModal();
                      }}
                    >
                      <AddBoxOutlinedIcon />
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>

        {paginator}
        {/*showModal*/}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }
  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const {
      open,
      school,
      filterstaff,
      rowsPerPage,
      page,
      permissions,
    } = this.state;
    if (filterstaff.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return filterstaff.map((data) => {
        return (
          <TableBody key={data.staff_id}>
            <TableRow>
              <TableCell>{data.employee_name}</TableCell>
              <TableCell>{data.appoin_description}</TableCell>
              {/*<TableCell>
                {permissions.schols_update === true && (
                  <Button
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.editModal({ data: data });
                    }}
                  >
                    <EditIcon />
                  </Button>
                )}
              </TableCell>
              <TableCell>
                {permissions.schols_destroy === true && (
                  <Button
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      this.handleClickOpen({ data: data }),
                        this.setState({ staff_id: data.staff_id });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                )}
              </TableCell>*/}
            </TableRow>
          </TableBody>
        );
      });
    }
  } //cierra el renderList
}
