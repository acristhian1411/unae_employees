import React, {Component} from 'react';

export default class Unauthorized extends Component{
  constructor(props){
    super(props);

  }
  render(){
    return(
      <div>No tiene permiso para esta ruta, contacte con el administrador del sistema.</div>
    );
  }
}
