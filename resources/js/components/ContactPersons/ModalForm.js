import React, {Component} from 'react';
import {Modal, Backdrop, Fade, InputLabel, TextField, Button, Snackbar} from '@material-ui/core';
import Select from 'react-select';
import SaveIcon from '@material-ui/icons/Save';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Autocomplete, Alert } from '@material-ui/lab';

 class ModalForm extends Component  {


  constructor (props) {
    super(props)
    this.state = {
      snack_open: false,
      open: true,
      setOpen: true,
      cntper_id: 0,
      cntper_description: '',
      cntper_data: '',
      cnttype_id: null,
      person_id: 0,
      cnttype_name: '',
      person_fname: '',
      edit: false,
      contactPerson:[],
      contactType:[],
      person:[],
      sucess: false,
      error: false,
      validator: {
        cntper_data:{
          message:'',
          error: false,
        },
        cnttype_id:{
          message:'',
          error: false
        }
      }
  }

  this.handleOpen = this.handleOpen.bind(this)
  this.handleClose = this.handleClose.bind(this)
  this.onChangeCntperData = this.onChangeCntperData.bind(this)
  this.onChangeContactType = this.onChangeContactType.bind(this)
  this.handleCreateObject = this.handleCreateObject.bind(this)
  this.handleUpdateObject = this.handleUpdateObject.bind(this)
  this.selectValue = this.selectValue.bind(this)
  this.closeSnack = this.closeSnack.bind(this);
}

componentDidMount () {
  if (this.props.edit) {
  var id = this.props.contactPerson.data.cntper_id
    axios.get('/api/contactPersons/'+id).then(response=>{
      this.setState({
        cntper_id: response.data.data.cntper_id,
        cntper_description: response.data.data.cntper_description,
        cntper_data: response.data.data.cntper_data,
        cnttype_id: response.data.data.cnttype_id,
        cnttype_name: response.data.data.cnttype_name,
        person_id: response.data.data.person_id,
      })
    })
  }


  axios.get('/api/contactTypes').then(response=>{
    this.setState({
      contactType: response.data.data
    })
  })

}

selectContacType(){
  return (this.state.contactType.map(data =>({ label: data.cnttype_name, value: data.cnttype_id})));
}
closeSnack(){
  this.setState({snack_open: false});
}


handleOpen () {
  this.setState({
    setOpen: true
  });
};

handleClose () {
  this.setState({
    open: false
  });
  this.props.onHandleSubmit();
}



onChangeCntperData(e){
  this.setState({
    cntper_data: e.target.value
  });
  this.props.changeAtribute(true);

};

onChangeContactType(e, values){
  this.setState({cnttype_id: values.value});
  this.setState({cnttype_name: values.label});
  this.props.changeAtribute(true);
};

selectValue(){
    return {label:this.state.cnttype_name, value:this.state.cnttype_id};
}
handleCreateObject (e) {
  e.preventDefault();
  var validator = {cntper_data: {error: false,  message: ''},
                   cnttype_id: {error: false,  message: ''}};
  var object_error = {};
  const object = {
    cnttype_id: this.state.cnttype_id,
    person_id: this.props.person,
    cntper_description: this.state.cnttype_name,
    cntper_data: this.state.cntper_data,
  }
  axios.post('/api/contactPersons', {cnttype_id:object.cnttype_id, person_id:object.person_id, cntper_description:object.cntper_description, cntper_data:object.cntper_data})
    .then(response => {
      this.props.onSuccess(response.data);
      this.props.changeAtribute(false);
      this.handleClose()
  }).catch(error => {
     //console.log(Object.keys(error.response.data.errors));
     this.setState({snack_open: true});
    var errores = Object.keys(error.response.data.errors);
    for (var i = 0; i < errores.length; i++) {
      object_error = {
        ...object_error,
        [errores[i]]: {
          error: true,
          message: error.response.data.errors[errores[i]][0]
        }
      }
    }
    this.setState({
      validator:{
        ...validator,
        ...object_error
      }
    });
  });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var validator = {cntper_data: {error: false,  message: ''},
                     cnttype_id: {error: false,  message: ''}};
    var object_error = {};
    const object = {

      cnttype_id: this.state.cnttype_id,
      person_id: this.props.person,
      cntper_description: this.state.cnttype_name,
      cntper_data: this.state.cntper_data,
    }
  axios.put(`/api/contactPersons/${this.state.cntper_id}`,{cntper_description:object.cntper_description, cntper_data:object.cntper_data, cnttype_id:object.cnttype_id, person_id:object.person_id})
  .then(res=>{
    this.props.onSuccess(res.data);
    this.handleClose()
  }).catch(error => {
     //console.log(Object.keys(error.response));
     this.setState({snack_open: true});
    var errores = Object.keys(error.response.data.errors);
    for (var i = 0; i < errores.length; i++) {
      object_error = {
        ...object_error,
        [errores[i]]: {
          error: true,
          message: error.response.data.errors[errores[i]][0]
        }
      }
    }
    this.setState({
      validator:{
        ...validator,
        ...object_error
      }
    });
  });
}

hasErrorFor (field) {
  return !!this.state.errors[field]
}

renderErrorFor (field) {
  if (this.hasErrorFor(field)) {
    return (
      <span className='invalid-feedback'>
        <strong>{this.state.errors[field][0]}</strong>
      </span>
      )
    }
  }

  botonEdit(){
    if(this.props.edit){
      return(
        <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleUpdateObject}>Actualizar</Button>);
    }
    else{
      return(
        <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleCreateObject}>Guardar</Button>);
    }
  }


  render(){
    const {classes} = this.props;
    const {validator, snack_open} = this.state;
    var showSnack;
    if (snack_open){
          showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                         <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="error">
                           {validator['cntper_data']['error']?<li>Error: {validator['cntper_data']['message']}</li>:''}
                           {validator['cnttype_id']['error']?<li>Error: {validator['cnttype_id']['message']}</li>:''}
                         </Alert>
                       </Snackbar>
        }
    return(
      <div>

          <h3>Formulario de Contactos</h3>
          <hr />
          <form encType="multipart/form-data">

        <br />
        <div className='form-group'>
        <Autocomplete
          id="combo-box-demo"
          disableClearable
          options={[this.selectValue(), ...this.selectContacType()]}
          filterSelectedOptions
          getOptionLabel={(option) => option.label}
          onChange={this.onChangeContactType.bind(this)}
          value={this.selectValue()}
          getOptionSelected={(option, value)=>option.value === value.value}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => <TextField {...params} error={this.state.validator.cnttype_id.error} helperText={this.state.validator.cnttype_id.message} label="Seleccione un tipo de contacto *" variant="outlined" fullWidth/>}
        />

      </div>

      <div className='form-group'>
      <TextField
          id="outlined-full-width"
          error={this.state.validator.cntper_data.error}
          helperText={this.state.validator.cntper_data.message}
          label="Dato *"
          style={{ margin: 8 }}
          value={this.state.cntper_data}
          onChange={this.onChangeCntperData.bind(this)}
          fullWidth
          margin="normal"
          variant="outlined"
        />
      {/*  <TextField
         id="outlined-full-width"
         label="Dato"
         name="cntper_data"
         style={{ margin: 8 }}
         value={this.state.cntper_data}
         onChange={this.onChangeCntperData}
         fullWidth
         margin="normal"
         variant="outlined"
       />*/}
      </div>
    <br />
    {this.botonEdit()}
    {showSnack}

    </form>

    </div>
    );
  }
}
export default ModalForm;
