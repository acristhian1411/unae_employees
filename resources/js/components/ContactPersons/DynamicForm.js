import React, {Component, Fragment} from 'react';
import {Modal, Backdrop, Fade, InputLabel, TextField, Button, Grid, FormControl, Typography, Fab} from '@material-ui/core';
import Select from 'react-select';
import SaveIcon from '@material-ui/icons/Save';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Autocomplete } from '@material-ui/lab';
import AddIcon from "@material-ui/icons/Add";
import InputAdornment from "@material-ui/core/InputAdornment";
import Tooltip from "@material-ui/core/Tooltip";
import RemoveIcon from '@material-ui/icons/Remove';
import InputMask from "react-input-mask";

class DynamicForm extends Component  {
  constructor (props) {
    super(props)
    this.state = {
      open: true,
      setOpen: true,
      cntper_id: 0,
      cntper_description: '',
      cntper_data: '',
      cnttype_id: null,
      person_id: 0,
      cnttype_name: '',
      person_fname: '',
      edit: false,
      contactPerson:[],
      contactType:[],
      person:[],
      directors_array: ["director-0"],
      sucess: false,
      error: false,
      inputFields:[
                  {
          cnttype_id: 0,
          cnttype_name: "",
          cntper_data: ''
                  }
                  ],
      validator: {
        cntper_data:{
          message:'',
          error: false,
        },
        cnttype_id:{
          message:'',
          error: false
        }
      }
  }

  this.handleOpen = this.handleOpen.bind(this)
  this.handleClose = this.handleClose.bind(this)
  this.onChangeCntperData = this.onChangeCntperData.bind(this)
  this.onChangeContactType = this.onChangeContactType.bind(this)
  this.handleCreateObject = this.handleCreateObject.bind(this)
  this.handleUpdateObject = this.handleUpdateObject.bind(this)
  this.selectValue = this.selectValue.bind(this)
}

appendInput_director() {
  var newInput = `director-${this.state.directors_array.length}`;
  this.setState(prevState => ({
    directors_array: prevState.directors_array.concat([newInput])
  }));
}

componentDidMount () {
  if (this.props.edit) {
  var id = this.props.contactPerson.data.cntper_id
    axios.get('/api/contactPersons/'+id).then(response=>{
      this.setState({
        cntper_id: response.data.data.cntper_id,
        cntper_description: response.data.data.cntper_description,
        cntper_data: response.data.data.cntper_data,
        cnttype_id: response.data.data.cnttype_id,
        cnttype_name: response.data.data.cnttype_name,
        person_id: response.data.data.person_id,
      })
    })
  }


  axios.get('/api/contactTypes').then(response=>{
    this.setState({
      contactType: response.data.data
    })
  })

}

selectContacType(){
  return (this.state.contactType.map(data =>({ label: data.cnttype_name, value: data.cnttype_id})));
}



handleOpen () {
  this.setState({
    setOpen: true
  });
};

handleClose () {
  this.setState({
    open: false
  });
  this.props.onHandleSubmit();
}



onChangeCntperData(e){
  this.setState({
    cntper_data: e.target.value
  });
  this.props.changeAtribute(true);

};



selectValue(){
    return {label:this.state.cnttype_name, value:this.state.cnttype_id};
}
handleCreateObject (e) {
  e.preventDefault();
  var validator = {cntper_data: {error: false,  message: ''},
                   cnttype_id: {error: false,  message: ''}};
  var object_error = {};
  const object = {
    cnttype_id: this.state.cnttype_id,
    person_id: this.props.person,
    cntper_description: this.state.cnttype_name,
    cntper_data: this.state.cntper_data,
  }

  axios.post('/api/contactPersons-per', {person_id: 42,
    contacts:this.state.inputFields})
    .then(response => {
      // this.props.onSuccess(response.data);
      // this.props.changeAtribute(false);
      this.handleClose()
  }).catch(error => {
     //console.log(Object.keys(error.response.data.errors));
    var errores = Object.keys(error.response.data.errors);
    for (var i = 0; i < errores.length; i++) {
      object_error = {
        ...object_error,
        [errores[i]]: {
          error: true,
          message: error.response.data.errors[errores[i]][0]
        }
      }
    }
    this.setState({
      validator:{
        ...validator,
        ...object_error
      }
    });
  });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var validator = {cntper_data: {error: false,  message: ''},
                     cnttype_id: {error: false,  message: ''}};
    var object_error = {};
    const object = {

      cnttype_id: this.state.cnttype_id,
      person_id: this.props.person,
      cntper_description: this.state.cnttype_name,
      cntper_data: this.state.cntper_data,
    }
  axios.put(`/api/contactPersons/${this.state.cntper_id}`,{cntper_description:object.cntper_description, cntper_data:object.cntper_data, cnttype_id:object.cnttype_id, person_id:object.person_id})
  .then(res=>{
    this.props.onSuccess(res.data);
    this.handleClose()
  }).catch(error => {
     //console.log(Object.keys(error.response));
    var errores = Object.keys(error.response.data.errors);
    for (var i = 0; i < errores.length; i++) {
      object_error = {
        ...object_error,
        [errores[i]]: {
          error: true,
          message: error.response.data.errors[errores[i]][0]
        }
      }
    }
    this.setState({
      validator:{
        ...validator,
        ...object_error
      }
    });
  });
}

hasErrorFor (field) {
  return !!this.state.errors[field]
}

 handleAddFields  ()  {
    var values = [...this.state.inputFields];
    values.push({ cnttype_id: 0, cnttype_name: '', cntper_data: '' });
    this.setState({inputFields: values});
  };

   handleRemoveFields  (index)  {
     console.log(index);
    var values = [...this.state.inputFields];
    values.splice(index,1);
    this.setState({inputFields: values});
  };

   handleInputChange (index, event)  {
    const values = [...this.state.inputFields];
    if (event.target.name === "cntper_data") {
      values[index].cntper_data = event.target.value;
    } else if (event.target.name === "cnttype_id") {
      values[index].cnttype_id = event.target.value;

    } else if (event.target.name === "cnttype_name") {
      values[index].cnttype_name = event.target.value;

    }

    this.setState({inputFields: values});
  };

  onChangeContactType(e, values, index){
    const value = [...this.state.inputFields];
    value[index].cnttype_id = values.value;
    value[index].cnttype_name = values.label;
    this.setState({inputFields: value});
  };

renderErrorFor (field) {
  if (this.hasErrorFor(field)) {
    return (
      <span className='invalid-feedback'>
        <strong>{this.state.errors[field][0]}</strong>
      </span>
      )
    }
  }

  botonEdit(){
    if(this.props.edit){
      return(
        <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleUpdateObject}>Actualizar</Button>);
    }
    else{
      return(
        <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleCreateObject}>Guardar</Button>);
    }
  }

  render(){
    const {classes} = this.props;
    var phone = '(0999)999-999'

    return(
      <div>

          <h3>Formulario de Contactos</h3>
          <hr />


        <br />

        <form >
               <div className="form-row">
                 {this.state.inputFields.map((inputField, index) => (
                   <Fragment key={`${inputField}~${index}`}>
                   <Grid xs={10} container spacing={1} item>
                     <Grid xs={4} item>

                     <Autocomplete
                       id={index}
                       name="cnttype_id"
                       disableClearable
                       options={this.selectContacType()}
                       filterSelectedOptions
                       getOptionLabel={(option) => option.label}
                       onChange={ (e, values)=> {this.onChangeContactType(e, values,index)}}
                       value={ {value: inputField.cnttype_id, label: inputField.cnttype_name}}
                       getOptionSelected={(option, value)=>option.value === value.value}
                       style={{ margin: 8 }}
                       renderInput={(params) => <TextField {...params}
                       required
                       error={this.state.validator.cnttype_id.error}
                       helperText={this.state.validator.cnttype_id.message}
                       label="Tipo de contacto "
                       variant="outlined"
                       size="small"
                       />}
                     />
                     </Grid>
                     {/*<Grid xs={6} item>
                     <InputMask
              mask={phone}
              value={this.state.phone}
              disabled={false}
              maskChar=" "
            >
              {() => <TextField
                variant="outlined"
                 />}
            </InputMask>
            </Grid>*/}
                     <Grid xs={6} item>
                     <InputMask
                      mask={phone}
                      value={inputField.cntper_data}
                      disabled={false}
                      onChange={event => this.handleInputChange(index, event)}

                      maskChar=" "
                    >
                    {() =>  <TextField
                               variant="outlined"
                               required
                               label="Email Address"
                               className="email"
                               size="small"
                               id="cntper_data"
                               name="cntper_data"
                               label="Dato "
                               style={{ margin: 8 }}
                               fullWidth
                               margin="normal"
                               variant="outlined"
                               InputProps={{
                                 endAdornment: index + 1 <=
                                   this.state.inputFields.length && (
                                   <InputAdornment position="start">
                                     <Tooltip title="Add Statement">
                                       <Fab
                                         color="primary"
                                         size="small"
                                         onClick={() => this.handleAddFields()}
                                       >
                                         <AddIcon />
                                       </Fab>
                                     </Tooltip>
                                     <Tooltip title="Remove Statement">
                                       <Fab
                                         color="secondary"
                                         size="small"
                                         onClick={() => this.handleRemoveFields(index)}
                                       >
                                         <RemoveIcon />
                                       </Fab>
                                     </Tooltip>
                                   </InputAdornment>
                                 )
                               }
                             }
                             />}
                  </InputMask>

                     </Grid>
                     </Grid>
                   </Fragment>
                 ))}
               </div>
               <div className="submit-button">

               </div>
               <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleCreateObject}>Guardar</Button>

               <br/>
               <pre>
                         {JSON.stringify(this.state.inputFields, null, 2)}
                       </pre>
             </form>



{/*
        {this.state.directors_array.map((input, index) => (
                  <Grid xs={12} container spacing={1} item>
                    <Grid xs={3} item>
                      <FormControl fullWidth margin="dense">

                      <Autocomplete
                        id="combo-box-demo"
                        disableClearable
                        options={[this.selectValue(), ...this.selectContacType()]}
                        filterSelectedOptions
                        getOptionLabel={(option) => option.label}
                        onChange={this.onChangeContactType.bind(this)}
                        value={this.selectValue()}
                        getOptionSelected={(option, value)=>option.value === value.value}
                        style={{ margin: 8 }}
                        renderInput={(params) => <TextField {...params}
                        required
                        error={this.state.validator.cnttype_id.error}
                        helperText={this.state.validator.cnttype_id.message}
                        label="Tipo de contacto "
                        variant="outlined"
                        size="small"
                        id={input + "-name"}
                        name={input + "-name"}
                        className="name"

                        />}
                      />

                      </FormControl>
                    </Grid>
                    <Grid xs={3} item>
                      <FormControl fullWidth margin="dense">
                        <TextField
                          variant="outlined"
                          required
                          type="email"
                          id={input + "-email"}
                          label="Email Address"
                          name={input + "-email"}
                          className="email"
                          size="small"
                          id="outlined-full-width"
                          error={this.state.validator.cntper_data.error}
                          helperText={this.state.validator.cntper_data.message}
                          label="Dato "
                          style={{ margin: 8 }}
                          value={this.state.cntper_data}
                          onChange={this.onChangeCntperData.bind(this)}
                          fullWidth
                          margin="normal"
                          variant="outlined"
                          InputProps={{
                            endAdornment: index + 1 ===
                              this.state.directors_array.length && (
                              <InputAdornment position="start">
                                <Tooltip title="Add Statement">
                                  <Fab
                                    color="primary"
                                    size="small"
                                    onClick={() => this.appendInput_director()}
                                  >
                                    <AddIcon />
                                  </Fab>
                                </Tooltip>
                              </InputAdornment>
                            )
                          }}
                        />
                      </FormControl>
                    </Grid>


                  </Grid>
                ))}*/}




        {/*<div className='form-group'>
        <Autocomplete
          id="combo-box-demo"
          disableClearable
          options={[this.selectValue(), ...this.selectContacType()]}
          filterSelectedOptions
          getOptionLabel={(option) => option.label}
          onChange={this.onChangeContactType.bind(this)}
          value={this.selectValue()}
          getOptionSelected={(option, value)=>option.value === value.value}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => <TextField {...params} error={this.state.validator.cnttype_id.error} helperText={this.state.validator.cnttype_id.message} label="Seleccione un tipo de contacto *" variant="outlined" fullWidth/>}
        />

      </div>

      <div className='form-group'>
      <TextField
          id="outlined-full-width"
          error={this.state.validator.cntper_data.error}
          helperText={this.state.validator.cntper_data.message}
          label="Dato *"
          style={{ margin: 8 }}
          value={this.state.cntper_data}
          onChange={this.onChangeCntperData.bind(this)}
          fullWidth
          margin="normal"
          variant="outlined"
        />

      </div>
    <br />
    {this.botonEdit()}
*/}

    </div>
    );
  }
}
export default DynamicForm;
