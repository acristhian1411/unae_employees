import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { AppBar, Tabs, Tab, Typography } from "@material-ui/core";
import { Card } from "react-bootstrap";
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    height: 140,
  },
});

class ContactPersonShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactPersons: {},
      value: 0,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    axios.get(`/api/contactPersons/${9290}`).then((response) => {
      console.log(response);
      this.setState({
        contactPersons: response.data,
      });
    });
  }

  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes } = this.props;
    const { contactPersons, value } = this.state;
    return (
      <div className={classes.root}>
        <div>
          <Card style={{ width: "25rem" }}>
            <Card.Body>
              <Card.Title>Contac Person Descripción:</Card.Title>
              <Card.Text>*{contactPersons.cntper_description}</Card.Text>
              <Card.Title>Contac Person Datos:</Card.Title>
              <Card.Text>*{contactPersons.cntper_data}</Card.Text>
              <Card.Title>Tipo de Contacto:</Card.Title>
              <Card.Text>*{contactPersons.cnttype_name}</Card.Text>
              <Card.Title>Persona:</Card.Title>
              <Card.Text>*{contactPersons.person_fname}</Card.Text>
            </Card.Body>
          </Card>
        </div>
      </div>
    );
  }
}

ContactPersonShow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContactPersonShow);
