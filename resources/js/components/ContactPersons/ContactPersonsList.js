import React, { Component } from "react";
import { Link } from "react-router-dom";
import ModalForm from "./ModalForm";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Tooltip,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  TablePagination,
  TableFooter,
  Paper,
  Select,
  MenuItem,
  InputBase,
  TextField,
  Snackbar,
  IconButton
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import { Alert } from "@material-ui/lab";

class ContactPersonsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      contactPersons: [],
      cntper_id: 0,
      contactTypes: [],
      persons: [],
      contactPerson: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredContactPersons: [],
      prevActiveStep: 1,
      page: 1,
      rowsPerPage: 10,
      order: "asc",
      orderBy: "cntper_description",
      selected: [],
      page_lenght: 0,
      time_out: false,
      permissions: {
        contactPersons_index: null,
        contactPersons_show: null,
        contactPersons_store: null,
        contactPersons_update: null,
        contactPersons_destroy: null,
      },
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res = await axios.get(
        `/api/contactPersons-per/${this.props.cperson}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
      );
      let { data } = await res;
      if (this._ismounted) {
        if(res.data != false){
        this.setState({
          contactPersons: data,
          paginator: res.data,
          filteredContactPersons: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };

    //funciones
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.search = this.search.bind(this);

    this.handleClose = () => {
      this.setState({ open: false });
    };
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    var permissions = [
      "contactPersons_index",
      "contactPersons_show",
      "contactPersons_store",
      "contactPersons_update",
      "contactPersons_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }

  handleChangePage(event, newPage) {
    this.setState({ page: newPage });
  }
  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    this.setState({ snack_open: true, message_success: param });
  }
  perPageChange(value) {
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  search(event) {
    var e = event;
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredContactPersons: this.state.contactPersons,
            paginator: this.state.contactPersons,
            page_lenght: this.state.contactPersons.last_page,
            page: 1,
            time_out: false,
          });
        } else {
          axios
            .get(
              `/api/contactPersons/search/${e.target.value}?sort_by=${
                this.state.orderBy
              }&order=${this.state.order}&per_page=${
                this.state.rowsPerPage
              }&page=${1}`
            )
            .then((res) => {
              this.setState({
                filteredContactPersons: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }
  handleClickOpen(data) {
    this.setState({ open: true, contactPerson: data });
  }

  deleteObject() {
    axios.delete(`/api/contactPersons/${this.state.contactPerson.data.cntper_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      contactPerson: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }

  render() {
    const {
      contactPerson,
      snack_open,
      message_success,
      permissions,
    } = this.state;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    if (this.state.new) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: {
              person: this.props.cperson,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: {
              edit: true,
              person: this.props.cperson,
              contactPerson: this.state.contactPerson,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (this.state.open) {
      showDialogDestroy = (
        <DialogDestroy
          index={contactPerson.data.cntper_description}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (this.state.snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div className="card-body">
        <p className="value_column" align="center">
          Contactos
          {/* <TextField id="outlined-search" label="Buscar" type="search" variant="outlined" onChange={this.search.bind(this)} /> */}
          {permissions.contactPersons_store === true && (
          <span>
            <Tooltip title="Agregar">
              <IconButton aria-label="close" onClick={this.clickAgregar}>
                <AddIcon style={{border: '1px solid gray', borderRadius:'50%'}} fontSize="small" />
              </IconButton>
            </Tooltip>
          </span>
          )}
        </p>
        <ul>
          {this.renderList()}
        </ul>
        <hr />



        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredContactPersons.length === 0) {
      return (
          <li className="title_column">No contiene datos</li>
      );
    } else {
      return this.state.filteredContactPersons.map((data) => {
        return (
          <li className="title_column" key={data.cntper_id}>
            {data.cnttype_name}: {data.cntper_data}
            {permissions.contactPersons_update === true && (
            <span>
              <Tooltip title="Editar">
                <IconButton aria-label="close" onClick={()=>{this.clickEditar({data: data})}}>
                  <EditIcon style={{border: '1px solid gray', borderRadius:'50%'}} fontSize="small" />
                </IconButton>
              </Tooltip>
            </span>
            )}
            {permissions.contactPersons_destroy === true && (
            <span>
              <Tooltip title="Quitar">
                <IconButton aria-label="close" onClick={()=>{this.handleClickOpen({data: data})}}>
                  <DeleteIcon style={{border: '1px solid gray', borderRadius:'50%', color: 'red'}} fontSize="small" />
                </IconButton>
              </Tooltip>
            </span>
          )}
          </li>
        );
      });
    }
  }
}

export default ContactPersonsList;
