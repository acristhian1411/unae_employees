import React, { Component, lazy } from "react";
// const FormUnit = lazy(()=> import('./FormUnit'));
// const DialogDestroy = lazy(()=> import('../Dialogs/DialogDestroy'));
import DialogDestroy from "../Dialogs/DialogDestroy";
import MidModal from "../Modals/MidModal";
import FormUnit from "./FormUnit";
import Paginator from "../Paginators/Paginator";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  InputLabel,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { getAuth } from "../../contexts";

export default class Units extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      units: [],
      filteredUnits: [],
      unit_id: 0,
      unit: [],
      edit: false,
      _new: false,
      open: false,
      paginator: [],
      rowsPerPage: 10,
      order: "asc",
      orderBy: "unit_code",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/units",
      busqueda: false,
      permissions: {
        users_index: null,
        users_show: null,
        users_store: null,
        users_update: null,
        users_destroy: null,
      },
    };
    // this.handleClickOpen = (data) =>{
    //   this.setState({open: true, unit: data});
    // }
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let { data } = await res;
      if (this._ismounted) {
        this.setState({
          units: data,
          paginator: res.data,
          filteredUnits: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        _new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };

    this.destroy = this.destroy.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);
    this.managePermission = this.managePermission.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    // var permissions = [{'users_permission'}, {'users_destroy'}];
    this.props.changeTitlePage("INSTITUCIONES");
    var permissions = [
      "units_index",
      "units_show",
      "units_store",
      "units_update",
      "units_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  handleModal() {
    this.setState({ _new: true });
  }
  handleClickOpen(data) {
    this.setState({ open: true, unit: data });
  }
  handleChangePage(event, newPage) {
    this.setState({ page: newPage });
  }
  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  editModal(data) {
    this.setState({ edit: true, unit: data });
  }
  destroy() {
    axios.delete(`/api/units/${this.state.unit_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      axios
        .get("/api/units")
        .then((res) => {
          this.setState({ units: res.data.data, open: false });
        })
        .catch((error) => {
          alert(error);
        });
      this.updateState();
    });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredUnits: this.state.units.data,
            paginator: this.state.units,
            page_lenght: this.state.units.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/units`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/units?unit_name=${e.target.value}&unit_code=${e.target.value}`,
          });
          axios
            .get(`${this.state.url}`)
            .then((res) => {
              this.setState({
                filteredUnits: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  managePermission(value, permission) {
    this.setState({ [permission]: value });
    // console.log(permission);
  }

  render() {
    const {
      snack_open,
      message_success,
      open,
      _new,
      edit,
      unit,
      filteredUnits,
      rowsPerPage,
      orderBy,
      order,
      permissions,
    } = this.state;
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    if (_new) {
      // showModal = <FormUnit onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormUnit />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (edit) {
      // showModal = <FormUnit edit ={true} unit={unit} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormUnit />,
            props_form: { edit: true, unit: unit, onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={unit.data.unit_name}
          onClose={this.handleClose}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filteredUnits.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <getAuth.Consumer>
        {(context) => {
          // context.hasPermission('users_index', this.managePermission);
          // console.log(permissions.units_index);
          return (
            <div style={{textAlign: "center"}}>
                <TextField
                  id="outlined-search"
                  label="Escriba para buscar"
                  type="search"
                  variant="outlined"
                  // onChange={this.search}
                  onKeyDown={(e) =>{
                    if(e.key === 'Enter'){
                      this.search(e)
                    }
                  }}
                  style={{ width: "40%"}}
                  InputProps={{
                    endAdornment: (
                      <React.Fragment>
                        {this.state.time_out === true ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                      </React.Fragment>
                    ),
                  }}
                />
              <TableContainer component={Paper}>
                <Table aria-label="simple table" option={{ search: true }}>
                  <TableHead>
                    <TableRow>
                      <TableCell
                        sortDirection={orderBy === "unit_code" ? order : false}
                      >
                        <TableSortLabel
                          active={orderBy === "unit_code"}
                          direction={orderBy === "unit_code" ? order : "asc"}
                          onClick={() => {
                            this.createSortHandler("unit_code");
                          }}
                        >
                          <h3>Codigo</h3>
                        </TableSortLabel>
                      </TableCell>
                      <TableCell
                        sortDirection={orderBy === "unit_name" ? order : false}
                      >
                        <TableSortLabel
                          active={orderBy === "unit_name"}
                          direction={orderBy === "unit_name" ? order : "asc"}
                          onClick={() => {
                            this.createSortHandler("unit_name");
                          }}
                        >
                          <h3>Nombre</h3>
                        </TableSortLabel>
                      </TableCell>
                      <TableCell>
                        <h3>Logo</h3>
                      </TableCell>
                      <TableCell colSpan="2">
                        {permissions.units_store === true && (
                          <Button
                            name="new_unit"
                            variant="outlined"
                            color="primary"
                            type="submit"
                            onClick={() => {
                              this.handleModal();
                            }}
                          >
                            <AddBoxOutlinedIcon />
                          </Button>
                        )}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  {this.renderList()}
                </Table>
              </TableContainer>
              {/* <Paginator
        lenght={this.state.page_lenght}
        perPageChange= {this.perPageChange}
        pageChange ={this.pageChange}
        page= {this.state.page}
        /> */}
              {paginator}
              {showModal}
              {showDialogDestroy}
              {showSnack}
            </div>
          );
        }}
      </getAuth.Consumer>
    );
  }
  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { open, filteredUnits, rowsPerPage, page, permissions } = this.state;
    if (filteredUnits.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return filteredUnits.map((data) => {
        return (
          <TableBody key={data.unit_id}>
            <TableRow>
              <TableCell>{data.unit_code}</TableCell>
              <TableCell>{Capitalize(data.unit_name)}</TableCell>
              <TableCell>
                <img className="image-table" src={`img/${data.unit_logo}`} />
              </TableCell>
              <TableCell>
                {permissions.units_update === true && (
                  <Button
                    name="edit_unit"
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.editModal({ data: data });
                    }}
                  >
                    <EditIcon />
                  </Button>
                )}
              </TableCell>
              <TableCell>
                {permissions.units_destroy === true && (
                  <Button
                    name="delete_unit"
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      this.handleClickOpen({ data: data }),
                        this.setState({ unit_id: data.unit_id });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  } //cierra el renderList
}
