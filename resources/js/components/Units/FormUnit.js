import React, { Component } from "react";
import {
  Button,
  TextField,
  Snackbar,
  Checkbox,
  Grid,
  FormControlLabel,
  CircularProgress,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { DropzoneArea } from "material-ui-dropzone";
import { Alert } from "@material-ui/lab";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";

class FormUnit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time_out: false,
      snack_open: false,
      open: true,
      openDialog: false,
      unit_name: "",
      unit_code: "",
      unit_logo: "",
      unit_company: false,
      comp_det_id: 0,
      comp_det_bussiness_name: "NO DEFINIDO",
      comp_det_bussiness_name2: "NO DEFINIDO",
      comp_det_ruc: null,
      unit_type_id: null,
      unit_type_description: "",
      unit_types: [],
      sucess: false,
      errors: [],
      companyDetails: [],
      companys: [],
      imagePreviewUrl: false,
      validator: {
        unit_name: {
          message: "",
          error: false,
        },
        unit_code: {
          message: "",
          error: false,
        },
        unit_type_id: {
          message: "",
          error: false,
        },
      },
    };
    this.mounted = false;
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.fieldsChange = this.fieldsChange.bind(this);
    this.fieldChange2 = this.fieldChange2.bind(this);
    this.fieldChange3 = this.fieldChange3.bind(this);
    this.selectValue = this.selectValue.bind(this);
    this.imageChange = this.imageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCreateCompanyDetails = this.handleCreateCompanyDetails.bind(
      this
    );
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  handleOpen() {
    this.setState({ open: true });
  }
  handleClose() {
    this.setState({ open: false });
    this.props.onHandleSubmit();
  }
  handleCloseDialog() {
    this.setState({
      openDialog: false,
      comp_det_bussiness_name: "",
      comp_det_bussiness_name2: "",
      comp_det_id: 0,
    });
  }
  handleChange(event) {
    this.setState({ unit_company: event.target.checked });
  }

  fieldsChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    this.props.changeAtribute(true);
  }
  fieldChange2(e, values) {
    this.setState({
      unit_type_id: values.value,
      unit_type_description: values.label,
    });
    this.props.changeAtribute(true);
  }
  fieldChange3(e, values) {
    this.setState({
      comp_det_id: values.value,
      comp_det_bussiness_name: values.label,
    });
    this.props.changeAtribute(true);
  }
  searchFieldChange(event) {
    var e = event;
    this.setState({
      unit_type_description: e.target.value,
      unit_type_id: null,
    });
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          axios.get("api/unit_types").then((res) => {
            this.setState({ unit_types: res.data.data, time_out: false });
          });
        } else {
          axios
            .get(`api/unit_types/search/${e.target.value}`)
            .then((res) => {
              this.setState({
                unit_types: res.data.data,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }
  imageChange(e) {
    this.props.changeAtribute(true);
    if (e[0]) {
      this.createImage(e[0]);
      // this.setState({
      //   unit_logo: e[0]
      // });
    } else {
      this.setState({
        unit_logo: "default.png",
      });
    }
  }
  createImage(file) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        unit_logo: e.target.result,
      });
    };
    reader.readAsDataURL(file);
  }

  handleCreateCompanyDetails(e) {
    e.preventDefault();
    const { comp_det_bussiness_name2, comp_det_ruc } = this.state;

    var object_error = {};
    axios
      .post("api/companyDetails", {
        comp_det_bussiness_name: comp_det_bussiness_name2,
        comp_det_ruc,
      })
      .then((res) => {
        this.setState({
          comp_det_bussiness_name2: "",
          comp_det_bussiness_name: res.data.dato.comp_det_bussiness_name,
          comp_det_id: res.data.dato.comp_det_id,
          comp_det_ruc: "",
        });
        this.handleCloseDialog();
        this.props.onSuccess(res.data.message);
        axios.get("api/companyDetails").then((res) => {
          this.setState({ companyDetails: res.data.data });
          var c = [];
          res.data.data.forEach((item, i) => {
            c.push({
              label: item.comp_det_bussiness_name,
              value: item.comp_det_id,
            });
          });
          this.setState({ companys: c });
        });
      });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { unit_name, unit_code, unit_logo, unit_type_id } = this.state;
    var validator = {
      unit_name: { error: false, message: "" },
      unit_code: { error: false, message: "" },
      unit_type_id: { error: false, message: "" },
    };
    var object_error = {};
    axios
      .post("api/units", {
        unit_name,
        unit_code,
        unit_logo,
        unit_type_id,
        unit_company: this.state.unit_company,
        comp_det_id: this.state.comp_det_id,
      })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.setState({ unit_name: "", unit_code: "", unit_logo: "" });
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  handleSubmit1(e) {
    const { unit_name, unit_code, unit_logo, unit_type_id } = this.state;
    var validator = {
      unit_name: { error: false, message: "" },
      unit_code: { error: false, message: "" },
      unit_type_id: { error: false, message: "" },
    };
    var object_error = {};
    e.preventDefault();
    axios
      .put(`api/units/${this.props.unit.data.unit_id}`, {
        unit_name,
        unit_code,
        unit_logo,
        unit_type_id,
        unit_company: this.state.unit_company,

        comp_det_id: this.state.comp_det_id,
      })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.handleClose();
      })
      .catch((error) => {
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  componentDidMount() {
    axios.get("api/companyDetails").then((res) => {
      this.setState({ companyDetails: res.data.data });
      var c = [];
      res.data.data.forEach((item, i) => {
        c.push({
          label: item.comp_det_bussiness_name,
          value: item.comp_det_id,
        });
      });
      this.setState({ companys: c });
    });
    axios.get("api/unit_types").then((res) => {
      this.setState({ unit_types: res.data.data });
    });
    if (this.props.edit) {
      const {
        unit_name,
        unit_code,
        unit_logo,
        unit_type_id,
        unit_company,
        comp_det_id,
      } = this.props.unit.data;
      axios.get("api/companyDetails/" + comp_det_id).then((res) => {
        this.setState({
          comp_det_bussiness_name: res.data.data.comp_det_bussiness_name,
          comp_det_ruc: res.data.data.comp_det_ruc,
        });
      });
      axios
        .get("/api/unit_types/" + unit_type_id)
        .then((res) => {
          this.setState({
            unit_name,
            unit_code,
            unit_logo,
            unit_type_id,
            unit_type_description: res.data.data.unit_type_description,
            comp_det_id,
            unit_company,
          });
        })
        .catch(() => {
          this.setState({
            unit_name,
            unit_code,
            unit_logo,
            comp_det_id,
            unit_company,
          });
        });
      // axios.get(`http://localhost:8000/api/units/${this.props.unit.data.unit_id}`).then(res=>{
      //   this.setState({unit_name: res.data.data.unit_name,
      //                  unit_code: res.data.data.unit_code,
      //                  unit_logo: res.data.data.unit_logo
      //                });
      //   }).catch(error=>{alert(error)});
    }
  }
  UnitTypes() {
    return this.state.unit_types.map((data) => ({
      label: data.unit_type_description,
      value: data.unit_type_id,
    }));
  }
  Companys() {
    return this.state.companyDetails.map((data) => ({
      label: data.comp_det_bussiness_name,
      value: data.comp_det_id,
    }));
  }

  selectValue() {
    return {
      label: this.state.unit_type_description,
      value: this.state.unit_type_id,
    };
  }
  selectValue2() {
    return {
      label: this.state.comp_det_bussiness_name,
      value: this.state.comp_det_id,
    };
  }

  initialFile() {
    if (this.props.edit) {
      return ["img/" + this.props.unit.data.unit_logo];
    } else {
      return [];
    }
  }
  editOrNew() {
    if (this.props.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit1}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit}
        >
          Guardar
        </Button>
      );
    }
  }
  render() {
    const {
      unit_name,
      unit_code,
      open,
      validator,
      snack_open,
      companys,
    } = this.state;
    const filter = createFilterOptions();
    var showSnack;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["unit_name"]["error"] ? (
              <li>Error: {validator["unit_name"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["unit_code"]["error"] ? (
              <li>Error: {validator["unit_code"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3 align="center">Formulario de Instituciones</h3>
        <hr />
        <form method="POST" encType="multipart/form-data">
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={12}>
                <TextField
                  id="outlined-full-width"
                  label="Nombre"
                  error={validator.unit_name.error}
                  helperText={validator.unit_name.message}
                  style={{ margin: 8 }}
                  placeholder="Ejemplo: Universidad Autonoma de Encarnacion"
                  value={unit_name}
                  name="unit_name"
                  onChange={this.fieldsChange.bind(this)}
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={6}>
                <TextField
                  id="outlined-full-width"
                  label="Codigo"
                  error={validator.unit_code.error}
                  helperText={validator.unit_code.message}
                  style={{ margin: 8 }}
                  placeholder="Ejemplo: UNAE"
                  value={unit_code}
                  name="unit_code"
                  onChange={this.fieldsChange.bind(this)}
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={6}>
                <Autocomplete
                  id="combo-box-demo"
                  disableClearable
                  options={[this.selectValue(), ...this.UnitTypes()]}
                  filterSelectedOptions
                  getOptionLabel={(option) => option.label}
                  onChange={this.fieldChange2}
                  value={this.selectValue()}
                  loading={this.state.time_out}
                  getOptionSelected={(option, value) =>
                    option.value === value.value
                  }
                  style={{ margin: 8 }}
                  fullWidth
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      error={this.state.validator.unit_type_id.error}
                      helperText={this.state.validator.unit_type_id.message}
                      label="Seleccione un tipo de unidad *"
                      variant="outlined"
                      fullWidth
                      onChange={this.searchFieldChange}
                      InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                          <React.Fragment>
                            {this.state.time_out === true ? (
                              <CircularProgress color="inherit" size={20} />
                            ) : null}
                            {params.InputProps.endAdornment}
                          </React.Fragment>
                        ),
                      }}
                    />
                  )}
                />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={6}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.unit_company}
                      onChange={(event) => {
                        this.handleChange(event);
                      }}
                      name="unit_company"
                      color="primary"
                    />
                  }
                  label="Es empresa"
                />
              </Grid>
              {this.state.unit_company === true && (
                <Grid item xs={6}>
                  <React.Fragment>
                    <Autocomplete
                      value={this.selectValue2()}
                      onChange={(event, newValue) => {
                        if (typeof newValue === "string") {
                          // timeout to avoid instant validation of the dialog's form.
                          setTimeout(() => {
                            this.setState({ openDialog: true });
                            this.setState({
                              comp_det_bussiness_name: newValue.label,
                              comp_det_ruc: "",
                              comp_det_id: newValue.value,
                            });
                          });
                        } else if (newValue && newValue.inputvalue) {
                          this.setState({ openDialog: true });
                          this.setState({
                            comp_det_bussiness_name: newValue.label,
                            comp_det_bussiness_name2: newValue.inputvalue,
                            comp_det_ruc: "",
                            comp_det_id: newValue.value,
                          });
                        } else {
                          this.setState({
                            comp_det_bussiness_name: newValue.label,
                            comp_det_id: newValue.value,
                          });
                        }
                      }}
                      filterOptions={(options, params) => {
                        const filtered = filter(options, params);
                        if (params.inputValue !== "") {
                          filtered.push({
                            inputvalue: params.inputValue,
                            label: `Agregar "${params.inputValue}"`,
                          });
                        }
                        return filtered;
                      }}
                      id="free-solo-dialog-demo"
                      options={companys}
                      getOptionLabel={(option) => {
                        // e.g value selected with enter, right from the input

                        if (typeof option === "string") {
                          return option;
                        }
                        if (option.inputvalue) {
                          return option.inputvalue;
                        }
                        return option.label;
                      }}
                      selectOnFocus
                      clearOnBlur
                      handleHomeEndKeys
                      renderOption={(option) => option.label}
                      style={{ width: 300 }}
                      freeSolo
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Datos Administrativos"
                          variant="outlined"
                        />
                      )}
                    />

                    <Dialog
                      open={this.state.openDialog}
                      onClose={() => {
                        this.handleCloseDialog();
                      }}
                      aria-labelledby="form-dialog-title"
                    >
                      <form onSubmit={this.handleCreateCompanyDetails}>
                        <DialogTitle id="form-dialog-title">
                          Agregar datos
                        </DialogTitle>
                        <DialogContent>
                          <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={this.state.comp_det_bussiness_name2}
                            onChange={(event) =>
                              this.setState({
                                comp_det_bussiness_name2: event.target.value,
                              })
                            }
                            label="Razon social"
                            type="text"
                          />
                          <TextField
                            margin="dense"
                            id="name"
                            value={this.state.comp_det_ruc}
                            onChange={(event) =>
                              this.setState({
                                comp_det_ruc: event.target.value,
                              })
                            }
                            label="Ruc"
                            type="text"
                          />
                        </DialogContent>
                        <DialogActions>
                          <Button
                            onClick={() => {
                              this.handleCloseDialog();
                            }}
                            color="primary"
                          >
                            Cancelar
                          </Button>
                          <Button type="submit" color="primary">
                            Guardar
                          </Button>
                        </DialogActions>
                      </form>
                    </Dialog>
                  </React.Fragment>
                </Grid>
              )}
            </Grid>
          </Grid>

          <DropzoneArea
            acceptedFiles={["image/*"]}
            initialFiles={this.initialFile()}
            onChange={this.imageChange.bind(this)}
            dropzoneText="Arrastre la imagen aqui o click para selecionar un logo"
            showAlerts={false}
            filesLimit={1}
          />
          <br />
          {this.editOrNew()}
          {showSnack}

          <Button
            variant="outlined"
            color="secondary"
            startIcon={<ExitToAppIcon />}
            onClick={this.handleClose}
          >
            Cerrar
          </Button>
        </form>
      </div>
    );
  }
}
export default FormUnit;
