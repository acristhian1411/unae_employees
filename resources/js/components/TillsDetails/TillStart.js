import React, { Component } from "react";
import {
  Button,
  TextField,
  Grid,
  CircularProgress,
  Tooltip,
  Snackbar,
} from "@material-ui/core";
import { Autocomplete, Alert } from "@material-ui/lab";
import SaveIcon from "@material-ui/icons/Save";

export default class TillStart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: "",
      disabled: false,
      till_id: null,
      till_name: "",
      tills: [],
      validator: {
        amount: {
          message: "",
          error: false,
        },
        till_id: {
          message: "",
          error: false,
        },
      },
    };
    this.fieldChange2 = this.fieldChange2.bind(this);
    this.fieldChange1 = this.fieldChange1.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidMount() {
    if (this.props.till) {
      this.setState({
        till_name: this.props.till.till_name,
        till_id: this.props.till.till_id,
        disabled: true,
      });
    } else {
      axios.get("/api/tills?to_start=true").then((res) => {
        this.setState({ tills: res.data.data });
      });
    }
  }
  fieldChange1(e) {
    this.setState({ [e.target.name]: e.target.value });
    this.props.changeAtribute(true);
  }
  fieldChange2(e, values) {
    // console.log(values);
    this.setState({ till_id: values.value, till_name: values.label });
    this.props.changeAtribute(true);
  }
  handleClose() {
    // axios.get('/api/till_is_starting').then(res=>{
    //   console.log(res.data);
    //   if (res.data === true) {
    //     this.setState({open: false});
    //   }
    //   else {
    //     this.setState({open: true});
    //   }
    // }).catch(error=>{
    //   console.log(error);
    // })
    // this.setState({open:false});
    this.props.onHandleSubmit();
  }
  handleSubmit(e) {
    e.preventDefault();
    // console.log('recibe algo');
    // console.log(this.state);
    var { amount, till_id } = this.state;
    var validator = {
      amount: { error: false, message: "" },
      till_id: { error: false, message: "" },
    };
    var object_error = {};
    var object_submit = {
      till_id,
      amount,
      factura_id: 0,
      description: "Apertura de caja",
      dettills_type: true,
    };
    axios
      .post("/api/tillDetails", { ...object_submit })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        // this.setState({amount: '', till_id: '', till_name:''});
        if (res.data == 'Detalle de Caja se agrego con exito!') {
          this.handleClose();
        }
        else {
          this.props.onSuccess(res.data, 'error');
        }

      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open2: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  Tills() {
    return this.state.tills.map((data) => ({
      label: data.till_name,
      value: data.till_id,
    }));
  }

  render() {
    return (
      <div>
        <h3>Apertura de caja</h3>
        <hr />
        <form>
          <Autocomplete
            id="combo-box-demo"
            disableClearable
            disabled={this.state.disabled}
            options={[
              { label: this.state.till_name, value: this.state.till_id },
              ...this.Tills(),
            ]}
            filterSelectedOptions
            getOptionLabel={(option) => option.label}
            onChange={this.fieldChange2}
            value={{ label: this.state.till_name, value: this.state.till_id }}
            // loading={this.state.time_out}
            getOptionSelected={(option, value) => option.value === value.value}
            style={{ margin: 8 }}
            fullWidth
            renderInput={(params) => (
              <TextField
                {...params}
                error={this.state.validator.till_id.error}
                helperText={this.state.validator.till_id.message}
                label="Seleccione una caja *"
                variant="outlined"
                fullWidth
                // onChange={this.searchFieldChange}
                // InputProps={{
                //   ...params.InputProps,
                //   endAdornment: (
                //     <React.Fragment>
                //       {this.state.time_out === true ? <CircularProgress color="inherit" size={20} /> : null}
                //       {params.InputProps.endAdornment}
                //     </React.Fragment>
                //   ),
                // }}
              />
            )}
          />
          <TextField
            id="outlined-full-width"
            error={this.state.validator.amount.error}
            helperText={this.state.validator.amount.message}
            label="Monto"
            style={{ margin: 8 }}
            name="amount"
            value={this.state.amount}
            onChange={this.fieldChange1}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <Button
            variant="outlined"
            color="primary"
            startIcon={<SaveIcon />}
            type="submit"
            onClick={this.handleSubmit}
          >
            Guardar
          </Button>
          {/*<Button variant="outlined" color="secondary" startIcon={<ExitToAppIcon />} onClick={this.handleClose}>Cerrar</Button>
           */}
        </form>
      </div>
    );
  }
}
