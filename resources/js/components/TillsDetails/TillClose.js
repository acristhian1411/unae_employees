import React, { Component } from "react";
import {
  Button,
  TextField,
  Grid,
  CircularProgress,
  Tooltip,
  Snackbar,
} from "@material-ui/core";
import { Autocomplete, Alert } from "@material-ui/lab";
import SaveIcon from "@material-ui/icons/Save";

export default class TillClose extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time_out:false,
      severity:'error',
      message_success:'',
      snack_open:false,
      amount: "",
      disabled: false,
      till_id: null,
      destiny_id: null,
      tills_to_close:[],
      till_name: "",
      destiny_name: "",
      suma: null,
      tills: [],
      validator: {
        amount: {
          message: "",
          error: false,
        },
        till_id: {
          message: "",
          error: false,
        },
      },
    };
    this.fieldChange3 = this.fieldChange3.bind(this);
    this.fieldChange2 = this.fieldChange2.bind(this);
    this.fieldChange1 = this.fieldChange1.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  componentDidMount() {
    console.log("hola desde tillclose")
      console.log(this.props);
    if (this.props.till) {
      
      this.setState({
        till_name: this.props.till.till_name,
        till_id: this.props.till.till_id,
        disabled: true,
      });
      axios.get("/api/tills-starting").then((res) => {
        this.setState({ tills: res.data.data });
      });
      axios
        .get("/api/tills_close/" + this.props.till.till_id)
        .then((res) => {
          this.setState({ suma: 0,
            tills_to_close:res.data.data
           });
        });
    } else {
      axios.get("/api/tills").then((res) => {
        this.setState({ tills: res.data.data });
      });
    }
  }
  fieldChange1(e) {
    this.setState({ [e.target.name]: e.target.value });
    this.props.changeAtribute(true);
  }
  fieldChange2(e, values) {
    this.setState({ till_id: values.value, till_name: values.label });
    this.props.changeAtribute(true);
  }
  fieldChange3(e, values, index) {
    var till = this.state.tills_to_close;
    till[index].till_id = values.value
    till[index].till_name = values.label
    this.setState({ tills_to_close: till });
    this.props.changeAtribute(true);
  }
  handleClose() {
    // axios.get('/api/till_is_starting').then(res=>{
    //   if (res.data === true) {
    //     this.setState({open: false});
    //   }
    //   else {
    //     this.setState({open: true});
    //   }
    // }).catch(error=>{
    //   console.log(error);
    // })
    // this.setState({open:false});
    this.props.onHandleSubmit();
  }

  exportToPDF() {
    open(`/#/reporte_cierre_caja/${this.state.till_id}`);
  }

  handleSubmit(e) {
    e.preventDefault();
    // console.log('recibe algo');
    // console.log(this.state);
    var valid = false;
    var { amount, till_id } = this.state;
    var validator = {
      amount: { error: false, message: "" },
      till_id: { error: false, message: "" },
    };
    var object_error = {};
    var object_submit;
    if (this.props.status === "close") {

      var till = this.state.tills_to_close
      // .filter(x=> x.till_id == 0)

      if(till.length == 0){
        this.setState({snack_open:true, message_success:'Espere a que termine de cargar el formulario', severity:'error'})
        valid = false;
      }else{
        var till = this.state.tills_to_close.filter(x=> x.till_id == 0)
        if(till.length > 0){
          this.setState({snack_open:true, message_success:'Debe seleccionar todas las cajas a transferir', severity:'error'})
        valid = false;
        }else{
          valid = true;
          this.props.onSuccess()
        }
      }


      object_submit = {
        till_id,
        amount: this.state.suma,
        factura_id: 0,
        description: "Cierre de caja",
        dettills_type: false,
        till_to_close:this.state.tills_to_close,
        destiny_id: this.state.destiny_id
      };
    } else if (this.props.status === "insert") {
      valid = true;
      object_submit = {
        till_id,
        amount: this.state.amount,
        factura_id: 0,
        description: "Ingreso de dinero",
        dettills_type: true,
      };
    }
if(valid == true){
  axios.post("/api/tillDetails", { ...object_submit }).then((res) => {
      this.props.onSuccess(res.data);
      this.props.changeAtribute(false);
      if (this.props.status === "close") {
        this.exportToPDF();
      }
      this.handleClose();
    });
    console.log("se envió detalles")
}
  
    // .catch((error) => {
    //   // console.log(Object.keys(error.response.data.errors));
    //   this.setState({ snack_open2: true });
    //   var errores = Object.keys(error.response.data.errors);
    //   for (var i = 0; i < errores.length; i++) {
    //     object_error = {
    //       ...object_error,
    //       [errores[i]]: {
    //         error: true,
    //         message: error.response.data.errors[errores[i]][0],
    //       },
    //     };
    //   }
    //   this.setState({
    //     validator: {
    //       ...validator,
    //       ...object_error,
    //     },
    //   });
    // });
  }
  Tills() {
    return this.state.tills.map((data) => ({
      label: data.till_name,
      value: data.till_id,
    }));
  }

  search(event, index) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    this.setState({destiny_name: e.target.value})
    var till = this.state.tills_to_close;
    // if (this.state.time_out == false) {
    till[index].time_out = true;
      this.setState({ tills_to_close: till });
      // setTimeout(() => {
        var url = ''
        if (e.target.value === "") {
          url = `/api/tills-starting`
          till[index].time_out = false;

          this.setState({
            time_out: false,
            url: url,
            tills_to_close: till
          });
          axios.get(`${url}`).then((res) => {
          till[index].time_out = false;
            this.setState({
              tills: res.data.data,
              time_out: false,
              tills_to_close: till
            });
          }).catch((error) => {
          till[index].time_out = false;
            this.setState({time_out: false,
              tills_to_close: till});
            console.log("Ingrese un valor valido");
          });

          } else {
          url = `/api/tills?till_name=${e.target.value}`
          this.setState({
            url: url,
          });
          axios.get(`${url}`).then((res) => {
          till[index].time_out = false;
              this.setState({
                tills: res.data.data,
                time_out: false,
                tills_to_close: till
              });
            }).catch((error) => {
          till[index].time_out = false;
              this.setState({time_out: false,
                tills_to_close: till
              });
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  render() {
    var showSnack
    if (this.state.snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={this.state.snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity={this.state.severity}
          >
            {this.state.message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
      {showSnack}
        {this.props.status === "close" ? (
          <h3>Cierre de caja</h3>
        ) : this.props.status === "insert" ? (
          <h3>Ingresar dinero en caja</h3>
        ) : (
          <p></p>
        )}

        <hr />
        {/* <form> */}

          <Autocomplete
            id="combo-box-demo"
            disableClearable
            disabled={this.state.disabled}
            options={[
              { label: this.state.till_name, value: this.state.till_id },
              ...this.Tills(),
            ]}
            filterSelectedOptions
            getOptionLabel={(option) => option.label}
            onChange={this.fieldChange2}
            value={{ label: this.state.till_name, value: this.state.till_id }}
            // loading={this.state.time_out}
            getOptionSelected={(option, value) => option.value === value.value}
            style={{ margin: 8 }}
            fullWidth
            renderInput={(params) => (
              <TextField
                {...params}
                error={this.state.validator.till_id.error}
                helperText={this.state.validator.till_id.message}
               
                label="Caja actual "
                variant="outlined"
                fullWidth
              />
            )}
          />
          {this.props.status === "insert" && (
            <TextField
              id="outlined-full-width"
              error={this.state.validator.amount.error}
              helperText={this.state.validator.amount.message}
              label="Saldo en caja"
              style={{ margin: 8 }}
              name="amount"
              type="number"
              value={this.state.amount}
              onChange={this.fieldChange1}
              fullWidth
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
            />
          )}
        {this.props.status === "close" && (
          this.state.tills_to_close.map((data,key)=>(
            <div>
              <label>{data.pay_type_desc}</label>
              <TextField
              id="outlined-full-width"
              // error={this.state.validator.amount.error}
              // helperText={this.state.validator.amount.message}
              label={"Monto de "+data.pay_type_desc+" en caja"}
              disabled
              style={{ margin: 8 }}
              name="amount"
              type="number"
              value={data.amount}
              // onChange={this.fieldChange1}
              fullWidth
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
              }}
            />
        <Autocomplete
          id="combo-box-demo"
          disableClearable
          options={[
            { label: data.till_name, value: data.till_id },
            ...this.Tills(),
          ]}
          filterSelectedOptions
          getOptionLabel={(option) => option.label}
          onChange={(e, values) =>{
            if(values === null){
              // console.log("aplicar los cambios");
              this.setState({destiny_name: '', destiny_id:''})
             
            }else{
              this.fieldChange3(e, values, key)
            }
          }}
          // onChange={(event, values) =>{this.fieldChange3(e, values)}}
          value={{ label: data.till_name, value: data.till_id }}
          getOptionSelected={(option, value) => option.value === value.value}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => (
            <TextField
              {...params}
              label={"Seleccione caja para transferir saldo de "+data.pay_type_desc+" *"}
              variant="outlined"
              fullWidth
              onKeyDown={(e) =>{
                if(e.key === 'Enter'){
                  e.preventDefault();
                  this.search(e, key)
                }
              }}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {data.time_out === true ? (
                      <CircularProgress color="inherit" size={20} />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                ),
              }}
            />
          )}
        />
        <br/>
            </div>
          ))
          
        )  }

          <Button
            variant="outlined"
            color="primary"
            startIcon={<SaveIcon />}
            type="submit"
            onClick={this.handleSubmit}
          >
          {this.props.status == 'insert'?
            'Guardar'
            :
            'Cerrar caja'
          }

          </Button>
          {/*<Button variant="outlined" color="secondary" startIcon={<ExitToAppIcon />} onClick={this.handleClose}>Cerrar</Button>
           */}
        {/* </form> */}
      </div>
    );
  }
}
