/*
*desplegar la lista de details en una tabla, que muestre la persona,
                                    el numero de details, monto total
*/
import React, { Component } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Grid,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
export default class TillDetailShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [],
      ticketDetails: [],
      value: 1234567,
      url: `/api/tillDetails/${this.props.details}`,
      // url: `/api/tillDetails/1`,
      permissions: {
        ticket_index: null,
        ticket_show: null,
        ticket_store: null,
        ticket_update: null,
        ticket_destroy: null,
      },
    };

    this.getObject = async () => {
      let res;
      res = await axios.get(`${this.state.url}`);
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          details: data.data,
          ticketDetails: data.data.ticketDetails,
        });
      }
    };
  }

  onChangeTariffPayamount(e) {
    var d = parseInt(e.target.value);
    this.setState({
      value: d,
    });
  }

  componentDidMount() {
    this._ismounted = true;
    var permissions = [
      "ticket_index",
      "ticket_show",
      "ticket_store",
      "ticket_update",
      "ticket_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject();
        }
      });
  }

  componentWillUnmount() {
    this._ismounted = false;
  }

  render() {
    return (
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={6}>
              <h2>Fecha: {this.state.details.date}</h2>
            </Grid>
            <Grid item xs={6}>
              <h2>
                Tipo de transaccion:{" "}
                {this.state.details.dettills_type === true
                  ? "Ingreso"
                  : "Egreso"}
              </h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={6}>
              <h2>Descripcion: {this.state.details.description}</h2>
            </Grid>
            <Grid item xs={6}>
              <h2>Caja : {this.state.details.till_name}</h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2>
                Monto:{" "}
                {new Intl.NumberFormat("es-PY", {
                  style: "currency",
                  currency: "PYG",
                }).format(this.state.details.amount)}
              </h2>
            </Grid>
          </Grid>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2>Observacion: </h2>
              <p>
                {this.state.details.tilltrans_observation === null
                  ? "--"
                  : this.state.details.tilltrans_observation}
              </p>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}
