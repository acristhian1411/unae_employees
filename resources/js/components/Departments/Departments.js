import React, { Component, lazy } from "react";
import MidModal from "../Modals/MidModal";
import FormDepartment from "./FormDepartment";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {Capitalize} from '../GlobalFunctions/LetterCase';
// const FormDepartment = lazy(()=> import('./FormDepartment'));
// const DialogDestroy = lazy(()=> import('../Dialogs/DialogDestroy'));
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

export default class Departments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      departments: [],
      filterDepartments: [],
      depart_id: 0,
      department: [],
      edit: false,
      _new: false,
      open: false,
      paginator: [],
      rowsPerPage: 10,
      order: "asc",
      orderBy: "depart_name",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/departments",
      busqueda: false,
      permissions: {
        departments_index: null,
        departments_show: null,
        departments_store: null,
        departments_update: null,
        departments_destroy: null,
      },
    };
    // this.handleClickOpen = (data) =>{
    //   this.setState({open: true, department: data});
    // }
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let { data } = await res;
      // console.log(res.data.last_page);
      if (this._ismounted) {
        this.setState({
          departments: data,
          paginator: res.data,
          filterDepartments: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        _new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };

    this.destroy = this.destroy.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.search = this.search.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("DEPARTAMENTOS");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "departments_index",
      "departments_show",
      "departments_store",
      "departments_update",
      "departments_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  handleModal() {
    this.setState({ _new: true });
  }
  handleClickOpen(data) {
    this.setState({ open: true, department: data });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  editModal(data) {
    this.setState({ edit: true, department: data });
  }
  destroy() {
    // event.preventDefault();
    axios.delete(`/api/departments/${this.state.depart_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filterDepartments: this.state.departments.data,
            paginator: this.state.departments,
            page_lenght: this.state.departments.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/departments`,
          });
        } else {
          this.setState({
            url: `/api/departments/search/${e.target.value}`,
          });
          axios
            .get(
              `${this.state.url}?sort_by=${this.state.orderBy}&order=${
                this.state.order
              }&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filterDepartments: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  render() {
    const {
      snack_open,
      message_success,
      open,
      _new,
      edit,
      department,
      filterDepartments,
      rowsPerPage,
      orderBy,
      order,
      permissions,
    } = this.state;
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    if (_new) {
      // showModal = <FormDepartment onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormDepartment />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (edit) {
      // showModal = <FormDepartment edit ={true} department={department} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormDepartment />,
            props_form: {
              edit: true,
              department: department,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={department.data.depart_name}
          onClose={this.handleClose}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filterDepartments.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div style={{textAlign: "center"}}>
          <TextField
            id="outlined-search"
            label="Escriba para buscar"
            type="search"
            name="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "depart_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "depart_name"}
                    direction={orderBy === "depart_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("depart_name");
                    }}
                  >
                    <h3>Nombre</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "country_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "country_name"}
                    direction={orderBy === "country_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("country_name");
                    }}
                  >
                    <h3>Pais</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2">
                  {permissions.departments_store === true && (
                    <Button
                      name="new_item"
                      variant="outlined"
                      color="primary"
                      type="submit"
                      onClick={() => {
                        this.handleModal();
                      }}
                    >
                      <AddBoxOutlinedIcon />
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {/* <Paginator
          lenght={this.state.page_lenght}
          perPageChange= {this.perPageChange}
          pageChange ={this.pageChange}
          page= {this.state.page}
          /> */}
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }
  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const {
      open,
      department,
      filterDepartments,
      rowsPerPage,
      page,
      permissions,
    } = this.state;
    if (filterDepartments.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return filterDepartments.map((data) => {
        return (
          <TableBody key={data.depart_id}>
            <TableRow>
              <TableCell>{Capitalize(data.depart_name)}</TableCell>
              <TableCell>{Capitalize(data.country_name)}</TableCell>
              <TableCell>
                {permissions.departments_update === true && (
                  <Button
                    name="edit_item"
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.editModal({ data: data });
                    }}
                  >
                    <EditIcon />
                  </Button>
                )}
              </TableCell>
              <TableCell>
                {permissions.departments_destroy === true && (
                  <Button
                    name="delete_item"
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      this.handleClickOpen({ data: data }),
                        this.setState({ depart_id: data.depart_id });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  } //cierra el renderList
}
