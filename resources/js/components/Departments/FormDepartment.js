import React, {Component} from 'react';
import {Button, TextField, Grid, CircularProgress, Tooltip, Snackbar} from '@material-ui/core';
import { Autocomplete, Alert } from '@material-ui/lab';
import SaveIcon from '@material-ui/icons/Save';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import SmallModal from '../Modals/SmallModal';
import ModalCountry from '../Countries/ModalCountry';

class FormDepartment extends Component {
   constructor(props){
     super(props)
     this.state = {
       snack_open2: false,
       snack_open: false,
       message_success: '',
       open: true,
       depart_name: '',
       country_id: null,
       country_name: '',
       modal_country: false,
       countries: [],
       sucess: false,
       error: false,
       time_out: false,
       validator: {
         depart_name:{
           message:'',
           error: false,
         },
         country_id:{
           message:'',
           error: false
         }
       }

     }
     this.updateState= () =>{
       this.setState({
         modal_country: false,
       });
       axios.get('api/countries').then(res=>{
         this.setState({countries: res.data.data});
         });

     }
     this.handleOpen = this.handleOpen.bind(this);
     this.handleClose = this.handleClose.bind(this);
     this.fieldChange1 = this.fieldChange1.bind(this);
     this.fieldChange2 = this.fieldChange2.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
     this.handleSubmit1 = this.handleSubmit1.bind(this);
     this.selectValue= this.selectValue.bind(this);
     this.openModalCountry = this.openModalCountry.bind(this);
     this.searchFieldChange = this.searchFieldChange.bind(this);
     this.closeSnack = this.closeSnack.bind(this);
     this.openSnack = this.openSnack.bind(this);
     this.closeSnack2 = this.closeSnack2.bind(this);
   }
   closeSnack2(){
     this.setState({snack_open2: false});
   }
   handleOpen(){
     //habra que ver si esta funcion y el open sirven para algo en los formularios
     this.setState({open:true});
   };
   openModalCountry(){
     this.setState({modal_country:true});
   };
   closeSnack(){
     this.setState({snack_open: false});
   }
   openSnack(param){
     // console.log(param);
     this.setState({snack_open: true, message_success: param});
   }
   handleClose(){
     this.setState({open:false});
     this.props.onHandleSubmit();
   }
   fieldChange1(e){
     this.setState({[e.target.name]: e.target.value});
     this.props.changeAtribute(true);
   }
   fieldChange2(e, values){
     // console.log(values);
     this.setState({country_id: values.value, country_name: values.label});
     this.props.changeAtribute(true);
   }
   searchFieldChange(event){
     var e = event;
     this.setState({country_name: e.target.value, country_id: null});
     e.persist();
     if (this.state.time_out==false) {
       this.setState({time_out:true});
       setTimeout(()=>{
         if(e.target.value===''){
           axios.get('api/countries').then(res=>{
             this.setState({countries: res.data.data, time_out: false});
             });
         }
         else {
           axios.get(`api/countries/search/${e.target.value}`).then(res=>{
                   this.setState({
                     countries: res.data.data,
                     time_out:false
                   });
                 }).catch(error=>{
                   console.log("Ingrese un valor valido");
                 });
         }}
       , 3000);
     }
   }
   handleSubmit(e){
     e.preventDefault();
     var {depart_name, country_id} = this.state;
     var validator = {depart_name: {error: false,  message: ''},
                      country_id: {error: false,  message: ''}};
     var object_error = {};
       axios.post('api/departments', {depart_name, country_id}).then(
           res=>{
             this.props.onSuccess(res.data);
             this.props.changeAtribute(false);
             this.setState({depart_name: '', country_id: '', country_name:''});
             this.handleClose();
           }).catch(error => {
             // console.log(Object.keys(error.response.data.errors));
             this.setState({snack_open2: true});
             var errores = Object.keys(error.response.data.errors);
             for (var i = 0; i < errores.length; i++) {
               object_error = {
                 ...object_error,
                 [errores[i]]: {
                   error: true,
                   message: error.response.data.errors[errores[i]][0]
                 }
               }
             }
             this.setState({
               validator:{
                 ...validator,
                 ...object_error
               }
             });
           });
   }

   handleSubmit1(e){
     e.preventDefault();
     var {depart_name, country_id} = this.state;
     var validator = {depart_name: {error: false,  message: ''},
                      country_id: {error: false,  message: ''}};
     var object_error = {};
       axios.put(`api/departments/${this.props.department.data.depart_id}`, {depart_name, country_id}).then(
         res=>{
           this.props.onSuccess(res.data);
           this.handleClose();
         }
       ).catch(error => {
         // console.log(Object.keys(error.response.data.errors));
         this.setState({snack_open2: true});
         var errores = Object.keys(error.response.data.errors);
         for (var i = 0; i < errores.length; i++) {
           object_error = {
             ...object_error,
             [errores[i]]: {
               error: true,
               message: error.response.data.errors[errores[i]][0]
             }
           }
         }
         this.setState({
           validator:{
             ...validator,
             ...object_error
           }
         });
       });

   }
   componentDidMount(){
     // console.log(this);
     axios.get('api/countries').then(res=>{
       this.setState({countries: res.data.data});
       });
     if (this.props.edit) {
       // console.log(this.props);
       const {depart_name, country_id, country_name} = this.props.department.data;
       this.setState(
         {depart_name, country_id, country_name,
        });
        // axios.get(`http://localhost:8000/api/departments/${this.props.department.data.depart_id}`).then(res=>{
        //  this.setState({depart_name: res.data.data.depart_name,
        //                 country_id: res.data.data.country_id,
        //                 country_name: res.data.data.country_name,
        //                 });
        //  }).catch(error=>{alert(error)});
       // var d_id = this.props.department.data.depart_id;
       // var d_name = this.state.depart_name;
       // var d_cou_id = this.state.country_id;
       // var edit = this.props.edit;

     }
   }
   Country() {
          return (this.state.countries.map(data => ({ label: data.country_name, value: data.country_id })));
        }
   editOrNew(){
     if (this.props.edit) {
       return(
         <Button name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleSubmit1}>Actualizar</Button>
       );
     }
     else {
       return(
       <Button name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleSubmit}>Guardar</Button>
       );
     }
   }
   selectValue(){
       return {label:this.state.country_name, value:this.state.country_id};
   }
   render(){
     const {validator, snack_open2} = this.state;
     var showModalCountry;
     var showSnack;
     var showSnack2;
     if (snack_open2){
           showSnack2 = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={snack_open2} autoHideDuration={6000} onClose={this.closeSnack2}>
                          <Alert elevation={6} variant="filled" onClose={this.closeSnack2} severity="error">
                            {validator['country_id']['error']?<li>Error: {validator['country_id']['message']}</li>:''}
                            {validator['depart_name']['error']?<li>Error: {validator['depart_name']['message']}</li>:''}
                          </Alert>
                        </Snackbar>
         }
     if (this.state.modal_country) {
       showModalCountry = <SmallModal >
                     {{onHandleSubmit: this.updateState,
                       form: <ModalCountry />,
                       props_form: {onSuccess:this.openSnack}
                     }}
                   </ SmallModal>
     }
     if (this.state.snack_open){
           showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={this.state.snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                          <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="success">
                            {this.state.message_success}
                          </Alert>
                        </Snackbar>
         }
     return (

        <div>
               <h3>Formulario de Departamentos</h3>
               <hr />
                <form>
                <Grid container spacing={1}>
                  <Grid container item xs={12} spacing={0}>
                      <Grid item xs={11}>
                        <Autocomplete
                          id="combo-box-demo"
                          disableClearable
                          options={[this.selectValue(), ...this.Country()]}
                          filterSelectedOptions
                          getOptionLabel={(option) => option.label}
                          onChange={this.fieldChange2}
                          value={this.selectValue()}
                          loading={this.state.time_out}
                          getOptionSelected={(option, value)=>option.value === value.value}
                          style={{ margin: 8 }}
                          fullWidth
                          renderInput={(params) => <TextField {...params} error={this.state.validator.country_id.error}
                                                              name="country_name"
                                                              helperText={this.state.validator.country_id.message}
                                                              label="Seleccione un Pais *"
                                                              variant="outlined"
                                                              fullWidth
                                                              onChange={this.searchFieldChange}
                                                              InputProps={{
                                                                ...params.InputProps,
                                                                endAdornment: (
                                                                  <React.Fragment>
                                                                    {this.state.time_out === true ? <CircularProgress color="inherit" size={20} /> : null}
                                                                    {params.InputProps.endAdornment}
                                                                  </React.Fragment>
                                                                ),
                                                              }}
                                                              />}
                        />
                      </Grid>
                      <Grid item xs={1}>
                        <Tooltip title="Nuevo Pais">
                          <Button style={{ marginTop: 8,  height: 52, float: 'right'}} variant="outlined" color="primary" onClick={this.openModalCountry}>
                            <AddBoxOutlinedIcon />
                          </Button>
                        </Tooltip>
                      </Grid>
                  </Grid>
                </Grid>
                  <TextField
                      id="outlined-full-width"
                      error={this.state.validator.depart_name.error}
                      helperText={this.state.validator.depart_name.message}
                      label="Nombre *"
                      style={{ margin: 8 }}
                      placeholder="Ejemplo: ITAPUA"
                      name="depart_name"
                      value={this.state.depart_name}
                      onChange={this.fieldChange1}
                      fullWidth
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="outlined"
                    />

                {this.editOrNew()}
                {showModalCountry}
                {showSnack}
                {showSnack2}
                  {/*<Button variant="outlined" color="secondary" startIcon={<ExitToAppIcon />} onClick={this.handleClose}>Cerrar</Button>
                */}</form>
        </div>
       );
   }
 }
export default FormDepartment;
