import React, { Component, lazy } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import ModalForm from "./ModalForm";
import DialogDestroy from "../Dialogs/DialogDestroy";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import SearchIcon from "@material-ui/icons/Search";
import ClearIcon from "@material-ui/icons/Clear";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Grid,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  Tooltip,
  TableSortLabel,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import { Alert } from "@material-ui/lab";
import { Autocomplete } from "@material-ui/lab";
import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import EmployeesFilters from "./EmployeesFilters";
export default class Employees extends Component {
  constructor(props) {
    super(props);

    this.state = {
      snack_open: false,
      message_success: "",
      open_filters:false,
      employees: [],
      filteredemployees: [],
      person_id: 0,
      person: [],
      edit: false,
      _new: false,
      open: false,
      paginator: [],
      prevActiveStep: 1,
      rowsPerPage: 10,
      order: "asc",
      orderBy: "person_idnumber",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/employees",
      busqueda: false,
      buscador: false,
      person_fname: "",
      person_lastname: "",
      person_idnumber: "",
      person_birthdate: "",
      country_id: "",
      country_name: "",
      countries: [],
      appointments: [],
      app_appointment_id: "",
      appoin_description: "",
      permissions: {
        employees_index: null,
        employees_show: null,
        employees_store: null,
        employees_update: null,
        employees_destroy: null,
        employee_account_store: null,
      },
    };
    this.handleClickOpen = (data) => {
      this.setState({ open: true, person: data });
    };
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.getObject = async (
      perPage,
      page,
      route,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (route) {
        res = await axios.get(
          `${route}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        if (this.state.busqueda) {
          res = await axios.get(
            `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
          );
        } else {
          res = await axios.get(
            `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
          );
        }
      }
      let { data } = await res;
      if (this._ismounted) {
        this.setState({
          employees: data,
          paginator: res.data,
          filteredemployees: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        _new: false,
        open: false,
        openFilters: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };
    // pagina siguiente
    this.handleNext = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep + 1 });
      var newPage = this.state.prevActiveStep + 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //pagina anterior
    this.handleBack = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep - 1 });
      var newPage = this.state.prevActiveStep - 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //filas por pagina
    this.handleChangeSelect = (event) => {
      this.setState({ rowsPerPage: event.target.value, prevActiveStep: 1 });
      this.getObject(event.target.value, 1);
    };
    this.openFilters = (event)=>{
      this.setState({openFilters: true})
    }
    this.closeFilters = (event)=>{
      this.setState({openFilters: false})
    }
    this.destroy = this.destroy.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.search = this.search.bind(this);
    this.cleanfilter = this.cleanfilter.bind(this);
    this.selectsChange = this.selectsChange.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    this.props.changeTitlePage("FUNCIONARIOS");
    axios.get("/api/countries").then((res) => {
      this.setState({ countries: res.data.data });
    });
    axios.get("/api/appointments").then((res) => {
      this.setState({ appointments: res.data.data });
    });
    var permissions = [
      "employees_index",
      "employees_show",
      "employees_store",
      "employees_update",
      "employees_destroy",
      "employee_account_store",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }

  generateSalary() {
    axios.get(`/api/employeeSalary`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
    });
  }

  handleModal() {
    this.setState({ _new: true });
  }
  handleChangePage(event, newPage) {
    this.setState({ page: newPage });
  }
  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  editModal(data) {
    this.setState({ edit: true, person: data });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  destroy(event) {
    // event.preventDefault();
    axios.delete(`/api/employees/${this.state.person_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });

      this.updateState();
    });
  }
  Country() {
    return this.state.countries.map((data) => ({
      label: data.country_name,
      value: data.country_id,
    }));
  }
  Appointment() {
    return this.state.appointments.map((data) => ({
      label: data.appoin_description,
      value: data.appoin_id,
    }));
  }
  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }
  selectsChange(e, values, nameValue, nameLabel) {
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
  }
  fieldsChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }
  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true, filteredemployees:[]});
      // setTimeout(() => {
        if (this.state.buscador) {
          var param = `/api/employees-search?filter[person_fname]=${this.state.person_fname}
          &filter[person_lastname]=${this.state.person_lastname}
          &filter[person_idnumber]=${this.state.person_idnumber}
          &filter[nationality]=${this.state.country_id}
          &filter[birthdate]=${this.state.person_birthdate}
          &filter[appointment]=${this.state.app_appointment_id}`
          this.setState({
            url: param,
          });
          axios
            .get(`${param}&per_page=${this.state.rowsPerPage}`)
            .then((res) => {
              if(res.data != false){
                this.setState({
                  filteredemployees: res.data.data,
                  paginator: res.data,
                  page_lenght: res.data.last_page,
                  page: res.data.current_page,
                  time_out: false,
                  busqueda: true,
                });
              }else{
                this.setState({
                  filteredemployees: [],
                  time_out: false})
              }

            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        } else {
          if (e.target.value === "") {
            var param = `/api/employees`
            this.setState({
              filteredemployees: this.state.employees.data,
              paginator: this.state.employees,
              page_lenght: this.state.employees.last_page,
              page: 1,
              time_out: false,
              busqueda: false,
              url: param,
            });
            this.getObject(this.state.rowsPerPage, this.state.page, param);
          } else {
            var param = `/api/employees-search?search=${e.target.value}`
            this.setState({
              url: param,
            });
            axios
              .get(
                `${param}&sort_by=${this.state.orderBy}&order=${
                  this.state.order
                }&per_page=${this.state.rowsPerPage}&page=${1}`
              )
              .then((res) => {
                if(res.data!== false){
                this.setState({
                  filteredemployees: res.data.data,
                  paginator: res.data,
                  page_lenght: res.data.last_page,
                  page: res.data.current_page,
                  time_out: false,
                  busqueda: true,
                });
              }else{
                this.setState({
                  filteredemployees: [],
                  time_out: false})
              }
              })
              .catch((error) => {
                this.setState({time_out: false});
                console.log("Ingrese un valor valido");
              });
          }
        }
    //   }, 3000);
    // }
  }

  filter() {
    return (
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Nombre"
                type="search"
                variant="outlined"
                size="small"
                name="person_fname"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.person_fname}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Apellido"
                type="search"
                variant="outlined"
                size="small"
                name="person_lastname"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.person_lastname}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Cedula"
                type="number"
                variant="outlined"
                size="small"
                name="person_idnumber"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.person_idnumber}
              />
            </Grid>
            <Grid item xs={3}>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SearchIcon />}
                onClick={this.search.bind(this)}
              >
                Buscar
              </Button>
              <Tooltip title="Limpiar filtro">
                <Button
                  variant="contained"
                  color="secondary"
                  size="small"
                  startIcon={<ClearIcon />}
                  onClick={() => {
                    this.cleanfilter();
                  }}
                >
                  {" "}
                </Button>
              </Tooltip>
            </Grid>
          </Grid>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={3}>
              <Autocomplete
                id="combo-box-demo"
                disableClearable
                options={this.Country()}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) =>
                  this.selectsChange(e, values, "country_id", "country_name")
                }
                value={this.selectValue("country_name", "country_id")}
                getOptionSelected={(option, value) => {
                  if (value === option.value) {
                    return option.label;
                  } else {
                    return false;
                  }
                }}
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Nacionalidad"
                    className="textField"
                    size="small"
                    variant="outlined"
                  />
                )}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-basic"
                label="Fecha de Nacimiento"
                fullWidth
                type="date"
                className="textField"
                size="small"
                value={this.state.person_birthdate}
                name="person_birthdate"
                onChange={this.fieldsChange.bind(this)}
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
              />
            </Grid>
            <Grid item xs={4}>
              <Autocomplete
                id="combo-box-demo"
                disableClearable
                options={this.Appointment()}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) =>
                  this.selectsChange(
                    e,
                    values,
                    "app_appointment_id",
                    "appoin_description"
                  )
                }
                value={this.selectValue(
                  "appoin_description",
                  "app_appointment_id"
                )}
                getOptionSelected={(option, value) => {
                  if (value === option.value) {
                    return option.label;
                  } else {
                    return false;
                  }
                }}
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Cargo *"
                    className="textField"
                    size="small"
                    variant="outlined"
                  />
                )}
              />
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
  cleanfilter() {
    this.setState({
      url: `/api/employees`,
      busqueda: false,
      page: 1,
      person_fname: "",
      person_lastname: "",
      person_idnumber: "",
      country_id: "",
      country_name: "",
      app_appointment_id: "",
      appoin_description: "",
    });
    this.getObject(this.state.rowsPerPage, this.state.page, "/api/employees");
  }

  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.prevActiveStep,
      this.state.url,
      name,
      order
    );
  }
  render() {
    let newDate = new Date();
    var nowDate = newDate.getDate();
    const {
      snack_open,
      open,
      _new,
      message_success,
      edit,
      person,
      filteredemployees,
      rowsPerPage,
      orderBy,
      order,
      permissions,
    } = this.state;
    var busqueda;
    var showFilters;
    var busquedaAv;
    var paginator;
    var showModal;
    var showSnack;
    var showDialogDestroy;
    if (this.state.buscador) {
      busquedaAv = this.filter();
    } else {
      busqueda = (
        <TextField
          id="outlined-search"
          label="Escriba para buscar"
          type="search"
          variant="outlined"
          // onChange={this.search}
          onKeyDown={(e) =>{
            if(e.key === 'Enter'){
              this.search(e)
            }
          }}
          style={{ width: "40%"}}
          InputProps={{
            endAdornment: (
              <React.Fragment>
                {this.state.time_out === true ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
              </React.Fragment>
            ),
          }}
        />
      );
    }
    if (_new) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: { onSuccess: this.openSnack, permissions: this.state.permissions },
          }}
        </MidModal>
      );
      //ModalForm onHandleSubmit={this.updateState}
    } else if (edit) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: {
              edit: true,
              person: person,
              permissions: this.state.permissions,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );

      //ModalForm onHandleSubmit={this.updateState} edit ={true} person={person}
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={`${person.data.person_fname} ${person.data.person_lastname}`}
          onClose={this.handleClose}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filteredemployees.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    if(this.state.openFilters == true){
      showFilters = ( <MidModal>
        {{
          onHandleSubmit: this.updateState,
          form: <EmployeesFilters />,
          props_form: { onSuccess: this.openSnack, permissions: this.state.permissions },
        }}
      </MidModal>)
    }
    return (
      <div style={{textAlign: "center"}}>
        {showFilters}
                <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2 align="center">
                {busqueda}
                <Button
                  variant="outlined"
                  color="primary"
                  type="submit"
                  onClick={() => {
                    this.state.buscador === false
                      ? this.setState({ buscador: true })
                      : this.setState({ buscador: false });
                  }}
                >
                  {this.state.buscador?"Salir de busqueda avanzada": "Busqueda avanzada"}
                </Button>
              </h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <div align="center">{busquedaAv}</div>
            </Grid>
          </Grid>
          {permissions.employee_account_store === true &&(
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={6}>
                  <div align="center">
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        this.generateSalary();
                      }}
                    >
                      Generar Salario
                    </Button>
                  </div>
                </Grid>
                <Grid item xs={6}>
                  <div align="center">
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={(e) => {
                        this.openFilters(e);
                      }}
                    >
                     Reportes
                    </Button>
                  </div>
                </Grid>
              </Grid>
            )}
        </Grid>
        <hr />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "person_id" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_id"}
                    direction={orderBy === "person_id" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_id");
                    }}
                  >
                    <h3>ID</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "person_idnumber" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_idnumber"}
                    direction={orderBy === "person_idnumber" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_idnumber");
                    }}
                  >
                    <h3>Documento</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "person_lastname" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_lastname"}
                    direction={orderBy === "person_lastname" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_lastname");
                    }}
                  >
                    <h3>Apellidos</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "person_fname" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_fname"}
                    direction={orderBy === "person_fname" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_fname");
                    }}
                  >
                    <h3>Nombres</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "telephone" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "telephone"}
                    direction={orderBy === "telephone" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("telephone");
                    }}
                  >
                    <h3>Telefono</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2">
                  {permissions.employees_store === true && (
                    <Button
                      variant="outlined"
                      color="primary"
                      type="submit"
                      onClick={() => {
                        this.handleModal();
                      }}
                    >
                      <AddBoxOutlinedIcon />
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {/* <Paginator
        lenght={this.state.page_lenght}
        perPageChange= {this.perPageChange}
        pageChange ={this.pageChange}
        page= {this.state.page}
        /> */}

        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }
  renderList() {
    const {
      open,
      filteredemployees,
      rowsPerPage,
      page,
      permissions,
    } = this.state;
    if (filteredemployees.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return filteredemployees.map((data) => {
        return (
          <TableBody key={data.person_id}>
            <TableRow>
              <TableCell>{data.person_id}</TableCell>
              <TableCell>{data.person_idnumber}</TableCell>
              <TableCell>{Capitalize(data.person_lastname)}</TableCell>
              <TableCell>{Capitalize(data.person_fname)}</TableCell>
              <TableCell>{data.telephone}</TableCell>
              <TableCell>
                {permissions.employees_show === true && (
                  <Link
                    className="navbar-brand"
                    to={"/funcionarios/show/" + data.person_id}
                  >
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<VisibilityIcon />}
                    >
                      {" "}
                    </Button>
                  </Link>
                )}
              </TableCell>
              <TableCell>
                {permissions.employees_update === true && (
                  <Button
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.editModal({ data: data });
                    }}
                  >
                    <EditIcon />
                  </Button>
                )}
              </TableCell>
              <TableCell>
                {permissions.employees_destroy === true && (
                  <Button
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      this.handleClickOpen({ data: data }),
                        this.setState({ person_id: data.person_id });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  } //cierra el renderList
}
