import React, { Component, Fragment } from "react";
import {
  Modal,
  Grid,
  Backdrop,
  Fade,
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  Stepper,
  Fab,
  Step,
  StepLabel,
  Divider,
  TextareaAutosize,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { DropzoneArea, DropzoneDialog } from "material-ui-dropzone";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import { Autocomplete, Alert } from "@material-ui/lab";
import AddIcon from "@material-ui/icons/Add";
import InputAdornment from "@material-ui/core/InputAdornment";
import Tooltip from "@material-ui/core/Tooltip";
import RemoveIcon from "@material-ui/icons/Remove";
import DialogDestroy from "../Dialogs/DialogDestroy";
function getSteps() {
  return [
    "Datos Personales",
    "Vivienda",
    "Datos Laborales",
    "Contactos",
    "Usuarios",
  ];
}

class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role_time_out: false,
      time_out: false,
      time_out_till: false,
      snack_open: false,
      snack_openDestroy: false,
      message_success: "",
      openAlert: false,
      cntper_id: 0,
      index: 0,
      contacts: [
        {
          cnttype_id: 0,
          cnttype_name: "",
          cntper_data: "",
        },
      ],
      faculties: [],
      facu_name: null,
      facu_id: null,
      contactType: [],
      contactsEdit: [],
      contactsEdited: false,
      edit: false,
      roles: [],
      role_id: null,
      role_name: "",
      open: true,
      activeStep: 0,
      person_fname: "",
      person_lastname: "",
      till_id: "",
      till_name: "",
      tills:[],
      homecity_id: 0,
      homecity_name: "ciudad de residencia",
      hc_country_id: 0,
      hc_country_name: "pais de residencia",
      hc_depart_id: 0,
      hc_depart_name: "seleccione un departamento",
      hc_departments: [],
      hc_cities: [],
      birthplace_id: 0,
      birthplace_name: "ciudad de nacimiento",
      bp_country_id: 0,
      bp_country_name: "pais de origen",
      bp_depart_id: 0,
      bp_depart_name: "seleccione un departamento",
      bp_departments: [],
      bp_cities: [],
      country_id: 0,
      staff_id: 0,
      person_birthdate: "2000-01-01",
      person_gender: "Seleccione su sexo",
      person_idnumber: "",
      person_address: "",
      person_bloodtype: "Tipo de sangre",
      person_business_name: "",
      person_ruc: "",
      appointments: [],
      app_appointment_id: 0,
      appoin_description: "Seleccione un cargo",
      emplo_startdate: "2000-01-01",
      emplo_status: false,
      emplo_status2: "Seleccione un estado",
      emplo_observation: "",
      emplo_salary: 0,
      office_desc: '',
      number_of_children: '',
      civil_status: '',
      country_name: "pais de nacionalidad",
      person_photo: "",
      person_photo2: "",
      sucess: false,
      error: false,
      imagePreviewUrl: false,
      dropzone: false,
      whours: [],
      countries: [],
      validator: {
        app_appointment_id: { message: "", error: false },
        emplo_status: { message: "", error: false },
        person_fname: { message: "", error: false },
        person_lastname: { message: "", error: false },
        homecity_id: { message: "", error: false },
        birthplace_id: { message: "", error: false },
        country_id: { message: "", error: false },
        person_birthdate: { message: "", error: false },
        person_gender: { message: "", error: false },
        person_idnumber: { message: "", error: false },
        person_address: { message: "", error: false },
        person_bloodtype: { message: "", error: false },
        person_business_name: { message: "", error: false },
        person_ruc: { message: "", error: false },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.openDropzone = this.openDropzone.bind(this);
    this.closeDropzone = this.closeDropzone.bind(this);
    this.fieldsChange = this.fieldsChange.bind(this); //.person_fname, .person_lastname, .person_birthdate
    this.selectsChange = this.selectsChange.bind(this); // homecity_id:0,
    this.fieldChange13 = this.fieldChange13.bind(this); //person_photo
    this.fieldChange14 = this.fieldChange14.bind(this); //country_home_city
    this.fieldChange15 = this.fieldChange15.bind(this); //depart_home_city
    this.fieldChange16 = this.fieldChange16.bind(this); //country_bp
    this.fieldChange17 = this.fieldChange17.bind(this); //depart_bp
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.handleCloseAlert = this.handleCloseAlert.bind(this);
    this.destroy = this.destroy.bind(this);
  }
  handleNext() {
    this.setState({ activeStep: this.state.activeStep + 1 });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }

  handleBack() {
    this.setState({ activeStep: this.state.activeStep - 1 });
  }

  handleReset() {
    this.setState({ activeStep: 0 });
  }

  handleCloseAlert() {
    this.setState({
      openAlert: false,
    });
  }

  handleOpen(data) {
    this.setState({
      openAlert: true,
      cnttype_name: data,
    });
  }

  openDropzone() {
    this.initialFile();
    this.setState({ dropzone: true });
  }
  closeDropzone() {
    this.setState({ dropzone: false });
  }
  handleClose() {
    this.setState({ open: false });
    this.props.onHandleSubmit();
  }
  fieldsChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }
  selectsChange(e, values, nameValue, nameLabel, route, obj) {
    if (route) {
      axios.get("api/" + route + "/select/" + values.value).then((res) => {
        this.setState({ [obj]: res.data });
      });
    }

    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    this.props.changeAtribute(true);
  }
  fieldChange13(e) {
    if (e[0]) {
      this.createImage(e[0]);
    } else {
      this.setState({
        person_photo: "default.png",
        dropzone: false,
      });
    }
  }
  fieldChange14(e) {
    axios.get("api/departments/select/" + e.value).then((res) => {
      this.setState({ hc_departments: res.data.data });
    });
    this.setState({ hc_country_id: e.value, hc_country_name: e.label });
  }
  fieldChange15(e) {
    axios.get("api/cities/select/" + e.value).then((res) => {
      this.setState({ hc_cities: res.data.data });
    });
    this.setState({ hc_depart_id: e.value, hc_depart_name: e.label });
  }
  fieldChange16(e) {
    axios.get("api/departments/select/" + e.value).then((res) => {
      this.setState({ bp_departments: res.data.data });
    });
    this.setState({ bp_country_id: e.value, bp_country_name: e.label });
  }
  fieldChange17(e) {
    axios.get("api/cities/select/" + e.value).then((res) => {
      this.setState({ bp_cities: res.data });
    });
    this.setState({ bp_depart_id: e.value, bp_depart_name: e.label });
  }
  createImage(file) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        person_photo: e.target.result,
        dropzone: false,
      });
    };
    reader.readAsDataURL(file);
    reader.onloadend = function (e) {
      this.setState({
        person_photo2: [reader.result],
      });
      // console.log([reader.result]);
    }.bind(this);
  }

  handleSubmit(e) {
    const {
      person_fname,
      person_lastname,
      birthplace_id,
      homecity_id,
      country_id,
      till_id,
      person_birthdate,
      person_gender,
      person_idnumber,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
      person_photo,
      app_appointment_id,
      emplo_startdate,
      emplo_status,
      emplo_observation,
      emplo_salary,
      office_desc,
      number_of_children,
      civil_status,
      whours
    } = this.state;
    e.preventDefault();
    axios
      .post("/api/employees", {
        person_fname,
        person_lastname,
        homecity_id,
        birthplace_id,
        country_id,
        till_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        person_photo,
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
        office_desc,
        number_of_children,
        civil_status,
        whours
      })
      .then((res) => {
        var person = res.data.dato.person_id;
        axios.post("/api/contactPersons-per", {
          person_id: person,
          contacts: this.state.contacts,
        });
        axios.post("/api/staffs", {
          person_id: person,
          facu_id: this.state.facu_id,
        });
        const object = {
          email: this.state.email.toUpperCase(),
          person_id: person,
          name: this.state.person_fname + " " + this.state.person_lastname,
          role_id: this.state.role_id,
        };
        axios.post("/api/users", {
          email: object.email,
          person_id: object.person_id,
          name: object.name,
          username: object.email,
          role_id: object.role_id,
        });

        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  handleSubmit1(e) {
    e.preventDefault();
    const {
      person_fname,
      person_lastname,
      birthplace_id,
      homecity_id,
      country_id,
      till_id,
      person_birthdate,
      person_gender,
      person_idnumber,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
      person_photo,
      app_appointment_id,
      emplo_startdate,
      emplo_status,
      emplo_observation,
      emplo_salary,
      office_desc,
      number_of_children,
      civil_status,
      whours
    } = this.state;
    axios
      .put(`/api/employees/${this.props.person.data.person_id}`, {
        person_fname,
        person_lastname,
        homecity_id,
        birthplace_id,
        country_id,
        till_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        person_photo,
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
        office_desc,
        number_of_children,
        civil_status,
        whours
      })
      .then((res) => {


        if (this.state.contactsEdit.length != 0) {
          axios.post("/api/contactPersons-per", {
            person_id: this.props.person.data.person_id,
            contacts: this.state.contactsEdit,
          });
        }

        if (this.state.contactsEdited === true) {
          axios.put("/api/contactPersons", {
            person_id: this.props.person.data.person_id,
            contacts: this.state.contacts,
          });
        }
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        this.handleClose();

      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });

      axios.put("/api/staffs/"+this.state.staff_id, {
        person_id: this.props.person.data.person_id,
        facu_id: this.state.facu_id,
      }).then((res)=>{
        this.handleClose();
      });

  }
  componentDidMount() {
    axios.get("/api/roles").then((res) => {
      this.setState({ roles: res.data.data });
    });
    axios.get("/api/countries").then((res) => {
      this.setState({ countries: res.data.data });
    });
    axios.get("/api/appointments").then((res) => {
      this.setState({ appointments: res.data.data });
    });
    axios.get("/api/faculties").then((res) => {
      this.setState({ faculties: res.data.data });
    });
    axios.get("/api/tills?till_type=3").then((res) => {
      this.setState({ tills: res.data.data });
    });
    axios.get("/api/contactTypes").then((response) => {
      this.setState({
        contactType: response.data.data,
      });
    });

    if (this.props.edit) {
      const {
        person_fname,
        person_lastname,
        homecity_id,
        birthplace_id,
        country_id,
        till_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        person_photo,
        country_name,
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
        person_id,
        appoin_description,
        office_desc,
        number_of_children,
        civil_status
      } = this.props.person.data;
      if (emplo_status) {
        this.setState({ emplo_status2: "Activo" });
      } else {
        this.setState({ emplo_status2: "Inactivo" });
      }
      axios
      .get(`/api/tills/${this.props.person.data.till_id}`)
      .then((res) => {
        this.setState({ till_name: res.data.data.till_name });
      });
      axios
        .get(`/api/contactPersons-per/${this.props.person.data.person_id}`)
        .then((res) => {
          var cont = res.data.data;
          var values = [];
          if (res.data != false) {
            cont.forEach((item) => {
              values.push({
                cntper_id: item.cntper_id,
                cnttype_id: item.cnttype_id,
                cnttype_name: item.cnttype_name,
                cntper_data: item.cntper_data,
              });
            });
            this.setState({
              contacts: values,
            });
          } else {
            this.setState({ contactsEdit: this.state.contacts });
          }
        });
      axios.get("/api/cities/" + homecity_id).then((res) => {
        // console.log(res.data.data.city_name);
        this.setState({
          homecity_name: res.data.data.city_name,
          hc_country_id: res.data.data.country_id,
          hc_country_name: res.data.data.country_name,
          hc_depart_id: res.data.data.depart_id,
          hc_depart_name: res.data.data.depart_name,
        });

        axios
          .get(`/api/departments/select/${res.data.data.country_id}`)
          .then((res) => {
            this.setState({ hc_departments: res.data });
          });
        axios
          .get(`/api/cities/select/${res.data.data.depart_id}`)
          .then((res) => {
            this.setState({ hc_cities: res.data });
          });
      });
      axios.get("/api/cities/" + birthplace_id).then((res) => {
        // console.log(res.data.data.city_name);
        this.setState({
          birthplace_name: res.data.data.city_name,
          bp_country_id: res.data.data.country_id,
          bp_country_name: res.data.data.country_name,
          bp_depart_id: res.data.data.depart_id,
          bp_depart_name: res.data.data.depart_name,
        });
        axios
          .get(`/api/departments/select/${res.data.data.country_id}`)
          .then((res) => {
            this.setState({ bp_departments: res.data });
          });
        axios
          .get(`/api/cities/select/${res.data.data.depart_id}`)
          .then((res) => {
            this.setState({ bp_cities: res.data });
          });
      });
      axios
        .get(`/api/employeeStaff/${this.props.person.data.person_id}`)
        .then((res) => {
          this.setState({
            staff_id: res.data.data.staff_id,
            facu_id: res.data.data.facu_id,
            facu_name: res.data.data.facu_name,
          });
        });
      axios.get(`/api/working_hours/${this.props.person.data.person_id}`)
      .then((res) => {
        this.setState({whours: res.data});
      });
      this.setState({
        person_fname,
        person_lastname,
        homecity_id,
        till_id,
        birthplace_id,
        country_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        country_name,
        person_photo,
        person_photo2: `/img/${this.props.person.data.person_photo}`,
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
        appoin_description,
        office_desc,
        number_of_children,
        civil_status,
        edit: true,
      });
    }
  }
  Country() {
    return this.state.countries.map((data) => ({
      label: data.country_name,
      value: data.country_id,
    }));
  }
  Faculties() {
    return this.state.faculties.map((data) => ({
      label: data.facu_name,
      value: data.facu_id,
    }));
  }
  Tills() {
    return this.state.tills.map((data) => ({
      label: data.till_name,
      value: data.till_id,
    }));
  }
  HcDepartments() {
    if (this.state.hc_departments.length > 0) {
      return this.state.hc_departments.map((data) => ({
        label: data.depart_name,
        value: data.depart_id,
      }));
    } else {
      return [];
    }
  }
  HcCities() {
    if (this.state.hc_cities.length > 0) {
      return this.state.hc_cities.map((data) => ({
        label: data.city_name,
        value: data.city_id,
      }));
    } else {
      return [];
    }
  }
  BpDepartments() {
    if (this.state.bp_departments.length > 0) {
      return this.state.bp_departments.map((data) => ({
        label: data.depart_name,
        value: data.depart_id,
      }));
    } else {
      return [];
    }
  }
  BpCities() {
    if (this.state.bp_cities.length > 0) {
      return this.state.bp_cities.map((data) => ({
        label: data.city_name,
        value: data.city_id,
      }));
    } else {
      return [];
    }
  }
  Schools() {
    return this.state.schools.map((data) => ({
      label: data.school_name,
      value: data.school_id,
    }));
  }
  Hstitles() {
    return this.state.hstitles.map((data) => ({
      label: data.hstitle_name,
      value: data.hstitle_id,
    }));
  }
  Appointment() {
    return this.state.appointments.map((data) => ({
      label: data.appoin_description,
      value: data.appoin_id,
    }));
  }
  selectValue1() {
    // axios.get('api/countries/'+this.state.country_id).then(res=>{
    //   this.setState({country_name: res.data.data});
    //   });
    return { label: this.state.country_name, value: this.state.country_id };
  }
  selectValue2() {
    return {
      label: this.state.hc_country_name,
      value: this.state.hc_country_id,
    };
  }
  selectValue3() {
    return { label: this.state.hc_depart_name, value: this.state.hc_depart_id };
  }
  selectValue4() {
    return { label: this.state.homecity_name, value: this.state.homecity_id };
  }
  selectValue5() {
    return {
      label: this.state.bp_country_name,
      value: this.state.bp_country_id,
    };
  }
  selectValue6() {
    return { label: this.state.bp_depart_name, value: this.state.bp_depart_id };
  }
  selectValue7() {
    return {
      label: this.state.birthplace_name,
      value: this.state.birthplace_id,
    };
  }
  selectValue8() {
    return { label: this.state.school_name, value: this.state.school_id };
  }
  selectValue9() {
    return { label: this.state.hstitle_name, value: this.state.hstitle_id };
  }
  selectValue10() {
    return {
      label: this.state.appoin_description,
      value: this.state.appoin_id,
    };
  }
  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }
  initialFile() {
    if (this.props.edit) {
      return [`/img/${this.props.person.data.person_photo}`];
    } else if (this.state.person_photo2) {
      // console.log("es correcto");
      return [this.state.person_photo2];
    } else {
      return [];
    }
  }
  returnImage() {
    if (this.state.person_photo) {
      // console.log(this.state.person_photo);
    } else {
      return false;
    }
  }
  editOrNew() {
    if (this.state.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit1}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit}
        >
          Guardar
        </Button>
      );
    }
  }

  handleAddFields() {
    if (this.state.edit) {
      var values = [...this.state.contacts];
      values.push({ cnttype_id: 0, cnttype_name: "", cntper_data: "" });
      this.setState({ contacts: values });
      var values2 = [...this.state.contactsEdit];
      values2.push({ cnttype_id: 0, cnttype_name: "", cntper_data: "" });
      this.setState({ contactsEdit: values2 });
    } else {
      var values = [...this.state.contacts];
      values.push({ cnttype_id: 0, cnttype_name: "", cntper_data: "" });
      this.setState({ contacts: values });
    }
  }

  handleRemoveFields(index) {
    if (this.edit.edit) {
      const values = [...this.state.contacts];
      this.setState({
        index: index,
        cntper_id: values[index].cntper_id,
      });
      this.handleOpen(values[index].cnttype_name);
    } else {
      var values = [...this.state.contacts];
      values.splice(index, 1);
      this.setState({ contacts: values });
    }
  }
  destroy() {
    // event.preventDefault();
    axios.delete(`/api/contactPersons/${this.state.cntper_id}`).then((res) => {
      var values = [...this.state.contacts];
      values.splice(this.state.index, 1);
      this.setState({
        snack_open: true,
        message_success: res.data.data,
        contacts: values,
      });
      this.handleAddFields();
    });
  }
  handleInputChange(index, event) {
    if (this.state.edit) {
      const values = [...this.state.contacts];
      const values2 = [...this.state.contactsEdit];
      var index2;
      if (values.length === values2.length) {
        index2 = index;
        this.setState({contactsEdited: false})
      } else if (values2.length != 0) {
        index2 = index - 1;
        this.setState({contactsEdited: true})
        if (event.target.name === "cntper_data") {
          // values[index].cntper_data = event.target.value;
          values2[index2].cntper_data = event.target.value;
        } else if (event.target.name === "cnttype_id") {
          // values[index].cnttype_id = event.target.value;
          values2[index2].cnttype_id = event.target.value;
        } else if (event.target.name === "cnttype_name") {
          values2[index2].cnttype_name = event.target.value;
          // values[index].cnttype_name = event.target.value;
        }
      }
      if (event.target.name === "cntper_data") {
        values[index].cntper_data = event.target.value;
        // values2[index2].cntper_data = event.target.value;
      } else if (event.target.name === "cnttype_id") {
        values[index].cnttype_id = event.target.value;
        // values2[index2].cnttype_id = event.target.value;
      } else if (event.target.name === "cnttype_name") {
        // values2[index2].cnttype_name = event.target.value;
        values[index].cnttype_name = event.target.value;
      }

      this.setState({
        contacts: values,
        contactsEdit: values2,
      });
    } else {
      const values = [...this.state.contacts];
      if (event.target.name === "cntper_data") {
        values[index].cntper_data = event.target.value;
      } else if (event.target.name === "cnttype_id") {
        values[index].cnttype_id = event.target.value;
      } else if (event.target.name === "cnttype_name") {
        values[index].cnttype_name = event.target.value;
      }
      this.setState({ contacts: values });
    }
  }

  selectContacType() {
    return this.state.contactType.map((data) => ({
      label: data.cnttype_name,
      value: data.cnttype_id,
    }));
  }
  Roles() {
    return this.state.roles.map((data) => ({
      label: data.role_name,
      value: data.role_id,
    }));
  }

  onChangeContactType(e, values, index) {
    if (this.state.edit) {
      const value = [...this.state.contacts];
      const value2 = [...this.state.contactsEdit];
      var index2;
      if (value.length === value2.length) {
        this.setState({contactsEdited:false})
        index2 = index;
      } else if (value2.length != 0) {
        index2 = index - 1;
        this.setState({contactsEdited:true})
        value2[index2].cnttype_id = values.value;
        value2[index2].cnttype_name = values.label;
      }
      value[index].cnttype_id = values.value;
      // value2[index2].cnttype_id = values.value;
      value[index].cnttype_name = values.label;
      // value2[index2].cnttype_name = values.label;
      this.setState({
        contacts: value,
        contactsEdit: value2,
      });
    } else {
      const value = [...this.state.contacts];
      value[index].cnttype_id = values.value;
      value[index].cnttype_name = values.label;
      this.setState({ contacts: value });
    }
  }
  SearchTill(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    this.setState({till_name: e.target.value})
    // if (this.state.time_out_till == false) {
      this.setState({ time_out_till: true });

      // setTimeout(() => {
        var url = ''
        if (e.target.value === "") {
          url = `/api/tills`

          this.setState({
            time_out_till: false,
            url: url,
          });
          axios.get(`${url}`).then((res) => {
            this.setState({
              tills: res.data.data,
              time_out_till: false,
            });
          }).catch((error) => {
            this.setState({time_out_till: false });
            console.log("Ingrese un valor valido");
          });

          } else {
          url = `/api/tills-search?till_type=3&till_name=${e.target.value}`
          this.setState({
            url: url,
          });
          axios.get(`${url}`).then((res) => {
              this.setState({
                tills: res.data.data,
                time_out_till: false,
              });
            }).catch((error) => {
          till[index].time_out_till = false;
              this.setState({time_out_till: false,
              });
              console.log("Ingrese un valor valido");
            });
        }
      // }, 3000);
    // }
  }
  searchPerson(even) {
    var e = even;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out_till == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        axios
          .get(
            `/api/employees-search?filter[person_idnumber]=${this.state.person_idnumber}`
          )
          .then((res) => {
            if (res.data != false) {
              var emplo_status2;
              axios
                .get(`/api/employeeStaff/${res.data.data[0].person_id}`)
                .then((res) => {
                  this.setState({
                    facu_id: res.data.data.facu_id,
                    facu_name: res.data.data.facu_name,
                  });
                });
              axios
                .get(`/api/countries/${res.data.data[0].country_id}`)
                .then((res) => {
                  this.setState({
                    country_name: res.data.data.country_name,
                  });
                });
              axios
                .get(`/api/cities/${res.data.data[0].homecity_id}`)
                .then((res) => {
                  this.setState({
                    homecity_name: res.data.data.city_name,
                    hc_country_id: res.data.data.country_id,
                    hc_country_name: res.data.data.country_name,
                    hc_depart_id: res.data.data.depart_id,
                    hc_depart_name: res.data.data.depart_name,
                  });
                });
              axios
                .get(`/api/cities/${res.data.data[0].birthplace_id}`)
                .then((res) => {
                  this.setState({
                    birthplace_name: res.data.data.city_name,
                    bp_country_id: res.data.data.country_id,
                    bp_country_name: res.data.data.country_name,
                    bp_depart_id: res.data.data.depart_id,
                    bp_depart_name: res.data.data.depart_name,
                  });
                });
              axios
                .get(`/api/appointments/${res.data.data[0].app_appointment_id}`)
                .then((res) => {
                  this.setState({
                    appoin_description: res.data.data.appoin_description,
                  });
                });
              axios
                .get(`/api/contactPersons-per/${res.data.data[0].person_id}`)
                .then((res) => {
                  var cont = res.data.data;
                  var values = [];
                  if (cont.length != 0) {
                    cont.forEach((item) => {
                      values.push({
                        cntper_id: item.cntper_id,
                        cnttype_id: item.cnttype_id,
                        cnttype_name: item.cnttype_name,
                        cntper_data: item.cntper_data,
                      });
                    });
                    this.setState({
                      contacts: values,
                    });
                  } else {
                    this.setState({ contactsEdit: this.state.contacts });
                  }
                });
              if (res.data.data[0].emplo_status) {
                emplo_status2 = "Activo";
              } else {
                emplo_status2 = "Inactivo";
              }
              this.setState({
                person_id: res.data.data[0].person_id,
                person_fname: res.data.data[0].person_fname,
                person_lastname: res.data.data[0].person_lastname,
                person_idnumber: res.data.data[0].person_idnumber,
                person_business_name: res.data.data[0].person_business_name,
                person_birthdate: res.data.data[0].person_birthdate,
                person_ruc: res.data.data[0].person_ruc,
                person_address: res.data.data[0].person_address,
                person_bloodtype: res.data.data[0].person_bloodtype,
                person_gender: res.data.data[0].person_gender,
                person_photo: res.data.data[0].person_photo,
                person_photo2: `/img/${res.data.data[0].person_photo}`,
                country_id: res.data.data[0].country_id,
                homecity_id: res.data.data[0].homecity_id,
                birthplace_id: res.data.data[0].birthplace_id,
                emplo_salary: res.data.data[0].emplo_salary,
                emplo_observation: res.data.data[0].emplo_observation,
                emplo_status: res.data.data[0].emplo_status,
                emplo_status2: emplo_status2,
                emplo_startdate: res.data.data[0].emplo_startdate,
                app_appointment_id: res.data.data[0].app_appointment_id,
                time_out: false,
                edit: true,
              });
            } else {
              this.setState({
                time_out: false,
                edit: false,
                cntper_id: 0,
                index: 0,

              });
            }
          });
      }, 3000);
    }
  }
  getDayLetter(day){
    var array_days = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    return array_days[day];
  }
  renderWorkingHoursList(){
    var {whours} = this.state;
    return(whours.map((row,index)=>{
      if(!row.deleted){
        return(
          <tr key={index}>
            <td>{index+1}</td>
            <td className="monitoring-text">
              {row.saved ? this.getDayLetter(row.wh_day) : (
                <FormControl
                  variant="outlined"
                  size="small"
                  style={{ width: '100%' }}
                  inputlabelprops={{
                    shrink: true,
                  }}
                >
                  <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: 'white', padding: '0 5px'}} shrink={true}>Dia</InputLabel>
                  <Select
                    native
                    label="Dia"
                    value={row.wh_day}
                    inputProps={{
                      name:"Dia",
                      id: "demo-simple-select-helper",
                    }}
                    onChange={(e)=>{
                      var whours_temp = this.state.whours;
                      whours_temp[index].wh_day = e.target.value;
                      this.setState({whours: whours_temp});
                    }}
                  >
                    <option value={0}>{"Domingo"}</option>
                    <option value={1}>{"Lunes"}</option>
                    <option value={2}>{"Martes"}</option>
                    <option value={3}>{"Miercoles"}</option>
                    <option value={4}>{"Jueves"}</option>
                    <option value={5}>{"Viernes"}</option>
                    <option value={6}>{"Sabado"}</option>
                  </Select>
                </FormControl>
              )}
            </td>
            <td>
                <TextField
                  id="outlined-basic"
                  fullWidth
                  label="Hora entrada"
                  size="small"
                  className="textField"
                  type="time"
                  name="wh_start"
                  value={row.wh_start}
                  onChange={(e)=>{
                    var whours_temp = this.state.whours;
                    whours_temp[index].wh_start = e.target.value;
                    this.setState({whours: whours_temp});
                  }}
                  margin="normal"
                  variant="outlined"
                />
            </td>
            <td>
                <TextField
                  id="outlined-basic"
                  fullWidth
                  label="Hora salida"
                  size="small"
                  className="textField"
                  type="time"
                  name="wh_end"
                  value={row.wh_end}
                  onChange={(e)=>{
                    var whours_temp = this.state.whours;
                    whours_temp[index].wh_end = e.target.value;
                    this.setState({whours: whours_temp});
                  }}
                  margin="normal"
                  variant="outlined"
                />
            </td>
            <td align="center">
              <Tooltip title="Quitar fila">
                <Fab
                  color="secondary"
                  size="small"
                  onClick={() => {
                    var whours_temp = this.state.whours;
                    if(whours_temp[index].deleted === false && whours_temp[index].saved === true){
                      whours_temp[index].deleted = 'true';
                    }else{
                      whours_temp.splice(index, 1);
                    }
                    this.setState({whours: whours_temp});
                  }}
                >
                  <RemoveIcon />
                </Fab>
              </Tooltip>
            </td>
          </tr>
        )
      }
      
    }))
  }
  getStepContent(stepIndex) {
    const {
      open,
      person_fname,
      person_lastname,
      person_idnumber,
      dropzone,
      person_birthdate,
      person_gender,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
      emplo_observation,
      emplo_status,
      emplo_status2,
      emplo_salary,
      emplo_startdate,
      office_desc,
      number_of_children,
      civil_status
    } = this.state;
    switch (stepIndex) {
      case 0:
        return (
          <div>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    fullWidth
                    label="Nombres"
                    className="textField"
                    name="person_fname"
                    style={{ margin: 8 }}
                    value={person_fname}
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>

                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    fullWidth
                    label="Apellidos"
                    className="textField"
                    style={{ margin: 8 }}
                    value={person_lastname}
                    name="person_lastname"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>

                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    fullWidth
                    label="Cedula"
                    className="textField"
                    style={{ margin: 8 }}
                    value={person_idnumber}
                    name="person_idnumber"
                    onChange={(event) => {
                      this.fieldsChange(event), this.searchPerson(event);
                    }}
                    margin="normal"
                    variant="outlined"
                    InputProps={{
                      endAdornment: (
                        <React.Fragment>
                          {this.state.time_out === true ? (
                            <CircularProgress color="inherit" size={20} />
                          ) : null}
                        </React.Fragment>
                      ),
                    }}
                  />
                </Grid>
              </Grid>

              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Fecha de Nacimiento"
                    fullWidth
                    type="date"
                    className="textField"
                    style={{ margin: 8 }}
                    value={person_birthdate}
                    name="person_birthdate"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    fullWidth
                    label="Razon social"
                    className="textField"
                    style={{ margin: 8 }}
                    value={person_business_name}
                    name="person_business_name"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>

                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="RUC"
                    fullWidth
                    className="textField"
                    style={{ margin: 8 }}
                    value={person_ruc}
                    name="person_ruc"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
              </Grid>

              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      { value: "A+", label: "A+" },
                      { value: "B+", label: "B+" },
                      { value: "AB+", label: "AB+" },
                      { value: "0+", label: "0+" },
                      { value: "0-", label: "0-" },
                      { value: "A-", label: "A-" },
                      { value: "B-", label: "B-" },
                      { value: "AB-", label: "AB-" },
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "person_bloodtype",
                        "person_bloodtype"
                      )
                    }
                    value={this.selectValue(
                      "person_bloodtype",
                      "person_bloodtype"
                    )}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Tipo de sangre *"
                        error={this.state.validator.person_bloodtype.error}
                        helperText={
                          this.state.validator.person_bloodtype.message
                        }
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>

                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      { value: "Masculino", label: "Masculino" },
                      { value: "Femenino", label: "Femenino" },
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "person_gender",
                        "person_gender"
                      )
                    }
                    value={this.selectValue("person_gender", "person_gender")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Sexo *"
                        error={this.state.validator.person_gender.error}
                        helperText={this.state.validator.person_gender.message}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Oficina"
                    fullWidth
                    className="textField"
                    style={{ margin: 8 }}
                    value={office_desc}
                    name="office_desc"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Cantidad de hijos"
                    fullWidth
                    type="number"
                    className="textField"
                    style={{ margin: 8 }}
                    value={number_of_children}
                    name="number_of_children"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Estado civil"
                    fullWidth
                    className="textField"
                    style={{ margin: 8 }}
                    value={civil_status}
                    name="civil_status"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Grid>
          </div>
        );
      case 1:
        return (
          <div>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <Autocomplete
                  id="combo-box-demo"
                  disableClearable
                  options={this.Country()}
                  getOptionLabel={(option) => option.label}
                  onChange={(e, values) =>
                    this.selectsChange(e, values, "country_id", "country_name")
                  }
                  value={this.selectValue("country_name", "country_id")}
                  getOptionSelected={(option, value) => {
                    if (value === option.value) {
                      return option.label;
                    } else {
                      return false;
                    }
                  }}
                  style={{ margin: 8 }}
                  fullWidth
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Pais de Nacionalidad *"
                      error={this.state.validator.country_id.error}
                      helperText={this.state.validator.country_id.message}
                      className="textField"
                      variant="outlined"
                    />
                  )}
                />
              </Grid>
              <Grid container item xs={12} spacing={3}>
                
                <p className="divisor">Seleccione la ciudad de residencia</p>

                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.Country()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "hc_country_id",
                        "hc_country_name",
                        "departments",
                        "hc_departments"
                      )
                    }
                    value={this.selectValue("hc_country_name", "hc_country_id")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Pais *"
                        error={this.state.validator.homecity_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.HcDepartments()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "hc_depart_id",
                        "hc_depart_name",
                        "cities",
                        "hc_cities"
                      )
                    }
                    value={this.selectValue("hc_depart_name", "hc_depart_id")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Departamento *"
                        error={this.state.validator.homecity_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.HcCities()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "homecity_id",
                        "homecity_name"
                      )
                    }
                    value={this.selectValue("homecity_name", "homecity_id")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Ciudad *"
                        error={this.state.validator.homecity_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
              </Grid>

              <Grid container item xs={12} spacing={3}>
                <p className="divisor">Seleccione la ciudad de origen</p>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.Country()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "bp_country_id",
                        "bp_country_name",
                        "departments",
                        "bp_departments"
                      )
                    }
                    value={this.selectValue("bp_country_name", "bp_country_id")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Pais *"
                        error={this.state.validator.birthplace_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.BpDepartments()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "bp_depart_id",
                        "bp_depart_name",
                        "cities",
                        "bp_cities"
                      )
                    }
                    value={this.selectValue("bp_depart_name", "bp_depart_id")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Departamento *"
                        error={this.state.validator.birthplace_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.BpCities()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "birthplace_id",
                        "birthplace_name"
                      )
                    }
                    value={this.selectValue("birthplace_name", "birthplace_id")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Ciudad *"
                        error={this.state.validator.birthplace_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={3}>
                  <TextField
                    id="outlined-basic"
                    label="Direccion"
                    fullWidth
                    className="textField"
                    style={{ margin: 8 }}
                    value={person_address}
                    name="person_address"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
            </Grid>
          </div>
        );
      case 2:
        return (
          <div>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.Appointment()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "app_appointment_id",
                        "appoin_description"
                      )
                    }
                    value={this.selectValue(
                      "appoin_description",
                      "app_appointment_id"
                    )}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Cargo *"
                        error={this.state.validator.app_appointment_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Fecha de Inicio"
                    type="date"
                    fullWidth
                    className="textField"
                    style={{ margin: 8 }}
                    value={emplo_startdate}
                    name="emplo_startdate"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                  />
                </Grid>
                {this.props.permissions.employee_account_store &&
                  <Grid item xs={4}>
                    <TextField
                      id="outlined-basic"
                      label="Salario"
                      fullWidth
                      type="number"
                      className="textField"
                      style={{ margin: 8 }}
                      value={emplo_salary}
                      name="emplo_salary"
                      onChange={this.fieldsChange.bind(this)}
                      margin="normal"
                      variant="outlined"
                    />
                  </Grid>
                }
              </Grid>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      { value: true, label: "Activo" },
                      { value: false, label: "Inactivo" },
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "emplo_status",
                        "emplo_status2"
                      )
                    }
                    value={this.selectValue("emplo_status2", "emplo_status")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Status *"
                        error={this.state.validator.emplo_status.error}
                        helperText={this.state.validator.emplo_status.message}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Observaciones"
                    className="textField"
                    multiline={true}
                    fullWidth
                    rows="3"
                    style={{ margin: 8 }}
                    value={emplo_observation}
                    name="emplo_observation"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4}>
                  <Button onClick={this.openDropzone.bind(this)}>
                    Foto de Perfil
                  </Button>
                  <DropzoneDialog
                    dialogTitle="Cargar imagen"
                    open={dropzone}
                    acceptedFiles={["image/*"]}
                    initialFiles={this.initialFile()}
                    onSave={this.fieldChange13.bind(this)}
                    dropzoneText="Arrastre la imagen aqui o haga click para selecionar una foto"
                    showAlerts={false}
                    filesLimit={1}
                    onClose={this.closeDropzone.bind(this)}
                    cancelButtonText="Cancelar"
                    submitButtonText="Enviar"
                  />
                  <img
                    className="image-profile"
                    src={this.state.person_photo2}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={6}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={this.Faculties()}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(e, values, "facu_id", "facu_name")
                    }
                    value={this.selectValue("facu_name", "facu_id")}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Facultad *"
                        // error={this.state.validator.emplo_status.error}
                        // helperText={this.state.validator.emplo_status.message}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                {this.props.permissions.employee_account_store &&
                  <Grid item xs={6}>
                    <Autocomplete
                      id="combo-box-demo"
                      disableClearable
                      options={this.Tills()}
                      getOptionLabel={(option) => option.label}
                      onChange={(e, values) =>
                        this.selectsChange(e, values, "till_id", "till_name")
                      }
                      value={this.selectValue("till_name", "till_id")}
                      getOptionSelected={(option, value) => {
                        if (value === option.value) {
                          return option.label;
                        } else {
                          return false;
                        }
                      }}
                      style={{ margin: 8 }}
                      fullWidth
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Caja *"
                          onKeyDown={(e) =>{
                            if(e.key === 'Enter'){
                              e.preventDefault();
                              this.SearchTill(e)
                            }
                          }}
                          className="textField"
                          variant="outlined"
                          InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                              <React.Fragment>
                                {this.state.time_out_till === true ? (
                                  <CircularProgress color="inherit" size={20} />
                                ) : null}
                                {params.InputProps.endAdornment}
                              </React.Fragment>
                            ),
                          }}
                        />
                      )}
                    />
                  </Grid>
                }
              </Grid>
              <Grid container item xs={12} spacing={3}>
                <table className="table_monitoring" style={{marginBottom: 20}}>
                  <thead>
                    <tr>
                      <th className="monitoring-th">#</th>
                      <th className="monitoring-th">Dia</th>
                      <th className="monitoring-th">Hora entrada</th>
                      <th className="monitoring-th">Hora salida</th>
                      <th className="monitoring-th">
                        <Tooltip title="Agregar">
                          <Fab
                            color="primary"
                            size="small"
                            onClick={() => {
                              var whours_temp = this.state.whours;
                              whours_temp.push({
                                saved: false,
                                wh_day: '1',
                                wh_start: '00:00',
                                wh_end: '00:00'
                              });
                              this.setState({whours: whours_temp});
                            }}
                          >
                            <AddIcon />
                          </Fab>
                        </Tooltip>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                      {this.renderWorkingHoursList()}
                  </tbody>
                </table>
              </Grid>
            </Grid>
          </div>
        );
      case 3:
        return (
          <div>
            <Grid container item xs={12} spacing={1}>
              <Grid item xs={12} style={{ textAlign: "center" }}>
                <h3>Contactos</h3>
                <hr />
              </Grid>
            </Grid>

            {this.state.contacts.map((inputField, index) => (
              <Fragment key={`${inputField}~${index}`}>
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={4} style={{ textAlign: "center" }}>
                    <Autocomplete
                      id={index}
                      name="cnttype_id"
                      disableClearable
                      options={this.selectContacType()}
                      filterSelectedOptions
                      getOptionLabel={(option) => option.label}
                      onChange={(e, values) => {
                        this.onChangeContactType(e, values, index);
                      }}
                      value={{
                        value: inputField.cnttype_id,
                        label: inputField.cnttype_name,
                      }}
                      getOptionSelected={(option, value) =>
                        option.value === value.value
                      }
                      style={{ margin: 8 }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          required
                          label="Tipo de contacto "
                          variant="outlined"
                          size="small"
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={6} style={{ textAlign: "center" }}>
                    <TextField
                      variant="outlined"
                      required
                      className="email"
                      size="small"
                      id="cntper_data"
                      name="cntper_data"
                      value={inputField.cntper_data}
                      onChange={(event) => this.handleInputChange(index, event)}
                      label="Dato "
                      style={{ margin: 8 }}
                      fullWidth
                      margin="normal"
                      InputProps={{
                        endAdornment: index + 1 <=
                          this.state.contacts.length && (
                          <InputAdornment position="start">
                            <Tooltip title="Agregar Contacto">
                              <Fab
                                color="primary"
                                size="small"
                                onClick={() => this.handleAddFields()}
                              >
                                <AddIcon />
                              </Fab>
                            </Tooltip>
                            <Tooltip title="Eliminar Contacto">
                              <Fab
                                color="secondary"
                                size="small"
                                onClick={() => this.handleRemoveFields(index)}
                              >
                                <RemoveIcon />
                              </Fab>
                            </Tooltip>
                          </InputAdornment>
                        ),
                      }}
                    />
                  </Grid>
                </Grid>
              </Fragment>
            ))}
          </div>
        );
      case 4:
        return (
          <div>
            <Autocomplete
              id="combo-box-demo"
              disableClearable
              options={[
                this.selectValue("role_name", "role_id"),
                ...this.Roles(),
              ]}
              filterSelectedOptions
              getOptionLabel={(option) => option.label}
              onChange={(e, values) => {
                this.selectsChange(e, values, "role_id", "role_name");
              }}
              value={this.selectValue("role_name", "role_id")}
              loading={this.state.role_time_out}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Seleccione un Rol *"
                  variant="outlined"
                  fullWidth
                  onChange={this.searchFieldChange}
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {this.state.role_time_out === true ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
            <TextField
              id="outlined-full-width"
              label="Correo"
              style={{ margin: 8 }}
              name="email"
              value={this.state.email}
              onChange={this.fieldsChange.bind(this)}
              fullWidth
              margin="normal"
              variant="outlined"
            />
          </div>
        );
      default:
        return "Unknown stepIndex";
    }
  }
  render() {
    var steps = [];
    if (this.state.edit) {
      steps = ["Datos Personales", "Vivienda", "Datos Laborales", "Contactos"];
    } else {
      steps = [
        "Datos Personales",
        "Vivienda",
        "Datos Laborales",
        "Contactos",
        "Usuarios",
      ];
    }
    const {
      open,
      person_fname,
      person_lastname,
      person_idnumber,
      dropzone,
      person_birthdate,
      person_gender,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
      office_desc,
      number_of_children,
      civil_status,
      validator,
      snack_open,
    } = this.state;
    var showSnack;

    var showDialogDestroy;
    var showSnackDestoy;
    if (this.state.openAlert) {
      showDialogDestroy = (
        <DialogDestroy
          index={`${this.state.cnttype_name}`}
          onClose={this.handleCloseAlert}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (this.state.snack_openDestroy) {
      showSnackDestoy = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={this.state.snack_openDestroy}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {" "}
            {this.state.message_success}{" "}
          </Alert>{" "}
        </Snackbar>
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["app_appointment_id"]["error"] ? (
              <li>Error: {validator["app_appointment_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_status"]["error"] ? (
              <li>Error: {validator["person_status"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_fname"]["error"] ? (
              <li>Error: {validator["person_fname"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_lastname"]["error"] ? (
              <li>Error: {validator["person_lastname"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["homecity_id"]["error"] ? (
              <li>Error: {validator["homecity_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["birthplace_id"]["error"] ? (
              <li>Error: {validator["birthplace_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["country_id"]["error"] ? (
              <li>Error: {validator["country_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_birthdate"]["error"] ? (
              <li>Error: {validator["person_birthdate"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_idnumber"]["error"] ? (
              <li>Error: {validator["person_idnumber"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_address"]["error"] ? (
              <li>Error: {validator["person_address"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_bloodtype"]["error"] ? (
              <li>Error: {validator["person_bloodtype"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_business_name"]["error"] ? (
              <li>Error: {validator["person_business_name"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_ruc"]["error"] ? (
              <li>Error: {validator["person_ruc"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }

    return (
      <div>

        {/*
          <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className="modal"
            open={open}
            onClose={this.handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
              timeout: 500,
            }}
          >
           <Fade in={open}>
               <div className="paper">
               */}
        <h3 align="center">Formulario de Funcionarios</h3>
        <Divider />
        <Stepper activeStep={this.state.activeStep} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {this.state.activeStep === steps.length ? (
            <div>
              All steps completed
              <Button onClick={this.handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              {this.getStepContent(this.state.activeStep)}
              <div>
                <Button
                  disabled={this.state.activeStep === 0}
                  onClick={this.handleBack}
                  className="backButton"
                  variant="contained"
                >
                  <NavigateBeforeIcon />
                  Atras
                </Button>

                {this.state.activeStep === steps.length - 1 ? (
                  this.editOrNew()
                ) : (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.handleNext}
                  >
                    Continuar
                    <NavigateNextIcon />
                  </Button>
                )}
                {showSnack}

                <Button
                  variant="outlined"
                  color="secondary"
                  startIcon={<ExitToAppIcon />}
                  onClick={this.handleClose}
                >
                  Cerrar
                </Button>
              </div>
            </div>
          )}
        </div>
        {showSnackDestoy}
      </div>
    );
  }
}
export default ModalForm;
