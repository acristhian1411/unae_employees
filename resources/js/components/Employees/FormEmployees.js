import React, { Component } from "react";
import {
  Modal,
  Grid,
  Backdrop,
  Fade,
  Button,
  TextField,
  Stepper,
  Step,
  StepLabel,
  Divider,
  TextareaAutosize,
  Snackbar,
} from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { DropzoneArea, DropzoneDialog } from "material-ui-dropzone";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import { Autocomplete, Alert } from "@material-ui/lab";

class FormEmployees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      open: true,
      edit: true,
      activeStep: 0,
      emplo_startdate: null,
      emplo_salary: 0,
      emplo_observation: "",
      emplo_status: false,
      emplo_status2: "",
      app_appointment_id: 0,
      appoin_description: "",
      appointments: [],
      facu_id: 0,
      facu_name: "",
      faculties: [],
      sucess: false,
      error: false,
      imagePreviewUrl: false,
      dropzone: false,
      countries: [],
      validator: {
        emplo_startdate: { message: "", error: false },
        emplo_salary: { message: "", error: false },
        emplo_observation: { message: "", error: false },
        emplo_status: { message: "", error: false },
        app_appointment_id: { message: "", error: false },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.openDropzone = this.openDropzone.bind(this);
    this.closeDropzone = this.closeDropzone.bind(this);
    this.fieldsChange = this.fieldsChange.bind(this); //.person_fname, .person_lastname, .person_birthdate
    this.fieldsChangeInteger = this.fieldsChangeInteger.bind(this); //.person_fname, .person_lastname, .person_birthdate
    this.selectsChange = this.selectsChange.bind(this); // homecity_id:0,
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.handleUpdateObject = this.handleUpdateObject.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.selectValue = this.selectValue.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  handleNext() {
    this.setState({ activeStep: this.state.activeStep + 1 });
  }

  handleBack() {
    this.setState({ activeStep: this.state.activeStep - 1 });
  }

  handleReset() {
    this.setState({ activeStep: 0 });
  }

  handleOpen() {
    this.setState({ open: true });
  }
  openDropzone() {
    this.initialFile();
    this.setState({ dropzone: true });
  }
  closeDropzone() {
    this.setState({ dropzone: false });
  }
  handleClose() {
    this.setState({ open: false });
    this.props.onHandleSubmit();
  }
  fieldsChange(e) {
    e.preventDefault();
    // console.log(e.target.name);
    this.setState({ [e.target.name]: e.target.value });
  }
  fieldsChangeInteger(e) {
    e.preventDefault();
    // console.log(e.target.name);
    this.setState({ [e.target.name]: e.target.value });
  }
  selectsChange(e, values, nameValue, nameLabel, route, obj) {
    // console.log(name);
    // console.log(nameValue+' '+nameLabel);
    // console.log(e);
    // console.log(values);
    if (route) {
      axios.get("api/" + route + "/select/" + values.value).then((res) => {
        this.setState({ [obj]: res.data });
      });
    }
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    // this.props.changeAtribute(true);
  }
  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }

  handleCreateObject(e) {
    const {
      app_appointment_id,
      emplo_startdate,
      emplo_status,
      emplo_observation,
      emplo_salary,
    } = this.state;
    e.preventDefault();
    axios
      .post("/api/employees", {
        person_id: this.props.person_id,
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
      })
      .then((res) => {
        axios.post("/api/staffs", {
          person_id: this.props.person_id,
          facu_id: this.state.facu_id,
        });
        this.setState({
          emplo_startdate: null,
          emplo_salary: 0,
          emplo_observation: "",
          emplo_status: false,
          emplo_status2: "",
          app_appointment_id: 0,
          appoin_description: "",
        });
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  handleUpdateObject(e) {
    e.preventDefault();
    const {
      app_appointment_id,
      emplo_startdate,
      emplo_status,
      emplo_observation,
      emplo_salary,
    } = this.state;
    axios
      .put(`/api/employees-person/${this.props.person_id}`, {
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
      })
      .then((res) => {
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  componentDidMount() {
    axios.get("/api/appointments").then((res) => {
      this.setState({ appointments: res.data.data });
    });
    axios.get("/api/faculties").then((res) => {
      this.setState({ faculties: res.data.data });
    });
    // console.log(this.props);
    if (this.props.edit) {
      const {
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
        appoin_description,
      } = this.props.person.data;
      this.setState({
        app_appointment_id,
        emplo_startdate,
        emplo_status,
        emplo_observation,
        emplo_salary,
        appoin_description,
      });
    }
  }

  Appointment() {
    return this.state.appointments.map((data) => ({
      label: data.appoin_description,
      value: data.appoin_id,
    }));
  }
  Faculties() {
    return this.state.faculties.map((data) => ({
      label: data.facu_name,
      value: data.facu_id,
    }));
  }
  editOrNew() {
    //  if (this.props.edit) {
    if (this.props.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleUpdateObject}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
      );
    }
  }

  render() {
    const { validator, snack_open } = this.state;
    var showSnack;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["person_startdate"]["error"] ? (
              <li>Error: {validator["person_startdate"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_salary"]["error"] ? (
              <li>Error: {validator["person_salary"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_observation"]["error"] ? (
              <li>Error: {validator["person_observation"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_status"]["error"] ? (
              <li>Error: {validator["person_status"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["app_appointment_id"]["error"] ? (
              <li>Error: {validator["app_appointment_id"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3 align="center">Formulario de Funcionarios</h3>
        <Divider />
        <Autocomplete
          id="combo-box-demo"
          disableClearable
          options={this.Appointment()}
          getOptionLabel={(option) => option.label}
          onChange={(e, values) =>
            this.selectsChange(
              e,
              values,
              "app_appointment_id",
              "appoin_description"
            )
          }
          value={this.selectValue("appoin_description", "app_appointment_id")}
          getOptionSelected={(option, value) => {
            if (value === option.value) {
              return option.label;
            } else {
              return false;
            }
          }}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => (
            <TextField
              {...params}
              label="Cargo *"
              error={this.state.validator.app_appointment_id.error}
              className="textField"
              variant="outlined"
            />
          )}
        />
        <Autocomplete
          id="combo-box-demo"
          disableClearable
          options={this.Faculties()}
          getOptionLabel={(option) => option.label}
          onChange={(e, values) =>
            this.selectsChange(e, values, "facu_id", "facu_name")
          }
          value={this.selectValue("facu_name", "facu_id")}
          getOptionSelected={(option, value) => {
            if (value === option.value) {
              return option.label;
            } else {
              return false;
            }
          }}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => (
            <TextField
              {...params}
              label="Facultad *"
              className="textField"
              variant="outlined"
            />
          )}
        />
        <Autocomplete
          id="combo-box-demo"
          disableClearable
          options={[
            { value: true, label: "ACTIVO" },
            { value: false, label: "INACTIVO" },
          ]}
          getOptionLabel={(option) => option.label}
          onChange={(e, values) =>
            this.selectsChange(e, values, "emplo_status", "emplo_status2")
          }
          value={this.selectValue("emplo_status2", "emplo_status")}
          getOptionSelected={(option, value) => {
            if (value === option.value) {
              return option.label;
            } else {
              return false;
            }
          }}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => (
            <TextField
              {...params}
              label="Status *"
              error={this.state.validator.emplo_status.error}
              helperText={this.state.validator.emplo_status.message}
              className="textField"
              variant="outlined"
            />
          )}
        />
        <TextField
          id="outlined-basic"
          label="Fecha de Inicio"
          type="date"
          fullWidth
          className="textField"
          style={{ margin: 8 }}
          value={this.state.emplo_startdate}
          name="emplo_startdate"
          onChange={this.fieldsChange.bind(this)}
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
        />
        <TextField
          id="outlined-basic"
          label="Salario"
          type="number"
          fullWidth
          className="textField"
          name="emplo_salary"
          style={{ margin: 8 }}
          value={this.state.emplo_salary}
          onChange={this.fieldsChangeInteger.bind(this)}
          margin="normal"
          variant="outlined"
        />
        <TextField
          id="outlined-basic"
          label="Observaciones"
          className="textField"
          multiline={true}
          fullWidth
          rows="3"
          style={{ margin: 8 }}
          value={this.state.emplo_observation}
          name="emplo_observation"
          onChange={this.fieldsChange.bind(this)}
          margin="normal"
          variant="outlined"
        />
        {this.editOrNew()}
        {showSnack}
        <Button
          variant="outlined"
          color="secondary"
          startIcon={<ExitToAppIcon />}
          onClick={this.handleClose}
        >
          Cerrar
        </Button>
      </div>
    );
  }
}
export default FormEmployees;
