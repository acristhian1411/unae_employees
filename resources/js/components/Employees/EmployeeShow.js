import React, { Component, lazy } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Tabs,
  Tab,
  Typography,
  List,
  ListItem,
  ListItemText,
  Divider,
} from "@material-ui/core";
import HomeIcon from '@material-ui/icons/Home';
import ListIcon from '@material-ui/icons/List';
import PersonIcon from '@material-ui/icons/Person';
// import DocPersons from "../DocPersons/DocPersons";
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import EmployeeComissionShow from "../EmployeeComission/EmployeeComissionShow";
import LaboralData from './LaboralData';
import ContactPersonsList from '../ContactPersons/ContactPersonsList';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  media: {
    height: 140,
  },
});

class EmployeeShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: {},
      value: 0,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    axios.get(`/api/employees/${id}`).then((response) => {
      this.setState({
        persons: response.data.data,
      });
    });
  }

  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes } = this.props;
    const { persons, value } = this.state;
    return (
      <div className={classes.root}>
        <div className="breadcrumb-container">
          <ul className="breadcrumb-list">
            <li className="breadcrumb-item">
              <Link className='breadcrumb-link'to={"/"}>
                  <HomeIcon className="breadcrumb-icon"/>
                  Inicio
                  <ChevronRightIcon className="breadcrumb-icon"/>
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link className='breadcrumb-link'to={"/funcionarios"}>
                  <ListIcon className="breadcrumb-icon"/>
                  Lista de funcionarios
                  <ChevronRightIcon className="breadcrumb-icon"/>
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Typography style={{color:"#666"}}>
                <PersonIcon className="breadcrumb-icon"/>
                {this.state.persons.person_idnumber}
              </Typography>
            </li>
          </ul>
        </div>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
          >
            <Tab label="Datos Personales" />
            <Tab label="Datos Laborales" />
            
            {
            persons.appoin_description === 'AGENTE DE INSCRIPCIÓN' && (
            <Tab label="Comision" />

            )
            }
            
          </Tabs>
        </AppBar>
        {value === 0 && (
          <TabContainer>
            <div className="card_person">
              <img
                className="profile_photo"
                src={`/img/${persons.person_photo}`}
              />
              <div className="card_label_field">
                <p className="title_column">Nombres </p>
                <p className="value_column">{persons.person_fname}</p>
                <p className="title_column">Apellidos </p>
                <p className="value_column">{persons.person_lastname}</p>
                <p className="title_column">Documento </p>
                <p className="value_column">{persons.person_idnumber}</p>
                <p className="title_column">Celular </p>
                <p className="value_column">----</p>
                <p className="title_column">Correo </p>
                <p className="value_column">----</p>
              </div>
            </div>
            <div className="card_person1">
                <div className="card_label_field">
                <ContactPersonsList cperson={this.props.match.params.id} />

                  {/* <p className="title_column">Situación Ocupacional </p>
                  <p className="value_column">---</p>
                  <p className="title_column">Estado Civil</p>
                  <p className="value_column">---</p> */}
                </div>
            </div>
            <div className="card_person1">
              <div className="card_label_field">
                <p className="title_column">Sexo </p>
                <p className="value_column">{persons.person_gender}</p>
                <p className="title_column">Pais de Nacionalidad </p>
                <p className="value_column">{persons.country_name}</p>
                <p className="title_column">Dirección </p>
                <p className="value_column">{persons.person_address}</p>
                <p className="title_column">Tipo de sangre </p>
                <p className="value_column">{persons.person_bloodtype}</p>
              </div>
            </div>
          </TabContainer>
        )}
        {value === 1 &&( 
          <TabContainer>
            <LaboralData persons={persons} />
          </TabContainer>
        )}
        
        {value === 3 && <TabContainer><EmployeeComissionShow person={persons.person_id} /></TabContainer>}
      </div>
    );
  }
}

EmployeeShow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EmployeeShow);
