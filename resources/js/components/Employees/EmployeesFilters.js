import React, { Component } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Checkbox, FormControlLabel,Button,TextField,Grid } from '@material-ui/core';
import { PictureAsPdf } from '@material-ui/icons';

const currentYear = new Date().getFullYear();

const meses = [
    { label: 'Enero', value: '01' },
    { label: 'Febrero', value: '02' },
    { label: 'Marzo', value: '03' },
    { label: 'Abril', value: '04' },
    { label: 'Mayo', value: '05' },
    { label: 'Junio', value: '06' },
    { label: 'Julio', value: '07' },
    { label: 'Agosto', value: '08' },
    { label: 'Septiembre', value: '09' },
    { label: 'Octubre', value: '10' },
    { label: 'Noviembre', value: '11' },
    { label: 'Diciembre', value: '12' },
  ];

class EmployeesFilters extends Component {
    constructor(props){
        super(props);
        this.state = {
            pagado: {label:'',value:''},
            mes: null,
            anio: currentYear,
            cajas:[],
            company_det:[],
            caja: {label:'',value:''},
            detail: {label:'',value:''},
            pagoParcial: null,
          };
        
          
          this.handlePagadoChange = (event,values) => {
            var caj 
            if(this.state.pagado != null){
              caj = this.state.pagado
            }else if(values == null || this.state.pagado == null){
              caj = {label:'',value:''}
            }
            caj.label = values != null ? values.label : ''
            caj.value = values != null ? values.value : ''
            this.setState({ pagado: caj });
        };
        this.handleGenerarReporte = (e)=>{
          if (this.state.anio && this.state.mes &&(this.state.mes.label != '')) {
            // Realizar la exportación
            console.log(this.state.mes)
            console.log(`Exportando para ${this.state.mes}-${this.state.anio}`);
            open(`/reportes/salarios-funcionarios?type=employees&
            filter[month]=${this.state.mes.value}&
            filter[till]=${this.state.caja.value}&
            filter[paid]=${this.state.pagado.value}&
            filter[comp_det]=${this.state.detail.value}&
            filter_label[comp_det]=${this.state.detail.label}&
            filter_label[paid]=${this.state.pagado.label}&
            filter[comp_det]=${this.state.detail.value}&
            filter_label[comp_det]=${this.state.detail.label}&
            filter[partial]=${this.state.pagoParcial}&
            filter_label[month]=${this.state.mes.label}&
            filter_label[till]=${this.state.caja.label}&
            filter[year]=${this.state.anio}`);
            console.log('esto es para generar reportes')
          } else {
            alert('Seleccione el mes y año antes de exportar');
          }
        }
          this.handleMesChange = (event,values) => {
            var caj 
            if(this.state.mes != null){
              caj = this.state.mes
            }else if(values == null || this.state.mes == null){
              caj = {label:'',value:''}
            }
            caj.label = values != null ? values.label : ''
            caj.value = values != null ? values.value : ''
            this.setState({ mes: caj });
          };
        
          this.handleAnioChange = (event) => {
            this.setState({ anio: event.target.value });
          };
        
          this.handleCajaChange = (event,values) => {
            var caj 
            if(this.state.caja != null){
              caj = this.state.caja
            }else if(values == null || this.state.caja == null){
              caj = {label:'',value:''}
            }
            caj.label = values != null ? values.label : ''
            caj.value = values != null ? values.value : ''
            this.setState({ caja: caj });
          };
          this.handleCompanyChange = (event,values) => {
            var caj 
            if(this.state.detail != null){
              caj = this.state.detail
            }else if(values == null || this.state.detail == null){
              caj = {label:'',value:''}
            }
            caj.label = values != null ? values.label : ''
            caj.value = values != null ? values.value : ''
            this.setState({ detail: caj });
          };
          this.handlePagoParcialChange = (event,values) => {
            this.setState({ pagoParcial: event.target.checked });
          };
    }

    componentDidMount(props){
      axios
      .get("/api/tills" )
      .then((response) => {
        console.log(response.data)
        this.setState({
          cajas: response.data.data
        });
      });
      axios
      .get("/api/companyDetails" )
      .then((response) => {
        console.log(response.data)
        this.setState({
          company_det: response.data.data
        });
      });
    }
tills() {
  return this.state.cajas.map((data) => ({
    label: data.till_name,
    value: data.till_id,
  }));
}
details() {
  return this.state.company_det.map((data) => ({
    label: data.comp_det_bussiness_name,
    value: data.comp_det_id,
  }));
}
  render() {
    const{anio,mes,caja,pagoParcial,pagado} = this.state;
    return (
      <div>
        {console.log(this.state.mes)}
        <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Autocomplete
            id="pagado-autocomplete"
            options={[
              { label: 'Pagado', value: 'true' },
              { label: 'No pagado', value: 'false' },
            ]}
            getOptionLabel={(option) => option.label}
            value={{label:this.state.pagado.label, value: this.state.pagado.value}}
            onChange={(e,values)=>{this.handlePagadoChange(e,values)}}
            style={{ width: '100%' }}
            renderInput={(params) => <TextField {...params} label="Pagado o no pagado" variant="outlined" />}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Autocomplete
            id="caja-autocomplete"
            options={this.tills()}
            getOptionLabel={(option) => option.label}
            value={{label:(this.state.caja != null ? this.state.caja.label : ''), value: (this.state.caja != null ? this.state.caja.value : '')}}
            onChange={(e,values)=>{this.handleCajaChange(e,values)}}
            style={{ width: '100%' }}
            renderInput={(params) => <TextField {...params} label="Caja" variant="outlined" />}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Autocomplete
            id="caja-autocomplete"
            options={this.details()}
            getOptionLabel={(option) => option.label}
            value={{label:(this.state.detail != null ? this.state.detail.label : ''), value: (this.state.detail != null ? this.state.detail.value : '')}}
            onChange={(e,values)=>{this.handleCompanyChange(e,values)}}
            style={{ width: '100%' }}
            renderInput={(params) => <TextField {...params} label="Entidad" variant="outlined" />}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Autocomplete
            id="mes-autocomplete"
            options={meses}
            getOptionLabel={(option) => option.label}
            style={{ width: '100%' }}
            value={{label:(this.state.mes != null ? this.state.mes.label : ''), value: (this.state.mes != null ? this.state.mes.value : '')}}
            onChange={(e,values)=>{this.handleMesChange(e,values)}}
            renderInput={(params) => <TextField {...params} label="Mes" variant="outlined" />}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <TextField
            id="anio-textfield"
            label="Año"
            variant="outlined"
            type="number"
            defaultValue={currentYear}
            value={anio}
            onChange={(e)=>(this.handleAnioChange(e))}
            InputProps={{ inputProps: { min: 1900, max: currentYear + 1 } }}
            fullWidth
          />
        </Grid>
        
        <Grid item xs={12} sm={4}>
          {this.state.pagado.value == 'false' && (
              <FormControlLabel
              control={<Checkbox 
                  checked={this.state.pagoParcial == null ? false : this.state.pagoParcial}
                  onChange={(e)=>{this.handlePagoParcialChange(e)}}
                  name="pago-parcial-checkbox" color="primary" />}
              label="Pago parcial"
              />
          )}
         
        </Grid>
        <Grid item xs={12} sm={12}>
          <Button 
            variant="contained" 
            color="primary" 
            startIcon={<PictureAsPdf/>}
            onClick={(e)=>(this.handleGenerarReporte(e))}
          >
            Exportar
          </Button>
        </Grid>
      </Grid>
      </div>
    );
  }
}

export default EmployeesFilters;
