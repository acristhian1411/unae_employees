import React,{Component} from 'react';
import {
  CircularProgress,
  Grid,
  TextField,
  Button,
  Snackbar,
  IconButton,
  Accordion,
	AccordionSummary,
	AccordionDetails,
	Checkbox,
	FormControlLabel 
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {format, disformat} from '../GlobalFunctions/Format';
export default class LaboralData extends Component{
	constructor(props){
		super(props);
		this.state = {
			snack_open: false,
      message_success: "",
			employ_detail: [],
			cuentas: [],
			add_emp_accounts: false,
			emp_account_desc: '',
			emp_account_amount: 0,
			emp_account_qty: 0,
			array_emp_account: [],
			employ_edit_salary: false,
		}
		this.updateArrayEmp = this.updateArrayEmp.bind(this);
		this.closeSnack = this.closeSnack.bind(this);
	}
	componentDidMount(){
		// console.log("componentDidMount");
		// console.log(this.props);
		axios.get(`/api/employees-laboral-data/${this.props.persons.person_id}`).then((res)=>{
			// console.log(res.data);
			this.setState({employ_detail: res.data.employ_detail, cuentas:res.data.cuentas})
		})
	}
  closeSnack() {
    this.setState({ snack_open: false });
  }
	 getMonth1(num) {
    var date = new Date(num);
    var num1 = date.getMonth();
    var month = new Array();
    month[0] = "ENERO";
    month[1] = "FEBRERO";
    month[2] = "MARZO";
    month[3] = "ABRIL";
    month[4] = "MAYO";
    month[5] = "JUNIO";
    month[6] = "JULIO";
    month[7] = "AGOSTO";
    month[8] = "SEPTIEMBRE";
    month[9] = "OCTUBRE";
    month[10] = "NOVIEMBRE";
    month[11] = "DICIEMBRE";

    return month[num1];
  }
  updateArrayEmp(monto, cantidad, desc=this.state.emp_account_desc){
  	var array_ea = [];
  	// console.log("monto");
  	// console.log(monto);
  	// console.log("cantidad");
  	// console.log(cantidad);
  	var hoy = new Date();
  	for(var i = 0; i < cantidad; i++){
  		var fecha = hoy;
  		fecha.setMonth(i+1);
  			var formato_fecha = fecha.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ fecha.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + fecha.toLocaleDateString("en-US", { day: '2-digit' });
			var obj = {
	  			description: `${desc} - ${i+1}`,
	  			amount: monto,
	  			fecha: formato_fecha
	  		};
	  		array_ea.push(obj);
  	}
  	this.setState({array_emp_account: array_ea});
  }
	render(){
		var {persons} = this.props;
		var {employ_detail, add_emp_accounts, cuentas, snack_open, message_success} = this.state;
		var form_emp_account = '';
		var showSnack = '';
		/*console.log(employ_detail);*/
		if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
		return(
			<div>
				{showSnack}
				<Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={3}>
							<div className="card_person">
	              <img
	                className="profile_photo"
	                src={`/img/${persons.person_photo}`}
	              />
				              <div className="card_label_field">
	                <p className="title_column">Nombres </p>
	                <p className="value_column">{persons.person_fname}</p>
	                <p className="title_column">Apellidos </p>
	                <p className="value_column">{persons.person_lastname}</p>
	                <p className="title_column">Documento </p>
	                <p className="value_column">{persons.person_idnumber}</p>
	                <p className="title_column">Celular </p>
	                <p className="value_column">{persons.telephone}</p>
	                <p className="title_column">Correo </p>
	                <p className="value_column">{persons.email}</p>
	                <p className="title_column">Sexo </p>
	                <p className="value_column">{persons.person_gender}</p>
	                <p className="title_column">Pais de Nacionalidad </p>
	                <p className="value_column">{persons.country_name}</p>
	                <p className="title_column">Dirección </p>
	                <p className="value_column">{persons.person_address}</p>
	                <p className="title_column">Tipo de sangre </p>
	                <p className="value_column">{persons.person_bloodtype}</p>
	                <p className="title_column">Fecha de inicio </p>
	                <p className="value_column">{employ_detail.emplo_startdate}</p>
	                <p className="title_column">Estado</p>
	                <p className="value_column">
	                {employ_detail.emplo_status?
	                	'Activo':!(employ_detail.emplo_status)&&
	                	'Inactivo'
	                }
	                </p>
	              </div>
	            </div>
	          </Grid>
	          <Grid item xs={9}>
	            <div className="card_person4">
	               <h4 className="value_column" align="center">Estado de cuenta</h4>
	               <hr style={{color: '#ccc', width: '90%', marginLeft: '5%'}}/>
	               <div style={{width: '90%', marginLeft: '5%'}}>
			              <Grid container item xs={12} spacing={3}>
		            			<Grid item xs={6}>
		            				{this.state.employ_edit_salary?
		            					<p 
		               					className="title_column2" 
		               					style={{textDecoration:'underline'}}
		               					onClick={()=>this.setState({employ_edit_salary:false})}
		               					>
		               					<span>
		               						<CloseIcon style={{marginBottom:-5}} fontSize="small" />
		               					</span>
		               					Cancelar
		               				</p>
		            					:
		            					<p className="title_column" style={{fontSize:18}}>Salario: 
		               					<span className="value_column">
		               						{employ_detail.has_ips?
		               							format(Math.round(parseFloat(employ_detail.emplo_salary)-((parseFloat(employ_detail.porcent_ips)*employ_detail.emplo_salary)/100)))
		               							:
		               							format(employ_detail.emplo_salary)
		               						}
		               					</span>
		               					<span>
	                            <IconButton style={{marginLeft:10, marginTop:-5}}  size="small" onClick={()=>{
	                            	this.setState({employ_edit_salary: true})
	                            }}>
	                                <EditIcon style={{color: 'blue'}} fontSize="small" />
	                            </IconButton>
	                          </span>
		               				</p>
		            				}
	               				
	               			</Grid>
	               			<Grid item xs={6}>
	               				{this.state.add_emp_accounts?
	               					<p 
		               					align="center" 
		               					className="title_column2" 
		               					style={{textDecoration:'underline'}}
		               					onClick={()=>this.setState({add_emp_accounts:false})}
		               					>
		               					<span>
		               						<CloseIcon style={{marginRight:5, marginBottom:-5}} fontSize="small" />
		               					</span>
		               					Cancelar
		               				</p>
	               					:(!this.state.employ_edit_salary)&&
		               				<p 
		               					align="center" 
		               					className="title_column2" 
		               					style={{textDecoration:'underline'}}
		               					onClick={()=>this.setState({add_emp_accounts:true})}
		               					>
		               					<span>
		               						<AddIcon style={{marginRight:5, marginBottom:-5}} fontSize="small" />
		               					</span>
		               					Agregar prestamos
		               				</p>
	               				}
	               			</Grid>
	               		</Grid>
	               		{this.state.add_emp_accounts?
	               			<div>
	               				<TextField
			                    label="Descripción"
			                    id="outlined-full-width"
			                    variant="outlined"
			                    style={{ margin: 8 }}
			                    value={this.state.emp_account_desc}
			                    size="small"
			                    onChange={(e)=>{
			                    	this.setState({emp_account_desc: e.target.value})
			                    	this.updateArrayEmp(this.state.emp_account_amount, this.state.emp_account_qty, e.target.value);
			                    }}
			                    margin="normal"
			                  />
			                  <TextField
			                    label="Monto por cuota"
			                    id="outlined-full-width"
			                    variant="outlined"
			                    type="number"
			                    style={{ margin: 8 }}
			                    value={this.state.emp_account_amount}
			                    size="small"
			                    onChange={(e)=>{
			                    	this.setState({emp_account_amount: parseFloat(e.target.value)})
			                    	this.updateArrayEmp(e.target.value, this.state.emp_account_qty);
			                    }}
			                    margin="normal"
			                  />
			                  <TextField
			                    label="Cantidad de cuotas"
			                    id="outlined-full-width"
			                    variant="outlined"
			                    type="number"
			                    style={{ margin: 8 }}
			                    value={this.state.emp_account_qty}
			                    size="small"
			                    onChange={(e)=>{
			                    	this.setState({emp_account_qty: parseFloat(e.target.value)})
			                    	this.updateArrayEmp(this.state.emp_account_amount, e.target.value);
			                    }}
			                    margin="normal"
			                  />
			                  <div>Monto total: {this.state.emp_account_qty*this.state.emp_account_amount}</div>
			                  <table style={{width: '100%'}}>
			                  <tbody>
			                  {this.state.array_emp_account.map((row, index)=>{
			                  	return(
			                  		<tr key={index}>
			                  			<td>
			                  			{row.description}
			                  			</td>
			                  			<td>
			                  			{row.monto}
			                  			</td>
			                  			<td>
			                  			<TextField
						                    label="Fecha"
						                    type="date"
						                    id="outlined-full-width"
						                    variant="outlined"
						                    style={{ margin: 8 }}
						                    value={row.fecha}
						                    size="small"
						                    onChange={(e)=>{
						                    	var array_temp = this.state.array_emp_account;
						                    	array_temp[index].fecha = e.target.value;
						                    	this.setState({array_emp_account:array_temp});
						                    }}
						                    margin="normal"
						                  />
			                  			
			                  			</td>
			                  		</tr>
						              )
			                  })}
			                  </tbody>
			                  </table>
			                  <Button
								          name="save"
								          variant="outlined"
								          color="primary"
								          startIcon={<SaveIcon />}
								          type="submit"
								          onClick={()=>{
								          	axios.post(`/api/employees-add-discount/${this.props.persons.person_id}`, {
								          		emp_account: this.state.array_emp_account
								          	}).then((res)=>{
															this.setState({ snack_open: true, message_success: res.data, add_emp_accounts: false });
															axios.get(`/api/employees-laboral-data/${this.props.persons.person_id}`).then((res)=>{
																// console.log(res.data);
																this.setState({employ_detail: res.data.employ_detail, cuentas:res.data.cuentas})
															})
														})
								          }}
								          fullWidth
								        >
								          Guardar
								        </Button>
	               			</div>
	               			:this.state.employ_edit_salary?
	               				<div>
		               				<TextField
				                    label="Salario"
				                    id="outlined-full-width"
				                    variant="outlined"
				                    type="text"
				                    style={{ margin: 8 }}
				                    value={format(this.state.employ_detail.emplo_salary)}
				                    size="small"
				                    onChange={(e)=>{
				                    	var employ_detail = this.state.employ_detail;
				                    	employ_detail.emplo_salary = disformat(e.target.value);
				                    	this.setState({employ_detail: employ_detail});
				                    	// this.updateArrayEmp(this.state.emp_account_amount, this.state.emp_account_qty, e.target.value);
				                    }}
				                    margin="normal"
				                  />
				                  <FormControlLabel
										        control={
										          <Checkbox
										            checked={this.state.employ_detail.has_ips}
										            onChange={()=>{
										            	var employ_detail = this.state.employ_detail;
						                    	employ_detail.has_ips = !this.state.employ_detail.has_ips;
						                    	this.setState({employ_detail: employ_detail});
										            }}
										            name="checkedB"
										            color="primary"
										          />
										        }
										        label={this.state.employ_detail.has_ips?"Tiene IPS":"No tiene IPS"}
										      />
										      {this.state.employ_detail.has_ips?
										      	<div>
										      		<TextField
						                    label="Porcentaje"
						                    id="outlined-full-width"
											disabled
						                    variant="outlined"
						                    type="number"
						                    style={{ margin: 8 }}
						                    value={this.state.employ_detail.porcent_ips}
						                    size="small"
						                    onChange={(e)=>{
						                    	var employ_detail = this.state.employ_detail;
						                    	employ_detail.porcent_ips = e.target.value;
						                    	this.setState({employ_detail: employ_detail});
						                    	// this.updateArrayEmp(this.state.emp_account_amount, this.state.emp_account_qty, e.target.value);
						                    }}
						                    margin="normal"
						                	/>
						                	<p className="title_column" style={{fontSize:18}}>Total: 
				               					<span className="value_column">
				               						{format(Math.round(parseFloat(employ_detail.emplo_salary)-((parseFloat(employ_detail.porcent_ips)*employ_detail.emplo_salary)/100)))}
				               					</span>
				               				</p>
										      	</div>
										      	:
										      	<div>
										      		<p className="title_column" style={{fontSize:18}}>Total: 
				               					<span className="value_column">
				               						{format(employ_detail.emplo_salary)}
				               					</span>
				               				</p>
										      	</div>
										      }
										      <TextField
				                    label="Observacion"
				                    id="outlined-full-width"
				                    variant="outlined"
				                    style={{ margin: 8 }}
				                    value={this.state.employ_detail.emplo_observa}
				                    size="small"
				                    onChange={(e)=>{
				                    	var employ_detail = this.state.employ_detail;
				                    	employ_detail.emplo_observation = e.target.value;
				                    	this.setState({employ_detail: employ_detail});
				                    	// this.updateArrayEmp(this.state.emp_account_amount, this.state.emp_account_qty, e.target.value);
				                    }}
				                    margin="normal"
				                	/>
										      <Button
									          name="save"
									          variant="outlined"
									          color="primary"
									          startIcon={<SaveIcon />}
									          type="submit"
									          onClick={()=>{
								          	axios.put(`/api/employees-update-salary/${this.props.persons.person_id}`, {
									          		employ_detail: employ_detail
									          	}).then((res)=>{
																this.setState({ snack_open: true, message_success: res.data, employ_edit_salary: false });
															})
									          }}
									          fullWidth
									        >
									          Guardar
									        </Button>
			                  </div>
	               			:
	               			<div>
	               				<p 
	               					className="title_column" 
	               					>
	               					{employ_detail.emplo_observation}
	               				</p>
		               			<Accordion defaultExpanded={true}>
		                    <AccordionSummary
		                      expandIcon={<ExpandMoreIcon />}
		                      aria-controls="panel1a-content"
		                      id="panel1a-header"
		                    >
		                    	<div>
		                    		<h4 className="value_column" align="center" style={{fontSize:15}}>Prestamos pendientes</h4>
		                    	</div>
		                    </AccordionSummary>
		                    <AccordionDetails>
		                    	<div className="grid_cuotas_container">
			                    	{cuentas.map((row, index) =>{
			                    		var sta_color = "";
			                    		if (row.emp_acc_status) {
			                    			sta_color = "green";
			                    		}else if(
			                    			row.emp_acc_amountpaid > 0 &&
		                                    row.emp_acc_amountpaid <
		                                      row.emp_acc_amount
			                    		){
			                    			sta_color = "mid_paid";
			                    		}else{
			                    			sta_color = "orange";
			                    		}
			                    		return(
			                    			<div 
			                    				className={"main-enroll-paid_mat" +" staccount_color-" +sta_color}
			                    				key={index}>
			                    				<span className="quot_title_desc">
			                    					{row.emp_acc_desc}
			                    				</span>
			                    				<p
	                                  className="quot_title_month"
	                                >
	                                  {this.getMonth1(
	                                    row.emp_acc_expiration
	                                  )}
	                                </p>
			                    			</div>
			                    		)
			                    	})}
			                    	</div>
		                    </AccordionDetails>
		                  </Accordion>
	                  </div>
	               		}
	               </div>
	            </div>
	          </Grid>
	        </Grid>
	      </Grid>
			</div>
		);
	}
}