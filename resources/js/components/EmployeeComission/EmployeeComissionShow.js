/*
*desplegar la lista de accounts en una tabla, que muestre la persona,
                                    el numero de accounts, monto total
*/
import React, { Component } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Grid,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
export default class EmployeeComissionShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accounts: [],
      order: "asc",
      ticketDetails: [],
      orderBy: "",
      page_lenght: 0,
      rowsPerPage: 5,
      page: 1,
      url: `/api/employee_comission-per/${this.props.person}`,
      // url: `/api/tillsaccounts/6`,
      permissions: {
        ticket_index: null,
        ticket_show: null,
        ticket_store: null,
        ticket_update: null,
        ticket_destroy: null,
      },
    };

    this.getObject = async () => {
      let res;
      res = await axios.get(`${this.state.url}?sort_by=${this.state.orderBy}&order=${this.state.order}&per_page=${this.state.rowsPerPage}&page=${this.state.page}`);
      let data = await res.data;
      console.log(data.data)
      if (this._ismounted) {
        this.setState({
          accounts: data.data,
          // ticketDetails: data.data.ticketDetails,
        });
      }
    };
  }

  componentDidMount() {
    this._ismounted = true;
    var permissions = [
      "ticket_index",
      "ticket_show",
      "ticket_store",
      "ticket_update",
      "ticket_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject();
        }
      });
  }

  componentWillUnmount() {
    this._ismounted = false;
  }

  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.prevActiveStep,
      name,
      order
    );
  }

  render() {
    return (
      <div>
        {console.log(this.state.accounts)}
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={this.state.orderBy === "enroll_id" ? this.state.order : false}
                >
                  <TableSortLabel
                    active={this.state.orderBy === "enroll_id"}
                    direction={this.state.orderBy === "enroll_id" ? this.state.order : "asc"}
                    onClick={() => {
                      this.createSortHandler("enroll_id");
                    }}
                  >
                    <h3>Inscripcion</h3>
                  </TableSortLabel>
                </TableCell>

                  <TableCell
                  sortDirection={this.state.orderBy === "emp_com_date" ? this.state.order : false}
                >
                  <TableSortLabel
                    active={this.state.orderBy === "emp_com_date"}
                    direction={this.state.orderBy === "emp_com_date" ? this.state.order : "asc"}
                    onClick={() => {
                      this.createSortHandler("emp_com_date");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell
                  sortDirection={this.state.orderBy === "emp_com_amount" ? this.state.order : false}
                >
                  <TableSortLabel
                    active={this.state.orderBy === "emp_com_amount"}
                    direction={this.state.orderBy === "emp_com_amount" ? this.state.order : "asc"}
                    onClick={() => {
                      this.createSortHandler("emp_com_amount");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>

                
                
              </TableRow>
            </TableHead>

           
<TableBody >

{
  this.state.accounts.map((account, index) =>{ 
return(
              <TableRow>
              <TableCell><p>{account.enroll_id}</p></TableCell>
              <TableCell>{account.emp_com_date}</TableCell>
              <TableCell>{account.emp_com_amount}</TableCell>   
            </TableRow>
         )  
  })
}
</TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}
