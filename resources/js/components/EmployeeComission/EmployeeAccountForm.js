import React, { Component } from "react";
import { Grid, Button, TextField, Snackbar } from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import { Autocomplete, Alert } from "@material-ui/lab";
import InputMask from "react-input-mask";

export default class EmployeeAccountForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      type: "",
      person_id: 0,
      person_name: 0,
      emp_acc_id: 0,
      emplo_salary: 0,
      persons: [],
      employeeAccounts: [],
      emp_acc_status: null,
      disabled: false,
      emp_acc_desc: null,
      emp_acc_amountpaid: null,
      emp_acc_month: null,
      emp_acc_month2: null,
      emp_acc_ticket_number: "",
    };
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.updateState = () => {
      this.setState({
      snack_open: false,
      message_success: "",
      });
      axios
      .get("/api/employeeAccounts/" + this.props.person_id)
      .then((response) => {
        this.setState({
          employeeAccounts: response.data.data,
        });
      });
    };
  }

  componentDidMount() {
    axios.get("/api/employees/" + this.props.person_id).then((res) => {
      this.setState({
        emplo_salary: res.data.data.emplo_salary,
        // emp_acc_amountpaid: res.data.data.emplo_salary,
        person_id: res.data.data.person_id,
        person_name:
          res.data.data.person_fname + " " + res.data.data.person_lastname,
        disabled: true,
      });
    });
    axios
      .get("/api/employeeAccounts/" + this.props.person_id)
      .then((response) => {
        this.setState({
          employeeAccounts: response.data.data,
        });
      });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param, type) {
    // console.log(param);
    if (type === false) {
      this.setState({
        snack_open: true,
        message_success: param,
        type: "error",
      });
    } else {
      this.setState({
        snack_open: true,
        message_success: param,
        type: "success",
      });
    }
  }
  handleCreateObject(e) {
    e.preventDefault();

    axios
      .put("/api/employeeAccount/" + this.state.emp_acc_id, {
        person_id: this.state.person_id,
        emp_acc_ticket_number: this.state.emp_acc_ticket_number,
        emp_acc_amountpaid: this.state.emp_acc_amountpaid,
      })
      .then((res) => {
        this.openSnack(res.data.message, res.data.dato);
        if (res.data.dato === true) {
          this.setState({
            emp_acc_amountpaid: 0,
            emp_acc_ticket_number: "",
            emp_acc_desc: "",
            emp_acc_month: "",
            emp_acc_month2: "",
          });
        }
      });
  }

  fieldsChange(e, name) {
    e.preventDefault();
    this.setState({ [name]: e.target.value });
  }

  selectsChange(e, values, nameValue, nameLabel) {
    if (nameValue === "emp_acc_month") {
      this.setState({
        emp_acc_amountpaid: values.salary,
        emp_acc_id: values.emp_acc_id,
      });
    }
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    // this.props.changeAtribute(true);
  }

  selectReturn() {
    return this.state.persons.map((data) => ({
      label: data.person_fname + " " + data.person_lastname,
      value: data.person_id,
    }));
  }
  salarys() {
    return this.state.employeeAccounts.map((data) => ({
      label: data.emp_acc_desc,
      value: data.emp_acc_month,
      salary: data.emp_acc_amount,
      emp_acc_id: data.emp_acc_id,
    }));
  }
  render() {
    var d = new Date();
    var showSnack;
    if (this.state.snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={this.state.snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity={this.state.type}
          >
            {this.state.message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <Autocomplete
          id="combo-box-demo"
          disableClearable
          disabled={this.state.disabled}
          options={this.selectReturn()}
          getOptionLabel={(option) => option.label}
          onChange={(e, values) =>
            this.selectsChange(e, values, "person_id", "person_name")
          }
          value={{
            label: this.state.person_name,
            value: this.state.person_id,
          }}
          getOptionSelected={(option, value) => {
            if (value === option.value) {
              return option.label;
            } else {
              return false;
            }
          }}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => (
            <TextField
              {...params}
              // error={this.state.validator.unit_id.error}
              // helperText={this.state.validator.unit_id.message}
              label="Selecione un funcionario *"
              variant="outlined"
              fullWidth
            />
          )}
        />
        <div>
          <InputMask
            mask="999-999-9999999"
            maskChar=" "
            name="emp_acc_ticket_number"
            disabled={false}
            value={this.state.emp_acc_ticket_number}
            onChange={(event) => {
              this.fieldsChange(event, "emp_acc_ticket_number");
            }}
          >
            {() => (
              <TextField
                variant="outlined"
                type="text"
                label="N. de Recibo"
                fullWidth
                style={{ margin: 8 }}
              />
            )}
          </InputMask>
        </div>

        <Autocomplete
          id="combo-box-demo"
          disableClearable
          options={this.salarys()}
          getOptionLabel={(option) => option.label}
          onChange={(e, values) =>
            this.selectsChange(e, values, "emp_acc_month", "emp_acc_month2")
          }
          value={{
            label: this.state.emp_acc_month2,
            value: this.state.emp_acc_month,
          }}
          getOptionSelected={(option, value) => {
            if (value === option.value) {
              return option.label;
            } else {
              return false;
            }
          }}
          style={{ margin: 8 }}
          fullWidth
          renderInput={(params) => (
            <TextField
              {...params}
              // error={this.state.validator.unit_id.error}
              // helperText={this.state.validator.unit_id.message}
              label="Selecione un Mes *"
              variant="outlined"
              fullWidth
            />
          )}
        />
        <TextField
          id="outlined-basic"
          label="Monto"
          className="textField"
          type="number"
          fullWidth
          style={{ margin: 8 }}
          value={this.state.emp_acc_amountpaid}
          name="emp_acc_amountpaid"
          onChange={(event) => {
            this.fieldsChange(event, "emp_acc_amountpaid");
          }}
          margin="normal"
          variant="outlined"
        />
        {/*<TextField
          id="outlined-basic"
          label="Observaciones"
          className="textField"
          multiline={true}
          fullWidth
          rows="3"
          style={{ margin: 8 }}
          value={this.state.emp_acc_desc}
          name="emp_acc_desc"
          onChange={(event) => {
            this.fieldsChange(event, "emp_acc_desc");
          }}
          margin="normal"
          variant="outlined"
        />*/}
        <Button
          variant="outlined"
          color="primary"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
        {showSnack}
      </div>
    );
  }
}
