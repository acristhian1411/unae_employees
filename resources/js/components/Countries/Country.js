import React, { Component, lazy } from "react";
import { Link } from "react-router-dom";
import ModalCountry from "./ModalCountry";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
// const ModalCountry = lazy(()=> import('./ModalCountry'));
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

class Country extends Component {
  constructor() {
    super();
    this.state = {
      snack_open: false,
      message_success: "",
      countries: [],
      country_id: 0,
      country: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredCountries: [],
      prevActiveStep: 1,
      paginator: [],
      rowsPerPage: 10,
      order: "asc",
      orderBy: "country_name",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/countries",
      busqueda: false,
      permissions: {
        countries_index: null,
        countries_show: null,
        countries_store: null,
        countries_update: null,
        countries_destroy: null,
      },
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          countries: data,
          paginator: data,
          filteredCountries: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    this.handleClickOpen = (data) => {
      this.setState({ open: true, country: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    // pagina siguiente
    this.handleNext = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep + 1 });
      var newPage = this.state.prevActiveStep + 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //pagina anterior
    this.handleBack = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep - 1 });
      var newPage = this.state.prevActiveStep - 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //filas por pagina
    this.handleChangeSelect = (event) => {
      this.setState({ rowsPerPage: event.target.value, prevActiveStep: 1 });
      this.getObject(event.target.value, 1);
    };
  }

  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("PAISES");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "countries_index",
      "countries_show",
      "countries_store",
      "countries_update",
      "countries_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredCountries: this.state.countries.data,
            paginator: this.state.countries,
            page_lenght: this.state.countries.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/countries`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/countries?country_name=${e.target.value}`,
          });
          axios
            .get(
              `${this.state.url}&sort_by=${this.state.orderBy}&order=${
                this.state.order
              }&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filteredCountries: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  deleteObject() {
    axios.delete(`api/countries/${this.state.country_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  clickEditar(data) {
    console.log("boton editar");
    this.setState({
      edit: true,
      country: data,
    });
  }

  clickAgregar() {
    console.log("boton agregar");
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.prevActiveStep,
      name,
      order
    );
  }
  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      country,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.new) {
      // showModal = <ModalCountry edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalCountry />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      // showModal = <ModalCountry edit={true} onHandleSubmit={this.updateState} pais={this.state.country}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalCountry />,
            props_form: {
              edit: true,
              pais: this.state.country,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={country.data.country_name}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (this.state.filteredCountries.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body" style={{textAlign: "center"}}>
          <TextField
            name="search"
            id="outlined-search"
            label="Escriba para buscar"
            type="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "country_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "country_name"}
                    direction={orderBy === "country_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("country_name");
                    }}
                  >
                    <h3>Nombre</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "country_code" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "country_code"}
                    direction={orderBy === "country_code" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("country_code");
                    }}
                  >
                    <h3>Codigo</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="3">
                  {permissions.countries_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        name="new_item"
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>

            {this.renderList()}
          </Table>
        </TableContainer>
        {/* <Paginator
            lenght={this.state.page_lenght}
            perPageChange= {this.perPageChange}
            pageChange ={this.pageChange}
            page= {this.state.page}
            /> */}

        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredCountries.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredCountries.map((data) => {
        return (
          <TableBody key={data.country_id}>
            <TableRow>
              <TableCell>{Capitalize(data.country_name)}</TableCell>
              <TableCell>{data.country_code}</TableCell>

              <TableCell>
                {permissions.countries_update === true && (
                  <Tooltip title="Editar">
                    <Button
                      name="edit_item"
                      variant="outlined"
                      color="primary"
                      startIcon={<EditIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>

              <TableCell>
                {permissions.countries_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      name="delete_item"
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ country_id: data.country_id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
export default Country;
