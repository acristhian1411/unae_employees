import React, {Component} from 'react';
import {Modal, Backdrop, Fade, Button, TextField, Snackbar} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Alert } from '@material-ui/lab';


class ModalCountry extends Component {
   constructor(props){
     super(props)
     this.state = {
       snack_open: false,
       open: true,
       country_id: 0,
       country_name: '' ,
       country_code: '' ,
       sucess: false,
       error: false,
       validator: {
         country_name:{
           message:'',
           error: false,
         },
         country_code:{
           message:'',
           error: false
         }
       }
     }
     this.mounted = false;
     this.handleOpen = this.handleOpen.bind(this);
     this.handleClose = this.handleClose.bind(this);
     this.fieldsChange = this.fieldsChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
     this.handleSubmit1 = this.handleSubmit1.bind(this);
     this.closeSnack = this.closeSnack.bind(this);
   }
   closeSnack(){
     this.setState({snack_open: false});
   }
   handleOpen(){
     this.setState({open:true});
   };
   handleClose(){
     this.setState({open:false});
     this.props.onHandleSubmit();
   }
   fieldsChange(e){
     this.setState({[e.target.name]: e.target.value});
     this.props.changeAtribute(true);
   }

   handleSubmit(e){
     e.preventDefault();
     const {country_name, country_code} = this.state;
     var validator = {country_name: {error: false,  message: ''},
                      country_code: {error: false,  message: ''}};
     var object_error = {};
     axios.post('api/countries', {
       country_name, country_code}).then(
       res=>{
         this.props.onSuccess(res.data);
         this.props.changeAtribute(false);
         this.setState({country_name: '', country_code: ''});
         this.handleClose();
       }).catch(error => {
         // console.log(Object.keys(error.response.data.errors));
         this.setState({snack_open: true});
         var errores = Object.keys(error.response.data.errors);
         for (var i = 0; i < errores.length; i++) {
           object_error = {
             ...object_error,
             [errores[i]]: {
               error: true,
               message: error.response.data.errors[errores[i]][0]
             }
           }
         }
         this.setState({
           validator:{
             ...validator,
             ...object_error
           }
         });
       });
   }
   handleSubmit1(e){
     const {country_name, country_code, country_id} = this.state;
     e.preventDefault();
     var validator = {country_name: {error: false,  message: ''},
                      country_code: {error: false,  message: ''}};
     var object_error = {};
     // console.log(this.props.career_type.data.tcareer_id);
     axios.put(`api/countries/${country_id}`, {country_name, country_code}).then(
       res=>{
         this.props.onSuccess(res.data);
         this.props.changeAtribute(false);
        // console.log("se guardo con exito");
        this.handleClose();
       }
     ).catch(error => {
       // console.log(Object.keys(error.response.data.errors));
       this.setState({snack_open: true});
       var errores = Object.keys(error.response.data.errors);
       for (var i = 0; i < errores.length; i++) {
         object_error = {
           ...object_error,
           [errores[i]]: {
             error: true,
             message: error.response.data.errors[errores[i]][0]
           }
         }
       }
       this.setState({
         validator:{
           ...validator,
           ...object_error
         }
       });
     });
   }
   componentDidMount(){
     if (this.props.edit) {
       const {country_name, country_code, country_id} = this.props.pais.data;
       // axios.get(`http://localhost:8000/api/career_types/${this.props.career_type.data.career_type_id}`).then(res=>{
       //   this.setState({country_name: res.data.data.country_name,
       //                  career_type_code: res.data.data.career_type_code,
       //                  career_type_logo: res.data.data.career_type_logo
       //                });
       //   }).catch(error=>{alert(error)});
       this.setState({
         country_name, country_code, country_id
       });
     }
   }

   editOrNew(){
     if (this.props.edit) {
       return(
         <Button name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleSubmit1}>Actualizar</Button>
       );
     }
     else {
       return(
       <Button name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleSubmit}>Guardar</Button>
       );
     }
   }
   render(){
     const { classes } = this.props;
     const {country_name, country_code, open, validator, snack_open} = this.state;
     var showSnack;
     if (snack_open){
           showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                          <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="error">
                            {validator['country_name']['error']?<li>Error: {validator['country_name']['message']}</li>:''}
                            {validator['country_code']['error']?<li>Error: {validator['country_code']['message']}</li>:''}
                          </Alert>
                        </Snackbar>
         }
     return (

        <div>
               <h3 align='center'>Formulario de Paises</h3>
               <hr />
                <form>
                <TextField
                    id="outlined-full-width"
                    error={this.state.validator.country_name.error}
                    helperText={this.state.validator.country_name.message}
                    label="Nombre"
                    style={{ margin: 8 }}
                    placeholder="Ejemplo: Paraguay"
                    value={country_name}
                    name='country_name'
                    onChange={this.fieldsChange.bind(this)}
                    fullWidth
                    margin="normal"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                  />
                  <TextField
                      id="outlined-full-width"
                      error={this.state.validator.country_code.error}
                      helperText={this.state.validator.country_code.message}
                      label="Codigo"
                      style={{ margin: 8 }}
                      placeholder="Ejemplo: PY"
                      value={country_code}
                      name='country_code'
                      onChange={this.fieldsChange.bind(this)}
                      fullWidth
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      variant="outlined"
                    />
                <br />
                {this.editOrNew()}
                {showSnack}

                  <Button variant="outlined" color="secondary" startIcon={<ExitToAppIcon />} onClick={this.handleClose}>Cerrar</Button>
                </form>
        </div>
       );
   }
 }
export default ModalCountry;
