import React,{Component} from 'react';
import {Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Tooltip, Button} from '@material-ui/core';
import RestoreIcon from '@material-ui/icons/Restore';

export default class FormViewLog extends Component {
  constructor(props){
    super(props);

  }
  componentDidMount(){
    // console.log(this.props);
  }
  restore(values){
  var old_values = JSON.parse(values.old_values);
  axios.put(values.url, {...old_values, audit_restore: true}).then(res=>{
    console.log(res);
  }).catch(error=>{
    console.log(error);
  })
  }
  render(){
    var {log} = this.props;
    var new_values = JSON.parse(log.new_values);
    var old_values = JSON.parse(log.old_values);
    // console.log(Object.keys(new_values));
    var keys_nvalues = Object.keys(new_values);
    var keys_ovalues = Object.keys(new_values);
    return(
      <div>
      <Grid container spacing={1}>
        <Grid container item xs={12} spacing={1}>
          <Grid item xs={4}>
            <div className="card_person">
                <div className="card_label_field">
                  <p className="title_column">Modelo</p>
                  <p className="value_column">{log.auditable_type}</p>
                  <p className="title_column">Evento </p>
                  <p className="value_column">{log.event}</p>
                  <p className="title_column">Fecha </p>
                  <p className="value_column">{log.created_at}</p>
                  <p className="title_column">URL </p>
                  <p className="value_column">{log.url}</p>
                  <p className="title_column">IP_ADDRESS </p>
                  <p className="value_column">{log.ip_address}</p>
                  <p className="title_column">Usuario </p>
                  <p className="value_column">{log.user_id}</p>
                  <p className="title_column">Agente del usuario </p>
                  <p className="value_column">{log.user_agent}</p>
                </div>
            </div>
          </Grid>
          <Grid item xs={8}>
           {(log.event === 'updated'||log.event === 'created')&&
            <div>
            <h3 style={{textAlign:'center'}}>Datos nuevos</h3>
            <hr/>
            <TableContainer component={Paper}>
              <Table aria-label="simple table" option={{ search: true }}>
                <TableHead>
                  <TableRow>
                    {keys_nvalues.map(data=>{
                      // console.log(data);
                      return(
                      <TableCell><h3>{data}</h3></TableCell>
                      );

                    })}
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    {keys_nvalues.map(data=>{
                      return(
                      <TableCell>{new_values[data]}</TableCell>
                      );
                    })}

                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            </div>
           }
           {(log.event === 'updated'||log.event === 'deleted')&&
           <div>
            <h3 style={{textAlign:'center'}}>{`Datos Anteriores `}
              <Tooltip title="Restaurar">
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={<RestoreIcon />}
                  type="submit"
                  onClick={() => {
                    this.restore(log);
                  }}
                >
                  {" "}
                </Button>
              </Tooltip>
            </h3>
            <hr/>

            <TableContainer component={Paper}>
              <Table aria-label="simple table" option={{ search: true }}>
                <TableHead>
                  <TableRow>
                    {keys_ovalues.map(data=>{
                      // console.log(data);
                      return(
                      <TableCell><h3>{data}</h3></TableCell>
                      );

                    })}
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    {keys_ovalues.map(data=>{
                      return(
                      <TableCell>{old_values[data]}</TableCell>
                      );
                    })}

                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            </div>
          }
          </Grid>
        </Grid>
      </Grid>

      </div>
    );
  }
}
