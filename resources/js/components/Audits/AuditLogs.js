import FormViewLog from "./FormViewLog";
import MidModal from "../Modals/MidModal";
import React, { Component, lazy } from "react";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
// const ModalForm = lazy(()=> import('./ModalForm'));
import { Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import Paginator from "../Paginators/Paginator";

class AuditLogs extends Component {
  constructor() {
    super();
    this.state = {
      snack_open: false,
      view: false,
      message_success: "",
      logs: [],
      id: 0,
      log: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredlogs: [],
      rowsPerPage: 10,
      paginator: [],
      order: "desc",
      orderBy: "id",
      page_lenght: 0,
      page: 1,
      time_out: false,
      busqueda: false,
      url: "/api/audit_logs",
      permissions: {
        hstitle_index: null,

      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;

      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          logs: data,
          paginator: data,
          filteredlogs: data.data,
          open: false,
          page_lenght: res.data.last_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        view: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    //funciones
    // this.clickSortByName = this.clickSortByName.bind(this)
    // this.clickSortByCode = this.clickSortByCode.bind(this)
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);
    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, log: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    //fin constructor
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "hstitle_index",
      // "audits_show",

    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }

  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredlogs: this.state.logs.data,
            paginator: this.state.logs,
            page_lenght: this.state.logs.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/logs`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/logs?auditable_type=${e.target.value}`,
          });
          axios
            .get(`${this.state.url}&per_page=${this.state.rowsPerPage}`)
            .then((res) => {
              this.setState({
                filteredlogs: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  deleteObject() {
    axios.delete(`/api/logs/${this.state.id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data.message });
      this.updateState();
    });
  }

  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }

  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      log,
      orderBy,
      order,
      permissions,
    } = this.state;
    // if (this.state.new) {
    //   // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <ModalForm />,
    //         props_form: { onSuccess: this.openSnack },
    //       }}
    //     </MidModal>
    //   );
    // } else if (this.state.edit) {
    //   // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} log={this.state.log}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <ModalForm />,
    //         props_form: {
    //           edit: true,
    //           log: this.state.log,
    //           onSuccess: this.openSnack,
    //         },
    //       }}
    //     </MidModal>
    //   );
    // }
    if (this.state.view) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormViewLog />,
            props_form: { onSuccess: this.openSnack, log: this.state.log },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={log.data.auditable_type}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (this.state.filteredlogs.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body">
        <h2 align="center">
          Registro de actividad
          <TextField
            id="outlined-search"
            label="Buscar"
            type="search"
            variant="outlined"
            onChange={this.search}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />
        </h2>
        <hr />
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={
                    orderBy === "id" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "id"}
                    direction={orderBy === "id" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("id");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={
                    orderBy === "user_id" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "user_id"}
                    direction={orderBy === "user_id" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("user_id");
                    }}
                  >
                    <h3>Usuario</h3>
                  </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "event" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "event"}
                  direction={orderBy === "event" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("event");
                  }}
                >
                  <h3>Evento</h3>
                </TableSortLabel>
            </TableCell>
                <TableCell
                  sortDirection={
                    orderBy === "auditable_type" ? order : false
                  }
                >

                  <TableSortLabel
                    active={orderBy === "auditable_type"}
                    direction={orderBy === "auditable_type" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("auditable_type");
                    }}
                  >
                    <h3>Modelo</h3>
                  </TableSortLabel>
                  </TableCell>
                <TableCell colSpan="2">

                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>

        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
        <br />
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    if (this.state.filteredlogs.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredlogs.map((data) => {
        return (
          <TableBody key={data.id}>
            <TableRow>
              <TableCell>{data.created_at}</TableCell>
              <TableCell>{data.user_id}</TableCell>
              <TableCell>{data.event}</TableCell>
              <TableCell>{data.auditable_type}</TableCell>

              <TableCell>
                  <Tooltip title="Ver">
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<VisibilityIcon />}
                      type="submit"
                      onClick={() => {
                        this.setState({view: true, log: data});
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
              </TableCell>
              <TableCell>
                {this.state.permissions.logs_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ id: data.id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}

export default AuditLogs;
