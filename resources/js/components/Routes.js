// import React, { Component } from "react";
import React, { Component, lazy, Suspense} from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
//

const Units = lazy(()=> import('./Units'));

const CitiesList = lazy(()=> import('./Cities/CitiesList'));

const Departments = lazy(()=> import('./Departments'));
const Country = lazy(()=> import('./Countries/Country'));

const ContactTypesList = lazy(()=> import('./ContactTypes/ContactTypesList'));
const ContactTypeShow = lazy(()=> import('./ContactTypes/ContactTypeShow'));
const Persons = lazy(()=> import('./Persons'));

const PersonPayments = lazy(()=> import('./Homes/PersonPayments'));
const PaymentTypes = lazy(()=> import('./PaymentTypes/PaymentTypes'));
const EmployeeAccountForm = lazy(()=> import('./EmployeeAccount/EmployeeAccountForm'));
const TicketDetailTypes = lazy(()=> import('./TicketDetailTypes/TicketDetailTypes'));
const Tickets = lazy(()=> import('./Tickets/Tickets'));
const TicketShow = lazy(()=> import('./Tickets/TicketShow'));
const UnitType = lazy(()=> import('./UnitTypes/UnitType'));


const DettillProofPayment = lazy(()=> import('./DettillProofPayment/DettillProofPayment'));

const AppointmentsList = lazy(()=> import('./Appointments/AppointmentsList'));
const TillsList = lazy(()=> import('./Tills/TillsList'));
const TillsReports = lazy(()=> import('./Tills/TillsReports'));
const TillShow = lazy(()=> import('./Tills/TillShow'));
const Employees = lazy(()=> import('./Employees/Employees'));
const EmployeeShow = lazy(()=> import('./Employees/EmployeeShow'));
const Login = lazy(()=> import('./Login'));

const Users = lazy(()=> import('./Users'));
const UserShow = lazy(()=> import('./Users/UserShow'));
const Roles = lazy(()=> import('./Roles'));
const RoleShow = lazy(()=> import('./Roles/RoleShow'));
const Unauthorized = lazy(()=> import('./Errors/Unauthorized'));
const RouteContentLoad = lazy(()=> import('./RouteContentLoad'));
const Homes = lazy(()=> import('./Homes'));
const TillDetailsReport = lazy(()=> import('./Tills/TillDetailsReport'));
const TillCloseReport = lazy(()=> import('./Tills/TillCloseReport'));
const IvaTypesList = lazy(()=> import('./IvaTypes/IvaTypesList'));
import {CircularProgress} from '@material-ui/core';

import { getAuth } from "../contexts";

class Routes extends Component {
  redirectPermissions(params_routes) {
    return (
      <getAuth.Consumer>
        {(context) => {
          return params_routes.map((data) => {
            return (
              <div key={data.route}>
                {context.permissions[data.permission] === true ? (
                  <Route exact path={data.route} render={(props)=>(<data.component {...props} changeTitlePage={this.props.changeTitlePage} />)}  />
                ) : context.permissions[data.permission] === false ? (
                  <Route exact path={data.route} component={Unauthorized} />
                ) : (
                  <Route exact path={data.route} component={RouteContentLoad} />
                )}
              </div>
            );
          });
        }}
      </getAuth.Consumer>
    );
  }
  render() {
    var array_routes = [
     
      {
        route: "/reporte_cierre_caja/:id",
        component: TillCloseReport,
        permission: "subjects_index",
        //audit_index
      },
     
      {
        route: "/reporte_caja_agrupado/:id",
        component: TillDetailsReport,
        permission: "subjects_index",
        //audit_index
      },
     
      
     
      {
        route: "/formas_pago",
        component: PaymentTypes,
        permission: "paymentTypes_index",
        //audit_index
      },
      {
        route: "/ficha_pago",
        component: PersonPayments,
        permission: "paymentTypes_index",
        //audit_index
      },
      {
        route: "/ficha_pago/:id",
        component: PersonPayments,
        permission: "paymentTypes_index",
        //audit_index
      },
      
      { route: "/cajas", component: TillsList, permission: "tills_index" },
      {
        route: "/cajas/show/:id",
        component: TillShow,
        permission: "tills_show",
      },
      {
        route: "/cargos",
        component: AppointmentsList,
        permission: "appointments_index",
      },
      
      // { route: "/controles", component: ProfessorControlsList, permission: "professor_controls_index" },
      // { route: "/cheques", component: ChecksList, permission: "checks_index" },
      { route: "/ciudades", component: CitiesList, permission: "cities_index" },
     {
        route: "/departamentos",
        component: Departments,
        permission: "departments_index",
      },
     
     
      {
        route: "/funcionarios",
        component: Employees,
        permission: "employees_index",
      },
      {
        route: "/funcionarios/show/:id",
        component: EmployeeShow,
        permission: "employees_show",
      },
      { route: "/paises", component: Country, permission: "countries_index" },
      { route: "/personas", component: Persons, permission: "persons_index" },
      // {
      //   route: "/personas/show/:id",
      //   component: PersonShow,
      //   permission: "persons_show",
      // },
      
      {
        route: "/reportes/balances",
        component: TillsReports,
        permission: "tills_show",
      },
      // {
      //   route: "/reportes/balance",
      //   component: BalanceReports,
      //   permission: "tills_show",
      // },
      // {
      //   route: "/reportes/egresos",
      //   component: ExpensesReports,
      //   permission: "tills_show",
      // },
      // {
      //   route: "/reportes/ingresos",
      //   component: IncomeReports,
      //   permission: "tills_show",
      // },
      { route: "/roles", component: Roles, permission: "roles_index" },
      {
        route: "/roles/show/:id",
        component: RoleShow,
        permission: "roles_show",
      },
     
      {
        route: "/tipo_contacto",
        component: ContactTypesList,
        permission: "contactTypes_index",
      },
      {
        route: "/tipo_contacto/show/:id",
        component: ContactTypeShow,
        permission: "contactTypes_show",
      },
     
      {
        route: "/tipos_iva",
        component: IvaTypesList,
        permission: "ivaType_index",
      },
     
      { route: "/instituciones", component: Units, permission: "units_index" },
      { route: "/usuarios", component: Users, permission: "users_index" },
      {
        route: "/tipo_unidades",
        component: UnitType,
        permission: "unit_types_index",
        // permission: "units_index"
      },
     
      {
        route: "/usuarios/show/:id",
        component: UserShow,
        permission: "users_show",
      },
     
    ];
    return (
      <getAuth.Consumer>
        {(context) => {
          return (
            <div>
              <Suspense fallback={<div style={{display: 'flex',alignItems:'center',justifyContent:'center', flexDirection: 'column'}}><CircularProgress /></div>}>

              <Switch>
                {/*<Route exact path="/menu/:type" component={Homes} />*/}
                <Route exact path="/menu/:type" render={(props)=>(<Homes {...props} changeTitlePage={this.props.changeTitlePage} />)}  />
                {/*<Route exact path="/" component={Homes} />*/}
                <Route exact path="/" render={(props)=>(<Homes {...props} changeTitlePage={this.props.changeTitlePage} />)}  />
                {this.redirectPermissions(array_routes)}
              </Switch>
              </Suspense>
            </div>
          );
        }}
      </getAuth.Consumer>
    );
  }
}
export default Routes;
