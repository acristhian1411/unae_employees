import React, {Component} from 'react';
import {Stepper, Step, StepLabel, Modal, Backdrop, Fade, Button, TextField, Snackbar} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import { Alert } from '@material-ui/lab';
import FormPermission from './FormPermission';

function getSteps() {
  return ['Crear Rol', 'Asignar Permisos'];
}

export default class FormRole extends Component {
   constructor(props){
     super(props);
     this.state = {
       snack_open: false,
       open: true,
       activeStep: 0,
       role_id: 0,
       role_name: '' ,
       sucess: false,
       role: [],
       error: false,
       validator: {
         role_name:{
           message:'',
           error: false,
         },
       }
     }
     this.mounted = false;
     this.handleOpen = this.handleOpen.bind(this);
     this.handleClose = this.handleClose.bind(this);
     this.fieldsChange = this.fieldsChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
     this.handleSubmit1 = this.handleSubmit1.bind(this);
     this.closeSnack = this.closeSnack.bind(this);
     this.handleNext = this.handleNext.bind(this);
     this.handleBack = this.handleBack.bind(this);
     this.handleReset = this.handleReset.bind(this);
   }
   closeSnack(){
     this.setState({snack_open: false});
   }
   handleOpen(){
     this.setState({open:true});
   };
   handleClose(){
     this.setState({open:false});
     this.props.onHandleSubmit();
   }
   handleNext(){
     // const {person_fname, person_lastname, birthplace_id, homecity_id, country_id,
     // person_birthdate, person_gender, person_idnumber, person_address,
     // person_bloodtype,person_business_name, person_ruc,person_photo} = this.state;
     // if (validator.prototype.validated([{person_fname},{person_lastname},
     //   {birthplace_id}, {homecity_id}, {country_id},
     //   {person_birthdate}, {person_gender}, {person_idnumber}, {person_address},
     //   {person_bloodtype}, {person_ruc}],this.updateField)===true) {

       this.setState({activeStep: this.state.activeStep+1});
     // }
  }
  handleReset(){
    this.setState({activeStep: 0});
  }
  handleBack(){
    this.setState({activeStep: this.state.activeStep-1});
  }

   fieldsChange(e){
     this.setState({[e.target.name]: e.target.value});
     this.props.changeAtribute(true);
   }

   handleSubmit(e){
     e.preventDefault();
     const {role_name} = this.state;
     var validator = {role_name: {error: false,  message: ''}};
     var object_error = {};
     axios.post('api/roles', {
       role_name}).then(
       res=>{
         this.props.onSuccess(res.data.message);
         this.props.changeAtribute(false);
         this.setState({role_name: '', role: res.data.dato});
         this.handleNext()

       }).catch(error => {
         // console.log(Object.keys(error.response.data.errors));
         this.setState({snack_open: true});
         var errores = Object.keys(error.response.data.errors);
         for (var i = 0; i < errores.length; i++) {
           object_error = {
             ...object_error,
             [errores[i]]: {
               error: true,
               message: error.response.data.errors[errores[i]][0]
             }
           }
         }
         this.setState({
           validator:{
             ...validator,
             ...object_error
           }
         });
       });
   }
   handleSubmit1(e){
     const {role_name, role_id} = this.state;
     e.preventDefault();
     var validator = {role_name: {error: false,  message: ''}};
     var object_error = {};
     // console.log(this.props.career_type.data.tcareer_id);
     axios.put(`api/roles/${role_id}`, {role_name}).then(
       res=>{
         this.props.onSuccess(res.data);
         this.props.changeAtribute(false);
        // console.log("se guardo con exito");
        this.handleClose();
       }
     ).catch(error => {
       // console.log(Object.keys(error.response.data.errors));
       this.setState({snack_open: true});
       var errores = Object.keys(error.response.data.errors);
       for (var i = 0; i < errores.length; i++) {
         object_error = {
           ...object_error,
           [errores[i]]: {
             error: true,
             message: error.response.data.errors[errores[i]][0]
           }
         }
       }
       this.setState({
         validator:{
           ...validator,
           ...object_error
         }
       });
     });
   }
   componentDidMount(){
     if (this.props.edit) {
       const {role_name, role_id} = this.props.role.data;
       // axios.get(`http://localhost:8000/api/career_types/${this.props.career_type.data.career_type_id}`).then(res=>{
       //   this.setState({name: res.data.data.name,
       //                  career_type_code: res.data.data.career_type_code,
       //                  career_type_logo: res.data.data.career_type_logo
       //                });
       //   }).catch(error=>{alert(error)});
       this.setState({
         role_name, role_id: role_id
       });
     }
   }
   getStepContent(stepIndex) {
     const {role_name, country_code, open, validator, snack_open} = this.state;
     switch (stepIndex) {
       case 0:
         return (
           <div>
           <form>
             <TextField
                 id="outlined-full-width"
                 error={this.state.validator.role_name.error}
                 helperText={this.state.validator.role_name.message}
                 label="Nombre"
                 style={{ margin: 8 }}
                 value={role_name}
                 name='role_name'
                 onChange={this.fieldsChange.bind(this)}
                 fullWidth
                 margin="normal"
                 variant="outlined"
               />

             <br />
             {this.editOrNew()}
             </form>
           </div>
         );
       case 1:
         return (
           <div>
            <FormPermission role= {this.state.role} onHandleSubmit={()=>this.handleClose()} changeAtribute={this.props.hasChanges}/>
           </div>
         );

       default:
         return 'Unknown stepIndex';
     }
   }
   editOrNew(){
     if (this.props.edit) {
       return(
         <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleSubmit1}>Actualizar</Button>
       );
     }
     else {
       return(
       <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleSubmit}>Guardar</Button>
       );
     }
   }
   render(){
     const steps = getSteps();
     // const { classes } = this.props;
     const {role_name, country_code, open, validator, snack_open} = this.state;


     var showSnack;
     if (snack_open){
           showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                          <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="error">
                            {validator['role_name']['error']?<li>Error: {validator['role_name']['message']}</li>:''}
                          </Alert>
                        </Snackbar>
         }
     return (

        <div>
               <h3 align='center'>Formulario de Roles</h3>
               <hr />
               {!this.props.edit &&
                 <Stepper activeStep={this.state.activeStep} alternativeLabel>
                   {steps.map((label) => (
                     <Step key={label}>
                       <StepLabel>{label}</StepLabel>
                     </Step>
                   ))}
                 </Stepper>
               }


                    {this.getStepContent(this.state.activeStep)}

                  <Button variant="outlined" color="secondary" startIcon={<ExitToAppIcon />} onClick={this.handleClose}>Cerrar</Button>
        </div>
       );
   }
 }
