import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import HomeIcon from '@material-ui/icons/Home';
import ListIcon from '@material-ui/icons/List';
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
  TableSortLabel, AppBar, Tabs, Tab, Typography, List, ListItem, ListItemText,
  Divider, Paper, Button, Snackbar, CircularProgress} from '@material-ui/core';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import Paginator from '../Paginators/Paginator';
import MidModal from '../Modals/MidModal';
import FormPermission from './FormPermission';


export default class RoleShow extends Component {
  constructor(props){
    super(props);
    this.state = {
      role : [],
      permissions: [],
      paginator:[],
      order: 'asc',
      orderBy: 'permission_name',
      rowsPerPage: 10,
      page_lenght: 0,
      page: 1,
      open: false
    }
    this.getObject = async(id, perPage, page, orderBy=this.state.orderBy, order=this.state.order)=>{
      let res = await axios.get(`/api/roles_get_permission/${id}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`);
      let {data} = await res;
      // console.log(res.data.last_page);
      // console.log(res);
      if (this._ismounted) {
        this.setState({permissions: data.data, paginator:res.data, open:false, page_lenght: res.data.last_page, page: res.data.current_page});
      }
    }
    this.updateState= () =>{
      this.setState({
        edit: false,
        _new: false,
        open: false
      });
      this.getObject(this.state.role.role_id, this.state.rowsPerPage, this.state.page);
    }
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
  }
  perPageChange(value){
    // console.log(value);
    this.setState({rowsPerPage: value, page: 1});
    this.getObject(this.state.role.role_id, value, 1);
  }
  pageChange(value){
    this.setState({page: value});
    this.getObject(this.state.role.role_id,this.state.rowsPerPage, value);
  }
  createSortHandler(name){
    var order = '';
    if(this.state.orderBy===name){
      if(this.state.order==='asc'){
        order = 'desc';
        this.setState({order});
      }
      else{
        order = 'asc';
        this.setState({order});
      }
    }
    else{
      order = 'asc';
      this.setState({orderBy: name, order});
    }
      this.getObject(this.state.role.role_id, this.state.rowsPerPage, this.state.page, name, order);
  }
  handleModal(){
    this.setState({open:true});
  }
  componentDidMount(){
    // console.log('llega por aqui');
    this._ismounted = true;
    const id = this.props.match.params.id;
    axios.get(`/api/roles/${id}`).then(response => {
      // console.log(response.data);
      this.setState({
        role: response.data.data
      });
    });

    this.getObject(id ,this.state.rowsPerPage, this.state.page);
  }
  render(){
    const {open, orderBy, order, permissions} = this.state;
    var paginator;
    var showModal;
    if (open) {
        // showModal = <FormPermission onHandleSubmit={this.updateState}/>
        showModal = <MidModal >
                      {{onHandleSubmit: this.updateState,
                        form: <FormPermission />,
                        props_form: {role: this.state.role}
                      }}
                    </ MidModal>
      }
    if(permissions.length === 0){
      paginator =  <div>{''}</div>
    }else{
      paginator = <Paginator
      lenght={this.state.page_lenght}
      perPageChange= {this.perPageChange}
      pageChange ={this.pageChange}
      page= {this.state.page}
      />
    }
    return(
      <div>
        <div className="breadcrumb-container">
          <ul className="breadcrumb-list">
            <li className="breadcrumb-item">
              <Link className='breadcrumb-link'to={"/"}>
                  <HomeIcon className="breadcrumb-icon"/>
                  Inicio
                  <ChevronRightIcon className="breadcrumb-icon"/>
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link className='breadcrumb-link'to={"/roles"}>
                  <ListIcon className="breadcrumb-icon"/>
                  Lista de Roles
                  <ChevronRightIcon className="breadcrumb-icon"/>
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Typography style={{color:"#666"}}>
                <GroupWorkIcon className="breadcrumb-icon"/>
                {this.state.role.role_name}
              </Typography>
            </li>
          </ul>
        </div>
        <div>
        <h2 align='center'>
          Permisos
        </h2>
        {'            '}
        <Button variant="outlined" color="primary" type='submit' startIcon={< PlaylistAddCheckIcon />} onClick={()=>{this.handleModal()}}>
          Gestión de permisos
        </Button>
        <hr />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{search: true}}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === 'permission_name' ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === 'permission_name'}
                    direction={orderBy === 'permission_name' ? order : 'asc'}
                    onClick={()=>{this.createSortHandler('permission_name')}}
                  >
                    <h3>Codigo</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === 'permission_description' ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === 'permission_description'}
                    direction={orderBy === 'permission_description' ? order : 'asc'}
                    onClick={()=>{this.createSortHandler('permission_description')}}
                  >
                    <h3>Description</h3>
                  </TableSortLabel>
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {paginator}
        {showModal}

        </div>
      </div>
    );
  }
  renderList(){
    const {open, role, permissions, rowsPerPage, page} = this.state;
    if(permissions.length === 0){
      return(
        <TableBody>
        <TableRow>
          <TableCell colSpan={2}>{'No contiene datos'}</TableCell>
          </TableRow>
          </TableBody>
      )
    }else{
    return (permissions.map((data)=>{
      return(
    <TableBody key={data.permission_id}>
        <TableRow>
          <TableCell>{data.permission_name}</TableCell>
          <TableCell>{data.permission_description}</TableCell>
        </TableRow>

    </TableBody>
            )
          }
        )
      )
    }
  }//cierra el renderList
}
