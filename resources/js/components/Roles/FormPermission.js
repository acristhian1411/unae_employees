import React, {Component} from 'react';
import {Grid, List, ListItem, ListItemIcon, ListItemSecondaryAction,
        ListItemText, Checkbox, IconButton, TextField, CircularProgress,
        Menu, MenuItem, Button} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SaveIcon from '@material-ui/icons/Save';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Paginator from '../Paginators/Paginator';

export default class FormPermission extends Component {
  constructor(props){
    super(props);
    this.state = {
      time_out: false,
      time_out2: false,
      search: '',
      search2: '',
      page_length: 0,
      page_length2: 0,
      page: 1,
      page2: 1,
      perPage: 10,
      perPage2:10,
      orderBy: 'permission_name',
      orderBy2: 'permission_name',
      order: 'asc',
      order2: 'asc',
      permissions: [],
      permissions2: [],
      permissions_addes: [],
      permissions_addes_id: [],
      permissions_removes: [],
      permissions_removes_id: [],
      open_menu: false,
      anchorElMenu: null,
      anchorElMenu2: null,
      anchorElMenu3: null,
      anchorElMenu4: null,
      menuData: null,
      list_checked: [],
      listCheckedId: [],
      list_checked2: [],
      listCheckedId2: [],
      list_checked3: [],
      listCheckedId3: [],
      list_checked4: [],
      listCheckedId4: [],
    }
    this.getObject = async(perPage, page, orderBy=this.state.orderBy, order=this.state.order, search=this.state.search)=>{
      var s;
        s = `&permission_description=${search}`;
      let res = await axios.get(`/api/roles_get_permission/exclude/${this.props.role.role_id}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}${s}`);
      let {data} = await res;
      // console.log(res.data.last_page);
      // console.log(res);
      if (this._ismounted) {
        this.setState({permissions: data.data, paginator:res.data, open:false, page_length: res.data.last_page, page: res.data.current_page, time_out: false});
      }
    }
    this.getObject2 = async(perPage, page, orderBy=this.state.orderBy2, order=this.state.order2, search=this.state.search2)=>{
      var s;
        s = `&permission_description=${search}`;
      let res = await axios.get(`/api/roles_get_permission/${this.props.role.role_id}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}${s}`);
      let {data} = await res;
      // console.log(res.data.last_page);
      // console.log(res);
      if (this._ismounted) {
        this.setState({permissions2: data.data, paginator:res.data, open:false, page_length2: res.data.last_page, page2: res.data.current_page, time_out2: false});
      }
    }
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);
    this.search2 = this.search2.bind(this);
    this.perPageChange2 = this.perPageChange2.bind(this);
    this.pageChange2 = this.pageChange2.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleCheck2 = this.handleCheck2.bind(this);
    this.handleCheck3 = this.handleCheck3.bind(this);
    this.handleCheck4 = this.handleCheck4.bind(this);
  }
  componentDidMount(){
    this._ismounted = true;
    this.getObject(this.state.perPage, this.state.page);
    this.getObject2(this.state.perPage2, this.state.page2);
  }

  perPageChange(value){
    // console.log(value);
    this.setState({perPage: value, page: 1});
    this.getObject(value, 1);
  }
  pageChange(value){
    this.setState({page: value});
    this.getObject(this.state.perPage, value);
  }
  perPageChange2(value){
    // console.log(value);
    this.setState({perPage2: value, page2: 1});
    this.getObject2(value, 1);
  }
  pageChange2(value){
    this.setState({page2: value});
    this.getObject2(this.state.perPage2, value);
  }
  search(event){
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out==false) {
      this.setState({time_out:true});
      setTimeout(()=>{
        if(e.target.value===''){
              this.setState({search:''});
              this.getObject(this.state.perPage, 1, this.state.orderBy, this.state.order, '');
            }
            else {
              this.setState({search: e.target.value});
              this.getObject(this.state.perPage, this.state.page, this.state.orderBy, this.state.order, e.target.value);
        }}
      , 3000);
    }
  }
  search2(event){
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out2==false) {
      this.setState({time_out2:true});
      setTimeout(()=>{
        if(e.target.value===''){
              this.setState({search2:''});
              this.getObject2(this.state.perPage2, 1, this.state.orderBy2, this.state.order2, '');
            }
            else {
              this.setState({search2: e.target.value});
              this.getObject2(this.state.perPage2, this.state.page2, this.state.orderBy2, this.state.order2, e.target.value);
        }}
      , 3000);
    }
  }
  handleCloseMenu(){
    this.setState({anchorElMenu: null, anchorElMenu2: null, anchorElMenu3: null, anchorElMenu4: null, menuData: null});
  }
  asignePermission(e){
    // console.log(this.state.menuData);
    var indexOf = this.state.permissions_addes_id.indexOf(this.state.menuData.permission_id);
    if (indexOf === -1) {
      this.setState({permissions_addes: [...this.state.permissions_addes,
                                         this.state.menuData],
                     permissions_addes_id:[...this.state.permissions_addes_id,
                                            this.state.menuData.permission_id],
                     menuData: null,
                     anchorElMenu: null
                     });
    }
    else {
      this.setState({anchorElMenu: null});
    }
  }
  removePermission(e){
    // console.log(this.state.menuData);
    var indexOf = this.state.permissions_removes_id.indexOf(this.state.menuData.permission_id);
    if (indexOf === -1) {
      this.setState({permissions_removes: [...this.state.permissions_removes,
                                         this.state.menuData],
                     permissions_removes_id:[...this.state.permissions_removes_id,
                                            this.state.menuData.permission_id],
                     menuData: null,
                     anchorElMenu2: null
                     });
    }
    else {
      this.setState({anchorElMenu2: null});
    }
  }
  cancelRemovePermissions(){
    var array_checked = this.state.list_checked3;
    var array_checked_id =  this.state.listCheckedId3;
    var state_permission_removes = this.state.permissions_removes;
    var state_permission_removes_id = this.state.permissions_removes_id;
        for (var i = 0; i < array_checked.length; i++) {
          var currentIndex = this.state.permissions_removes.indexOf(array_checked[i]);
          var currentIndex1 = this.state.permissions_removes_id.indexOf(array_checked_id[i]);
          state_permission_removes_id.splice(currentIndex1, 1);
          state_permission_removes.splice(currentIndex, 1);
        }
        this.setState({list_checked3: [], listCheckedId3: [], permissions_removes: state_permission_removes,
                       permissions_removes_id: state_permission_removes_id, anchorElMenu3: null, menuData:null});

    // console.log(this.state.list_checked3);
  }
  cancelAddPermissions(){
    var array_checked = this.state.list_checked4;
    var array_checked_id =  this.state.listCheckedId4;
    var state_permission_addes = this.state.permissions_addes;
    var state_permission_addes_id = this.state.permissions_addes_id;
        for (var i = 0; i < array_checked.length; i++) {
          var currentIndex = this.state.permissions_addes.indexOf(array_checked[i]);
          var currentIndex1 = this.state.permissions_addes_id.indexOf(array_checked_id[i]);
          state_permission_addes_id.splice(currentIndex1, 1);
          state_permission_addes.splice(currentIndex, 1);
        }
        this.setState({list_checked4: [], listCheckedId4: [], permissions_addes: state_permission_addes,
                       permissions_addes_id: state_permission_addes_id, anchorElMenu3: null, menuData:null});

    // console.log(this.state.list_checked3);
  }
  addPermissions(){
    this.setState({permissions_addes: [...this.state.permissions_addes,
                                            ...this.state.list_checked],
                   permissions_addes_id: [...this.state.permissions_addes_id,
                                                 ...this.state.listCheckedId],
                   list_checked: [],
                   listCheckedId: []});
  }
  removePermissions(){
    this.setState({permissions_removes: [...this.state.permissions_removes,
                                            ...this.state.list_checked2],
                   permissions_removes_id: [...this.state.permissions_removes_id,
                                                 ...this.state.listCheckedId2],
                   list_checked2: [],
                   listCheckedId2: []});
  }
  handleCheck(value){
    // console.log(value);
    var array_checked_id = this.state.listCheckedId;
    var array_checked = this.state.list_checked;
    var currentIndex = array_checked.indexOf(value.value);
    var currentIndex1 = array_checked_id.indexOf(value.value.permission_id);
    if (currentIndex1 === -1) {
      array_checked_id.push(value.value.permission_id);
      array_checked.push(value.value);
    }
    else {
      array_checked_id.splice(currentIndex1, 1);
      array_checked.splice(currentIndex, 1);
    }
    this.setState({list_checked: array_checked, listCheckedId: array_checked_id, anchorElMenu: null, menuData:null});
  }
  handleCheck2(value){
    // console.log(value);
    var array_checked_id = this.state.listCheckedId2;
    var array_checked = this.state.list_checked2;
    var currentIndex = array_checked.indexOf(value.value);
    var currentIndex1 = array_checked_id.indexOf(value.value.permission_id);
    if (currentIndex1 === -1) {
      array_checked_id.push(value.value.permission_id);
      array_checked.push(value.value);
    }
    else {
      array_checked_id.splice(currentIndex1, 1);
      array_checked.splice(currentIndex, 1);
    }
    this.setState({list_checked2: array_checked, listCheckedId2: array_checked_id, anchorElMenu2: null, menuData:null});
  }
  handleCheck3(value){
    var array_checked_id = this.state.listCheckedId3;
    var array_checked = this.state.list_checked3;
    var currentIndex = array_checked.indexOf(value.value);
    var currentIndex1 = array_checked_id.indexOf(value.value.permission_id);
    if (currentIndex1 === -1) {
      array_checked_id.push(value.value.permission_id);
      array_checked.push(value.value);
    }
    else {
      array_checked_id.splice(currentIndex1, 1);
      array_checked.splice(currentIndex, 1);
    }
    this.setState({list_checked3: array_checked, listCheckedId3: array_checked_id, anchorElMenu3: null, menuData:null});
  }
  handleCheck4(value){
    var array_checked_id = this.state.listCheckedId4;
    var array_checked = this.state.list_checked4;
    var currentIndex = array_checked.indexOf(value.value);
    var currentIndex1 = array_checked_id.indexOf(value.value.permission_id);
    if (currentIndex1 === -1) {
      array_checked_id.push(value.value.permission_id);
      array_checked.push(value.value);
    }
    else {
      array_checked_id.splice(currentIndex1, 1);
      array_checked.splice(currentIndex, 1);
    }
    this.setState({list_checked4: array_checked, listCheckedId4: array_checked_id, anchorElMenu4: null, menuData:null});
  }
  savePermission(){
    // console.log(this.state.permissions_addes);
    const {permissions_addes, permissions_removes} = this.state;
    const {role_id} = this.props.role;
    if (permissions_addes.length !== 0) {
      axios.post('/api/roles_give_permissions', {permissions: permissions_addes, role_id}).then(res=>{
        // console.log(res.data);
        this.setState({permissions_addes: [], permissions_addes_id: []});
        this.getObject(this.state.perPage, this.state.page);
        this.getObject2(this.state.perPage2, this.state.page2);
      });
    }
    if (permissions_removes.length !== 0) {
      axios.post('/api/roles_revoke_permissions', {permissions: permissions_removes, role_id}).then(res=>{
        // console.log(res.data);
        this.setState({permissions_removes: [], permissions_removes_id: []});
        this.getObject(this.state.perPage, this.state.page);
        this.getObject2(this.state.perPage2, this.state.page2);
      });
    }



  }
  render(){
    var paginator, paginator2, menu, menu2;
    var {role} = this.props;
    if(this.state.permissions.length === 0){
      paginator =  <div>{''}</div>
    }else{
      paginator = <Paginator
      lenght={this.state.page_length}
      perPageChange= {this.perPageChange}
      pageChange ={this.pageChange}
      page= {this.state.page}
      />
    }
    if(this.state.permissions2.length === 0){
      paginator2 =  <div>{''}</div>
    }else{
      paginator2 = <Paginator
      lenght={this.state.page_length2}
      perPageChange= {this.perPageChange2}
      pageChange ={this.pageChange2}
      page= {this.state.page2}
      />
    }
    menu = <Menu
        id="simple-menu"
        anchorEl={this.state.anchorElMenu}
        keepMounted
        open={Boolean(this.state.anchorElMenu)}
        onClose={()=>this.handleCloseMenu()}
      >
        <MenuItem onClick={()=>this.asignePermission()}>Asignar</MenuItem>
        <MenuItem onClick={()=>this.handleCheck({value: this.state.menuData})}>Seleccionar</MenuItem>
        {/*<MenuItem onClick={()=>this.handleCloseMenu()}>Logout</MenuItem>*/}
      </Menu>;
    menu2 = <Menu
        id="simple-menu"
        anchorEl={this.state.anchorElMenu2}
        keepMounted
        open={Boolean(this.state.anchorElMenu2)}
        onClose={()=>this.handleCloseMenu()}
      >
        <MenuItem onClick={()=>this.removePermission()}>Quitar</MenuItem>
        <MenuItem onClick={()=>this.handleCheck2({value: this.state.menuData})}>Seleccionar</MenuItem>
        {/*<MenuItem onClick={()=>this.handleCloseMenu()}>Logout</MenuItem>*/}
      </Menu>;

    return(
      <div>
        <h3 align='center'>{`Permisos para el rol: ${role.role_name}`}
        </h3>
        <hr />
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
              <Grid item xs={5}>
                <div className='box-list-simple'>
                   <h3 align='center'>
                      Permisos no asignados

                   </h3>
                   <TextField
                   id="outlined-search"
                   label="Buscar"
                   type="search"
                   variant="outlined"
                   fullWidth
                   onChange={this.search}
                   InputProps={{
                     endAdornment: (
                       <React.Fragment>
                         {this.state.time_out === true ? <CircularProgress color="inherit" size={20} /> : null}
                       </React.Fragment>
                     ),
                   }}
                   />
                   {this.state.permissions_removes.length !== 0 &&

                       <fieldset className='box-list-child'>
                         <legend>Permisos quitados sin guardar:</legend>
                         <List>
                           {this.state.permissions_removes.map((value) => {
                             const labelId = `checkbox-list-label-${value}`;

                             return (
                               <ListItem key={value.permission_id} role={undefined} dense button onClick={()=>this.handleCheck3({value})}>
                                 <ListItemIcon>
                                   <Checkbox
                                     edge="start"
                                     checked={this.state.listCheckedId3.indexOf(value.permission_id) !== -1}
                                     tabIndex={-1}
                                     disableRipple
                                     inputProps={{ 'aria-labelledby': labelId }}
                                   />
                                 </ListItemIcon>
                                 <ListItemText id={labelId} primary={value.permission_description} />
                               </ListItem>
                             );
                           })}
                         </List>
                       </fieldset>
                   }
                   <List>
                     {this.state.permissions.map((value) => {
                       const labelId = `checkbox-list-label-${value}`;
                       if (this.state.permissions_addes_id.indexOf(value.permission_id) !== -1) {
                         return '';
                       }
                       else {
                         return (

                           <ListItem key={value.permission_id} role={undefined} dense button onClick={()=>this.handleCheck({value})}>
                             <ListItemIcon>
                               <Checkbox
                                 edge="start"
                                 checked={this.state.listCheckedId.indexOf(value.permission_id) !== -1}
                                 tabIndex={-1}
                                 disableRipple
                                 inputProps={{ 'aria-labelledby': labelId }}
                               />
                             </ListItemIcon>
                             <ListItemText id={labelId} primary={value.permission_description} />
                             <ListItemSecondaryAction>
                               <IconButton edge="end" aria-label="comments" onClick={(event, data)=>this.setState({anchorElMenu:event.currentTarget, menuData: value})}>
                                 <MoreVertIcon />
                               </IconButton>
                               {menu}
                             </ListItemSecondaryAction>
                           </ListItem>
                         );
                       }
                     })}
                   </List>
                </div>
                {paginator}
              </Grid>
              <Grid item xs={2} align= 'center'>
              <IconButton edge="end"
                          color="primary"
                          aria-label="comments"
                          onClick={()=>this.addPermissions()}
                          disabled={this.state.list_checked.length === 0}
                          >
                <ArrowForwardIcon />
              </IconButton>

              <br/>
              <Button variant="outlined"
                      color="primary"
                      type='submit'
                      onClick={()=>this.savePermission()}
                      disabled={this.state.permissions_addes.length === 0 && this.state.permissions_removes.length === 0}
                      >
                  <SaveIcon />
              </Button>
              <br/>
              <IconButton edge="end"
                          color="primary"
                          aria-label="comments"
                          onClick={()=>this.removePermissions()}
                          disabled={this.state.list_checked2.length === 0}
                          >
                <ArrowBackIcon />
              </IconButton>
              <br/>
              {this.state.list_checked3.length !==0 &&
                <Button variant="outlined" color="primary" type='submit' onClick={()=>this.cancelRemovePermissions()}>Volver a asignar</Button>
              }
              <br/>
              {this.state.list_checked4.length !==0 &&
                <Button variant="outlined" color="primary" type='submit' onClick={()=>this.cancelAddPermissions()}>Volver a quitar</Button>
              }
              </Grid>
              <Grid item xs={5}>
                <div className='box-list-simple'>
                  <h3 align='center'>Permisos asignados</h3>
                  <TextField
                  id="outlined-search"
                  label="Buscar"
                  type="search"
                  variant="outlined"
                  fullWidth
                  onChange={this.search2}
                  InputProps={{
                    endAdornment: (
                      <React.Fragment>
                        {this.state.time_out2 === true ? <CircularProgress color="inherit" size={20} /> : null}
                      </React.Fragment>
                    ),
                  }}
                  />
                  {this.state.permissions_addes.length !== 0 &&
                    <fieldset className='box-list-child'>
                      <legend>Permisos agregados sin guardar:</legend>
                      <List>
                        {this.state.permissions_addes.map((value) => {
                          const labelId = `checkbox-list-label-${value}`;

                          return (
                            <ListItem key={value.permission_id} role={undefined} dense button onClick={()=>this.handleCheck4({value})}>
                              <ListItemIcon>
                                <Checkbox
                                  edge="start"
                                  checked={this.state.listCheckedId4.indexOf(value.permission_id) !== -1}
                                  tabIndex={-1}
                                  disableRipple
                                  inputProps={{ 'aria-labelledby': labelId }}
                                />
                              </ListItemIcon>
                              <ListItemText id={labelId} primary={value.permission_description} />
                            </ListItem>
                          );
                        })}
                      </List>
                    </fieldset>
                  }
                  <List>
                    {this.state.permissions2.map((value) => {
                      const labelId = `checkbox-list-label-${value}`;
                      if (this.state.permissions_removes_id.indexOf(value.permission_id) !== -1) {
                        return '';
                      }
                      else {
                      return (
                        <ListItem key={value.permission_id} role={undefined} dense button onClick={()=>this.handleCheck2({value})}>
                          <ListItemIcon>
                            <Checkbox
                              edge="start"
                              checked={this.state.listCheckedId2.indexOf(value.permission_id) !== -1}
                              tabIndex={-1}
                              disableRipple
                              inputProps={{ 'aria-labelledby': labelId }}
                            />
                          </ListItemIcon>
                          <ListItemText id={labelId} primary={value.permission_description} />
                          <ListItemSecondaryAction>
                            <IconButton edge="end" aria-label="comments" onClick={(event, data)=>this.setState({anchorElMenu2:event.currentTarget, menuData: value})}>
                              <MoreVertIcon />
                            </IconButton>
                            {menu2}
                          </ListItemSecondaryAction>
                        </ListItem>
                      );
                    }})}
                  </List>
               </div>
               {paginator2}
              </Grid>

          </Grid>
        </Grid>
      </div>
    );
  }
}
