import React, {Component, lazy} from 'react';
import {Link} from 'react-router-dom';
import MidModal from '../Modals/MidModal';
import FormRole from './FormRole';
import Paginator from '../Paginators/Paginator';
import DialogDestroy from '../Dialogs/DialogDestroy';
// const FormRole = lazy(()=> import('./FormRole'));
// const DialogDestroy = lazy(()=> import('../Dialogs/DialogDestroy'));
import {Button, Dialog, DialogActions, DialogTitle, Icon, Table,
  TableBody, TableCell, TableContainer, TableHead, TableRow,
  TablePagination, Paper, TextField, MobileStepper,
  Select, MenuItem, TableSortLabel, Snackbar, CircularProgress} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';

export default class Roles extends Component{
  constructor(props){
      super(props);
      this.state = {
        snack_open: false,
        message_success: '',
        roles:[],
        filterRoles:[],
        id: 0,
        role:[],
        edit: false,
        _new: false,
        open: false,
        paginator:[],
        rowsPerPage: 10,
        order: 'asc',
        orderBy: 'role_name',
        page_lenght: 0,
        page: 1,
        time_out: false,
        permissions:{
          roles_index: null,
          roles_show: null,
          roles_store: null,
          roles_update: null,
          roles_destroy: null,
        }
      }
      // this.handleClickOpen = (data) =>{
      //   this.setState({open: true, role: data});
      // }
      this.handleClose = () =>{
        this.setState({open: false});
      }
      this.getObject = async(perPage, page, orderBy=this.state.orderBy, order=this.state.order)=>{
        let res = await axios.get(`/api/roles?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`);
        let {data} = await res;
        // console.log(res.data.last_page);
        // console.log(res);
        if (this._ismounted) {
          this.setState({roles: data, paginator:res.data, filterRoles: data.data, open:false, page_lenght: res.data.last_page, page: res.data.current_page});
        }
      }
      this.updateState= () =>{
        this.setState({
          edit: false,
          _new: false,
          open: false
        });
        this.getObject( this.state.rowsPerPage, this.state.page);
      }

        this.destroy = this.destroy.bind(this);
        this.createSortHandler = this.createSortHandler.bind(this);
        this.perPageChange = this.perPageChange.bind(this);
        this.pageChange = this.pageChange.bind(this);
        this.closeSnack = this.closeSnack.bind(this);
        this.openSnack = this.openSnack.bind(this);
        this.search = this.search.bind(this);
    }

  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("ROLES");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = ['roles_index', 'roles_show', 'roles_store', 'roles_update', 'roles_destroy'];
    axios.get(`/api/roles_has_permission`, {params: {
                                            permission: permissions
                                          }}).then(res=>{
      // console.log('algo');
      this.setState({permissions: res.data});
      if (res.data!==false) {
        this.getObject( this.state.rowsPerPage, this.state.page);
      }
    });

  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack(){
    this.setState({snack_open: false});
  }
  openSnack(param){
    // console.log(param);
    this.setState({snack_open: true, message_success: param});
  }
  handleModal(){
    this.setState({_new:true});
  }
  handleClickOpen(data){
    this.setState({open: true, role: data});
  }
  searchChange(e){
    this.setState({search:e.value});
  }
  editModal(data){
    this.setState({edit: true, role: data})
  }
  destroy(){
     // event.preventDefault();
    axios.delete(`/api/roles/${this.state.id}`).then(
      res=>{
        this.setState({snack_open: true, message_success: res.data});
        this.updateState();
      }
    );
  }

  search(event){
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out==false) {
      this.setState({time_out:true});
      // setTimeout(()=>{
        if(e.target.value===''){
              this.setState({
                filterRoles:this.state.roles.data,
                paginator: this.state.roles,
                page_lenght: this.state.roles.last_page,
                page: 1,
                time_out:false
              });
            }
            else {
          axios.get(`/api/roles/search/${e.target.value}?sort_by=${this.state.orderBy}&order=${this.state.order}&per_page=${this.state.rowsPerPage}&page=${1}`).then(res=>{
                  this.setState({
                    filterRoles: res.data.data,
                    paginator: res.data,
                    page_lenght: res.data.last_page,
                    page: res.data.current_page,
                    time_out:false
                  });
                }).catch(error=>{
                  this.setState({time_out: false});
                  console.log("Ingrese un valor valido");
                });
        }
    //   , 3000);
    // }

  }


  createSortHandler(name){
    var order = '';
    if(this.state.orderBy===name){
      if(this.state.order==='asc'){
        order = 'desc';
        this.setState({order});
      }
      else{
        order = 'asc';
        this.setState({order});
      }
    }
    else{
      order = 'asc';
      this.setState({orderBy: name, order});
    }
      this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value){
    // console.log(value);
    this.setState({rowsPerPage: value, page: 1});
    this.getObject(value, 1);
  }
  pageChange(value){
    this.setState({page: value});
    this.getObject(this.state.rowsPerPage, value);
  }

  render(){
    const {snack_open, message_success, open,_new, edit, role, filterRoles, rowsPerPage,  orderBy, order, permissions} = this.state;
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
      if (_new) {
        // showModal = <FormRole onHandleSubmit={this.updateState}/>
        showModal = <MidModal >
                      {{onHandleSubmit: this.updateState,
                        form: <FormRole />,
                        props_form: {onSuccess:this.openSnack}
                      }}
                    </ MidModal>
      }
      else if (edit) {
        // showModal = <FormRole edit ={true} role={role} onHandleSubmit={this.updateState}/>
        showModal = <MidModal >
                      {{onHandleSubmit: this.updateState,
                        form: <FormRole />,
                        props_form: {edit: true, role:role, onSuccess:this.openSnack}
                      }}
                    </ MidModal>
      }
      if(open) {
        showDialogDestroy = <DialogDestroy index={role.data.name} onClose={this.handleClose} onHandleAgree={this.destroy}/>;
      }
      if (snack_open){
            showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                           <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="success">
                             {message_success}
                           </Alert>
                         </Snackbar>
          }
          if(filterRoles.length === 0){
            paginator =  <div>{''}</div>
          }else{
            paginator = <Paginator
            lenght={this.state.page_lenght}
            perPageChange= {this.perPageChange}
            pageChange ={this.pageChange}
            page= {this.state.page}
            />
          }
    return(
      <div  style={{textAlign: "center"}}>
        <TextField
        id="outlined-search"
        label="Escriba para buscar"
        type="search"
        variant="outlined"
        // onChange={this.search}
        onKeyDown={(e) =>{
          if(e.key === 'Enter'){
            this.search(e)
          }
        }}
        style={{ width: "40%"}}
        InputProps={{
          endAdornment: (
            <React.Fragment>
              {this.state.time_out === true ? <CircularProgress color="inherit" size={20} /> : null}
            </React.Fragment>
          ),
        }}
        />

      <TableContainer component={Paper}>
        <Table aria-label="simple table" option={{search: true}}>
          <TableHead>
            <TableRow>
              <TableCell
                sortDirection={orderBy === 'name' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'name'}
                  direction={orderBy === 'name' ? order : 'asc'}
                  onClick={()=>{this.createSortHandler('name')}}
                >
                  <h3>Nombre</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell colSpan="2">
              {permissions.roles_store === true &&
                <Button variant="outlined" color="primary" type='submit' onClick={()=>{this.handleModal()}}>
                  < AddBoxOutlinedIcon />
                </Button>
              }
              </TableCell>
            </TableRow>
          </TableHead>
          {this.renderList()}
        </Table>
      </TableContainer>
        {/* <Paginator
          lenght={this.state.page_lenght}
          perPageChange= {this.perPageChange}
          pageChange ={this.pageChange}
          page= {this.state.page}
          /> */}
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }
  selectValue(event){
      this.setState({search: event.target.value})
    };
  renderList(){
    const {open, role, filterRoles, rowsPerPage, page, permissions} = this.state;
    if(filterRoles.length === 0){
      return(
        <TableBody>
        <TableRow>
          <TableCell>{' '}</TableCell>
          <TableCell>{'No contiene datos'}</TableCell>
          <TableCell>{' '}</TableCell>
          <TableCell>{''}</TableCell>
          </TableRow>
          </TableBody>
      )
    }else{
    return (filterRoles.map((data)=>{
      return(
    <TableBody key={data.role_id}>
        <TableRow>
          <TableCell>{data.role_name}</TableCell>
          <TableCell>
          {permissions.roles_show === true &&
            <Link className='navbar-brand'to={"/roles/show/"+ data.role_id}>
              <Button variant="outlined" color="primary" startIcon={< VisibilityIcon />} >{' '}</Button>
            </Link>
          }
          </TableCell>
          <TableCell>
          {permissions.roles_update === true &&
            <Button variant="outlined" color="primary" type='submit' onClick={()=>{this.editModal({data: data})}}>
            < EditIcon />
            </Button>
          }
          </TableCell>
          <TableCell>
          {permissions.roles_destroy === true &&
            <Button variant="outlined" color="secondary" onClick={()=>{this.handleClickOpen({data: data}), this.setState({id: data.role_id})}}>
              <DeleteIcon />
            </Button>
          }
          </TableCell>
        </TableRow>

    </TableBody>
            )
          }
        )
      )
    }
  }//cierra el renderList
}
