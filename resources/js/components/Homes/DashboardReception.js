import React, {Component} from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { Snackbar, Grid } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import {getPerson} from '../../contexts';
import MidModal from '../Modals/MidModal';
import ModalForm from '../Students/ModalForm';

export default class DashboardRecepcion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: '',
      _new: false
    }
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.updateState = this.updateState.bind(this);
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("MENÚ DE RECEPCIÓN");
    var element = document.getElementById('page');
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  updateState(){
    // e.preventDefault();
    this.setState({_new: false});
  }
  closeSnack(){
    this.setState({snack_open: false});
  }
  openSnack(param){
    // console.log(param);
    this.setState({snack_open: true, message_success: param});
  }

  render(){
    var showSnack, showModal;
    const {snack_open, message_success, _new} = this.state;
    if (snack_open){
          showSnack = <Snackbar anchorOrigin={{vertical: 'bottom', horizontal: 'center'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                         <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="error">
                           {message_success}
                         </Alert>
                       </Snackbar>
        }
    if (_new) {
      showModal = <MidModal >
                    {{onHandleSubmit: this.updateState,
                      form: <ModalForm />,
                      props_form: {onSuccess:this.openSnack}
                    }}
                  </ MidModal>
    }
    return(
      <getPerson.Consumer>{(context)=>{
          return(
            <div className="container-home">
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={0} style={{justifyContent:'center', verticalAlign: 'middle'}} >
                <Grid item xs={'auto'}>
                  <div className='dashboard-button' onClick={()=>{this.setState({_new: true})}}>
                    <img className="dashoard-img-button" src={`img/dashboard/icons/new_person.jpeg`} /><br/>
                    <p>NUEVO ESTUDIANTE</p>
                  </div>
                </Grid>
                <Grid item xs={'auto'}>
                  <Link className="dashboard-link-button"
                   to={`/consultas`}>
                  <div className='dashboard-button'>
                    <img className="dashoard-img-button" src={`img/dashboard/inscripciones.jpeg`} /><br/>
                    <p>CONSULTAS</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={'auto'}>
                  <Link className="dashboard-link-button"
                   to={`/documentos_requeridos`}>
                  <div className='dashboard-button'>
                    <img className="dashoard-img-button" src={`img/dashboard/icons/documentos.png`} /><br/>
                    <p>DOCUMENTOS</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={'auto'}>
                  <Link className="dashboard-link-button" to={`/carreras`}>
                  <div className='dashboard-button'>
                    <img className="dashoard-img-button" src={`img/dashboard/icons/atom.png`} /><br/>
                    <p>CARRERAS</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"} >
                  <Link
                    className="dashboard-link-button"
                    to={`/tarifas`}
                  >
                    <div
                      className="dashboard-button"
                      // onClick={() => alert("aun se encuentra en desarrollo")}
                    >
                      <img className="dashoard-img-button" src={`img/dashboard/icons/tarifas.png`} />
                      <br />
                      <p>TARIFAS</p>
                    </div>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
              <br/>
              {showSnack}
              {showModal}
            </div>
          )}}
      </getPerson.Consumer>
    );
  }
}
