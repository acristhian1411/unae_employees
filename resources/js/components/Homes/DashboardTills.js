import React, {Component} from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { Snackbar, Grid } from '@material-ui/core';
// const ModalForm = lazy(()=> import('./ModalForm'));
import { Alert } from '@material-ui/lab';

import {getPerson} from '../../contexts';
import MidModalRequire from "../Modals/MidModalRequire";
import TillStart from "../TillsDetails/TillStart";
import TillRestart from "../Tills/TillRestart";
// import FormEnrolled from "../Enrolleds/FormEnrolled";
import MidModal from "../Modals/MidModal";


export default class DashboardTills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: '',
      till_start: false,
      till_start_det: [],
      form_enroll: false,
      action_till: 0,
    }
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.redirectStudent = this.redirectStudent.bind(this);
    this.updateState = () => {
      axios.get('/api/till_is_starting').then(res=>{
        // console.log(res.data);
        if (res.data[0].till_starting === true) {
          var fecha_hoy = new Date();
          var fecha_hoy_format = fecha_hoy.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ fecha_hoy.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + fecha_hoy.toLocaleDateString("en-US", { day: '2-digit' });
          var fecha_apertura = new Date(res.data[0].update);
          var fecha_apertura_format = fecha_apertura.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ fecha_apertura.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + fecha_apertura.toLocaleDateString("en-US", { day: '2-digit' });
          if (fecha_hoy_format === fecha_apertura_format) {
            if (res.data[0].till_type == 1) {
              // console.log("tiene caja normal con apertura de hoy");
            }else if ([2,3].includes(res.data[0].till_type)){
              // console.log("tiene caja continua con apertura de hoy");
            }else if (res.data[0].till_type == 3){
              // console.log("tiene caja banco con apertura de hoy");
            }else{
              // console.log("ocurrio un error inesperado DashboardAdmin linea 42");
            }
          }else{
            // console.log("la fecha de hoy es distinta a la fecha de apertura de caja");
          }
          this.setState({till_start: false, form_enroll: false, till_start_det: res.data[0]});
        }
        else {
          this.setState({till_start: true, form_enroll: false});
        }
      }).catch(error=>{
        console.log(error);
      })
      // this.getObject(this.state.rowsPerPage, this.state.page);
    };
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("MENÚ DE CAJA");
    axios.get('/api/till_is_starting').then(res=>{
      console.log(res.data);
      if (res.data[0].till_starting === true) {
          var fecha_hoy = new Date();
          var fecha_hoy_format = fecha_hoy.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ fecha_hoy.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + fecha_hoy.toLocaleDateString("en-US", { day: '2-digit' });
          var fecha_apertura = new Date(res.data[0].updated_at);
          var fecha_apertura_format = fecha_apertura.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ fecha_apertura.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + fecha_apertura.toLocaleDateString("en-US", { day: '2-digit' });
          if (fecha_hoy_format === fecha_apertura_format) {
            this.setState({till_start: false, form_enroll: false, till_start_det: res.data[0]});
          }else{
            if (res.data[0].till_type == 1) {
              console.log("tiene caja normal, reabrir caja");
              this.setState({till_start: false, form_enroll: false, till_start_det: res.data[0], action_till: 1});
            }else if (res.data[0].till_type == 2){
              console.log("tiene caja continua, preguntar si seguir abierto");
              this.setState({till_start: false, form_enroll: false, till_start_det: res.data[0], action_till: 2});
            }else if (res.data[0].till_type == 3){
              console.log("tiene caja banco");
              this.setState({till_start: false, form_enroll: false, till_start_det: res.data[0]});
            }else{
              console.log("ocurrio un error inesperado DashboardAdmin linea 42");
            }
          }
          
      }
      else {
        this.setState({till_start: true,  form_enroll: false});
      }
    }).catch(error=>{
      console.log(error);
    });
    var element = document.getElementById('page');
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack(){
    this.setState({snack_open: false});
  }
  openSnack(message, result = 'success') {
    // console.log(param);
    this.setState({ snack_open: true, message_success: message, snack_alert: result });
  }
  redirectStudent(){
      this.setState({form_enroll: false});
  }
  render(){
    var showSnack;
    var formActionTill;
    const {snack_open, message_success, action_till} = this.state;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity={this.state.snack_alert}
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if(action_till ===0){
      formActionTill =  "";
    }else if(action_till ===1){
      formActionTill =  <MidModal>
                          {{onHandleSubmit: this.updateState,
                            form: <TillRestart />,
                            props_form: {onSuccess:(()=>{this.setState({action_till:0})}), till: this.state.till_start_det}
                          }}
                        </MidModal>;
    }else if(action_till ===2){
      formActionTill =  <MidModal>
                          {{onHandleSubmit: this.updateState,
                            form: <TillRestart />,
                            props_form: {onSuccess:(()=>{this.setState({action_till:0})}), till: this.state.till_start_det}
                          }}
                        </MidModal>;
    }else{

    }
    return(
      <getPerson.Consumer>{(context)=>{
          return(
            <div className="container-home">
            <Grid container spacing={1}>
              <Grid
                container
                item
                xs={12}
                spacing={0}
                style={{ justifyContent: "center", verticalAlign: "middle" }}
              >
              <Grid item xs={"auto"}>
                <Link
                  className="dashboard-link-button"
                  onClick={(event) => {
                    if (this.state.till_start_det.till_id === undefined) {
                      event.preventDefault();
                      this.openSnack(
                        "No tiene una caja abierta", "error"
                      );
                      this.setState({till_start: true});
                    }
                  }}
                  to={`/cajas/show/${this.state.till_start_det.till_id}`}
                >
                  <div
                    className="dashboard-button"
                    // onClick={() => alert("aun se encuentra en desarrollo")}
                  >
                    <img className="dashoard-img-button" src={`img/dashboard/icons/caja.jpeg`} />
                    <br />
                    <p>MI CAJA</p>
                  </div>
                </Link>
              </Grid>
              <Grid item xs={'auto'}>
                <div className='dashboard-button' onClick={()=>{this.setState({_new: true})}}>
                  <img className="dashoard-img-button" src={`img/dashboard/icons/new_person.jpeg`} /><br/>
                  <p>NUEVO ESTUDIANTE</p>
                </div>
              </Grid>
              <Grid item xs={"auto"}>
                <Link
                  className="dashboard-link-button"
                  onClick={(event) => {
                    if (context.person_id === null) {
                      event.preventDefault();
                      location.replace("/#/ficha_cobro/buscar");
                      // this.openSnack('Elige una persona que sea estudiante');
                    }
                  }}
                  to={`/ficha_cobro/${context.person_id}`}
                >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/cobros.jpeg`} />
                    <br />
                    <p>COBROS</p>
                  </div>
                </Link>
              </Grid>
                <Grid item xs={"auto"} >
                    <Link
                      className="dashboard-link-button"
                      to={`/monitoreo/administrativo`}
                    >
                      <div
                        className="dashboard-button"
                        // onClick={() => alert("aun se encuentra en desarrollo")}
                      >
                        <img className="dashoard-img-button" src={`img/dashboard/informes.jpeg`} />
                        <br />
                        <p>ESTADOS DE CUENTAS</p>
                      </div>
                    </Link>
                  </Grid>
                <Grid item xs={"auto"}>
                <Link
                className="dashboard-link-button"
                  onClick={(event) => {
                    if (context.person_id === null) {
                      event.preventDefault();
                      location.replace("/#/historial_pago");
                      // this.openSnack('Elige una persona que sea estudiante');
                    }
                    else {
                      event.preventDefault();
                      this.setState({form_enroll:true});
                      // ()=> this.openForm();
                    }
                  }}
                  to={`/#/historial_pago`}
                >
                    <div className="dashboard-button">
                      <img className="dashoard-img-button" src={`img/dashboard/icons/recepcion.png`} />
                      <br />
                      <p>HISTORIAL</p>
                    </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"} >
                  <Link
                    className="dashboard-link-button"
                    to={`/tarifas`}
                  >
                    <div
                      className="dashboard-button"
                      // onClick={() => alert("aun se encuentra en desarrollo")}
                    >
                      <img className="dashoard-img-button" src={`img/dashboard/icons/tarifas.png`} />
                      <br />
                      <p>TARIFAS</p>
                    </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        // event.preventDefault();
                        // location.replace("/#/informes/academico");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/informes/academico`}
                  >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/icons/documentos.png`} />
                    <br />
                    <p>INFORMES ACADEMICOS</p>
                  </div>
                  </Link>
                </Grid>
              </Grid>
            </Grid>

            <br />
            {showSnack}
            {formActionTill}
            {this.state.till_start === true &&
              <MidModal>
                {{
                  onHandleSubmit: this.updateState,
                  form: <TillStart />,
                  props_form: { onSuccess: this.openSnack, },
                }}
              </MidModal>
            }
            </div>
          )}}
      </getPerson.Consumer>
    );
  }
}
