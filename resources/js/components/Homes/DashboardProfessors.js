import React, { Component } from "react";
import {
  Button,
  TextField,
  CircularProgress,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Tooltip,
  AppBar,
  Tabs,
  Tab,
  Typography,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { Autocomplete } from "@material-ui/lab";
import DashboardAdmin from "./DashboardAdmin";
import DashboardReception from "./DashboardReception";
import { getPerson } from "../../contexts";
import PropTypes from "prop-types";
import { getAuth } from "../../contexts";
import PeopleIcon from "@material-ui/icons/People";
import ListAltIcon from "@material-ui/icons/ListAlt";
import { withStyles } from "@material-ui/core/styles";
// import StudentEnrolleds from "../Students/StudentEnrolleds";
import BorderColorIcon from "@material-ui/icons/BorderColor";
import Paginator from "../Paginators/Paginator";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    height: 140,
  },
});
class DashboardProfessors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      subjects: [],
      student: false,
      planilla: false,
      person_id: null,
      subj_id: null,
      filteredsubjects: [],
      person_name: "",
      value: 0,
      url: "/api/carsubsProfessor?all=true",
      time_out: false,
      busqueda: false,
      prevActiveStep: 1,
      rowsPerPage: 5,
      page_lenght: 0,
      page: 1,
      validator: {
        person_id: { message: "", error: false },
      },
    };

    this.getObject = async (perPage, page) => {
      let res;
      res = await axios.get(
        `${this.state.url}&per_page=${perPage}&page=${page}`
      );

      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          subjects: data,
          paginator: data,
          filteredsubjects: data.data,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      // this.setState({
      //   edit: false,
      //   new: false,
      //   open: false,
      // });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
  }
  componentDidMount() {
    this._ismounted = true;
    this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    var element = document.getElementById('page');
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredsubjects: this.state.subjects.data,
            page_lenght: this.state.subjects.last_page,
            page: 1,
            time_out: false,
            url: `/api/carsubsProfessor?all=true`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/carsubsProfessor?all=false&subject=${e.target.value}`,
          });
          axios
            .get(
              `${this.state.url}&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filteredsubjects: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  searchFieldChange(event) {
    var e = event;
    this.setState({
      person_name: e.target.value,
      person_id: null,
    });
    e.persist();
    if (this.state.time_out == false) {
      this.setState({
        time_out: true,
      });
      setTimeout(() => {
        if (e.target.value === "") {
          axios.get("api/persons").then((res) => {
            this.setState({
              persons: res.data.data,
              time_out: false,
            });
          });
        } else {
          axios
            .get(
              `/api/persons-search?search=${e.target.value}`
            )
            .then((res) => {
              this.setState({
                persons: res.data.data,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 2000);
    }
  }
  selectsChange(e, values, nameValue, nameLabel) {
    axios.get(`/api/persontype/${values.value}`).then((res) => {
      this.setState({ person_type: res.data });
    });
    this.setState({
      [nameValue]: values.value,
      [nameLabel]: values.label,
    });
  }
  Persons() {
    return this.state.persons.map((data) => ({
      label: `${data.person_idnumber} - ${data.person_fname} ${data.person_lastname}`,
      value: data.person_id,
    }));
  }
  selectValue(name, label) {
    return {
      label: this.state[name],
      value: this.state[label],
    };
  }

  render() {
    const { value } = this.state;
    const { classes } = this.props;
    var paginator;

    if (this.state.filteredsubjects.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }

    return (
      <div>
        <div>
          <h2 align="center">
            Materias{" "}
            <TextField
              id="outlined-search"
              label="Buscar"
              type="search"
              variant="outlined"
              onChange={this.search}
              InputProps={{
                endAdornment: (
                  <React.Fragment>
                    {this.state.time_out === true ? (
                      <CircularProgress color="inherit" size={20} />
                    ) : null}
                  </React.Fragment>
                ),
              }}
            />
          </h2>
          <TabContainer>
            <TableContainer component={Paper}>
              <Table
                aria-label="a dense table"
                size="small"
                option={{ search: true }}
              >
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <h3>Facultad</h3>
                    </TableCell>
                    <TableCell>
                      <h3>Carrera</h3>
                    </TableCell>
                    <TableCell>
                      <h3>Materia</h3>
                    </TableCell>
                    <TableCell colSpan="2"></TableCell>
                  </TableRow>
                </TableHead>
                {this.state.filteredsubjects.map((data) => (
                  <TableBody key={data.carsubj_id}>
                    <TableRow>
                      <TableCell>{data.facu_name}</TableCell>
                      <TableCell>{data.career_name}</TableCell>
                      <TableCell>{data.subj_name}</TableCell>

                      <TableCell>
                        <Tooltip title="Mostrar Alumnos">
                          <Link
                            className="navbar-brand"
                            to={
                              "/menu/docente/estudiantes/show/" +
                              data.carsubj_id
                            }
                          >
                            <Button
                              variant="outlined"
                              color="inherit"
                              type="submit"
                              startIcon={<PeopleIcon />}
                            ></Button>
                          </Link>
                        </Tooltip>
                      </TableCell>
                      <TableCell>
                        <Tooltip title="Mostrar Evaluaciones">
                          <Link
                            className="navbar-brand"
                            to={
                              "/menu/docente/evaluaciones/show/" +
                              data.carsubj_id
                            }
                          >
                            <Button
                              variant="outlined"
                              color="primary"
                              startIcon={<BorderColorIcon />}
                            ></Button>
                          </Link>
                        </Tooltip>
                      </TableCell>
                      <TableCell>
                        <Tooltip title="Mostrar Planilla">
                          <Button
                            variant="outlined"
                            color="secondary"
                            startIcon={<ListAltIcon />}
                          ></Button>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                ))}
              </Table>
            </TableContainer>
          </TabContainer>
          {paginator}
        </div>
      </div>
    );
  }
}
DashboardProfessors.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(DashboardProfessors);
