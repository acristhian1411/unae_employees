import React, {Component} from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { Snackbar, Grid, IconButton, TextField } from '@material-ui/core';
// const ModalForm = lazy(()=> import('./ModalForm'));
import { Alert, Autocomplete } from '@material-ui/lab';
import {RoutesList} from '../ArraysCalls/RoutesList';
// import FormEnrolled from "../Enrolleds/FormEnrolled";
import MidModal from "../Modals/MidModal";

import {getPerson} from '../../contexts';

export default class DashboardSystem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: '',
      form_enroll: false
    }
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.redirectStudent = this.redirectStudent.bind(this);
    this.updateState = () => {
      this.setState({
        snack_open: false,
        message_success: '',
        form_enroll: false
      });
      // this.getObject(this.state.rowsPerPage, this.state.page);
    };
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("MENÚ DE ADMINISTRACIÓN DE SISTEMA");
    var element = document.getElementById('page');
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack(){
    this.setState({snack_open: false});
  }
  openSnack(param){
    this.setState({snack_open: true, message_success: param});
  }
  openForm(value){
    this.setState({form_enroll:true});
  }
  redirectStudent(){
      this.setState({form_enroll: false});
  }

  render(){
    var showSnack;
    const {snack_open, message_success} = this.state;
    if (snack_open){
          showSnack = <Snackbar anchorOrigin={{vertical: 'bottom', horizontal: 'center'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                         <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="error">
                           {message_success}
                         </Alert>
                       </Snackbar>
        }
    return(
      <getPerson.Consumer>{(context)=>{
          return[
            <div className="container-home">
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={0} style={{justifyContent:'center', verticalAlign: 'middle'}} >
                <Grid item xs={3} style={{marginTop: '10%'}}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        event.preventDefault();
                        location.replace("/#/reportes/balances");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/reportes/balance`}
                  >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/informes.jpeg`} />
                    <br />
                    <p>INFORMES</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={3} style={{marginTop: '10%'}}>
                  <Link
                  className="dashboard-link-button"
                    to={`/menu/recepcion`}
                  >
                    <div className="dashboard-button">
                      <img className="dashoard-img-button" src={`img/dashboard/icons/recepcion.png`} />
                      <br />
                      <p>RECEPCION</p>
                    </div>
                  </Link>
                </Grid>
                <Grid item xs={3} style={{marginTop: '10%'}}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        event.preventDefault();
                        location.replace("/#/menu/administracion");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/menu/administracion`}
                  >
                    <div className="dashboard-button">
                      <img className="dashoard-img-button" src={`img/dashboard/icons/administracion.png`} />
                      <br />
                      <p>ADMINISTRACIÓN</p>
                    </div>
                  </Link>
                </Grid>
                <Grid item xs={3} style={{marginTop: '10%'}}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        event.preventDefault();
                        location.replace("/#/menu/academico");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/menu/academico`}
                  >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/icons/academica.png`} />
                    <br />
                    <p>ACADEMICO</p>
                  </div>
                  </Link>
                </Grid>
              </Grid>
            </Grid>
            {this.state.form_enroll &&
              <MidModal>
                {{
                  onHandleSubmit: ()=>{this.redirectStudent; context.clearPerson()},
                  form:<p></p>,
                  props_form: { onSuccess: this.openSnack, person_id: context.person_id },
                }}
              </MidModal>
            }
            </div>,
            <Autocomplete
              id="combo-box-demo"
              className="buscador"
              disableClearable
              options={RoutesList("Administrador del sistema")}
              getOptionLabel={(option) => option.label}
              groupBy={(option)=>option.group}
              onChange={(e, values) =>(
                 window.location.replace(`/#${values.value}`),
                 document.getElementById('page').focus()
               )
              }
              // value={this.selectValue("facu_name", "faculty_id")}
              getOptionSelected={(option, value) => {
                if (value === option.value) {
                  return option.label;
                } else {
                  return false;
                }
              }}
              // style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  autoFocus
                  label="Busqueda de rutas *"
                  //error={this.state.validator.faculty_id.error}
                  className="textField"
                  variant="outlined"
                />
              )}
            />
          ]
        }}

      </getPerson.Consumer>
    );
  }
}
