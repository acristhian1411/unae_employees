import React, { Component } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import {
  Snackbar,
  Grid,
  Fab,
  Slide,
  TextField,
  CircularProgress,
  Button
} from "@material-ui/core";
// const ModalForm = lazy(()=> import('./ModalForm'));
import { Alert } from "@material-ui/lab";
import FindReplaceIcon from "@material-ui/icons/FindReplace";
import { Autocomplete } from "@material-ui/lab";
// import ProviderAccountForm from "../ProviderAccount/ProviderAccountForm";
import EmployeeAccountForm from "../EmployeeAccount/EmployeeAccountForm";
// import ProfessorPayment from "../ProfessorAccounts/ProfessorPayment";

import { getPerson } from "../../contexts";

export default class PersonPayments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      search: true,
      person_type: [],
      person_name: "",
      id: 0,
      time_out: false,
      persons: [],
      validator: {
        id: { message: "", error: false },
      },
    };
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
  }
  componentDidMount() {
    console.log("PersonPayments");
    if (this.props.person_type != undefined) {
      console.log(this.props)
      if(this.props.person_type == 'provider'){
      this.setState({ person_type: ['provider'] });
    }else if(this.props.person_type == 'employee'){
      this.setState({ person_type: ['employee'] });
    }else if(this.props.person_type == 'professor'){
      this.setState({ person_type: ['professor'] });
    }
      
    }else{
      axios.get(`/api/persons/${this.props.match.params.id}`).then((res) => {
        this.setState({
          id: res.data.data.person_id,
          person_name:
            res.data.data.person_idnumber +
            " - " +
            res.data.data.person_fname +
            " " +
           ( res.data.data.person_lastname != null ? res.data.data.person_lastname : ''),
        });
      });
      axios.get(`/api/persontype/${this.props.match.params.id}`).then((res) => {
        this.setState({ person_type: res.data });
      });
    }
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  selectsChange(e, values, nameValue, nameLabel) {
    // axios.get(`/api/persontype/${values.value}`).then((res) => {
    //   this.setState({ person_type: res.data });
    // });
    this.setState({
      id: values.value,
      person_name: values.label,
    });
    // location.replace("/#/ficha_pago/" + values.value);

  }
  searchFieldChange(event) {
    var e = event;
    this.setState({
      person_name: e.target.value,
      id: null,
    });
    e.persist();
    if (this.state.time_out == false) {
      this.setState({
        time_out: true,
      });
      setTimeout(() => {
        if (e.target.value === "") {
          
          axios.get("api/persons").then((res) => {
            this.setState({
              persons: res.data.data,
              time_out: false,
            });
          });
        } else {
          let url = ''
          if(this.props.person_type != undefined){
            if(this.props.person_type[0] == 'professor'){
              url = 'professors-search?search='
            }else if(this.props.person_type[0] == 'provider'){
              url = 'providers-search?search='
            }else if(this.props.person_type[0] == 'employee'){
              url = 'employees-search?search='
            }
          }else{
            url = 'persons-search?search='
          }
          axios
            .get(
              `/api/${url}${e.target.value}`
            )
            .then((res) => {
              if(res.data != false){
              this.setState({
                persons: res.data.data,
                time_out: false,
              });
            }else{
              this.setState({
                persons: [],
                time_out: false,
              });
            }
            })
            .catch((error) => {
              this.setState({time_out: false})
              console.log("Ingrese un valor valido");
            });
        }
      }, 2000);
    }
  }
  Persons() {
    return this.state.persons.map((data) => ({
      label: `${data.person_idnumber} - ${data.person_fname} ${data.person_lastname}`,
      value: data.person_id,
    }));
  }
  selectValue(name, label) {
    return {
      label: this.state[name],
      value: this.state[label],
    };
  }
  render() {
    var showSnack;
    const { snack_open, message_success } = this.state;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={1}>
            <Grid item xs={1}>
              <Fab
                color="primary"
                aria-label="add"
                onClick={() => this.setState({ search: !this.state.search })}
              >
                <FindReplaceIcon />
              </Fab>
            </Grid>
            <Grid item xs={10}>
              <Slide
                direction="left"
                in={this.state.search}
                mountOnEnter
                unmountOnExit
              >
                <Autocomplete
                  id="combo-box-demo"
                  className="textfield-home"
                  disableClearable
                  options={[
                    this.selectValue("person_name", "id"),
                    ...this.Persons(),
                  ]}
                  filterSelectedOptions
                  getOptionLabel={(option) => option.label}
                  onChange={(e, values) => this.selectsChange(e, values)}
                  value={this.selectValue("person_name", "id")}
                  loading={this.state.time_out}
                  getOptionSelected={(option, value) =>
                    option.value === value.value
                  }
                  style={{
                    margin: 0,
                  }}
                  fullWidth
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      error={this.state.validator.id.error}
                      helperText={this.state.validator.id.message}
                      label="Ingrese nombre, o número de cédula, o lo que necesite"
                      color="secondary"
                      variant="outlined"
                      fullWidth
                      onChange={this.searchFieldChange}
                      autoFocus={this.state.search}
                      InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                          <React.Fragment>
                            {" "}
                            {this.state.time_out === true ? (
                              <CircularProgress color="inherit" size={20} />
                            ) : null}{" "}
                            {params.InputProps.endAdornment}{" "}
                          </React.Fragment>
                        ),
                      }}
                    />
                  )}
                />
              </Slide>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={1}>
           
          <Grid item xs={4}>
          <Button variant="outlined" color="secondary" onClick={()=>{
                  this.setState({person_type:['employee'],
              modal:true
              })}} >Pago a Funcionarios</Button>
          </Grid>
          <Grid item xs={4}>
          <Button variant="outlined" color="secondary" onClick={()=>{
                  this.setState({
                    person_type:['provider'],
              modal:true
              })}}>Pago a Proveedores</Button>

          </Grid>
          <Grid item xs={4}>
          <Button variant="outlined" color="secondary" onClick={()=>{
                  this.setState({
                    person_type:['professor'],
              modal:true
              })}}>Pago a Docentes</Button>

          </Grid>
          </Grid>
        </Grid>

        
                
                

        {this.state.person_type.indexOf("provider") != -1 && [
          <h3 align="center">Pago a Proveedor</h3>,
          // <ProviderAccountForm person_id={this.state.id} till_id={this.props.till_id} />,
          <p></p>,
          <hr/>
        ]}
        {this.state.person_type.indexOf("professor") != -1 && [
          <h3 align="center">Pago a Docente</h3>,
          // <ProfessorPayment person_id={this.state.id} till_id={this.props.till_id} />,
          <p></p>,
          <hr/>
        ]}

        {this.state.person_type.indexOf("employee") != -1 && [
          <h3 align="center">Pago a Funcionario</h3>,
            <EmployeeAccountForm person_id={this.state.id} till_id={this.props.till_id} />,
            <hr/>
          ]
        }
        {console.log(this.state.person_type)}
      </div>
    );
  }
}
