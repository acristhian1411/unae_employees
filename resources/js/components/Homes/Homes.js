import React, { Component } from "react";
import { Button, TextField, CircularProgress, IconButton } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import CloseIcon from '@material-ui/icons/Close';
import DashboardSystem from "./DashboardSystem";
import DashboardAdmin from "./DashboardAdmin";
import DashboardTills from "./DashboardTills";
// import DashboardReception from "./DashboardReception";
// import DashboardProfessors from "./DashboardProfessors";
// import DashboardAcademico from "./DashboardAcademico";
import { getPerson, getAuth } from "../../contexts";
import {RoutesList} from '../ArraysCalls/RoutesList';

export default class Homes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      person_id: null,
      person_type: [],
      person_name: "",
      time_out: false,
      validator: {
        person_id: { message: "", error: false },
      },
    };
    this.searchFieldChange = this.searchFieldChange.bind(this);
  }
  componentDidMount() {
    // console.log(this.props.match.params.type === undefined);
    if (this.props.match.params.type === undefined) {

      // location.replace('/#/menu/caja')
    }
    var element = document.getElementById('page');
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
    // location.replace('/#/menu/caja')
  }
  getMenu(){
    return(
      <getAuth.Consumer>
        {(context) => {
          if (context.user.roles !== undefined && this.props.match.params.type === undefined) {
            if (context.user.roles[0].role_name === 'Cajas') {
              location.replace('/#/menu/caja');
            }
            if (context.user.roles[0].role_name === 'Archivo' ||
                context.user.roles[0].role_name === 'Secretaria Academica' ||
                context.user.roles[0].role_name === 'Secretaria Academica.' ||
                context.user.roles[0].role_name === 'Administrador Academico'){
              location.replace('/#/menu/academico');
            }
            if (context.user.roles[0].role_name === 'Administrador de parte Administrativa'){
              location.replace('/#/menu/administracion');
            }
            if (context.user.roles[0].role_name === 'Administrador del sistema'){
              location.replace('/#/menu/sistema');
            }
            if (context.user.roles[0].role_name === 'Recepcion'){
              location.replace('/#/menu/recepcion');
            }
          }
        }}
      </getAuth.Consumer>
    )
  }

  searchFieldChange(event) {
    var e = event;
    this.setState({
      person_name: e.target.value,
      person_id: null,
    });
    e.persist();
    if (this.state.time_out == false) {
      this.setState({
        time_out: true,
      });
      setTimeout(() => {
        if (e.target.value === "") {
          axios.get("api/persons").then((res) => {
            this.setState({
              persons: res.data.data,
              time_out: false,
            });
          });
        } else {
          axios
            .get(
              `/api/persons-search?search=${e.target.value}`
            )
            .then((res) => {
              this.setState({
                persons: res.data.data,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 2000);
    }
  }
  selectsChange(e, values, nameValue, nameLabel) {
    axios.get(`/api/persontype/${values.value}`).then((res) => {
      this.setState({ person_type: res.data });
    });
    this.setState({
      [nameValue]: values.value,
      [nameLabel]: values.label,
    });
  }
  Persons() {
    return this.state.persons.map((data) => ({
      label: `${data.person_idnumber} - ${data.person_fname} ${data.person_lastname}`,
      value: data.person_id,
    }));
  }
  selectValue(name, label) {
    return {
      label: this.state[name],
      value: this.state[label],
    };
  }
  render() {
    const { value } = this.state;
    return (
      <getPerson.Provider
        value={{
          person_id: this.state.person_id,
          person_type: this.state.person_type,
          clearPerson: ()=>{this.setState({person_id: null, person_type: null, person_name: "",})}
        }}
      >
      {this.getMenu()}
        <div className="home-container">
          {this.props.match.params.type === "sistema" ? (
            // <DashboardSystem changeTitlePage={this.props.changeTitlePage} />
            <p>a</p>
          ) : this.props.match.params.type === "administracion" ? (
            <DashboardAdmin changeTitlePage={this.props.changeTitlePage} />
          ) : this.props.match.params.type === "docente" ? (
            <p></p>
          ) : this.props.match.params.type === "recepcion" ? (
            <p>a</p>
          ) : this.props.match.params.type === "caja" ? (
            <DashboardTills changeTitlePage={this.props.changeTitlePage} />
          ) : this.props.match.params.type === "academico" ? (
            <p>a</p>
          ) : (
            ''
            // window.location.replace("/")
          )}

          {this.props.match.params.type === "docente" ? (
            // <DashboardProfessors />
            <p>a</p>
          ) : (
            ''

          )}
        </div>
      </getPerson.Provider>
    );
  }
}
