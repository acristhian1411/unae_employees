import React, {Component} from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { Snackbar, Grid } from '@material-ui/core';
// const ModalForm = lazy(()=> import('./ModalForm'));
import { Alert } from '@material-ui/lab';
import FormEnrolled from "../Enrolleds/FormEnrolled";
import MidModal from "../Modals/MidModal";
import ModalForm from '../Students/ModalForm';
import AssistanceMultiple from '../Assistance/AssistanceMultiple';

import {getPerson} from '../../contexts';

export default class DashboardAcademico extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _new: false,
      modal_asistances: false,
      snack_open: false,
      message_success: '',
      form_enroll: false
    }
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.redirectStudent = this.redirectStudent.bind(this);
    this.updateState = () => {
      this.setState({
        snack_open: false,
        message_success: '',
        form_enroll: false,
        modal_asistances: false,
      });
      // this.getObject(this.state.rowsPerPage, this.state.page);
    };
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("MENÚ ACADEMICO");
    var element = document.getElementById('page');
    element.focus();
    setTimeout(function () { element.focus(); }, 1);
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack(){
    this.setState({snack_open: false});
  }
  openSnack(param){
    this.setState({snack_open: true, message_success: param});
  }
  openForm(value){
    this.setState({form_enroll:true});
  }
  redirectStudent(){
      this.setState({form_enroll: false});
  }

  render(){
    var showSnack;
    var showModal;
    var showModalAsistances;
    const {snack_open, message_success, modal_asistances} = this.state;
    if (snack_open){
      showSnack = <Snackbar anchorOrigin={{vertical: 'bottom', horizontal: 'center'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                    <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="error">
                     {message_success}
                    </Alert>
                  </Snackbar>
    }
    if (this.state._new) {
      showModal = <MidModal >
                    {{onHandleSubmit: this.updateState,
                      form: <ModalForm />,
                      props_form: {onSuccess:this.openSnack}
                    }}
                  </ MidModal>
    }
    if(modal_asistances){
      showModalAsistances = 
        <MidModal >
          {{onHandleSubmit: this.updateState,
            form: <AssistanceMultiple />,
            props_form: {onSuccess:this.openSnack}
          }}
        </ MidModal>
    }
    return(
      <getPerson.Consumer>{(context)=>{
          return(
            <div className="container-home">
              {showModal}
              {showSnack}
              {showModalAsistances}
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={0} style={{justifyContent:'center', verticalAlign: 'middle'}} >
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                        // event.preventDefault();
                        // location.replace("/#/estudiantes");
                  }}
                    to={`/estudiantes`}
                  >
                    <div className="dashboard-button">
                      <img className="dashoard-img-button" src={`img/dashboard/inscripciones.jpeg`} />
                      <br />
                      <p>ESTUDIANTES</p>
                    </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        event.preventDefault();
                        location.replace("/#/inscripciones");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                      else {
                        event.preventDefault();
                        this.setState({form_enroll:true});
                        // ()=> this.openForm();
                      }
                    }}
                    to={`/inscripciones-menu/${context.person_id}`}
                  >
                    <div className="dashboard-button">
                      <img className="dashoard-img-button" src={`img/dashboard/informe_inscripciones.jpeg`} />
                      <br />
                      <p>INSCRIPCIONES</p>
                    </div>
                  </Link>
                </Grid>
                <Grid item xs={'auto'}>
                    <div className='dashboard-button' onClick={()=>{this.setState({_new: true})}}>
                      <img className="dashoard-img-button" src={`img/dashboard/icons/new_person.jpeg`} /><br/>
                      <p>NUEVO ESTUDIANTE</p>
                    </div>
                  </Grid>
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        event.preventDefault();
                        location.replace("/#/monitoreo/administrativo");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/monitoreo/administrativo`}
                  >
                    <div className="dashboard-button">
                      <img className="dashoard-img-button" src={`img/dashboard/cobros.jpeg`} />
                      <br />
                      <p>ESTADOS DE CUENTAS</p>
                    </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        event.preventDefault();
                        location.replace("/#/monitoreo/academico");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/monitoreo/academico`}
                  >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/informes.jpeg`} />
                    <br />
                    <p>SEGUIMIENTO</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        event.preventDefault();
                        location.replace("/#/materias");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/materias`}
                  >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/materias.jpeg`} />
                    <br />
                    <p>MATERIAS</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        // event.preventDefault();
                        // location.replace("/#/informes/academico");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/informes/academico`}
                  >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/icons/recepcion.png`} />
                    <br />
                    <p>INFORMES</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"}>
                  <Link
                  className="dashboard-link-button"
                    onClick={(event) => {
                      if (context.person_id === null) {
                        // event.preventDefault();
                        // location.replace("/#/informes/academico");
                        // this.openSnack('Elige una persona que sea estudiante');
                      }
                    }}
                    to={`/documentos`}
                  >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/icons/documentos.png`} />
                    <br />
                    <p>DOCUMENTOS</p>
                  </div>
                  </Link>
                </Grid>
                <Grid item xs={"auto"}>
                  <div className="dashboard-button" onClick={()=>{this.setState({modal_asistances:true})}}>
                    <img style={{maxWidth:100, maxHeight:100}} className="dashoard-img-button" src={`img/inscribir.png`} />
                    <br />
                    <p>CARGAR ASISTENCIAS</p>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            {this.state.form_enroll &&
              <MidModal>
                {{
                  onHandleSubmit: ()=>{this.redirectStudent; context.clearPerson()},
                  form: <FormEnrolled />,
                  props_form: { onSuccess: this.openSnack, person_id: context.person_id },
                }}
              </MidModal>
            }
            </div>
          )}}
      </getPerson.Consumer>
    );
  }
}
