import React, { Component } from "react";
import { Link } from "react-router-dom";
import FormUser from "./FormUser";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Tooltip,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TableSortLabel,
  TablePagination,
  TableFooter,
  Paper,
  Select,
  MenuItem,
  InputBase,
  TextField,
  Snackbar,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import { Alert } from "@material-ui/lab";

class UserPerson extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      users: [],
      user_id: 0,
      roles: [],
      persons: [],
      user: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredUser: [],
      prevActiveStep: 1,
      page: 1,
      rowsPerPage: 10,
      order: "asc",
      orderBy: "email",
      selected: [],
      page_lenght: 0,
      time_out: false,
      permissions: {
        users_index: null,
        users_show: null,
        users_store: null,
        users_update: null,
        users_destroy: null,
        roles_index: null,
      },
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res = await axios.get(
        `/api/users-person/${this.props.person}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
      );
      let { data } = await res;
      if (this._ismounted) {
        this.setState({
          users: data,
          paginator: res.data,
          filteredUser: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };

    //funciones
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.search = this.search.bind(this);

    this.handleClose = () => {
      this.setState({ open: false });
    };
  }

  componentDidMount() {
    this._ismounted = true;
    this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    var permissions = [
      "users_index",
      "users_show",
      "users_store",
      "users_update",
      "users_destroy",
      "roles_index"
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }

  handleChangePage(event, newPage) {
    this.setState({ page: newPage });
  }
  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value, page: 0 });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    this.setState({ snack_open: true, message_success: param });
  }
  perPageChange(value) {
    this.setState({ rowsPerPage: value, prevActiveStep: 1, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ prevActiveStep: value, page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  search(event) {
    var e = event;
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredUser: this.state.users,
            paginator: this.state.users,
            page_lenght: this.state.users.last_page,
            page: 1,
            time_out: false,
          });
        } else {
          axios
            .get(
              `/api/users/search/${e.target.value}?sort_by=${
                this.state.orderBy
              }&order=${this.state.order}&per_page=${
                this.state.rowsPerPage
              }&page=${1}`
            )
            .then((res) => {
              this.setState({
                filteredUser: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }
  handleClickOpen(data) {
    this.setState({ open: true, user: data });
  }

  deleteObject() {
    axios.delete(`/api/users/${this.state.user_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      user: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }

  render() {
    const {
      user,
      snack_open,
      message_success,
      permissions,
    } = this.state;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    if (this.state.new) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormUser />,
            props_form: {
              person: this.props.person,
              onSuccess: this.openSnack,
              person_data: this.props.person_data
            },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormUser />,
            props_form: {
              edit: true,
              person: this.props.person,
              user: this.state.user,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (this.state.open) {
      showDialogDestroy = (
        <DialogDestroy
          index={user.data.cntper_description}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (this.state.snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div className="card-body">
        <h2 align="center">
          Usuarios
          {/* <TextField id="outlined-search" label="Buscar" type="search" variant="outlined" onChange={this.search.bind(this)} /> */}
        </h2>
        {permissions.users_store === true && permissions.roles_index === true && (
          <Tooltip title="Agregar">
            <Button
              variant="outlined"
              color="primary"
              startIcon={<AddBoxOutlinedIcon />}
              type="submit"
              onClick={this.clickAgregar}
            >
              {" "}
            </Button>
          </Tooltip>
        )}
        <hr />

        {this.renderList()}

        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredUser.length === 0) {
      return (
        <div className="card_label_field">
          <p className="title_column">No definido</p>
        </div>
      );
    } else {
      return this.state.filteredUser.map((data) => {
        return (
          <div className="card_label_field" key={data.id}>
            <p className="title_column">{data.email}</p>
            {permissions.users_update === true && (
              <p className="value_column">
                <Button
                  startIcon={<EditIcon />}
                  type="submit"
                  onClick={() => {
                    this.clickEditar({ data: data });
                  }}
                >
                  {data.cntper_data}
                </Button>
              </p>
            )}
          </div>
        );
      });
    }
  }
}

export default UserPerson;
