import React, { Component, Fragment } from "react";
import {
  Modal,
  Backdrop,
  Fade,
  InputLabel,
  TextField,
  Button,
  Fab,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { Autocomplete, Alert } from "@material-ui/lab";
import AddIcon from "@material-ui/icons/Add";
import InputAdornment from "@material-ui/core/InputAdornment";
import Tooltip from "@material-ui/core/Tooltip";
import RemoveIcon from "@material-ui/icons/Remove";
import DialogDestroy from "../Dialogs/DialogDestroy";

export default class FormUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      person_time_out: false,
      role_time_out: false,
      open: true,
      setOpen: true,
      openAlert: false,
      snack_openDestroy: false,
      message_success: "",
      id: 0,
      email: "",
      person_id: null,
      person_name: "",
      role_id: null,
      role_name: "",
      index: 0,
      persons: [],
      roleSelect: [],
      roles: [
        {
          role_id: 0,
          role_name: "No definido",
          created: false,
          edited: false,
        },
      ],
      edit: false,
      errors: [],
      validator: {
        email: { message: "", error: false },
        person_id: { message: "", error: false },
        role_id: { message: "", error: false },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onChangeField1 = this.onChangeField1.bind(this);
    this.selectChange = this.selectChange.bind(this);
    this.selectChange1 = this.selectChange1.bind(this);
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.handleUpdateObject = this.handleUpdateObject.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.destroy = this.destroy.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    if (this.props.person) {
      this.setState({
        person_id: this.props.person,
        person: this.props.person_data,
      });
    } else {
      axios.get("/api/persons").then((res) => {
        this.setState({ persons: res.data.data });
      });
    }
    axios.get("/api/roles").then((res) => {
      this.setState({ roleSelect: res.data.data });
    });
    if (this.props.edit) {
      var { id, person_id, name, email } = this.props.user.data;
      this.setState({ person_id, person_name: name, id, email });
      axios.get("api/users/" + id).then((response) => {
        var d = [];
        response.data.data.roles.forEach((item, i) => {
          d.push({
            role_id: item.role_id,
            role_name: item.role_name,
            old_role_id: item.role_id,
            new_role_id: 0,
            created: true,
            edited: false,
          });
        });
        this.setState({
          id: response.data.data.id,
          email: response.data.data.email,
          roles: d,
        });
      });
    }
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  handleOpen() {
    this.setState({
      setOpen: true,
    });
  }

  handleClose() {
    this.setState({
      open: false,
    });
    this.props.onHandleSubmit();
  }
  handleOpenAlert(data) {
    this.setState({
      openAlert: true,
      role_name: data,
    });
  }
  handleCloseAlert() {
    this.setState({
      openAlert: false,
    });
  }
  onChangeField1(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
    this.props.changeAtribute(true);
  }
  selectChange(e, values) {
    // console.log(values);
    this.setState({ person_id: values.value, person_name: values.label });
    this.props.changeAtribute(true);
  }
  selectChange1(e, values) {
    // console.log(values);
    this.setState({ role_id: values.value, role_name: values.label });
    this.props.changeAtribute(true);
  }
  searchFieldChange(event) {
    var e = event;
    this.setState({ person_name: e.target.value, person_id: null });
    e.persist();
    if (this.state.person_time_out == false) {
      this.setState({ person_time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          axios.get("/api/persons").then((res) => {
            this.setState({ persons: res.data.data, person_time_out: false });
          });
        } else {
          axios
            .get(
              `/api/persons-search?search=${e.target.value}`
            )
            .then((res) => {
              this.setState({
                persons: res.data.data,
                person_time_out: false,
              });
            })
            .catch((error) => {
              this.setState({person_time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  handleAddFields() {
    if (this.props.edit) {
      var values = [...this.state.roles];
      values.push({
        role_id: 0,
        role_name: "",
        created: false,
        edited: false,
        old_role_id: 0,
        new_role_id: 0,
      });
      this.setState({ roles: values });
    } else {
      var values = [...this.state.roles];
      values.push({ role_id: 0, role_name: "", created: false, edited: false });
      this.setState({ roles: values });
    }
  }

  handleRemoveFields(index) {
    if (this.props.edit) {
      const values = [...this.state.roles];
      if (values[index].created === true) {
        console.log("entra");
        this.setState({
          index: index,
          role_id: values[index].role_id,
        });
        this.handleOpenAlert(values[index].role_name);
      } else {
        values.splice(index, 1);
        this.setState({
          roles: values,
        });
      }
    } else {
      var values = [...this.state.roles];
      values.splice(index, 1);
      this.setState({ roles: values });
    }
  }

  onChangeRole(e, values, index) {
    if (this.props.edit) {
      const value = [...this.state.roles];

      if (value[index].created === true) {
        value[index].edited = true;
        value[index].old_role_id = value[index].role_id;
        value[index].new_role_id = values.value;
      }
      value[index].role_id = values.value;
      value[index].role_name = values.label;
      // value2[index2].role_name = values.label;
      this.setState({
        roles: value,
      });
    } else {
      const value = [...this.state.roles];
      value[index].role_id = values.value;
      value[index].role_name = values.label;
      this.setState({ roles: value });
    }
  }

  handleCreateObject(e) {
    e.preventDefault();
    var { email, person_id } = this.state;
    var validator = {
      email: { error: false, message: "" },
      person_id: { error: false, message: "" },
      role_id: { error: false, message: "" },
    };
    var object_error = {};
    const object = {
      email: this.state.email.toUpperCase(),
      person_id: this.state.person_id,
      // name: `${this.state.person.person_fname} ${this.state.person.person_lastname}`,
      roles: this.state.roles,
      // role_id: this.state.role_id,
    };
    // console.log(object);
    const { history } = this.props;
    axios
      .post("/api/users", {
        email: object.email,
        person_id: object.person_id,
        name: this.state.person_name,
        username: object.email,
        roles: this.state.roles,
      })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var { email, person_id } = this.state;
    var validator = {
      email: { error: false, message: "" },
      person_id: { error: false, message: "" },
      role_id: { error: false, message: "" },
    };
    var object_error = {};
    const object = {
      email: this.state.email.toUpperCase(),
      person_id: this.state.person_id,
      name: this.state.person_name,
      role_id: this.state.role_id,
    };
    const id = this.state.id;
    axios
      .put(`/api/users/${id}`, {
        email: object.email,
        person_id: object.person_id,
        name: object.name,
        username: object.email,
        roles: this.state.roles,
      })
      .then((res) => {
        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  destroy() {
    // event.preventDefault();
    axios
      .delete(`/api/usersRemove/${this.state.id}?role_id=${this.state.role_id}`)
      .then((res) => {
        var values = [...this.state.roles];
        values.splice(this.state.index, 1);
        this.setState({
          snack_openDestroy: true,
          message_success: res.data.data,
          roles: values,
        });
      });
  }

  botonEdit() {
    if (this.props.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleUpdateObject}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
      );
    }
  }
  Persons() {
    return this.state.persons.map((data) => ({
      label: data.person_fname + " " + data.person_lastname,
      value: data.person_id,
    }));
  }
  Roles() {
    return this.state.roleSelect.map((data) => ({
      label: data.role_name,
      value: data.role_id,
    }));
  }
  selectValue() {
    return { label: this.state.person_name, value: this.state.person_id };
  }
  selectValue1() {
    return { label: this.state.role_name, value: this.state.role_id };
  }

  render() {
    const { classes } = this.props;
    const { snack_open, validator } = this.state;
    var showSnack;
    var showSnackDestoy;
    var showDialogDestroy;
    if (this.state.openAlert) {
      showDialogDestroy = (
        <DialogDestroy
          index={`${this.state.role_name}`}
          onClose={this.handleCloseAlert}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (this.state.snack_openDestroy) {
      showSnackDestoy = (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={this.state.snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {" "}
            {this.state.message_success}{" "}
          </Alert>{" "}
        </Snackbar>
      );
    }

    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["email"]["error"] ? (
              <li>Error: {validator["email"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        {console.log(this.state.roles)}
        <h3>Formulario de Usuarios</h3>
        <hr />
        <form>
          <div className="form-group">
            {this.props.person === undefined && (
              <Autocomplete
                id="combo-box-demo"
                disableClearable
                options={[this.selectValue(), ...this.Persons()]}
                filterSelectedOptions
                getOptionLabel={(option) => option.label}
                onChange={this.selectChange}
                value={this.selectValue()}
                loading={this.state.person_time_out}
                getOptionSelected={(option, value) =>
                  option.value === value.value
                }
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    error={this.state.validator.person_id.error}
                    helperText={this.state.validator.person_id.message}
                    label="Seleccione una Persona *"
                    variant="outlined"
                    fullWidth
                    onChange={this.searchFieldChange}
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.person_time_out === true ? (
                            <CircularProgress color="inherit" size={20} />
                          ) : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
            )}

            {this.state.roles.map((inputField, index) => (
              <Fragment key={`${inputField}~${index}`}>
                <Autocomplete
                  id={index}
                  name="cnttype_id"
                  disableClearable
                  options={this.Roles()}
                  filterSelectedOptions
                  getOptionLabel={(option) => option.label}
                  onChange={(e, values) => {
                    this.onChangeRole(e, values, index);
                  }}
                  value={{
                    value: inputField.role_id,
                    label: inputField.role_name,
                  }}
                  getOptionSelected={(option, value) =>
                    option.value === value.value
                  }
                  style={{ margin: 8 }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      required
                      label="Roles "
                      variant="outlined"
                      size="small"
                    />
                  )}
                />

                {this.state.roles.length >= 1 && (
                  <div>
                    <Tooltip title="Agregar Contacto">
                      <Fab
                        color="primary"
                        size="small"
                        onClick={() => this.handleAddFields()}
                      >
                        <AddIcon />
                      </Fab>
                    </Tooltip>
                    <Tooltip title="Eliminar Contacto">
                      <Fab
                        color="secondary"
                        size="small"
                        onClick={() => this.handleRemoveFields(index)}
                      >
                        <RemoveIcon />
                      </Fab>
                    </Tooltip>
                  </div>
                )}
              </Fragment>
            ))}

            <TextField
              id="outlined-full-width"
              label="Correo"
              error={this.state.validator.email.error}
              helperText={this.state.validator.email.message}
              style={{ margin: 8 }}
              name="email"
              value={this.state.email}
              onChange={this.onChangeField1}
              fullWidth
              margin="normal"
              variant="outlined"
            />
          </div>
          {this.botonEdit()}
          {showSnack}
          {showSnackDestoy}
          {showDialogDestroy}
          <Button
            variant="outlined"
            color="secondary"
            startIcon={<ExitToAppIcon />}
            onClick={this.handleClose}
            onClick={this.handleClose}
          >
            Cancelar
          </Button>
        </form>
      </div>
    );
  }
}
