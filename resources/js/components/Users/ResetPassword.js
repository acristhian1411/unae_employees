import React, {Component} from 'react';
import {Button, TextField, InputAdornment, IconButton} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

export default class ResetPassword extends Component {
	constructor(props){
		super(props);
		this.state = {
			show_password:false,
			user_detail: [],
			password: 'maius20220816',
			validator:{email:{message:'',error: false},
                 password:{message:'',error: false}}
		}
		this.onChangeFields = this.onChangeFields.bind(this);
	}
	componentDidMount(){
		axios.get(`/api/users-details/${this.props.user.id}`).then((res)=>{
			this.setState({user_detail: res.data});
		})
	}
	onChangeFields(e){
    this.setState({[e.target.name]: e.target.value});
    this.props.changeAtribute(true);
  }
  formatDate(fecha){
  	var f = new Date(`${fecha} 00:00`);
  	return `Fecha de nacimiento: ${f.toLocaleDateString("en-US", { day: '2-digit' })+ "/"+ f.toLocaleDateString("en-US", { month: '2-digit' })+ "/" + f.toLocaleDateString("en-US", { year: 'numeric' })}`
  }
	render(){
		return(
			<div>
				<h3>{`Reseteo de contraseña para: ${this.props.user.name} (${this.props.user.email})`}</h3>
				<h4>Datos de usuario</h4>
				{this.state.user_detail.length !== 0 && (
						<ul>
							<li>{`Nombres: ${this.state.user_detail.person_fname}`}</li>
							<li>{`Apellidos: ${this.state.user_detail.person_lastname}`}</li>
							<li>{`Cedula: ${this.state.user_detail.person_idnumber}`}</li>
							<li>{`Fecha de nacimiento: ${this.formatDate(this.state.user_detail.person_birthdate)}`}</li>
							<li>{`Rol: ${this.state.user_detail.roles.map(row=>row.role_name)}`}</li>
						</ul>
					)
				}
				
				<TextField
          id="outlined-full-width"
          label="Contraseña"
          color="secondary"
          error={this.state.validator.password.error}
          helperText={this.state.validator.password.message}
          // style={{ margin: 8 }}
          type={this.state.show_password ? 'text' : 'password'}
          name = "password"
          value={this.state.password}
          onChange={this.onChangeFields}
          fullWidth
          margin="normal"
          variant="outlined"
          InputProps={{
            endAdornment:
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={()=>this.setState({show_password: !this.state.show_password})}
                  // onMouseDown={(e)=>this.handleMouseDownPassword(e)}
                  // onMouseUp={(e)=>this.handleMouseDownPassword(e)}
                >
                  {this.state.show_password ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>

          }}

        />
        <Button 
        	variant="outlined" 
        	color="primary"
        	onClick={()=>{
        		axios.post(`/api/users/reset-password/${this.props.user.id}`, {new_password: this.state.password}).then((res)=>{
        			if (res.data.type === 2) {
								this.props.onSuccess(res.data.message, true);
        			}else{
        				this.props.onSuccess(res.data.message);	
        			}
			        this.props.changeAtribute(false);
			        this.props.onHandleSubmit();
        		});
        	}}
        >
        	Guardar
        </Button>
			</div>
		)
	}
}