import React, {Component, lazy} from 'react';
import {Link} from 'react-router-dom';
import MidModal from '../Modals/MidModal';
import SmallModal from '../Modals/SmallModal';
import FormUser from './FormUser';
import ResetPassword from './ResetPassword';
import Paginator from '../Paginators/Paginator';
import DialogDestroy from '../Dialogs/DialogDestroy';
// const FormUser = lazy(()=> import('./FormUser'));
// const DialogDestroy = lazy(()=> import('../Dialogs/DialogDestroy'));
import {Button, Dialog, DialogActions, DialogTitle, Icon, Table,
  TableBody, TableCell, TableContainer, TableHead, TableRow,
  TablePagination, Paper, TextField, MobileStepper,
  Select, MenuItem, TableSortLabel, Snackbar, CircularProgress, Tooltip} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import RestoreIcon from '@material-ui/icons/Restore';
import {Capitalize} from '../GlobalFunctions/LetterCase';

export default class Users extends Component{
  constructor(props){
      super(props);
      this.state = {
        snack_open: false,
        message_success: '',
        snack_severity: 'success',
        form_reset_password: false,
        users:[],
        filterUsers:[],
        id: 0,
        user:[],
        edit: false,
        _new: false,
        open: false,
        paginator:[],
        rowsPerPage: 10,
        order: 'asc',
        orderBy: 'name',
        page_lenght: 0,
        page: 1,
        time_out: false,
        permissions:{
          users_index: null,
          users_show: null,
          users_store: null,
          users_update: null,
          users_destroy: null,
          reset_password: null,
        }
      }
      // this.handleClickOpen = (data) =>{
      //   this.setState({open: true, user: data});
      // }
      this.handleClose = () =>{
        this.setState({open: false});
      }
      this.getObject = async(perPage, page, orderBy=this.state.orderBy, order=this.state.order)=>{
        let res = await axios.get(`/api/users?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`);
        let {data} = await res;
        // console.log(res.data.last_page);
        // console.log(res);
        if (this._ismounted) {
          this.setState({users: data, paginator:res.data, filterUsers: data.data, open:false, page_lenght: res.data.last_page, page: res.data.current_page});
        }
      }
      this.updateState= () =>{
        this.setState({
          edit: false,
          _new: false,
          open: false,
          form_reset_password: false,
        });
        this.getObject( this.state.rowsPerPage, this.state.page);
      }

        this.destroy = this.destroy.bind(this);
        this.createSortHandler = this.createSortHandler.bind(this);
        this.perPageChange = this.perPageChange.bind(this);
        this.pageChange = this.pageChange.bind(this);
        this.closeSnack = this.closeSnack.bind(this);
        this.openSnack = this.openSnack.bind(this);
        this.search = this.search.bind(this);
    }

  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("USUARIOS");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = ['users_index', 'users_show', 'users_store', 'users_update', 'users_destroy', 'reset_password'];
    axios.get(`/api/roles_has_permission`, {params: {
                                            permission: permissions
                                          }}).then(res=>{
      // console.log('algo');
      this.setState({permissions: res.data});
      if (res.data!==false) {
        this.getObject( this.state.rowsPerPage, this.state.page);
      }
    });

  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
     this._ismounted = false;
  }
  closeSnack(){
    this.setState({snack_open: false});
  }
  openSnack(param, is_error=false){
    // console.log(param);
    if(is_error){
      this.setState({snack_open: true, message_success: param, snack_severity: 'error'});
    }else{
      this.setState({snack_open: true, message_success: param, snack_severity: 'success'});  
    }
    
  }
  handleModal(){
    this.setState({_new:true});
  }
  handleClickOpen(data){
    this.setState({open: true, user: data});
  }
  searchChange(e){
    this.setState({search:e.value});
  }
  editModal(data){
    this.setState({edit: true, user: data})
  }
  destroy(){
     // event.preventDefault();
    axios.delete(`/api/users/${this.state.id}`).then(
      res=>{
        this.setState({snack_open: true, message_success: res.data});
        this.updateState();
      }
    );
  }

  search(event){
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out==false) {
      this.setState({time_out:true});
      // setTimeout(()=>{
        if(e.target.value===''){
              this.setState({
                filterUsers:this.state.users.data,
                paginator: this.state.users,
                page_lenght: this.state.users.last_page,
                page: 1,
                time_out:false
              });
            }
            else {
          axios.get(`/api/users/search/${e.target.value}?sort_by=${this.state.orderBy}&order=${this.state.order}&per_page=${this.state.rowsPerPage}&page=${1}`).then(res=>{
                  this.setState({
                    filterUsers: res.data.data,
                    paginator: res.data,
                    page_lenght: res.data.last_page,
                    page: res.data.current_page,
                    time_out:false
                  });
                }).catch(error=>{
                  this.setState({time_out: false});
                  console.log("Ingrese un valor valido");
                });
        }
    //   , 3000);
    // }

  }


  createSortHandler(name){
    var order = '';
    if(this.state.orderBy===name){
      if(this.state.order==='asc'){
        order = 'desc';
        this.setState({order});
      }
      else{
        order = 'asc';
        this.setState({order});
      }
    }
    else{
      order = 'asc';
      this.setState({orderBy: name, order});
    }
      this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value){
    // console.log(value);
    this.setState({rowsPerPage: value, page: 1});
    this.getObject(value, 1);
  }
  pageChange(value){
    this.setState({page: value});
    this.getObject(this.state.rowsPerPage, value);
  }

  render(){
    const {snack_open, message_success, snack_severity, open,_new, edit, user, filterUsers, rowsPerPage,  orderBy, order, permissions, form_reset_password} = this.state;
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    var showResetPassword;
      if (_new) {
        // showModal = <FormUser onHandleSubmit={this.updateState}/>
        showModal = <MidModal >
                      {{onHandleSubmit: this.updateState,
                        form: <FormUser />,
                        props_form: {onSuccess:this.openSnack}
                      }}
                    </ MidModal>
      }
      else if (edit) {
        // showModal = <FormUser edit ={true} user={user} onHandleSubmit={this.updateState}/>
        showModal = <MidModal >
                      {{onHandleSubmit: this.updateState,
                        form: <FormUser />,
                        props_form: {edit: true, user:user, onSuccess:this.openSnack}
                      }}
                    </ MidModal>
      }
      if(open) {
        showDialogDestroy = <DialogDestroy index={user.data.user_name} onClose={this.handleClose} onHandleAgree={this.destroy}/>;
      }
      if(form_reset_password){
        showResetPassword = 
        <SmallModal >
          {{onHandleSubmit: this.updateState,
            form: <ResetPassword />,
            props_form: {user:user, onSuccess:this.openSnack}
          }}
        </ SmallModal>;
      }
      if (snack_open){
            showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'center'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                           <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity={snack_severity}>
                             {message_success}
                           </Alert>
                         </Snackbar>
      }
      if(filterUsers.length === 0){
        paginator =  <div>{''}</div>
      }else{
        paginator = <Paginator
        lenght={this.state.page_lenght}
        perPageChange= {this.perPageChange}
        pageChange ={this.pageChange}
        page= {this.state.page}
        />
      }

    return(
      <div style={{textAlign: "center"}}>
        <TextField
        id="outlined-search"
        label="Escriba para buscar"
        type="search"
        variant="outlined"
        // onChange={this.search}
        onKeyDown={(e) =>{
          if(e.key === 'Enter'){
            this.search(e)
          }
        }}
        style={{ width: "40%"}}
        InputProps={{
          endAdornment: (
            <React.Fragment>
              {this.state.time_out === true ? <CircularProgress color="inherit" size={20} /> : null}
            </React.Fragment>
          ),
        }}
        />

      <TableContainer component={Paper}>
        <Table aria-label="simple table" option={{search: true}}>
          <TableHead>
            <TableRow>
              <TableCell
                sortDirection={orderBy === 'name' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'name'}
                  direction={orderBy === 'name' ? order : 'asc'}
                  onClick={()=>{this.createSortHandler('name')}}
                >
                  <h3>Nombre</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={orderBy === 'email' ? order : false}
              >
                <TableSortLabel
                  active={orderBy === 'email'}
                  direction={orderBy === 'email' ? order : 'asc'}
                  onClick={()=>{this.createSortHandler('email')}}
                >
                  <h3>Correo electronico</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell colSpan="3">
              {permissions.users_store === true &&
                <Button variant="outlined" color="primary" type='submit' onClick={()=>{this.handleModal()}}>
                  < AddBoxOutlinedIcon />
                </Button>
              }
              </TableCell>
            </TableRow>
          </TableHead>
          {this.renderList()}
        </Table>
      </TableContainer>
        {/* <Paginator
          lenght={this.state.page_lenght}
          perPageChange= {this.perPageChange}
          pageChange ={this.pageChange}
          page= {this.state.page}
          /> */}
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
        {showResetPassword}
      </div>
    );
  }
  selectValue(event){
      this.setState({search: event.target.value})
    };
  renderList(){
    const {open, user, filterUsers, rowsPerPage, page, permissions} = this.state;
    if(filterUsers.length === 0){
      return(
        <TableBody>
        <TableRow>
          <TableCell>{' '}</TableCell>
          <TableCell>{'No contiene datos'}</TableCell>
          <TableCell>{' '}</TableCell>
          <TableCell>{''}</TableCell>
          </TableRow>
          </TableBody>
      )
    }else{
    return (filterUsers.map((data)=>{
      return(
    <TableBody key={data.id}>
        <TableRow>
          <TableCell>{Capitalize(data.name)}</TableCell>
          <TableCell>{data.email}</TableCell>
          <TableCell>
          {permissions.users_show === true &&
            <Link className='navbar-brand'to={"/usuarios/show/"+ data.id}>
              <Button variant="outlined" color="primary" startIcon={< VisibilityIcon />} >{' '}</Button>
            </Link>
          }
          </TableCell>          
          <TableCell>
          {permissions.reset_password === true &&
            <Tooltip title="Retroceder">
              <Button
                variant="outlined"
                color="secondary"
                onClick={() => {
                  this.setState({user:data, form_reset_password: true});
                }}
              >
                <RestoreIcon />
              </Button>
            </Tooltip>
          }
          </TableCell>
          <TableCell>
          {permissions.users_update === true &&
            <Button variant="outlined" color="primary" type='submit' onClick={()=>{this.editModal({data: data})}}>
            < EditIcon />
            </Button>
          }
          </TableCell>
          <TableCell>
          {permissions.users_destroy === true &&
            <Button variant="outlined" color="secondary" onClick={()=>{this.handleClickOpen({data: data}), this.setState({id: data.id})}}>
              <DeleteIcon />
            </Button>
          }
          </TableCell>
        </TableRow>

    </TableBody>
            )
          }
        )
      )
    }
  }//cierra el renderList
}
