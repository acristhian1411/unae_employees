import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import HomeIcon from '@material-ui/icons/Home';
import ListIcon from '@material-ui/icons/List';
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
  TableSortLabel, AppBar, Tabs, Tab, Typography, List, ListItem, ListItemText,
  Divider, Paper, Button, Snackbar, CircularProgress, Grid} from '@material-ui/core';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import Paginator from '../Paginators/Paginator';
import MidModal from '../Modals/MidModal';


export default class UserShow extends Component {
  constructor(props){
    super(props);
    this.state = {
      user : [],
      roles: [],
      paginator:[],
      order: 'asc',
      orderBy: 'role_name',
      rowsPerPage: 10,
      page_lenght: 0,
      page: 1,
      open: false,
      person: [],
      roles: []
    }
    this.getObject = async(id, perPage, page, orderBy=this.state.orderBy, order=this.state.order)=>{
      let res = await axios.get(`/api/roles_get_role/${id}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`);
      let {data} = await res;
      // console.log(res.data.last_page);
      // console.log(res);
      if (this._ismounted) {
        this.setState({roles: data.data, paginator:res.data, open:false, page_lenght: res.data.last_page, page: res.data.current_page});
      }
    }
    this.updateState= () =>{
      this.setState({
        edit: false,
        _new: false,
        open: false
      });
      this.getObject(this.state.role.role_id, this.state.rowsPerPage, this.state.page);
    }
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
  }
  perPageChange(value){
    // console.log(value);
    this.setState({rowsPerPage: value, page: 1});
    this.getObject(this.state.role.role_id, value, 1);
  }
  pageChange(value){
    this.setState({page: value});
    this.getObject(this.state.role.role_id,this.state.rowsPerPage, value);
  }
  createSortHandler(name){
    var order = '';
    if(this.state.orderBy===name){
      if(this.state.order==='asc'){
        order = 'desc';
        this.setState({order});
      }
      else{
        order = 'asc';
        this.setState({order});
      }
    }
    else{
      order = 'asc';
      this.setState({orderBy: name, order});
    }
      this.getObject(this.state.role.role_id, this.state.rowsPerPage, this.state.page, name, order);
  }
  handleModal(){
    this.setState({open:true});
  }
  componentDidMount(){
    // console.log('llega por aqui');
    this._ismounted = true;
    const id = this.props.match.params.id;
    axios.get(`/api/users/${id}`).then(response => {
      axios.get(`/api/persons/${response.data.data.person_id}`).then(res=>{
        this.setState({person: res.data.data});
      });
      this.setState({
        user: response.data.data,
        roles: response.data.data.roles
      });
    });

    // this.getObject(id ,this.state.rowsPerPage, this.state.page);
  }
  render(){
    var showPhoto;
    if (this.state.person.person_photo===undefined) {
      showPhoto = null;
    }
    else {
      showPhoto = <img className="profile_photo" src={`/img/${this.state.person.person_photo}`} />;
    }

    return(
      <div>
        <div className="breadcrumb-container">
          <ul className="breadcrumb-list">
            <li className="breadcrumb-item">
              <Link className='breadcrumb-link'to={"/"}>
                  <HomeIcon className="breadcrumb-icon"/>
                  Inicio
                  <ChevronRightIcon className="breadcrumb-icon"/>
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link className='breadcrumb-link'to={"/usuarios"}>
                  <ListIcon className="breadcrumb-icon"/>
                  Lista de Usuarios
                  <ChevronRightIcon className="breadcrumb-icon"/>
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Typography style={{color:"#666"}}>
                <GroupWorkIcon className="breadcrumb-icon"/>
                {this.state.user.name}
              </Typography>
            </li>
          </ul>
        </div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
              <Grid item xs={6}>
              <div className="card_person">
                  {showPhoto}
                  <div className="card_label_field">
                    <p className="title_column">Nombres </p>
                    <p className="value_column">{this.state.person.person_fname}</p>
                    <p className="title_column">Apellidos </p>
                    <p className="value_column">{this.state.person.person_lastname}</p>
                    <p className="title_column">Documento </p>
                    <p className="value_column">{this.state.person.person_idnumber}</p>
                    <p className="title_column">Correo Electronico </p>
                    <p className="value_column">{this.state.user.email}</p>
                    {this.state.user.provider_name !== null &&
                      <div>
                      <p className="title_column">Proveedor de correos </p>
                      <p className="value_column">{this.state.user.provider_name}</p>
                      </div>
                    }
                    <p className="title_column">Sexo </p>
                    <p className="value_column">{this.state.person.person_gender}</p>
                    <p className="title_column">Pais de Nacionalidad </p>
                    <p className="value_column">{this.state.person.country_name}</p>
                    <p className="title_column">Dirección </p>
                    <p className="value_column">{this.state.person.person_address}</p>
                    <p className="title_column">Tipo de sangre </p>
                    <p className="value_column">{this.state.person.person_bloodtype}</p>
                  </div>
              </div>
                {/*<div className='box-list-simple'>
                <div className="card_label_field">
                  <p className="title_column">Nombres </p>
                  <p className="value_column">{this.state.user.name}</p>
                  <p className="title_column">Correo Electronico </p>
                  <p className="value_column">{this.state.user.email}</p>
                  <p className="title_column">Proveedor </p>
                  <p className="value_column">{this.state.user.provider_name}</p>
                </div>
                </div>*/}
              </Grid>
              <Grid item xs={6}>
              <div className="card_person">
                  <div className="card_label_field">
                    <h4>Roles</h4>
                    <ul>
                    {this.state.roles.map(data=>{
                      return(
                        <li key={data.role_id}>{data.role_name}</li>
                      );
                    })}
                    </ul>
                  </div>
              </div>
              </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }

}
