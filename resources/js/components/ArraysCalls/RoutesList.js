function RoutesList(rol){
  if (rol === "Administrador del sistema") {
    return todos;
  }else if (rol === "Secretaria Academica" ||
            rol === "Comunicaciones" ||
            rol === "Secretaria Academica."||
            rol === "Recepcion"
           ) {
    return secretaria;
  }else if (rol === "Cajas" ||
            rol === "Administrador de parte Administrativa"
          ) {
    return administracion;
  }
}
var todos = [
  {
    value:"/instituciones",
    label: "Instituciones",
    group: "Academico"
  },
  {
    value:"/facultades",
    label: "Facultades",
    group: "Academico"
  },
  {
    value:"/documentos",
    label: "Documentos",
    group: "Academico"
  },
  {
    value:"/carreras",
    label: "Carreras",
    group: "Academico"
  },
  {
    value:"/materias",
    label: "Materias",
    group: "Academico"
  },
  {
    value:"/monitoreo/academico",
    label: "Monitoreo Academico",
    group: "Academico"
  },
  {
    value:"/inscripciones",
    label: "Inscripciones",
    group: "Academico"
  },
  {
    value:"/autorizacion_docente",
    label: "Autorizacion Docente",
    group: "Administración"
  },
  {
    value:"/formas_pago",
    label: "Metodos de pago",
    group: "Administración"
  },
  {
    value:"/ficha_pago",
    label: "Ficha de pago",
    group: "Administración"
  },
  {
    value:"/historial_pago",
    label: "Historial de pago",
    group: "Administración"
  },
  {
    value:"/planilla/congreso",
    label: "Mesas de congresos",
    group: "Administración"
  },
  {
    value:"/promociones",
    label: "Promociones",
    group: "Administración"
  },
  {
    value:"/monitoreo/administrativo",
    label: "Seguimiento Administrativo",
    group: "Administración"
  },
  {
    value:"/descuentos",
    label: "Descuentos",
    group: "Administración"
  },
  {
    value:"/cajas",
    label: "Cajas",
    group: "Administración"
  },
  {
    value:"/facturas",
    label: "Facturas",
    group: "Administración"
  },
  {
    value:"/asistencia_docente",
    label: "Asistencia para docentes",
    group: "Academico"
  },
  {
    value:"/ficha_cobro/buscar",
    label: "Ficha de Cobro",
    group: "Administración"
  },
  {
    value:"/gestiones",
    label: "Gestiones",
    group: "Administración"
  },
  {
    value:"/tarifas",
    label: "Tarifas",
    group: "Administración"
  },
  {
    value:"/tipo_detalle_tickets",
    label: "Tipo de detalle de factura",
    group: "Administración"
  },
  {
    value:"/tipos_iva",
    label: "Tipos de IVA",
    group: "Administración"
  },
  {
    value:"/proveedores",
    label: "Proveedores",
    group: "Administración"
  },
  {
    value:"/edificios",
    label: "Edificios",
    group: "Definiciones"
  },
  {
    value:"/salas",
    label: "Salas",
    group: "Definiciones"
  },
  {
    value:"/tipos_sala",
    label: "Tipos de sala",
    group: "Definiciones"
  },
  {
    value:"/secciones",
    label: "Secciones",
    group: "Definiciones"
  },
  {
    value:"/categorias",
    label: "Categorias de productos | eventos",
    group: "Definiciones"
  },
  {
    value:"/bachillerato",
    label: "Bachilleres",
    group: "Definiciones"
  },
  {
    value:"/ciudades",
    label: "Ciudades",
    group: "Definiciones"
  },
  {
    value:"/colegios",
    label: "Colegios",
    group: "Definiciones"
  },
  {
    value:"/departamentos",
    label: "Departamentos",
    group: "Definiciones"
  },
  {
    value:"/documentos_requeridos",
    label: "Documentos Requeridos",
    group: "Definiciones"
  },
  {
    value:"/lugares_trabajo",
    label: "Lugares de trabajos",
    group: "Definiciones"
  },
  {
    value:"/paises",
    label: "Paises",
    group: "Definiciones"
  },
  {
    value:"/seguros_medicos",
    label: "Seguros Medicos",
    group: "Definiciones"
  },
  {
    value:"/tipo_contacto",
    label: "Tipo de contacto",
    group: "Definiciones"
  },
  {
    value:"/tipo_carrera",
    label: "Tipo de carreras",
    group: "Definiciones"
  },
  {
    value:"/tipos_evaluacion",
    label: "Tipos de evaluaciones",
    group: "Definiciones"
  },
  {
    value:"/tipos_perfil",
    label: "Tipos de perfil",
    group: "Definiciones"
  },
  {
    value:"/tipo_profesor",
    label: "Tipo de profesores",
    group: "Definiciones"
  },
  {
    value:"/tipo_unidades",
    label: "Tipos de instituciones",
    group: "Definiciones"
  },
  {
    value:"/productos",
    label: "Productos",
    group: "Definiciones"
  },
  {
    value:"/preguntas_retencion",
    label: "Preguntas de retención",
    group: "Definiciones"
  },
  {
    value:"/informes/academico",
    label: "Informes Academicos",
    group: "Informes"
  },
  {
    value:"/informe_eventos",
    label: "Informe de productos y eventos",
    group: "Informes"
  },
  {
    value:"/reportes/balances",
    label: "Menú de informes generales",
    group: "Informes"
  },
  {
    value:"/reportes/balance",
    label: "Informe de balance",
    group: "Informes"
  },
  {
    value:"/reportes/egresos",
    label: "Informe de egresos",
    group: "Informes"
  },
  {
    value:"/reportes/ingresos",
    label: "Informe de ingresos",
    group: "Informes"
  },
  {
    value:"/pre_inscripcion",
    label: "Registro rapido de personas",
    group: "Personas"
  },
  {
    value:"/cargos",
    label: "Cargos",
    group: "Personas"
  },
  {
    value:"/estudiantes",
    label: "Estudiantes",
    group: "Personas"
  },
  {
    value:"/funcionarios",
    label: "Funcionarios",
    group: "Personas"
  },
  {
    value:"/personas",
    label: "Personas",
    group: "Personas"
  },
  {
    value:"/profesores",
    label: "Profesores",
    group: "Personas"
  },
  {
    value:"/roles",
    label: "Roles",
    group: "Personas"
  },
  {
    value:"/usuarios",
    label: "Usuarios",
    group: "Personas"
  },
  {
    value:"/auditoria",
    label: "Auditoria",
    group: "Procesos"
  },
  {
    value:"/reservas",
    label: "Reservas",
    group: "Procesos"
  },
  {
    value:"/consultas",
    label: "Consultas",
    group: "Recepcion"
  },
];

var secretaria = [
  {
    value:"/instituciones",
    label: "Instituciones",
    group: "Academico"
  },
  {
    value:"/facultades",
    label: "Facultades",
    group: "Academico"
  },
  {
    value:"/documentos",
    label: "Documentos",
    group: "Academico"
  },
  {
    value:"/carreras",
    label: "Carreras",
    group: "Academico"
  },
  {
    value:"/materias",
    label: "Materias",
    group: "Academico"
  },
  {
    value:"/monitoreo/academico",
    label: "Monitoreo Academico",
    group: "Academico"
  },
  {
    value:"/inscripciones",
    label: "Inscripciones",
    group: "Academico"
  },
  {
    value:"/monitoreo/administrativo",
    label: "Seguimiento Administrativo",
    group: "Administración"
  },
  {
    value:"/ficha_cobro/buscar",
    label: "Ficha de Cobro",
    group: "Administración"
  },
  {
    value:"/gestiones",
    label: "Gestiones",
    group: "Administración"
  },
  {
    value:"/planilla/congreso",
    label: "Mesas de congresos",
    group: "Administración"
  },
  {
    value:"/tarifas",
    label: "Tarifas",
    group: "Administración"
  },
  {
    value:"/edificios",
    label: "Edificios",
    group: "Definiciones"
  },
  {
    value:"/salas",
    label: "Salas",
    group: "Definiciones"
  },
  {
    value:"/tipos_sala",
    label: "Tipos de sala",
    group: "Definiciones"
  },
  {
    value:"/secciones",
    label: "Secciones",
    group: "Definiciones"
  },
  {
    value:"/categorias",
    label: "Categorias de productos | eventos",
    group: "Definiciones"
  },
  {
    value:"/bachillerato",
    label: "Bachilleres",
    group: "Definiciones"
  },
  {
    value:"/ciudades",
    label: "Ciudades",
    group: "Definiciones"
  },
  {
    value:"/colegios",
    label: "Colegios",
    group: "Definiciones"
  },
  {
    value:"/departamentos",
    label: "Departamentos",
    group: "Definiciones"
  },
  {
    value:"/documentos_requeridos",
    label: "Documentos Requeridos",
    group: "Definiciones"
  },
  {
    value:"/lugares_trabajo",
    label: "Lugares de trabajos",
    group: "Definiciones"
  },
  {
    value:"/paises",
    label: "Paises",
    group: "Definiciones"
  },
  {
    value:"/seguros_medicos",
    label: "Seguros Medicos",
    group: "Definiciones"
  },
  {
    value:"/tipo_contacto",
    label: "Tipo de contacto",
    group: "Definiciones"
  },
  {
    value:"/tipo_carrera",
    label: "Tipo de carreras",
    group: "Definiciones"
  },
  {
    value:"/tipos_evaluacion",
    label: "Tipos de evaluaciones",
    group: "Definiciones"
  },
  {
    value:"/tipos_perfil",
    label: "Tipos de perfil",
    group: "Definiciones"
  },
  {
    value:"/tipo_profesor",
    label: "Tipo de profesores",
    group: "Definiciones"
  },
  {
    value:"/productos",
    label: "Productos",
    group: "Definiciones"
  },
  {
    value:"/preguntas_retencion",
    label: "Preguntas de retención",
    group: "Definiciones"
  },
  {
    value:"/informes/academico",
    label: "Informes Academicos",
    group: "Informes"
  },
  {
    value:"/informe_eventos",
    label: "Informe de productos y eventos",
    group: "Informes"
  },
  {
    value:"/pre_inscripcion",
    label: "Registro rapido de personas",
    group: "Personas"
  },
  {
    value:"/cargos",
    label: "Cargos",
    group: "Personas"
  },
  {
    value:"/estudiantes",
    label: "Estudiantes",
    group: "Personas"
  },
  {
    value:"/funcionarios",
    label: "Funcionarios",
    group: "Personas"
  },
  {
    value:"/personas",
    label: "Personas",
    group: "Personas"
  },
  {
    value:"/profesores",
    label: "Profesores",
    group: "Personas"
  },
  {
    value:"/roles",
    label: "Roles",
    group: "Personas"
  },
  {
    value:"/usuarios",
    label: "Usuarios",
    group: "Personas"
  },
  {
    value:"/reservas",
    label: "Reservas",
    group: "Procesos"
  },
  {
    value:"/consultas",
    label: "Consultas",
    group: "Recepcion"
  },
];

var administracion = [
  {
    value:"/instituciones",
    label: "Instituciones",
    group: "Academico"
  },
  {
    value:"/documentos",
    label: "Documentos",
    group: "Academico"
  },
  {
    value:"/facultades",
    label: "Facultades",
    group: "Academico"
  },
  {
    value:"/carreras",
    label: "Carreras",
    group: "Academico"
  },
  {
    value:"/monitoreo/academico",
    label: "Monitoreo Academico",
    group: "Academico"
  },
  {
    value:"/materias",
    label: "materias",
    group: "Academico"
  },
  {
    value:"/autorizacion_docente",
    label: "Autorizacion Docente",
    group: "Administración"
  },
  {
    value:"/formas_pago",
    label: "Metodos de pago",
    group: "Administración"
  },
  {
    value:"/ficha_pago",
    label: "Ficha de pago",
    group: "Administración"
  },
  {
    value:"/historial_pago",
    label: "Historial de pago",
    group: "Administración"
  },
  {
    value:"/planilla/congreso",
    label: "Mesas de congresos",
    group: "Administración"
  },
  {
    value:"/promociones",
    label: "Promociones",
    group: "Administración"
  },
  {
    value:"/monitoreo/administrativo",
    label: "Seguimiento Administrativo",
    group: "Administración"
  },
  {
    value:"/descuentos",
    label: "Descuentos",
    group: "Administración"
  },
  {
    value:"/cajas",
    label: "Cajas",
    group: "Administración"
  },
  {
    value:"/facturas",
    label: "Facturas",
    group: "Administración"
  },
  {
    value:"/ficha_cobro/buscar",
    label: "Ficha de Cobro",
    group: "Administración"
  },
  {
    value:"/gestiones",
    label: "Gestiones",
    group: "Administración"
  },
  {
    value:"/tarifas",
    label: "Tarifas",
    group: "Administración"
  },
  {
    value:"/tipo_detalle_tickets",
    label: "Tipo de detalle de factura",
    group: "Administración"
  },
  {
    value:"/tipos_iva",
    label: "Tipos de IVA",
    group: "Administración"
  },
  {
    value:"/proveedores",
    label: "Proveedores",
    group: "Administración"
  },
  {
    value:"/categorias",
    label: "Categorias de productos | eventos",
    group: "Definiciones"
  },
  {
    value:"/bachillerato",
    label: "Bachilleres",
    group: "Definiciones"
  },
  {
    value:"/ciudades",
    label: "Ciudades",
    group: "Definiciones"
  },
  {
    value:"/colegios",
    label: "Colegios",
    group: "Definiciones"
  },
  {
    value:"/departamentos",
    label: "Departamentos",
    group: "Definiciones"
  },
  {
    value:"/documentos_requeridos",
    label: "Documentos Requeridos",
    group: "Definiciones"
  },
  {
    value:"/lugares_trabajo",
    label: "Lugares de trabajos",
    group: "Definiciones"
  },
  {
    value:"/paises",
    label: "Paises",
    group: "Definiciones"
  },
  {
    value:"/seguros_medicos",
    label: "Seguros Medicos",
    group: "Definiciones"
  },
  {
    value:"/tipo_contacto",
    label: "Tipo de contacto",
    group: "Definiciones"
  },
  {
    value:"/tipo_carrera",
    label: "Tipo de carreras",
    group: "Definiciones"
  },
  {
    value:"/tipos_evaluacion",
    label: "Tipos de evaluaciones",
    group: "Definiciones"
  },
  {
    value:"/tipos_perfil",
    label: "Tipos de perfil",
    group: "Definiciones"
  },
  {
    value:"/tipo_profesor",
    label: "Tipo de profesores",
    group: "Definiciones"
  },
  {
    value:"/tipo_unidades",
    label: "Tipos de instituciones",
    group: "Definiciones"
  },
  {
    value:"/productos",
    label: "Productos",
    group: "Definiciones"
  },
  {
    value:"/informes/academico",
    label: "Informes Academicos",
    group: "Informes"
  },
  {
    value:"/informe_eventos",
    label: "Informe de productos y eventos",
    group: "Informes"
  },
  {
    value:"/reportes/egresos",
    label: "Informe de egresos",
    group: "Informes"
  },
  {
    value:"/reportes/ingresos",
    label: "Informe de ingresos",
    group: "Informes"
  },
  {
    value:"/pre_inscripcion",
    label: "Registro rapido de personas",
    group: "Personas"
  },
  {
    value:"/cargos",
    label: "Cargos",
    group: "Personas"
  },
  {
    value:"/estudiantes",
    label: "Estudiantes",
    group: "Personas"
  },
  {
    value:"/funcionarios",
    label: "Funcionarios",
    group: "Personas"
  },
  {
    value:"/personas",
    label: "Personas",
    group: "Personas"
  },
  {
    value:"/profesores",
    label: "Profesores",
    group: "Personas"
  },
  {
    value:"/usuarios",
    label: "Usuarios",
    group: "Personas"
  },
];
export {RoutesList};
