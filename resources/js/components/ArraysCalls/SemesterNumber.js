function SemesterInLetter(sems){
  var letter = new Array();
  letter[0] = "Primer Semestre";
  letter[1] = "Segundo Semestre";
  letter[2] = "Tercer Semestre";
  letter[3] = "Cuarto Semestre";
  letter[4] = "Quinto Semestre";
  letter[5] = "Sexto Semestre";
  letter[6] = "Septimo Semestre";
  letter[7] = "Octavo Semestre";
  letter[8] = "Noveno Semestre";
  letter[9] = "Decimo Semestre";
  letter[10] = "Undecimo Semestre";

  return letter[sems -1];
}
function SemesterYearInLetter(param){
  var letter = new Array();
  letter[0] = "Primer Curso";
  letter[1] = "Segundo Curso";
  letter[2] = "Tercer Curso";
  letter[3] = "Cuarto Curso";
  letter[4] = "Quinto Curso";
  letter[5] = "Sexto Curso";
  letter[6] = "Septimo Curso";
  return letter[param -1];
}

export {SemesterInLetter, SemesterYearInLetter};
