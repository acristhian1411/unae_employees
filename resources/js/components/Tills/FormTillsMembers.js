import React, {Component} from 'react';
import {
	Button, TextField, Grid, CircularProgress, Tooltip, Snackbar, FormControl, InputLabel, Select, Checkbox,
	FormControlLabel 
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import SaveIcon from '@material-ui/icons/Save';

export default class FormTillsMembers extends Component {
	constructor(props){
		super(props);
		this.state ={
			filter_employee_name: "",
			filter_employee_id:"",
			filter_employee_time_out: false,
			employees:[],
			tills_role: "Ver",
			expire: false,
			start_date: "",
			expiration_date:"",
			tills_member_id: '',
		}
		this.handleCreate = this.handleCreate.bind(this);
		this.handleUpdate = this.handleUpdate.bind(this);
	}
  Employees() {
    return this.state.employees.map((data) => ({
      label: data.person_fname+' '+data.person_lastname,
      value: data.person_id,
    }));
  }
  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }
	componentDidMount(){
		if(this.props.edit){
			console.log("props.edit");
			console.log(this.props);
			var start_date = new Date(`${this.props.current_member.start_date}`);
	    var start_date_format = start_date.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ start_date.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + start_date.toLocaleDateString("en-US", { day: '2-digit' });
	    if(this.props.current_member.expiration_date == null){
	    	var expiration_date = new Date();
	    	var expiration_date_format = expiration_date.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ expiration_date.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + expiration_date.toLocaleDateString("en-US", { day: '2-digit' });
	    }else{
	    	var expiration_date = new Date(`${this.props.current_member.expiration_date}`);
	   		var expiration_date_format = expiration_date.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ expiration_date.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + expiration_date.toLocaleDateString("en-US", { day: '2-digit' });
	    }
	    this.setState({
				filter_employee_id: this.props.current_member.person_id,
				filter_employee_name: this.props.current_member.person_name,
				expire: this.props.current_member.expire,
				start_date: start_date_format,
				expiration_date: expiration_date_format,
				tills_role: this.props.current_member.tills_role,
				tills_id: this.props.current_member.tills_id,
				tills_member_id: this.props.current_member.tills_member_id
			})
		}else{
			var fecha_hoy = new Date();
	    var fecha_hoy_format = fecha_hoy.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ fecha_hoy.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + fecha_hoy.toLocaleDateString("en-US", { day: '2-digit' });
	    this.setState({start_date: fecha_hoy_format, expiration_date: fecha_hoy_format});	
		}
	}
	searchEmployees(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    this.setState({filter_employee_name: e.target.value})
    // console.log(e.target.value)
    // if (this.state.filter_employee_time_out == false) {
      this.setState({ filter_employee_time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filter_employee_time_out: false,
          });
          // this.updateState();
        } else {

          axios
            .get(
              `/api/employees-search?search=${e.target.value}`
            )
            .then((res) => {
              if(res.data != false){
              this.setState({
                employees: res.data.data,
                filter_employee_time_out: false,
              });
            }else{
              this.setState({
                employees: [],
                filter_employee_time_out: false,
              });
            }
            })
            .catch((error) => {
              this.setState({filter_employee_time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }
  selectsChange(e, values, nameValue, nameLabel) {
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
  }
  handleUpdate(){
  	axios.put(`/api/tills-members_update/${this.state.tills_member_id}`, {
			tills_role: this.state.tills_role,
			start_date: this.state.start_date,
			expire: this.state.expire,
			expiration_date: this.state.expiration_date
		}).then((res)=>{
			this.props.onSuccess("Se guardo con éxito");
		})
  }
	handleCreate(){
		axios.post(`/api/tills-members_create/${this.props.till.till_id}`, {
			person_id: this.state.filter_employee_id,
			tills_role: this.state.tills_role,
			start_date: this.state.start_date,
			expire: this.state.expire,
			expiration_date: this.state.expiration_date
		}).then((res)=>{
			this.props.onSuccess("Se guardo con éxito");
		})
	}
	render(){
		return(
			<div>
				<h3 align="center">{`${this.props.edit?"Editar":"Nuevo"} Integrante de ${this.props.till.till_name}`}</h3>
				<hr />
				{this.props.edit?
					<div>
						<p>{`Integrante: ${this.state.filter_employee_name}`}</p>
						<FormControl
                variant="outlined"
                size="small"
                // style={{ width: '170px' }}
                inputlabelprops={{
                  shrink: true,
                }}
                fullWidth
                style={{ margin: 8 }}
              >
                <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: 'white', padding: '0 5px'}} shrink={true}>Rol</InputLabel>
                <Select
                  native
                  label="Rol"
                  value={this.state.tills_role}
                  inputProps={{
                    name:"Rol",
                    id: "demo-simple-select-helper",
                  }}
                  onChange={(e)=>{
                    this.setState({tills_role: e.target.value})
                  }}
                >
                        <option value={"Administrar"}>{"Administrar"}</option>
                        <option value={"Ver"}>{"Ver"}</option>
                        <option value={"Pagar"}>{"Pagar"}</option>
                      {/*  <option value={2}>{"Otro tipo de reporte"}</option>*/}
                  })}

                </Select>
              </FormControl>
              <TextField
                id="date"
                label="Fecha"
                fullWidth
                type="date"
                className="textField"
                size="small"
                value={this.state.start_date}
                name="start_date"
                onChange={(e) =>{
                  // console.log(e.target.value === "");
                  if(e.target.value === ""){
                    // console.log("aplicar los cambios");
                    this.setState({start_date:''})
                    let filter=[];
                    filter = {date: ""};
                    this.updateState(filter);
                    // this.updateState();
                  }else{
                    this.fieldsChange(e)
                  }
                }}
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
              />
              <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.expire}
                      onChange={(e) => {
                        this.setState({expire:!this.state.expire});
                      }}
                      name="unit_company"
                      color="primary"
                    />
                  }
                  label={`Esta sesión ${this.state.expire? 'vence': 'no vence'}. Click en el cuadro para cambiar`}
                />
              {this.state.expire?
	              <TextField
	                id="date"
	                label="Fecha"
	                fullWidth
	                type="date"
	                className="textField"
	                size="small"
	                value={this.state.start_date}
	                name="start_date"
	                onChange={(e) =>{
	                  // console.log(e.target.value === "");
	                  if(e.target.value === ""){
	                    // console.log("aplicar los cambios");
	                    this.setState({start_date:''})
	                    let filter=[];
	                    filter = {date: ""};
	                    this.updateState(filter);
	                    // this.updateState();
	                  }else{
	                    this.fieldsChange(e)
	                  }
	                }}
	                margin="normal"
	                InputLabelProps={{
	                  shrink: true,
	                }}
	                variant="outlined"
	              />
	              : ''
              }
					</div>
					:
					<div>
						<Autocomplete
                id="combo-box-demo"
                filterSelectedOptions
                options={[this.selectValue('filter_employee_name','filter_employee_id'), ...this.Employees()]}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) =>{
                  if(values === null){
                    // console.log("aplicar los cambios");
                    this.setState({filter_employee_name: '', filter_employee_id:''})
                    let filter = {employee: ""};
                    this.updateState(filter);
                  }else{
                    this.selectsChange(e, values, "filter_employee_id", "filter_employee_name")
                  }
                }}
                value={this.selectValue("filter_employee_name", "filter_employee_id")}
                getOptionSelected={(option, value)=>option.value === value.value}
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Integrante"
                    className="textField"
                    size="small"
                    variant="outlined"
                    // onChange={(e)=>{this.searchEmployees(e)}}
                    onKeyDown={(e) =>{
                      if(e.key === 'Enter'){
                        e.preventDefault();
                        this.searchEmployees(e)
                      }
                    }}
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.filter_employee_time_out === true ? (
                            <CircularProgress color="inherit" size={20} />
                          ) : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
              <FormControl
                variant="outlined"
                size="small"
                // style={{ width: '170px' }}
                inputlabelprops={{
                  shrink: true,
                }}
                fullWidth
                style={{ margin: 8 }}
              >
                <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: 'white', padding: '0 5px'}} shrink={true}>Rol</InputLabel>
                <Select
                  native
                  label="Rol"
                  value={this.state.tills_role}
                  inputProps={{
                    name:"Rol",
                    id: "demo-simple-select-helper",
                  }}
                  onChange={(e)=>{
                    this.setState({tills_role: e.target.value})
                  }}
                >
                        <option value={"Administrar"}>{"Administrar"}</option>
                        <option value={"Ver"}>{"Ver"}</option>
                        <option value={"Pagar"}>{"Pagar"}</option>
                        {/* <option value={"Cobrar"}>{"Cobrar"}</option> */}
                      {/*  <option value={2}>{"Otro tipo de reporte"}</option>*/}
                  })}

                </Select>
              </FormControl>
              <TextField
                id="date"
                label="Fecha"
                fullWidth
                type="date"
                className="textField"
                size="small"
                value={this.state.start_date}
                name="start_date"
                onChange={(e) =>{
                  // console.log(e.target.value === "");
                  if(e.target.value === ""){
                    // console.log("aplicar los cambios");
                    this.setState({start_date:''})
                    let filter=[];
                    filter = {date: ""};
                    this.updateState(filter);
                    // this.updateState();
                  }else{
                    this.fieldsChange(e)
                  }
                }}
                margin="normal"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
              />
              <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.expire}
                      onChange={(e) => {
                        this.setState({expire:!this.state.expire});
                      }}
                      name="unit_company"
                      color="primary"
                    />
                  }
                  label={`Esta sesión ${this.state.expire? 'vence': 'no vence'}. Click en el cuadro para cambiar`}
                />
              {this.state.expire?
	              <TextField
	                id="date"
	                label="Fecha"
	                fullWidth
	                type="date"
	                className="textField"
	                size="small"
	                value={this.state.start_date}
	                name="start_date"
	                onChange={(e) =>{
	                  // console.log(e.target.value === "");
	                  if(e.target.value === ""){
	                    // console.log("aplicar los cambios");
	                    this.setState({start_date:''})
	                    let filter=[];
	                    filter = {date: ""};
	                    this.updateState(filter);
	                    // this.updateState();
	                  }else{
	                    this.fieldsChange(e)
	                  }
	                }}
	                margin="normal"
	                InputLabelProps={{
	                  shrink: true,
	                }}
	                variant="outlined"
	              />
	              : ''
              }
					</div>
				}
				{this.props.edit?
        	<Button fullWidth name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleUpdate}>Actualizar</Button>
        	:
        	<Button fullWidth name="save" variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={this.handleCreate}>Guardar</Button>
        }
			</div>
		)
	}
}