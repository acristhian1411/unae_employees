import React, {Component} from 'react';
import MidModal from "../Modals/MidModal";
import FormTillsMembers from "./FormTillsMembers";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

export default class TillsMembers extends Component {
	constructor(props){
		super(props);
		this.state = {
			snack_open: false,
      message_success: "",
			tills_members: [],
			new_member: false,
			edit: false,
      open: false,
			time_out: false,
			current_member: [],
			permissions: {
        manage_tills: null
      },
		}
		this.handleClose = () => {
      this.setState({ open: false });
    };
		this.updateState = this.updateState.bind(this);
		this.destroy = this.destroy.bind(this);
		this.openSnack = this.openSnack.bind(this);
	}
	componentDidMount(){
		console.log("tills_members");
		console.log(this.props);
		var permissions = [
      "manage_tills"
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
      });
		axios.get(`/api/tills-members/${this.props.till.till_id}`).then(res=>{
			this.setState({tills_members: res.data});
		});
	}
	updateState(){
		this.setState({new_member: false, edit: false});
		axios.get(`/api/tills-members/${this.props.till.till_id}`).then(res=>{
			this.setState({tills_members: res.data});
		});
	}
	openSnack(param){
		this.setState({snack_open: true, message_success: param});
		this.updateState();
	}
	destroy() {
    // event.preventDefault();
    axios.delete(`/api/tills-members_delete/${this.state.current_member.tills_member_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }
	render(){
		var addMember = '';
		var showDialogDestroy = '';
		var showSnack = '';
		const {
      snack_open,
      message_success,
      open,
      new_member,
      edit,
      current_member,
      permissions
    } = this.state;
		if(new_member){
			addMember = <MidModal >
                    {{onHandleSubmit: this.updateState,
                      form: <FormTillsMembers />,
                      props_form: {onSuccess:this.openSnack, till: this.props.till, edit: false}
                    }}
                  </ MidModal>
		}else if (edit) {
      // showModal = <FormDepartment edit ={true} department={department} onHandleSubmit={this.updateState}/>
      addMember = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormTillsMembers />,
            props_form: {
              edit: true,
              till: this.props.till,
              current_member: current_member,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }else{
			addMember = "";
		}
		 if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={current_member.person_name}
          onClose={this.handleClose}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
		return (
			<div>
				<TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell>
                    <h3>Integrante</h3>
                </TableCell>
                <TableCell>
                    <h3>Rol</h3>
                </TableCell>
                <TableCell>
                    <h3>Inicio</h3>
                </TableCell>
                <TableCell>
                    <h3>Vencimiento</h3>
                </TableCell>
                <TableCell colSpan="2">
                	{(permissions.manage_tills || this.props.rol === 'Administrar') &&
                		<Button
                      name="new_item"
                      variant="outlined"
                      color="primary"
                      type="submit"
                      onClick={() => {
                        this.setState({new_member: true})
                      }}
                    >
                      <AddBoxOutlinedIcon />
                    </Button>
                	}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {this.renderList()}
            </TableBody>
          </Table>
        </TableContainer>
				{addMember}
				{showDialogDestroy}
        {showSnack}
			</div>
		)
	}
	renderList(){
		const {
      permissions
    } = this.state;
		if(this.state.tills_members.length ===0){
			return (
				<TableRow>
					<TableCell colSpan="5">No hay datos</TableCell>
				</TableRow>
			)	
		}else{
			return(
				this.state.tills_members.map((row,index)=>{
					return(
						<TableRow key={index}>
              <TableCell>{Capitalize(row.person_name)}</TableCell>
              <TableCell>{Capitalize(row.tills_role)}</TableCell>
              <TableCell>{Capitalize(row.start_date)}</TableCell>
              <TableCell>{row.expire == true?(Capitalize(row.expiration_date)):('No vence')
              
              }</TableCell>
              <TableCell>
              	{(permissions.manage_tills || this.props.rol === 'Administrar') &&
              		<Button
                    name="edit_item"
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.setState({ current_member: row, edit: true });
                    }}
                  >
                    <EditIcon />
                  </Button>
              	}
              </TableCell>
              <TableCell>
              	{(permissions.manage_tills || this.props.rol === 'Administrar') &&
                  <Button
                    name="delete_item"
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      // this.handleClickOpen({ data: data }),
                        this.setState({ current_member: row, open: true });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                }
              </TableCell>
            </TableRow>
					)
				})
			)
		}
		
	}
}