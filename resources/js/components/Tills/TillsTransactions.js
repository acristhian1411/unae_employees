import ModalForm from "./ModalForm";
import React, { Component, lazy } from "react";
import DialogDestroy from "../Dialogs/DialogDestroy";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
  IconButton,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { Autocomplete, Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import TicketShow from "../Tickets/TicketShow";
import TillTransferShow from "../TillTransfers/TillTransferShow";
import TillDetailShow from "../TillsDetails/TillDetailShow";
// import ProviderAccountShow from "../ProviderAccount/ProviderAccountShow";
import EmployeeAccountShow from "../EmployeeAccount/EmployeeAccountShow";
import {format, disformat} from '../GlobalFunctions/Format';

class TillsTransactions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      dettills_id:0,
      dettills_description:0,
      dettills_type:true,
      dettills_type_label:'Ingreso',
      message_success: "",
      tills: [],
      tillsdetails: [],
      till_id: 0,
      units: [],
      till: [],
      from_date: "",
      to_date: "",
      provider: false,
      employee: false,
      transfer: false,
      tickets: false,
      detail: false,
      new: false,
      open: false,
      search: "",
      filteredtills: [],
      prevActiveStep: 1,
      rowsPerPage: 10,
      paginator: [],
      order: "asc",
      orderBy: "date",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: `/api/tillDetails-till/${this.props.till}?filter=false`,
      permissions: {
        tills_details_index: null,
        tills_details_show: null,
        tills_details_store: null,
        tills_details_update: null,
        tills_details_destroy: null,
      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order,
      route
    ) => {
      let res;
      if (route) {
        res = await axios.get(
          `${route}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}&per_page=${perPage}&page=${page}&order=${order}&sort_by=${orderBy}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          tills: data,
          paginator: data,
          filteredtills: data.data,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        employee: false,
        provider: false,
        transfer: false,
        tickets: false,
        detail: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    // this.clickSortByName = this.clickSortByName.bind(this)
    // this.clickSortByCode = this.clickSortByCode.bind(this)
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, tillsdetails: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };

    //fin constructor
  }

  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "tills_details_index",
      "tills_details_show",
      "tills_details_store",
      "tills_details_update",
      "tills_details_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({
          permissions: res.data,
          from_date: this.getCurrentDate(),
          to_date: this.getCurrentDate(),
        });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  //
  // clickSortByName(){
  //    this.setState({prevActiveStep: 1});
  //     this.getObject(this.state.rowsPerPage, 1);
  //  }
  //
  //  clickSortByCode(){
  //     this.setState({prevActiveStep: 1});
  //      this.getObject(this.state.rowsPerPage, 1);
  //   }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredtills: this.state.tills.data,
            paginator: this.state.tills,
            page_lenght: this.state.tills.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/tills`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/tills?till_name=${e.target.value}`,
          });
          axios
            .get(`${this.state.url}`)
            .then((res) => {
              this.setState({
                filteredtills: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  deleteObject() {
    axios.delete(`api/tillDetails/${this.state.dettills_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data.message });
      this.updateState();
    });
  }

  clickEditar(data) {
    var d = data.data.description;
    if (d.match(/Transferencia.*/)) {
      this.setState({
        transfer: true,
        till: data.data.ref_id,
      });
    } else if (d.match(/Ingreso.*/)) {
      this.setState({
        detail: true,
        till: data.data.dettills_id,
      });
    } else if (d.match(/aranceles.*/)) {
      this.setState({
        tickets: true,
        till: data.data.ref_id,
      });
    } else if (d.match(/Apertura.*/) || d.match(/Cierre.*/)) {
      this.setState({
        detail: true,
        till: data.data.dettills_id,
      });
    } else if (d.match(/proveedor.*/)) {
      this.setState({
        provider: true,
        till: data.data.ref_id,
      });
    } else if (d.match(/funcionario.*/)) {
      this.setState({
        employee: true,
        till: data.data.ref_id,
      });
    }
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  changeFields(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  selectsChange(e, values, nameValue, nameLabel){
    this.setState({
      [nameValue]: values.value,
      [nameLabel]: values.label,
    });
  }

  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      filteredtills,
      till,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.transfer) {
      // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TillTransferShow />,
            props_form: {
              onSuccess: this.openSnack,
              transfer: this.state.till,
            },
          }}
        </MidModal>
      );
    } else if (this.state.tickets) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TicketShow />,
            props_form: {
              ticket: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.detail) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TillDetailShow />,
            props_form: {
              details: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.employee) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <EmployeeAccountShow />,
            props_form: {
              account: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={this.state.tillsdetails.data.dettills_description}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filteredtills.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body">
        {console.log('this.state.tillsdetails')}
        {console.log(this.state.tillsdetails)}
        {/*<Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=today`
            );
          }}
        >
          De Hoy
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=week`
            );
          }}
        >
          De Esta semana
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=month`
            );
          }}
        >
          De Este mes
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=false`
            );
          }}
        >
          Todo
        </Button>*/}

        <TextField
          id="date"
          label="Desde"
          type="date"
          name="from_date"
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e) => {
            this.changeFields(e);
          }}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="date"
          label="Hasta"
          type="date"
          name="to_date"
          // defaultValue="2017-05-24"
          value={this.state.to_date}
          onChange={(e) => {
            this.changeFields(e);
          }}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Autocomplete
              id="combo-box-demo"
              disableClearable
              options={[
                {
                  value: false,
                  label: "Egreso",
                },
                {
                  value: true,
                  label: "Ingreso",
                }
              ]}
              getOptionLabel={(option) => option.label}
              onChange={(e, values) =>
                this.selectsChange(
                  e,
                  values,
                  "dettills_type",
                  "dettills_type_label"
                )
              }
              value={this.selectValue("dettills_type", "dettills_type_label")}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              size='small'
              
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Tipo de transaccion *"
                  className="textField"
                  variant="outlined"
                />
              )}
            />
        <IconButton
          aria-label="search"
          onClick={() =>{
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?from_date=${this.state.from_date}&to_date=${this.state.to_date}&dettills_type=${this.state.dettills_type}`
            ), this.setState({url:`/api/tillDetails-till/${this.props.till}?from_date=${this.state.from_date}&to_date=${this.state.to_date}&dettills_type=${this.state.dettills_type}`})}
          }
        >
          <SearchIcon />
        </IconButton>

        <hr />
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "td_descrip" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "td_descrip"}
                    direction={orderBy === "td_descrip" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("td_descrip");
                    }}
                  >
                    <h3>Descripcion</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "emp_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "emp_name"}
                    direction={orderBy === "emp_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("emp_name");
                    }}
                  >
                    <h3>Prov/Alumno</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "cajero_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "cajero_name"}
                    direction={orderBy === "cajero_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("cajero_name");
                    }}
                  >
                    <h3>Usuario</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell sortDirection={orderBy === "pay_type_desc" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "pay_type_desc"}
                    direction={orderBy === "pay_type_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("pay_type_desc");
                    }}
                  >
                    <h3>Metodo</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "dettills_pr_pay_desc" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "dettills_pr_pay_desc"}
                    direction={orderBy === "dettills_pr_pay_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("dettills_pr_pay_desc");
                    }}
                  >
                    <h3>Comprobante</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "date" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "date"}
                    direction={orderBy === "date" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("date");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "amount" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "amount"}
                    direction={orderBy === "amount" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("amount");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell
                  sortDirection={orderBy === "dettills_type" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "dettills_type"}
                    direction={orderBy === "dettills_type" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("dettills_type");
                    }}
                  >
                    <h3>Tipo</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell colSpan="2"></TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
        {/*<div className="till-seccion-totales" align="center">
                  <h4>Total</h4>
                </div>*/}
      </div>
    );
  }

  selectValue(name, label) {
    return {
      label: this.state[label],
      value: this.state[name],
    };
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredtills.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredtills.map((data) => {
        return (
          <TableBody key={data.dettills_id}>
            <TableRow>
              <TableCell>{data.td_descrip}</TableCell>
              <TableCell>{data.emp_name}</TableCell>
              <TableCell>{data.cajero_name}</TableCell>
              <TableCell>{data.pay_type_desc}</TableCell>
              <TableCell>{data.dettills_pr_pay_desc}</TableCell>
              <TableCell>{data.date}</TableCell>
              <TableCell>{format(Math.round(data.amount))}</TableCell>
              <TableCell>
                {data.dettills_type === true ? <p>Ingreso</p> : <p>Egreso</p>}
              </TableCell>
              <TableCell>
                {permissions.tills_details_show === true && (
                  <Tooltip title="Mostrar">
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<VisibilityIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>

              {/*<TableCell>
                {permissions.tills_details_update === true && (
                  <Tooltip title="Editar">
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<EditIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>*/}
              <TableCell>
                {permissions.tills_details_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ dettills_id: data.dettills_id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}

export default TillsTransactions;
