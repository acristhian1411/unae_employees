import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Tabs,
  Tab,
  Typography,
  Button,
  Grid,
  Snackbar,
} from "@material-ui/core";
import { Card } from "react-bootstrap";
import { Alert } from "@material-ui/lab";
import PersonPayments from "../Homes/PersonPayments";
import HomeIcon from "@material-ui/icons/Home";
import ListIcon from "@material-ui/icons/List";
import SchoolIcon from "@material-ui/icons/School";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import TillsTransactions from "./TillsTransactions";
import TillTransfer from "../TillTransfers/TillTransfer";
import TillStart from "../TillsDetails/TillStart";
import TillClose from "../TillsDetails/TillClose";
import TillsMembers from "../Tills/TillsMembers";
import MidModal from "../Modals/MidModal";
import BigModal from "../Modals/BigModal";
import FlagIcon from "@material-ui/icons/Flag";
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    height: 140,
  },
});

class TillShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      snack_alert: "",
      tills: {},
      till_name:'',
      prev_till_name:'',
      start: false,
      modal:false,
      person_type:[],
      close: false,
      insert: false,
      value: 0,
      cajaAmount: 0,
      current_role:[],
      mytills:[]
    };

    this.getObject = async () => {
      let res;
      res = await axios.get(`/api/tills/${this.props.match.params.id}`);
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          tills: data.data,
          till_name:data.data.till_name
        });
    this.props.changeTitlePage(data.data.till_name);

        if (res.data.data.till_starting === true) {
          axios
            .get("/api/tillDetails-sum/" + this.props.match.params.id)
            .then((res) => {
              this.setState({ cajaAmount: res.data.suma });
            });
        }
      }
      axios.get(`/api/tills-current-member/${this.props.match.params.id}`).then(res=>{
        this.setState({current_role: res.data});
      });
    };

    this.updateState = () => {
      this.getObject();
      this.setState({ start: false, close: false, insert: false, modal:false });
    };
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  openModal(name) {
    this.setState({ [name]: true });
  }
  closeModal() {
    this.setState({ modal: false });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(message, result = 'success') {
    // console.log(param);
    this.setState({ snack_open: true, message_success: message, snack_alert: result });
  }
  componentDidMount() {
    console.log("TillShow");
    this._ismounted = true;
    this.getObject();
    axios.get('/api/till_is_starting').then(res=>{
        this.setState({mytills: res.data[0] != undefined ? res.data[0] : false});
    }).catch(error=>{
      console.log(error);
    });
  }


  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes } = this.props;
    const { tills, value, snack_open, message_success } = this.state;
    var showSnack;

    var modal;
    if (this.state.start === true) {
      modal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TillStart />,
            props_form: {
              till: tills,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.close === true) {
      modal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TillClose />,
            props_form: {
              till: tills,
              status: "close",
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.insert === true) {
      modal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TillClose />,
            props_form: {
              till: tills,
              status: "insert",
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if(this.state.modal == true){
      modal = (
        <BigModal>
          {{
            onHandleSubmit: this.updateState,
            form: <PersonPayments />,
            props_form: {
              till_id: tills.till_id,
              person_type: this.state.person_type,
              onSuccess: this.openSnack,
            },
          }}
        </BigModal>
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity={this.state.snack_alert}
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div className={classes.root}>
<div className="breadcrumb-container">
          <ul className="breadcrumb-list">
            <li className="breadcrumb-item">
              <Link className="breadcrumb-link" to={"/"}>
                <HomeIcon className="breadcrumb-icon" />
                Inicio
                <ChevronRightIcon className="breadcrumb-icon" />
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link className="breadcrumb-link" to={"/cajas"}>
                <ListIcon className="breadcrumb-icon" />
                Lista de cajas
                <ChevronRightIcon className="breadcrumb-icon" />
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Typography style={{ color: "#666" }}>
                <FlagIcon className="breadcrumb-icon" />
                {this.state.tills.till_name != undefined && this.state.tills.till_name}
              </Typography>
            </li>
          </ul>
        </div>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
          >
            <Tab label="Datos" />
            <Tab label="Transacciones" />
            <Tab label="Transferencias" />
            <Tab label="Integrantes" />
          </Tabs>
        </AppBar>
        {value === 0 && (
          <TabContainer>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={6}>
            <div aling="left">
              <Card style={{ width: "25rem" }}>
                <Card.Body>
                  <Card.Title>Descripcion:</Card.Title>
                  <Card.Text>{tills.till_name}</Card.Text>

                  <Card.Title>Estado:</Card.Title>
                  <Card.Text>
                    {tills.till_starting === true
                      ? "Caja abierta"
                      : "Caja cerrada"}
                  </Card.Text>
                  {tills.till_starting === true && (
                    <Card.Text>
                      Monto en caja:{" "}
                      {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.cajaAmount)}
                    </Card.Text>
                  )}
                  <Card.Title>Institucion:</Card.Title>
                  <Card.Text>{tills.unit_name}</Card.Text>
                </Card.Body>
              </Card>
            </div>
            <div aling="right">
              {this.state.tills.till_starting === false ? (
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={() => {
                    this.openModal("start");
                  }}
                >
                  Abrir caja
                </Button>
              ):this.state.mytills !== false?
                (
                  this.state.mytills.till_id == this.state.tills.till_id ||
                    (this.state.current_role != false &&  ["Administrar", "Pagar", "Cobrar"].includes(this.state.current_role[0].tills_role))?
                  <div>
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        this.openModal("close");
                      }}
                    >
                      Cerrar caja
                    </Button>
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        this.openModal("insert");
                      }}
                    >
                      Ingresar dinero en caja
                    </Button>

                  </div>
                  :''
                )
              :
              (
                <div>
                  <p>Cajero: {this.state.tills.cajero_name}</p>
                <Button
                  variant="contained"
                  color="primary"
                  disabled
                >
                  Caja no disponible
                </Button>
               </div>
              )
              }
            </div>
            </Grid>
            {
            this.state.mytills != false &&(
              this.state.mytills.till_id == this.state.tills.till_id || (this.state.current_role != false &&
                ["Administrar", "Pagar"].includes(this.state.current_role[0].tills_role))?
    <Grid item xs={6}>
      {this.state.tills.till_starting == true && (<div>
        <Button variant="outlined" color="secondary"  onClick={()=>{
          this.setState({person_type:['employee'],
      modal:true
      })}} >Pago a Funcionarios</Button>
        <Button variant="outlined" color="secondary"  onClick={()=>{
          this.setState({
            person_type:['provider'],
      modal:true
      })}}>Pago a Proveedores</Button>
        <Button variant="outlined" color="secondary"  onClick={()=>{
          this.setState({
            person_type:['professor'],
      modal:true
      })}}>Pago a Docentes</Button>
      </div>)}
      
    </Grid>  
    :''
            )
            
            }
            
            </Grid>
            </Grid>
            {modal}
            {showSnack}
          </TabContainer>
        )}
        {value === 1 && (
          <TabContainer>
            <TillsTransactions till={tills.till_id} />
          </TabContainer>
        )}
        {value === 2 && (
          <TabContainer>
            {this.state.mytills.till_id == this.state.tills.till_id ? 
            <TillTransfer till={tills.till_id} rol={"Cobrar"} />
            :(this.state.current_role != false &&
                          ["Administrar", "Pagar", "Cobrar"].includes(this.state.current_role[0].tills_role))?

            <TillTransfer till={tills.till_id} rol={this.state.current_role[0].tills_role} />
            :
            <TillTransfer till={tills.till_id} />
          }
          </TabContainer>
        )}
        {value === 3 && (
          <TabContainer>
            {this.state.mytills.till_id == this.state.tills.till_id ? 
            <TillsMembers till={tills} rol={"Cobrar"} />
            :(this.state.current_role != false &&
                          ["Administrar", "Pagar", "Cobrar"].includes(this.state.current_role[0].tills_role))?

            <TillsMembers till={tills} rol={this.state.current_role[0].tills_role} />
            :
            <TillsMembers till={tills} />
          }
          </TabContainer>
        )}
      </div>
    );
  }
}

TillShow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TillShow);
