import React, {Component} from 'react';
import TillStart from "../TillsDetails/TillStart";
import TillClose from "../TillsDetails/TillClose";
import {
  Button,
  Stepper,
  Step,
  StepLabel,
  Divider,
  Snackbar,
  Grid
} from "@material-ui/core";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";

function getSteps() {
  return ["Cerrar caja", "Abrir caja"];
  // return ['Paso 1', 'Paso 2', 'Paso 3', 'Contactos'];
}

export default class TillRestart extends Component {
	constructor(props){
		super(props);
		this.state ={
			activeStep: 1,
			till: "",
			view_mode: 0,
			wanna_continue: 0
		}
	}
	componentDidMount(){
		console.log("formulario para reabrir caja");
		console.log(this.props.till.till_type);
		if(this.props.till.till_type == 1){
			this.setState({till:this.props.till, view_mode:0});
		}else if(this.props.till.till_type == 2){
			this.setState({till:this.props.till, view_mode:1});
		}else{
			console.log("ha ocurrido un error inesperado tillRestart 37");
		}
		
	}
	render(){
		const steps = getSteps();
		// var {till} = this.state;
		//aca se debe tener un formulario con dos pasos en el primer paso tiene que despliegar para cerrar caja y el siguiente para abrir
		return(
			<div>
			{this.state.view_mode === 0?
				<div>
	        <Stepper activeStep={this.state.activeStep} alternativeLabel>
	          {steps.map((label) => (
	            <Step key={label}>
	              <StepLabel>{label}</StepLabel>
	            </Step>
	          ))}
	        </Stepper>
	        {this.state.activeStep ===1?
	        	<TillClose 
							till={this.props.till}
		          status="close"
		          onSuccess= {()=>{this.setState({activeStep:2})}}
						/>
					:this.state.activeStep ===2?
						<TillStart 
							till={this.props.till}
		          onSuccess= {()=>{this.props.onSuccess()}}
						/>
					: ''
	        }
				</div>
				:this.state.view_mode ===1?
					<div>
						{this.state.wanna_continue ===0?
							<div>
								<h3 align="center">
									{`Tiene una caja abierta ("${this.props.till.till_name}"). Elija una opción.`}			
								</h3>		
								<Divider/>
								<Grid container item xs={12} spacing={2} >
                	<Grid item xs={4}>
                		<Button 
                			variant="outlined"
                    	color="primary"
                			fullWidth
                			onClick={()=>{axios.put(`/api/tills_continue/${this.props.till.till_id}`).then((res)=>{this.props.onSuccess()})}}
                			>
                			Continuar usando con el saldo anterior 
                		</Button>
                	</Grid>
                	<Grid item xs={4}>
                	<Button 
                			variant="outlined"
                    	color="primary"
                			fullWidth
                			onClick={()=>{this.setState({view_mode:2})}}
                			>
                			Cerrar caja
                		</Button>
                	</Grid>
                	<Grid item xs={4}>
                	<Button 
                			variant="outlined"
                    	color="primary"
                			fullWidth
                			onClick={()=>{this.setState({view_mode:0})}}
                			>
                			Reabrir caja
                		</Button>
                	</Grid>
								</Grid>

							</div>
							:''
						}
					</div>
				:this.state.view_mode ===2 ?
					<div>
						<TillClose 
							till={this.props.till}
		          status="close"
		          onSuccess= {()=>{this.props.onSuccess()}}
						/>
					</div>
				:''
			}
				
			</div>
		);
	}
}