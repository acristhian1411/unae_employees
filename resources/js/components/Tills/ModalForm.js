import React, { Component } from "react";
import {
  Modal,
  Backdrop,
  Fade,
  InputLabel,
  TextField,
  Checkbox,
  Button,
  Snackbar,
  FormControl,
  Select
} from "@material-ui/core";
import { Autocomplete, Alert } from "@material-ui/lab";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      open: true,
      setOpen: true,
      till_id: 0,
      till_name: "",
      till_account_number: "",
      unit_id: null,
      unit_name: "",
      till_type: "",
      edit: false,
      units: [],
      errors: [],
      validator: {
        till_name: {
          message: "",
          error: false,
        },
        till_account_number: {
          message: "",
          error: false,
        },
        unit_id: {
          message: "",
          error: false,
        },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onChangeField1 = this.onChangeField1.bind(this);
    this.onChangedSelect1 = this.onChangedSelect1.bind(this);
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.handleUpdateObject = this.handleUpdateObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  componentDidMount() {
    if (this.props.edit) {
      var id = this.props.till.data.till_id;
      axios.get("/api/tills/" + id).then((response) => {
        this.setState({
          till_id: response.data.data.till_id,
          till_name: response.data.data.till_name,
          unit_id: response.data.data.unit_id,
          unit_name: response.data.data.unit_name,
          till_account_number: response.data.data.till_account_number,
          till_type: response.data.data.till_type
        });
      });
    }
    axios.get("/api/units").then((response) => {
      this.setState({
        units: response.data.data,
      });
    });
  }

  selectReturn() {
    return this.state.units.map((data) => ({
      label: data.unit_name,
      value: data.unit_id,
    }));
  }

  handleOpen() {
    this.setState({
      setOpen: true,
    });
  }

  handleClose() {
    this.setState({
      open: false,
    });
    this.props.onHandleSubmit();
  }

  onChangeField1(e) {
    this.setState({
      till_name: e.target.value,
    });
    this.props.changeAtribute(true);
  }

  //funcion para capturar inputs tipo int
  onChangedSelect1(e, values) {
    this.setState({ unit_id: values.value });
    this.setState({ unit_name: values.label });
    this.props.changeAtribute(true);
  }

  handleCreateObject(e) {
    e.preventDefault();
    var { till_name, unit_id } = this.state;
    var validator = {
      till_name: { error: false, message: "" },
      unit_id: { error: false, message: "" },
    };
    var object_error = {};
    const object = {
      unit_id: this.state.unit_id,
      till_name: this.state.till_name.toUpperCase(),
      till_type: this.state.till_type,
      till_account_number: this.state.till_account_number
    };
    const { history } = this.props;
    axios
      .post("/api/tills", {
        till_name: object.till_name,
        unit_id: object.unit_id,
        till_account_number: object.till_account_number,
        till_type: object.till_type
      })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var { till_name, unit_id } = this.state;
    var validator = {
      till_name: { error: false, message: "" },
      unit_id: { error: false, message: "" },
    };
    var object_error = {};
    const object = {
      till_id: this.state.till_id,
      unit_id: this.state.unit_id,
      till_name: this.state.till_name.toUpperCase(),
      till_type: this.state.till_type,
      till_account_number: this.state.till_account_number
    };
    const id = object.till_id;
    axios
      .put(`/api/tills/${id}`, {
        till_name: object.till_name,
        unit_id: object.unit_id,
        till_type: object.till_type,
        till_account_number: object.till_account_number
      })
      .then((res) => {
        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  botonEdit() {
    if (this.props.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleUpdateObject}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
      );
    }
  }

  render() {
    const { classes } = this.props;
    const { validator, snack_open } = this.state;
    var showSnack;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["till_name"]["error"] ? (
              <li>Error: {validator["till_name"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["unit_id"]["error"] ? (
              <li>Error: {validator["unit_id"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3>Formulario de Cajas</h3>
        <hr />
        <form>
          <Autocomplete
            id="combo-box-demo"
            disableClearable
            options={this.selectReturn()}
            getOptionLabel={(option) => option.label}
            onChange={this.onChangedSelect1}
            value={{ label: this.state.unit_name, value: this.state.unit_id }}
            getOptionSelected={(option, value) => {
              if (value === option.value) {
                return option.label;
              } else {
                return false;
              }
            }}
            style={{ margin: 8 }}
            fullWidth
            renderInput={(params) => (
              <TextField
                {...params}
                error={this.state.validator.unit_id.error}
                helperText={this.state.validator.unit_id.message}
                label="Selecione una Unidad *"
                variant="outlined"
                fullWidth
              />
            )}
          />
          <div className="form-group">
            <TextField
              id="outlined-full-width"
              label="Descripcion"
              error={this.state.validator.till_name.error}
              helperText={this.state.validator.till_name.message}
              style={{ margin: 8 }}
              value={this.state.till_name}
              onChange={this.onChangeField1}
              fullWidth
              margin="normal"
              variant="outlined"
            />
          </div>
          <FormControl
                variant="outlined"
                inputlabelprops={{
                  shrink: true,
                }}
                className="input_small"
                fullWidth
                style={{margin: 8}}
              >
                <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: '#fafafa', padding: '0 5px'}} shrink={true}>Tipo de caja:</InputLabel>
                <Select
                  native
                  label="Tipo de caja:"
                  value={this.state.till_type}
                  inputProps={{
                    name:"Tipo de caja:",
                    id: "demo-simple-select-helper",
                  }}
                  onChange={(e)=>{
                    this.setState({till_type: e.target.value})
                  }}
                  style={{height: 56}}
                >
                  <option value={1}>Normal</option>
                  <option value={2}>Continua</option>
                  <option value={3}>Banco</option>
                </Select>
              </FormControl>
              {this.state.till_type == 3 &&(
                <div className="form-group">
                  <TextField
                    id="outlined-full-width"
                    label="Numero de cuenta"
                    error={this.state.validator.till_account_number.error}
                    helperText={this.state.validator.till_account_number.message}
                    style={{ margin: 8 }}
                    value={this.state.till_account_number}
                    onChange={(e)=>{
                      this.setState({till_account_number:e.target.value})
                    }}
                    fullWidth
                    margin="normal"
                    variant="outlined"
                  />
                </div>
              )}
             
          {this.botonEdit()}
          {showSnack}

          <Button
            variant="outlined"
            color="secondary"
            startIcon={<ExitToAppIcon />}
            onClick={this.handleClose}
            onClick={this.handleClose}
          >
            Cancelar
          </Button>
        </form>
      </div>
    );
  }
}
export default ModalForm;
