import ModalForm from "./ModalForm";
import React, { Component, lazy } from "react";
import DialogDestroy from "../Dialogs/DialogDestroy";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  IconButton,
  TableSortLabel,
  TableFooter,
  InputBase,
  Checkbox,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
// const ModalForm = lazy(()=> import('./ModalForm'));
import { Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import InfoIcon from "@material-ui/icons/Info";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import SearchIcon from '@material-ui/icons/Search';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import { getAuth } from "../../contexts";

class TillsList extends Component {
  constructor() {
    super();
    this.state = {
      snack_open: false,
      message_success: "",
      tills: [],
      till_id: 0,
      units: [],
      till: [],
      mytills:[],
      edit: false,
      new: false,
      open: false,
      checkedIdArray:[],
      search: "",
      filteredtills: [],
      prevActiveStep: 1,
      rowsPerPage: 10,
      paginator: [],
      order: "asc",
      orderBy: "till_name",
      page_lenght: 0,
      page: 1,
      snack: 'success',
      time_out: false,
      all: false,
      url: "/api/tills",
      busqueda: false,
      permissions: {
        tills_index: null,
        tills_show: null,
        tills_store: null,
        tills_update: null,
        tills_destroy: null,
        manage_tills: null,
      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          tills: data,
          paginator: data,
          filteredtills: data.data,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    //funciones
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    // this.clickSortByName = this.clickSortByName.bind(this)
    // this.clickSortByCode = this.clickSortByCode.bind(this)
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, till: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };

    //fin constructor
  }

  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("CAJAS");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "tills_index",
      "tills_show",
      "tills_store",
      "tills_update",
      "tills_destroy",
      "manage_tills"
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
      axios.get('/api/till_is_starting').then(res=>{
        this.setState({mytills: res.data[0] != undefined ? res.data[0] : false});
      }).catch(error=>{
        console.log(error);
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  //
  // clickSortByName(){
  //    this.setState({prevActiveStep: 1});
  //     this.getObject(this.state.rowsPerPage, 1);
  //  }
  //
  //  clickSortByCode(){
  //     this.setState({prevActiveStep: 1});
  //      this.getObject(this.state.rowsPerPage, 1);
  //   }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  handle_submit(data){
    // axios.post(`/api/tillDetailsReport`,{tills:this.state.checkedIdArray}).then((res) => {
    //   console.log(res.data)
    // })
    if(this.state.checkedIdArray.length !== 0){
      if(data == 'pdf'){
        open(`/reportes/caja_agrupado?type=caja_agrupado&response=report&tills=${this.state.checkedIdArray}`);
      }else if(data == 'view'){
        open(`/#/reporte_caja_agrupado/${this.state.checkedIdArray}`);
      }
    }else{
      this.setState({ snack_open: true, message_success: 'Seleccione al menos una caja', snack:'error' });
    }

  }
  onCheck(id) {
    if (this.state.checkedIdArray.includes(id)) {
      this.setState({
        checkedIdArray: this.state.checkedIdArray.filter(
          (val) => val !== id
        ),
      });
    } else {
      var till = this.state.filteredtills
      if(this.state.permissions.manage_tills == true){
        var valid = till.filter(x=>x.till_id==id )

      }else{
        var valid = till.filter(x=>x.till_id==id && (x.myrol != 'ninguno' || this.state.mytills.till_id == x.till_id))

      }
      if(valid.length > 0){
        this.setState({
          checkedIdArray: [...this.state.checkedIdArray, id],
        });
      }else{
      console.log('no se puede agregar')

      }
      
    }
  }
  checkAll() {
    if(this.state.all === false){
      this.setState({ all: true})
      var a = [];
      
      var till = this.state.filteredtills
      if(this.state.permissions.manage_tills == true){
        var valid = till

      }else{
        var valid = till.filter(x=> (x.myrol != 'ninguno' || this.state.mytills.till_id == x.till_id))

      }
      // if(valid.length > 0){
      //   this.setState({
      //     checkedIdArray: [...this.state.checkedIdArray, id],
      //   });
        
      // }else{
      // console.log('no se puede agregar')

      // }

      valid.forEach((item, i) => {
      a.push(item.till_id)
    });
    this.setState({
      checkedIdArray: [...this.state.checkedIdArray, ...a]
    });
    
    }else{
      this.setState({ all: false, checkedIdArray:[]})

    }


  }
  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredtills: this.state.tills.data,
            paginator: this.state.tills,
            page_lenght: this.state.tills.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/tills`,
          });
          this.updateState();
        } else {
          var url = `/api/tills?till_name=${e.target.value}`
          this.setState({
            url: url,
          });
          axios
            .get(`${url}`)
            .then((res) => {
              this.setState({
                filteredtills: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  deleteObject() {
    axios.delete(`api/tills/${this.state.till_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data.message, snack:'success' });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      till: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      filteredtills,
      till,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.new) {
      // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: {
              edit: true,
              till: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={till.data.till_name}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity={this.state.snack}
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filteredtills.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body" style={{textAlign: "center"}}>
          <TextField
            id="outlined-search"
            label="Escriba para buscar"
            type="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "till_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "till_name"}
                    direction={orderBy === "till_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("till_name");
                    }}
                  >
                    <h3>Descripcion</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "till_starting" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "till_starting"}
                    direction={orderBy === "till_starting" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("till_starting");
                    }}
                  >
                    <h3>Estado</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "cajero_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "cajero_name"}
                    direction={orderBy === "cajero_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("cajero_name");
                    }}
                  >
                    <h3>Cajero</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "updated_at" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "updated_at"}
                    direction={orderBy === "updated_at" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("updated_at");
                    }}
                  >
                    <h3>Ultima Actualización</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell >
                  {permissions.tills_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
                <TableCell colSpan="2">
                <Tooltip title="Informe de cierres de caja">
                <IconButton aria-label="search" 
                onClick={()=>this.handle_submit('view')}
                >
          <SearchIcon />
        </IconButton>
        </Tooltip>
        <Tooltip title="Informe en PDF de cierres de caja">
        <IconButton aria-label="search" 
        onClick={()=>this.handle_submit('pdf')}
        >
          <PictureAsPdfIcon />
        </IconButton>
        </Tooltip>

                    {/* <Tooltip title="Informe de cierres de caja">
                      <Button
                        variant="outlined"
                        color="secondary"
                        startIcon={<InfoIcon />}
                        type="submit"
                        onClick={()=>{this.handle_submit()}}
                      >
                        {" "}
                      </Button>
                    </Tooltip> */}
                </TableCell>
                <TableCell>
              <Checkbox
                  color="primary"
                  inputProps={{
                    "aria-label": "secondary checkbox",
                  }}
                  // checked={this.state.checkedIdArray.includes(data.till_id)}
                  onClick={() => this.checkAll()}
                />
              </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {/* <Paginator
                           lenght={this.state.page_lenght}
                           perPageChange= {this.perPageChange}
                           pageChange ={this.pageChange}
                           page= {this.state.page}
                           /> */}
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredtills.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return(
        <getAuth.Consumer>
          {(context) => {
            return this.state.filteredtills.map((data) => {
              var last_date = new Date(`${data.updated_at}`);
              var last_date_format = last_date.toLocaleDateString("en-US", { day: '2-digit' })+ "-"+ last_date.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + last_date.toLocaleDateString("en-US", { year: 'numeric' })
              return (
                <TableBody key={data.till_id}>
                  <TableRow>
                    <TableCell>{data.till_name}</TableCell>
                    <TableCell>{data.till_starting == true ? ("Caja Abierta") : ("Caja Cerrada") }</TableCell>
                    <TableCell>{data.cajero_name}</TableCell>
                    <TableCell align="center">{last_date_format}</TableCell>

                    <TableCell>
                      {(permissions.tills_show === true && (this.state.mytills.till_id === data.till_id || permissions.manage_tills === true || ['Administrar', 'Ver', 'Pagar', 'Cobrar'].includes(data.myrol)) ) && (
                        <Link
                          className="navbar-brand"
                          to={"/cajas/show/" + data.till_id}
                        >
                          <Tooltip title="Mostrar">
                            <Button
                              variant="outlined"
                              color="primary"
                              startIcon={<VisibilityIcon />}
                              type="submit"
                              // onClick={() => {
                              //   this.clickEditar({ data: data });
                              // }}
                            >
                              {" "}
                            </Button>
                          </Tooltip>
                        </Link>
                      )}
                    </TableCell>

                    <TableCell>
                      {permissions.tills_update === true && (
                        <Tooltip title="Editar">
                          <Button
                            variant="outlined"
                            color="primary"
                            startIcon={<EditIcon />}
                            type="submit"
                            onClick={() => {
                              this.clickEditar({ data: data });
                            }}
                          >
                            {" "}
                          </Button>
                        </Tooltip>
                      )}
                    </TableCell>
                    <TableCell>
                      {permissions.tills_destroy === true && (
                        <Tooltip title="Eliminar">
                          <Button
                            variant="outlined"
                            color="secondary"
                            onClick={() => {
                              this.handleClickOpen({ data: data }),
                                this.setState({ till_id: data.till_id });
                            }}
                          >
                            <DeleteIcon />
                          </Button>
                        </Tooltip>
                      )}
                    </TableCell>
                    <TableCell>
                      {(this.state.mytills.till_id === data.till_id || permissions.manage_tills === true || ['Administrar', 'Ver', 'Pagar', 'Cobrar'].includes(data.myrol) ) &&
                        <Checkbox
                          color="primary"
                          inputProps={{
                            "aria-label": "secondary checkbox",
                          }}
                          checked={this.state.checkedIdArray.includes(data.till_id)}
                          onClick={() => this.onCheck(data.till_id)}
                        />
                      
                      }
                    </TableCell>
                  </TableRow>
                </TableBody>
              );
            });
          }}
        </getAuth.Consumer>
      );
      
    }
  }
}

export default TillsList;
