import ModalForm from "./ModalForm";
import React, { Component, lazy } from "react";
import DialogDestroy from "../Dialogs/DialogDestroy";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
  IconButton,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import {NumerosALetras} from 'numero-a-letras'
import { Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import PrintIcon from "@material-ui/icons/Print";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import TicketShow from "../Tickets/TicketShow";
import TillTransferShow from "../TillTransfers/TillTransferShow";
import TillDetailShow from "../TillsDetails/TillDetailShow";
// import ProviderAccountShow from "../ProviderAccount/ProviderAccountShow";
import EmployeeAccountShow from "../EmployeeAccount/EmployeeAccountShow";

class TillCloseReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      tills: [],
      till_id: 0,
      units: [],
      till: [],
      from_date: "",
      to_date: "",
      provider: false,
      employee: false,
      transfer: false,
      tickets: false,
      detail: false,
      new: false,
      open: false,
      search: "",
      incomes: [],
      expenses: [],
      payments: [],
      filteredtills: [],
      prevActiveStep: 1,
      rowsPerPage: 10,
      paginator: [],
      till_date: "",
      till_name: "",
      cashier: "-",
      cuotas: 0,
      extraordinario: 0,
      ingreso: 0,
      pago_docente: 0,
      pago_funcionario: 0,
      pago_proveedor: 0,
      parciales: 0,
      primera: 0,
      recuperatorio: 0,
      segunda: 0,
      tercera:0,
      transferenciaE: 0,
      total_ingreso: 0,
      total_egreso: 0,
      transferenciaIn: 0,
      order: "asc",
      orderBy: "till_name",
      total_income: 0,
      total_expense: 0,
      apertura: 0,
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: `/api/tillDetails-till/${this.props.match.params.id}?response=report&type=caja`,
      permissions: {
        tills_details_index: null,
        tills_details_show: null,
        tills_details_store: null,
        tills_details_update: null,
        tills_details_destroy: null,
      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order,
      route
    ) => {
      let res;
      if (route) {
        res = await axios.get(
          `${route}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
       console.log(data)

        this.setState({
          apertura: data.apertura,
          cuotas: data.cuotas,
          extraordinario: data.extraordinario,
          ingreso: data.ingreso,
          pago_docente: data.pago_docente,
          pago_funcionario: data.pago_funcionario,
          pago_proveedor: data.pago_proveedor,
          parciales: data.parciales,
          primera: data.primera,
          recuperatorio: data.recuperatorio,
          segunda: data.segunda,
          tercera: data.tercera,
          transferenciaE: data.transferenciaE,
          transferenciaIn: data.transferenciaIn,
          total_egreso: data.total_egreso,
          total_ingreso: data.total_ingreso,
          cashier: data.cajero,
          till_name: data.till_name,
          till_date: data.date,
          incomes: data.ingresos,
          expenses: data.egresos,
          // expenses: data.egresos,
          payments: data.paymentsIn,
          // paginator: data,
          // // filteredtills: data.data,
          // // page_lenght: res.data.last_page,
          // page: res.data.current_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        employee: false,
        provider: false,
        transfer: false,
        tickets: false,
        detail: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    // this.clickSortByName = this.clickSortByName.bind(this)
    // this.clickSortByCode = this.clickSortByCode.bind(this)
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);

    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, till: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };

    //fin constructor
  }

  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }

  componentDidMount() {
    console.log(this.props.match.params.id)
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "tills_details_index",
      "tills_details_show",
      "tills_details_store",
      "tills_details_update",
      "tills_details_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({
          permissions: res.data,
          from_date: this.getCurrentDate(),
          to_date: this.getCurrentDate(),
        });
        if (res.data !== false) {
          console.log('debe traer cosas')
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  //
  // clickSortByName(){
  //    this.setState({prevActiveStep: 1});
  //     this.getObject(this.state.rowsPerPage, 1);
  //  }
  //
  //  clickSortByCode(){
  //     this.setState({prevActiveStep: 1});
  //      this.getObject(this.state.rowsPerPage, 1);
  //   }

  exportToPDF(data) {
    if(data == true){
      open(`/reportes/caja?type=caja&response=report&till=${this.props.match.params.id}&report_complete=true`);
    }else{
      open(`/reportes/caja?type=caja&response=report&till=${this.props.match.params.id}&report_complete=false`);
    }
  }

  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredtills: this.state.tills.data,
            paginator: this.state.tills,
            page_lenght: this.state.tills.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/tills`,
          });
          this.updateState();
        } else {
          this.setState({
            url: `/api/tills?till_name=${e.target.value}`,
          });
          axios
            .get(`${this.state.url}`)
            .then((res) => {
              this.setState({
                filteredtills: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  deleteObject() {
    axios.delete(`api/tills/${this.state.till_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data.message });
      this.updateState();
    });
  }

  clickEditar(data) {
    var d = data.data.description;
    if (d.match(/Transferencia.*/)) {
      this.setState({
        transfer: true,
        till: data.data.ref_id,
      });
    } else if (d.match(/Ingreso.*/)) {
      this.setState({
        detail: true,
        till: data.data.dettills_id,
      });
    } else if (d.match(/aranceles.*/)) {
      this.setState({
        tickets: true,
        till: data.data.ref_id,
      });
    } else if (d.match(/Apertura.*/) || d.match(/Cierre.*/)) {
      this.setState({
        detail: true,
        till: data.data.dettills_id,
      });
    } else if (d.match(/proveedor.*/)) {
      this.setState({
        provider: true,
        till: data.data.ref_id,
      });
    } else if (d.match(/funcionario.*/)) {
      this.setState({
        employee: true,
        till: data.data.ref_id,
      });
    }
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  changeFields(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      filteredtills,
      till,
      orderBy,
      order,
      permissions,
    } = this.state;
    var cantidad_ingresos = this.state.incomes.filter((x)=> x.dettills_type == true).length 
    if (this.state.transfer) {
      // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TillTransferShow />,
            props_form: {
              onSuccess: this.openSnack,
              transfer: this.state.till,
            },
          }}
        </MidModal>
      );
    } else if (this.state.tickets) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TicketShow />,
            props_form: {
              ticket: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.detail) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TillDetailShow />,
            props_form: {
              details: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.employee) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <EmployeeAccountShow />,
            props_form: {
              account: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    } else if (this.state.provider) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <p></p>,
            props_form: {
              account: this.state.till,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={till.data.till_name}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filteredtills.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body">
        {/*<Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=today`
            );
          }}
        >
          De Hoy
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=week`
            );
          }}
        >
          De Esta semana
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=month`
            );
          }}
        >
          De Este mes
        </Button>{" "}
        <Button
          variant="contained"
          onClick={() => {
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?filter=false`
            );
          }}
        >
          Todo
        </Button>*/}

        {/* <TextField
          id="date"
          label="Desde"
          type="date"
          name="from_date"
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e) => {
            this.changeFields(e);
          }}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="date"
          label="Hasta"
          type="date"
          name="to_date"
          // defaultValue="2017-05-24"
          value={this.state.to_date}
          onChange={(e) => {
            this.changeFields(e);
          }}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <IconButton
          aria-label="search"
          onClick={() =>
            this.getObject(
              this.state.rowsPerPage,
              this.state.page,
              this.state.orderBy,
              this.state.order,
              `/api/tillDetails-till/${this.props.till}?from_date=${this.state.from_date}&to_date=${this.state.to_date}`
            )
          }
        >
          <SearchIcon />
        </IconButton> */}
        <Tooltip title="Imprimir completo">
 <IconButton
          aria-label="search"
          onClick={()=>{this.exportToPDF(true)}}
        >
          <PrintIcon />
        </IconButton> 
        </Tooltip>
        <Tooltip title="Imprimir resumen">
 <IconButton
          aria-label="search"
          onClick={()=>{this.exportToPDF(false)}}
        >
          <PrintIcon />
        </IconButton> 
        </Tooltip>
        <hr />
        <h1 align="center">Cierre de caja del {this.state.till_date}</h1>
        <h2 align="center">Caja: {this.state.till_name} {'     '}Usuario: {this.state.cashier}</h2>
       <br/>
       <br/>
       <h2>Ingresos</h2>
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "till_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "till_name"}
                    direction={orderBy === "till_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("till_name");
                    }}
                  >
                    <h3>Sede</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "tdtype_desc" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "tdtype_desc"}
                    direction={orderBy === "tdtype_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tdtype_desc");
                    }}
                  >
                    <h3>Alumno</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "unit_code" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "unit_code"}
                    direction={orderBy === "unit_code" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("unit_code");
                    }}
                  >
                    <h3>Factura</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "ticket_number" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "ticket_number"}
                    direction={orderBy === "ticket_number" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("ticket_number");
                    }}
                  >
                    <h3>Tipo</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "created_at" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "created_at"}
                    direction={orderBy === "created_at" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("created_at");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell
                  sortDirection={orderBy === "pay_type_desc" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "student_name"}
                    direction={orderBy === "student_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("student_name");
                    }}
                  >
                    <h3>Concepto</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "sems_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "student_name"}
                    direction={orderBy === "student_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("student_name");
                    }}
                  >
                    <h3>Concepto</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "sems_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "sems_name"}
                    direction={orderBy === "sems_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("sems_name");
                    }}
                  >
                    <h3>Curso</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "amount" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "amount"}
                    direction={orderBy === "amount" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("amount");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody >
            {
              this.state.incomes.map((data, index)=>(
                <TableRow>
                  <TableCell>{data.unit_code}</TableCell>
                  <TableCell>{data.student_name}</TableCell>
                  <TableCell>{data.ticket_number}</TableCell>
                  <TableCell>{data.dettills_type == true ? ('Ingreso'): ('Egreso')}</TableCell>
                  <TableCell>
                  {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(data.amount)}</TableCell>
                  <TableCell>{data.description}</TableCell>
                  <TableCell>{data.sems_name}</TableCell>
                  <TableCell>{this.state.cashier}</TableCell>
                  <TableCell>
                  {data.created_at}
                    </TableCell>
    
                  {/* <TableCell>
                    {permissions.tills_details_show === true && (
                      <Tooltip title="Mostrar">
                        <Button
                          variant="outlined"
                          color="primary"
                          startIcon={<VisibilityIcon />}
                          type="submit"
                          onClick={() => {
                            this.clickEditar({ data: data });
                          }}
                        >
                          {" "}
                        </Button>
                      </Tooltip>
                    )}
                  </TableCell> */}
    
                  {/*<TableCell>
                    {permissions.tills_details_update === true && (
                      <Tooltip title="Editar">
                        <Button
                          variant="outlined"
                          color="primary"
                          startIcon={<EditIcon />}
                          type="submit"
                          onClick={() => {
                            this.clickEditar({ data: data });
                          }}
                        >
                          {" "}
                        </Button>
                      </Tooltip>
                    )}
                  </TableCell>*/}
                  
                </TableRow>
              ))
            }
            
              </TableBody>

            {/* {this.renderList()} */}
          </Table>
        </TableContainer>
<hr/>
<br/>
<TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "till_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "till_name"}
                    direction={orderBy === "till_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("till_name");
                    }}
                  >
                    <h3>Origen de Pago</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "tdtype_desc" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "tdtype_desc"}
                    direction={orderBy === "tdtype_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tdtype_desc");
                    }}
                  >
                    <h3>Ingreso</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "unit_code" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "unit_code"}
                    direction={orderBy === "unit_code" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("unit_code");
                    }}
                  >
                    <h3>Cantidad</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "ticket_number" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "ticket_number"}
                    direction={orderBy === "ticket_number" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("ticket_number");
                    }}
                  >
                    <h3>%</h3>
                  </TableSortLabel>
                </TableCell>

               
                <TableCell colSpan="2"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody >
            {
              this.state.payments.map((data, index)=>(
                <TableRow>
                  <TableCell>{data.pay_type_desc}</TableCell>
                  <TableCell>
                  {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(data.amount_income)}
                    </TableCell>
                    <TableCell>
                  {data.qty_income}
                    </TableCell>
                    <TableCell>
                      {
                        (data.qty_income/cantidad_ingresos)*100
                      }
                  {/* {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(data.amount_income-data.amount_expenses)} */}
                    </TableCell>

                 
    
                  {/*<TableCell>
                    {permissions.tills_details_update === true && (
                      <Tooltip title="Editar">
                        <Button
                          variant="outlined"
                          color="primary"
                          startIcon={<EditIcon />}
                          type="submit"
                          onClick={() => {
                            this.clickEditar({ data: data });
                          }}
                        >
                          {" "}
                        </Button>
                      </Tooltip>
                    )}
                  </TableCell>*/}
                  
                </TableRow>
              ))
            }
           
              </TableBody>

            {/* {this.renderList()} */}
          </Table>
        </TableContainer>
<br/>
<h2>Egresos</h2>
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "till_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "till_name"}
                    direction={orderBy === "till_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("till_name");
                    }}
                  >
                    <h3>Caja</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell sortDirection={orderBy === "description" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "description"}
                    direction={orderBy === "description" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("description");
                    }}
                  >
                    <h3>Concepto</h3>
                  </TableSortLabel>
                </TableCell>

                

                

                <TableCell sortDirection={orderBy === "created_at" ? order : false}>
                  <TableSortLabel
                    active={orderBy === "created_at"}
                    direction={orderBy === "created_at" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("created_at");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell
                  sortDirection={orderBy === "pay_type_desc" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "pay_type_desc"}
                    direction={orderBy === "pay_type_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("pay_type_desc");
                    }}
                  >
                    <h3>Medio de pago</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "amount" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "amount"}
                    direction={orderBy === "amount" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("amount");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>

            {
              this.state.expenses.map((data, index)=>(
                <TableRow>
                  <TableCell>{data.till_name}</TableCell>
                  <TableCell>{data.description}</TableCell>
                  <TableCell>{data.created_at}</TableCell>
                  <TableCell>{data.pay_type_desc}</TableCell>
                  <TableCell>
                  {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(data.amount)}
                    </TableCell>
                 
    
                  <TableCell>
                    {permissions.tills_details_show === true && (
                      <Tooltip title="Mostrar">
                        <Button
                          variant="outlined"
                          color="primary"
                          startIcon={<VisibilityIcon />}
                          type="submit"
                          onClick={() => {
                            this.clickEditar({ data: data });
                          }}
                        >
                          {" "}
                        </Button>
                      </Tooltip>
                    )}
                  </TableCell>
    
                  {/*<TableCell>
                    {permissions.tills_details_update === true && (
                      <Tooltip title="Editar">
                        <Button
                          variant="outlined"
                          color="primary"
                          startIcon={<EditIcon />}
                          type="submit"
                          onClick={() => {
                            this.clickEditar({ data: data });
                          }}
                        >
                          {" "}
                        </Button>
                      </Tooltip>
                    )}
                  </TableCell>
                  <TableCell>
                    {permissions.tills_details_destroy === true && (
                      <Tooltip title="Eliminar">
                        <Button
                          variant="outlined"
                          color="secondary"
                          onClick={() => {
                            this.handleClickOpen({ data: data }),
                              this.setState({ till_id: data.till_id });
                          }}
                        >
                          <DeleteIcon />
                        </Button>
                      </Tooltip>
                    )}
                  </TableCell>*/}
                </TableRow>
              ))
            }
             <TableRow>
              <TableCell>
                Total
              </TableCell>
              <TableCell colspan="7">
              <p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.total_income)} </p>
                 <p> Guaranies. {NumerosALetras(this.state.total_expense).split("Pesos")[0]
                }</p>
              </TableCell>
            </TableRow>
              </TableBody>

            {/* {this.renderList()} */}
          </Table>
        </TableContainer>

<br/>

<tableContainer>
  <Table>
  <TableRow>
   <TableCell align="center">
    Ingresos 
     </TableCell> 
     <TableCell align="center">
    Egresos 
     </TableCell> 
  </TableRow>
  <TableRow>
    <TableCell>
      <Table>
        
        <TableRow>
          <TableCell>
            Ingreso de dinero
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.ingreso)} </p>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           Transferencia de caja
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.transferenciaIn)} </p>
          </TableCell>
          </TableRow>

          
        <TableRow>
          <TableCell>
            Cuotas
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.cuotas)} </p>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
            Parciales
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.parciales)} </p>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           1era Oportunidad
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.primera)} </p>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           2da Oportunidad
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.segunda)} </p>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           3era Oportunidad
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.tercera)} </p>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           Recuperatorio
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.recuperatorio)} </p>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           Extraordinario
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.extraordinario)} </p>
          </TableCell>
        </TableRow>
        <TableRow>

          <TableCell colspan="3">
            <h1>Total Ingreso:  {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.total_ingreso)} </h1>
            <h3>Guaranies: {NumerosALetras(this.state.total_ingreso).split("Pesos")[0]}</h3>
          </TableCell>
        </TableRow>
      </Table>
    </TableCell>
    <TableCell>
      <Table>
      <TableRow>
          <TableCell>
           Transferencia de Caja
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.transferenciaE)} </p></TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           Pagos a docentes
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.pago_docente)} </p></TableCell>
        </TableRow>
        <TableRow>
          <TableCell>
           Pagos a funcionarios
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.pago_funcionario)} </p></TableCell>
        </TableRow>
        
        <TableRow>
          <TableCell>
           Pagos a proveedores
          </TableCell>
          <TableCell class="td-line"></TableCell>
          <TableCell align="right"><p>{new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.pago_proveedor)} </p></TableCell>
        </TableRow>
        <TableRow>
        <TableCell colspan="3">
            <h1>Total Egreso: {this.state.total_egreso}</h1>
            <h3>Guaranies: {NumerosALetras(this.state.total_egreso).split("Pesos")[0]}</h3>
          </TableCell>
        </TableRow>
      </Table>
    </TableCell>
  </TableRow>
  <TableRow>
    <TableCell align="center" colSpan="2">
                      <h1>Ingreso: {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.total_ingreso)} </h1>
                      <h1>Egreso: {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.total_egreso)}</h1>
                      <h1>_______________________________________</h1>
                      <h1>Total: {new Intl.NumberFormat("es-PY", {
                        style: "currency",
                        currency: "PYG",
                      }).format(this.state.total_ingreso - this.state.total_egreso)}</h1>
                      <h1>Guaranies: {NumerosALetras(this.state.total_ingreso - this.state.total_egreso).split("Pesos")[0]}</h1>
    </TableCell>
  </TableRow>
  </Table>
</tableContainer>
        

        
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredtills.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredtills.map((data) => {
        return (
          <TableBody key={data.dettills_id}>
            <TableRow>
              <TableCell>{data.description}</TableCell>
              <TableCell>{data.pay_type_desc}</TableCell>
              <TableCell>{data.dettills_pr_pay_desc}</TableCell>
              <TableCell>{data.date}</TableCell>
              <TableCell>{data.amount}</TableCell>
              <TableCell>
                {data.dettills_type === true ? <p>Ingreso</p> : <p>Egreso</p>}
              </TableCell>

              {/* <TableCell>
                {permissions.tills_details_show === true && (
                  <Tooltip title="Mostrar">
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<VisibilityIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell> */}

              {/*<TableCell>
                {permissions.tills_details_update === true && (
                  <Tooltip title="Editar">
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<EditIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
              <TableCell>
                {permissions.tills_details_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ till_id: data.till_id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>*/}
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}

export default TillCloseReport;
