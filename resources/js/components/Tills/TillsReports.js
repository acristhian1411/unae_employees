import React, {Component} from 'react';
import { Grid } from "@material-ui/core";
import { BrowserRouter as Router, Link } from "react-router-dom";

import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import AssessmentIcon from '@material-ui/icons/Assessment';
import BarChartIcon from '@material-ui/icons/BarChart';

export default class TillsReports extends Component {
  constructor(props) {
    super(props);
    this.state ={

    }
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("INFORMES GENERALES");
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  render(){
    return(
      <div className="home-container">
        <div className="container-home">
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={0} style={{justifyContent:'center', verticalAlign: 'middle'}} >
              <Grid item xs={3} >
                <Link
                className="dashboard-link-button"
                  to={`/reportes/ingresos`}
                >
                <div className="dashboard-button">
                  <img className="dashoard-img-button" src={`img/dashboard/ingresos.jpeg`} />
                  <br />
                  <p>Ingresos</p>
                </div>
                </Link>
              </Grid>
              <Grid item xs={3} >
                <Link
                className="dashboard-link-button"
                  to={`/reportes/egresos`}
                >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/egresos.jpeg`} />
                    <br />
                    <p>Egresos</p>
                  </div>
                </Link>
              </Grid>
              <Grid item xs={3} >
                <Link
                className="dashboard-link-button"
                  to={`/reportes/balance`}
                >
                  <div className="dashboard-button">
                    <img className="dashoard-img-button" src={`img/dashboard/balance.jpeg`} />
                    <br />
                    <p>Balance</p>
                  </div>
                </Link>
              </Grid>
              <Grid item xs={3} >
                <Link
                className="dashboard-link-button"
                  to={`/reportes/inscripciones`}
                >
                <div className="dashboard-button">
                  <img className="dashoard-img-button" src={`img/dashboard/informe_inscripciones.jpeg`} />
                  <br />
                  <p>Inscripciones</p>
                </div>
                </Link>
              </Grid>
              <Grid item xs={3} >
                <Link
                className="dashboard-link-button"
                  to={`/reportes/demografico`}
                >
                <div className="dashboard-button">
                  <img className="dashoard-img-button" src={`img/dashboard/impuestos.jpeg`} />
                  <br />
                  <p>Demografico</p>
                </div>
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </div>

    );
  }
}
