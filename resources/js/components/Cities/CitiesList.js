import ModalForm from "./ModalForm";
import React, { Component, lazy } from "react";
import MidModal from "../Modals/MidModal";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { Link } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

class CitiesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      cities: [],
      city_id: 0,
      departaments: [],
      city: [],
      edit: false,
      new: false,
      open: false,
      search: "",
      filteredCities: [],
      rowsPerPage: 10,
      paginator: [],
      prevActiveStep: 1,
      order: "asc",
      orderBy: "city_name",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/cities",
      busqueda: false,
      permissions: {
        cities_index: null,
        cities_show: null,
        cities_store: null,
        cities_update: null,
        cities_destroy: null,
      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res;
      if (this._ismounted) {
        this.setState({
          cities: data,
          paginator: data,
          filteredCities: data.data.data,
          open: false,
          page_lenght: res.data.last_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.prevActiveStep);
    };
    //funciones
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.deleteObject = this.deleteObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.search = this.search.bind(this);
    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, city: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    // pagina siguiente
    this.handleNext = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep + 1 });
      var newPage = this.state.prevActiveStep + 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //pagina anterior
    this.handleBack = () => {
      this.setState({ prevActiveStep: this.state.prevActiveStep - 1 });
      var newPage = this.state.prevActiveStep - 1;
      this.getObject(this.state.rowsPerPage, newPage);
    };
    //filas por pagina
    this.handleChangeSelect = (event) => {
      this.setState({ rowsPerPage: event.target.value, prevActiveStep: 1 });
      this.getObject(event.target.value, 1);
    };
    //fin constructor
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    this.props.changeTitlePage("CIUDADES");
    var permissions = [
      "cities_index",
      "cities_show",
      "cities_store",
      "cities_update",
      "cities_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }

  searchChange(e) {
    this.setState({ search: e.value });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filteredCities: this.state.cities.data.data,
            paginator: this.state.cities,
            page_lenght: this.state.cities.data.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/cities`,
          });
          this.updateState();
        } else {
          // this.setState({
          //   url: `/api/cities/search/${e.target.value}`,
          // });
          axios
            .get(
              `/api/cities/search/${e.target.value}?sort_by=${this.state.orderBy}&order=${
                this.state.order
              }&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filteredCities: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }

  deleteObject() {
    axios.delete(`/api/cities/${this.state.city_id}`).then((res) => {
      this.setState({
        snack_open: true,
        message_success: res.data.message,
        prevActiveStep: 1,
      });
      this.updateState();
    });
  }

  clickEditar(data) {
    this.setState({
      edit: true,
      city: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(
      this.state.rowsPerPage,
      this.state.prevActiveStep,
      name,
      order
    );
  }

  render() {
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    const {
      snack_open,
      message_success,
      open,
      city,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.new) {
      // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (this.state.edit) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} city={this.state.city}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <ModalForm />,
            props_form: {
              edit: true,
              city: this.state.city,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={city.data.city_name}
          onClose={this.handleClose}
          onHandleAgree={this.deleteObject}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (this.state.filteredCities.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body" style={{textAlign: "center"}}>
          <TextField
            id="outlined-search"
            label="Escriba para buscar"
            type="search"
            name="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%", textAlign: "center"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "city_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "city_name"}
                    direction={orderBy === "city_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("city_name");
                    }}
                  >
                    <h3>Nombre</h3>
                  </TableSortLabel>
                </TableCell>

                <TableCell
                  sortDirection={orderBy === "depart_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "depart_name"}
                    direction={orderBy === "depart_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("depart_name");
                    }}
                  >
                    <h3>Departamento</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "country_name" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "country_name"}
                    direction={orderBy === "country_name" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("country_name");
                    }}
                  >
                    <h3>Pais</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2">
                  {permissions.cities_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        name="new_item"
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
        <br />
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const { permissions } = this.state;
    if (this.state.filteredCities.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredCities.map((data) => {
        return (
          <TableBody key={data.city_id}>
            <TableRow>
              <TableCell>{Capitalize(data.city_name)}</TableCell>
              <TableCell>{Capitalize(data.depart_name)}</TableCell>
              <TableCell>{Capitalize(data.country_name)}</TableCell>
              <TableCell>
                {permissions.cities_update === true && (
                  <Tooltip title="Editar">
                    <Button
                      name="edit_item"
                      variant="outlined"
                      color="primary"
                      startIcon={<EditIcon />}
                      type="submit"
                      onClick={() => {
                        this.clickEditar({ data: data });
                      }}
                    >
                      {" "}
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
              <TableCell>
                {permissions.cities_destroy === true && (
                  <Tooltip title="Eliminar">
                    <Button
                      name="delete_item"
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                          this.setState({ city_id: data.city_id });
                      }}
                    >
                      <DeleteIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}

export default CitiesList;
