import React, { Component } from "react";
import {
  Modal,
  Backdrop,
  InputLabel,
  TextField,
  Button,
  CircularProgress,
  Snackbar,
} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import Select from "react-select";
import { Autocomplete, Alert } from "@material-ui/lab";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

class ModalForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      open: true,
      setOpen: true,
      city_id: 0,
      city_name: "",
      depart_id: null,
      depart_name: "",
      edit: false,
      departments: [],
      countries: [],
      country_id: null,
      country_name: "",
      errors: [],
      time_out: false,
      validator: {
        city_name: {
          message: "",
          error: false,
        },
        depart_id: {
          message: "",
          error: false,
        },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onChangeField1 = this.onChangeField1.bind(this);
    this.onChangedSelect1 = this.onChangedSelect1.bind(this);
    this.onChangedSelect2 = this.onChangedSelect2.bind(this);
    this.onBlurSelect = this.onBlurSelect.bind(this);
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.handleUpdateObject = this.handleUpdateObject.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
  }

  componentDidMount() {
    if (this.props.edit) {
      var id = this.props.city.data.city_id;
      axios.get("/api/cities/" + id).then((response) => {
        this.setState({
          city_id: response.data.data.city_id,
          city_name: response.data.data.city_name,
          depart_id: response.data.data.depart_id,
          depart_name: response.data.data.depart_name,
        });
      });
      var c_id = this.props.city.data.depart_id;
      axios.get("/api/departments/" + c_id).then((response) => {
        this.setState({
          country_id: response.data.data.country_id,
          country_name: response.data.data.country_name,
        });
      });
    }
    axios.get("/api/departments").then((response) => {
      this.setState({
        departments: response.data.data,
      });
    });

    axios.get("/api/countries").then((response) => {
      this.setState({
        countries: response.data.data,
      });
    });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }

  selectReturn() {
    return this.state.departments.map((data) => ({
      label: data.depart_name,
      value: data.depart_id,
    }));
  }

  selectReturn2() {
    return this.state.countries.map((data) => ({
      label: data.country_name,
      value: data.country_id,
    }));
  }

  handleOpen() {
    this.setState({
      setOpen: true,
    });
  }

  handleClose() {
    this.setState({
      open: false,
    });
    this.props.onHandleSubmit();
  }

  onChangeField1(e) {
    this.setState({
      city_name: e.target.value,
    });
    this.props.changeAtribute(true);
  }

  //funcion para capturar inputs tipo int
  onChangedSelect1(e, values) {
    this.setState({ depart_id: values.value });
    this.setState({ depart_name: values.label });
    this.props.changeAtribute(true);
  }

  onChangedSelect2(e, values) {
    this.setState({ country_id: values.value });
    this.setState({ country_name: values.label });

    this.setState({ depart_name: "", depart_id: null });
    this.props.changeAtribute(true);
  }

  onBlurSelect(e) {
    var algo = this.state.country_id;
    axios.get(`/api/departCountry/${algo}`).then((response) => {
      this.setState({
        departments: response.data,
      });
    });
    this.setState({ depart_name: "", depart_id: null });
  }
  searchFieldChange(event) {
    var e = event;
    this.setState({ country_name: e.target.value, country_id: null });
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          axios.get("/api/countries").then((res) => {
            this.setState({ countries: res.data.data, time_out: false });
          });
        } else {
          axios
            .get(`/api/countries/search/${e.target.value}`)
            .then((res) => {
              this.setState({
                countries: res.data.data,
                time_out: false,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }
  handleCreateObject(e) {
    e.preventDefault();
    var { city_name, depart_id } = this.state;
    var validator = {
      city_name: { error: false, message: "" },
      depart_id: { error: false, message: "" },
    };
    var object_error = {};
    const object = {
      depart_id: this.state.depart_id,
      city_name: this.state.city_name.toUpperCase(),
    };
    const { history } = this.props;
    axios
      .post("/api/cities", {
        city_name: object.city_name,
        depart_id: object.depart_id,
      })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        // console.log('se guardo con exito');
        this.handleClose();
        // this.props.history.push('/cities');
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var { city_name, depart_id } = this.state;
    var validator = {
      city_name: { error: false, message: "" },
      depart_id: { error: false, message: "" },
    };
    var object_error = {};
    const object = {
      city_id: this.state.city_id,
      depart_id: this.state.depart_id,
      city_name: this.state.city_name.toUpperCase(),
    };
    const id = object.city_id;
    axios
      .put(`/api/cities/${id}`, {
        city_name: object.city_name,
        depart_id: object.depart_id,
      })
      .then((res) => {
        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        // console.log('se actualizo con exito');
        // this.props.history.push('/cities');
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  botonEdit() {
    if (this.props.edit) {
      return (
        <Button
          name="save"
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleUpdateObject}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          name="save"
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
      );
    }
  }

  render() {
    const { classes } = this.props;
    const { validator, snack_open } = this.state;
    var showSnack;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["city_name"]["error"] ? (
              <li>Error: {validator["city_name"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["depart_id"]["error"] ? (
              <li>Error: {validator["depart_id"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3>Formulario de Ciudades</h3>
        <hr />
        <form>
          <div className="form-group">
            <Autocomplete
              id="combo-box-demo"
              disableClearable
              options={[
                {
                  label: this.state.country_name,
                  value: this.state.country_id,
                },
                ...this.selectReturn2(),
              ]}
              getOptionLabel={(option) => option.label}
              onChange={this.onChangedSelect2}
              onBlur={this.onBlurSelect}
              value={{
                label: this.state.country_name,
                value: this.state.country_id,
              }}
              loading={this.state.time_out}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              filterSelectedOptions
              style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  name="country_name"
                  error={this.state.validator.depart_id.error}
                  label="Seleccione un Pais *"
                  variant="outlined"
                  fullWidth
                  // onChange={this.searchFieldChange}
                  onKeyDown={(e) =>{
                    if(e.key === 'Enter'){
                      e.preventDefault();
                      this.searchFieldChange(e)
                    }
                  }}
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {this.state.time_out === true ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
          </div>

          <div className="form-group">
            <Autocomplete
              id="combo-box-demo"
              disableClearable
              options={[
                { label: this.state.depart_name, value: this.state.depart_id },
                ...this.selectReturn(),
              ]}
              filterSelectedOptions
              getOptionLabel={(option) => option.label}
              onChange={this.onChangedSelect1}
              value={{
                label: this.state.depart_name,
                value: this.state.depart_id,
              }}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  name="depart_name"
                  error={this.state.validator.depart_id.error}
                  helperText={this.state.validator.depart_id.message}
                  label="Seleccione un Departamento *"
                  variant="outlined"
                  fullWidth
                />
              )}
            />
          </div>

          <div className="form-group">
            <TextField
              id="outlined-full-width"
              label="Nombre"
              name="city_name"
              error={this.state.validator.city_name.error}
              helperText={this.state.validator.city_name.message}
              className={`form-control ${
                this.hasErrorFor("city_name") ? "is-invalid" : ""
              }`}
              style={{ margin: 8 }}
              placeholder="Ejemplo: Encarnacion"
              value={this.state.city_name}
              onChange={this.onChangeField1}
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
              variant="outlined"
            />
            {this.renderErrorFor("city_name")}
          </div>
          {this.botonEdit()}
          {showSnack}

          <Button
            variant="outlined"
            color="secondary"
            startIcon={<ExitToAppIcon />}
            onClick={this.handleClose}
            onClick={this.handleClose}
          >
            Cancelar
          </Button>
        </form>
      </div>
    );
  }
}
// export default withRouter( withStyles(styles, { withTheme: true })(ModalForm));

export default ModalForm;
