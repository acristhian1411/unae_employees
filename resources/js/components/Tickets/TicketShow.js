/*
*desplegar la lista de tickets en una tabla, que muestre la persona,
                                    el numero de tickets, monto total
*/
import React, { Component } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Grid,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import InputMask from "react-input-mask";
import SaveIcon from "@material-ui/icons/Save";
import {format, disformat} from '../GlobalFunctions/Format';

export default class TicketShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tickets: [],
      ticketDetails: [],
      is_edit: false,
      url: `/api/tickets/${this.props.ticket}`,
      // url: `/api/tickets/9`,
      permissions: {
        ticket_index: null,
        ticket_show: null,
        ticket_store: null,
        ticket_update: null,
        ticket_destroy: null,
      },
    };

    this.getObject = async () => {
      let res;
      res = await axios.get(`${this.state.url}`);
      let data = await res.data;
      if (this._ismounted) {
        let is_edit;
        if(data.data.ticket_number === null
        ||data.data.ticket_number === "0"){
          is_edit =  true;
        }else{
          is_edit = false;
        }
        this.setState({
          tickets: data.data,
          ticketDetails: data.data.ticketDetails,
          is_edit
        });
      }
    };
  }

  componentDidMount() {
    this._ismounted = true;
    var permissions = [
      "ticket_index",
      "ticket_show",
      "ticket_store",
      "ticket_update",
      "ticket_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject();
        }
      });
  }

  componentWillUnmount() {
    this._ismounted = false;
  }
  handleUpdateObject() {
    axios.put(`/api/tickets/${this.state.tickets.ticket_id}`,
      this.state.tickets
    ).then((res)=>{
      // console.log("se actualizo con exito");
      this.props.onSuccess("se actualizo con exito");
      open(`/reportes/factura?type=ticket&id=${this.state.tickets.ticket_id}`);
      this.props.onHandleSubmit();
    });
  }
  render() {
    var {is_edit} = this.state;
    var today;
    if(is_edit){
      today = new Date();
    }
    return (
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={2}>
              <h2>Fecha:</h2>
            </Grid>
            <Grid item xs={4}>
              {is_edit?
                <h4>{
                  `${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()}`
                }
                </h4>
              :
              <h4>{this.state.tickets.ticket_date}</h4>
              }
            </Grid>
            <Grid item xs={2}>
              <h2>Numero de factura</h2>
            </Grid>
            <Grid item xs={4}>
            {is_edit?
              <InputMask
                 mask="999-999-9999999"
                 maskChar=" "
                 name="ticket_number"
                 disabled={false}
                 value={this.state.tickets.ticket_number}
                 onChange={(e) => {
                   var ticket = this.state.tickets;
                   ticket = {...ticket, ticket_number: e.target.value};
                   this.setState({ tickets: ticket });
                 }}
               >{() => (
                   <TextField
                     variant="outlined"
                     type="text"
                     label="N. de Factura"
                     type="ticket_number"
                     size="small"
                     className="input_small"
                     style={{width:'100%'}}
                   />
                 )}
               </InputMask>
            :
            <h4>
              {this.state.tickets.ticket_number === null
                ? "---"
                : this.state.tickets.ticket_number}
            </h4>
            }
            {is_edit &&
              <Button variant="outlined" color="primary" startIcon={<SaveIcon />} type='submit' onClick={()=>{this.handleUpdateObject()}}>Actualizar</Button>
            }
            </Grid>
          </Grid>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={2}>
              <h2>Razon social:</h2>
            </Grid>
            <Grid item xs={4}>
              <h4>{this.state.tickets.person_name}</h4>
            </Grid>
            <Grid item xs={2}>
              <h2>Ruc</h2>
            </Grid>
            <Grid item xs={4}>
              <h4>{this.state.tickets.person_idnumber}</h4>
            </Grid>
          </Grid>
        </Grid>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <h3>Cantidad</h3>
                </TableCell>
                <TableCell>
                  <h3>Descripcion</h3>
                </TableCell>
                <TableCell>
                  <h3>Exentas</h3>
                </TableCell>
                <TableCell>
                  <h3>Iva 5%</h3>
                </TableCell>
                <TableCell>
                  <h3>Iva 10%</h3>
                </TableCell>
                <TableCell>
                  <h3>Monto</h3>
                </TableCell>

                {/*<TableCell colSpan="2">
                  {this.state.permissions.ticket_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>*/}
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.ticketDetails.length === 0 ? (
                <p>No datos</p>
              ) : (
                this.state.tickets.ticketDetails.map((data, index) => (
                  <TableRow key={index}>
                    <TableCell>{data.tickdet_qty}</TableCell>
                    <TableCell>{data.tickdet_desciption}</TableCell>

                    <TableCell>
                      {data.tickdet_iva === "0" ? format(Math.round(data.tickdet_monto)) : 0}
                    </TableCell>
                    <TableCell>
                      {data.tickdet_iva === "5" ? format(Math.round(data.tickdet_monto)) : 0}
                    </TableCell>
                    <TableCell>
                      {data.tickdet_iva === "10" ? format(Math.round(data.tickdet_monto)) : 0}
                    </TableCell>
                    <TableCell>
                      {format(Math.round(data.tickdet_qty * data.tickdet_monto))}
                    </TableCell>
                  </TableRow>
                ))
              )}
              <TableRow>
                <TableCell rowSpan={3} />
                <TableCell>Subtotal</TableCell>
                <TableCell>{format(Math.round(this.state.tickets.exentas))}</TableCell>
                <TableCell>{format(Math.round(this.state.tickets.cinco))}</TableCell>
                <TableCell>
                  {this.state.tickets.diez === null
                    ? 0
                    : format(Math.round(this.state.tickets.diez))}
                </TableCell>
              </TableRow>

              <TableRow>
                <TableCell colSpan={4}>Total</TableCell>
                <TableCell>{format(Math.round(this.state.tickets.total))}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}
