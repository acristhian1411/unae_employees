/*
*desplegar la lista de tickets en una tabla, que muestre la persona,
                                    el numero de tickets, monto total
*/
import React, { Component } from "react";
// import ModalForm from "./ModalForm";
import MidModal from "../Modals/MidModal";
import DialogRollback from "../Dialogs/DialogRollback";
import TicketShow from "../Tickets/TicketShow";
import SearchIcon from "@material-ui/icons/Search";
import ClearIcon from "@material-ui/icons/Clear";
import InputMask from "react-input-mask";

import {
  Button,
  Grid,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import PrintIcon from '@material-ui/icons/Print'
// const ModalForm = lazy(()=> import('./ModalForm'));
import { Link } from "react-router-dom";
import RestoreIcon from '@material-ui/icons/Restore';
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import VisibilityIcon from "@material-ui/icons/Visibility";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import Paginator from "../Paginators/Paginator";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import { Autocomplete,Alert } from "@material-ui/lab";

export default class Tickets extends Component {
  constructor() {
    super();
    this.state = {
      snack_open: false,
      message_success: "",
      filter_person_time_out:false,
      tickets: [],
      ticket_id: 0,
      person_id: '',
      person_name: '',
      ticket_date: '',
      ticket_status: '',
      ticket_number: '',
      ticket: [],
      persons: [],
      edit: false,
      new: false,
      open: false,
      ticket_show: false,
      buscador: false,
      search: "",
      filteredtickets: [],
      rowsPerPage: 10,
      paginator: [],
      order: "desc",
      orderBy: "ticket_date",
      page_lenght: 0,
      page: 1,
      time_out: false,
      busqueda: false,
      url: "/api/tickets",
      permissions: {
        ticket_index: null,
        ticket_show: null,
        ticket_store: null,
        ticket_update: null,
        ticket_destroy: null,
      },
    };

    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;

      if (this.state.busqueda) {
        res = await axios.get(
          `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        res = await axios.get(
          `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      }
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          tickets: data,
          paginator: data,
          filteredtickets: data.data,
          open: false,
          page_lenght: res.data.last_page,
        });
      }
    };

    this.updateState = () => {
      this.setState({
        edit: false,
        new: false,
        open: false,
        ticket_show: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };
    //funciones
    this.clickAgregar = this.clickAgregar.bind(this);
    this.clickEditar = this.clickEditar.bind(this);
    this.showTicket = this.showTicket.bind(this);
    // this.clickSortByName = this.clickSortByName.bind(this)
    // this.clickSortByCode = this.clickSortByCode.bind(this)
    this.deleteObject = this.deleteObject.bind(this);
    this.rollbackTicket = this.rollbackTicket.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.fieldsChange = this.fieldsChange.bind(this);
    this.search = this.search.bind(this);
    // funciones para abrir Dialog
    this.handleClickOpen = (data) => {
      this.setState({ open: true, ticket: data });
    };

    this.handleClose = () => {
      this.setState({ open: false });
    };
    //fin constructor
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    this.props.changeTitlePage("FACTURAS");
    var permissions = [
      "ticket_index",
      "ticket_show",
      "ticket_store",
      "ticket_update",
      "ticket_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }

  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
      if(this.state.buscador != true){
        if (e.target.value === "") {
          this.setState({
            filteredtickets: this.state.tickets.data,
            paginator: this.state.tickets,
            page_lenght: this.state.tickets.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/tickets`,
          });
          this.updateState();
        } else {
          var rl = `/api/ticketsearch?search=${e.target.value}`
          this.setState({
            url: `/api/ticketsearch?search=${e.target.value}`,
          });
          axios
            .get(`${rl}&per_page=${this.state.rowsPerPage}`)
            .then((res) => {
              this.setState({
                filteredtickets: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              this.setState({time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
      }else{
        var rl = `/api/ticketsearch?advanced=true&filter[person_id]=${this.state.person_id}&filter[ticket_date]=${this.state.ticket_date}&filter[ticket_number]=${this.state.ticket_number}&filter[ticket_status]=${this.state.ticket_status}`
        
        this.setState({
          url: `/api/ticketsearch?filter[person_id]=${this.state.person_id}&filter[ticket_date]=${this.state.ticket_date}&filter[ticket_number]=${this.state.ticket_number}`,
        });
        axios
          .get(`${rl}&per_page=${this.state.rowsPerPage}`)
          .then((res) => {
            this.setState({
              filteredtickets: res.data.data,
              paginator: res.data,
              page_lenght: res.data.last_page,
              page: res.data.current_page,
              time_out: false,
              busqueda: true,
            });
          })
          .catch((error) => {
            this.setState({time_out: false});
            console.log("Ingrese un valor valido");
          });
      }
    //   }, 3000);
    // }
  }

  deleteObject() {
    axios.delete(`/api/tickets/${this.state.ticket_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data.message });
      this.updateState();
    });
  }
  rollbackTicket(){
    axios.put(`/api/ticket-rollback/${this.state.ticket.data.ticket_id}`).then(res=>{
      this.updateState();
    });
  }

  openTicketReport(id){
    open(`/reportes/factura?type=ticket&id=${id}`);
  }
  clickEditar(data) {
    this.setState({
      edit: true,
      ticket: data,
    });
  }
  showTicket(data) {
    // console.log(data);
    this.setState({
      ticket_show: true,
      ticket: data,
    });
  }

  clickAgregar() {
    this.setState({ new: true });
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }

  render() {
    var busquedaAV
    var busqueda
    var paginator;
    var showModal;
    var showDialogRollback;
    var showSnack;
    var showModal;
    const {
      snack_open,
      message_success,
      open,
      ticket,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.buscador == true) {
      busqueda = this.filter();
    } else {
      busqueda = (
        <TextField
          id="outlined-search"
          label="Escriba para buscar"
          type="search"
          variant="outlined"
          // onChange={this.search}
          onKeyDown={(e) =>{
            if(e.key === 'Enter'){
              this.search(e)
            }
          }}
          style={{ width: "40%"}}
          InputProps={{
            endAdornment: (
              <React.Fragment>
                {this.state.time_out === true ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
              </React.Fragment>
            ),
          }}
        />
      );
    }
    if (this.state.ticket_show) {
      // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} till={this.state.till}/>
      // console.log(this.state.ticket.data.ticket_id);
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <TicketShow />,
            props_form: {
              ticket: this.state.ticket.data.ticket_id,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    // if (this.state.new) {
    //   // showModal = <ModalForm edit={false} onHandleSubmit={this.updateState}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <ModalForm />,
    //         props_form: { onSuccess: this.openSnack },
    //       }}
    //     </MidModal>
    //   );
    // } else if (this.state.edit) {
    //   // showModal = <ModalForm edit={true} onHandleSubmit={this.updateState} ticket={this.state.ticket}/>
    //   showModal = (
    //     <MidModal>
    //       {{
    //         onHandleSubmit: this.updateState,
    //         form: <ModalForm />,
    //         props_form: {
    //           edit: true,
    //           ticket: this.state.ticket,
    //           onSuccess: this.openSnack,
    //         },
    //       }}
    //     </MidModal>
    //   );
    // }
    if (open) {
      //cambiar dialog destroy por dialog rollback
      showDialogRollback = (
        <DialogRollback
          index={ticket.data.ticket_number}
          onClose={this.handleClose}
          onHandleAgree={this.rollbackTicket}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (this.state.filteredtickets.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div className="card-body" style={{textAlign: "center"}}>
          <h2 align="center">
                 {busqueda}
                <Button
                  variant="outlined"
                  color="primary"
                  type="submit"
                  onClick={() => {
                    this.setState({ buscador: !this.state.buscador })
                  }}
                >
                  {this.state.buscador?"Salir de busqueda avanzada": "Busqueda avanzada"}
                </Button>
              </h2>

          {/* <TextField
            id="outlined-search"
            label="Escriba Nro. de Fact para buscar"
            type="search"
            variant="outlined"
            // onChange={this.search}
            onKeyDown={(e) =>{
              if(e.key === 'Enter'){
                this.search(e)
              }
            }}
            style={{ width: "40%"}}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          /> */}
        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={
                    orderBy === "ticket_number" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "ticket_number"}
                    direction={orderBy === "ticket_number" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("ticket_number");
                    }}
                  >
                    <h3>Numero de ticket</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={
                    orderBy === "person_id" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "person_id"}
                    direction={orderBy === "person_id" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_id");
                    }}
                  >
                    <h3>Persona</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={
                    orderBy === "ticket_date" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "ticket_date"}
                    direction={orderBy === "ticket_date" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("ticket_date");
                    }}
                  >
                    <h3>Fecha</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={
                    orderBy === "total" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "total"}
                    direction={orderBy === "total" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("total");
                    }}
                  >
                    <h3>Monto Total</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={
                    orderBy === "ticket_is_invoice" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "ticket_is_invoice"}
                    direction={orderBy === "ticket_is_invoice" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("ticket_is_invoice");
                    }}
                  >
                    <h3>Es factura</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell>
                    <h3>Estado</h3>
                </TableCell>
                <TableCell colSpan="2">
                  {permissions.ticket_store === true && (
                    <Tooltip title="Agregar">
                      <Button
                        variant="outlined"
                        color="primary"
                        startIcon={<AddBoxOutlinedIcon />}
                        type="submit"
                        onClick={this.clickAgregar}
                      >
                        {" "}
                      </Button>
                    </Tooltip>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>

        {paginator}
        {showModal}
        {showDialogRollback}
        {showSnack}
        <br />
      </div>
    );
  }

  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  fieldsChange(e) {
    e.preventDefault();
    console.log(e.target.name)
    console.log(e.target.value)
    this.setState({ [e.target.name]: e.target.value });
  }

  searchEmployees(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    this.setState({person_name: e.target.value})
    // console.log(e.target.value)
    // if (this.state.filter_person_time_out == false) {
      this.setState({ filter_person_time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            page: 1,
            filter_person_time_out: false,
          });
          // this.updateState();
        } else {

          axios
            .get(
              `/api/persons-search?search=${e.target.value}`
            )
            .then((res) => {
              if(res.data != false){
              this.setState({
                persons: res.data.data,
                filter_person_time_out: false,
              });
            }else{
              this.setState({
                persons: [],
                filter_person_time_out: false,
              });
            }
            })
            .catch((error) => {
              this.setState({filter_person_time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }
  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }

  selectsChange(e, values, nameValue, nameLabel) {
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    
  }


  Employees() {
    return this.state.persons.map((data) => ({
      label: data.person_fname+' '+data.person_lastname,
      value: data.person_id,
    }));
  }


  filter() {
    return (
      <div >
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={3}>
            <Autocomplete
                id="combo-box-demo"
                filterSelectedOptions
                options={[this.selectValue('person_name','person_id'), ...this.Employees()]}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) =>{
                  if(values === null){
                    // console.log("aplicar los cambios");
                    this.setState({person_name: '', person_id:''})
                   
                  }else{
                    this.selectsChange(e, values, "person_id", "person_name")
                  }
                }}
                value={this.selectValue("person_name", "person_id")}
                getOptionSelected={(option, value)=>option.value === value.value}
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Persona"
                    className="textField"
                    size="small"
                    variant="outlined"
                    // onChange={(e)=>{this.searchEmployees(e)}}
                    onKeyDown={(e) =>{
                      if(e.key === 'Enter'){
                        e.preventDefault();
                        this.searchEmployees(e)
                      }
                    }}
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.filter_person_time_out === true ? (
                            <CircularProgress color="inherit" size={20} />
                          ) : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Fecha"
                type="date"
                variant="outlined"
                size="small"
                name="ticket_date"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.ticket_date}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Numero de factura"
                variant="outlined"
                size="small"
                name="ticket_number"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.ticket_number}
              />
            </Grid>
            <Grid item xs={3}>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SearchIcon />}
                onClick={this.search.bind(this)}
              >
                Buscar
              </Button>
              <Tooltip title="Limpiar filtro">
                <Button
                  variant="contained"
                  color="secondary"
                  size="small"
                  startIcon={<ClearIcon />}
                  onClick={() => {
                    this.cleanfilter();
                  }}
                >
                  {" "}
                </Button>
              </Tooltip>
            </Grid>
          </Grid>
          <Grid container item xs={12} spacing={3}>
          <Grid item xs={3}>
          <Autocomplete
              id="combo-box-demo"
              disableClearable
              options={[
                {
                  value: "Generado",
                  label: "Generado",
                },
                {
                  value: "Pendiente",
                  label: "Pendiente",
                },
                {
                  value: "Anulado",
                  label: "Anulado",
                }
              ]}
              getOptionLabel={(option) => option.label}
              defaultValue={{
                value: "Generado",
                label: "Generado",
              }}
              onChange={(e, values) =>
                this.selectsChange(
                  e,
                  values,
                  "ticket_status",
                  "ticket_status"
                )
              }
              value={this.selectValue("ticket_status", "ticket_status")}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              size='small'
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Estado "
                  className="textField"
                  variant="outlined"
                />
              )}
            />
        </Grid>
        </Grid>
        </Grid>
      </div>
    );
  }
  cleanfilter() {
    this.setState({
      url: `/api/tickets`,
      busqueda: false,
      page: 1,
      person_fname: "",
      person_lastname: "",
      person_idnumber: "",
    });
    this.getObject(this.state.rowsPerPage, this.state.page);
  }
  renderList() {
    if (this.state.filteredtickets.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.filteredtickets.map((data, index) => {
        return (
          <TableBody key={data.ticket_id}>
            <TableRow>
              <TableCell>{
                data.ticket_status == 'Pendiente' ? (
                  <InputMask
                 mask="999-999-9999999"
                 maskChar=" "
                 name="ticket_number"
                 disabled={false}
                 value={data.ticket_number}
                     onChange={(e) => {
                      var tik = this.state.filteredtickets
                      tik[index].ticket_number = e.target.value
                      tik[index].modified = true
                      this.setState({filteredtickets:tik})
                     }}
               >{() => (
                   <TextField
                     variant="outlined"
                     type="text"
                     label={`N. de factura`}
                     className="input_small"
                     style={{width:'100%'}}
                   />
                 )}
               </InputMask>
                // <TextField
                //   id="outlined-search"
                //   label="N. de factura"
                //   variant="outlined"
                //   size="small"
                //   name="ticket_number"
                //   onChange={(e)=>{
                //     var tik = this.state.filteredtickets
                //     tik[index].ticket_number = e.target.value
                //     this.setState({filteredtickets:tik})
                //   }}
                //   value={data.ticket_number}
                // />
                ):(Capitalize(data.ticket_number))
              
              }</TableCell>
              <TableCell>{Capitalize(data.person_name)}</TableCell>
              <TableCell>{data.ticket_date}</TableCell>
              <TableCell>{data.total}</TableCell>
              <TableCell>{data.ticket_is_invoice == true ? ('Si'):('No')}</TableCell>
              <TableCell>{data.ticket_status}</TableCell>
              <TableCell>
                {this.state.permissions.ticket_show === true && (
                  //aqui tiene que abrir un modal en lugar de mandar a otro sitio

                  data.modified !== undefined ? (
                  <Tooltip title="Editar">
                  <Button
                    variant="outlined"
                    color="secondary"
                    startIcon={<SaveIcon />}
                    type="submit"
                    // onClick={() => {
                    //   this.showTicket({ data: data });
                    // }}
                  >
                    {" "}
                   </Button>
              </Tooltip>
              ):(
              <Tooltip title="Mostrar">
                  <Button
                    variant="outlined"
                    color="primary"
                    startIcon={<VisibilityIcon />}
                    type="submit"
                    onClick={() => {
                      this.showTicket({ data: data });
                    }}
                  >
                    {" "}
                   </Button>
              </Tooltip>
              )

                  
                )}
              </TableCell>
              <TableCell>
                {((this.state.permissions.ticket_destroy === true && data.ticket_status ==='Aceptado')|| (this.state.permissions.ticket_destroy === true && data.ticket_status ==='Generado')||(this.state.permissions.ticket_destroy === true && data.ticket_status ==='Pendiente') ) && (
                  //aqui tiene que abrir un modal para que el usuario confirme si se va hacer el rollback o no
                  <Tooltip title="Retroceder">
                    <Button
                      variant="outlined"
                      color="secondary"
                      onClick={() => {
                        this.handleClickOpen({ data: data }),
                        this.setState({ ticket_id: data.ticket_id });
                      }}
                    >
                      <RestoreIcon />
                    </Button>
                  </Tooltip>
                )}
              </TableCell>
              <TableCell>
              {(data.ticket_status ==='Aceptado' || data.ticket_status ==='Generado'|| data.ticket_status ==='Pendiente' ) && (

                  <Tooltip title="Imprimir">
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        this.openTicketReport(data.ticket_id)
                      }}
                    >
                      <PrintIcon />
                    </Button>
                  </Tooltip>
      )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
