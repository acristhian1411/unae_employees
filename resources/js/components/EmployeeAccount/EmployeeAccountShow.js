/*
*desplegar la lista de accounts en una tabla, que muestre la persona,
                                    el numero de accounts, monto total
*/
import React, { Component } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Grid,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import {format, disformat} from '../GlobalFunctions/Format';

export default class EmployeeAccountShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accounts: [],
      ticketDetails: [],
      url: `/api/employeeAccount/${this.props.account}`,
      // url: `/api/tillsaccounts/6`,
      permissions: {
        ticket_index: null,
        ticket_show: null,
        ticket_store: null,
        ticket_update: null,
        ticket_destroy: null,
      },
    };

    this.getObject = async () => {
      let res;
      res = await axios.get(`${this.state.url}`);
      let data = await res.data;
      if (this._ismounted) {
        this.setState({
          accounts: data.data,
          ticketDetails: data.data.ticketDetails,
        });
      }
    };
  }

  componentDidMount() {
    this._ismounted = true;
    var permissions = [
      "ticket_index",
      "ticket_show",
      "ticket_store",
      "ticket_update",
      "ticket_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject();
        }
      });
  }

  componentWillUnmount() {
    this._ismounted = false;
  }

  render() {
    return (
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2>Fecha: {this.state.accounts.emp_acc_datepaid}</h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={6}>
              <h2>Funcionario: {this.state.accounts.employee_name}</h2>
            </Grid>

            <Grid item xs={6}>
              <h2>Descripción: {this.state.accounts.emp_acc_desc}</h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2>Monto: {format(Math.round(parseFloat(this.state.accounts.emp_acc_amountpaid)+parseFloat(this.state.accounts.extra_hours)))}</h2>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}
