import React, { Component } from "react";
import { Grid, Checkbox, Button, TextField,Table, TableCell, TableHead, TableRow, TableBody, Snackbar } from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import { Autocomplete, Alert } from "@material-ui/lab";
import InputMask from "react-input-mask";
import {format, disformat} from '../GlobalFunctions/Format';
import EmployeeAccountPayAll from "./EmployeeAccountPayAll";
export default class EmployeeAccountForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      all_employees:false,
      snack_open: false,
      message_success: "",
      type: "",
      person_id: 0,
      to_pay: [],
      person_name: 0,
      emp_acc_id: 0,
      emplo_salary: 0,
      persons: [],
      employeeAccounts: [],
      emp_acc_status: null,
      disabled: false,
      emp_acc_desc: null,
      emp_acc_amount: 0,
      emp_acc_amountpaid: 0,
      emp_acc_amount_to_paid: 0,
      emp_acc_month: null,
      emp_acc_month2: null,
      emp_acc_ticket_number: "",
    };
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.updateState = () => {
      this.setState({
      snack_open: false,
      to_pay: [],
      message_success: "",
      });
      axios
      .get("/api/employeeAccounts/" + this.props.person_id)
      .then((response) => {
        this.setState({
          employeeAccounts: response.data.data,
        });
      });
    };
  }

  componentDidMount() {
    axios.get("/api/employees/" + this.props.person_id).then((res) => {
      this.setState({
        emplo_salary: res.data.data.emplo_salary,
        // emp_acc_amountpaid: res.data.data.emplo_salary,
        person_id: res.data.data.person_id,
        person_name:
          res.data.data.person_fname + " " + res.data.data.person_lastname,
        disabled: true,
      });
    });
    axios
      .get("/api/employeeAccounts/" + this.props.person_id)
      .then((response) => {
        this.setState({
          employeeAccounts: response.data.data,
        });
      });
  }

  componentDidUpdate(prevProps){
    if (this.props.person_id !== prevProps.person_id) {

      axios.get("/api/employees/" + this.props.person_id).then((res) => {
        this.setState({
          emplo_salary: res.data.data.emplo_salary,
          // emp_acc_amountpaid: res.data.data.emplo_salary,
          person_id: res.data.data.person_id,
          person_name:
            res.data.data.person_fname + " " + res.data.data.person_lastname,
          disabled: true,
        });
      });
      axios
        .get("/api/employeeAccounts/" + this.props.person_id)
        .then((response) => {
          this.setState({
            employeeAccounts: response.data.data,
          });
        });
    }
  }

  closeSnack() {
    this.setState({ snack_open: false });
  }

  onCheck(id, amount) {
    if (this.state.to_pay.includes(id)) {
      this.setState({
        to_pay: this.state.to_pay.filter(
          (val) => val !== id
        ),
        emp_acc_amountpaid: parseFloat(this.state.emp_acc_amountpaid) - parseFloat(amount)
      });
    } else {
      this.setState({
        to_pay: [...this.state.to_pay, id],
        emp_acc_amountpaid: parseFloat(this.state.emp_acc_amountpaid) + parseFloat(amount)
      });
    }
  }

  openSnack(param, type) {
    // console.log(param);
    if (type === false) {
      this.setState({
        snack_open: true,
        message_success: param,
        type: "error",
      });
    } else {
      this.setState({
        snack_open: true,
        message_success: param,
        type: "success",
      });
    }
  }
  handleCreateObject(e) {
    e.preventDefault();
    console.log('desde create primario')
    axios
      .put(`/api/employeeAccounts` , {
        person_id: this.state.person_id,
        to_pay: this.state.to_pay,
        till_id: this.props.till_id,
        emp_acc_ticket_number: this.state.emp_acc_ticket_number,
        emp_acc_amountpaid: this.state.emp_acc_amountpaid,
        emp_acc_amount: this.state.emp_acc_amount,
        emp_acc_amount_to_paid: this.state.emp_acc_amount_to_paid,
      })
      .then((res) => {
        this.openSnack(res.data.message, res.data.dato);
          this.setState({
            emp_acc_amountpaid: 0,
            emp_acc_ticket_number: "",
            emp_acc_desc: "",
            emp_acc_month: "",
            emp_acc_month2: "",
          });
          this.updateState()
        
      });
  }

  fieldsChange(e, name) {
    e.preventDefault();
    this.setState({ [name]: name == 'emp_acc_ticket_number' ? e.target.value : disformat(e.target.value) });
  }

  selectsChange(e, values, nameValue, nameLabel) {
    if (nameValue === "emp_acc_month") {
      this.setState({
        emp_acc_amountpaid: values.salary,
        emp_acc_id: values.emp_acc_id,
      });
    }
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    // this.props.changeAtribute(true);
  }

  selectReturn() {
    return this.state.persons.map((data) => ({
      label: data.person_fname + " " + data.person_lastname,
      value: data.person_id,
    }));
  }
  salarys() {
    return this.state.employeeAccounts.map((data) => ({
      label: data.emp_acc_desc,
      value: data.emp_acc_month,
      salary: data.emp_acc_amount,
      emp_acc_id: data.emp_acc_id,
    }));
  }
  render() {
    var d = new Date();
    var showSnack;

    if (this.state.snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={this.state.snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity={this.state.type}
          >
            {this.state.message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <Button variant="outlined" color='primary' onClick={(e)=>{this.setState({all_employees: !this.state.all_employees})}}>{this.state.all_employees == false ? ('Pago individual'):('Pago grupal')}</Button>
        {this.state.all_employees == false ? (
          <div>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={2}>
                <Grid item xs={6}>
                  <h3 aling="center">{this.state.person_name}</h3>
                </Grid>
                <Grid item xs={6}>
                  <InputMask
                    mask="999-999-9999999"
                    maskChar=" "
                    name="emp_acc_ticket_number"
                    disabled={false}
                    value={this.state.emp_acc_ticket_number}
                    onChange={(event) => {
                      this.fieldsChange(event, "emp_acc_ticket_number");
                    }}
                  >
                    {() => (
                      <TextField
                        variant="outlined"
                        type="text"
                        label="N. de Recibo"
                        fullWidth
                        style={{ margin: 8 }}
                      />
                    )}
                  </InputMask>
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={2}>
                <Grid item xs={6}>
                  <TextField
                    id="outlined-basic"
                    label="Monto"
                    className="textField"
                    type="number"
                    fullWidth
                    disabled
                    style={{ margin: 8 }}
                    value={this.state.emp_acc_amountpaid}
                    name="emp_acc_amountpaid"
                    onChange={(event) => {
                      this.fieldsChange(event, "emp_acc_amount");
                    }}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    id="outlined-basic"
                    label="A Pagar"
                    className="textField"
                    fullWidth
                    style={{ margin: 8 }}
                    value={format(this.state.emp_acc_amount_to_paid)}
                    name="emp_acc_amount_to_paid"
                    onChange={(event) => {
                      this.fieldsChange(event, "emp_acc_amount_to_paid");
                      }}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
            </Grid>
            <Table>
              <TableHead>
                  <TableRow>
                      <TableCell>Periodo</TableCell>
                      <TableCell>Monto</TableCell>
                     <TableCell>Saldo</TableCell>
                  </TableRow>
              </TableHead>
              <TableBody>
                {
                  this.state.employeeAccounts.map((data, index)=>(
                    <TableRow>
                      <TableCell>{data.emp_acc_month}</TableCell>
                      <TableCell>{format(data.emp_acc_amount)}</TableCell>
                      <TableCell>{format(data.emp_acc_amount - data.emp_acc_amountpaid)}</TableCell>
                      <TableCell>
                        <Checkbox
                          key={index}
                          color="primary"
                          inputProps={{
                              "aria-label": "secondary checkbox",
                          }}
                          name='profeacc_faculty_aprov'
                          checked={this.state.to_pay.includes(data.emp_acc_id)}
                          onClick={(event) => {
                            var value = parseFloat(data.emp_acc_amount) - parseFloat(data.emp_acc_amountpaid)  
                            this.onCheck( data.emp_acc_id, value);
                            this.setState({emp_acc_id: data.emp_acc_id, emp_acc_amount: data.emp_acc_amount})
                          }}
                        />
                      </TableCell> 
                    </TableRow>
                  ))
                }
              </TableBody>
            </Table>
            <Button
              variant="outlined"
              color="primary"
              onClick={this.handleCreateObject}
            >
              Guardar
            </Button>
          </div>
         ):(
            <div>
              <EmployeeAccountPayAll 
                till_id={this.props.till_id}
              />
            </div>
           )
        }
        {showSnack}
      </div>
    );
  }
}
