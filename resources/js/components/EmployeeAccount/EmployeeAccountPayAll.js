import React, { Component } from "react";
import { Grid, Checkbox, Button, TextField,Table, TableCell, TableHead, TableRow, TableBody, Snackbar, CircularProgress } from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import { Autocomplete, Alert } from "@material-ui/lab";
import InputMask from "react-input-mask";
import {format, disformat} from '../GlobalFunctions/Format';
import {Capitalize} from '../GlobalFunctions/LetterCase';
import { CheckBox } from "@material-ui/icons";

export default class EmployeeAccountPayAll extends Component {
  constructor(props) {
    super(props);
    var d = new Date().getUTCFullYear();
    this.state = {
      all_employees:false,
      month_value:'',
      month_label:"Enero",
      year_to_filter:d,
      comp_det_id: 0,
      comp_det_bussiness_name: "",
      selected_all_employees: false,
      filter_all_employees: false,
      companyDetails:[],
      send_payment: false,
      paymentTypes:[],
      pay_type_desc:'',
      pay_type_id:0,
      employees:[],
      snack_open: false,
      time_out_till:false,
      disabled_till:false,
      message_success: "",
      type: "",
      till_grid:6,
      person_id: 0,
      till_id: "",
      till_type: "",
      till_name: "",
      tills:[],      
      to_pay: [],
      person_name: 0,
      emp_acc_id: 0,
      emplo_salary: 0,
      persons: [],
      employeeAccounts: [],
      emp_acc_status: null,
      disabled: false,
      show_file: false,
      can_close:false,
      emp_acc_desc: null,
      emp_acc_amount: 0,
      emp_acc_amountpaid: 0,
      emp_acc_amount_to_paid: 0,
      emp_acc_month: null,
      emp_acc_month2: null,
      emp_acc_ticket_number: "",
      errors:{
        pay_type_id:{
          message:'',
          error:false,
        },
        comp_det_id:{
          message:'',
          error:false,
        },
        profeacc_amountpaid:{
          message:'',
          error:false,
        }
      },
    };
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    
    this.getObject = (month=this.state.month_value,till_id=this.state.till_id,filter) =>{
      axios.get(`/api/employeeAccounts?year=${this.state.year_to_filter}&
      month=${month}&till_id=${till_id}&
      filter_all=${filter}&per_page=100` ).then((res) => {
        this.setState({
          employees: res.data.data
          // emplo_salary: res.data.data.emp_acc_amount,
          // person_id: res.data.data.person_id,
          // person_name:
          //   res.data.data.person_fname + " " + res.data.data.person_lastname,
          // disabled: true,
        });
      });
    }
    
    this.updateState = () => {
      // this.setState({
      // snack_open: false,
      // message_success: "",
      // });
      this.getObject(this.state.month_value,this.state.till_id,this.state.filter_all_employees);
      // axios
      // .get("/api/employeeAccounts/" + this.props.person_id)
      // .then((response) => {
      //   this.setState({
      //     employeeAccounts: response.data.data,
      //   });
      // });
    };
  }

  componentDidMount() {
    axios.get("/api/companyDetails" ).then((res) => {
      this.setState({
        companyDetails:res.data.data
      });
    });
    axios
    .get("/api/paymentTypes" )
    .then((response) => {
      this.setState({
        paymentTypes: response.data.data,
      });
    });
  
    if(this.props.till_id){
      axios
      .get("/api/tills/"+this.props.till_id )
      .then((response) => {
        this.setState({
          till_name: response.data.data.till_name,
          till_id: this.props.till_id,
          till_type: response.data.data.till_type,
          till_grid:4
        });
      });
    }
    // axios
    //   .get("/api/employeeAccounts/" + this.props.person_id)
    //   .then((response) => {
    //     this.setState({
    //       employeeAccounts: response.data.data,
    //     });
    //   });
  }

 

  closeSnack() {
    this.setState({ snack_open: false });
  }

  SearchTill(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    this.setState({till_name: e.target.value})
    // if (this.state.time_out_till == false) {
      this.setState({ time_out_till: true });

      // setTimeout(() => {
        var url = ''
        if (e.target.value === "") {
          url = `/api/tills`

          this.setState({
            time_out_till: false,
            url: url,
          });
          axios.get(`${url}`).then((res) => {
            this.setState({
              tills: res.data.data,
              time_out_till: false,
            });
          }).catch((error) => {
            this.setState({time_out_till: false });
            console.log("Ingrese un valor valido");
          });

          } else {
          url = `/api/tills-search?till_type=3&till_name=${e.target.value}`
          this.setState({
            url: url,
          });
          axios.get(`${url}`).then((res) => {
              this.setState({
                tills: res.data.data,
                time_out_till: false,
              });
            }).catch((error) => {
          till[index].time_out_till = false;
              this.setState({time_out_till: false,
              });
              console.log("Ingrese un valor valido");
            });
        }
      // }, 3000);
    // }
  }

  Tills() {
    return this.state.tills.map((data) => ({
      label: data.till_name,
      value: data.till_id,
      till_type: data.till_type,
    }));
  }
  Company() {
    return this.state.companyDetails.map((data) => ({
      label: data.comp_det_bussiness_name,
      value: data.comp_det_id,
      comp_det_account_number: data.comp_det_account_number,
    }));
  }
  payments() {
    return this.state.paymentTypes.map((data) => ({
      label: data.pay_type_desc ,
      value: data.pay_type_id,
    }));
  }
  onCheck(id, amount) {
    if (this.state.to_pay.includes(id)) {
      this.setState({
        to_pay: this.state.to_pay.filter(
          (val) => val !== id
        ),
        emp_acc_amountpaid: parseFloat(this.state.emp_acc_amountpaid) - parseFloat(amount)
      });
    } else {
      var sel = this.state.employees.filter((x)=> x.emp_acc_id == id)
      if(this.state.till_id == sel[0].till_id || (this.state.till_type !== 3 && sel[0].till_id === null)){
        this.setState({
          to_pay: [...this.state.to_pay, id],
          emp_acc_amountpaid: parseFloat(this.state.emp_acc_amountpaid) + parseFloat(amount)
        });
      }
      

    }
  }

  openSnack(param, type) {
    if (type === false) {
      this.setState({
        snack_open: true,
        message_success: param,
        type: "error",
      });
    } else {
      this.setState({
        snack_open: true,
        message_success: param,
        type: "success",
      });
    }
  }
  handleCreateObject(e) {
    e.preventDefault();
    var validator =  {
      person_id:{
        message:'',
        error:false,
      },
      pay_type_id:{
        message:'',
        error:false,
      },
      comp_det_id:{
        message:'',
        error:false,
      },
      provacc_amountpaid:{
        message:'',
        error:false,
      },
      provacc_description:{
        message:'',
        error:false
      }
    } ;
    var object_error = {};
    this.setState({send_payment: true});
    var topay_data = this.state.employees.filter((x, tpindex)=>(this.state.to_pay.includes(x.emp_acc_id)));
    var till = this.state.tills.filter((x)=>x.till_id == this.state.till_id)
    axios
      .put("/api/employeeAccounts-masive" , {
        to_pay: topay_data,
        till_id: this.state.till_id,
        till: till,
        emp_acc_amountpaid: this.state.emp_acc_amountpaid,
        emp_acc_amount: this.state.emp_acc_amount,
        pay_type_id: this.state.pay_type_id,
        comp_det_id: this.state.comp_det_id,
        year: this.state.year_to_filter
        // emp_acc_amount_to_paid: this.state.emp_acc_amount_to_paid,
      })
      .then((res) => {
        this.openSnack(res.data.message, res.data.dato);
          this.setState({
            emp_acc_amountpaid: 0,
            emp_acc_ticket_number: "",
            emp_acc_desc: "",
            emp_acc_month: "",
            emp_acc_month2: "",
            show_file: res.data.show_file,
            // snack_open: true,
            // message_success: "Se guardo correctamente"
          });
          this.updateState();
          this.setState({send_payment: false});

      }).catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        // this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          errors: {
            ...validator,
            ...object_error,
          },
          send_payment:false
        });
      });
  }

  fieldsChange(e, name) {
    e.preventDefault();
    this.setState({ [name]: e.target.value });
  }

  selectsChange(e, values, nameValue, nameLabel) {
    if (nameValue === "emp_acc_month") {
      this.setState({
        emp_acc_amountpaid: values.salary,
        emp_acc_id: values.emp_acc_id,
      });
    }else if(nameValue == 'till_id'){
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label, to_pay:[]  });
    this.getObject(this.state.month_value,values.value,this.state.filter_all_employees)
    }
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    // this.props.changeAtribute(true);
  }

  selectReturn() {
    return this.state.persons.map((data) => ({
      label: data.person_fname + " " + data.person_lastname,
      value: data.person_id,
    }));
  }
  onCheckFilter(e){
    this.setState({filter_all_employees:e.target.checked})
    this.state.month_value != '' && this.getObject(this.state.month_value,this.state.till_id,e.target.checked)
  }
  salarys() {
    return this.state.employeeAccounts.map((data) => ({
      label: data.emp_acc_desc,
      value: data.emp_acc_month,
      salary: data.emp_acc_amount,
      emp_acc_id: data.emp_acc_id,
    }));
  }

  selectValue(name, label) {
    return {
      label: this.state[label],
      value: this.state[name],
    };
  }
  render() {
    var d = new Date();
    var showSnack;
    if (this.state.snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={this.state.snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity={this.state.type}
          >
            {this.state.message_success}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={2}>
            <Grid item xs={this.state.till_grid}>
              <TextField
                type="number"
                label="Año"
                value={this.state.year_to_filter}
                onChange={(event) => {this.setState({ year_to_filter: event.target.value })}}
              />
              <Checkbox 
                color="primary"
              inputProps={{
                  "aria-label": "secondary checkbox",
              }}
                checked={this.state.filter_all_employees}
                onClick={(e)=>{this.onCheckFilter(e)}}

              />
              {this.state.filter_all_employees == true ? ('Todos') : ('Solo de esta caja')}
            </Grid>
            {
              this.props.till_id != undefined && this.state.till_name != '' &&(
                <Grid item xs={this.state.till_grid}>
                <Autocomplete
                        id="combo-box-demo"
                        disableClearable
                        disabled={this.state.disabled_till}
                        options={this.Tills()}
                        getOptionLabel={(option) => option.label}
                        onChange={(e, values) =>
                          this.selectsChange(e, values, "till_id", "till_name")
                        }
                        value={this.selectValue( "till_id","till_name")}
                        getOptionSelected={(option, value) => {
                          if (value === option.value) {
                            return option.label;
                          } else {
                            return false;
                          }
                        }}
                        style={{ margin: 8 }}
                        size='small'
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label="Caja *"
                            onKeyDown={(e) =>{
                              if(e.key === 'Enter'){
                                e.preventDefault();
                                this.SearchTill(e)
                              }
                            }}
                            className="textField"
                            variant="outlined"
                            InputProps={{
                              ...params.InputProps,
                              endAdornment: (
                                <React.Fragment>
                                  {this.state.time_out_till === true ? (
                                    <CircularProgress color="inherit" size={20} />
                                  ) : null}
                                  {params.InputProps.endAdornment}
                                </React.Fragment>
                              ),
                            }}
                          />
                        )}
                      />
              </Grid>
              )
            }
            
            <Grid item xs={this.state.till_grid}>
              <Autocomplete
                id="combo-box-demo"
                disableClearable
                options={[
                  {
                    value: '01',
                    label: "Enero",
                  },
                  {
                    value: '02',
                    label: "Febrero",
                  },
                  {
                    value: '03',
                    label: "Marzo",
                  },
                  {
                    value: '04',
                    label: "Abril",
                  },
                  {
                    value: '05',
                    label: "Mayo",
                  },
                  {
                    value: '06',
                    label: "Junio",
                  },
                  {
                    value: '07',
                    label: "Julio",
                  },
                  {
                    value: '08',
                    label: "Agosto",
                  },
                  {
                    value: '09',
                    label: "Septiembre",
                  },
                  {
                    value: '10',
                    label: "Octubre",
                  },
                  {
                    value: '11',
                    label: "Noviembre",
                  },
                  {
                    value: '12',
                    label: "Diciembre",
                  }

                ]}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) =>{
                  this.selectsChange(
                    e,
                    values,
                    "month_value",
                    "month_label"
                  ),
                  this.getObject(values.value,this.state.till_id,this.state.filter_all_employees)
                }
                }
                value={this.selectValue("month_value", "month_label")}
                getOptionSelected={(option, value) =>
                  option.value === value.value
                }
                size='small'
                
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Meses"
                    className="textField"
                    variant="outlined"
                  />
                )}
              />
            </Grid>
         
          </Grid>
        </Grid>      
        <br/>
        {this.props.till_id != undefined && this.state.till_name != '' && this.state.till_type == 3 &&(
          <label>
            <span style={{color: 'red'}}>Se están filtrando solo funcionarios de {this.state.till_name}</span></label>
        )}
        
        <table className="employees_pay_multiple">
          <thead>
              <tr>
                <th> 
                  <Checkbox
                    color="primary"
                    inputProps={{
                        "aria-label": "secondary checkbox",
                    }}
                    name='profeacc_faculty_aprov'
                    checked={this.state.selected_all_employees}
                    onClick={(e) => {
                      if (!this.state.selected_all_employees) {
                        var employees = this.state.employees;
                        var len = employees.length;
                        var total = 0;
                        for (var i = 0; i < len; i++) {
                          var to_pay =this.state.to_pay;
                          if (!employees[i].emp_acc_status && (employees[i].till_id == this.state.till_id || (this.state.till_type !== 3 && employees[i].till_id === null))) {
                            var amount = 0
                            if(employees[i].has_ips){
                              amount = (parseFloat(employees[i].emp_acc_amount)-((parseFloat(employees[i].porcent_ips)*parseFloat(employees[i].emp_acc_amount))/100)) - parseFloat(employees[i].emp_acc_amountpaid) - parseFloat(employees[i].amount_loan === null?'0':`${employees[i].amount_loan}`) +parseFloat(employees[i].extra_hours);
                            }else{
                              amount = (parseFloat(employees[i].emp_acc_amount) - parseFloat(employees[i].emp_acc_amountpaid) - parseFloat(employees[i].amount_loan === null?'0':`${employees[i].amount_loan}`)) +parseFloat(employees[i].extra_hours);
                            }
                            to_pay.push(employees[i].emp_acc_id);
                            total += amount;
                          }
                        }
                        this.setState({selected_all_employees:true, to_pay: to_pay, emp_acc_amountpaid:total});
                      }else{
                        this.setState({selected_all_employees:false, to_pay: [], emp_acc_amountpaid:0});
                      } 
                      
                    }}
                  />
                </th>
                <th>Documento</th>
                <th>Apellidos</th>
                <th>Nombres</th>
                <th className="border-sides">Sueldo</th>
                <th className="border-sides">Horas extras</th>
                <th className="border-sides">Pago Anterior</th>
                <th className="border-sides">Cuotas</th>
                <th className="border-sides">IPS</th>
                <th className="border-sides">Total a pagar</th>
                <th className="border-sides">Estado</th>
              </tr>
          </thead>
          <tbody>
            {
              this.state.employees.map((data, index)=>(
                  <tr key={index}>
                    <td>
                    {/* this.state.till_id == sel[0].till_id || (this.state.till_type !== 3 && sel[0].till_id === null) */}
                      {!data.emp_acc_status&& (this.state.till_id == data.till_id || (this.state.till_type !== 3 && data.till_id === null)) &&
                        <Checkbox
                        key={index}
                        color="primary"
                        inputProps={{
                            "aria-label": "secondary checkbox",
                        }}
                        name='profeacc_faculty_aprov'
                          checked={this.state.to_pay.includes(data.emp_acc_id)}
                          onClick={(event) => {
                            if(data.has_ips){
                              var value = (parseFloat(data.emp_acc_amount)-((parseFloat(data.porcent_ips)*parseFloat(data.emp_acc_amount))/100)) - parseFloat(data.emp_acc_amountpaid) - parseFloat(data.amount_loan === null?'0':`${data.amount_loan}`) +parseFloat(data.extra_hours);
                            }else{
                              var value = (parseFloat(data.emp_acc_amount) - parseFloat(data.emp_acc_amountpaid) - parseFloat(data.amount_loan === null?'0':`${data.amount_loan}`)) +parseFloat(data.extra_hours);
                            }                              
                            this.onCheck( data.emp_acc_id, value);
                            this.setState({emp_acc_id: data.emp_acc_id, emp_acc_amount: data.emp_acc_amount})
                          }}
                      />
                      }
                    </td> 
                    <td align="center">{data.person_idnumber}</td>
                    <td align="center">{Capitalize(data.person_lastname)}</td>
                    <td align="center">{Capitalize(data.person_fname)}</td>
                    <td className="border-sides" align="right">{format(data.emp_acc_amount)}</td>
                    <td className="border-sides" align="right">
                      <span className="green">
                      +{data.emp_acc_status?
                        format(data.extra_hours)
                        :
                        <TextField
                          label="Horas extras"
                          id="outlined-full-width"
                          variant="outlined"
                          type="text"
                          style={{ width: 120 }}
                          value={format(data.extra_hours)}
                          size="small"
                          onChange={(e)=>{
                            var emplo = this.state.employees;
                            if (e.target.value == '') {
                              emplo[index].extra_hours = parseFloat(0);
                            }else{
                              emplo[index].extra_hours = parseFloat(disformat(e.target.value));  
                            }
                            this.setState({employees: emplo})
                            if (this.state.to_pay.includes(data.emp_acc_id)) {
                              var employees = this.state.employees.filter((x, tpindex)=>(this.state.to_pay.includes(x.emp_acc_id)));
                              var len = employees.length;
                              var total = 0;
                              for (var i = 0; i < len; i++) {
                                if (!employees[i].emp_acc_status) {
                                  console.log("verdadero: i ="+i);
                                  var amount = 0
                                  if(employees[i].has_ips){
                                    amount = (parseFloat(employees[i].emp_acc_amount)-((parseFloat(employees[i].porcent_ips)*parseFloat(employees[i].emp_acc_amount))/100)) - parseFloat(employees[i].emp_acc_amountpaid) - parseFloat(employees[i].amount_loan === null?'0':`${employees[i].amount_loan}`) +parseFloat(employees[i].extra_hours);
                                  }else{
                                    amount = (parseFloat(employees[i].emp_acc_amount) - parseFloat(employees[i].emp_acc_amountpaid) - parseFloat(employees[i].amount_loan === null?'0':`${employees[i].amount_loan}`)) +parseFloat(employees[i].extra_hours);
                                  }
                                  total += amount;
                                }
                              }
                              this.setState({emp_acc_amountpaid:total});
                            }

                            
                          }}
                          margin="normal"
                        />
                      }
                      </span>
                    </td>
                    <td className="border-sides" align="right">
                      <span className="red">
                        {data.emp_acc_amountpaid>0&&
                          '-'+format(Math.round(data.emp_acc_amountpaid))
                        }
                      </span>
                    </td>
                    <td className="border-sides red" align="right">
                      <span className="red">
                        {data.amount_loan !== null&&
                          '-'+format(data.amount_loan)
                        }
                      </span>
                    </td>
                    <td className="border-sides red" align="right">
                      <span className="red">
                        {
                          data.has_ips?
                            '-'+format(Math.round((parseFloat(data.porcent_ips)*data.emp_acc_amount)/100))
                          :0
                        }
                      </span>
                    </td>
                    {/* <td className="border-sides">{data.emp_acc_amount}</td> */}
                    <td className="border-sides" align="right">{
                      data.emp_acc_status?
                      0
                      :
                      data.has_ips?
                        format(Math.round((parseFloat(data.emp_acc_amount)-((parseFloat(data.porcent_ips)*parseFloat(data.emp_acc_amount))/100)) - parseFloat(data.emp_acc_amountpaid) - parseFloat(data.amount_loan === null?'0':`${data.amount_loan}`)+ parseFloat(data.extra_hours)))
                        :format(Math.round((parseFloat(data.emp_acc_amount) - parseFloat(data.emp_acc_amountpaid) - parseFloat(data.amount_loan === null?'0':`${data.amount_loan}`))+ parseFloat(data.extra_hours)))
                      }</td>
                    <td className="border-sides">{
                      data.emp_acc_status?
                      'PAGADO':
                      <span className="red">
                        POR PAGAR
                      </span>
                    }
                    </td>
                  </tr>
              ))
            }
          </tbody>
        </table>
        <Grid container item xs={12} spacing={2}>
          <Grid item xs={6}>
            <Autocomplete
              id="combo-box-demo"
              disableClearable
              options={this.Company()}
              getOptionLabel={(option) => option.label}
              onChange={(e, values) =>{
                  this.selectsChange(e, values, "comp_det_id", "comp_det_bussiness_name")
              }
              }
              value={{
                label: this.state.comp_det_bussiness_name,
                value: this.state.comp_det_id,
              }}
              getOptionSelected={(option, value) => {
                if (value === option.value) {
                  return option.label;
                } else {
                  return false;
                }
              }}
              style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  error={this.state.errors.comp_det_id.error}
                  helperText={this.state.errors.comp_det_id.message}
                  label="Selecione un Entidad *"
                  variant="outlined"
                  fullWidth
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            {/* <InputMask
            mask="999-999-9999999"
            maskChar=" "
            name="emp_acc_ticket_number"
            disabled={false}
            value={this.state.emp_acc_ticket_number}
            onChange={(event) => {
              this.fieldsChange(event, "emp_acc_ticket_number");
            }}
            >
            {() => (
              <TextField
                variant="outlined"
                type="text"
                label="N. de Recibo"
                fullWidth
                style={{ margin: 8 }}
              />
            )}
            </InputMask> */}
            <Autocomplete
              id="combo-box-demo"
              disableClearable
              filterSelectedOptions
              // loading={this.state.countries_load}
              options={[
                this.selectValue("pay_type_id", "pay_type_desc"),
                ...this.payments(),
              ]}
              getOptionLabel={(option) => option.label}
              onChange={(e, values) =>
                this.selectsChange(
                  e,
                  values,
                  "pay_type_id",
                  "pay_type_desc"
                )
              }
              value={this.selectValue("pay_type_id", "pay_type_desc")}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Forma de Pago *"
                  className="textField"
                  error={this.state.errors.pay_type_id.error}
                  helperText={this.state.errors.pay_type_id.message}
                  variant="outlined"
                  name="pay_type_desc"
                  
                  InputProps={{
                    ...params.InputProps,
                    
                  }}
                />
              )}
            />
          </Grid>
        </Grid>
        <div align="center" className="total-pago-funcionario">
          {'Total a pagar = Gs. '}
          <span>{format(Math.round(this.state.emp_acc_amountpaid))}</span>
        </div>
        <Button
          variant="outlined"
          color="primary"
          disabled={this.state.send_payment}
          onClick={(e)=>{this.handleCreateObject(e),
          this.setState({show_file:false})
          }}
          fullWidth
        >
          Pagar
        </Button>
        <br/>

        {
          this.state.show_file == true &&(
            <div>
              <a href={`/salario_banco.txt`}  download>Click para descargar</a>

            <iframe src={`/salario_banco.txt`} width="100%" height="500px"></iframe>
            </div>
          )
        }
        {showSnack}
      </div>
    );
  }
}
