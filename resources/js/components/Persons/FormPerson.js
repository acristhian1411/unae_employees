import React, { Component, Fragment } from "react";
import { Autocomplete } from "@material-ui/lab";
import {
  Button,
  TextField,
  Stepper,
  Step,
  StepLabel,
  Divider,
  Grid,
  CircularProgress,
  Fab,
  Snackbar,
} from "@material-ui/core";

import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { DropzoneArea, DropzoneDialog } from "material-ui-dropzone";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import validator from "../validator/validator";
import AddIcon from "@material-ui/icons/Add";
import InputAdornment from "@material-ui/core/InputAdornment";
import Tooltip from "@material-ui/core/Tooltip";
import RemoveIcon from "@material-ui/icons/Remove";
import { Alert } from "@material-ui/lab";
import DialogDestroy from "../Dialogs/DialogDestroy";
function getSteps() {
  return ["Paso 1", "Paso 2", "Paso 3"];
  // return ['Paso 1', 'Paso 2', 'Paso 3', 'Contactos'];
}

class FormPerson extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      snack_openDestroy: false,
      message_success: "",
      openAlert: false,
      cntper_id: 0,
      countries_load: false,
      hc_countries_load: false,
      bp_countries_load: false,
      search_country: "",
      open: true,
      activeStep: 0,
      person_fname: "",
      person_lastname: "",
      homecity_id: 0,
      homecity_name: "",
      hc_country_id: 0,
      hc_country_name: "",
      hc_depart_id: 0,
      hc_depart_name: "",
      hc_departments: [],
      hc_cities: [],
      birthplace_id: 0,
      birthplace_name: "",
      bp_country_id: 0,
      bp_country_name: "",
      bp_depart_id: 0,
      bp_depart_name: "",
      bp_departments: [],
      bp_cities: [],
      country_id: 0,
      person_birthdate: "2000-01-01",
      person_gender: "",
      person_idnumber: "",
      person_address: "",
      person_bloodtype: "",
      person_business_name: "",
      person_ruc: "",
      country_name: "",
      person_photo: "",
      cnttype_name: "",
      person_photo2: "",
      sucess: false,
      error: false,
      index: 0,
      imagePreviewUrl: false,
      dropzone: false,
      countries: [],
      contactType: [],
      contactsEdited: false,
      contacts: [
        {
          cnttype_id: 0,
          cnttype_name: "",
          cntper_data: "",
        },
      ],
      contactsEdit: [],
      validator: {
        person_fname: { message: "", error: false },
        person_lastname: { message: "", error: false },
        homecity_id: { message: "", error: false },
        birthplace_id: { message: "", error: false },
        country_id: { message: "", error: false },
        person_birthdate: { message: "", error: false },
        person_gender: { message: "", error: false },
        person_idnumber: { message: "", error: false },
        person_address: { message: "", error: false },
        person_bloodtype: { message: "", error: false },
        person_business_name: { message: "", error: false },
        person_ruc: { message: "", error: false },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.openDropzone = this.openDropzone.bind(this);
    this.closeDropzone = this.closeDropzone.bind(this);
    this.fieldsChange = this.fieldsChange.bind(this); //.person_fname, .person_lastname, .person_birthdate
    this.selectsChange = this.selectsChange.bind(this); // homecity_id:0,
    this.fieldChange13 = this.fieldChange13.bind(this); //person_photo
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.updateField = this.updateField.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.handleCloseAlert = this.handleCloseAlert.bind(this);
    this.destroy = this.destroy.bind(this);
  }
  handleCloseAlert() {
    this.setState({
      openAlert: false,
    });
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  handleNext() {
    // const {person_fname, person_lastname, birthplace_id, homecity_id, country_id,
    // person_birthdate, person_gender, person_idnumber, person_address,
    // person_bloodtype,person_business_name, person_ruc,person_photo} = this.state;
    // if (validator.prototype.validated([{person_fname},{person_lastname},
    //   {birthplace_id}, {homecity_id}, {country_id},
    //   {person_birthdate}, {person_gender}, {person_idnumber}, {person_address},
    //   {person_bloodtype}, {person_ruc}],this.updateField)===true) {

    this.setState({ activeStep: this.state.activeStep + 1 });
    // }
  }
  updateField(field) {
    var { validator } = this.state;
    this.setState({
      validator: {
        ...validator,
        ...field,
      },
    });
  }
  handleBack() {
    this.setState({ activeStep: this.state.activeStep - 1 });
  }

  handleReset() {
    this.setState({ activeStep: 0 });
  }

  handleOpen(data) {
    this.setState({
      openAlert: true,
      cnttype_name: data,
    });
  }
  openDropzone() {
    this.initialFile();
    this.setState({ dropzone: true });
  }
  closeDropzone() {
    this.setState({ dropzone: false });
  }
  handleClose() {
    this.setState({ open: false });
    this.props.onHandleSubmit();
  }
  fieldsChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
    this.props.changeAtribute(true);
  }
  selectsChange(e, values, nameValue, nameLabel, route, obj) {
    if (route) {
      axios.get("api/" + route + "/select/" + values.value).then((res) => {
        if (res.data == false || res.data.length == undefined) {
          this.setState({ [obj]: [] });
        } else {
          this.setState({ [obj]: res.data });
        }
        // this.setState({[obj]: res.data});
      });
    }
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    this.props.changeAtribute(true);
  }
  fieldChange13(e) {
    if (e[0]) {
      this.createImage(e[0]);
    } else {
      this.setState({
        person_photo: "default.png",
        dropzone: false,
      });
    }
    this.props.changeAtribute(true);
  }
  createImage(file) {
    let reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        person_photo: e.target.result,
        dropzone: false,
      });
    };
    reader.readAsDataURL(file);
    reader.onloadend = function (e) {
      this.setState({
        person_photo2: [reader.result],
      });
    }.bind(this);
  }
  searchFieldChange(event, to, id) {
    var e = event;
    var time_out = to;
    var id_name = id;
    this.setState({ [e.target.name]: e.target.value, [id_name]: null });
    e.persist();
    if (this.state.countries_load == false) {
      this.setState({ [time_out]: true });
      setTimeout(() => {
        if (e.target.value === "") {
          axios.get("api/countries").then((res) => {
            this.setState({ countries: res.data.data, [time_out]: false });
          });
        } else {
          axios
            .get(`api/countries/search/${e.target.value}`)
            .then((res) => {
              this.setState({
                countries: res.data.data,
                [time_out]: false,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  handleSubmit(e) {
    const {
      person_fname,
      person_lastname,
      birthplace_id,
      homecity_id,
      country_id,
      person_birthdate,
      person_gender,
      person_idnumber,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
      person_photo,
    } = this.state;
    e.preventDefault();
    var object_error = {};
    var validator = {
      person_fname: { message: "", error: false },
      person_lastname: { message: "", error: false },
      homecity_id: { message: "", error: false },
      birthplace_id: { message: "", error: false },
      country_id: { message: "", error: false },
      person_birthdate: { message: "", error: false },
      person_gender: { message: "", error: false },
      person_idnumber: { message: "", error: false },
      person_address: { message: "", error: false },
      person_bloodtype: { message: "", error: false },
      person_business_name: { message: "", error: false },
      person_ruc: { message: "", error: false },
    };
    axios
      .post("/api/persons", {
        person_fname,
        person_lastname,
        homecity_id,
        birthplace_id,
        country_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        person_photo,
      })
      .then((res) => {
        var person = res.data.dato.person_id;
        axios.post("/api/contactPersons-per", {
          person_id: person,
          contacts: this.state.contacts,
        });

        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        this.setState({
          person_fname: "",
          person_lastname: "",
          homecity_id: 0,
          birthplace_id: 0,
          country_id: 0,
          person_birthdate: "",
          person_gender: "",
          person_idnumber: "",
          person_address: "",
          person_bloodtype: "",
          person_business_name: "",
          person_ruc: "",
          country_name: "",
          person_photo: "",
        });
        this.handleClose();
      })
      .catch((error) => {
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  handleSubmit1(e) {
    e.preventDefault();
    const {
      person_fname,
      person_lastname,
      birthplace_id,
      homecity_id,
      country_id,
      person_birthdate,
      person_gender,
      person_idnumber,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
      person_photo,
    } = this.state;
    var object_error = {};
    var validator = {
      person_fname: { message: "", error: false },
      person_lastname: { message: "", error: false },
      homecity_id: { message: "", error: false },
      birthplace_id: { message: "", error: false },
      country_id: { message: "", error: false },
      person_birthdate: { message: "", error: false },
      person_gender: { message: "", error: false },
      person_idnumber: { message: "", error: false },
      person_address: { message: "", error: false },
      person_bloodtype: { message: "", error: false },
      person_business_name: { message: "", error: false },
      person_ruc: { message: "", error: false },
    };
    axios
      .put(`/api/persons/${this.props.person.data.person_id}`, {
        person_fname,
        person_lastname,
        homecity_id,
        birthplace_id,
        country_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        person_photo,
      })
      .then((res) => {
        if (this.state.contactsEdit.length != 0) {
          axios.post("/api/contactPersons-per", {
            person_id: this.props.person.data.person_id,
            contacts: this.state.contactsEdit,
          });
        }
        if (this.state.contactsEdited === true) {
          axios.put("/api/contactPersons", {
            person_id: this.props.person.data.person_id,
            contacts: this.state.contacts,
          });
        }

        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }
  componentDidMount() {
    axios.get("/api/countries").then((res) => {
      this.setState({ countries: res.data.data });
    });
    axios.get("/api/contactTypes").then((response) => {
      this.setState({
        contactType: response.data.data,
      });
    });
    if (this.props.edit) {
      const {
        person_fname,
        person_lastname,
        homecity_id,
        birthplace_id,
        country_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        person_photo,
        country_name,
      } = this.props.person.data;
      axios
        .get(`/api/contactPersons-per/${this.props.person.data.person_id}`)
        .then((res) => {
          var cont = res.data.data;
          var values = [];
          if (cont.length != 0) {
            cont.forEach((item) => {
              values.push({
                cntper_id: item.cntper_id,
                cnttype_id: item.cnttype_id,
                cnttype_name: item.cnttype_name,
                cntper_data: item.cntper_data,
              });
            });
            this.setState({
              contacts: values,
            });
          } else {
            this.setState({ contactsEdit: this.state.contacts });
          }
        });
      axios.get("/api/cities/" + homecity_id).then((res) => {
        this.setState({
          homecity_name: res.data.data.city_name,
          hc_country_id: res.data.data.country_id,
          hc_country_name: res.data.data.country_name,
          hc_depart_id: res.data.data.depart_id,
          hc_depart_name: res.data.data.depart_name,
        });
        axios
          .get(`/api/departments/select/${res.data.data.country_id}`)
          .then((res) => {
            this.setState({ hc_departments: res.data });
          });
        axios
          .get(`/api/cities/select/${res.data.data.depart_id}`)
          .then((res) => {
            // this.setState({hc_cities: res.data});
          });
      });
      axios.get("/api/cities/" + birthplace_id).then((res) => {
        this.setState({
          birthplace_name: res.data.data.city_name,
          bp_country_id: res.data.data.country_id,
          bp_country_name: res.data.data.country_name,
          bp_depart_id: res.data.data.depart_id,
          bp_depart_name: res.data.data.depart_name,
        });
        axios
          .get(`/api/departments/select/${res.data.data.country_id}`)
          .then((res) => {
            this.setState({ bp_departments: res.data });
          });
        axios
          .get(`/api/cities/select/${res.data.data.depart_id}`)
          .then((res) => {
            this.setState({ bp_cities: res.data });
          });
      });
      this.setState({
        person_fname,
        person_lastname,
        homecity_id,
        birthplace_id,
        country_id,
        person_birthdate,
        person_gender,
        person_idnumber,
        person_address,
        person_bloodtype,
        person_business_name,
        person_ruc,
        country_name,
        person_photo,
        person_photo2: `/img/${this.props.person.data.person_photo}`,
      });
    }
  }
  Country() {
    return this.state.countries.map((data) => ({
      label: data.country_name,
      value: data.country_id,
    }));
  }
  HcDepartments() {
    return this.state.hc_departments.map((data) => ({
      label: data.depart_name,
      value: data.depart_id,
    }));
  }
  HcCities() {
    return this.state.hc_cities.map((data) => ({
      label: data.city_name,
      value: data.city_id,
    }));
  }
  BpDepartments() {
    return this.state.bp_departments.map((data) => ({
      label: data.depart_name,
      value: data.depart_id,
    }));
  }
  BpCities() {
    return this.state.bp_cities.map((data) => ({
      label: data.city_name,
      value: data.city_id,
    }));
  }
  selectContacType() {
    return this.state.contactType.map((data) => ({
      label: data.cnttype_name,
      value: data.cnttype_id,
    }));
  }
  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }
  initialFile() {
    if (this.props.edit) {
      return [`/img/${this.props.person.data.person_photo}`];
    } else if (this.state.person_photo2) {
      return [this.state.person_photo2];
    } else {
      return [];
    }
  }
  returnImage() {
    if (this.state.person_photo) {
    } else {
      return false;
    }
  }
  editOrNew() {
    if (this.props.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit1}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleSubmit}
        >
          Guardar
        </Button>
      );
    }
  }

  handleAddFields() {
    if (this.props.edit) {
      var values = [...this.state.contacts];
      values.push({ cnttype_id: 0, cnttype_name: "", cntper_data: "" });
      this.setState({ contacts: values });
      var values2 = [...this.state.contactsEdit];
      values2.push({ cnttype_id: 0, cnttype_name: "", cntper_data: "" });
      this.setState({ contactsEdit: values2 });
    } else {
      var values = [...this.state.contacts];
      values.push({ cnttype_id: 0, cnttype_name: "", cntper_data: "" });
      this.setState({ contacts: values });
    }
  }

  handleRemoveFields(index) {
    if (this.props.edit) {
      const values = [...this.state.contacts];
      this.setState({
        index: index,
        cntper_id: values[index].cntper_id,
      });
      this.handleOpen(values[index].cnttype_name);
    } else {
      var values = [...this.state.contacts];
      values.splice(index, 1);
      this.setState({ contacts: values });
    }
  }

  destroy() {
    // event.preventDefault();
    axios.delete(`/api/contactPersons/${this.state.cntper_id}`).then((res) => {
      var values = [...this.state.contacts];
      values.splice(this.state.index, 1);
      this.setState({
        snack_open: true,
        message_success: res.data.data,
        contacts: values,
      });
      this.handleAddFields();
    });
  }

  handleInputChange(index, event) {
    if (this.props.edit) {
      const values = [...this.state.contacts];
      const values2 = [...this.state.contactsEdit];
      var index2;
      if (values.length === values2.length) {
        index2 = index;
      } else if (values2.length != 0) {
        index2 = index - 1;

        if (event.target.name === "cntper_data") {
          // values[index].cntper_data = event.target.value;
          values2[index2].cntper_data = event.target.value;
        } else if (event.target.name === "cnttype_id") {
          // values[index].cnttype_id = event.target.value;
          values2[index2].cnttype_id = event.target.value;
        } else if (event.target.name === "cnttype_name") {
          values2[index2].cnttype_name = event.target.value;
          // values[index].cnttype_name = event.target.value;
        }
      }

      if (event.target.name === "cntper_data") {
        values[index].cntper_data = event.target.value;
        // values2[index2].cntper_data = event.target.value;
      } else if (event.target.name === "cnttype_id") {
        values[index].cnttype_id = event.target.value;
        // values2[index2].cnttype_id = event.target.value;
      } else if (event.target.name === "cnttype_name") {
        // values2[index2].cnttype_name = event.target.value;
        values[index].cnttype_name = event.target.value;
      }
      this.setState({
        contacts: values,
        contactsEdit: values2,
        contactsEdited: true,
      });
    } else {
      const values = [...this.state.contacts];
      if (event.target.name === "cntper_data") {
        values[index].cntper_data = event.target.value;
      } else if (event.target.name === "cnttype_id") {
        values[index].cnttype_id = event.target.value;
      } else if (event.target.name === "cnttype_name") {
        values[index].cnttype_name = event.target.value;
      }
      this.setState({ contacts: values });
    }
  }

  onChangeContactType(e, values, index) {
    if (this.props.edit) {
      var value = [...this.state.contacts];
      var value2 = [...this.state.contactsEdit];
      var index2;
      if (value.length === value2.length) {
        index2 = index;
      } else if (value2.length != 0) {
        index2 = index - 1;
        value2[index2].cnttype_id = values.value;
        value2[index2].cnttype_name = values.label;
      }
      value[index].cnttype_id = values.value;
      value[index].cnttype_name = values.label;
      this.setState({
        contacts: value,
        contactsEdit: value2,
        contactsEdited: true,
      });
    } else {
      const value = [...this.state.contacts];
      value[index].cnttype_id = values.value;
      value[index].cnttype_name = values.label;
      this.setState({ contacts: value });
    }
  }

  getStepContent(stepIndex) {
    const {
      open,
      person_fname,
      person_lastname,
      person_idnumber,
      dropzone,
      person_birthdate,
      person_gender,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
    } = this.state;
    switch (stepIndex) {
      case 0:
        return (
          <div>
            <br />
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Nombres"
                    fullWidth
                    name="person_fname"
                    style={{ margin: 8 }}
                    value={person_fname}
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                    error={this.state.validator.person_fname.error}
                    helperText={this.state.validator.person_fname.message}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Apellidos"
                    fullWidth
                    style={{ margin: 8 }}
                    value={person_lastname}
                    name="person_lastname"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                    error={this.state.validator.person_lastname.error}
                    helperText={this.state.validator.person_lastname.message}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Cedula"
                    fullWidth
                    style={{ margin: 8 }}
                    value={person_idnumber}
                    name="person_idnumber"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                    error={this.state.validator.person_idnumber.error}
                    helperText={this.state.validator.person_idnumber.message}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    filterSelectedOptions
                    loading={this.state.countries_load}
                    options={[
                      this.selectValue("country_name", "country_id"),
                      ...this.Country(),
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "country_id",
                        "country_name"
                      )
                    }
                    value={this.selectValue("country_name", "country_id")}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Pais de Nacionalidad *"
                        error={this.state.validator.country_id.error}
                        helperText={this.state.validator.country_id.message}
                        className="textField"
                        variant="outlined"
                        name="country_name"
                        onChange={(e) =>
                          this.searchFieldChange(
                            e,
                            "countries_load",
                            "country_id"
                          )
                        }
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {this.state.countries_load === true ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Fecha de Nacimiento"
                    type="date"
                    fullWidth
                    style={{ margin: 8 }}
                    value={person_birthdate}
                    name="person_birthdate"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    variant="outlined"
                    error={this.state.validator.person_birthdate.error}
                    helperText={this.state.validator.person_birthdate.message}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("person_gender", "person_gender"),
                      { value: "Masculino", label: "Masculino" },
                      { value: "Femenino", label: "Femenino" },
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "person_gender",
                        "person_gender"
                      )
                    }
                    value={this.selectValue("person_gender", "person_gender")}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    filterSelectedOptions
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Sexo *"
                        error={this.state.validator.person_gender.error}
                        helperText={this.state.validator.person_gender.message}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </Grid>
          </div>
        );
      case 1:
        return (
          <div>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <p className="divisor">Seleccione la ciudad de residencia</p>
                <Divider absolute />
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("hc_country_name", "hc_country_id"),
                      ...this.Country(),
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "hc_country_id",
                        "hc_country_name",
                        "departments",
                        "hc_departments"
                      )
                    }
                    value={this.selectValue("hc_country_name", "hc_country_id")}
                    fullWidth
                    loading={this.state.hc_countries_load}
                    filterSelectedOptions
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    style={{ margin: 8 }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Pais *"
                        error={this.state.validator.homecity_id.error}
                        className="textField"
                        variant="outlined"
                        name="hc_country_name"
                        onChange={(e) =>
                          this.searchFieldChange(
                            e,
                            "hc_countries_load",
                            "hc_country_id"
                          )
                        }
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {this.state.hc_countries_load === true ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("hc_depart_name", "hc_depart_id"),
                      ...this.HcDepartments(),
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "hc_depart_id",
                        "hc_depart_name",
                        "cities",
                        "hc_cities"
                      )
                    }
                    value={this.selectValue("hc_depart_name", "hc_depart_id")}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    filterSelectedOptions
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Departamento *"
                        error={this.state.validator.homecity_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("homecity_name", "homecity_id"),
                      ...this.HcCities(),
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "homecity_id",
                        "homecity_name"
                      )
                    }
                    value={this.selectValue("homecity_name", "homecity_id")}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    filterSelectedOptions
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Ciudad *"
                        error={this.state.validator.homecity_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
              </Grid>
              <Divider absolute />
              <Grid container item xs={12} spacing={3}>
                <p className="divisor">Seleccione la ciudad de origen</p>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("bp_country_name", "bp_country_id"),
                      ...this.Country(),
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "bp_country_id",
                        "bp_country_name",
                        "departments",
                        "bp_departments"
                      )
                    }
                    value={this.selectValue("bp_country_name", "bp_country_id")}
                    fullWidth
                    loading={this.state.bp_countries_load}
                    filterSelectedOptions
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    style={{ margin: 8 }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Pais *"
                        error={this.state.validator.birthplace_id.error}
                        className="textField"
                        variant="outlined"
                        name="bp_country_name"
                        onChange={(e) =>
                          this.searchFieldChange(
                            e,
                            "bp_countries_load",
                            "bp_country_id"
                          )
                        }
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {this.state.bp_countries_load === true ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("bp_depart_name", "bp_depart_id"),
                      ...this.BpDepartments(),
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "bp_depart_id",
                        "bp_depart_name",
                        "cities",
                        "bp_cities"
                      )
                    }
                    value={this.selectValue("bp_depart_name", "bp_depart_id")}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    filterSelectedOptions
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Departamento *"
                        error={this.state.validator.birthplace_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("birthplace_name", "birthplace_id"),
                      ...this.BpCities(),
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "birthplace_id",
                        "birthplace_name"
                      )
                    }
                    value={this.selectValue("birthplace_name", "birthplace_id")}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    filterSelectedOptions
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Ciudad *"
                        error={this.state.validator.birthplace_id.error}
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </Grid>
          </div>
        );
      case 2:
        return (
          <div>
            <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Direccion"
                    fullWidth
                    style={{ margin: 8 }}
                    value={person_address}
                    name="person_address"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                    error={this.state.validator.person_address.error}
                    helperText={this.state.validator.person_address.message}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[
                      this.selectValue("person_bloodtype", "person_bloodtype"),
                      { value: "A+", label: "A+" },
                      { value: "B+", label: "B+" },
                      { value: "AB+", label: "AB+" },
                      { value: "0", label: "0" },
                      { value: "A-", label: "A-" },
                      { value: "B-", label: "B-" },
                      { value: "AB-", label: "AB-" },
                    ]}
                    getOptionLabel={(option) => option.label}
                    onChange={(e, values) =>
                      this.selectsChange(
                        e,
                        values,
                        "person_bloodtype",
                        "person_bloodtype"
                      )
                    }
                    value={this.selectValue(
                      "person_bloodtype",
                      "person_bloodtype"
                    )}
                    getOptionSelected={(option, value) =>
                      option.value === value.value
                    }
                    filterSelectedOptions
                    style={{ margin: 8 }}
                    fullWidth
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Tipo de sangre *"
                        error={this.state.validator.person_bloodtype.error}
                        helperText={
                          this.state.validator.person_bloodtype.message
                        }
                        className="textField"
                        variant="outlined"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Button
                    onClick={this.openDropzone.bind(this)}
                    variant="outlined"
                    fullWidth
                    style={{ margin: 8 }}
                  >
                    Foto de Perfil
                  </Button>
                  <DropzoneDialog
                    dialogTitle="Cargar imagen"
                    cancelButtonText="Cancelar"
                    submitButtonText="Enviar"
                    dropzoneText="Arrastre la imagen aqui o haga click para selecionar una foto"
                    open={dropzone}
                    acceptedFiles={["image/*"]}
                    initialFiles={this.initialFile()}
                    onSave={this.fieldChange13.bind(this)}
                    dropzoneText="Arrastre la imagen aqui o click para selecionar una foto"
                    showAlerts={false}
                    filesLimit={1}
                    onClose={this.closeDropzone.bind(this)}
                  />
                </Grid>
              </Grid>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="Nombre de negocio"
                    fullWidth
                    style={{ margin: 8 }}
                    value={person_business_name}
                    name="person_business_name"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={4}>
                  <TextField
                    id="outlined-basic"
                    label="RUC"
                    fullWidth
                    style={{ margin: 8 }}
                    value={person_ruc}
                    name="person_ruc"
                    onChange={this.fieldsChange.bind(this)}
                    margin="normal"
                    variant="outlined"
                    error={this.state.validator.person_ruc.error}
                    helperText={this.state.validator.person_ruc.message}
                  />
                </Grid>
                <Grid item xs={4} style={{ textAlign: "center" }}>
                  <img
                    className="image-profile"
                    src={this.state.person_photo2}
                  />
                </Grid>
                <Grid container item xs={12} spacing={1}>
                  <Grid item xs={12} style={{ textAlign: "center" }}>
                    <h3>Contactos</h3>
                    <hr />
                  </Grid>
                </Grid>
                {this.state.contacts.map((inputField, index) => (
                  <Fragment key={`${inputField}~${index}`}>
                    <Grid container item xs={12} spacing={1}>
                      <Grid item xs={4} style={{ textAlign: "center" }}>
                        <Autocomplete
                          id={index}
                          name="cnttype_id"
                          disableClearable
                          options={this.selectContacType()}
                          filterSelectedOptions
                          getOptionLabel={(option) => option.label}
                          onChange={(e, values) => {
                            this.onChangeContactType(e, values, index);
                          }}
                          value={{
                            value: inputField.cnttype_id,
                            label: inputField.cnttype_name,
                          }}
                          getOptionSelected={(option, value) =>
                            option.value === value.value
                          }
                          style={{ margin: 8 }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              required
                              label="Tipo de contacto "
                              variant="outlined"
                              size="small"
                            />
                          )}
                        />
                      </Grid>
                      <Grid item xs={6} style={{ textAlign: "center" }}>
                        <TextField
                          variant="outlined"
                          required
                          className="email"
                          size="small"
                          id="cntper_data"
                          name="cntper_data"
                          value={inputField.cntper_data}
                          onChange={(event) =>
                            this.handleInputChange(index, event)
                          }
                          label="Dato "
                          style={{ margin: 8 }}
                          fullWidth
                          margin="normal"
                          variant="outlined"
                          InputProps={{
                            endAdornment: index + 1 <=
                              this.state.contacts.length && (
                                <InputAdornment position="start">
                                  <Tooltip title="Agregar Contacto">
                                    <Fab
                                      color="primary"
                                      size="small"
                                      onClick={() => this.handleAddFields()}
                                    >
                                      <AddIcon />
                                    </Fab>
                                  </Tooltip>
                                  <Tooltip title="Eliminar Contacto">
                                    <Fab
                                      color="secondary"
                                      size="small"
                                      onClick={() =>
                                        this.handleRemoveFields(index)
                                      }
                                    >
                                      <RemoveIcon />
                                    </Fab>
                                  </Tooltip>
                                </InputAdornment>
                              ),
                          }}
                        />
                      </Grid>
                    </Grid>
                  </Fragment>
                ))}
              </Grid>
            </Grid>
          </div>
        );
      // case 3:
      //   return(
      //     <div>
      //      Formulario de contactos
      //     </div>
      //   );

      default:
        return "Unknown stepIndex";
    }
  }
  render() {
    const steps = getSteps();

    const {
      open,
      person_fname,
      person_lastname,
      person_idnumber,
      dropzone,
      person_birthdate,
      person_gender,
      person_address,
      person_bloodtype,
      person_business_name,
      person_ruc,
      validator,
      snack_open,
    } = this.state;
    var showSnack;
    var showDialogDestroy;
    var showSnackDestoy;
    if (this.state.openAlert) {
      showDialogDestroy = (
        <DialogDestroy
          index={`${this.state.cnttype_name}`}
          onClose={this.handleCloseAlert}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (this.state.snack_openDestroy) {
      showSnackDestoy = (
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={this.state.snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {" "}
            {this.state.message_success}{" "}
          </Alert>{" "}
        </Snackbar>
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["person_fname"]["error"] ? (
              <li>Error: {validator["person_fname"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_lastname"]["error"] ? (
              <li>Error: {validator["person_lastname"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["homecity_id"]["error"] ? (
              <li>Error: {validator["homecity_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["birthplace_id"]["error"] ? (
              <li>Error: {validator["birthplace_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["country_id"]["error"] ? (
              <li>Error: {validator["country_id"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_birthdate"]["error"] ? (
              <li>Error: {validator["person_birthdate"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_gender"]["error"] ? (
              <li>Error: {validator["person_gender"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_idnumber"]["error"] ? (
              <li>Error: {validator["person_idnumber"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_address"]["error"] ? (
              <li>Error: {validator["person_address"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_bloodtype"]["error"] ? (
              <li>Error: {validator["person_bloodtype"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_business_name"]["error"] ? (
              <li>Error: {validator["person_business_name"]["message"]}</li>
            ) : (
              ""
            )}
            {validator["person_ruc"]["error"] ? (
              <li>Error: {validator["person_ruc"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3 align="center">Formulario de Personas</h3>
        <Divider />
        <Stepper activeStep={this.state.activeStep} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {this.state.activeStep === steps.length ? (
            <div>
              All steps completed
              <Button onClick={this.handleReset}>Reset</Button>
            </div>
          ) : (
            <div>
              {this.getStepContent(this.state.activeStep)}
              <div>
                <Button
                  disabled={this.state.activeStep === 0}
                  onClick={this.handleBack}
                  className="backButton"
                >
                  <NavigateBeforeIcon />
                  Atras
                </Button>

                {this.state.activeStep === steps.length - 1 ? (
                  this.editOrNew()
                ) : (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.handleNext}
                  >
                    Continuar
                    <NavigateNextIcon />
                  </Button>
                )}
                {showSnack}
                {showDialogDestroy}
                <Button
                  variant="outlined"
                  color="secondary"
                  startIcon={<ExitToAppIcon />}
                  onClick={this.handleClose}
                >
                  Cerrar
                </Button>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
export default FormPerson;
