import React, { Component, lazy } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import FormPerson from "./FormPerson";
import MidModal from "../Modals/MidModal";
import DialogDestroy from "../Dialogs/DialogDestroy";
import Paginator from "../Paginators/Paginator";
import SearchIcon from "@material-ui/icons/Search";
import ClearIcon from "@material-ui/icons/Clear";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Grid,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  Tooltip,
  TableSortLabel,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import {Capitalize} from '../GlobalFunctions/LetterCase';

export default class Persons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      persons: [],
      filteredPersons: [],
      person_id: 0,
      person: [],
      edit: false,
      _new: false,
      open: false,
      paginator: [],
      rowsPerPage: 10,
      order: "asc",
      orderBy: "person_idnumber",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/persons",
      buscador: false,
      busqueda: false,
      person_fname: "",
      person_lastname: "",
      person_idnumber: "",
      permissions: {
        persons_index: null,
        persons_show: null,
        persons_store: null,
        persons_update: null,
        persons_destroy: null,
      },
    };
    this.handleClickOpen = (data) => {
      this.setState({ open: true, person: data });
    };
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.getObject = async (
      perPage,
      page,
      route,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (route) {
        res = await axios.get(
          `${route}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
        );
      } else {
        if (this.state.busqueda) {
          res = await axios.get(
            `${this.state.url}&sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
          );
        } else {
          res = await axios.get(
            `${this.state.url}?sort_by=${orderBy}&order=${order}&per_page=${perPage}&page=${page}`
          );
        }
      }
      let { data } = await res;
      if (this._ismounted) {
        this.setState({
          persons: data,
          paginator: res.data,
          filteredPersons: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        _new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };

    this.destroy = this.destroy.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.search = this.search.bind(this);
    this.cleanfilter = this.cleanfilter.bind(this);
    this.fieldsChange = this.fieldsChange.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("PERSONAS");
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "persons_index",
      "persons_show",
      "persons_store",
      "persons_update",
      "persons_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  handleModal() {
    this.setState({ _new: true });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  editModal(data) {
    this.setState({ edit: true, person: data });
  }
  fieldsChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }

  destroy(event) {
    // event.preventDefault();
    axios.delete(`/api/persons/${this.state.person_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    // if (this.state.time_out == false) {
      this.setState({ time_out: true });
      // setTimeout(() => {
        if (this.state.buscador) {
          var param = `/api/persons-search?filter[person_idnumber]=${this.state.person_idnumber}&filter[person_lastname]=${this.state.person_lastname}&filter[person_idnumber]=${this.state.person_idnumber}`
          this.setState({
            url: param,
          });
          axios
            .get(`${param}&per_page=${this.state.rowsPerPage}`)
            .then((res) => {
              this.setState({
                filteredPersons: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }else {
          if (e.target.value === "") {
            this.setState({
              filteredPersons: this.state.persons.data,
              paginator: this.state.persons,
              page_lenght: this.state.persons.last_page,
              page: 1,
              time_out: false,
              busqueda: false,
              url: `/api/persons`,
            });
            this.updateState();
          } else {
            var param = `/api/persons-search?search=${e.target.value}`
            this.setState({
              url: param,
            });
            axios
              .get(
                `${param}&sort_by=${this.state.orderBy}&order=${
                  this.state.order
                }&per_page=${this.state.rowsPerPage}&page=${1}`
              )
              .then((res) => {
                this.setState({
                  filteredPersons: res.data.data,
                  paginator: res.data,
                  page_lenght: res.data.last_page,
                  page: res.data.current_page,
                  time_out: false,
                  busqueda: true,
                });
              })
              .catch((error) => {
                this.setState({time_out: false});
                console.log("Ingrese un valor valido");
              });
            }



        }

    //   }, 3000);
    // }
  }

  filter() {
    return (
      <div >
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Nombre"
                type="search"
                variant="outlined"
                size="small"
                name="person_fname"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.person_fname}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Apellido"
                type="search"
                variant="outlined"
                size="small"
                name="person_lastname"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.person_lastname}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Cedula"
                type="number"
                variant="outlined"
                size="small"
                name="person_idnumber"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.person_idnumber}
              />
            </Grid>
            <Grid item xs={3}>
              <Button
                variant="contained"
                color="primary"
                startIcon={<SearchIcon />}
                onClick={this.search.bind(this)}
              >
                Buscar
              </Button>
              <Tooltip title="Limpiar filtro">
                <Button
                  variant="contained"
                  color="secondary"
                  size="small"
                  startIcon={<ClearIcon />}
                  onClick={() => {
                    this.cleanfilter();
                  }}
                >
                  {" "}
                </Button>
              </Tooltip>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
  cleanfilter() {
    this.setState({
      url: `/api/persons`,
      busqueda: false,
      page: 1,
      person_fname: "",
      person_lastname: "",
      person_idnumber: "",
    });
    this.getObject(this.state.rowsPerPage, this.state.page, "/api/persons");
  }

  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  render() {
    var busqueda;
    var busquedaAv;
    const {
      snack_open,
      message_success,
      open,
      _new,
      edit,
      person,
      filteredPersons,
      rowsPerPage,
      orderBy,
      order,
      permissions,
    } = this.state;
    if (this.state.buscador) {
      busquedaAv = this.filter();
    } else {
      busqueda = (
        <TextField
          id="outlined-search"
          label="Escriba para buscar"
          type="search"
          variant="outlined"
          // onChange={this.search}
          onKeyDown={(e) =>{
            if(e.key === 'Enter'){
              this.search(e)
            }
          }}
          style={{ width: "40%"}}
          InputProps={{
            endAdornment: (
              <React.Fragment>
                {this.state.time_out === true ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
              </React.Fragment>
            ),
          }}
        />
      );
    }
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    if (_new) {
      // showModal = <FormPerson onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormPerson />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (edit) {
      // showModal = <FormPerson onHandleSubmit={this.updateState} edit ={true} person={person}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormPerson />,
            props_form: {
              edit: true,
              person: person,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={`${person.data.person_fname} ${person.data.person_lastname}`}
          onClose={this.handleClose}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filteredPersons.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div style={{textAlign: "center"}}>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <h2 align="center">
                 {busqueda}
                <Button
                  variant="outlined"
                  color="primary"
                  type="submit"
                  onClick={() => {
                    this.state.buscador === false
                      ? this.setState({ buscador: true })
                      : this.setState({ buscador: false });
                  }}
                >
                  {this.state.buscador?"Salir de busqueda avanzada": "Busqueda avanzada"}
                </Button>
              </h2>
            </Grid>
          </Grid>

          <Grid container item xs={12} spacing={3}>
            <Grid item xs={12}>
              <div align="center">{busquedaAv}</div>
            </Grid>
          </Grid>
        </Grid>
        <hr />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "person_idnumber" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_idnumber"}
                    direction={orderBy === "person_idnumber" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_idnumber");
                    }}
                  >
                    <h3>Documento</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "person_lastname" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_lastname"}
                    direction={orderBy === "person_lastname" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_lastname");
                    }}
                  >
                    <h3>Apellidos</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "person_fname" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "person_fname"}
                    direction={orderBy === "person_fname" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("person_fname");
                    }}
                  >
                    <h3>Nombres</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "telephone" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "telephone"}
                    direction={orderBy === "telephone" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("telephone");
                    }}
                  >
                    <h3>Telefono</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2">
                  {/*{permissions.persons_store === true && (
                    <Button
                      variant="outlined"
                      color="primary"
                      type="submit"
                      onClick={() => {
                        this.handleModal();
                      }}
                    >
                      <AddBoxOutlinedIcon />
                    </Button>
                  )}*/}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {/* <Paginator
        lenght={this.state.page_lenght}
        perPageChange= {this.perPageChange}
        pageChange ={this.pageChange}
        page= {this.state.page}
        /> */}
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }
  renderList() {
    const {
      open,
      filteredPersons,
      rowsPerPage,
      page,
      permissions,
    } = this.state;
    if (filteredPersons.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return filteredPersons.map((data) => {
        return (
          <TableBody key={data.person_id}>
            <TableRow>
              <TableCell>{data.person_idnumber}</TableCell>
              <TableCell>{Capitalize(data.person_lastname)}</TableCell>
              <TableCell>{Capitalize(data.person_fname)}</TableCell>
              <TableCell>{data.telephone}</TableCell>
              <TableCell>
                {permissions.persons_show === true && (
                  <Link
                    className="navbar-brand"
                    to={"/personas/show/" + data.person_id}
                  >
                    <Button
                      variant="outlined"
                      color="primary"
                      startIcon={<VisibilityIcon />}
                    >
                      {" "}
                    </Button>
                  </Link>
                )}
              </TableCell>
              <TableCell>
                {permissions.persons_update === true && (
                  <Button
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.editModal({ data: data });
                    }}
                  >
                    <EditIcon />
                  </Button>
                )}
              </TableCell>
              <TableCell>
                {permissions.persons_destroy === true && (
                  <Button
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      this.handleClickOpen({ data: data }),
                        this.setState({ person_id: data.person_id });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  } //cierra el renderList
}
