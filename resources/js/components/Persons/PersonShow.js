import React, { Component, lazy} from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {AppBar, Tabs, Tab, Typography, List, ListItem, ListItemText, Divider,
  Button, Snackbar, CircularProgress} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Card } from 'react-bootstrap';
import ContactPersonsList from '../ContactPersons/ContactPersonsList';
import UserPerson from '../Users/UserPerson';
import AddBoxOutlinedIcon from '@material-ui/icons/AddBoxOutlined';
import HomeIcon from '@material-ui/icons/Home';
import ListIcon from '@material-ui/icons/List';
import PersonIcon from '@material-ui/icons/Person';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import FormStudent from '../Students/FormStudent';
import FormEmployees from '../Employees/FormEmployees';
import FormProfessor from '../Professors/FormProfessor';
import MidModal from '../Modals/MidModal';
import StudentWorkplacesList from '../StudentWorkplaces/StudentWorkplacesList';
import StudentHealthcaresList from '../StudentHealthcares/StudentHealthcaresList';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  media: {
    height: 140,
  },
});

class PersonShow extends Component{
  constructor(props){
    super(props)
    this.state = {
      persons: {},
      students: {},
      employees: {},
      professors: {},
      student: false,
      employee: false,
      professor: false,
      value: 0,
      form_student: false,
      form_employee: false,
      form_professor: false,
      snack_open: false,
      message_success: ''
    }
    this.updateState= () =>{
      this.setState({
        form_student: false,
        form_employee: false,
        form_professor: false
      });
      this.getStudent();
      this.getEmployee();
      this.getProfessor();
    }

    this.getProfessor=()=>{
      axios.get(`/api/professors/${this.props.match.params.id}`).then(response => {
        if(!response.data){
        this.setState({
          professor: false
        })
      }else{
        this.setState({
          professor: true,
          professors: response.data.data
        })
      }

      })
    }

    this.getStudent=()=>{
      axios.get(`/api/students/${this.props.match.params.id}`).then(response => {
        if(!response.data){
        this.setState({
          student: false
        })
      }else{
        this.setState({
          student: true,
          students: response.data.data
        })
      }

      })
    }

    this.getEmployee=()=>{
      axios.get(`/api/employees/${this.props.match.params.id}`).then(response => {
        if(!response.data){
        this.setState({
          employee: false
        })
      }else{
        this.setState({
          employee: true,
          employees: response.data.data
        })
      }

      })
    }
    this.handleChange = this.handleChange.bind(this);
    this.openFormModalStudent = this.openFormModalStudent.bind(this);
    this.openFormModalEmployee = this.openFormModalEmployee.bind(this);
    this.openFormModalProfessor = this.openFormModalProfessor.bind(this);
    this.openSnack = this.openSnack.bind(this);
  }

  componentDidMount () {
    this.getStudent();
    this.getEmployee();
    this.getProfessor();
      const id = this.props.match.params.id
      axios.get(`/api/persons/${id}`).then(response => {
        this.setState({
          persons: response.data.data
        })
      })
    }

    ReturnStudent(){
      if(this.state.student){
        return(
          <div>
          <div className="card_person">
                    <img className="profile_photo" src={`/img/${this.state.students.person_photo}`} />
                    <div className="card_label_field">
                      <p className="title_column">Status </p>
                      <p className="value_column">{this.state.students.stu_status}</p>
                      <p className="title_column">Alergias </p>
                      <p className="value_column">{this.state.students.stu_allergies}</p>
                      <p className="title_column">Institucion de apoyo </p>
                      <p className="value_column">{this.state.students.stu_instsupport}</p>
                      <p className="title_column">Año de graduacion </p>
                      <p className="value_column">{this.state.students.stu_gradyear}</p>
                      <p className="title_column">Colegio </p>
                      <p className="value_column">{this.state.students.school_name}</p>
                      <p className="title_column">Titulo secundario </p>
                      <p className="value_column">{this.state.students.hstitle_name}</p>
                    </div>
                </div>
                <div className="card_person1">
                  <div className="card_label_field">
                    <StudentWorkplacesList person={this.props.match.params.id} />

                  </div>
              </div>
              <div className="card_person1">
                  <div className="card_label_field">
                    <StudentHealthcaresList person={this.props.match.params.id} />

                  </div>
                  </div>

                </div>
        )
      }
      //fin if
      else{
        return(
          <div>
             <Button variant="outlined" color="primary" type='submit' onClick={()=>{this.openFormModalStudent()}}>
                  < AddBoxOutlinedIcon />
                </Button>
                <Divider />
            <div className="card_person1">
                    <div className="card_label_field">
                      <p className="value_column">No Tiene este perfil</p>

                    </div>
                </div>
          </div>
        )
      }
    //fin else
    }


    ReturnEmployee(){
      if(this.state.employee){
        return(
          <div>
          <div className="card_person">
                    <img className="profile_photo" src={`/img/${this.state.students.person_photo}`} />
                    <div className="card_label_field">
                      <p className="title_column">Fecha de inicio </p>
                      <p className="value_column">{this.state.employees.emplo_startdate}</p>
                     <p className="title_column">Salario </p>
                      <p className="value_column">{this.state.employees.emplo_salary}</p>
                      <p className="title_column">Cargo </p>
                      <p className="value_column">{this.state.employees.appoin_description}</p>
                      <p className="title_column">Observaciones </p>
                      <p className="value_column">{this.state.employees.emplo_observation}</p>

                    </div>
                </div>

                </div>
        )
      }
      //fin if
      else{
        return(
          <div>
             <Button variant="outlined" color="primary" type='submit' onClick={()=>{this.openFormModalEmployee()}}>
                  < AddBoxOutlinedIcon />
                </Button>
                <Divider />
            <div className="card_person1">
                    <div className="card_label_field">
                      <p className="value_column">No Tiene este perfil</p>

                    </div>
                </div>
          </div>
        )
      }
    //fin else
    }

    ReturnProfessor(){
      if(this.state.professor){
        return(
          <div>
          <div className="card_person">
                    <img className="profile_photo" src={`/img/${this.state.professors.person_photo}`} />
                    <div className="card_label_field">
                      <p className="title_column">Año de inicio </p>
                      <p className="value_column">{this.state.professors.profe_year_start}</p>
                     <p className="title_column">Status </p>
                      <p className="value_column">{this.state.professors.profe_status}</p>
                     <p className="title_column">Observaciones </p>
                      <p className="value_column">{this.state.professors.profe_observation}</p>

                    </div>
                </div>

                </div>
        )
      }
      //fin if
      else{
        return(
          <div>
          <Button variant="outlined" color="primary" type='submit' onClick={()=>{this.openFormModalProfessor()}}>
               < AddBoxOutlinedIcon />
             </Button>
             <Divider />
            <div className="card_person1">
                    <div className="card_label_field">
                      <p className="value_column">No Tiene este perfil</p>

                    </div>
                </div>
          </div>
        )
      }
    //fin else
    }

    handleChange (event, value) {
        this.setState({ value });
      };
    openFormModalStudent(){
      this.setState({form_student:true});
    }
    openFormModalEmployee(){
      this.setState({form_employee:true});
    }
    openFormModalProfessor(){
      this.setState({form_professor:true});
    }
    openSnack(param){
      this.setState({snack_open: true, message_success: param});
    }
    closeSnack(){
      this.setState({snack_open: false});
    }


render(){

  const { classes } = this.props;
  const {persons, students, value, form_student, form_employee, form_professor, snack_open, message_success} = this.state;
  var showModal, showSnack, showPhoto;
  if (persons.person_photo===undefined) {
    showPhoto = null;
  }
  else {
    showPhoto = <img className="profile_photo" src={`/img/${persons.person_photo}`} />;
  }
  if (form_student) {
    showModal = <MidModal >
                  {{onHandleSubmit: this.updateState,
                    form: <FormStudent />,
                    props_form: {onSuccess:this.openSnack, person_id: persons.person_id}
                  }}
                </ MidModal>
  }
  if (form_employee) {
    showModal = <MidModal >
                  {{onHandleSubmit: this.updateState,
                    form: <FormEmployees />,
                    props_form: {onSuccess:this.openSnack, person_id: persons.person_id}
                  }}
                </ MidModal>
  }
  if (form_professor) {
    showModal = <MidModal >
                  {{onHandleSubmit: this.updateState,
                    form: <FormProfessor />,
                    props_form: {onSuccess:this.openSnack, person_id: persons.person_id}
                  }}
                </ MidModal>
  }
  if (snack_open){
        showSnack = <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={snack_open} autoHideDuration={6000} onClose={this.closeSnack}>
                       <Alert elevation={6} variant="filled" onClose={this.closeSnack} severity="success">
                         {message_success}
                       </Alert>
                     </Snackbar>
      }
  return(

    <div className={classes.root}>
      {showModal}
      {showSnack}
      <div className="breadcrumb-container">
        <ul className="breadcrumb-list">
          <li className="breadcrumb-item">
            <Link className='breadcrumb-link'to={"/"}>
                <HomeIcon className="breadcrumb-icon"/>
                Inicio
                <ChevronRightIcon className="breadcrumb-icon"/>
            </Link>
          </li>
          <li className="breadcrumb-item">
            <Link className='breadcrumb-link'to={"/personas"}>
                <ListIcon className="breadcrumb-icon"/>
                Lista de personas
                <ChevronRightIcon className="breadcrumb-icon"/>
            </Link>
          </li>
          <li className="breadcrumb-item">
            <Typography style={{color:"#666"}}>
              <PersonIcon className="breadcrumb-icon"/>
              {this.state.persons.person_idnumber}
            </Typography>
          </li>
        </ul>
      </div>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
              >
                <Tab label="Datos" />
                <Tab label="Estudiante" />
                <Tab label="Funcionario" />
                <Tab label="Profesor" />

              </Tabs>
            </AppBar>
            {value === 0 && <TabContainer>
              <div className="card_person">
                  {showPhoto}
                  <div className="card_label_field">
                    <p className="title_column">Nombres </p>
                    <p className="value_column">{persons.person_fname}</p>
                    <p className="title_column">Apellidos </p>
                    <p className="value_column">{persons.person_lastname}</p>
                    <p className="title_column">Documento </p>
                    <p className="value_column">{persons.person_idnumber}</p>
                    <p className="title_column">Sexo </p>
                    <p className="value_column">{persons.person_gender}</p>
                    <p className="title_column">Pais de Nacionalidad </p>
                    <p className="value_column">{persons.country_name}</p>
                    <p className="title_column">Dirección </p>
                    <p className="value_column">{persons.person_address}</p>
                    <p className="title_column">Tipo de sangre </p>
                    <p className="value_column">{persons.person_bloodtype}</p>
                  </div>
              </div>
              <div className="card_person1">
                  <div className="card_label_field">
                  <ContactPersonsList cperson={this.props.match.params.id} />

                    {/* <p className="title_column">Situación Ocupacional </p>
                    <p className="value_column">---</p>
                    <p className="title_column">Estado Civil</p>
                    <p className="value_column">---</p> */}
                  </div>
              </div>
              <div className="card_person1">

                  <div className="card_label_field">
                    <UserPerson person={this.props.match.params.id} person_data={this.state.persons} />
                  </div>
              </div>
              </TabContainer>}
            {value === 1 && <TabContainer>

                {this.ReturnStudent()}

              </TabContainer>}
            {value === 2 && <TabContainer>{this.ReturnEmployee()}</TabContainer>}
            {value === 3 && <TabContainer>{this.ReturnProfessor()}</TabContainer>}

          </div>

  )
}


}

PersonShow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PersonShow);
