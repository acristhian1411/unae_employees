import React, { Component, lazy } from "react";
import MidModal from "../Modals/MidModal";
import FormTicketDetailType from "./FormTicketDetailType";
import Paginator from "../Paginators/Paginator";
import DialogDestroy from "../Dialogs/DialogDestroy";
// const FormTicketDetailType = lazy(()=> import('./FormTicketDetailType'));
// const DialogDestroy = lazy(()=> import('../Dialogs/DialogDestroy'));
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  Snackbar,
  CircularProgress,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

export default class TicketDetailTypes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      message_success: "",
      tdtypes: [],
      filterTicketDetailTypes: [],
      tdtype_id: 0,
      tdtype: [],
      edit: false,
      _new: false,
      open: false,
      paginator: [],
      rowsPerPage: 5,
      order: "asc",
      orderBy: "tdtype_desc",
      page_lenght: 0,
      page: 1,
      time_out: false,
      url: "/api/tdtypes/",
      busqueda: false,
      permissions: {
        tdtypes_index: null,
        tdtypes_show: null,
        tdtypes_store: null,
        tdtypes_update: null,
        tdtypes_destroy: null,
      },
    };
    // this.handleClickOpen = (data) =>{
    //   this.setState({open: true, tdtype: data});
    // }
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.getObject = async (
      perPage,
      page,
      orderBy = this.state.orderBy,
      order = this.state.order
    ) => {
      let res;
      if (this.state.busqueda) {
        res = await axios.get(`${this.state.url}`);
      } else {
        res = await axios.get(`${this.state.url}`);
      }
      let { data } = await res;
      // console.log(res.data.last_page);
      if (this._ismounted) {
        this.setState({
          tdtypes: data,
          paginator: res.data,
          filterTicketDetailTypes: data.data,
          open: false,
          page_lenght: res.data.last_page,
          page: res.data.current_page,
        });
      }
    };
    this.updateState = () => {
      this.setState({
        edit: false,
        _new: false,
        open: false,
      });
      this.getObject(this.state.rowsPerPage, this.state.page);
    };

    this.destroy = this.destroy.bind(this);
    this.createSortHandler = this.createSortHandler.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
    this.openSnack = this.openSnack.bind(this);
    this.search = this.search.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    // this.getObject(this.state.rowsPerPage, this.state.page);
    var permissions = [
      "tdtypes_index",
      "tdtypes_show",
      "tdtypes_store",
      "tdtypes_update",
      "tdtypes_destroy",
    ];
    axios
      .get(`/api/roles_has_permission`, {
        params: {
          permission: permissions,
        },
      })
      .then((res) => {
        // console.log('algo');
        this.setState({ permissions: res.data });
        if (res.data !== false) {
          this.getObject(this.state.rowsPerPage, this.state.page);
        }
      });
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  openSnack(param) {
    // console.log(param);
    this.setState({ snack_open: true, message_success: param });
  }
  handleModal() {
    this.setState({ _new: true });
  }
  handleClickOpen(data) {
    this.setState({ open: true, tdtype: data });
  }
  searchChange(e) {
    this.setState({ search: e.value });
  }
  editModal(data) {
    this.setState({ edit: true, tdtype: data });
  }
  destroy() {
    // event.preventDefault();
    axios.delete(`/api/tdtypes/${this.state.tdtype_id}`).then((res) => {
      this.setState({ snack_open: true, message_success: res.data });
      this.updateState();
    });
  }

  search(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out == false) {
      this.setState({ time_out: true });
      setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            filterTicketDetailTypes: this.state.tdtypes.data,
            paginator: this.state.tdtypes,
            page_lenght: this.state.tdtypes.last_page,
            page: 1,
            time_out: false,
            busqueda: false,
            url: `/api/tdtypes`,
          });
        } else {
          this.setState({
            url: `/api/tdtypes?tdtype_desc=${e.target.value}`,
          });
          axios
            .get(
              `${this.state.url}&sort_by=${this.state.orderBy}&order=${
                this.state.order
              }&per_page=${this.state.rowsPerPage}&page=${1}`
            )
            .then((res) => {
              this.setState({
                filterTicketDetailTypes: res.data.data,
                paginator: res.data,
                page_lenght: res.data.last_page,
                page: res.data.current_page,
                time_out: false,
                busqueda: true,
              });
            })
            .catch((error) => {
              console.log("Ingrese un valor valido");
            });
        }
      }, 3000);
    }
  }

  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }

  render() {
    const {
      snack_open,
      message_success,
      open,
      _new,
      edit,
      tdtype,
      filterTicketDetailTypes,
      rowsPerPage,
      orderBy,
      order,
      permissions,
    } = this.state;
    var paginator;
    var showModal;
    var showDialogDestroy;
    var showSnack;
    if (_new) {
      // showModal = <FormTicketDetailType onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormTicketDetailType />,
            props_form: { onSuccess: this.openSnack },
          }}
        </MidModal>
      );
    } else if (edit) {
      // showModal = <FormTicketDetailType edit ={true} tdtype={tdtype} onHandleSubmit={this.updateState}/>
      showModal = (
        <MidModal>
          {{
            onHandleSubmit: this.updateState,
            form: <FormTicketDetailType />,
            props_form: {
              edit: true,
              tdtype: tdtype,
              onSuccess: this.openSnack,
            },
          }}
        </MidModal>
      );
    }
    if (open) {
      showDialogDestroy = (
        <DialogDestroy
          index={tdtype.data.tdtype_desc}
          onClose={this.handleClose}
          onHandleAgree={this.destroy}
        />
      );
    }
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="success"
          >
            {message_success}
          </Alert>
        </Snackbar>
      );
    }
    if (filterTicketDetailTypes.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return (
      <div>
        <h2 align="center">
          Tipos de detalle de factura
          <TextField
            id="outlined-search"
            label="Buscar"
            type="search"
            variant="outlined"
            onChange={this.search}
            InputProps={{
              endAdornment: (
                <React.Fragment>
                  {this.state.time_out === true ? (
                    <CircularProgress color="inherit" size={20} />
                  ) : null}
                </React.Fragment>
              ),
            }}
          />
        </h2>
        <hr />

        <TableContainer component={Paper}>
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
                <TableCell
                  sortDirection={orderBy === "tdtype_desc" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "tdtype_desc"}
                    direction={orderBy === "tdtype_desc" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tdtype_desc");
                    }}
                  >
                    <h3>Descripcion</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell
                  sortDirection={orderBy === "tdtype_obs" ? order : false}
                >
                  <TableSortLabel
                    active={orderBy === "tdtype_obs"}
                    direction={orderBy === "tdtype_obs" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tdtype_obs");
                    }}
                  >
                    <h3>Observacion</h3>
                  </TableSortLabel>
                </TableCell>
                <TableCell colSpan="2">
                  {permissions.tdtypes_store === true && (
                    <Button
                      variant="outlined"
                      color="primary"
                      type="submit"
                      onClick={() => {
                        this.handleModal();
                      }}
                    >
                      <AddBoxOutlinedIcon />
                    </Button>
                  )}
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {/* <Paginator
          lenght={this.state.page_lenght}
          perPageChange= {this.perPageChange}
          pageChange ={this.pageChange}
          page= {this.state.page}
          /> */}
        {paginator}
        {showModal}
        {showDialogDestroy}
        {showSnack}
      </div>
    );
  }
  selectValue(event) {
    this.setState({ search: event.target.value });
  }
  renderList() {
    const {
      open,
      tdtype,
      filterTicketDetailTypes,
      rowsPerPage,
      page,
      permissions,
    } = this.state;
    if (filterTicketDetailTypes.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return filterTicketDetailTypes.map((data) => {
        return (
          <TableBody key={data.tdtype_id}>
            <TableRow>
              <TableCell>{data.tdtype_desc}</TableCell>
              <TableCell>{data.tdtype_obs}</TableCell>
              <TableCell>
                {permissions.tdtypes_update === true && (
                  <Button
                    variant="outlined"
                    color="primary"
                    type="submit"
                    onClick={() => {
                      this.editModal({ data: data });
                    }}
                  >
                    <EditIcon />
                  </Button>
                )}
              </TableCell>
              <TableCell>
                {permissions.tdtypes_destroy === true && (
                  <Button
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      this.handleClickOpen({ data: data }),
                        this.setState({ tdtype_id: data.tdtype_id });
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                )}
              </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  } //cierra el renderList
}
