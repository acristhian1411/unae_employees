import React, { Component } from "react";
import {
  Modal,
  Backdrop,
  Fade,
  InputLabel,
  TextField,
  Button,
  Snackbar,
} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { Alert } from "@material-ui/lab";

class FormTicketDetailType extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snack_open: false,
      open: true,
      setOpen: true,
      tdtype_id: 0,
      tdtype_desc: "",
      edit: false,
      errors: [],
      validator: {
        tdtype_desc: {
          message: "",
          error: false,
        },
      },
    };

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onChangeField1 = this.onChangeField1.bind(this);
    this.handleCreateObject = this.handleCreateObject.bind(this);
    this.handleUpdateObject = this.handleUpdateObject.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.closeSnack = this.closeSnack.bind(this);
  }

  componentDidMount() {
    this._ismounted = true;
    if (this.props.edit) {
      var id = this.props.tdtype.data.tdtype_id;
      axios.get("/api/tdtypes/" + id).then((response) => {
        if (this._ismounted) {
          this.setState({
            tdtype_id: response.data.data.tdtype_id,
            tdtype_desc: response.data.data.tdtype_desc,
            tdtype_obs: response.data.data.tdtype_obs,
          });
        }
      });
    }
  }
  componentWillUnmount() {
    this._ismounted = false;
  }
  closeSnack() {
    this.setState({ snack_open: false });
  }
  handleOpen() {
    this.setState({
      setOpen: true,
    });
  }

  handleClose() {
    this.setState({
      open: false,
    });
    this.props.onHandleSubmit();
  }

  onChangeField1(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
    this.props.changeAtribute(true);
  }

  handleCreateObject(e) {
    e.preventDefault();
    var { tdtype_desc, tdtype_obs } = this.state;
    var validator = { tdtype_desc: { error: false, message: "" } };
    var object_error = {};
    const object = {
      tdtype_desc: this.state.tdtype_desc.toUpperCase(),
      tdtype_obs: this.state.tdtype_obs.toUpperCase(),
    };
    const { history } = this.props;
    axios
      .post("/api/tdtypes", {
        tdtype_desc: object.tdtype_desc,
        tdtype_obs: object.tdtype_obs,
      })
      .then((res) => {
        this.props.onSuccess(res.data);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  handleUpdateObject(e) {
    e.preventDefault();
    var { tdtype_desc, tdtype_obs } = this.state;
    var validator = { tdtype_desc: { error: false, message: "" } };
    var object_error = {};
    const object = {
      tdtype_id: this.state.tdtype_id,
      tdtype_desc: this.state.tdtype_desc.toUpperCase(),
      tdtype_obs: this.state.tdtype_obs.toUpperCase(),
    };
    const id = object.tdtype_id;
    axios
      .put(`/api/tdtypes/${id}`, {
        tdtype_desc: object.tdtype_desc,
        tdtype_obs: object.tdtype_obs,
      })
      .then((res) => {
        this.props.onSuccess(res.data.message);
        this.props.changeAtribute(false);
        this.handleClose();
      })
      .catch((error) => {
        // console.log(Object.keys(error.response.data.errors));
        this.setState({ snack_open: true });
        var errores = Object.keys(error.response.data.errors);
        for (var i = 0; i < errores.length; i++) {
          object_error = {
            ...object_error,
            [errores[i]]: {
              error: true,
              message: error.response.data.errors[errores[i]][0],
            },
          };
        }
        this.setState({
          validator: {
            ...validator,
            ...object_error,
          },
        });
      });
  }

  hasErrorFor(field) {
    return !!this.state.errors[field];
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  botonEdit() {
    if (this.props.edit) {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleUpdateObject}
        >
          Actualizar
        </Button>
      );
    } else {
      return (
        <Button
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.handleCreateObject}
        >
          Guardar
        </Button>
      );
    }
  }

  render() {
    const { classes } = this.props;
    const { snack_open, validator } = this.state;
    var showSnack;
    if (snack_open) {
      showSnack = (
        <Snackbar
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          open={snack_open}
          autoHideDuration={6000}
          onClose={this.closeSnack}
        >
          <Alert
            elevation={6}
            variant="filled"
            onClose={this.closeSnack}
            severity="error"
          >
            {validator["tdtype_desc"]["error"] ? (
              <li>Error: {validator["tdtype_desc"]["message"]}</li>
            ) : (
              ""
            )}
          </Alert>
        </Snackbar>
      );
    }
    return (
      <div>
        <h3>Formulario de Cargos</h3>
        <hr />
        <form>
          <div className="form-group">
            <TextField
              id="outlined-full-width"
              label="Descripcion"
              error={this.state.validator.tdtype_desc.error}
              helperText={this.state.validator.tdtype_desc.message}
              style={{ margin: 8 }}
              name="tdtype_desc"
              value={this.state.tdtype_desc}
              onChange={this.onChangeField1}
              fullWidth
              margin="normal"
              variant="outlined"
            />
            <TextField
              id="outlined-full-width"
              label="Observacion"
              style={{ margin: 8 }}
              name="tdtype_obs"
              value={this.state.tdtype_obs}
              onChange={this.onChangeField1}
              fullWidth
              margin="normal"
              variant="outlined"
            />
          </div>
          {this.botonEdit()}
          {showSnack}
          <Button
            variant="outlined"
            color="secondary"
            startIcon={<ExitToAppIcon />}
            onClick={this.handleClose}
            onClick={this.handleClose}
          >
            Cancelar
          </Button>
        </form>
      </div>
    );
  }
}
// export default withRouter( withStyles(styles, { withTheme: true })(FormTicketDetailType));
export default FormTicketDetailType;
