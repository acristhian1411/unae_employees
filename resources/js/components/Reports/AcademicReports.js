import React, {Component} from 'react';
import {Grid, FormControl, InputLabel, Select, TextField, Table, TableBody,
  TableCell,TableContainer, TableHead, TableRow, TablePagination,
  Tooltip, IconButton, Paper, Slide, CircularProgress} from '@material-ui/core';
import {Autocomplete} from '@material-ui/lab';
import {CareerType} from '../ArraysCalls/Careers';
import VisibilityIcon from "@material-ui/icons/Visibility";
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {SemesterInLetter} from '../ArraysCalls/SemesterNumber';
import StudentScores from '../Students/StudentScores';
import ControlPlanilla from '../Subjects/ControlPlanilla';

export default class AcademicReports extends Component {
  constructor(props) {
    super(props);
    this.state = {
      report_type: 2,
      to_date: '',
      faculties:[],
      careers:[],
      semester:[],
      enroll_year: 2021,
      time_out: false,
      career_multipletime_out: false,
      career_name: "",
      career_id:"",
      facu_name: "",
      facu_id: "",
      evals: [],
      persons: [],
      id: '',
      person_name: '',
      validator: {
        id: { message: "", error: false },
      },
      careers2: [],
      select_multiple_career: [],
    }
    this.search = this.search.bind(this);
    this.searchFieldChange = this.searchFieldChange.bind(this);
  }
  exportToPDF(param){
    open(`/reportes/academico?type=habilitados&to_date=${this.state.to_date}&career_id=${this.state.career_id}&eval_id=${param}`);
  }
  califExportToPDF(){
    var careers_ids = this.state.select_multiple_career.map((c) => {
      if(c.career_pid !== 0){
        return c.career_pid;
      }else{
        return c.career_id;
      }
    });
    if (this.state.id != null && this.state.select_multiple_career.length > 0) {
       open(`/reportes/calificaciones?type=evstu&career_id=${careers_ids}&person_id=${this.state.id}`);
    }
  }
  searchFieldChange(event) {
    var e = event;
    this.setState({
      person_name: e.target.value,
      id: null,
      time_out: true
    });
    e.persist();
    // if (this.state.time_out == false) {
    //   this.setState({
    //     time_out: true,
    //   });
    //   setTimeout(() => {
    if (e.target.value === "") {
      axios.get("api/persons").then((res) => {
        this.setState({
          persons: res.data.data,
          time_out: false,
        });
      });
    } else {
      axios
        .get(
          `/api/persons-search?search=${e.target.value}`
        )
        .then((res) => {
          this.setState({
            persons: res.data.data,
            time_out: false,
          });
        })
        .catch((error) => {
          this.setState({time_out: false});
          console.log("Ingrese un valor valido");
        });
    }
    //   }, 2000)
    // }
  }
  selectsChange(e, values, nameValue, nameLabel, route, obj) {
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
    if(nameValue==="facu_id"){
      this.setState({ [nameValue]: values.value, [nameLabel]: values.label, career_id: '', career_name: '' });
      axios.get("/api/evalstudent-careers",{params: {facu_id: values.value, to_date: this.state.to_date}}).then(res=>{
        this.setState({evals: res.data});
      })
      axios.get(`/api/faculty_careers/${values.value}`).then(res=>{
        this.setState({careers:res.data})
      })
    }else if(nameValue==="career_id"){
      this.setState({ [nameValue]: values.value, [nameLabel]: values.label });
      axios.get("/api/evalstudent-careers",{params: {enroll_year: this.state.enroll_year, career_id: values.value, to_date: this.state.to_date}}).then(res=>{
        this.setState({evals: res.data});
      })
    }else if(nameValue==="id"){
      axios.get(`/api/student_careers/${values.value}`).then((res)=>{
        if (res.data.length > 0){
          this.setState({
            careers2: res.data,
            // career_id: res.data[0].career_id,
            // career_name: Capitalize(res.data[0].career_name.split("-")[0])});
          });
        }
      })
    }
  }
  search(event, timeout) {
    var e = event;
    //funciona pero falta validar error 500
    this.setState({ [e.target.name]: e.target.value });
    e.persist();
    if (this.state[timeout] == false) {
      this.setState({ [timeout]: true });
      setTimeout(() => {
          if (e.target.value === "") {
            this.setState({
              careers: [],
              [timeout]: false,
              url: `/api/careers`,
            });
            this.updateState();
          } else {
            this.setState({
              url: `/api/careers-search?filter[career_pid]=0&filter[career_name]=${e.target.value}`,
            });
            axios
              .get(`${this.state.url}`)
              .then((res) => {
                this.setState({
                  careers: res.data.data,
                  [timeout]: false,
                });
              })
              .catch((error) => {
                console.log("Ingrese un valor valido");
              });
        }
      }, 1500);
    }
  }
  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }
  changeFields(e){
    this.setState({[e.target.name]:e.target.value});
  }  Persons() {
      return this.state.persons.map((data) => ({
        label: Capitalize(`${data.person_idnumber} - ${data.person_fname} ${data.person_lastname}`),
        value: data.person_id,
      }));
    }
    selectValue(name, label) {
      return {
        label: this.state[name],
        value: this.state[label],
      };
    }
  selectReturn2() {
    return this.state.semester.map((data) => ({
      label: `${SemesterInLetter(data.sems_number)}${data.sems_name.toLowerCase().includes("online")?' - Online':
        ["A", "B", "C", "D", "E"].includes(data.sems_name.split(' ')[data.sems_name.split(' ').length - 1])?
        ' - '+data.sems_name.split(' ')[data.sems_name.split(' ').length - 1]:
      ''}`,
      value: data.sems_id,
      year: data.sems_year,
      career_name: data.career_name,
      sems_number: data.sems_number,
      career_id: data.career_id,
      facu_name: data.facu_name,
      facu_career_id: data.facu_career_id,
      unit_name: data.unit_name,
      unit_id: data.unit_id,
      unit_type_id: data.unit_type_id,
      tcareer_description: data.tcareer_description,
      year_in_letter: SemesterYearInLetter(data.sems_year),
    }));
  }
  selectReturn3() {
    return this.state.careers.map((data) => ({
      label: `(${data.career_code}) ${data.career_name}`,
      value: data.career_id,
      career_type: data.tcareer_description,
      unit_type: data.unit_type_id,
      career_type_translate: CareerType(data.tcareer_description)
    }));
  }selectReturn4() {
    return this.state.faculties.map((data) => ({
      label: `(${data.facu_code}) ${data.facu_name}`,
      value: data.facu_id,
      unit_name: data.unit_name,
    }));
  }
  componentDidMount() {
    this._ismounted = true;
    this.props.changeTitlePage("INFORMES ACADEMICOS");
    this.setState({to_date:this.getCurrentDate()});
  }
  componentDidUpdate(prevProps, prevState){
    if(prevState.report_type !== this.state.report_type){
      if(this.state.report_type === '1'){
        axios.get(`api/faculties?per_page=30`).then(res=>{
          this.setState({faculties: res.data.data});
        })
      }else{
        // console.log("facultades else");
        // console.log(this.state.report_type);
      }  
    }
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  Careers() {
    return (this.state.careers2.map(data => ({
      label: data.career_name.split('-')[0],
      value: data.career_id,
      career_id: data.career_id,
      career_pid: data.career_pid,
     })));
  }
  selectsMultipleChange(e, values) {
    this.setState({ select_multiple_career: values });
  }
  render() {
    return(
      <div>
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={3}>
            <Grid item xs={"auto"}>
              <FormControl
                variant="outlined"
                size="small"
                // style={{ width: '170px' }}
                inputlabelprops={{
                  shrink: true,
                }}
              >
                <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: 'white', padding: '0 5px'}} shrink={true}>Tipo de informe</InputLabel>
                <Select
                  native
                  label="Tipo de informe"
                  value={this.state.report_type}
                  inputProps={{
                    name:"Tipo de informe",
                    id: "demo-simple-select-helper",
                  }}
                  onChange={(e)=>{
                    this.setState({report_type: e.target.value})
                  }}
                >
                        <option value={1}>{"Lista de habilitados"}</option>
                        <option value={2}>{"Informe de calificaciones"}</option>
                        <option value={3}>{"Control de planillas"}</option>
                      {/*  <option value={2}>{"Otro tipo de reporte"}</option>*/}
                  })}

                </Select>
              </FormControl>
            </Grid>
            {parseInt(this.state.report_type) === 1?

              [ <Slide
                direction="left"
                in={parseInt(this.state.report_type) === 1}
                mountOnEnter
                unmountOnExit
                >
                  <Grid item xs={"auto"}>
                    <TextField
                      id="date"
                      label="Fecha"
                      type="date"
                      name="to_date"
                      variant="outlined"
                      size="small"
                      // defaultValue="2017-05-24"
                      value={this.state.to_date}
                      onChange={(e)=>{this.changeFields(e)}}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Grid>
                </Slide>,
                <Slide
                  direction="left"
                  in={parseInt(this.state.report_type) === 1}
                  mountOnEnter
                  unmountOnExit
                >
                  <Grid item xs={"auto"}>
                  <div className="form-group">
                    <Autocomplete
                      size="small"
                      id="combo-box-demo"
                      disableClearable
                      options={[
                        this.selectValue("facu_name", "facu_id"),
                        ...this.selectReturn4(),
                      ]}
                      filterSelectedOptions
                      getOptionLabel={(option) => option.label}
                      groupBy={(option)=>option.unit_name}
                      onChange={(e, values) =>
                        this.selectsChange(
                          e,
                          values,
                          "facu_id",
                          "facu_name",
                          "facu/career",
                          "careers"
                        )
                      }
                      loading={this.state.time_out}
                      value={this.selectValue("facu_name", "facu_id")}
                      getOptionSelected={(option, value) =>
                        option.value === value.value
                      }
                      style={{ minWidth: 300 }}
                      fullWidth
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          name="facu_name"
                          fullWidth
                          label="Facultad"
                          className="textField"
                          variant="outlined"
                          InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                              <React.Fragment>
                                {this.state.career_load === true ? (
                                  <CircularProgress color="inherit" size={20} />
                                ) : null}
                                {params.InputProps.endAdornment}
                              </React.Fragment>
                            ),
                          }}
                        />
                      )}
                    />
                  </div>
                  </Grid>
                </Slide>,
                <Slide
                  direction="left"
                  in={parseInt(this.state.report_type) === 1}
                  mountOnEnter
                  unmountOnExit
                >
                  <Grid item xs={"auto"}>
                  <div className="form-group">
                    <Autocomplete
                      size="small"
                      id="combo-box-demo"
                      options={[
                        this.selectValue("career_name", "career_id"),
                        ...this.selectReturn3(),
                      ]}
                      filterSelectedOptions
                      getOptionLabel={(option) => option.label.split('-')[0]}
                      groupBy={(option)=>option.career_type_translate}
                      onChange={(e, values) =>{
                        if(values === null){
                          // console.log("aplicar los cambios");
                          this.setState({career_name: '', career_id:''})
                          axios.get("/api/evalstudent-careers",{params: {facu_id: this.state.facu_id, to_date: this.state.to_date}}).then(res=>{
                            this.setState({evals: res.data});
                          })
                        }else{
                           this.selectsChange(
                              e,
                              values,
                              "career_id",
                              "career_name",
                              "semesters/career",
                              "semester"
                            )}
                        }
                                             
                      }
                      loading={this.state.time_out}
                      value={this.selectValue("career_name", "career_id")}
                      getOptionSelected={(option, value) =>
                        option.value === value.value
                      }
                      style={{ minWidth: 300 }}
                      fullWidth
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          name="career_name"
                          fullWidth
                          label="Carrera"
                          className="textField"
                          variant="outlined"
                          InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                              <React.Fragment>
                                {this.state.career_load === true ? (
                                  <CircularProgress color="inherit" size={20} />
                                ) : null}
                                {params.InputProps.endAdornment}
                              </React.Fragment>
                            ),
                          }}
                        />
                      )}
                    />
                  </div>
                  </Grid>
                </Slide>
              ]
              :parseInt(this.state.report_type) === 2?[
                <Grid item xs={4}>
                  <Slide
                    direction="left"
                    in={parseInt(this.state.report_type) === 2}
                    mountOnEnter
                    unmountOnExit
                  >
                    <Autocomplete
                      size="small"
                      id="combo-box-demo"
                      className="textfield-home"
                      disableClearable
                      options={[
                        this.selectValue("person_name", "id"),
                        ...this.Persons(),
                      ]}
                      filterSelectedOptions
                      getOptionLabel={(option) => option.label}
                      onChange={(e, values) => this.selectsChange(e, values, "id", "person_name")}
                      value={this.selectValue("person_name", "id")}
                      loading={this.state.time_out}
                      getOptionSelected={(option, value) =>
                        option.value === value.value
                      }
                      style={{
                        margin: 0,
                      }}
                      fullWidth
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          autoFocus={this.state.search}
                          error={this.state.validator.id.error}
                          helperText={this.state.validator.id.message}
                          label="Ingrese nombre, número de cédula, o algun contacto."
                          color="secondary"
                          variant="outlined"
                          fullWidth
                          // onChange={this.searchFieldChange}
                          onKeyDown={(e) =>{
                            if(e.key === 'Enter'){
                              this.searchFieldChange(e)
                            }
                          }}
                          InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                              <React.Fragment>
                                {" "}
                                {this.state.time_out === true ? (
                                  <CircularProgress color="inherit" size={20} />
                                ) : null}{" "}
                                {params.InputProps.endAdornment}{" "}
                              </React.Fragment>
                            ),
                          }}
                        />
                      )}
                    />
                  </Slide>
                </Grid>,
                <Grid item xs={4}>
                <Slide
                  direction="left"
                  in={parseInt(this.state.report_type) === 2}
                  mountOnEnter
                  unmountOnExit
                >
                <Autocomplete
                  multiple
                  size="small"
                  id="combo-box-demo"
                  disableClearable
                  options={[...this.Careers()]}
                  filterSelectedOptions
                  getOptionLabel={(option) => option.label}
                  onChange={(e, values) =>
                    this.selectsMultipleChange(e, values)}
                  value={this.state.select_multiple_career}
                  loading={this.state.career_multipletime_out}
                  getOptionSelected={(option, value)=>option.value === value.value}
                  fullWidth
                  renderInput={(params) => <TextField {...params}
                                                      label="Seleccione Carreras *"
                                                      name="career_name"
                                                      variant="outlined"
                                                      color="secondary"
                                                      fullWidth
                                                      onChange={this.searchFieldChange}
                                                      InputProps={{
                                                        ...params.InputProps,
                                                        endAdornment: (
                                                          <React.Fragment>
                                                            {this.state.career_multipletime_out === true ? <CircularProgress color="inherit" size={20} /> : null}
                                                            {params.InputProps.endAdornment}
                                                          </React.Fragment>
                                                        ),
                                                      }}
                                                      />}
                />
                </Slide>
                </Grid>,
                <Grid item xs={1}>
                  <Slide
                    direction="left"
                    in={parseInt(this.state.report_type) === 2}
                    mountOnEnter
                    unmountOnExit
                  >
                    <IconButton aria-label="search" onClick={()=>this.califExportToPDF()}>
                      <PictureAsPdfIcon />
                    </IconButton>
                  </Slide>
                </Grid>
              ]
              :parseInt(this.state.report_type) === 3?[
                <Slide
                direction="left"
                in={parseInt(this.state.report_type) === 3}
                mountOnEnter
                unmountOnExit
                >
                  <ControlPlanilla />
                </Slide>
              ]
              :"Insertar parametros de busqueda para la lista de informes"
            }
          </Grid>
        </Grid>
        {parseInt(this.state.report_type) == 1?
          <TableContainer component={Paper}>
            <Table aria-label="simple table" option={{ search: true }}>
                <TableHead>
                  <TableRow>
                  <TableCell></TableCell>
                    <TableCell><h3>Carrera</h3></TableCell>
                    <TableCell><h3>Curso</h3></TableCell>
                    <TableCell><h3>Materia</h3></TableCell>
                    <TableCell><h3>Tipo de Evaluacion</h3></TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {this.renderList()}
                </TableBody>
              </Table>
            </TableContainer>

          :parseInt(this.state.report_type) == 2?
            <StudentScores person={this.state.id} career={this.state.select_multiple_career} />
          :parseInt(this.state.report_type) == 3?
          ''
          :
          "insertar tablas para otro tipo de reporte"
        }
      </div>
    );
  }
  renderList() {
    const { open, evals, rowsPerPage, page } = this.state;
    if (evals.length === 0) {
      return (
        <TableRow>
          <TableCell></TableCell>
          <TableCell>{"No contiene datos"}</TableCell>
        </TableRow>
      );
    } else {
     return evals.map((data)=>{
       return (
         <TableRow>
         <TableCell>
           <Tooltip title="Reporte" style={{margin: 0, padding: 0}}>
             <IconButton aria-label="search" onClick={()=>{this.exportToPDF(data.subeval_id)}}>
               <PictureAsPdfIcon />
             </IconButton>
           </Tooltip>
         </TableCell>
          <TableCell>{Capitalize(data.career_name)}</TableCell>
          <TableCell>{`${SemesterInLetter(data.sems_number)}${data.sems_name.toLowerCase().includes("online")?' - Online':
            ["A", "B", "C", "D", "E"].includes(data.sems_name.split(' ')[data.sems_name.split(' ').length - 1])?
            ' - '+data.sems_name.split(' ')[data.sems_name.split(' ').length - 1]:
          ''}`}</TableCell>
           <TableCell>{`(${data.subj_code}) ${Capitalize(data.subj_name)}`}</TableCell>
           <TableCell>{Capitalize(data.evaltype_name)}</TableCell>
         </TableRow>
       );
     });

    }
  } //cierra el renderList
}
