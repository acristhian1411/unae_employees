import React, {Component} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
  IconButton,
  Grid
} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import CloseIcon from '@material-ui/icons/Close';
import Paginator from "../Paginators/Paginator";
import {  Autocomplete } from "@material-ui/lab";
// var f = new Date();
// var hoy = `${f.getFullYear()}-${(f.getMonth() + 1)}-${f.getDate()}`;
export default class EventsReports extends Component{
  constructor(props) {
    super(props);
    this.state = {
      from_date: '',
      to_date: '',
      product_desc: '',
      product_id: 0,
      cate_id: 3,
      cate_desc: 'Congresos',
      events: [],
      event_desc: 'Todos',
      event_id: '',
      asistevents: [],
      categories:[],
      semesters_grado:[
        {carrera: 'Lic. Relaciones públicas e institucionales', sems_id:3393},
        {carrera: 'Ingenieria comercial', sems_id:2877},
        {carrera: 'Lic. Marketing', sems_id:3356},
        {carrera: 'Lic. Administración', sems_id:3080},
        {carrera: 'Lic. Adm. Empresas agreopecuarias', sems_id:3624},
        {carrera: 'Lic. Comercio Internacional', sems_id:'3281,3289'},
        {carrera: 'Contaduria pública', sems_id:3222},
        {carrera: 'Lic. Hoteleria y turismo', sems_id:3327},

        {carrera: 'Lic. Analisis de sistemas informaticos', sems_id:1138},
        {carrera: 'Lic. Diseño de modas', sems_id:1335},
        {carrera: 'Lic. Diseño grafico', sems_id:1239},
        {carrera:'Derecho', sems_id:1840},
        {carrera:'Psicopedagogia', sems_id:1938},
        {carrera:'Trabajo social', sems_id:2054},

        {carrera:'Lic. Educación física', sems_id:2335},
        {carrera:'Prof. Educación física', sems_id:2343},
        {carrera:'Lic. Educación inicial', sems_id:2448},
        {carrera:'Prof. Educación inicial', sems_id:2445},
        {carrera:'Lic. Ciencias sociales', sems_id:2383},
        {carrera:'Prof. Ciencias sociales', sems_id:2380},
        {carrera:'Prof. Escolar Básica', sems_id:'2187, 2200, 2188'},
      ],
      semesters_posgrado:[
        {carrera: 'Espec. Metodologia de la investigacion', sems_id: 2084},
{carrera: 'Espec. Educacion inclusiva', sems_id: 2072},
{carrera: 'Habilitación Pedagógica para Egresados de Educ. Superior', sems_id: '2112, 2116'},
{carrera: 'Tecnico docente en gestion educativa', sems_id: 2571},
{carrera: 'Tecnico docente en evaluacion educativa', sems_id: 2560},
{carrera: 'Tecnico docente en integracion pedagogica de las tics', sems_id: 2556},
{carrera: 'Doctorado en Contabilidad y Auditoria', sems_id: 2480},
{carrera: 'Doctorado en Educacion y Desarrollo Humano', sems_id: 2494},
{carrera: 'Maestria en derecho penal y procesal penal', sems_id: 2545},
{carrera: 'Maestria en auditoria', sems_id: '2538,2540'},
{carrera: 'Maestria en administracion de negocios', sems_id: 2703},
{carrera: 'Maestria en ciencias forenses', sems_id: '2710,2713'},
{carrera: 'Espec. en ciencias forenses', sems_id: 2686},
{carrera: 'Espec. en Auditoria', sems_id: 2665},
{carrera: 'Espec. en neurociencia y cognicion', sems_id: 2524},
{carrera: 'Espec. Didactica superior universitaria', sems_id: '2650, 2725'}

      ],
      sems_id: '',
      career_name: '',
      incomes: [],
      rowsPerPage: 10,
      paginator: [],
      page_lenght: 0,
      page: 1,
      order: "desc",
      orderBy: "ticket_date",
      product_load: false,
      category_load: false
    }
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
  }
  changeFields(e){
    this.setState({[e.target.name]:e.target.value});
  }
  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }
  componentDidMount(){
    axios.get('/api/categories').then(res=>{
      this.setState({categories: res.data.data});
      // console.log(res.data.data);
    });
    axios.get('/api/events').then(res=>{
      this.setState({events: res.data.data});
    });
     // axios.get('/api/ticket_details/events', {params:
     //     {from: 'frontend',
     //      type:'events',
     //      product_id: '',
     //      per_page: this.state.perPage,
     //      page: this.state.page,
     //      sort_by:this.state.orderBy,
     //      order: this.state.order
     //     }
     //   }).then(res=>{
     //     this.setState({incomes: res.data.data,
     //                    paginator: res.data,
     //                    page_lenght: res.data.last_page
     //                  });
     // })
  }
  getObject(perPage = this.state.perPage,
            page = this.state.page,
            orderBy = this.state.orderBy,
            order = this.state.order,
            product_id = this.state.product_id,
            sems_id = this.state.sems_id
            ){
    axios.get('/api/ticket_details/events', {params:
        {from: 'frontend',
         type:'events',
         product_id,
         sems_id,
         // from_date: this.state.from_date,
         // to_date: this.state.to_date,
         per_page: perPage,
         page: page,
         sort_by:orderBy,
         order: order
        }
      }).then(res=>{
        this.setState({incomes: res.data.data,
                       paginator: res.data,
                       page_lenght: res.data.last_page
                    });
    })
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  exportToPDF(type=1){
    var referencia = '';
    if(this.state.event_id !== ''){
      referencia = this.state.event_id;
    }else if(this.state.product_id !== ''){
      referencia = this.state.product_id;
    }else{
      return false;
    }
    switch (type) {
    case 1:
      open(`/reportes/ingresos?type=event&product_id=${referencia}&sems_id=${this.state.sems_id}`);
      break;
    case 2:
      open(`/reportes/ingresos?type=event2&product_id=${referencia}&sems_id=${this.state.sems_id}`);
      break;
    default:
        open(`/reportes/ingresos?type=event&product_id=${referencia}&sems_id=${this.state.sems_id}`);
    }
    
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  selectValue2(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }
  selectReturn(object = "events") {
    if (object === "categories") {
      return this.state[object].map((data) => ({
        label: data.cate_desc,
        value: data.cate_id,
      }));
    } else if(object === "semesters"){
      return this.state.semesters_grado.map((data)=>({
        label: data.carrera,
        value: data.sems_id
      }))
    }else if(object === "semesters2"){
      return this.state.semesters_posgrado.map((data)=>({
        label: data.carrera,
        value: data.sems_id
      }))
    }  else {
      return this.state[object].map((data) => ({
        label: data.product_desc,
        value: data.product_id,
      }));
    }

  }
  selectsChange(e, values, from = "product") {
    // console.log(values);
    if (from == "product") {
      this.setState({
        product_desc: values.label,
        product_id: values.value,
      });
      if(values.value === 50013){
        this.setState({asistevents:[{}]});
      }else if(values.value === 50013){

      }else{
        axios.get('/api/sub-items/'+values.value).then(res=>{
          this.setState({asistevents: res.data});
        });  
      }
    }else if (from == "semesters" || from == "semesters2") {
      this.setState({sems_id: values.value, career_name: values.label});

    }else if (from == "asistevent") {
      console.log("llega a asistevent 258");
      console.log(values.value);
      this.setState({
        event_desc: values.label,
        event_id: values.value,
      });
    }else if (from == "categories") {
      this.setState({
        cate_desc: values.label,
        cate_id: values.value,
        events: [],
        product_id: '',
        product_desc: '',
        event_id: '',
        event_desc: 'Todos'
      });
      axios.get(`/api/products?cate_id=${values.value}`).then(res=>{
        this.setState({events:res.data.data});
      });
    }

    if(from === "semesters"||from === "semesters2"){
      var { rowsPerPage, page, orderBy, order } = this.state;
      this.getObject(rowsPerPage, 1, orderBy, order, this.state.product_id, values.value);
    }else{
      var { rowsPerPage, page, orderBy, order } = this.state;

    this.getObject(rowsPerPage, 1, orderBy, order, values.value);  
    }

    
    //getObjects where carsubj_id = career_id
  }
  searchFieldChange(event, to, id, route, object, parent = false) {
    var e = event;
    var time_out = to;
    var id_name = id;
    this.setState({ [e.target.name]: e.target.value, [id_name]: null });
    e.persist();
    if (this.state[time_out] == false) {
      this.setState({ [time_out]: true });
      setTimeout(() => {
        if (e.target.value === "") {
          //getObject
          // axios.get("api/" + route).then((res) => {
          //   this.setState({ [object]: res.data.data, [time_out]: false });
          // });
          this.setState({ [time_out]: false });
          this.getObject();
        } else {
          if (object === "products") {
            var url = `/api/products/search/${e.target.value}?cate_id=${this.state.cate_id}`;
            // if (parent) {
            //   url = `/api/careers-search?filter[career_name]=${e.target.value}`;
            // }else {
            //   url = `/api/careers-search?filter[career_pid]=0&filter[career_name]=${e.target.value}`;
            // }
            axios.get(url)
              .then((res) => {
                this.setState({
                  [object]: res.data.data,
                  [time_out]: false,
                });
              })
              .catch((error) => {
                console.log("Ingrese un valor valido");
                this.setState({ [time_out]: false, [e.target.name]: "" });
              });
          }else {
           // console.log("buscar por ruta");
           axios.get("api/" + route + e.target.value).then((res) => {
             this.setState({ [object]: res.data.data, [time_out]: false });
           }).catch(error=>{
             this.setState({ [time_out]: false });
           });
          }

        }
      }, 3000);
    }
  }
  render(){
    var paginator;
    const {orderBy, order} = this.state;
    if (this.state.incomes.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return(
      <div>
        {/*<TextField
          id="date"
          label="Desde"
          type="date"
          name="from_date"
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="date"
          label="Hasta"
          type="date"
          name="to_date"
          // defaultValue="2017-05-24"
          value={this.state.to_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />*/}
        <Grid container spacing={1}>
          <Grid container item xs={12} spacing={1}>
            <Grid item xs={4}>
            <Autocomplete
              id="combo-box-demo"
              disableClearable
              filterSelectedOptions
              loading={this.state.category_load}
              options={[
                this.selectValue2("cate_desc", "cate_id"),
                ...this.selectReturn("categories"),
              ]}
              getOptionLabel={(option) => option.label}
              onChange={(e, values) => this.selectsChange(e, values, "categories")}
              value={this.selectValue2("cate_desc", "cate_id")}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Categoria"
                  className="textField"
                  variant="outlined"
                  name="cate_desc"
                  onChange={(e) =>
                    this.searchFieldChange(
                      e,
                      "category_load",
                      "cate_id",
                      "categories/search/",
                      "categories"
                    )
                  }
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {this.state.category_load === true ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
            </Grid>
            <Grid item xs={4}>
            <Autocomplete
              id="combo-box-demo"
              disableClearable
              filterSelectedOptions
              loading={this.state.product_load}
              options={[
                this.selectValue2("product_desc", "product_id"),
                ...this.selectReturn(),
              ]}
              getOptionLabel={(option) => option.label}
              onChange={(e, values) => this.selectsChange(e, values)}
              value={this.selectValue2("product_desc", "product_id")}
              getOptionSelected={(option, value) =>
                option.value === value.value
              }
              style={{ margin: 8 }}
              fullWidth
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Producto o evento"
                  className="textField"
                  variant="outlined"
                  name="product_desc"
                  onChange={(e) =>
                    this.searchFieldChange(
                      e,
                      "product_load",
                      "product_id",
                      "products/search/",
                      "events"
                    )
                  }
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {this.state.product_load === true ? (
                          <CircularProgress color="inherit" size={20} />
                        ) : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
            </Grid>
            <Grid item xs={4}>
            {this.state.product_id === 50013?
              <Autocomplete
                id="combo-box-demo"
                disableClearable
                filterSelectedOptions
                loading={this.state.product_load}
                options={[
                  this.selectValue2("career_name", "sems_id"),
                  ...this.selectReturn("semesters"),
                ]}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) => this.selectsChange(e, values, "semesters")}
                value={this.selectValue2("career_name", "sems_id")}
                getOptionSelected={(option, value) =>
                  option.value === value.value
                }
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Items"
                    className="textField"
                    variant="outlined"
                    name="product_desc"
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.asistevents.length > 0 &&
                            <IconButton
                              size="small"
                              onClick={()=>(
                                this.setState({event_desc: 'Todos', event_id: ''},
                                this.getObject()
                              ))}>
                              <CloseIcon color="inherit" fontSize="small" />
                            </IconButton>
                          }
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
              :this.state.product_id === 50014?
              <Autocomplete
                id="combo-box-demo"
                disableClearable
                filterSelectedOptions
                loading={this.state.product_load}
                options={[
                  this.selectValue2("career_name", "sems_id"),
                  ...this.selectReturn("semesters2"),
                ]}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) => this.selectsChange(e, values, "semesters2")}
                value={this.selectValue2("career_name", "sems_id")}
                getOptionSelected={(option, value) =>
                  option.value === value.value
                }
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Items"
                    className="textField"
                    variant="outlined"
                    name="product_desc"
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.asistevents.length > 0 &&
                            <IconButton
                              size="small"
                              onClick={()=>(
                                this.setState({event_desc: 'Todos', event_id: ''},
                                this.getObject()
                              ))}>
                              <CloseIcon color="inherit" fontSize="small" />
                            </IconButton>
                          }
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
              :<Autocomplete
                id="combo-box-demo"
                disableClearable
                filterSelectedOptions
                loading={this.state.product_load}
                options={[
                  this.selectValue2("event_desc", "event_id"),
                  ...this.selectReturn("asistevents"),
                ]}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) => this.selectsChange(e, values, "asistevent")}
                value={this.selectValue2("event_desc", "event_id")}
                getOptionSelected={(option, value) =>
                  option.value === value.value
                }
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Items"
                    className="textField"
                    variant="outlined"
                    name="product_desc"
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.asistevents.length > 0 &&
                            <IconButton
                              size="small"
                              onClick={()=>(
                                this.setState({event_desc: 'Todos', event_id: ''},
                                this.getObject()
                              ))}>
                              <CloseIcon color="inherit" fontSize="small" />
                            </IconButton>
                          }
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
            }
              
            </Grid>
          </Grid>
        </Grid>

        <IconButton aria-label="search" onClick={()=>this.exportToPDF()}>
          1
          <PictureAsPdfIcon />
        </IconButton>
        <IconButton aria-label="search" onClick={()=>this.exportToPDF(2)}>
          2
          <PictureAsPdfIcon />
        </IconButton>
        <hr />
        <TableContainer >
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
              <TableCell
                sortDirection={
                  orderBy === "person_lastname" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "person_lastname"}
                  direction={orderBy === "person_lastname" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("person_lastname");
                  }}
                >
                  <h3>Apellido</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "person_fname" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "person_fname"}
                  direction={orderBy === "person_fname" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("person_fname");
                  }}
                >
                  <h3>Nombre</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "person_idnumber" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "person_idnumber"}
                  direction={orderBy === "person_idnumber" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("person_idnumber");
                  }}
                >
                  <h3>C.I/R.U.C</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "telephone" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "telephone"}
                  direction={orderBy === "telephone" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("telephone");
                  }}
                >
                  <h3>Telefono</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "email" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "email"}
                  direction={orderBy === "email" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("email");
                  }}
                >
                  <h3>Correo</h3>
                </TableSortLabel>
              </TableCell>
              {(this.state.product_id === 50013 || this.state.product_id === 50014)&&(
                <TableCell>
                    <h3>Carrera</h3>
                </TableCell>
              )}
              
              <TableCell
                sortDirection={
                  orderBy === "tickdet_desciption" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "tickdet_desciption"}
                  direction={orderBy === "tickdet_desciption" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("tickdet_desciption");
                  }}
                >
                  <h3>Concepto</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "ticket_date" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "ticket_date"}
                  direction={orderBy === "ticket_date" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("ticket_date");
                  }}
                >
                  <h3>Fecha</h3>
                </TableSortLabel>
              </TableCell>
                <TableCell colSpan="2"
                  sortDirection={
                    orderBy === "tickdet_monto" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "tickdet_monto"}
                    direction={orderBy === "tickdet_monto" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tickdet_monto");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {paginator}
      </div>

    );
  }
  renderList() {
    // const { permissions } = this.state;
    if (this.state.incomes.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.incomes.map((data) => {
        return (
          <TableBody key={data.tickdet_id}>
            <TableRow>
            <TableCell>{data.person_lastname}</TableCell>
            <TableCell>{data.person_fname}</TableCell>
            <TableCell>{data.person_idnumber}</TableCell>
            <TableCell>{data.telephone}</TableCell>
            <TableCell>{data.email}</TableCell>
            {(this.state.product_id === 50013 || this.state.product_id === 50014)&&(
              <TableCell>
                  {data.career_name}
              </TableCell>
            )}
            <TableCell>{data.tickdet_desciption}</TableCell>
            <TableCell>{data.ticket_date}</TableCell>
            <TableCell>{data.tickdet_monto*data.tickdet_qty}</TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
