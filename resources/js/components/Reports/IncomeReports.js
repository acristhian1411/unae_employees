import React, {Component} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Popper,
  Snackbar,
  CircularProgress,
  IconButton,
  Grid
} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import Paginator from "../Paginators/Paginator";
import { Autocomplete } from "@material-ui/lab";
import {PromiseGetObject} from "../GlobalFunctions/Promises";
import SmallModal from "../Modals/SmallModal";
import ClearIcon from "@material-ui/icons/Clear";
import {format, disformat} from '../GlobalFunctions/Format';
// var f = new Date();
// var hoy = `${f.getFullYear()}-${(f.getMonth() + 1)}-${f.getDate()}`;
class ListPDF extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount(){
    console.log("props list_pdf");
    console.log(this.props);
  }
  openPDF(index){
    open(`/reportes/ingresos?type=income&index=${index}&from_date=${this.props.from_date}&facu_id=${this.props.facu_id}&to_date=${this.props.to_date}&sort_by=${this.props.orderBy}&order=${this.props.order}`);
  }
  render(){
    return(
      <div>
        <div className="main-header-section">
          <img className="main-img-section" src={`img/dashboard/ingresos.jpeg`} />
          <span className="main-title-section">Ingreso por fecha</span>
        </div>
          <p>
            {`Desde ${this.props.from_date} hasta ${this.props.to_date}`}
          </p>
        <p>{`Para un total de ${this.props.total} registros`}</p>
        <ul className="list-pdf">
          {this.props.list_pdf.map((row, index)=>
            <li onClick={()=>this.openPDF(index)} key={index}><a>{row}</a></li>
          )}
        </ul>
      </div>
    );
  }
}
export default class IncomeReports extends Component{
  constructor(props) {
    super(props);
    this.state = {
      from_date: '',
      to_date: '',
      incomes: [],
      rowsPerPage: 100,
      paginator: [],
      faculties: [],
      persons: [],
      facu_filter:false,
      facu_id:null,
      facu_name:'',
      person_id:null,
      person_name:'',
      description:'',
      filter_person_time_out:false,
      time_out_facu:false,
      page_lenght: 0,
      page: 1,
      order: "desc",
      orderBy: "ticket_date",
      has_changes: false,
      list_pdf: [],
      pay_types_multiple:[],
      paymentTypes:[],
      list_pdf_open: false
    }
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
    this.fieldsChange = this.fieldsChange.bind(this);
  }
  changeFields(e){
    this.setState({[e.target.name]:e.target.value, has_changes: true});
  }
  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("BALANCE GENERAL - INGRESOS");
    axios.get("/api/paymentTypesProofPayments").then((res) => {
      this.setState({ paymentTypes: res.data.data });
      
    });
     axios.get('/api/ticket_details', {params:
         {from: 'frontend',
          type:'income',
          from_date: this.getCurrentDate(),
          to_date: this.getCurrentDate(),
          per_page: this.state.perPage,
          page: this.state.page,
          // sort_by:this.state.orderBy,
          // order: this.state.order
         }
       }).then(res=>{
         // console.log("ingresos");
         // console.log(res.data);
         this.setState({incomes: res.data.data,
                        from_date:this.getCurrentDate(),
                        to_date:this.getCurrentDate(),
                        paginator: res.data,
                        page_lenght: res.data.last_page
                      });
     })
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  getObject(perPage = this.state.rowsPerPage,
            page = this.state.page,
            orderBy = this.state.orderBy,
            order = this.state.order){
console.log('this.state.facu_id');
console.log(this.state.facu_id);
                axios.get('/api/ticket_details', {params:
                  {from: 'frontend',
                   type:'income',
                   from_date: this.state.from_date,
                   to_date: this.state.to_date,
                   per_page: perPage,
                   facu_id: this.state.facu_id,
                   person_id: this.state.person_id,
                   description: this.state.description,
                   pay_type_id: this.state.pay_types_multiple.map(x => x.pay_type_id),
                   page: page,
                  //  sort_by:orderBy,
                  //  order: order
                  }
                }).then(res=>{
                  this.setState({incomes: res.data.data,
                                page:res.data.current_page,
                                page_lenght: res.data.last_page,
                        paginator: res.data,
                        has_changes: false
                              });
              })

  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  exportToPDF(){
    let total_before = this.state.paginator.total;
    console.log("l133");
    console.log(total_before);
    if (this.state.has_changes == true) {
      let promiseGetObject = new Promise((resolve, reject) => {
        PromiseGetObject(total_before, ()=>this.state.paginator.total,()=> this.getObject(), ()=>resolve(true));
      });
      promiseGetObject.then(is_loaded=>{
        let len_file = parseInt((this.state.paginator.total / 500)+1);
        console.log("paso SUPER PROMESA");
        console.log("cantidad de archivos");
        console.log(len_file);
        if (len_file > 1) {
          var list_pdf = new Array();
          list_pdf[0] = "resumen";
          for (var i = 0; i < len_file; i++) {
            list_pdf[i+1] = "parte-"+(i+1)+".pdf";
          }

          console.log("l152");
          console.log(list_pdf);
          this.setState({list_pdf, list_pdf_open: true});
          //aca va el codigo para crear el array de los nombres de documentos, abrir el modal y pasarle el array por parametro

        }else {
          open(`/reportes/ingresos?type=income&from_date=${this.state.from_date}&pay_type_id=${this.state.pay_types_multiple.map(x => x.pay_type_id)}&description=${this.state.description}&person_id=${this.state.person_id}&facu_id=${this.state.facu_id}&to_date=${this.state.to_date}&sort_by=${this.state.orderBy}&order=${this.state.order}`);
          
        }
      })
    }else {
      let len_file = parseInt((this.state.paginator.total / 500)+1);
      console.log("paso SUPER PROMESA");
      console.log("cantidad de archivos");
      console.log(len_file);
      if (len_file > 1) {
        var list_pdf = new Array();
        list_pdf[0] = "resumen";
        for (var i = 0; i < len_file; i++) {
          list_pdf[i+1] = "parte-"+(i+1)+".pdf";
        }

        console.log("l152");
        console.log(list_pdf);
        this.setState({list_pdf, list_pdf_open: true});
        //aca va el codigo para crear el array de los nombres de documentos, abrir el modal y pasarle el array por parametro

      }else {
        open(`/reportes/ingresos?type=income&from_date=${this.state.from_date}&facu_id=${this.state.facu_id}&pay_type_id=${this.state.pay_types_multiple.map(x => x.pay_type_id)}&description=${this.state.description}&person_id=${this.state.person_id}&to_date=${this.state.to_date}&sort_by=${this.state.orderBy}&order=${this.state.order}`);
      }
    }

    // promiseGetObject.then((is_loaded)=>{
    //   if (total_before == this.state.paginator.total) {
    //     setTimeout(()=>{
    //       if (total_before == this.state.paginator.total) {
    //         setTimeout(()=>{
    //           console.log('l147');
    //           console.log(this.state.paginator.total);
    //         }, 500);
    //       }else {
    //         console.log('l151');
    //         console.log(this.state.paginator.total);
    //       }
    //     }, 500);
    //   }else {
    //     console.log('l156');
    //     console.log(this.state.paginator.total);
    //   }
    //   // console.log("count incomes l139");
    //   // console.log(this.state.paginator.total);
    // })

    // this.getObject();
    //
    //
    //
    // open(`/reportes/ingresos?type=income&from_date=${this.state.from_date}&facu_id=${this.state.facu_id}&to_date=${this.state.to_date}&sort_by=${this.state.orderBy}&order=${this.state.order}`);


  }

  searchFaculties(event){

    var e = event;
    this.setState({facu_name : e.target.value})
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out_facu == false) {
      this.setState({ time_out_facu: true });
      setTimeout(() => {
        if (e.target.value === "") {
          // this.getfacus();
          this.setState({time_out_facu: false});
        } else {
    axios
    .get(
      `/api/faculties/search/${e.target.value}`
    )
    .then((res) => {

      this.setState({
        faculties: res.data.data,
        time_out_facu: false,
      });
    })
    .catch((error) => {
      console.log("Ingrese un valor valido");
    });

      }
    })
  }

  }
  Faculties() {
    return this.state.faculties.map((data) => ({
      label: data.facu_name,
      value: data.facu_id,
    }));
  }

  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }

  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }
  fieldsChange(e) {
    e.preventDefault();
    
    this.setState({ [e.target.name]: e.target.value });
  }
  cleanfilter() {
    this.setState({
      // url: `/api/tickets`,
      busqueda: false,
      page: 1,
      person_fname: "",
      person_lastname: "",
      person_idnumber: "",
    });
    this.getObject(this.state.rowsPerPage, this.state.page);
  }
  Employees() {
    return this.state.persons.map((data) => ({
      label: data.person_fname+' '+data.person_lastname,
      value: data.person_id,
    }));
  }
  selectsChange(e, values, nameValue, nameLabel) {
    this.setState({ [nameValue]: values.value, [nameLabel]: values.label });   
  }
  searchEmployees(event) {
    var e = event;
    //funciona pero falta validar error 500
    e.persist();
    this.setState({person_name: e.target.value})
    // console.log(e.target.value)
    // if (this.state.filter_person_time_out == false) {
      this.setState({ filter_person_time_out: true });
      // setTimeout(() => {
        if (e.target.value === "") {
          this.setState({
            page: 1,
            filter_person_time_out: false,
          });
          // this.updateState();
        } else {

          axios
            .get(
              `/api/persons-search?search=${e.target.value}`
            )
            .then((res) => {
              if(res.data != false){
              this.setState({
                persons: res.data.data,
                filter_person_time_out: false,
              });
            }else{
              this.setState({
                persons: [],
                filter_person_time_out: false,
              });
            }
            })
            .catch((error) => {
              this.setState({filter_person_time_out: false});
              console.log("Ingrese un valor valido");
            });
        }
    //   }, 3000);
    // }
  }
  selectsMultipleChange(e, values) {
    this.setState({ pay_types_multiple: values });
   }
   PaymentTypes() {
    return (this.state.paymentTypes.map(data => ({ ...data })));
   }
  render(){
    var paginator, show_links;
    const {orderBy, order} = this.state;
    if (this.state.incomes.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    if (this.state.list_pdf_open) {
      // showModal = <ModalForm edit={false}  onHandleSubmit={this.updateState}/>
      show_links = (
        <SmallModal>
          {{
            onHandleSubmit: ()=>this.setState({list_pdf_open: false, list_pdf:[]}),
            form: <ListPDF />,
            props_form: { onSuccess: ()=>{console.log("exito!");},
              list_pdf:this.state.list_pdf,
              from_date: this.state.from_date,
              facu_id: this.state.facu_id,
              to_date: this.state.to_date,
              orderBy: this.state.orderBy,
              order: this.state.order,
              total: this.state.paginator.total
             },
          }}
        </SmallModal>
      );
    }
    return(
      <div>

      <div className="main-header-section">
        <img className="main-img-section" src={`img/dashboard/ingresos.jpeg`} />
        <span className="main-title-section">Ingreso por fecha</span>
      </div>
      <Grid container>
        <Grid container item xs={12} spacing={0}>
          <Grid item xs={3}>
          <TextField
          id="date"
          label="Desde"
          type="date"
          name="from_date"
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
          </Grid>
          <Grid item xs={3}>
          <TextField
          id="date"
          label="Hasta"
          type="date"
          name="to_date"
          // defaultValue="2017-05-24"
          value={this.state.to_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
          </Grid>
          <Grid item xs={3}>

          <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[this.selectValue("facu_name", "facu_id"), ...this.Faculties()]}
                    getOptionLabel={(option) => option.label.split("-")[0]}
                    onChange={(e, values) =>{
                      this.setState({facu_id: values.value, facu_name: values.label, facu_filter:true, has_changes: true})
                    }
                    }
                    value={{label: this.state.facu_name, value:this.state.facu_id}}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}

                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Facultad "
                        className="textField"
                        onChange={(e)=>{this.searchFaculties(e)}}
                      size="small"
                      id="outlined-search"
                      variant="outlined"
                        name="facu_id"
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {this.state.time_out_facu === true ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    )}
                  />
          </Grid>
          <Grid item xs={3}>
          <IconButton aria-label="search" onClick={()=>this.getObject()}>
          <SearchIcon />
        </IconButton>
        <IconButton aria-label="search" >
          <ClearIcon />
        </IconButton>
        <IconButton aria-label="search" onClick={()=>this.exportToPDF()}>
          <PictureAsPdfIcon />
        </IconButton>
          </Grid>
        </Grid>
        <Grid container item xs={12} spacing={3}>
            <Grid item xs={3}>
            <Autocomplete
                id="combo-box-demo"
                filterSelectedOptions
                options={[this.selectValue('person_name','person_id'), ...this.Employees()]}
                getOptionLabel={(option) => option.label}
                onChange={(e, values) =>{
                  if(values === null){
                    // console.log("aplicar los cambios");
                    this.setState({person_name: '', person_id:''})
                   
                  }else{
                    this.selectsChange(e, values, "person_id", "person_name")
                  }
                }}
                value={this.selectValue("person_name", "person_id")}
                getOptionSelected={(option, value)=>option.value === value.value}
                style={{ margin: 8 }}
                fullWidth
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Estudiante"
                    className="textField"
                    size="small"
                    variant="outlined"
                    // onChange={(e)=>{this.searchEmployees(e)}}
                    onKeyDown={(e) =>{
                      if(e.key === 'Enter'){
                        e.preventDefault();
                        this.searchEmployees(e)
                      }
                    }}
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {this.state.filter_person_time_out === true ? (
                            <CircularProgress color="inherit" size={20} />
                          ) : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
            </Grid>
            
            <Grid item xs={3}>
            <Autocomplete
              fullWidth
                multiple
                id="tags-standard"
                size="small"
                options={[...this.state.pay_types_multiple,
                          ...this.PaymentTypes()]}
                filterSelectedOptions
                value={this.state.pay_types_multiple}
                getOptionLabel={(option) => option.pay_type_desc}
                getOptionSelected={(option, value)=>option.pay_type_id === value.pay_type_id}
                defaultValue={[this.PaymentTypes()[1]]}
                onChange={(e, values) =>
                  this.selectsMultipleChange(e, values)}
                PopperComponent={(props) =>(
                  <Popper {...props} placement='top' disablePortal={true} />
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    label="Tipos de pagos"
                    size="small"
                    />
                  )}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="outlined-search"
                label="Concepto"
                variant="outlined"
                size="small"
                name="description"
                onChange={this.fieldsChange.bind(this)}
                value={this.state.description}
              />
            </Grid>
           
          </Grid>
      </Grid>

     


        <hr />
        <TableContainer >
          <Table aria-label="simple table" option={{ search: true }}>
            <TableHead>
              <TableRow>
              <TableCell
                sortDirection={
                  orderBy === "cajero_fname" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "cajero_name"}
                  direction={orderBy === "cajero_name" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("cajero_name");
                  }}
                >
                  <h3>Cajera</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "till_name" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "till_name"}
                  direction={orderBy === "till_name" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("till_name");
                  }}
                >
                  <h3>Caja</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "person_fname" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "person_fname"}
                  direction={orderBy === "person_fname" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("person_fname");
                  }}
                >
                  <h3>Persona/Empresa</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "person_idnumber" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "person_idnumber"}
                  direction={orderBy === "person_idnumber" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("person_idnumber");
                  }}
                >
                  <h3>C.I/R.U.C</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "tickdet_desciption" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "tickdet_desciption"}
                  direction={orderBy === "tickdet_desciption" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("tickdet_desciption");
                  }}
                >
                  <h3>Concepto</h3>
                </TableSortLabel>
              </TableCell>
              {/* <TableCell
                sortDirection={
                  orderBy === "unit_faculty" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "unit_faculty"}
                  direction={orderBy === "unit_faculty" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("unit_faculty");
                  }}
                >
                  <h3>Institucion</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "unit_faculty" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "unit_faculty"}
                  direction={orderBy === "unit_faculty" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("unit_faculty");
                  }}
                >
                  <h3>Carrera</h3>
                </TableSortLabel>
              </TableCell> */}
              <TableCell
                sortDirection={
                  orderBy === "ticket_date" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "ticket_date"}
                  direction={orderBy === "ticket_date" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("ticket_date");
                  }}
                >
                  <h3>Fecha y Hora</h3>
                </TableSortLabel>
              </TableCell>
                <TableCell>
                    <h3>Tipo de pago</h3>
                </TableCell>
                <TableCell colSpan="2"
                  sortDirection={
                    orderBy === "tickdet_monto" ? order : false
                  }
                >
                  <TableSortLabel
                    active={orderBy === "tickdet_monto"}
                    direction={orderBy === "tickdet_monto" ? order : "asc"}
                    onClick={() => {
                      this.createSortHandler("tickdet_monto");
                    }}
                  >
                    <h3>Monto</h3>
                  </TableSortLabel>
                </TableCell>
              </TableRow>
            </TableHead>
            {this.renderList()}
          </Table>
        </TableContainer>
        {paginator}
        {show_links}
      </div>

    );
  }
  renderList() {
    // const { permissions } = this.state;
    if (this.state.incomes.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.incomes.map((data) => {
        return (
          <TableBody key={data.tickdet_id}>
            <TableRow>
            <TableCell>{data.cajero_name}</TableCell>
            <TableCell>{data.till_name}</TableCell>
            <TableCell>{data.person_name}</TableCell>
            <TableCell>{data.person_idnumber}</TableCell>
            <TableCell>{data.tickdet_desciption}</TableCell>
            {/* <TableCell>{data.unit_faculty}</TableCell>
            <TableCell>{data.unit_faculty}</TableCell> */}
            <TableCell>{data.created_at}</TableCell>
            <TableCell>{data.pay_type_desc}</TableCell>
            <TableCell>{format(Math.round(data.tickdet_monto))}</TableCell>
            <TableCell>{data.comp_name}</TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
