import React, {Component} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
  IconButton,
  Grid
} from "@material-ui/core";
import {Chart, registerables} from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import SearchIcon from '@material-ui/icons/Search';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import Paginator from "../Paginators/Paginator";
import { Autocomplete } from "@material-ui/lab";
import {PromiseGetObject} from "../GlobalFunctions/Promises";
// var f = new Date();
// var hoy = `${f.getFullYear()}-${(f.getMonth() + 1)}-${f.getDate()}`;
var ageChart = "";
var pieChart = "";
var schoolChart = "";
var cityChart = "";
export default class DemographicReport extends Component{
  constructor(props) {
    super(props);
    this.state = {
      from_date: '',
      to_date: '',
      faculties: [],
      facu_filter:false,
      time_out_facu: false,
      facu_id:0,
      facu_name:'Todas las facultades',
      cantidad_desertores: '',
      cantidad_estudiantes:'',
      cantidad_funcionarios: '',
      cantidad_profesores: '',
      cantidad_hombres: {count:0, average_age: 0},
      cantidad_mujeres: {count:0, average_age: 0},
      promedio_18: '',
      promedio_22: '',
      promedio_25: '',
      promedio_29: '',
      promedio_30: '',
      colegios_provenientes: [],
      ciudades_provenientes: [],
      has_changes: false,
    }
    this.makePieChart = this.makePieChart.bind(this);
    this.ageChart = this.ageChart.bind(this);
    this.getObject = this.getObject.bind(this);
  }
  getObject(from_date = this.state.from_date){
    axios.get(`/api/demographic?from_date=${from_date}&facu=${this.state.facu_id}`).then(res=>{
      // console.log(res);
      var len_school = res.data.colegios_provenientes.length;
      var schools = [];
      for (var i = 0; i < len_school; i++) {
        let simbolos = "0123456789AB";
        let color = "#";
        for (var j = 0; j < 6; j++) {
          color = color+simbolos[Math.floor(Math.random()*12)];
        }
        schools.push({...res.data.colegios_provenientes[i], color});
      }
      var len_cities = res.data.ciudades_provenientes.length;
      var cities = [];
      for (var i = 0; i < len_cities; i++) {
        let simbolos = "0123456789AB";
        let color = "#";
        for (var j = 0; j < 6; j++) {
          color = color+simbolos[Math.floor(Math.random()*12)];
        }
        cities.push({...res.data.ciudades_provenientes[i], color});
      }
      // console.log("l78");
      // console.log(schools);
       this.setState({
         cantidad_desertores: res.data.cantidad_desertores,
         cantidad_estudiantes:res.data.cantidad_estudiantes,
         cantidad_funcionarios: res.data.cantidad_funcionarios,
         cantidad_profesores: res.data.cantidad_profesores,
         cantidad_hombres: res.data.cantidad_hombres,
         cantidad_mujeres: res.data.cantidad_mujeres,
         promedio_18: res.data.promedio_18,
         promedio_22: res.data.promedio_22,
         promedio_25: res.data.promedio_25,
         promedio_29: res.data.promedio_29,
         promedio_30: res.data.promedio_30,
         colegios_provenientes: schools,
         ciudades_provenientes: cities,
         has_changes: false
       });

    });
    // setTimeout(()=>{
    //   this.makePieChart();
    //   this.ageChart();
    //   this.schoolChart();
    //   this.citiesChart();
    // }, 1000);
  }
  getObjectWithPromise(){
    let total_before = this.state.cantidad_estudiantes;
    if (this.state.has_changes == true) {
      let promiseGetObject = new Promise((resolve, reject) => {
        PromiseGetObject(total_before, ()=>this.state.cantidad_estudiantes,()=> this.getObject(), ()=>resolve(true));
      });
      promiseGetObject.then(is_loaded=>{
        this.makePieChart();
        this.ageChart();
        this.schoolChart();
        this.citiesChart();
      });
    }
  }
  changeFields(e){
    this.setState({[e.target.name]:e.target.value, has_changes: true});
  }
  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("INFORMES GENERALES - DEMOGRAFICO");
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    this.setState({from_date: year, has_changes: true});
    // from_date=${year}-01-01&to_date=${this.getCurrentDate()}
    setTimeout(()=>{this.getObjectWithPromise()},100);
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  exportToPDF(){

    // open(`/reportes/demografico?type=demographic&from_date=${this.state.from_date}&facu_id=${this.state.facu_id}&to_date=${this.state.to_date}`);
    alert("No disponible");

  }

  searchFaculties(event){

    var e = event;
    this.setState({facu_name : e.target.value})
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out_facu == false) {
      this.setState({ time_out_facu: true });
      setTimeout(() => {
        if (e.target.value === "") {
          // this.getfacus();
          this.setState({time_out_facu: false});
        } else {
    axios
    .get(
      `/api/faculties/search/${e.target.value}`
    )
    .then((res) => {

      this.setState({
        faculties: res.data.data,
        time_out_facu: false,
      });
    })
    .catch((error) => {
      console.log("Ingrese un valor valido");
    });

      }
    })
  }

  }
  Faculties() {
    return this.state.faculties.map((data) => ({
      label: data.facu_name,
      value: data.facu_id,
    }));
  }

  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }
  makePieChart(){
    const pieData = {
        datasets:[{
          fill: true,
          label: 'My first dataset',
          data: [this.state.cantidad_hombres.count,this.state.cantidad_mujeres.count],
          backgroundColor:[
            '#a63c2a',
            '#bf8e06',
          ],
          hoverOffset: 4
        }]
    }
    const options = {
    rotation: -0.7 * Math.PI,
    responsive: false,
    tooltips: {
      enabled: false
    },
    plugins: {
      datalabels: {
        formatter: function(value, ctx){
          let sum = 0;
          let dataArr = ctx.chart.data.datasets[0].data;
          dataArr.map(data=>{
            sum += data;
          });
          let percentage = (value * 100 / sum).toFixed(0) + '%';
          return percentage;
        },
        color: '#fff',
        font: {
          weight: 'bold',
          family: 'Helvetica'
        }
      }
    }
    }
    const ctx = document.getElementById('pieChart').getContext("2d");
    Chart.register(...registerables, ChartDataLabels);
    if (pieChart) {
      pieChart.destroy();
    }
    pieChart = new Chart(ctx, {
      type: "pie",
      data: pieData,
      options: options
    })
  }
  schoolChart(){
    const pieData = {
        datasets:[{
          fill: true,
          label: 'My first dataset',
          data: this.state.colegios_provenientes.map(x=>x.count_schools),
          backgroundColor: this.state.colegios_provenientes.map(x=>x.color),
          hoverOffset: 4
        }]
    }
    const options = {
    rotation: -0.7 * Math.PI,
    responsive: false,
    tooltips: {
      enabled: false
    },
    plugins: {
      datalabels: {
        formatter: function(value, ctx){
          let sum = 0;
          let dataArr = ctx.chart.data.datasets[0].data;
          dataArr.map(data=>{
            sum += data;
          });
          let percentage = (value * 100 / sum).toFixed(2) + '%';
          return percentage;
        },
        color: '#fff',
        font: {
          weight: 'bold',
          family: 'Helvetica'
        }
      }
    }
    }
    const ctx = document.getElementById('schoolChart').getContext("2d");
    Chart.register(...registerables, ChartDataLabels);
    if (schoolChart) {
      schoolChart.destroy();
    }
    schoolChart = new Chart(ctx, {
      type: "pie",
      data: pieData,
      options: options
    })
  }
  citiesChart(){
    const pieData = {
        datasets:[{
          fill: true,
          label: 'My first dataset',
          data: this.state.ciudades_provenientes.map(x=>x.count_cities),
          backgroundColor: this.state.ciudades_provenientes.map(x=>x.color),
          hoverOffset: 4
        }]
    }
    const options = {
    rotation: -0.7 * Math.PI,
    responsive: false,
    tooltips: {
      enabled: false
    },
    plugins: {
      datalabels: {
        formatter: function(value, ctx){
          let sum = 0;
          let dataArr = ctx.chart.data.datasets[0].data;
          dataArr.map(data=>{
            sum += data;
          });
          let percentage = (value * 100 / sum).toFixed(2) + '%';
          return percentage;
        },
        color: '#fff',
        font: {
          weight: 'bold',
          family: 'Helvetica'
        }
      }
    }
    }
    const ctx = document.getElementById('cityChart').getContext("2d");
    Chart.register(...registerables, ChartDataLabels);
    if (cityChart) {
      cityChart.destroy();
    }
    cityChart = new Chart(ctx, {
      type: "pie",
      data: pieData,
      options: options
    })
  }
  ageChart(){
    const promedio_18 = this.state.promedio_18.count;
    const promedio_22 = this.state.promedio_22.count;
    const promedio_25 = this.state.promedio_25.count;
    const promedio_29 = this.state.promedio_29.count;
    const promedio_30 = this.state.promedio_30.count;
    const data = {
        labels: [`< 18 años: ${promedio_18}`,
                 `19 a 22 años: ${promedio_22}`,
                 `23 a 25 años: ${promedio_25}`,
                 `26 a 29 años: ${promedio_29}`,
                 `> 30 años: ${promedio_30}`
               ],
        datasets:[{
          axis: 'y',
          data: [promedio_18, promedio_22, promedio_25, promedio_29, promedio_30],
          fill: false,
          backgroundColor:[
            '#2a83a6',
            '#2a83a6',
            '#2a83a6',
            '#2a83a6',
            '#2a83a6',
          ],
          borderColor:[
            '#2a83a6',
            '#2a83a6',
            '#2a83a6',
            '#2a83a6',
            '#2a83a6',
          ],
          borderWidth: 1
        }]
    }
    const options = {
    responsive: false,
    tooltips: {
      enabled: false
    },
    plugins: {
      legend: {
        display: false
      },
      datalabels: {
        formatter: function(value, ctx){
          let sum = 0;
          let dataArr = ctx.chart.data.datasets[0].data;
          dataArr.map(data=>{
            sum += data;
          });
          let percentage = (value * 100 / sum).toFixed(0) + '%';
          return percentage;
        },
        color: '#fff',
        font: {
          weight: 'bold',
          family: 'Helvetica'
        }
      }
    },
    indexAxis: 'y'
    }
    const ctx = document.getElementById('ageChart').getContext("2d");
    Chart.register(...registerables, ChartDataLabels);
    if (ageChart) {
      ageChart.destroy();
    }
    ageChart = new Chart(ctx, {
      type: "bar",
      data: data,
      options: options
    })
  }
  render(){
    return(
      <div>

      <div className="main-header-section">
        <img className="main-img-section" src={`img/dashboard/impuestos.jpeg`} />
        <span className="main-title-section">Demográfico</span>
      </div>
      <Grid container>
        <Grid container item xs={12} spacing={0}>
          <Grid item xs={3}>
          <TextField
          id="text"
          label="Año"
          type="number"
          variant="outlined"
          size="small"
          name="from_date"
          style={{ margin: 8 }}
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
          </Grid>
          <Grid item xs={3}>

          <Autocomplete
                    id="combo-box-demo"
                    disableClearable
                    options={[this.selectValue("facu_name", "facu_id"), ...this.Faculties()]}
                    getOptionLabel={(option) => option.label.split("-")[0]}
                    onChange={(e, values) =>{
                      this.setState({facu_id: values.value, facu_name: values.label, facu_filter:true, has_changes: true})
                    }
                    }
                    value={{label: this.state.facu_name, value:this.state.facu_id}}
                    getOptionSelected={(option, value) => {
                      if (value === option.value) {
                        return option.label;
                      } else {
                        return false;
                      }
                    }}
                    style={{ margin: 8 }}

                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Facultad "
                        className="textField"
                        onChange={(e)=>{this.searchFaculties(e)}}
                      size="small"
                      id="outlined-search"
                      variant="outlined"
                        name="facu_id"
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <React.Fragment>
                              {this.state.time_out_facu === true ? (
                                <CircularProgress color="inherit" size={20} />
                              ) : null}
                              {params.InputProps.endAdornment}
                            </React.Fragment>
                          ),
                        }}
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    )}
                  />
          </Grid>
          <Grid item xs={3}>
          <IconButton aria-label="search" onClick={()=>this.getObjectWithPromise()}>
          <SearchIcon />
        </IconButton>
        {/*<IconButton aria-label="search" onClick={()=>this.exportToPDF()}>
          <PictureAsPdfIcon />
        </IconButton>*/}
          </Grid>
        </Grid>
      </Grid>




        <hr />
          <div className="demographic-quantity-section">
            <h4>Cantidad general</h4>
            <p className="qty-persons">
            {`Estudiantes: ${this.state.cantidad_estudiantes.count} |
              Docentes: ${this.state.cantidad_profesores.count} |
              Funcionarios: ${this.state.cantidad_funcionarios.count}
            `}
            </p>
            <p className="qty-desertors">{
              this.state.cantidad_desertores.count != 1 ?
              `Han desertado ${this.state.cantidad_desertores.count} Estudiantes`
              :`Ha desertado ${this.state.cantidad_desertores.count} Estudiante`
            }</p>
          </div>
          <div className="demographic-average-section">
            <h4>Edad y Sexo | Estudiantes</h4>
            <div className="demographic-card">
              <div style={{float: 'left'}}>
              <canvas id="pieChart" width="150" height="150"></canvas>
              </div>
              <ul className="demographic-option-piechart">
                <li style={{"--color-var": "#bf8e06"}} className="demographic-option-piechart-item1">{`Mujeres: ${this.state.cantidad_mujeres.count}`}</li>
                <li style={{"--color-var": "#a62c2a"}} className="demographic-option-piechart-item1">{`Hombres: ${this.state.cantidad_hombres.count}`}</li>
              </ul>
            </div>
            <div className="demographic-card">
              <h5 className="qty-persons">Promedio de edad:</h5>
              <div>
                <canvas id="ageChart" width="370" height="170"></canvas>
              </div>
            </div>
            <div className="demographic-card">
              <h5 className="qty-persons">Promedio de edad por sexo:</h5>
              <ul className="average-age-by-gender">
                <li>{`Mujeres: ${parseFloat(this.state.cantidad_mujeres.average_age).toFixed(0)}`}</li>
                <li>{`Hombres: ${parseFloat(this.state.cantidad_hombres.average_age).toFixed(0)}`}</li>
              </ul>
            </div>
          </div>
          <div className="demographic-average-section">
            <h4>Colegios provenientes</h4>
            <div className="demographic-fullcard">
            <ul className="demographic-option-left-piechart">
              {this.state.colegios_provenientes.map(row=>{
                return(
                    <li key={row.school_id}
                      className="demographic-option-piechart-item1"
                      style={{"--color-var": row.color}}
                      >
                      {`${row.school_name}: ${row.count_schools}`}
                    </li>
                )
                })
              }
            </ul>
              <div style={{float: 'left'}}>
              <canvas id="schoolChart" width="300" height="300"></canvas>
              </div>
            </div>
          </div>
          <div className="demographic-average-section">
            <h4>Ciudades provenientes</h4>
            <div className="demographic-fullcard">
            <ul className="demographic-option-left-piechart">
              {this.state.ciudades_provenientes.map(row=>{
                return(
                    <li key={row.city_id}
                      className="demographic-option-piechart-item1"
                      style={{"--color-var": row.color}}
                      >
                      {`${row.city_name}: ${row.count_cities}`}
                    </li>
                )
                })
              }
            </ul>
              <div style={{float: 'left'}}>
              <canvas id="cityChart" width="300" height="300"></canvas>
              </div>
            </div>
          </div>
      </div>

    );
  }
}
