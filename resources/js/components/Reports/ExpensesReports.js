import React, {Component} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableContainer,
  TableHead,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
  IconButton
} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'
import Paginator from "../Paginators/Paginator";
import { Autocomplete } from "@material-ui/lab";
import {format, disformat} from '../GlobalFunctions/Format';

export default class ExpensesReports extends Component{
  constructor(props){
    super(props);
    this.state = {
      time_out_till: false,
      time_out_person: false,
      from_date: this.getCurrentDate(),
      to_date: this.getCurrentDate(),
      amount: "",
      tills_multiple: [],
      tills: [],
      persons_multiple: [],
      persons: [],
      person_id: 0,
      person_name: "",
      till_id: "",
      till_name: "",
      expenses: [],
      rowsPerPage: 100,
      paginator: [],
      page_lenght: 0,
      page: 1,
      order: "desc",
      orderBy: "date",
    }
    this.perPageChange = this.perPageChange.bind(this);
    this.pageChange = this.pageChange.bind(this);
  }
  changeFields(e){
    this.setState({[e.target.name]:e.target.value});
  }
  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("BALANCE GENERAL - EGRESOS");
    axios.get('/api/tills').then(res=>{
      this.setState({tills: res.data.data
                   });
  })
    //  axios.get('/api/tills_details-expenses', {params:
    //      {from: 'frontend',
    //       type:'expenses',
    //       from_date: this.getCurrentDate(),
    //       to_date: this.getCurrentDate(),
    //       per_page: this.state.perPage,
    //       page: this.state.page,
    //       sort_by:this.state.orderBy,
    //       order: this.state.order
    //      }
    //    }).then(res=>{
    //      this.setState({expenses: res.data.data,
    //                     from_date:this.getCurrentDate(),
    //                     to_date:this.getCurrentDate(),
    //                     paginator: res.data,
    //                     page_lenght: res.data.last_page
    //                   });
    //  })
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  getObject(perPage = this.state.rowsPerPage,
            page = this.state.page,
            orderBy = this.state.orderBy,
            order = this.state.order){
    //cambiar
    axios.get('/api/tills_details-expenses', {params:
        {from: 'frontend',
         type:'expenses',
         from_date: this.state.from_date,
         to_date: this.state.to_date,
         amount: this.state.amount,
         tills: this.state.tills_multiple.map((x)=>{return x.value}),
         person: this.state.persons_multiple.map((x)=>{return x.value}),
         per_page: perPage,
         page: page,
         sort_by:orderBy,
         order: order
        }
      }).then(res=>{
        console.log('res.data.data')
        console.log(res.data.data)
        if(res.data.data !== null || res.data.data !== undefined){
          this.setState({expenses: res.data.data,
            paginator: res.data,
            page_lenght: res.data.last_page});
        }
    })
  }
  perPageChange(value) {
    // console.log(value);
    this.setState({ rowsPerPage: value, page: 1 });
    this.getObject(value, 1);
  }
  pageChange(value) {
    this.setState({ page: value });
    this.getObject(this.state.rowsPerPage, value);
  }
  exportToPDF(){
    open(`/reportes/egresos?type=expense&from_date=${this.state.from_date}&to_date=${this.state.to_date}
    &tills[]=${this.state.tills_multiple.map((x)=>{return x.value})}
    &persons[]=${this.state.persons_multiple.map((x)=>{return x.value})}
    &amount=${this.state.amount}
    &sort_by=${this.state.orderBy}&order=${this.state.order}`);
  }
  createSortHandler(name) {
    var order = "";
    if (this.state.orderBy === name) {
      if (this.state.order === "asc") {
        order = "desc";
        this.setState({ order });
      } else {
        order = "asc";
        this.setState({ order });
      }
    } else {
      order = "asc";
      this.setState({ orderBy: name, order });
    }
    this.getObject(this.state.rowsPerPage, this.state.page, name, order);
  }

Tills(){
  return (this.state.tills.map(data => ({ label: data.till_name, value: data.till_id })));
}
Persons(){
  return (this.state.persons.map(data => ({ label: data.person_fname+' '+data.person_lastname , value: data.person_id })));
}
selectValue(name,label){
  return {label:this.state[label], value:this.state[name]};
}
searchFieldChange(event){
  
  var e = event;
  
  e.persist();
  if(e.target.id == "till_name"){
  this.setState({till_name: e.target.value, till_id: ''});
    if (this.state.time_out_till==false) {
      this.setState({time_out_till:true});
      setTimeout(()=>{
        if(e.target.value===''){
          axios.get('api/tills').then(res=>{
            this.setState({tills: res.data.data, time_out_till: false});
            });
        }
        else {
          axios.get(`/api/tills?till_name=${e.target.value}`).then(res=>{
                  this.setState({
                    tills: res.data.data,
                    time_out_till:false
                  });
                  console.log(res.data.data)
                })
        }}
      , 3000);
    }
  }else if(e.target.id == "person_name"){
    this.setState({person_name: e.target.value, person_id: ''});
    if (this.state.time_out_person==false) {
      this.setState({time_out_person:true});
      setTimeout(()=>{
        if(e.target.value===''){
          axios.get('api/persons').then(res=>{
            this.setState({persons: res.data.data, time_out_person: false});
            });
        }
        else {
          axios.get(`/api/persons-search?search=${e.target.value}`).then(res=>{
            console.log(res.data.data)

                  this.setState({
                    persons: res.data.data,
                    time_out_person:false
                  });
                })
        }}
      , 3000);
    }
  }

  
}

selectsMultipleChange(e, values,name) {
  this.setState({ [name]: values });
}

  render(){
    var paginator;
    const {orderBy, order} = this.state;
    if (this.state.expenses.length === 0) {
      paginator = <div>{""}</div>;
    } else {
      paginator = (
        <Paginator
          lenght={this.state.page_lenght}
          perPageChange={this.perPageChange}
          pageChange={this.pageChange}
          page={this.state.page}
        />
      );
    }
    return(
      <div>
        <div className="main-header-section">
          <img className="main-img-section" src={`img/dashboard/egresos.jpeg`} />
          <span className="main-title-section">Egresos por fecha</span>
        </div>
        <TextField
          id="date"
          label="Desde"
          type="date"
          name="from_date"
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="date"
          label="Hasta"
          type="date"
          name="to_date"
          // defaultValue="2017-05-24"
          value={this.state.to_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
         <TextField
          id="number"
          label="Monto"
          type="number"
          name="amount"
          // defaultValue="2017-05-24"
          value={this.state.amount}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Autocomplete
        multiple
        disableClearable
        options={[this.selectValue('till_id','till_name'), ...this.Tills()]}
        filterSelectedOptions
        getOptionLabel={(option) => option.label}
        onChange={(e, values) =>
        this.selectsMultipleChange(e, values,'tills_multiple')}
        value={this.state.tills_multiple}
        style={{ margin: 8 }}
        id="till_name"
        loading={this.state.time_out_till}
        getOptionSelected={(option, value)=>option.value === value.value}
        fullWidth
        renderInput={(params) => <TextField {...params} 
                                                label="Seleccione una Caja"
                                                variant="outlined"
                                                fullWidth
                                                onChange={(e)=>{this.searchFieldChange(e)}}
                                                InputProps={{
                                                  ...params.InputProps,
                                                  endAdornment: (
                                                    <React.Fragment>
                                                      {this.state.time_out_till === true ? <CircularProgress color="inherit" size={20} /> : null}
                                                      {params.InputProps.endAdornment}
                                                    </React.Fragment>
                                                  ),
                                                }}
                                                />}
          />
          <Autocomplete
             multiple
             disableClearable
             options={[this.selectValue('person_id','person_name'), ...this.Persons()]}
             filterSelectedOptions
             getOptionLabel={(option) => option.label}
             onChange={(e, values) =>
             this.selectsMultipleChange(e, values,'persons_multiple')}
             value={this.state.persons_multiple}
             style={{ margin: 8 }}
             id="person_name"
             loading={this.state.time_out_person}
             getOptionSelected={(option, value)=>option.value === value.value}
             fullWidth
            renderInput={(params) => <TextField {...params} 
                                                label="Seleccione un Proveedor"
                                                variant="outlined"
                                                fullWidth
                                                onChange={(e)=>{this.searchFieldChange(e)}}
                                                InputProps={{
                                                  ...params.InputProps,
                                                  endAdornment: (
                                                    <React.Fragment>
                                                      {this.state.time_out_person === true ? <CircularProgress color="inherit" size={20} /> : null}
                                                      {params.InputProps.endAdornment}
                                                    </React.Fragment>
                                                  ),
                                                }}
                                                />}
          />
        <IconButton aria-label="search" onClick={()=>this.getObject()}>
          <SearchIcon />
        </IconButton>
        <IconButton aria-label="search" onClick={()=>this.exportToPDF()}>
          <PictureAsPdfIcon />
        </IconButton>
        <hr />
        <TableContainer >
        <Table aria-label="simple table" option={{ search: true }}>
          <TableHead>
            <TableRow>
            <TableCell>{'#'}</TableCell>
            <TableCell>{'Caja'}</TableCell>

              <TableCell
                sortDirection={
                  orderBy === "person_fname" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "person_fname"}
                  direction={orderBy === "person_fname" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("person_fname");
                  }}
                >
                  <h3>Persona/Empresa</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "person_idnumber" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "person_idnumber"}
                  direction={orderBy === "person_idnumber" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("person_idnumber");
                  }}
                >
                  <h3>C.I/R.U.C</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "provacc_description" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "provacc_description"}
                  direction={orderBy === "provacc_description" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("provacc_description");
                  }}
                >
                  <h3>Concepto</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell
                sortDirection={
                  orderBy === "date" ? order : false
                }
              >
                <TableSortLabel
                  active={orderBy === "date"}
                  direction={orderBy === "date" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("date");
                  }}
                >
                  <h3>Fecha y Hora</h3>
                </TableSortLabel>
              </TableCell>
              <TableCell>
                  <h3>Tipo de pago</h3>
              </TableCell>
              <TableCell colSpan="2"
                sortDirection={
                  orderBy === "provacc_amountpaid" ? order : false
                }
              >
                <TableSortLabel colSpan="2"
                  active={orderBy === "provacc_amountpaid"}
                  direction={orderBy === "provacc_amountpaid" ? order : "asc"}
                  onClick={() => {
                    this.createSortHandler("provacc_amountpaid");
                  }}
                >
                  <h3>Monto</h3>
                </TableSortLabel>
              </TableCell>
            </TableRow>
          </TableHead>
          {this.renderList()}
        </Table>
      </TableContainer>
      {paginator}
      </div>
    );
  }
  renderList() {
    //cambiar
    // const { permissions } = this.state;
    if (this.state.expenses.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.expenses.map((data,index) => {
        return (
          <TableBody key={data.tickdet_id}>
            <TableRow>
            <TableCell>{index+1}</TableCell>
            <TableCell>{data.till_name}</TableCell>
            <TableCell>
              {data.description == 'Pago a proveedor'?
                data.provider_name
                :data.description == 'Pago a docente'?
                `${data.profe_fname} `
                :data.description == 'Pago a funcionario'?
                `${data.provider_name}`
                :' '
              }
            </TableCell>
            <TableCell>
            {data.person_idnumber
              
            }
            </TableCell>
            <TableCell>
            {data.description == 'Pago a proveedor'?
              data.provacc_description
              :data.description == 'Pago a docente'?
              data.profeacc_desc
              :data.description == 'Pago a funcionario'?
              data.emp_acc_desc
              :' '
            }
            </TableCell>
            <TableCell>{data.date}</TableCell>
            <TableCell>Efectivo</TableCell>
            <TableCell>
            {
              format(Math.round(data.amount))
            }
            </TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
