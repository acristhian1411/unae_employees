import React,{Component} from 'react';
import {SemesterYearInLetter, SemesterInLetter} from '../ArraysCalls/SemesterNumber';
import {Grid, TextField, IconButton, CircularProgress, FormControl, InputLabel, Button, InputAdornment,
Select} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import SaveIcon from "@material-ui/icons/Save";
import AddBoxOutlinedIcon from "@material-ui/icons/AddBoxOutlined";
import { Autocomplete } from "@material-ui/lab";
import EnrolledsCareerCompare from "../Enrolleds/EnrolledsCareerCompare";
import MidModal from "../Modals/MidModal";
import SmallModal from "../Modals/SmallModal";
import DeleteIcon from '@material-ui/icons/Delete';

export default class EnrolledReports extends Component{
  constructor(props){
    super(props);
    this.state ={
      careers: [],
      facu_careers: [],
      type_asist: 1,
      view_mode: 1,
      filter_by: 1,
      abandon_type: 1,
      from_date: '',
      year: '2023',
      to_date: '',
      period: 1,
      faculties_multiple: [{
        value:50001,
        facu_name:'FACULTAD DE CIENCIAS ARTES Y TECNOLOGIAS',
        label:'FACAT'
      }],
      faculties: [],
      time_out_facu: false,
      facu_id: 50001,
      facu_code: 'FACAT',
      facu_name: 'FACULTAD DE CIENCIAS ARTES Y TECNOLOGIAS',
      facu_filter: false,
      has_changes: false,
      compare_open: false,
      row_to_compare: [],
      fechas_inscripciones:[
        '2022-12-24',
        '2022-12-25',
        '2022-12-26',
        '2022-12-27',
        '2022-12-28',
      ],
      fcareer_inscript:[],
      specific_date: true,
      specific_date_disabled: false,
      edit_sdate: false,
      datemode_is_loading: false,
    }
    this.getObject = this.getObject.bind(this);
    this.cambioFechas = this.cambioFechas.bind(this);
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("INFORMES GENERALES - INSCRIPCIONES");
    axios.get('/api/faculties?per_page=50&order_by=facu_name').then(res=>{
      this.setState({faculties: res.data.data})
    })
    this.setState({
      from_date:this.getCurrentDate(),
      to_date:this.getCurrentDate()
    })
    this.getObject(this.getCurrentDate(), this.getCurrentDate());
    var default_fechas = [];
    var current_date = new Date();
    for(var i=0; i<15;i++){
      default_fechas.push(
        current_date.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ current_date.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + current_date.toLocaleDateString("en-US", { day: '2-digit' })
      );
      current_date = new Date(current_date.getTime()-86400000);
    }
    this.setState({fechas_inscripciones: default_fechas.sort()});


  }
  cambioFechas(param) {
    // console.log("cambioFechas _ismounted");
    // console.log(this._ismounted);
    this.callUpdateFCareer(2, this.state.specific_date ,param);
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
    console.log("se desmonto")
  }
  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }
  getObject(from_date= this.state.from_date, to_date= this.state.to_date){
    if(this.state.view_mode == 1){
      axios.get(`/api/enrolleds_report?from_date=${from_date}&year=${this.state.year}&to_date=${to_date}&filter_by=${this.state.filter_by}&facu_id=${this.state.faculties_multiple.map((c)=>c.value)}&period=${this.state.period}`).then(res=>{
        this.setState({careers: res.data});
      });
    }else if(this.state.view_mode == 2){
      this.callUpdateFCareer(2, this.state.specific_date, this.state.fechas_inscripciones);
    }else{
      alert("ocurrio algun error inesperado");
    }
    
  }
  exportToPDF(){
    console.log("a futuro se abrira una ventana con el pdf generado");
    alert("No disponible aun");
  }
  searchFaculties(event){

    var e = event;
    this.setState({facu_name : e.target.value})
    //funciona pero falta validar error 500
    e.persist();
    if (this.state.time_out_facu == false) {
      this.setState({ time_out_facu: true });
      setTimeout(() => {
        if (e.target.value === "") {
          // this.getfacus();
          this.setState({time_out_facu: false});
        } else {
    axios
    .get(
      `/api/faculties/search/${e.target.value}?per_page=50&order_by=facu_name`
    )
    .then((res) => {

      this.setState({
        faculties: res.data.data,
        time_out_facu: false,
      });
    })
    .catch((error) => {
      console.log("Ingrese un valor valido");
    });

      }
    })
  }

  }
  Faculties() {
    return this.state.faculties.filter(x=>x.facu_id!=101).map((data) => {
      var label = '';
      if ([50003, 50004, 50005].includes(data.facu_id)){
        label = `Colegio - ${data.facu_code}`;
      }else{
        label = data.facu_code;
      }
      return({
        label: label,
        facu_name: data.facu_name,
        value: data.facu_id,
      })
    });
  }

  selectValue(name, label) {
    return { label: this.state[name], value: this.state[label] };
  }
  compare(row_to_compare){
    this.setState({row_to_compare, compare_open: true});
  }
  selectsMultipleChange(e, values) {
    this.setState({ faculties_multiple: values });
   }
  callUpdateFCareer(valor, sdate=this.state.specific_date, fechas=this.state.fechas_inscripciones, abandon_type=this.state.abandon_type){
    if(valor == 2){
      // console.log("se envio la peticion");
      // console.log(this.state.specific_date)
      this.setState({datemode_is_loading:true});
      axios.get(`/api/fcareer_inscript?year=${this.state.year}&fechas_inscri=${fechas}&sdate=${sdate}&abandon_type=${abandon_type}&facus[]=${this.state.faculties_multiple.map(i=>i.value)}`)
      .then(res=>{
        this.setState({fcareer_inscript:res.data, datemode_is_loading: false});
        if(fechas !== this.state.fechas_inscripciones){
          this.setState({fechas_inscripciones:fechas, datemode_is_loading: false});
        }
      }).catch(()=>{
        this.setState({datemode_is_loading:false});
      })
    }
  }
  getTotal(fecha, facultad = null){
    var total = 0;
    if(this.state.type_asist==1){
     var array_fcareers = this.state.fcareer_inscript;
    }else if(this.state.type_asist ==2){
      var array_fcareers = this.state.fcareer_inscript.filter(x=>!(x.career_name.toLowerCase().includes('online')));
    }else if(this.state.type_asist ==3){
      var array_fcareers = this.state.fcareer_inscript.filter(x=>x.career_name.toLowerCase().includes('online'));
      // console.log(array_careers);
    }else{
      // console.log("paso algo en el else");
      // console.log(type_asist);
    }
    if(facultad !== null){
      array_fcareers.filter(x=>x.faculty_id === facultad).map((row)=>{
        if (row.get_enroll_fromdate !== undefined){
          try{  
            row.get_enroll_fromdate = JSON.parse(row.get_enroll_fromdate);  
          }catch(e){
            row = row;
          }
          if(row.get_enroll_fromdate.filter(x=>x.fecha === fecha).length>0){
            total += row.get_enroll_fromdate.filter(x=>x.fecha === fecha)[0].cantidad;  
          }
        }else{
          row.get_enroll_fromdate = [];
        }
      })
    }else{
      array_fcareers.map((row)=>{
        if (row.get_enroll_fromdate !== undefined){
          try{  
            row.get_enroll_fromdate = JSON.parse(row.get_enroll_fromdate);  
          }catch(e){
            row = row;
          }
          if(row.get_enroll_fromdate.filter(x=>x.fecha === fecha).length>0){
            total += row.get_enroll_fromdate.filter(x=>x.fecha === fecha)[0].cantidad;  
          }
        }else{
          row.get_enroll_fromdate = [];
        }
      })
    }
    return total;
  }
  getSemesterSection(param){
    var last_letter = param.slice(-1);
    if(['A','B','C','D'].includes(last_letter)){
      return last_letter;
    }else{
      return "";
    }
  }
  render(){
    var total_enrolleds=0;
    var total_abandonos=0;
    var modal_compare = "";
    var editar_sdates = "";
    var array_careers = [];
    const {compare_open, from_date, to_date, filter_by, type_asist, period, edit_sdate} = this.state;
    if (compare_open) {
      modal_compare =
        <MidModal>
          {{
            onHandleSubmit: ()=>this.setState({compare_open: false}),
            form: <EnrolledsCareerCompare />,
            props_form: { row_compare: this.state.row_to_compare, year:this.state.year, from_date, to_date, filter_by, period },
          }}
        </MidModal>;
    }
    if(edit_sdate){
      editar_sdates = 
      <SmallModal>
        {{
          onHandleSubmit: ()=>this.setState({edit_sdate: false}),
          form: <EditFechas/>,
          props_form: { fechas_inscripciones: this.state.fechas_inscripciones,  onSuccess: this.cambioFechas },
        }}
      </SmallModal>;
    }
    if(type_asist==1){
      array_careers = this.state.careers;
    }else if(type_asist ==2){
      array_careers = this.state.careers.filter(x=>!(x.career_name.toLowerCase().includes('online')));
    }else if(type_asist ==3){
      array_careers = this.state.careers.filter(x=>x.career_name.toLowerCase().includes('online'));
      // console.log(array_careers);
    }else{
      // console.log("paso algo en el else");
      // console.log(type_asist);
    }
    return(
      <div>
        <div className="main-header-section">
          <img className="main-img-section" src={`img/dashboard/informe_inscripciones.jpeg`} />
          <span className="main-title-section">Inscripciones</span>
        </div>
        <Grid container>
          <Grid container item xs={12} spacing={0}>
            <Grid item xs={2}>
              <FormControl
                variant="outlined"
                inputlabelprops={{
                  shrink: true,
                }}
                className="input_small"
                style={{width:'150px',margin: 8}}

              >
                <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: '#fafafa', padding: '0 5px'}} shrink={true}>Modo de vista:</InputLabel>
                <Select
                  native
                  label="Modo de vista:"
                  value={this.state.view_mode}
                  inputProps={{
                    name:"view_mode:",
                    id: "demo-simple-select-helper",
                  }}
                  onChange={(e)=>{
                    this.callUpdateFCareer(e.target.value);
                    this.setState({view_mode: e.target.value})
                  }}
                >
                  <option value={1}>Mostrar cursos</option>
                  <option value={2}>Mostrar por fecha</option>
                </Select>
              </FormControl>
            </Grid>
            {this.state.view_mode==1&&(
              <Grid item xs={3}>
                <FormControl
                  variant="outlined"
                  inputlabelprops={{
                    shrink: true,
                  }}
                  className="input_small"
                  style={{width:'90%',marginTop: 8}}

                >
                  <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: '#fafafa', padding: '0 5px'}} shrink={true}>Filtrar por:</InputLabel>
                  <Select
                    native
                    label="Filtrar por:"
                    value={this.state.filter_by}
                    inputProps={{
                      name:"filter_by:",
                      id: "demo-simple-select-helper",
                    }}
                    onChange={(e)=>{this.setState({filter_by: e.target.value})}}
                  >
                    <option value={1}>Todo el año hasta fecha en especifico</option>
                    <option value={2}>Por rango de fechas</option>
                  </Select>
                </FormControl>
              </Grid>
            )}  
            
            {(this.state.filter_by == 2&&this.state.view_mode==1) &&
              (<Grid item xs={1}>
                <TextField
                className="input_small"
                id="date"
                label="Desde"
                type="date"
                name="from_date"
                style={{width:'95%'}}
                value={this.state.from_date}
                onChange={(e)=>{this.setState({from_date: e.target.value})}}
                InputLabelProps={{
                  shrink: true,
                }}
                />
              </Grid>)
            }
            {this.state.view_mode==1&&(
              <Grid item xs={1}>
                <TextField
                  className="input_small"
                  id="date"
                  label="Hasta"
                  type="date"
                  name="to_date"
                  style={{width:'95%'}}
                  value={this.state.to_date}
                  onChange={(e)=>{this.setState({to_date: e.target.value})}}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  size= "small"
                />
              </Grid>
            )}
            {this.state.view_mode==1&&(
            <Grid item xs={2}>
              <FormControl
                variant="outlined"
                inputlabelprops={{
                  shrink: true,
                }}
                className="input_small"
                style={{width:'80%',margin: 8}}

              >
                <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: '#fafafa', padding: '0 5px'}} shrink={true}>Periodos</InputLabel>
                <Select
                  native
                  label="Periodos"
                  value={this.state.period}
                  inputProps={{
                    name:"Periodo",
                    id: "demo-simple-select-helper",
                  }}
                  onChange={(e)=>{this.setState({period: e.target.value})}}
                >
                  <option value={1}>Primer periodo</option>
                  <option value={2}>Segundo periodo</option>
                </Select>
              </FormControl>
            </Grid>
            )}
            <Grid item xs={3}>
            {/* <Autocomplete
              fullWidth
                multiple
                id="tags-standard"
                size="small"
                options={[...this.state.pay_types_multiple,
                          ...this.PaymentTypes()]}
                filterSelectedOptions
                value={this.state.pay_types_multiple}
                getOptionLabel={(option) => option.pay_type_desc}
                getOptionSelected={(option, value)=>option.pay_type_id === value.pay_type_id}
                defaultValue={[this.PaymentTypes()[1]]}
                onChange={(e, values) =>
                  this.selectsMultipleChange(e, values)}
                PopperComponent={(props) =>(
                  <Popper {...props} placement='top' disablePortal={true} />
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    label="Tipos de pagos"
                    size="small"
                    />
                  )}
              /> */}
            <Autocomplete
                      multiple
                      disableClearable
                      options={[...this.Faculties()]}
                      // options={[this.selectValue("facu_name", "facu_id"), ...this.Faculties()]}
                      getOptionLabel={(option) => `(${option.label}) ${option.facu_name}`}
                      filterSelectedOptions
                      defaultValue={[this.state.faculties_multiple[0]]}
                      onChange={(e, values) => this.selectsMultipleChange(e, values)}
                      // onChange={(e, values) =>{
                      //   this.setState({facu_id: values.value, facu_name: values.label, facu_filter:true, has_changes: true})
                      // }
                      // }
                      value={this.state.faculties_multiple}
                      // value={{label: this.state.facu_name, value:this.state.facu_id}}
                    //  getOptionSelected={(option, value)=>option.value === value.value}
                      getOptionSelected={(option, value)=>option.value === value.value}
                      style={{ margin: 8 }}

                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Facultad "
                          onChange={(e)=>{this.searchFaculties(e)}}
                        id="outlined-search"
                        variant="outlined"
                          name="facu_id"
                          InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                              <React.Fragment>
                                {this.state.time_out_facu === true ? (
                                  <CircularProgress color="inherit" size={20} />
                                ) : null}
                                {params.InputProps.endAdornment}
                              </React.Fragment>
                            ),
                          }}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      )}
                    />
            </Grid>
             <Grid item xs={2}>
              <FormControl
                variant="outlined"
                inputlabelprops={{
                  shrink: true,
                }}
                className="input_small"
                style={{width:'150px',margin: 8}}

              >
                <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: '#fafafa', padding: '0 5px'}} shrink={true}>Tipo de asistencia:</InputLabel>
                <Select
                  native
                  label="Tipo de asistencia:"
                  value={this.state.type_asist}
                  inputProps={{
                    name:"type_asist:",
                    id: "demo-simple-select-helper",
                  }}
                  onChange={(e)=>{
                    this.setState({type_asist: e.target.value})
                  }}
                >
                  <option value={1}>Todos</option>
                  <option value={2}>Presencial</option>
                  <option value={3}>Online</option>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={1}>
            <TextField
              className="input_small"
              id="date"
              label="Año"
              type="number"
              name="year"
              style={{width:'80px'}}
              value={this.state.year}
              onChange={(e)=>{this.setState({year: e.target.value})}}
              InputLabelProps={{
                shrink: true,
              }}
              size= "small"
            />
            </Grid>
            {this.state.view_mode == 2&&
              <Grid item xs={2}>
                <FormControl
                  variant="outlined"
                  inputlabelprops={{
                    shrink: true,
                  }}
                  className="input_small"
                  style={{width:'200px',margin: 8}}

                >
                  <InputLabel htmlFor="demo-simple-select-outlined" style={{backgroundColor: '#fafafa', padding: '0 5px'}} shrink={true}>Filtro de abandonos:</InputLabel>
                  <Select
                    native
                    label="Tipo de asistencia:"
                    value={this.state.abandon_type}
                    inputProps={{
                      name:"abandon_type:",
                      id: "demo-simple-select-helper",
                    }}
                    onChange={(e)=>{
                      this.setState({abandon_type: e.target.value});
                    }}
                  >
                    <option value={1}>Incluye abandonados</option>
                    <option value={2}>Excluye abandonados</option>
                    <option value={3}>Solo Abandonados</option>
                  </Select>
                </FormControl>
              </Grid>
            }
            <Grid item xs={2}>
            <IconButton aria-label="search" onClick={()=>this.getObject()}>
              <SearchIcon />
            </IconButton>
            {/*<IconButton aria-label="search" onClick={()=>this.exportToPDF()}>
              <PictureAsPdfIcon />
            </IconButton>*/}
            </Grid>
          </Grid>
        </Grid>
        <hr/>
        {this.state.view_mode == 1?
          <div>
            <div className="enrolled_report-title-faculty">
              {this.state.faculties_multiple.map((facu, facu_index)=>{
                if(facu_index>0){
                  return(`; ${facu.facu_name}`);
                }else{
                  return(facu.facu_name);  
                }
                
              })}
            </div>
            {array_careers.map((row, index)=>{
              var sub_total_semester = 0;
              var sub_total_abandonos = 0;
              if (row.semesters.map((item)=>item.count_student).length > 0) {
                sub_total_semester = row.semesters.map((item)=>item.count_student).reduce((pv, cv)=> pv+cv);
                sub_total_abandonos = row.semesters.map((item)=>item.cantidad_abandono).reduce((pv, cv)=> pv+cv);
                total_enrolleds = total_enrolleds +sub_total_semester;
                total_abandonos = total_abandonos +sub_total_abandonos;
              }

              return(
                <div key={index} className="enrolled_report-career-section">
                  <div className="enrolled_report-title-career">{row.career_name}</div>
                  <div className="enrolled_report-semesters-section">
                    {row.semesters.map((semester, index_semester)=>{
                      return(
                        <div key={index_semester}>
                          <div className="enrolled_report-title-year" onClick={(e)=>{
                            window.open(`/#/monitoreo/administrativo/${semester.sems_id}`);
                          }}>
                            {SemesterYearInLetter(semester.sems_year)}
                          </div>
                          <div className="enrolled_report-title-period">
                            Primer Periodo
                          </div>
                          {this.getSemesterSection(semester.sems_name) !== ""&&
                            <div className="enrolled_report-title-period">
                              Sección: {this.getSemesterSection(semester.sems_name)}
                            </div>
                          }
                          <div className="enrolled_report-title-label">
                            {`Total Inscriptos: ${semester.count_student}`}
                          </div>
                          <div className="enrolled_report-title-period">
                            {`Abandonos: ${semester.cantidad_abandono}`}
                          </div>
                          <div className="enrolled_report-title-period">
                            {`Vigentes: ${semester.cantidad_vigente}`}
                          </div>
                        </div>
                      )
                    })}
                    <div >
                      <div className="enrolled_report-title-total">
                        TOTAL
                      </div>
                      <div className="enrolled_report-title-label">
                        {`Inscriptos: ${sub_total_semester}`}
                      </div>
                      <div className="enrolled_report-title-period">
                        {`Abandonos: ${sub_total_abandonos}`}
                      </div>
                      <div className="enrolled_report-title-period">
                        {`Vigentes: ${sub_total_semester-sub_total_abandonos}`}
                      </div>
                      <div className="enrolled_report-title-label">
                        {`Consultantes: ${row.count_consult}`}
                      </div>
                      {row.objectives.length > 0 &&
                        (
                          <>
                          <div className="enrolled_report-title-meta">
                            {`Minimo fijado: ${row.objectives[0].obj_min}`}
                          </div>
                          <div className="enrolled_report-title-meta">
                            {`Meta: ${row.objectives[0].obj_target}`}
                          </div>
                          </>
                        )
                      }
                      <div
                        onClick={()=>(this.compare(row))}
                        className="enrolled_report-button-compare">
                        Hacer comparación
                      </div>

                    </div>
                  </div>
                </div>
              );
            })}
            <br/>
            <div className="enrolled_report-total-section">
              <h3>TOTAL</h3>
              <div className="enrolled_report-value-total">
                {total_enrolleds}<span>Inscriptos</span>-
                {total_abandonos}<span>Abandonos</span>=
                {total_enrolleds-total_abandonos}<span>Vigentes</span>
              </div>
            </div>
          </div>
          :this.state.view_mode ==2 &&(
            <div>
              <table className="table-fechas-inscrip">
                <thead>
                  <tr>
                    <td>
                    <button
                      onClick={()=>{
                        this.setState({specific_date:!this.state.specific_date, specific_date_disabled: true});
                        this.callUpdateFCareer(2, !this.state.specific_date);
                        setTimeout(()=>{
                          this.setState({specific_date_disabled:false});
                        }, 3000);
                      }}
                      disabled={this.state.specific_date_disabled}
                    >
                      {!(this.state.specific_date)?
                        'Busqueda de lo anterior hasta fechas especificas. Click para cambiar'
                        :'Busqueda por fechas especificas. Click para cambiar'
                      }
                    </button>
                    </td>
                    {this.state.fechas_inscripciones.map((fecha, fecha_index)=>{
                      var th_date = new Date(`${fecha} 00:00`);
                      var th_date = th_date.toLocaleDateString("en-US", { day: '2-digit' })+ "/" + th_date.toLocaleDateString("en-US", { month: '2-digit' })

                      return(
                        <th key={`fecha-${fecha_index}`} className="th-fecha-inscript">{th_date}</th>
                      )
                    }
                    )}
                    <td>
                      <button 
                        onClick={()=>{
                          this.setState({edit_sdate: true});
                        }}
                      >
                        Editar o agregar fechas
                      </button>
                    </td>
                  </tr>

                </thead>
                {this.state.datemode_is_loading?
                  <tbody>
                    <tr>
                      <td colSpan={this.state.fechas_inscripciones.length+2} align="center">
                        <CircularProgress />
                      </td>
                    </tr>
                  </tbody>
                :
                <tbody>
                  {this.state.faculties_multiple.map((facu, facu_index)=>{
                      if(type_asist==1){
                       var array_fcareers = this.state.fcareer_inscript;
                      }else if(type_asist ==2){
                        var array_fcareers = this.state.fcareer_inscript.filter(x=>!(x.career_name.toLowerCase().includes('online')));
                      }else if(type_asist ==3){
                        var array_fcareers = this.state.fcareer_inscript.filter(x=>x.career_name.toLowerCase().includes('online'));
                        // console.log(array_careers);
                      }else{
                        // console.log("paso algo en el else");
                        // console.log(type_asist);
                      }
                      // return(`; ${facu.facu_name}`);              
                      return[
                        <tr key={`facu-${facu_index}`}>
                          <td colSpan={this.state.fechas_inscripciones.length} style={{fontWeight: 'bold', paddingLeft: 10}}>{facu.facu_name}</td>
                        </tr>,
                        array_fcareers.filter(x=>x.faculty_id === facu.value).map((career, career_index)=>{
                          if (career.get_enroll_fromdate !== undefined){
                              try{  
                                career.get_enroll_fromdate = JSON.parse(career.get_enroll_fromdate);  
                              }catch(e){
                                career = career;
                              }
                          }else{
                            career.get_enroll_fromdate = [];
                          }
                          return(
                          <tr className="tr-career-fechas_inscrip" key={`career-${career_index}`}>
                            <td style={{paddingLeft: 20}} className="td-career-description">{career.career_name}</td>
                            {this.state.fechas_inscripciones.map((fecha, fecha_index)=>{
                              if (career.get_enroll_fromdate.filter(x=>x.fecha == fecha).length>0) {
                                return(<td className="career-cantidad" align="center">{career.get_enroll_fromdate.filter(x=>x.fecha == fecha)[0].cantidad}</td>)
                              }else{
                                return("-");
                              }
                              
                            }
                            )}
                          </tr>
                          ) 
                        }),
                        <tr>
                          <td style={{fontWeight: 'bold', paddingLeft: 10}}>Total</td>
                          {this.state.fechas_inscripciones.map((fecha, fecha_index)=>(
                            <td style={{fontWeight: 'bold'}} align="center">{this.getTotal(fecha, facu.value)}</td>
                          ))}
                        </tr>
                        
                      ]
                  })}
                  <tr className="fila-total_total_inscrip">
                    <td style={{fontWeight: 'bold'}}>Total</td>
                    {this.state.fechas_inscripciones.map((fecha, fecha_index)=>(
                      <td style={{fontWeight: 'bold'}} align="center">{this.getTotal(fecha)}</td>
                    ))}
                  </tr>
                </tbody>
                }
                
              </table>
            </div>
          )
        }
        {/*array_careers.map((row, index)=>{
          var sub_total_semester = 0;
          if (row.semesters.map((item)=>item.count_student).length > 0) {
            var sub_total_semester = row.semesters.map((item)=>item.count_student).reduce((pv, cv)=> pv+cv);
            total_enrolleds = total_enrolleds +sub_total_semester;
          }

          return(
            <div key={index} className="enrolled_report-career-section">
              <div className="enrolled_report-title-career">{row.career_name}</div>
              <div className="enrolled_report-semesters-section">
                {row.semesters.map((semester, index_semester)=>{
                  return(
                    <div key={index_semester}>
                      <div className="enrolled_report-title-year" onClick={(e)=>{
                        window.open(`/#/monitoreo/administrativo/${semester.sems_id}`);
                      }}>
                        {SemesterYearInLetter(semester.sems_year)}
                      </div>
                      <div className="enrolled_report-title-period">
                        Primer Periodo
                      </div>
                      <div className="enrolled_report-title-label">
                        {`Inscriptos: ${semester.count_student}`}
                      </div>
                    </div>
                  )
                })}
                <div >
                  <div className="enrolled_report-title-total">
                    TOTAL
                  </div>
                  <div className="enrolled_report-title-label">
                    {`Inscriptos: ${sub_total_semester}`}
                  </div>
                  <div className="enrolled_report-title-label">
                    {`Consultantes: ${row.count_consult}`}
                  </div>
                  {row.objectives.length > 0 &&
                    (
                      <>
                      <div className="enrolled_report-title-meta">
                        {`Minimo fijado: ${row.objectives[0].obj_min}`}
                      </div>
                      <div className="enrolled_report-title-meta">
                        {`Meta: ${row.objectives[0].obj_target}`}
                      </div>
                      </>
                    )
                  }
                  <div
                    onClick={()=>(this.compare(row))}
                    className="enrolled_report-button-compare">
                    Hacer comparación
                  </div>

                </div>
              </div>
            </div>
          );
        })}
        <br/>
        <div className="enrolled_report-total-section">
          <h3>TOTAL</h3>
          <div className="enrolled_report-value-total">
            {total_enrolleds}<span>Inscriptos</span>
          </div>
        </div>*/}
        {modal_compare}
        {editar_sdates}
      </div>
    )
  }
}

class EditFechas extends Component {
  constructor(props){
    super(props);
    this.state = {
      fechas_inscripciones1: []
    }
    this.guardaFechas = this.guardaFechas.bind(this);
  }
  componentDidMount(){
    // console.log("props");
    // console.log(this.props);
    this.setState({fechas_inscripciones1:this.props.fechas_inscripciones});
  }
  guardaFechas(){
    this.props.onSuccess(this.state.fechas_inscripciones1.sort());
    this.props.onHandleSubmit();
  }
  render(){
    return(
      <div>
        <h4 align="center">
          {'Fechas de inscripciones '}
          <Button
            name="add"
            variant="outlined"
            color="primary"
            startIcon={<AddBoxOutlinedIcon />}
            type="submit"
            onClick={()=>{
              var sort_fechas = this.state.fechas_inscripciones1.sort();
              var last_date = new Date(`${sort_fechas.reverse()[0]} 00:00`);
              var new_date = new Date(last_date.getTime()+86400000);
              var new_date_format = new_date.toLocaleDateString("en-US", { year: 'numeric' })+ "-"+ new_date.toLocaleDateString("en-US", { month: '2-digit' })+ "-" + new_date.toLocaleDateString("en-US", { day: '2-digit' })
              sort_fechas.push(new_date_format);
              this.setState({fechas_inscripciones1: sort_fechas.sort().reverse()});
            }}
          >
            Agregar
          </Button>
        </h4><hr/>
        {this.state.fechas_inscripciones1.map((row, fecha_index)=>{
          return(
          <TextField 
            key={fecha_index}
            value={row}
            color="primary" 
            fullWidth
            variant="outlined"
            margin="normal"
            type="date"
            onChange={(e)=>{
              var temp_array = this.state.fechas_inscripciones1;
              temp_array[fecha_index] = e.target.value;
              this.setState({fechas_inscripciones1: temp_array});
            }}
            InputProps={{
              endAdornment: <InputAdornment position="end">
                              <IconButton aria-label="search" onClick={()=>{
                                if(this.state.fechas_inscripciones1.length ===1){
                                  alert("No se puede eliminar todos los registros");
                                }else{
                                  var array_fechas = this.state.fechas_inscripciones1.filter((row,index)=>index !==fecha_index);
                                  this.setState({fechas_inscripciones1: array_fechas});  
                                }
                              }}>
                                <DeleteIcon style={{color: 'red'}} />
                              </IconButton>
                            </InputAdornment>,
            }}
          />
          )
        })}
        <Button
          name="save"
          variant="outlined"
          color="primary"
          startIcon={<SaveIcon />}
          type="submit"
          onClick={this.guardaFechas}
        >
          Guardar
        </Button>
      </div>
    );
  }
}