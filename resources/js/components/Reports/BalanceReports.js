import React, {Component} from 'react';
import Paginator from "../Paginators/Paginator";
import {Capitalize} from '../GlobalFunctions/LetterCase';
import {format, disformat} from '../GlobalFunctions/Format';
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Icon,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableContainer,
  TableHead,
  TablePagination,
  Paper,
  TextField,
  MobileStepper,
  Select,
  MenuItem,
  TableSortLabel,
  TableFooter,
  InputBase,
  InputLabel,
  Tooltip,
  Snackbar,
  CircularProgress,
  IconButton
} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf'

export default class BalanceReports extends Component{
  constructor(props){
    super(props);
    this.state = {
      from_date: '',
      to_date: '',
      balance: {incomes: [], expenses: []}
    }
  }
  changeFields(e){
    this.setState({[e.target.name]:e.target.value});
  }
  getCurrentDate(separator = "-") {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date < 10 ? `0${date}` : `${date}`}`;
  }
  componentDidMount(){
    this._ismounted = true;
    this.props.changeTitlePage("BALANCE GENERAL");
     axios.get('/api/tills_details-balance', {params:
         {from: 'frontend_balance',
          type:'balance',
          from_date: this.getCurrentDate(),
          to_date: this.getCurrentDate()
         }
       }).then(res=>{
         this.setState({balance: res.data,
                        from_date:this.getCurrentDate(),
                        to_date:this.getCurrentDate()});
     })
  }
  componentWillUnmount() {
    this.props.changeTitlePage("");
    this._ismounted = false;
  }
  getObject(){
    //cambiar
    axios.get('/api/tills_details-balance', {params:
        {from: 'frontend_balance',
         type:'balance',
         from_date: this.state.from_date,
         to_date: this.state.to_date
        }
      }).then(res=>{
        this.setState({balance: res.data});
    })
  }
  exportToPDF(){
    open(`/reportes/balance?type=balance&from_date=${this.state.from_date}&to_date=${this.state.to_date}&from=frontend_balance`);
  }
  render(){
    return(
      <div>
        {console.log(this.state.balance)}
      <div className="main-header-section">
        <img className="main-img-section" src={`img/dashboard/balance.jpeg`} />
        <span className="main-title-section">Balance por fecha</span>
      </div>
        <TextField
          id="date"
          label="Desde"
          type="date"
          name="from_date"
          // defaultValue="2017-05-24"
          value={this.state.from_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          id="date"
          label="Hasta"
          type="date"
          name="to_date"
          // defaultValue="2017-05-24"
          value={this.state.to_date}
          onChange={(e)=>{this.changeFields(e)}}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <IconButton aria-label="search" onClick={()=>this.getObject()}>
          <SearchIcon />
        </IconButton>
        <IconButton aria-label="search" onClick={()=>this.exportToPDF()}>
          <PictureAsPdfIcon />
        </IconButton>
        <hr />
        {/*<TableContainer >
        <Table aria-label="simple table" option={{ search: true }}>
          <TableHead>
            <TableRow>
              <TableCell>
                  <h3>Persona/Empresa</h3>
              </TableCell>
              <TableCell>
                  <h3>C.I/R.U.C</h3>
              </TableCell>
              <TableCell>
                  <h3>Concepto</h3>
              </TableCell>
              <TableCell>
                  <h3>Fecha y Hora</h3>
              </TableCell>
              <TableCell>
                  <h3>Tipo de pago</h3>
              </TableCell>
              <TableCell colSpan="2">
                  <h3>Monto</h3>
              </TableCell>
            </TableRow>
          </TableHead>
          {this.renderList()}
        </Table>
      </TableContainer>*/}
      <Grid container spacing={1}>
        <Grid container item xs={12} style={{width: '100%'}} spacing={3}>
          <Grid item xs={6}>
            <div className="balance-report-card" style={{float: 'right'}}>
              <h3 className="balance-report-card-title1">Saldo Anterior</h3>
              <ul>
                { this.state.balance.tills_previous !== null && this.state.balance.tills_previous !== undefined &&(
                this.state.balance.tills_previous.map((data,key)=>{
                  if(data.till_name == 'RECTORADO'){
                    return(
                      <li className="balance-report-card-list1" key={key}>
                        <Grid container spacing={1} >
                          <Grid container item xs={12} spacing={3} >
                            <Grid item xs={6}>
                              <span className="balance-report-card-list1">
                                {`${Capitalize(data.till_name)}: `}
                              </span>
                            </Grid>
                            <Grid item xs={6} className="balance-report-card-item2">
                              <span >
                                {data.amount != null?
                                  `${format(data.amount)}`
                                  : 0
                                }
                              </span>
                            </Grid>
                          </Grid>
                        </Grid>
                      </li>
                    );
                  }
                }))}
               
              </ul>
              <h3 className="balance-report-card-title1">Cajas Bancarias</h3>
              <ul>
                { this.state.balance.tills_previous !== null && this.state.balance.tills_previous !== undefined &&(
                this.state.balance.tills_previous.map((data,key)=>{
                  if(data.till_name !== 'RECTORADO'){
                    return(
                      <li className="balance-report-card-list1" key={key}>
                        <Grid container spacing={1} >
                          <Grid container item xs={12} spacing={3} >
                            <Grid item xs={6}>
                              <span className="balance-report-card-list1">
                                {`${Capitalize(data.till_name)}: `}
                              </span>
                            </Grid>
                            <Grid item xs={6} className="balance-report-card-item2">
                              <span >
                                {data.amount != null?
                                  `${format(data.amount)}`
                                  : 0
                                }
                              </span>
                            </Grid>
                          </Grid>
                        </Grid>
                      </li>
                    );
                  }
                }))}
               
              </ul> 

              <h3 className="balance-report-card-title1">Ingresos</h3>
              <ul>
                {Object.keys(this.state.balance.incomes).map(key=>{
                  if (this.state.balance.incomes[key] >0) {
                    return(
                      <li className="balance-report-card-list1" key={key}>
                        <Grid container spacing={1} >
                          <Grid container item xs={12} spacing={3} >
                            <Grid item xs={6}>
                              <span className="balance-report-card-list1">
                                {`${Capitalize(key)}: `}
                              </span>
                            </Grid>
                            <Grid item xs={6} className="balance-report-card-item2">
                              <span >
                                {this.state.balance.incomes[key] != null?
                                  `${format(this.state.balance.incomes[key])}`
                                  : 0
                                }
                              </span>
                            </Grid>
                          </Grid>
                        </Grid>
                      </li>
                    );
                  }
                  
                })}
                <li className="balance-report-card-list1">
                  <Grid container spacing={1}>
                    <Grid container item xs={12} spacing={3}>
                      <Grid item xs={6}>
                        <span className="balance-report-card-list1">
                          Total
                        </span>
                      </Grid>
                      <Grid item xs={6} className="balance-report-card-item2">
                        <span>
                          {`${format(this.state.balance.incomes_total)}`}
                        </span>
                      </Grid>
                    </Grid>
                  </Grid>
                </li>
              </ul>
            </div>
          </Grid>
          <Grid item xs={6}>
            <div className="balance-report-card">
              <h3 className="balance-report-card-title2">Egresos</h3>
              <ul>
                {Object.keys(this.state.balance.expenses).map(key=>{
                  return(
                    <li className="balance-report-card-list1" key={key}>
                      <Grid container spacing={1}>
                        <Grid container item xs={12} spacing={3}>
                          <Grid item xs={6}>
                            <span className="balance-report-card-list1">
                              {`${Capitalize(key)}: `}
                            </span>
                          </Grid>
                          <Grid item xs={6} className="balance-report-card-item2">
                            <span>
                              {this.state.balance.expenses[key] != null?
                                `${format(this.state.balance.expenses[key])}`
                                : 0
                              }
                            </span>
                          </Grid>
                        </Grid>
                      </Grid>
                    </li>
                  );
                })}
                <li className="balance-report-card-list1">
                  <Grid container spacing={1}>
                    <Grid container item xs={12} spacing={3}>
                      <Grid item xs={6}>
                        <span className="balance-report-card-list1">
                          Total
                        </span>
                      </Grid>
                      <Grid item xs={6} className="balance-report-card-item2">
                        <span>
                          {this.state.balance.expenses_total != null?
                            `${format(this.state.balance.expenses_total)}`
                            : 0
                          }
                        </span>
                      </Grid>
                    </Grid>
                  </Grid>
                </li>
              </ul>
            </div>
          </Grid>
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <hr className='line-total'></hr>
          <div className='report-balance-total-title'>TOTAL</div>
          <hr className='line-total'></hr>
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <h1 className='report-balance-total'>
            {`${format((this.state.balance.incomes_total +  (this.state.balance.tills_previous != undefined ? (this.state.balance.tills_previous.reduce((total, cur)=> {
                    if(cur){
                        return parseFloat(total)+parseFloat(cur.amount);
                    }else{
                      return parseFloat(total)+0;
                    }
                  },0)):(0))) - this.state.balance.expenses_total)} GS.`}
          </h1>
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <div style={{textAlign: 'center'}} className='report-balance-total-title'>Rentabilidad:
          {
            (((this.state.balance.incomes_total - this.state.balance.expenses_total)/this.state.balance.incomes_total)*100).toFixed(2).replace(".", ",") !=-Infinity?
              (((this.state.balance.incomes_total - this.state.balance.expenses_total)/this.state.balance.incomes_total)*100).toFixed(2).replace(".", ",")
            : "-Infinito"
          }%
          </div>
        </Grid>
        <Grid container item xs={12} style={{width: '100%'}} spacing={3}>
          <Grid item xs={6}>
        <h3 className="balance-report-card-title1">Saldo Actual</h3>
              <ul>
                { this.state.balance.till_actual !== null && this.state.balance.till_actual !== undefined &&(
                this.state.balance.till_actual.map((data,key)=>{
                  if(data.till_name == 'RECTORADO'){
                    return(
                      <li className="balance-report-card-list1" key={key}>
                        <Grid container spacing={1} >
                          <Grid container item xs={12} spacing={3} >
                            <Grid item xs={6}>
                              <span className="balance-report-card-list1">
                                {`${Capitalize(data.till_name)}: `}
                              </span>
                            </Grid>
                            <Grid item xs={6} className="balance-report-card-item2">
                              <span >
                                {data.amount != null?
                                  `${format(data.amount)}`
                                  : 0
                                }
                              </span>
                            </Grid>
                          </Grid>
                        </Grid>
                      </li>
                    );
                  }
                }))}
               
              </ul>
              <h3 className="balance-report-card-title1">Cajas Bancarias</h3>
              <ul>
                { this.state.balance.till_actual !== null && this.state.balance.till_actual !== undefined &&(
                this.state.balance.till_actual.map((data,key)=>{
                  if(data.till_name !== 'RECTORADO'){
                    return(
                      <li className="balance-report-card-list1" key={key}>
                        <Grid container spacing={1} >
                          <Grid container item xs={12} spacing={3} >
                            <Grid item xs={6}>
                              <span className="balance-report-card-list1">
                                {`${Capitalize(data.till_name)}: `}
                              </span>
                            </Grid>
                            <Grid item xs={6} className="balance-report-card-item2">
                              <span >
                                {data.amount != null?
                                  `${format(data.amount)}`
                                  : 0
                                }
                              </span>
                            </Grid>
                          </Grid>
                        </Grid>
                      </li>
                    );
                  }
                }))}
               
              </ul>

      </Grid>
      </Grid>
      </Grid>
      </div>
    );
  }
  renderList(){
    //cambiar
    // const { permissions } = this.state;
    if (this.state.balance.length === 0) {
      return (
        <TableBody>
          <TableRow>
            <TableCell> </TableCell>
            <TableCell>{"No contiene datos"}</TableCell>
            <TableCell> </TableCell>
            <TableCell>{""}</TableCell>
          </TableRow>
        </TableBody>
      );
    } else {
      return this.state.balance.map((data) => {
        return (
          <TableBody key={data.tickdet_id}>
            <TableRow>
            <TableCell>{data.person_fname}</TableCell>
            <TableCell>{data.person_idnumber}</TableCell>
            <TableCell>{data.provacc_description}</TableCell>
            <TableCell>{data.date}</TableCell>
            <TableCell>Efectivo</TableCell>
            <TableCell>{data.provacc_amountpaid}</TableCell>
            <TableCell>{data.comp_name}</TableCell>
            </TableRow>
          </TableBody>
        );
      });
    }
  }
}
