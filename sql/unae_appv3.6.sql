-- SQL Manager Lite for PostgreSQL 6.1.0.53492
-- ---------------------------------------
-- Host      : 192.168.56.102
-- Database  : unae_appv3
-- Version   : PostgreSQL 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1) on x86_64-pc-linux-gnu, compiled by gcc (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0, 64-bit



SET search_path = public, pg_catalog;
DROP INDEX IF EXISTS public.wpla_idx;
DROP INDEX IF EXISTS public.unit_idx;
DROP INDEX IF EXISTS public.titles_idx;
DROP INDEX IF EXISTS public.ttransf_idx;
DROP INDEX IF EXISTS public.tillsperson_idx;
DROP INDEX IF EXISTS public.tillsdet_idx;
DROP INDEX IF EXISTS public.tills_idx;
DROP INDEX IF EXISTS public.tdettick_idx;
DROP INDEX IF EXISTS public.tariff_idx;
DROP INDEX IF EXISTS public.subj_idx;
DROP INDEX IF EXISTS public.stuscholar_idx;
DROP INDEX IF EXISTS public.sturet_idx;
DROP INDEX IF EXISTS public.stumon_idx;
DROP INDEX IF EXISTS public.perhcar_idx;
DROP INDEX IF EXISTS public.stuacc_id;
DROP INDEX IF EXISTS public.student_idx;
DROP INDEX IF EXISTS public.semes_idx;
DROP INDEX IF EXISTS public.school_idx;
DROP INDEX IF EXISTS public.schol_idx;
DROP INDEX IF EXISTS public.regdocs_idx;
DROP INDEX IF EXISTS public.provacc_idx;
DROP INDEX IF EXISTS public.procpk_idx;
DROP INDEX IF EXISTS public.tprof_idx;
DROP INDEX IF EXISTS public.prereq_idx;
DROP INDEX IF EXISTS public.pplan_idx;
DROP INDEX IF EXISTS public.opera_idx;
DROP INDEX IF EXISTS public.tiva_idx;
DROP INDEX IF EXISTS public.hstitle_idx;
DROP INDEX IF EXISTS public.hcare_idx;
DROP INDEX IF EXISTS public.facar_idx;
DROP INDEX IF EXISTS public.facu_idx;
DROP INDEX IF EXISTS public.facadm_idx;
DROP INDEX IF EXISTS public.evstu_idx;
DROP INDEX IF EXISTS public.enroll_idx;
DROP INDEX IF EXISTS public.emplo_idx;
DROP INDEX IF EXISTS public.docpers_idx;
DROP INDEX IF EXISTS public.depart_idx;
DROP INDEX IF EXISTS public.countcode_idx;
DROP INDEX IF EXISTS public.counname_idx;
DROP INDEX IF EXISTS public.tcontac_idx;
DROP INDEX IF EXISTS public.cntper_idx;
DROP INDEX IF EXISTS public.city_idx;
DROP INDEX IF EXISTS public.career_idx;
DROP INDEX IF EXISTS public.carrers_code;
DROP INDEX IF EXISTS public.careers_idx;
DROP INDEX IF EXISTS public.assit_idx;
DROP INDEX IF EXISTS public.appoin_idx;
DROP INDEX IF EXISTS public.ruc_idx;
DROP INDEX IF EXISTS public.person_ci_idx;
DROP SEQUENCE IF EXISTS public.countries_country_id_seq;
DROP TABLE IF EXISTS public.titles;
DROP TABLE IF EXISTS public.till_transfers;
DROP TABLE IF EXISTS public.tills_details;
DROP TABLE IF EXISTS public.tills;
DROP TABLE IF EXISTS public.ticket_details;
DROP TABLE IF EXISTS public.ticket_details_types;
DROP TABLE IF EXISTS public.tickets;
DROP TABLE IF EXISTS public.tariffs;
DROP TABLE IF EXISTS public.student_workplaces;
DROP TABLE IF EXISTS public.workplace;
DROP TABLE IF EXISTS public.student_scholarship;
DROP TABLE IF EXISTS public.student_retention;
DROP TABLE IF EXISTS public.student_monitoring;
DROP TABLE IF EXISTS public.student_healthcare;
DROP TABLE IF EXISTS public.student_account;
DROP TABLE IF EXISTS public.students;
DROP TABLE IF EXISTS public.schools;
DROP TABLE IF EXISTS public.scholarship;
DROP TABLE IF EXISTS public.provider_accounts;
DROP TABLE IF EXISTS public.providers;
DROP TABLE IF EXISTS public.promos;
DROP TABLE IF EXISTS public.professors;
DROP TABLE IF EXISTS public.pre_requirements;
DROP TABLE IF EXISTS public.persons_checks;
DROP TABLE IF EXISTS public.payplans;
DROP TABLE IF EXISTS public.operations;
DROP TABLE IF EXISTS public.iva_type;
DROP TABLE IF EXISTS public.hscholl_titles;
DROP TABLE IF EXISTS public.healthcare;
DROP TABLE IF EXISTS public.facucar_adm;
DROP TABLE IF EXISTS public.eval_students;
DROP TABLE IF EXISTS public.subject_evaluation;
DROP TABLE IF EXISTS public.evaluation_types;
DROP TABLE IF EXISTS public.employees;
DROP TABLE IF EXISTS public.document_persons;
DROP TABLE IF EXISTS public.required_docs;
DROP TABLE IF EXISTS public.profile_type;
DROP TABLE IF EXISTS public.contact_persons;
DROP TABLE IF EXISTS public.contact_type;
DROP TABLE IF EXISTS public.careers_subject_details;
DROP TABLE IF EXISTS public.assistances;
DROP TABLE IF EXISTS public.enrolleds;
DROP TABLE IF EXISTS public.careers_subjets;
DROP TABLE IF EXISTS public.subjects;
DROP TABLE IF EXISTS public.semesters;
DROP TABLE IF EXISTS public.facu_careers;
DROP TABLE IF EXISTS public.career_type;
DROP TABLE IF EXISTS public.careers;
DROP TABLE IF EXISTS public.faculties;
DROP TABLE IF EXISTS public.units;
DROP TABLE IF EXISTS public.appointments;
DROP TABLE IF EXISTS public.persons;
DROP TABLE IF EXISTS public.cities;
DROP TABLE IF EXISTS public.departaments;
DROP TABLE IF EXISTS public.countries;
SET check_function_bodies = false;
--
-- Structure for table persons (OID = 28009) :
--
CREATE TABLE public.persons (
    person_id integer NOT NULL,
    homecity_id integer,
    birthplace_id integer,
    country_id integer,
    person_fname varchar(255),
    person_lastname varchar(255),
    person_birthdate date,
    person_gender varchar(255),
    person_idnumber varchar(255) NOT NULL,
    person_address text,
    person_bloodtype varchar(255),
    person_photo varchar(255),
    person_business_name varchar(255),
    person_ruc varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table appointments (OID = 28021) :
--
CREATE TABLE public.appointments (
    appoin_id serial NOT NULL,
    appoin_description varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table assistances (OID = 28030) :
--
CREATE TABLE public.assistances (
    enroll_id integer,
    assist_date date NOT NULL,
    asisst_present boolean,
    asisst_belated boolean,
    asisst_id serial NOT NULL,
    carsubj_id integer,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table careers (OID = 28039) :
--
CREATE TABLE public.careers (
    career_id serial NOT NULL,
    career_name varchar(255) NOT NULL,
    career_code varchar(255) NOT NULL,
    career_semsqity integer NOT NULL,
    career_logo varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table careers_subjets (OID = 28052) :
--
CREATE TABLE public.careers_subjets (
    carsubj_id serial NOT NULL,
    facu_career_id integer NOT NULL,
    subj_id integer NOT NULL,
    person_id integer NOT NULL,
    carsubj_year numeric NOT NULL,
    carsubj_shift varchar(255) NOT NULL,
    carsubj_startdate date,
    carsubj_enddate date,
    carsubj_require double precision NOT NULL,
    carsubj_evalprocess integer,
    carsubj_evalfinals integer,
    carsubj_code varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table career_type (OID = 28063) :
--
CREATE TABLE public.career_type (
    tcareer_id serial NOT NULL,
    tcareer_description varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table careers_subject_details (OID = 28070) :
--
CREATE TABLE public.careers_subject_details (
    carsubj_id integer,
    carsubdet_day varchar(255) NOT NULL,
    carsubdet_range integer NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone,
    carsubdet_id bigint DEFAULT nextval('carrera_mat_deetails_carmatdet_id_seq'::regclass) NOT NULL
)
WITH (oids = false);
--
-- Structure for table cities (OID = 28075) :
--
CREATE TABLE public.cities (
    city_id serial NOT NULL,
    depart_id integer NOT NULL,
    city_name varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table contact_persons (OID = 28084) :
--
CREATE TABLE public.contact_persons (
    cntper_id serial NOT NULL,
    cnttype_id integer,
    person_id integer,
    cntper_description varchar(50) NOT NULL,
    cntper_data varchar(50) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table contact_type (OID = 28093) :
--
CREATE TABLE public.contact_type (
    cnttype_id serial NOT NULL,
    cnttype_name varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table countries (OID = 28100) :
--
CREATE TABLE public.countries (
    country_id bigint DEFAULT nextval(('public.countries_country_id_seq'::text)::regclass) NOT NULL,
    country_name varchar(255) NOT NULL,
    country_code varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table departaments (OID = 28112) :
--
CREATE TABLE public.departaments (
    depart_id serial NOT NULL,
    country_id integer,
    depart_name varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table document_persons (OID = 28121) :
--
CREATE TABLE public.document_persons (
    docpers_id serial NOT NULL,
    person_id integer,
    reqdoc_id integer,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table employees (OID = 28128) :
--
CREATE TABLE public.employees (
    appoin_id integer,
    person_startdate date,
    person_status boolean,
    person_observation varchar(255),
    person_salary double precision,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
INHERITS (persons)
WITH (oids = false);
--
-- Structure for table enrolleds (OID = 28135) :
--
CREATE TABLE public.enrolleds (
    enroll_id integer NOT NULL,
    person_id integer,
    carsubj_id integer,
    enrollt_date date NOT NULL,
    enroll_obs varchar(255) NOT NULL,
    enroll_status varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table evaluation_types (OID = 28146) :
--
CREATE TABLE public.evaluation_types (
    evaltype_id serial NOT NULL,
    evaltype_name varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table eval_students (OID = 28154) :
--
CREATE TABLE public.eval_students (
    evstu_id serial NOT NULL,
    enroll_id integer,
    subeval_id integer,
    evstu_earned double precision,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table facucar_adm (OID = 28163) :
--
CREATE TABLE public.facucar_adm (
    fcaradm_id serial NOT NULL,
    facu_career_id integer,
    fcaradm_fee1sem integer NOT NULL,
    fcaradm_fee2sem integer NOT NULL,
    fcaradm_dueday integer DEFAULT 10 NOT NULL,
    fcaradm_year integer,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone,
    CONSTRAINT ckc_fcaradm_dueday_facucar_ CHECK (((fcaradm_dueday >= 1) AND (fcaradm_dueday <= 31))),
    CONSTRAINT ckc_fcaradm_year_facucar_ CHECK (((fcaradm_year IS NULL) OR ((fcaradm_year >= 2000) AND (fcaradm_year <= 3500))))
)
WITH (oids = false);
--
-- Structure for table faculties (OID = 28175) :
--
CREATE TABLE public.faculties (
    facu_id serial NOT NULL,
    facu_name varchar(255) NOT NULL,
    facu_code varchar(255) NOT NULL,
    facu_unit_id integer,
    facu_logo varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table facu_careers (OID = 28185) :
--
CREATE TABLE public.facu_careers (
    facu_career_id integer NOT NULL,
    fcar_name varchar(255) NOT NULL,
    career_id integer NOT NULL,
    faculty_id integer NOT NULL,
    tcareer_id integer,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table healthcare (OID = 28193) :
--
CREATE TABLE public.healthcare (
    hcare_id serial NOT NULL,
    hcare_description varchar(255) NOT NULL,
    hcare_address varchar(255),
    hcare_contacto varchar(255),
    hcare_telephone varchar(255),
    hcare_email varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table hscholl_titles (OID = 28205) :
--
CREATE TABLE public.hscholl_titles (
    hstitle_id serial NOT NULL,
    hstitle_code varchar(255) NOT NULL,
    hstitle_name varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table iva_type (OID = 28217) :
--
CREATE TABLE public.iva_type (
    ivatype_id serial NOT NULL,
    ivatype_description varchar(255) NOT NULL,
    ivatype_valor numeric NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table operations (OID = 28229) :
--
CREATE TABLE public.operations (
    oper_id serial NOT NULL,
    ivatype_id integer,
    oper_description varchar(255) NOT NULL,
    oper_amount numeric NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table payplans (OID = 28241) :
--
CREATE TABLE public.payplans (
    pplan_id serial NOT NULL,
    fcaradm_id integer,
    pplan_year numeric(4,0),
    pplan_payqty integer NOT NULL,
    pplan_payamount numeric,
    pplan_desc varchar(191) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table persons_checks (OID = 28253) :
--
CREATE TABLE public.persons_checks (
    person_id integer,
    percheck_id serial NOT NULL,
    percchek_time timestamp without time zone,
    percchek_code_marc varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table pre_requirements (OID = 28259) :
--
CREATE TABLE public.pre_requirements (
    preq_id integer NOT NULL,
    subj_id integer NOT NULL,
    prereq_id integer NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table professors (OID = 28265) :
--
CREATE TABLE public.professors (
    person_year_start numeric,
    person_observation varchar(255),
    person_status varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
INHERITS (persons)
WITH (oids = false);
--
-- Structure for table profile_type (OID = 28273) :
--
CREATE TABLE public.profile_type (
    proftype_id serial NOT NULL,
    proftype_description varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table promos (OID = 28282) :
--
CREATE TABLE public.promos (
    promo_id serial NOT NULL,
    name varchar(255) NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    beneficio numeric NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table providers (OID = 28291) :
--
CREATE TABLE public.providers (
    prov_status boolean NOT NULL,
    prov_obs varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
INHERITS (persons)
WITH (oids = false);
--
-- Structure for table provider_accounts (OID = 28300) :
--
CREATE TABLE public.provider_accounts (
    provacc_id serial NOT NULL,
    person_id integer,
    provacc_description varchar(255) NOT NULL,
    provacc_expiration date NOT NULL,
    provacc_status boolean NOT NULL,
    provacc_amountpaid double precision,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table required_docs (OID = 28309) :
--
CREATE TABLE public.required_docs (
    reqdoc_id serial NOT NULL,
    proftype_id integer,
    reqdoc_description varchar(255) NOT NULL,
    reqdoc_doctype varchar(255) NOT NULL,
    reqdoc_qty integer NOT NULL,
    reqdoc_required boolean NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table scholarship (OID = 28321) :
--
CREATE TABLE public.scholarship (
    scholar_id serial NOT NULL,
    scholar_name varchar(255) NOT NULL,
    scholar_discount numeric,
    scholar_condition varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table schools (OID = 28333) :
--
CREATE TABLE public.schools (
    school_id serial NOT NULL,
    city_id integer,
    school_name varchar(255) NOT NULL,
    school_code varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table semesters (OID = 28343) :
--
CREATE TABLE public.semesters (
    sems_id integer NOT NULL,
    sems_name varchar(255) NOT NULL,
    career_id integer NOT NULL,
    sems_year numeric NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table students (OID = 28352) :
--
CREATE TABLE public.students (
    stu_status varchar(255) NOT NULL,
    stu_gradyear numeric NOT NULL,
    stu_obs varchar(255) NOT NULL,
    stu_allergies varchar(255),
    stu_instsupport varchar(255),
    school_id integer,
    hstitle_id integer,
    stu_status2 varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
INHERITS (persons)
WITH (oids = false);
--
-- Structure for table student_account (OID = 28361) :
--
CREATE TABLE public.student_account (
    staccount_id serial NOT NULL,
    person_id integer,
    enroll_id integer,
    oper_id integer,
    staccount_quot_number smallint NOT NULL,
    staccount_quot_amount double precision NOT NULL,
    staccount_description varchar(255) NOT NULL,
    staccount_start_period date NOT NULL,
    staccount_end_period date NOT NULL,
    staccount_expiration date NOT NULL,
    staccount_status boolean NOT NULL,
    staccount_amount_paid double precision,
    staccount_type varchar(50),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table student_healthcare (OID = 28370) :
--
CREATE TABLE public.student_healthcare (
    stuhcare_id serial NOT NULL,
    person_id integer,
    hcare_id integer,
    stuhcare_carnet varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table student_monitoring (OID = 28379) :
--
CREATE TABLE public.student_monitoring (
    stmonit_id serial NOT NULL,
    person_id integer,
    caller_id integer,
    stmonit_calldate date,
    stmonit_obs text,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table student_retention (OID = 28391) :
--
CREATE TABLE public.student_retention (
    student_ret_id serial NOT NULL,
    person_id integer,
    civil_status varchar(255) NOT NULL,
    sons numeric NOT NULL,
    sibling numeric NOT NULL,
    emfermedad varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table student_scholarship (OID = 28401) :
--
CREATE TABLE public.student_scholarship (
    scholar_id integer,
    person_id integer,
    scholar_year integer,
    scholar_status integer,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table student_workplaces (OID = 28405) :
--
CREATE TABLE public.student_workplaces (
    wplace_id integer,
    person_id integer,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table subjects (OID = 28410) :
--
CREATE TABLE public.subjects (
    subj_id serial NOT NULL,
    subj_code varchar(255) NOT NULL,
    subj_name varchar(255) NOT NULL,
    subj_durationhs integer NOT NULL,
    subj_weeklyhs integer NOT NULL,
    sems_id integer NOT NULL,
    subj_mattertype varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table subject_evaluation (OID = 28422) :
--
CREATE TABLE public.subject_evaluation (
    subeval_id serial NOT NULL,
    subeval_name varchar(255),
    subeval_total integer,
    subeval_earned integer,
    subeval_date date,
    subeval_exam boolean,
    subeval_spprice double precision,
    evaltype_id integer,
    carsubj_id integer,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table tariffs (OID = 28430) :
--
CREATE TABLE public.tariffs (
    tariff_id serial NOT NULL,
    fcaradm_id integer NOT NULL,
    tariff_payamount numeric,
    tariff_caryear numeric DEFAULT 2020 NOT NULL,
    tariff_regamount numeric,
    tariff_obs text,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone,
    CONSTRAINT ckc_tariff_caryear_tariffs CHECK (((tariff_caryear >= (2000)::numeric) AND (tariff_caryear <= (2100)::numeric)))
)
WITH (oids = false);
--
-- Structure for table tickets (OID = 28444) :
--
CREATE TABLE public.tickets (
    ticket_id serial NOT NULL,
    person_id integer,
    ticket_date date,
    ticket_number varchar(191),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table ticket_details (OID = 28452) :
--
CREATE TABLE public.ticket_details (
    tickdet_id serial NOT NULL,
    ticket_id integer NOT NULL,
    tdtype_id integer NOT NULL,
    tickdet_monto numeric NOT NULL,
    tickdet_iva numeric NOT NULL,
    tickdet_qty integer NOT NULL,
    tickdet_desciption varchar(255),
    tickdet_ref integer NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table ticket_details_types (OID = 28463) :
--
CREATE TABLE public.ticket_details_types (
    tdtype_id serial NOT NULL,
    tdtype_desc varchar(191) NOT NULL,
    tdtype_obs varchar(191),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table tills (OID = 28472) :
--
CREATE TABLE public.tills (
    till_id serial NOT NULL,
    unit_id integer,
    till_name varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table tills_details (OID = 28481) :
--
CREATE TABLE public.tills_details (
    dettills_id serial NOT NULL,
    till_id integer,
    person_id integer,
    ticket_id integer,
    dettills_description varchar(255) NOT NULL,
    dettills_date date NOT NULL,
    dettills_amount double precision NOT NULL,
    ticket_number varchar(50),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table till_transfers (OID = 28491) :
--
CREATE TABLE public.till_transfers (
    tilltrans_id serial NOT NULL,
    origin_till_id integer,
    destiny_till_id integer,
    tilltrans_date date NOT NULL,
    tilltrans_amount double precision NOT NULL,
    tilltrans_observation text,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table titles (OID = 28503) :
--
CREATE TABLE public.titles (
    title_id serial NOT NULL,
    person_id integer,
    title_description varchar(255) NOT NULL,
    title_type_title varchar(255) NOT NULL,
    title_status boolean,
    title_doument varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table units (OID = 28515) :
--
CREATE TABLE public.units (
    unit_id serial NOT NULL,
    unit_name varchar(255) NOT NULL,
    unit_code varchar(255) NOT NULL,
    unit_logo varchar(255) NOT NULL,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Structure for table workplace (OID = 28527) :
--
CREATE TABLE public.workplace (
    wplace_id serial NOT NULL,
    wplace_description varchar(255) NOT NULL,
    wplace_address varchar(255),
    wplace_telephone varchar(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone,
    deleted_at timestamp(0) with time zone
)
WITH (oids = false);
--
-- Definition for sequence countries_country_id_seq (OID = 29251) :
--
CREATE SEQUENCE public.countries_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
--
-- Data for table public.careers (OID = 28039) (LIMIT 0,125)
--
INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (1, 'ACTUAL. Y PERFEC. DOCENTE EN EL DESARROLLO DE LA 1º INF', 'APDDI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (2, 'ADMINISTRACION GENERAL', 'ADM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (3, 'ARQUITECTURA', 'ARQ', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (4, 'CARRERA CREADA PARA PRUEBAS AG', 'CAG', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (5, 'COTILLÓN', 'COT', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (6, 'CURSO DE PRODUCCION AUDIOVISUAL', 'CPA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (7, 'DERECHO', 'DER', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (8, 'DERECHO LABORAL - CURSO PRACTICO', 'CPDL', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (9, 'DIPLOMADO EN ADMINISTRACION DE EMPRESAS', 'DAE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (10, 'DIPLOMADO EN ADMINISTRACION DE NEGOCIOS', 'DAN', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (11, 'DIPLOMADO EN AGRONEGOCIOS', 'DA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (12, 'DIPLOMADO EN ALTA GERENCIA', 'DAG', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (13, 'DIPLOMADO EN ARBITRAJE', 'DAR', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (14, 'DIPLOMADO EN ATENCION INTEGRAL DE LA SALUD', 'DAIS', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (15, 'DIPLOMADO EN CEREMONIAL Y PROTOCOLO', 'DCP', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (16, 'DIPLOMADO EN CIENCIAS FORENSES', 'CF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (17, 'DIPLOMADO EN COMUNICACIÓN POLÍTICA', 'DCO', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (18, 'DIPLOMADO EN CONTROL INTERNO Y PREVENCIÓN DE FRAUDES', 'DCI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (19, 'DIPLOMADO EN DERECHO CONSTITUCIONAL COMPARADO', 'DCC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (20, 'DIPLOMADO EN DERECHO CONSTITUCIONAL Y PROCESAL CONSTITUCIONAL', 'DCPC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (21, 'DIPLOMADO EN DERECHO DEL TRABAJO Y LAS RELACIONES LABORALES EN LAS EMPRESAS DEL SIGLO XXI', 'DDT', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (22, 'DIPLOMADO EN DERECHO LABORAL', 'DDL', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (23, 'DIPLOMADO EN DERECHO PROCESAL LABORAL', 'DDPL', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (24, 'DIPLOMADO EN DISEÑO DE PAGINAS WEB', 'DDPW', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (25, 'DIPLOMADO EN EDICION Y PRODUCCION AUDIOVISUAL', 'DEPA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (26, 'DIPLOMADO EN ENTRENAMIENTO', 'DEN', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (27, 'DIPLOMADO EN ENTRENAMIENTO DEPORTIVO', 'DEP', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (28, 'DIPLOMADO EN EPIDEMIOLOGIA DE ENFERMEDADES TRANSMITIDAS POR DENGUE', 'DEETV', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (29, 'DIPLOMADO EN EPIDEMIOLOGIA Y SALUD COMUNITARIA', 'DESC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (30, 'DIPLOMADO EN GESTION DE ORGANIZACIONES CON PERSPECTIVA DE GENERO', 'DGOPG', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (31, 'DIPLOMADO EN MARKETING DIGITAL', 'DMD', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (32, 'DIPLOMADO EN MARKETING Y GESTION DE NEGOCIOS', 'DMGN', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (33, 'DIPLOMADO EN MEDIACION', 'DM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (34, 'DIPLOMADO EN MEDIACION EN PROPIEDAD INTELECTUAL', 'DMPI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (35, 'DIPLOMADO EN MEDICINA PREVENTIVA', 'DMP', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (122, 'TÉCNICO SUPERIOR EN GESTIÓN CONTABLE', 'TGC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (36, 'DIPLOMADO EN MEJORES PRÁCTICAS EN CONTROL INTERNO Y PREVENCIÓN DE FRAUDES', 'DPF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (37, 'DIPLOMADO EN NEGOCIACIÓN', 'DN', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (38, 'DIPLOMADO EN NEUROCIENCIA EDUCACIONAL', 'DNE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (39, 'DIPLOMADO EN PRACTICA JURIDICA LABORAL', 'DPJL', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (40, 'DIPLOMADO EN TERAPIA COGNITIVO CONDUCTUAL', 'DTCC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (41, 'DIPLOMADO EN TURISMO', 'DT', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (42, 'DIPLOMADO INTENSIVO EN DERECHOS HUMANOS', 'DDH', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (43, 'DIPLOMADO INTERNACIONAL EN COOPERATIVISMO', 'DIC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (44, 'DIPLOMADO SUPERIOR EN PREVENCION, DIAGNOSTICO Y ASISTENCIA EN ADICCIONES', 'DPA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (45, 'DIPLOMADO UNIVERSITARIO EN NEGOCIOS INTERNACIONALES Y FINANZAS', 'DNI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (46, 'DIRECCION TECNICA DE FUTBOL', 'DTF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (47, 'DISEÑO DE MODAS', 'DDM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (48, 'DOCTORADO EN CONTABILIDAD Y AUDITORÍA', 'DCA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (49, 'DOCTORADO EN EDUCACION Y DESARROLLO HUMANO', 'DE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (50, 'EDUCACION MEDIA', 'EM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (51, 'ELECTRICIDAD DE MOTOS', 'EMT', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (52, 'ELECTRICIDAD DEL AUTOMÓVIL', 'EAUT', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (53, 'ESPECIALIZACION EN ADMINISTRACION DE NEGOCIOS', 'EAE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (54, 'ESPECIALIZACION EN AUDITORIA', 'EA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (55, 'ESPECIALIZACION EN AUDITORIA Y CONTROL', 'EAC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (56, 'ESPECIALIZACION EN CIENCIAS FORENSES', 'ECF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (57, 'ESPECIALIZACION EN DIDACTICA SUPERIOR UNIVERSITARIA', 'DIUN', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (58, 'ESPECIALIZACION EN EDUCACION INCLUSIVA', 'EEI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (59, 'ESPECIALIZACIÓN EN EVALUACIÓN EDUCACIONAL', 'EEE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (60, 'ESPECIALIZACIÓN EN GESTIÓN Y ADMINISTRACIÓN EDUCACIONAL', 'EGAE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (61, 'ESPECIALIZACION EN METODOLOGIA DE LA INVESTIGACION EDUCATIVA', 'EMIE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (62, 'ESPECIALIZACION EN NEGOCIACION Y MEDIACION', 'ENM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (63, 'ESPECIALIZACIÓN EN NEUROCIENCIAS Y COGNICIÓN', 'ENC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (64, 'ESPECIALIZACIÓN EN ORIENTACIÓN', 'EO', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (65, 'ESPECIALIZACIÓN EN REHABILITACIÓN FÍSICA KINÉSICA', 'ERF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (66, 'ESPECIALIZACIÓN PARA EL TERCER CICLO', 'ETC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (67, 'FARMACIA', 'LFA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (68, 'FONTANERÍA', 'FON', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (69, 'FORMACION PEDAGOGICA 2009', 'FP2009', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (70, 'HABILITACIÓN PEDAGÓGICA PARA EL NIVEL MEDIO', 'FP', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (71, 'INGENIERIA COMERCIAL', 'IC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (72, 'LIC CIENCIAS DE LA EDUC C/ E. EN CIENCIAS AMBIENTALES', 'LCECA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (73, 'LIC EN CIENCIAS DE LA EDUCACION CON ENF. EN TECNOLOGIA E INFORMATICA', 'LCETI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (74, 'LIC. CIENCIAS DE LA EDUC. C/ENF EN EVALUACION EDUCACIONAL', 'LCSEE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (75, 'LIC. EN ADM. DE EMPRESAS AGROPECUARIAS', 'LADA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (76, 'LICENCIATURA EN ADMINISTRACION DE EMPRESAS', 'LADE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (77, 'LICENCIATURA EN ANALISIS DE SISTEMAS INFORMATICOS', 'LASI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (78, 'LICENCIATURA EN CIENCIAS CONTABLES', 'LCC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (79, 'LICENCIATURA EN CIENCIAS DE LA EDUCACION CON ENF EN CIENCIAS SOCIALES', 'LCS', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (80, 'LICENCIATURA EN CIENCIAS DE LA EDUCACIÓN CON ÉNFASIS EN ADMINISTRACIÓN EDUCACIONAL', 'LCEA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (81, 'LICENCIATURA EN CIENCIAS DE LA EDUCACIÓN CON ÉNFASIS EN EVALUACIÓN', 'LCEE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (82, 'LICENCIATURA EN COMERCIO INTERNACIONAL', 'LCI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (83, 'LICENCIATURA EN DISEÑO DE MODAS', 'LDM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (84, 'LICENCIATURA EN DISEÑO GRAFICO', 'LDG', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (85, 'LICENCIATURA EN EDUCACION EN CIENCIAS SOCIALES', 'LECS', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (86, 'LICENCIATURA EN EDUCACIÓN FÍSICA', 'LEA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (87, 'LICENCIATURA EN EDUCACION INICIAL', 'LEI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (88, 'LICENCIATURA EN HOTELERIA Y TURISMO', 'LHT', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (89, 'LICENCIATURA EN MARKETING', 'LM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (90, 'LICENCIATURA EN PSICOPEDAGOGIA', 'LPP', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (91, 'LICENCIATURA EN RELACIONES PUBLICAS', 'LRP', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (92, 'LICENCIATURA EN TRABAJO SOCIAL', 'LTS', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (93, 'MAESTRIA EN ADMINISTACION DE NEGOCIOS', 'MADN', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (94, 'MAESTRIA EN AUDITORIA', 'MA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (95, 'MAESTRIA EN AUDITORIA Y CONTROL', 'MAC', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (96, 'MAESTRIA EN CIENCIAS FORENSES', 'MCF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (97, 'MAESTRIA EN DERECHO PENAL Y PROCESAL PENAL', 'MSPPP', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (98, 'MAESTRIA EN DOCENCIA Y GESTION UNIVERSITARIA', 'MDGU', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (99, 'MAESTRÍA EN EDUCACIÓN Y DESARROLLO', 'MED', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (100, 'MAESTRIA EN EDUCACION Y DESARROLLO HUMANO', 'MEDH', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (101, 'MAESTRIA EN EDUCACION Y DESARROLLO HUMANO', 'MEDU', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (102, 'MAESTRIA EN POLITICAS PUBLICAS Y GENERO', 'MPPG', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (103, 'MANUALIDADES', 'MAN', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (104, 'MECANICA DE MOTOS 2009', 'MM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (105, 'NIVEL INICIAL ESPERANCITA', 'NIE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (106, 'PRIMER CICLO ESC. DIVINA ESPERANZA', '1CDE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (107, 'PROFESORADO DE EDUCACIÓN FÍSICA', 'PEF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (108, 'PROFESORADO DE INFORMATICA', 'PDI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (109, 'PROFESORADO EN EDUCACIÓN ARTÍSTICA', 'PEA', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (110, 'PROFESORADO EN EDUCACIÓN ESCOLAR BÁSICA 1º Y 2º CICLOS', 'PEEB', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (111, 'PROFESORADO NIVEL MEDIO - CIENCIAS SOCIALES', 'PNMCS', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (112, 'PROFESORADO NIVEL MEDIO - MATEMÁTICA', 'PNMM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (113, 'SEGUNDO CICLO ESC. DIVINA ESPERANZA', '2CDE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (114, 'TEC. EN ADMINISTRACION HOTELERA', 'TAH', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (115, 'TECN. SUP. EN RELACIONES PUBLICAS E INSTITUCIONALES', 'TRPI', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (116, 'TECNICATURA SUPERIOR EN GASTRONOMIA', 'TG', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (117, 'TÉCNICO SUPERIOR ASISTENTE SOCIAL', 'TSAS', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (118, 'TÉCNICO SUPERIOR EN DISEÑO DE MODAS', 'TSDM', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (119, 'TÉCNICO SUPERIOR EN DISEÑO GRÁFICO', 'TSDG', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (120, 'TECNICO SUPERIOR EN ENFERMERIA', 'TSE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (121, 'TECNICO SUPERIOR EN FARMACIA', 'TSF', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (123, 'TÉCNICO SUPERIOR EN MARKETING', 'TMK', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (124, 'TERCER CICLO ESC. DIVINA ESPERANZA', '3CDE', 0, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO careers (career_id, career_name, career_code, career_semsqity, career_logo, created_at, updated_at, deleted_at)
VALUES (125, 'VETERINARIA', 'VET', 0, 'blank.jpg', NULL, NULL, NULL);

--
-- Data for table public.cities (OID = 28075) (LIMIT 0,250)
--
INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (1, 18, 'ASUNCION', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (2, 1, 'CONCEPCION', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (3, 1, 'BELEN', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (4, 1, 'HORQUETA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (5, 1, 'LORETO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (6, 1, 'SAN CARLOS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (7, 1, 'SAN LAZARO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (8, 1, 'YVY YA´U', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (9, 1, 'AZOTEY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (10, 1, 'SGTO. JOSE FELIX LOPEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (11, 1, 'SAN ALFREDO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (12, 1, 'PASO BARRETO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (13, 2, 'SAN PEDRO DEL YKUAMANDIYU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (14, 2, 'ANTEQUERA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (15, 2, 'CHORE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (16, 2, 'GENERAL ELIZARDO AQUINO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (17, 2, 'ITACURUBI DEL ROSARIO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (18, 2, 'LIMA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (19, 2, 'NUEVA GERMANIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (20, 2, 'SAN ESTANISLAO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (21, 2, 'SAN PABLO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (22, 2, 'TACUATI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (23, 2, 'UNION', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (24, 2, '25 DE DICIEMBRE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (25, 2, 'VILLA DEL ROSARIO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (26, 2, 'GENERAL RESQUIN', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (27, 2, 'YATAITY DEL NORTE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (28, 2, 'GUAJAYVI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (29, 2, 'CAPIIBARY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (30, 2, 'SANTA ROSA DEL AGUARAY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (31, 2, 'YRYVU CUA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (32, 2, 'LIBERACION', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (33, 3, 'CAACUPE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (34, 3, 'ALTOS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (35, 3, 'ARROYOS Y ESTEROS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (36, 3, 'ATYRA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (37, 3, 'CARAGUATAY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (38, 3, 'EMBOSCADA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (39, 3, 'EUSEBIO AYALA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (40, 3, 'ISLA PUCU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (41, 3, 'ITACURUBI DE LA CORDILLERA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (42, 3, 'JUAN DE MENA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (43, 3, 'LOMA GRANDE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (44, 3, 'MBOCAYATY DEL YHAGUY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (45, 3, 'NUEVA COLOMBIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (46, 3, 'PIRIBEBUY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (47, 3, 'PRIMERO DE MARZO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (48, 3, 'SAN BERNARDINO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (49, 3, 'SANTA ELENA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (50, 3, 'TOBATI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (51, 3, 'VALENZUELA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (52, 3, 'SAN JOSE OBRERO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (53, 4, 'VILLARRICA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (54, 4, 'BORJA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (55, 4, 'MAURICIO JOSE TROCHE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (56, 4, 'CORONEL MARTINEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (57, 4, 'FELIX PEREZ CARDOZO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (58, 4, 'GENERAL EUGENIO A. GARAY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (59, 4, 'COLONIA INDEPENDENCIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (60, 4, 'ITAPE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (61, 4, 'ITURBE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (62, 4, 'JOSE FASSARDI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (63, 4, 'MBOCAYATY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (64, 4, 'NATALICIO TALAVERA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (65, 4, 'ÑUMI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (66, 4, 'SAN SALVADOR', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (67, 4, 'YATAITY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (68, 4, 'DR. BOTTRELL', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (69, 4, 'PASO YOBAI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (70, 4, 'TEBICUARY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (71, 5, 'CORONEL OVIEDO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (72, 5, 'CAAGUAZU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (73, 5, 'CARAYAO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (74, 5, 'CECILIO BAEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (75, 5, 'SANTA ROSA DEL MBUTUY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (76, 5, 'DR. JUAN MANUEL FRUTOS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (77, 5, 'REPATRIACION', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (78, 5, 'NUEVA LONDRES', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (79, 5, 'SAN JOAQUIN', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (80, 5, 'SAN JOSE DE LOS ARROYOS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (81, 5, 'YHU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (82, 5, 'J EULOGIO ESTIGARRIBIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (83, 5, 'R.I. 3 CORRALES', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (84, 5, 'RAUL ARSENIO OVIEDO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (85, 5, 'JOSE DOMINGO OCAMPOS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (86, 5, 'MCAL. FRANCISCO SOLANO LOPEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (87, 5, 'LA PASTORA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (88, 5, '3 DE FEBRERO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (89, 5, 'SIMON BOLIVAR', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (90, 5, 'VAQUERIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (91, 5, 'TEMBIAPORA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (92, 5, 'NUEVA TOLEDO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (93, 6, 'CAAZAPA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (94, 6, 'ABAI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (95, 6, 'BUENA VISTA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (96, 6, 'MOISES BERTONI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (97, 6, 'GENERAL HIGINIO MORINIGO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (98, 6, 'MACIEL', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (99, 6, 'SAN JUAN NEPOMUCENO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (100, 6, 'TAVAI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (101, 6, 'FULGENCIO YEGROS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (102, 6, 'YUTY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (103, 6, '3 DE MAYO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (104, 7, 'ENCARNACION', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (105, 7, 'BELLA VISTA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (106, 7, 'CAMBYRETA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (107, 7, 'CAPITAN MEZA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (108, 7, 'CAPITAN MIRANDA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (109, 7, 'NUEVA ALBORADA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (110, 7, 'CARMEN DEL PARANA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (111, 7, 'CORONEL BOGADO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (112, 7, 'CARLOS ANTONIO LOPEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (113, 7, 'NATALIO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (114, 7, 'FRAM', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (115, 7, 'GENERAL ARTIGAS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (116, 7, 'GENERAL DELGADO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (117, 7, 'HOHENAU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (118, 7, 'JESUS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (119, 7, 'LEANDRO OVIEDO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (120, 7, 'OBLIGADO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (121, 7, 'MAYOR OTAÐO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (122, 7, 'SAN COSME Y DAMIAN', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (123, 7, 'SAN PEDRO DEL PARANA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (124, 7, 'SAN RAFAEL DEL PARANA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (125, 7, 'TRINIDAD', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (126, 7, 'EDELIRA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (127, 7, 'TOMAS ROMERO PEREIRA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (128, 7, 'ALTO VERA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (129, 7, 'LA PAZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (130, 7, 'YATYTAY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (131, 7, 'SAN JUAN DEL PARANA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (132, 7, 'PIRAPO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (133, 7, 'ITAPUA POTY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (134, 8, 'SAN JUAN BAUTISTA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (135, 8, 'AYOLAS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (136, 8, 'SAN IGNACIO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (137, 8, 'SAN MIGUEL', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (138, 8, 'SAN PATRICIO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (139, 8, 'SANTA MARIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (140, 8, 'SANTA ROSA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (141, 8, 'SANTIAGO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (142, 8, 'VILLA FLORIDA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (143, 8, 'YABEBYRY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (144, 9, 'PARAGUARI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (145, 9, 'ACAHAY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (146, 9, 'CAAPUCU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (147, 9, 'GENERAL BERNARDINO CABALLERO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (148, 9, 'CARAPEGUA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (149, 9, 'ESCOBAR', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (150, 9, 'LA COLMENA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (151, 9, 'MBUYAPEY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (152, 9, 'PIRAYU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (153, 9, 'QUIINDY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (154, 9, 'QUYQUYHO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (155, 9, 'SAN ROQUE GONZALEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (156, 9, 'SAPUCAI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (157, 9, 'TEBICUARY-MI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (158, 9, 'YAGUARON', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (159, 9, 'YBYCUI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (160, 9, 'YVYTIMI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (161, 10, 'CIUDAD DEL ESTE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (162, 10, 'PRESIDENTE FRANCO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (163, 10, 'DOMINGO MARTINEZ DE IRALA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (164, 10, 'DR. JUAN LEON MALLORQUIN', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (165, 10, 'HERNANDARIAS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (166, 10, 'ITAKYRY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (167, 10, 'JUAN E. OLEARY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (168, 10, 'ÑACUNDAY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (169, 10, 'YGUAZU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (170, 10, 'LOS CEDRALES', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (171, 10, 'MINGA GUAZU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (172, 10, 'SAN CRISTOBAL', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (173, 10, 'SANTA RITA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (174, 10, 'NARANJAL', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (175, 10, 'SANTA ROSA DEL MONDAY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (176, 10, 'MINGA PORA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (177, 10, 'MBARACAYU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (178, 10, 'SAN ALBERTO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (179, 10, 'IRUÑA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (180, 10, 'SANTA FE DEL PARANA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (181, 10, 'TAVAPY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (182, 10, 'DR. RAUL PEÑA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (183, 11, 'AREGUA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (184, 11, 'CAPIATA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (185, 11, 'FERNANDO DE LA MORA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (186, 11, 'GUARAMBARE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (187, 11, 'ITA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (188, 11, 'ITAUGUA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (189, 11, 'LAMBARE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (190, 11, 'LIMPIO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (191, 11, 'LUQUE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (192, 11, 'MARIANO ROQUE ALONSO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (193, 11, 'NUEVA ITALIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (194, 11, 'ÐEMBY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (195, 11, 'SAN ANTONIO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (196, 11, 'SAN LORENZO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (197, 11, 'VILLA ELISA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (198, 11, 'VILLETA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (199, 11, 'YPACARAI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (200, 11, 'YPANE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (201, 11, 'J AUGUSTO SALDIVAR', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (202, 12, 'PILAR', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (203, 12, 'ALBERDI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (204, 12, 'CERRITO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (205, 12, 'DESMOCHADOS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (206, 12, 'GENERAL DIAZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (207, 12, 'GUAZU CUA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (208, 12, 'HUMAITA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (209, 12, 'ISLA UMBU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (210, 12, 'LOS LAURELES', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (211, 12, 'MAYOR MARTINEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (212, 12, 'PASO DE PATRIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (213, 12, 'SAN JUAN BAUTISTA DE ÐEEMBUCU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (214, 12, 'TACUARAS', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (215, 12, 'VILLA FRANCA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (216, 12, 'VILLA OLIVA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (217, 12, 'VILLALBIN', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (218, 13, 'PEDRO JUAN CABALLERO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (219, 13, 'BELLA VISTA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (220, 13, 'CAPITAN BADO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (221, 13, 'ZANJA PYTA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (222, 13, 'KARAPA´I', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (223, 14, 'SALTOS DEL GUAIRA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (224, 14, 'CORPUS CHRISTI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (225, 14, 'CURUGUATY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (226, 14, 'VILLA YGATIMI', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (227, 14, 'ITANARA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (228, 14, 'YPE JHU', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (229, 14, 'FRANCISCO CABALLERO ALVAREZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (230, 14, 'KATUETE', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (231, 14, 'LA PALOMA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (232, 14, 'NUEVA ESPERANZA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (233, 14, 'YASY KAÑY', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (234, 14, 'YBYRAROBANA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (235, 14, 'YBY PYTA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (236, 15, 'BENJAMIN ACEVAL', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (237, 15, 'PUERTO PINASCO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (238, 15, 'VILLA HAYES', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (239, 15, 'NANAWA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (240, 15, 'JOSE FALCON', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (241, 15, 'TTE 1RO MANUEL IRALA FERNANDEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (242, 15, 'TTE. ESTEBAN MARTINEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (243, 15, 'GRAL JOSE MARIA BRUGUEZ', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (244, 16, 'MARISCAL ESTIGARRIBIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (245, 16, 'FILADELFIA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (246, 16, 'LOMA PLATA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (247, 17, 'FUERTE OLIMPO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (248, 17, 'PUERTO CASADO', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (249, 17, 'BAHIA NEGRA', NULL, NULL, NULL);

INSERT INTO cities (city_id, depart_id, city_name, created_at, updated_at, deleted_at)
VALUES (250, 17, 'CARMELO PERALTA', NULL, NULL, NULL);

--
-- Data for table public.countries (OID = 28100) (LIMIT 0,5)
--
INSERT INTO countries (country_id, country_name, country_code, created_at, updated_at, deleted_at)
VALUES (1, 'PARAGUAY', 'PY', NULL, NULL, NULL);

INSERT INTO countries (country_id, country_name, country_code, created_at, updated_at, deleted_at)
VALUES (2, 'ARGENTINA', 'AR', NULL, NULL, NULL);

INSERT INTO countries (country_id, country_name, country_code, created_at, updated_at, deleted_at)
VALUES (3, 'BRASIL', 'BR', NULL, NULL, NULL);

INSERT INTO countries (country_id, country_name, country_code, created_at, updated_at, deleted_at)
VALUES (4, 'BOLIVIA', 'BO', NULL, NULL, NULL);

INSERT INTO countries (country_id, country_name, country_code, created_at, updated_at, deleted_at)
VALUES (5, 'URUGUAY', 'UY', NULL, NULL, NULL);

--
-- Data for table public.departaments (OID = 28112) (LIMIT 0,18)
--
INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (1, 1, 'CONCEPCION', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (2, 1, 'SAN PEDRO', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (3, 1, 'CORDILLERA', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (4, 1, 'GUAIRA', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (5, 1, 'CAAGUAZU', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (6, 1, 'CAAZAPA', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (7, 1, 'ITAPUA', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (8, 1, 'MISIONES', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (9, 1, 'PARAGUARI', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (10, 1, 'ALTO PARANA', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (11, 1, 'CENTRAL', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (12, 1, 'ÑEEMBUCU', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (13, 1, 'AMAMBAY', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (14, 1, 'CANINDEYU', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (15, 1, 'PRESIDENTE HAYES', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (16, 1, 'BOQUERON', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (17, 1, 'ALTO PARAGUAY', NULL, NULL, NULL);

INSERT INTO departaments (depart_id, country_id, depart_name, created_at, updated_at, deleted_at)
VALUES (18, 1, 'ASUNCION', NULL, NULL, NULL);

--
-- Data for table public.faculties (OID = 28175) (LIMIT 0,22)
--
INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (1, 'ADMINISTRACION GENERAL', 'ADM', 1, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (2, 'EDUCACION MEDIA', 'EM', 2, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (3, 'EDUCACION ESCOLAR BASICA DIVINA ESPERANZA', 'EEBDE', 3, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (4, 'NIVEL INICIAL ESPERANCITA', 'ENI', 3, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (5, 'FORMACIÓN PROFESIONAL', 'PRO', 4, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (6, 'FORMACIÓN DOCENTE CONTÍNUA', 'FDC', 5, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (7, 'FORMACIÓN DOCENTE INICIAL', 'FDI', 5, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (8, 'LICENCIATURAS', 'LIC', 5, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (9, 'PROFESIONALIZACIÓN', 'PROF', 5, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (10, 'FACULTAD DE CIENCIAS DE LA SALUD', 'FCDLS', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (11, 'FACULTAD DE CIENCIAS EMPRESARIALES', 'FCE', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (12, 'FACULTAD DE CIENCIA, ARTE Y TECNOLOGIA', 'FCIT', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (13, 'FACULTAD DE CIENCIAS JURÍDICAS, HUMANAS Y SOCIALES', 'FCJS', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (14, 'FACULTAD DE CIENCIAS SOCIALES Y HUMANIDADES', 'FCSH', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (15, 'LICENCIATURA EN CIENCIAS CONTABLES', 'GC', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (16, 'MAESTRIA EN EDUCACION Y DESARROLLO HUMANO', 'MEDH', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (17, 'INGENIERIA COMERCIAL', 'MK', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (18, 'POSGRADO', 'PG', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (19, 'RECTORADO', 'REC', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (20, 'LICENCIATURA EN TRABAJO SOCIAL', 'TS', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (21, 'FACULTAD CREADA PARA PRUEBAS AG', 'FAG', 7, 'blank.jpg', NULL, NULL, NULL);

INSERT INTO faculties (facu_id, facu_name, facu_code, facu_unit_id, facu_logo, created_at, updated_at, deleted_at)
VALUES (49, 'test', 'test', 2, 'qYP4mliCbqMGus2j9pwm.jpg', '2020-03-31 16:29:18-04', '2020-03-31 16:29:18-04', NULL);

--
-- Data for table public.hscholl_titles (OID = 28205) (LIMIT 0,27)
--
INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (1, 'BCCS', 'Bachillerato Científico con Énfasis en Ciencias Sociales', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (2, 'BCLA', 'Bachillerato Científico con Énfasis en Letras y Artes', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (3, 'BCCB', 'Bachillerato Científico con Énfasis en Ciencias Básicas', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (4, 'BTAN', 'Bachillerato Técnico en Administración de Negocios', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (5, 'BTAM', 'Bachillerato Técnico en Agromecánica', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (6, 'BTAG', 'Bachillerato Técnico Agropecuario', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (7, 'BTAD', 'Bachillerato Técnico en Asistencia deportiva', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (8, 'BTCA', 'Bachillerato Técnico en Ciencias Ambientales', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (9, 'BTCI', 'Bachillerato Técnico en Confección Industrial', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (10, 'BTCC', 'Bachillerato Técnico en Construcciones Civiles', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (11, 'BTC', 'Bachillerato Técnico en Contabilidad', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (12, 'BTDG', 'Bachillerato Técnico en Diseño Gráfico y Publicidad', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (13, 'BTEA', 'Bachillerato Técnico en Elaboración de alimentos', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (14, 'BTEL', 'Bachillerato Técnico en Electricidad', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (15, 'BTEM', 'Bachillerato Técnico en Electromecánica', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (16, 'BTE', 'Bachillerato Técnico en Electrónica', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (17, 'BTET', 'Bachillerato Técnico en Electrotecnia', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (18, 'BTHT', 'Bachillerato Técnico en Hoteleria y Turismo', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (19, 'BTI', 'Bachillerato Técnico en Informática', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (20, 'BTMA', 'Bachillerato Técnico en Mecánica Automotriz', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (21, 'BTMG', 'Bachillerato Técnico en Mecánica General', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (22, 'BTMT', 'Bachillerato Técnico en Mercadotecnia', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (23, 'BTMM', 'Bachillerato Técnico en Metalmecánica', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (24, 'BTPA', 'Bachillerato Técnico en Producción Artesanal', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (25, 'BTQ', 'Bachillerato Técnico en Química', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (26, 'BTS', 'Bachillerato Técnico en Salud', NULL, NULL, NULL);

INSERT INTO hscholl_titles (hstitle_id, hstitle_code, hstitle_name, created_at, updated_at, deleted_at)
VALUES (27, 'BTTA', 'Bachillerato Técnico en Tecnología de alimentos', NULL, NULL, NULL);

--
-- Data for table public.units (OID = 28515) (LIMIT 0,7)
--
INSERT INTO units (unit_id, unit_name, unit_code, unit_logo, created_at, updated_at, deleted_at)
VALUES (1, 'ADMINISTRACION GENERAL', 'ADM', 'blank.jpg', NULL, NULL, NULL);

INSERT INTO units (unit_id, unit_name, unit_code, unit_logo, created_at, updated_at, deleted_at)
VALUES (2, 'COLEGIO DIVINA ESPERANZA', 'CDE', 'blank.jpg', NULL, NULL, NULL);

INSERT INTO units (unit_id, unit_name, unit_code, unit_logo, created_at, updated_at, deleted_at)
VALUES (3, 'ESCUELA BÁSICA DIVINA ESPERANZA Nº 7097', 'EBDE', 'blank.jpg', NULL, NULL, NULL);

INSERT INTO units (unit_id, unit_name, unit_code, unit_logo, created_at, updated_at, deleted_at)
VALUES (4, 'INSTITUTO DE FORMACIÓN PROFESIONAL DIVINA ESPERANZA', 'IPDE', 'blank.jpg', NULL, NULL, NULL);

INSERT INTO units (unit_id, unit_name, unit_code, unit_logo, created_at, updated_at, deleted_at)
VALUES (5, 'INSTITUTO SUPERIOR DE EDUCACION DIVINA ESPERANZA', 'ISEDE', 'blank.jpg', NULL, NULL, NULL);

INSERT INTO units (unit_id, unit_name, unit_code, unit_logo, created_at, updated_at, deleted_at)
VALUES (6, 'INSTITUTO TÉCNICO SUPERIOR DIVINA ESPERANZA', 'ITSDE', 'blank.jpg', NULL, NULL, NULL);

INSERT INTO units (unit_id, unit_name, unit_code, unit_logo, created_at, updated_at, deleted_at)
VALUES (7, 'UNIVERSIDAD AUTONOMA DE ENCARNACION', 'UNAE', 'blank.jpg', NULL, NULL, NULL);

--
-- Definition for index person_ci_idx (OID = 28017) :
--
CREATE UNIQUE INDEX person_ci_idx ON public.persons USING btree (birthplace_id);
--
-- Definition for index ruc_idx (OID = 28018) :
--
CREATE INDEX ruc_idx ON public.persons USING btree (person_ruc);
--
-- Definition for index appoin_idx (OID = 28027) :
--
CREATE INDEX appoin_idx ON public.appointments USING btree (appoin_description);
--
-- Definition for index assit_idx (OID = 28036) :
--
CREATE INDEX assit_idx ON public.assistances USING btree (enroll_id, assist_date, carsubj_id);
--
-- Definition for index careers_idx (OID = 28048) :
--
CREATE INDEX careers_idx ON public.careers USING btree (career_name);
--
-- Definition for index carrers_code (OID = 28049) :
--
CREATE UNIQUE INDEX carrers_code ON public.careers USING btree (career_code);
--
-- Definition for index career_idx (OID = 28069) :
--
CREATE UNIQUE INDEX career_idx ON public.career_type USING btree (tcareer_description);
--
-- Definition for index city_idx (OID = 28081) :
--
CREATE INDEX city_idx ON public.cities USING btree (depart_id, city_name);
--
-- Definition for index cntper_idx (OID = 28090) :
--
CREATE UNIQUE INDEX cntper_idx ON public.contact_persons USING btree (person_id);
--
-- Definition for index tcontac_idx (OID = 28099) :
--
CREATE UNIQUE INDEX tcontac_idx ON public.contact_type USING btree (cnttype_name);
--
-- Definition for index counname_idx (OID = 28108) :
--
CREATE UNIQUE INDEX counname_idx ON public.countries USING btree (country_name);
--
-- Definition for index countcode_idx (OID = 28109) :
--
CREATE UNIQUE INDEX countcode_idx ON public.countries USING btree (country_code);
--
-- Definition for index depart_idx (OID = 28118) :
--
CREATE UNIQUE INDEX depart_idx ON public.departaments USING btree (country_id, depart_name);
--
-- Definition for index docpers_idx (OID = 28127) :
--
CREATE UNIQUE INDEX docpers_idx ON public.document_persons USING btree (person_id, reqdoc_id);
--
-- Definition for index emplo_idx (OID = 28134) :
--
CREATE INDEX emplo_idx ON public.employees USING btree (person_startdate);
--
-- Definition for index enroll_idx (OID = 28143) :
--
CREATE UNIQUE INDEX enroll_idx ON public.enrolleds USING btree (person_id, carsubj_id);
--
-- Definition for index evstu_idx (OID = 28160) :
--
CREATE INDEX evstu_idx ON public.eval_students USING btree (enroll_id, subeval_id);
--
-- Definition for index facadm_idx (OID = 28172) :
--
CREATE INDEX facadm_idx ON public.facucar_adm USING btree (facu_career_id, fcaradm_year);
--
-- Definition for index facu_idx (OID = 28184) :
--
CREATE UNIQUE INDEX facu_idx ON public.faculties USING btree (facu_code);
--
-- Definition for index facar_idx (OID = 28190) :
--
CREATE INDEX facar_idx ON public.facu_careers USING btree (fcar_name, career_id, faculty_id);
--
-- Definition for index hcare_idx (OID = 28202) :
--
CREATE UNIQUE INDEX hcare_idx ON public.healthcare USING btree (hcare_description);
--
-- Definition for index hstitle_idx (OID = 28214) :
--
CREATE INDEX hstitle_idx ON public.hscholl_titles USING btree (hstitle_code, hstitle_name);
--
-- Definition for index tiva_idx (OID = 28226) :
--
CREATE UNIQUE INDEX tiva_idx ON public.iva_type USING btree (ivatype_valor);
--
-- Definition for index opera_idx (OID = 28238) :
--
CREATE INDEX opera_idx ON public.operations USING btree (oper_description);
--
-- Definition for index pplan_idx (OID = 28250) :
--
CREATE UNIQUE INDEX pplan_idx ON public.payplans USING btree (fcaradm_id, pplan_desc, pplan_year);
--
-- Definition for index prereq_idx (OID = 28264) :
--
CREATE INDEX prereq_idx ON public.pre_requirements USING btree (subj_id, prereq_id);
--
-- Definition for index tprof_idx (OID = 28279) :
--
CREATE UNIQUE INDEX tprof_idx ON public.profile_type USING btree (proftype_description);
--
-- Definition for index procpk_idx (OID = 28297) :
--
CREATE UNIQUE INDEX procpk_idx ON public.providers USING btree (person_id);
--
-- Definition for index provacc_idx (OID = 28306) :
--
CREATE UNIQUE INDEX provacc_idx ON public.provider_accounts USING btree (person_id);
--
-- Definition for index regdocs_idx (OID = 28318) :
--
CREATE INDEX regdocs_idx ON public.required_docs USING btree (reqdoc_description);
--
-- Definition for index schol_idx (OID = 28330) :
--
CREATE INDEX schol_idx ON public.scholarship USING btree (scholar_name);
--
-- Definition for index school_idx (OID = 28342) :
--
CREATE UNIQUE INDEX school_idx ON public.schools USING btree (school_name);
--
-- Definition for index semes_idx (OID = 28351) :
--
CREATE INDEX semes_idx ON public.semesters USING btree (sems_name, career_id, sems_year);
--
-- Definition for index student_idx (OID = 28358) :
--
CREATE UNIQUE INDEX student_idx ON public.students USING btree (person_id);
--
-- Definition for index stuacc_id (OID = 28367) :
--
CREATE INDEX stuacc_id ON public.student_account USING btree (staccount_id);
--
-- Definition for index perhcar_idx (OID = 28376) :
--
CREATE UNIQUE INDEX perhcar_idx ON public.student_healthcare USING btree (hcare_id, stuhcare_carnet);
--
-- Definition for index stumon_idx (OID = 28388) :
--
CREATE INDEX stumon_idx ON public.student_monitoring USING btree (person_id);
--
-- Definition for index sturet_idx (OID = 28400) :
--
CREATE INDEX sturet_idx ON public.student_retention USING btree (person_id);
--
-- Definition for index stuscholar_idx (OID = 28404) :
--
CREATE INDEX stuscholar_idx ON public.student_scholarship USING btree (person_id);
--
-- Definition for index subj_idx (OID = 28419) :
--
CREATE UNIQUE INDEX subj_idx ON public.subjects USING btree (subj_code);
--
-- Definition for index tariff_idx (OID = 28441) :
--
CREATE INDEX tariff_idx ON public.tariffs USING btree (fcaradm_id, tariff_caryear);
--
-- Definition for index tdettick_idx (OID = 28469) :
--
CREATE INDEX tdettick_idx ON public.ticket_details_types USING btree (tdtype_desc);
--
-- Definition for index tills_idx (OID = 28478) :
--
CREATE UNIQUE INDEX tills_idx ON public.tills USING btree (unit_id, till_name);
--
-- Definition for index tillsdet_idx (OID = 28487) :
--
CREATE INDEX tillsdet_idx ON public.tills_details USING btree (dettills_date);
--
-- Definition for index tillsperson_idx (OID = 28488) :
--
CREATE INDEX tillsperson_idx ON public.tills_details USING btree (person_id);
--
-- Definition for index ttransf_idx (OID = 28500) :
--
CREATE INDEX ttransf_idx ON public.till_transfers USING btree (tilltrans_date);
--
-- Definition for index titles_idx (OID = 28512) :
--
CREATE INDEX titles_idx ON public.titles USING btree (title_description);
--
-- Definition for index unit_idx (OID = 28524) :
--
CREATE UNIQUE INDEX unit_idx ON public.units USING btree (unit_code);
--
-- Definition for index wpla_idx (OID = 28536) :
--
CREATE UNIQUE INDEX wpla_idx ON public.workplace USING btree (wplace_description);
--
-- Definition for index pk_persons (OID = 28015) :
--
ALTER TABLE ONLY persons
    ADD CONSTRAINT pk_persons
    PRIMARY KEY (person_id);
--
-- Definition for index pk_appointments (OID = 28025) :
--
ALTER TABLE ONLY appointments
    ADD CONSTRAINT pk_appointments
    PRIMARY KEY (appoin_id);
--
-- Definition for index pk_assistances (OID = 28034) :
--
ALTER TABLE ONLY assistances
    ADD CONSTRAINT pk_assistances
    PRIMARY KEY (asisst_id);
--
-- Definition for index pk_careers (OID = 28046) :
--
ALTER TABLE ONLY careers
    ADD CONSTRAINT pk_careers
    PRIMARY KEY (career_id);
--
-- Definition for index pk_careers_subjets (OID = 28059) :
--
ALTER TABLE ONLY careers_subjets
    ADD CONSTRAINT pk_careers_subjets
    PRIMARY KEY (carsubj_id);
--
-- Definition for index pk_career_type (OID = 28067) :
--
ALTER TABLE ONLY career_type
    ADD CONSTRAINT pk_career_type
    PRIMARY KEY (tcareer_id);
--
-- Definition for index pk_cities (OID = 28079) :
--
ALTER TABLE ONLY cities
    ADD CONSTRAINT pk_cities
    PRIMARY KEY (city_id);
--
-- Definition for index pk_contact_persons (OID = 28088) :
--
ALTER TABLE ONLY contact_persons
    ADD CONSTRAINT pk_contact_persons
    PRIMARY KEY (cntper_id);
--
-- Definition for index pk_contact_type (OID = 28097) :
--
ALTER TABLE ONLY contact_type
    ADD CONSTRAINT pk_contact_type
    PRIMARY KEY (cnttype_id);
--
-- Definition for index pk_departaments (OID = 28116) :
--
ALTER TABLE ONLY departaments
    ADD CONSTRAINT pk_departaments
    PRIMARY KEY (depart_id);
--
-- Definition for index pk_document_persons (OID = 28125) :
--
ALTER TABLE ONLY document_persons
    ADD CONSTRAINT pk_document_persons
    PRIMARY KEY (docpers_id);
--
-- Definition for index pk_enrolleds (OID = 28141) :
--
ALTER TABLE ONLY enrolleds
    ADD CONSTRAINT pk_enrolleds
    PRIMARY KEY (enroll_id);
--
-- Definition for index pk_evaluation_types (OID = 28150) :
--
ALTER TABLE ONLY evaluation_types
    ADD CONSTRAINT pk_evaluation_types
    PRIMARY KEY (evaltype_id);
--
-- Definition for index pk_eval_students (OID = 28158) :
--
ALTER TABLE ONLY eval_students
    ADD CONSTRAINT pk_eval_students
    PRIMARY KEY (evstu_id);
--
-- Definition for index pk_facucar_adm (OID = 28170) :
--
ALTER TABLE ONLY facucar_adm
    ADD CONSTRAINT pk_facucar_adm
    PRIMARY KEY (fcaradm_id);
--
-- Definition for index pk_faculties (OID = 28182) :
--
ALTER TABLE ONLY faculties
    ADD CONSTRAINT pk_faculties
    PRIMARY KEY (facu_id);
--
-- Definition for index pk_facu_careers (OID = 28188) :
--
ALTER TABLE ONLY facu_careers
    ADD CONSTRAINT pk_facu_careers
    PRIMARY KEY (facu_career_id);
--
-- Definition for index pk_healthcare (OID = 28200) :
--
ALTER TABLE ONLY healthcare
    ADD CONSTRAINT pk_healthcare
    PRIMARY KEY (hcare_id);
--
-- Definition for index pk_hscholl_titles (OID = 28212) :
--
ALTER TABLE ONLY hscholl_titles
    ADD CONSTRAINT pk_hscholl_titles
    PRIMARY KEY (hstitle_id);
--
-- Definition for index pk_iva_type (OID = 28224) :
--
ALTER TABLE ONLY iva_type
    ADD CONSTRAINT pk_iva_type
    PRIMARY KEY (ivatype_id);
--
-- Definition for index pk_operations (OID = 28236) :
--
ALTER TABLE ONLY operations
    ADD CONSTRAINT pk_operations
    PRIMARY KEY (oper_id);
--
-- Definition for index pk_payplans (OID = 28248) :
--
ALTER TABLE ONLY payplans
    ADD CONSTRAINT pk_payplans
    PRIMARY KEY (pplan_id);
--
-- Definition for index pk_persons_checks (OID = 28257) :
--
ALTER TABLE ONLY persons_checks
    ADD CONSTRAINT pk_persons_checks
    PRIMARY KEY (percheck_id);
--
-- Definition for index pk_pre_requirements (OID = 28262) :
--
ALTER TABLE ONLY pre_requirements
    ADD CONSTRAINT pk_pre_requirements
    PRIMARY KEY (preq_id);
--
-- Definition for index pk_profile_type (OID = 28277) :
--
ALTER TABLE ONLY profile_type
    ADD CONSTRAINT pk_profile_type
    PRIMARY KEY (proftype_id);
--
-- Definition for index pk_promos (OID = 28289) :
--
ALTER TABLE ONLY promos
    ADD CONSTRAINT pk_promos
    PRIMARY KEY (promo_id);
--
-- Definition for index pk_provider_accounts (OID = 28304) :
--
ALTER TABLE ONLY provider_accounts
    ADD CONSTRAINT pk_provider_accounts
    PRIMARY KEY (provacc_id);
--
-- Definition for index pk_required_docs (OID = 28316) :
--
ALTER TABLE ONLY required_docs
    ADD CONSTRAINT pk_required_docs
    PRIMARY KEY (reqdoc_id);
--
-- Definition for index pk_scholarship (OID = 28328) :
--
ALTER TABLE ONLY scholarship
    ADD CONSTRAINT pk_scholarship
    PRIMARY KEY (scholar_id);
--
-- Definition for index pk_schools (OID = 28340) :
--
ALTER TABLE ONLY schools
    ADD CONSTRAINT pk_schools
    PRIMARY KEY (school_id);
--
-- Definition for index pk_semesters (OID = 28349) :
--
ALTER TABLE ONLY semesters
    ADD CONSTRAINT pk_semesters
    PRIMARY KEY (sems_id);
--
-- Definition for index pk_student_account (OID = 28365) :
--
ALTER TABLE ONLY student_account
    ADD CONSTRAINT pk_student_account
    PRIMARY KEY (staccount_id);
--
-- Definition for index pk_student_healthcare (OID = 28374) :
--
ALTER TABLE ONLY student_healthcare
    ADD CONSTRAINT pk_student_healthcare
    PRIMARY KEY (stuhcare_id);
--
-- Definition for index pk_student_monitoring (OID = 28386) :
--
ALTER TABLE ONLY student_monitoring
    ADD CONSTRAINT pk_student_monitoring
    PRIMARY KEY (stmonit_id);
--
-- Definition for index pk_student_retention (OID = 28398) :
--
ALTER TABLE ONLY student_retention
    ADD CONSTRAINT pk_student_retention
    PRIMARY KEY (student_ret_id);
--
-- Definition for index pk_subjects (OID = 28417) :
--
ALTER TABLE ONLY subjects
    ADD CONSTRAINT pk_subjects
    PRIMARY KEY (subj_id);
--
-- Definition for index pk_subject_evaluation (OID = 28426) :
--
ALTER TABLE ONLY subject_evaluation
    ADD CONSTRAINT pk_subject_evaluation
    PRIMARY KEY (subeval_id);
--
-- Definition for index pk_tariffs (OID = 28439) :
--
ALTER TABLE ONLY tariffs
    ADD CONSTRAINT pk_tariffs
    PRIMARY KEY (tariff_id);
--
-- Definition for index pk_tickets (OID = 28448) :
--
ALTER TABLE ONLY tickets
    ADD CONSTRAINT pk_tickets
    PRIMARY KEY (ticket_id);
--
-- Definition for index pk_ticket_details (OID = 28459) :
--
ALTER TABLE ONLY ticket_details
    ADD CONSTRAINT pk_ticket_details
    PRIMARY KEY (tickdet_id);
--
-- Definition for index pk_ticket_details_types (OID = 28467) :
--
ALTER TABLE ONLY ticket_details_types
    ADD CONSTRAINT pk_ticket_details_types
    PRIMARY KEY (tdtype_id);
--
-- Definition for index pk_tills (OID = 28476) :
--
ALTER TABLE ONLY tills
    ADD CONSTRAINT pk_tills
    PRIMARY KEY (till_id);
--
-- Definition for index pk_tills_details (OID = 28485) :
--
ALTER TABLE ONLY tills_details
    ADD CONSTRAINT pk_tills_details
    PRIMARY KEY (dettills_id);
--
-- Definition for index pk_till_transfers (OID = 28498) :
--
ALTER TABLE ONLY till_transfers
    ADD CONSTRAINT pk_till_transfers
    PRIMARY KEY (tilltrans_id);
--
-- Definition for index pk_titles (OID = 28510) :
--
ALTER TABLE ONLY titles
    ADD CONSTRAINT pk_titles
    PRIMARY KEY (title_id);
--
-- Definition for index pk_units (OID = 28522) :
--
ALTER TABLE ONLY units
    ADD CONSTRAINT pk_units
    PRIMARY KEY (unit_id);
--
-- Definition for index pk_workplace (OID = 28534) :
--
ALTER TABLE ONLY workplace
    ADD CONSTRAINT pk_workplace
    PRIMARY KEY (wplace_id);
--
-- Definition for index fk_assistan_reference_enrolled (OID = 28537) :
--
ALTER TABLE ONLY assistances
    ADD CONSTRAINT fk_assistan_reference_enrolled
    FOREIGN KEY (enroll_id) REFERENCES enrolleds(enroll_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_assistan_reference_careers_ (OID = 28542) :
--
ALTER TABLE ONLY assistances
    ADD CONSTRAINT fk_assistan_reference_careers_
    FOREIGN KEY (carsubj_id) REFERENCES careers_subjets(carsubj_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_careers__reference_professo (OID = 28547) :
--
ALTER TABLE ONLY careers_subjets
    ADD CONSTRAINT fk_careers__reference_professo
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_careers__reference_facu_car (OID = 28552) :
--
ALTER TABLE ONLY careers_subjets
    ADD CONSTRAINT fk_careers__reference_facu_car
    FOREIGN KEY (facu_career_id) REFERENCES facu_careers(facu_career_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_careers__reference_subjects (OID = 28557) :
--
ALTER TABLE ONLY careers_subjets
    ADD CONSTRAINT fk_careers__reference_subjects
    FOREIGN KEY (subj_id) REFERENCES subjects(subj_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_carrera__reference_careers_ (OID = 28562) :
--
ALTER TABLE ONLY careers_subject_details
    ADD CONSTRAINT fk_carrera__reference_careers_
    FOREIGN KEY (carsubj_id) REFERENCES careers_subjets(carsubj_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_cities_reference_departam (OID = 28567) :
--
ALTER TABLE ONLY cities
    ADD CONSTRAINT fk_cities_reference_departam
    FOREIGN KEY (depart_id) REFERENCES departaments(depart_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_contact__reference_persons (OID = 28572) :
--
ALTER TABLE ONLY contact_persons
    ADD CONSTRAINT fk_contact__reference_persons
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_contact__reference_contact_ (OID = 28577) :
--
ALTER TABLE ONLY contact_persons
    ADD CONSTRAINT fk_contact__reference_contact_
    FOREIGN KEY (cnttype_id) REFERENCES contact_type(cnttype_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_document_reference_persons (OID = 28587) :
--
ALTER TABLE ONLY document_persons
    ADD CONSTRAINT fk_document_reference_persons
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_document_reference_required (OID = 28592) :
--
ALTER TABLE ONLY document_persons
    ADD CONSTRAINT fk_document_reference_required
    FOREIGN KEY (reqdoc_id) REFERENCES required_docs(reqdoc_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_employee_reference_appointm (OID = 28602) :
--
ALTER TABLE ONLY employees
    ADD CONSTRAINT fk_employee_reference_appointm
    FOREIGN KEY (appoin_id) REFERENCES appointments(appoin_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_enrolled_reference_students (OID = 28607) :
--
ALTER TABLE ONLY enrolleds
    ADD CONSTRAINT fk_enrolled_reference_students
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_enrolled_reference_careers_ (OID = 28612) :
--
ALTER TABLE ONLY enrolleds
    ADD CONSTRAINT fk_enrolled_reference_careers_
    FOREIGN KEY (carsubj_id) REFERENCES careers_subjets(carsubj_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_eval_stu_reference_enrolled (OID = 28617) :
--
ALTER TABLE ONLY eval_students
    ADD CONSTRAINT fk_eval_stu_reference_enrolled
    FOREIGN KEY (enroll_id) REFERENCES enrolleds(enroll_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_eval_stu_reference_subject_ (OID = 28622) :
--
ALTER TABLE ONLY eval_students
    ADD CONSTRAINT fk_eval_stu_reference_subject_
    FOREIGN KEY (subeval_id) REFERENCES subject_evaluation(subeval_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_facucar__reference_facu_car (OID = 28627) :
--
ALTER TABLE ONLY facucar_adm
    ADD CONSTRAINT fk_facucar__reference_facu_car
    FOREIGN KEY (facu_career_id) REFERENCES facu_careers(facu_career_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_unidades_facultades (OID = 28632) :
--
ALTER TABLE ONLY faculties
    ADD CONSTRAINT fk_unidades_facultades
    FOREIGN KEY (facu_unit_id) REFERENCES units(unit_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_facu_car_reference_facultie (OID = 28637) :
--
ALTER TABLE ONLY facu_careers
    ADD CONSTRAINT fk_facu_car_reference_facultie
    FOREIGN KEY (faculty_id) REFERENCES faculties(facu_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_facu_car_reference_careers (OID = 28642) :
--
ALTER TABLE ONLY facu_careers
    ADD CONSTRAINT fk_facu_car_reference_careers
    FOREIGN KEY (career_id) REFERENCES careers(career_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_facu_car_reference_career_t (OID = 28647) :
--
ALTER TABLE ONLY facu_careers
    ADD CONSTRAINT fk_facu_car_reference_career_t
    FOREIGN KEY (tcareer_id) REFERENCES career_type(tcareer_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_operatio_reference_iva_type (OID = 28652) :
--
ALTER TABLE ONLY operations
    ADD CONSTRAINT fk_operatio_reference_iva_type
    FOREIGN KEY (ivatype_id) REFERENCES iva_type(ivatype_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_payplans_reference_facucar_ (OID = 28657) :
--
ALTER TABLE ONLY payplans
    ADD CONSTRAINT fk_payplans_reference_facucar_
    FOREIGN KEY (fcaradm_id) REFERENCES facucar_adm(fcaradm_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_persons_reference_cities2 (OID = 28662) :
--
ALTER TABLE ONLY persons
    ADD CONSTRAINT fk_persons_reference_cities2
    FOREIGN KEY (homecity_id) REFERENCES cities(city_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_persons_reference_cities (OID = 28667) :
--
ALTER TABLE ONLY persons
    ADD CONSTRAINT fk_persons_reference_cities
    FOREIGN KEY (birthplace_id) REFERENCES cities(city_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_persons__reference_persons (OID = 28677) :
--
ALTER TABLE ONLY persons_checks
    ADD CONSTRAINT fk_persons__reference_persons
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_materia_prereq (OID = 28682) :
--
ALTER TABLE ONLY pre_requirements
    ADD CONSTRAINT fk_materia_prereq
    FOREIGN KEY (subj_id) REFERENCES subjects(subj_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_prerequi_materia (OID = 28687) :
--
ALTER TABLE ONLY pre_requirements
    ADD CONSTRAINT fk_prerequi_materia
    FOREIGN KEY (prereq_id) REFERENCES subjects(subj_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_professo_reference_persons (OID = 28692) :
--
ALTER TABLE ONLY professors
    ADD CONSTRAINT fk_professo_reference_persons
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_provider_reference_persons (OID = 28697) :
--
ALTER TABLE ONLY providers
    ADD CONSTRAINT fk_provider_reference_persons
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_provider_reference_provider (OID = 28702) :
--
ALTER TABLE ONLY provider_accounts
    ADD CONSTRAINT fk_provider_reference_provider
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_required_reference_profile_ (OID = 28707) :
--
ALTER TABLE ONLY required_docs
    ADD CONSTRAINT fk_required_reference_profile_
    FOREIGN KEY (proftype_id) REFERENCES profile_type(proftype_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_schools_reference_cities (OID = 28712) :
--
ALTER TABLE ONLY schools
    ADD CONSTRAINT fk_schools_reference_cities
    FOREIGN KEY (city_id) REFERENCES cities(city_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_semester_reference_careers (OID = 28717) :
--
ALTER TABLE ONLY semesters
    ADD CONSTRAINT fk_semester_reference_careers
    FOREIGN KEY (career_id) REFERENCES careers(career_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_students_reference_persons (OID = 28722) :
--
ALTER TABLE ONLY students
    ADD CONSTRAINT fk_students_reference_persons
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_students_reference_hscholl_ (OID = 28727) :
--
ALTER TABLE ONLY students
    ADD CONSTRAINT fk_students_reference_hscholl_
    FOREIGN KEY (hstitle_id) REFERENCES hscholl_titles(hstitle_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_students_reference_schools (OID = 28732) :
--
ALTER TABLE ONLY students
    ADD CONSTRAINT fk_students_reference_schools
    FOREIGN KEY (school_id) REFERENCES schools(school_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_students (OID = 28737) :
--
ALTER TABLE ONLY student_account
    ADD CONSTRAINT fk_student__reference_students
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_enrolled (OID = 28742) :
--
ALTER TABLE ONLY student_account
    ADD CONSTRAINT fk_student__reference_enrolled
    FOREIGN KEY (enroll_id) REFERENCES enrolleds(enroll_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_operatio (OID = 28747) :
--
ALTER TABLE ONLY student_account
    ADD CONSTRAINT fk_student__reference_operatio
    FOREIGN KEY (oper_id) REFERENCES operations(oper_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_students (OID = 28752) :
--
ALTER TABLE ONLY student_healthcare
    ADD CONSTRAINT fk_student__reference_students
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_healthca (OID = 28757) :
--
ALTER TABLE ONLY student_healthcare
    ADD CONSTRAINT fk_student__reference_healthca
    FOREIGN KEY (hcare_id) REFERENCES healthcare(hcare_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_students (OID = 28762) :
--
ALTER TABLE ONLY student_monitoring
    ADD CONSTRAINT fk_student__reference_students
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_persons (OID = 28767) :
--
ALTER TABLE ONLY student_monitoring
    ADD CONSTRAINT fk_student__reference_persons
    FOREIGN KEY (caller_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_students (OID = 28772) :
--
ALTER TABLE ONLY student_retention
    ADD CONSTRAINT fk_student__reference_students
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_scholars (OID = 28777) :
--
ALTER TABLE ONLY student_scholarship
    ADD CONSTRAINT fk_student__reference_scholars
    FOREIGN KEY (scholar_id) REFERENCES scholarship(scholar_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_students (OID = 28782) :
--
ALTER TABLE ONLY student_scholarship
    ADD CONSTRAINT fk_student__reference_students
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_workplac (OID = 28787) :
--
ALTER TABLE ONLY student_workplaces
    ADD CONSTRAINT fk_student__reference_workplac
    FOREIGN KEY (wplace_id) REFERENCES workplace(wplace_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_student__reference_students (OID = 28792) :
--
ALTER TABLE ONLY student_workplaces
    ADD CONSTRAINT fk_student__reference_students
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_subjects_reference_semester (OID = 28797) :
--
ALTER TABLE ONLY subjects
    ADD CONSTRAINT fk_subjects_reference_semester
    FOREIGN KEY (sems_id) REFERENCES semesters(sems_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_mate_evaltip (OID = 28802) :
--
ALTER TABLE ONLY subject_evaluation
    ADD CONSTRAINT fk_mate_evaltip
    FOREIGN KEY (evaltype_id) REFERENCES evaluation_types(evaltype_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_subject__reference_careers_ (OID = 28807) :
--
ALTER TABLE ONLY subject_evaluation
    ADD CONSTRAINT fk_subject__reference_careers_
    FOREIGN KEY (carsubj_id) REFERENCES careers_subjets(carsubj_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_tariffs_reference_facucar_ (OID = 28812) :
--
ALTER TABLE ONLY tariffs
    ADD CONSTRAINT fk_tariffs_reference_facucar_
    FOREIGN KEY (fcaradm_id) REFERENCES facucar_adm(fcaradm_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_tickets_reference_persons (OID = 28817) :
--
ALTER TABLE ONLY tickets
    ADD CONSTRAINT fk_tickets_reference_persons
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_ticket_d_reference_tickets (OID = 28822) :
--
ALTER TABLE ONLY ticket_details
    ADD CONSTRAINT fk_ticket_d_reference_tickets
    FOREIGN KEY (ticket_id) REFERENCES tickets(ticket_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_ticket_d_reference_ticket_d (OID = 28827) :
--
ALTER TABLE ONLY ticket_details
    ADD CONSTRAINT fk_ticket_d_reference_ticket_d
    FOREIGN KEY (tdtype_id) REFERENCES ticket_details_types(tdtype_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_tills_reference_units (OID = 28832) :
--
ALTER TABLE ONLY tills
    ADD CONSTRAINT fk_tills_reference_units
    FOREIGN KEY (unit_id) REFERENCES units(unit_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_tills_de_reference_employee (OID = 28837) :
--
ALTER TABLE ONLY tills_details
    ADD CONSTRAINT fk_tills_de_reference_employee
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_tills_de_reference_tills (OID = 28842) :
--
ALTER TABLE ONLY tills_details
    ADD CONSTRAINT fk_tills_de_reference_tills
    FOREIGN KEY (till_id) REFERENCES tills(till_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_tills_de_reference_tickets (OID = 28847) :
--
ALTER TABLE ONLY tills_details
    ADD CONSTRAINT fk_tills_de_reference_tickets
    FOREIGN KEY (ticket_id) REFERENCES tickets(ticket_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_till_tra_reference_tills2 (OID = 28852) :
--
ALTER TABLE ONLY till_transfers
    ADD CONSTRAINT fk_till_tra_reference_tills2
    FOREIGN KEY (origin_till_id) REFERENCES tills(till_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_till_tra_reference_tills (OID = 28857) :
--
ALTER TABLE ONLY till_transfers
    ADD CONSTRAINT fk_till_tra_reference_tills
    FOREIGN KEY (destiny_till_id) REFERENCES tills(till_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_titles_reference_professo (OID = 28862) :
--
ALTER TABLE ONLY titles
    ADD CONSTRAINT fk_titles_reference_professo
    FOREIGN KEY (person_id) REFERENCES persons(person_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index carrera_mat_deetails_pkey (OID = 29245) :
--
ALTER TABLE ONLY careers_subject_details
    ADD CONSTRAINT carrera_mat_deetails_pkey
    PRIMARY KEY (carsubdet_id);
--
-- Definition for index pk_countries (OID = 29253) :
--
ALTER TABLE ONLY countries
    ADD CONSTRAINT pk_countries
    PRIMARY KEY (country_id);
--
-- Definition for index fk_departam_reference_countrie (OID = 29255) :
--
ALTER TABLE ONLY departaments
    ADD CONSTRAINT fk_departam_reference_countrie
    FOREIGN KEY (country_id) REFERENCES countries(country_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Definition for index fk_persons_reference_countrie (OID = 29260) :
--
ALTER TABLE ONLY persons
    ADD CONSTRAINT fk_persons_reference_countrie
    FOREIGN KEY (country_id) REFERENCES countries(country_id) ON UPDATE RESTRICT ON DELETE RESTRICT;
--
-- Data for sequence public.appointments_appoin_id_seq (OID = 28019)
--
SELECT pg_catalog.setval('appointments_appoin_id_seq', 1, false);
--
-- Data for sequence public.assistances_asisst_id_seq (OID = 28028)
--
SELECT pg_catalog.setval('assistances_asisst_id_seq', 1, false);
--
-- Data for sequence public.careers_career_id_seq (OID = 28037)
--
SELECT pg_catalog.setval('careers_career_id_seq', 1, false);
--
-- Data for sequence public.careers_subjets_carsubj_id_seq (OID = 28050)
--
SELECT pg_catalog.setval('careers_subjets_carsubj_id_seq', 1, false);
--
-- Data for sequence public.career_type_tcareer_id_seq (OID = 28061)
--
SELECT pg_catalog.setval('career_type_tcareer_id_seq', 1, false);
--
-- Data for sequence public.cities_city_id_seq (OID = 28073)
--
SELECT pg_catalog.setval('cities_city_id_seq', 1, false);
--
-- Data for sequence public.contact_persons_cntper_id_seq (OID = 28082)
--
SELECT pg_catalog.setval('contact_persons_cntper_id_seq', 1, false);
--
-- Data for sequence public.contact_type_cnttype_id_seq (OID = 28091)
--
SELECT pg_catalog.setval('contact_type_cnttype_id_seq', 1, false);
--
-- Data for sequence public.departaments_depart_id_seq (OID = 28110)
--
SELECT pg_catalog.setval('departaments_depart_id_seq', 1, false);
--
-- Data for sequence public.document_persons_docpers_id_seq (OID = 28119)
--
SELECT pg_catalog.setval('document_persons_docpers_id_seq', 1, false);
--
-- Data for sequence public.evaluation_types_evaltype_id_seq (OID = 28144)
--
SELECT pg_catalog.setval('evaluation_types_evaltype_id_seq', 1, false);
--
-- Data for sequence public.eval_students_evstu_id_seq (OID = 28152)
--
SELECT pg_catalog.setval('eval_students_evstu_id_seq', 1, false);
--
-- Data for sequence public.facucar_adm_fcaradm_id_seq (OID = 28161)
--
SELECT pg_catalog.setval('facucar_adm_fcaradm_id_seq', 1, false);
--
-- Data for sequence public.faculties_facu_id_seq (OID = 28173)
--
SELECT pg_catalog.setval('faculties_facu_id_seq', 49, true);
--
-- Data for sequence public.healthcare_hcare_id_seq (OID = 28191)
--
SELECT pg_catalog.setval('healthcare_hcare_id_seq', 1, false);
--
-- Data for sequence public.hscholl_titles_hstitle_id_seq (OID = 28203)
--
SELECT pg_catalog.setval('hscholl_titles_hstitle_id_seq', 1, false);
--
-- Data for sequence public.iva_type_ivatype_id_seq (OID = 28215)
--
SELECT pg_catalog.setval('iva_type_ivatype_id_seq', 1, false);
--
-- Data for sequence public.operations_oper_id_seq (OID = 28227)
--
SELECT pg_catalog.setval('operations_oper_id_seq', 1, false);
--
-- Data for sequence public.payplans_pplan_id_seq (OID = 28239)
--
SELECT pg_catalog.setval('payplans_pplan_id_seq', 1, false);
--
-- Data for sequence public.persons_checks_percheck_id_seq (OID = 28251)
--
SELECT pg_catalog.setval('persons_checks_percheck_id_seq', 1, false);
--
-- Data for sequence public.profile_type_proftype_id_seq (OID = 28271)
--
SELECT pg_catalog.setval('profile_type_proftype_id_seq', 1, false);
--
-- Data for sequence public.promos_promo_id_seq (OID = 28280)
--
SELECT pg_catalog.setval('promos_promo_id_seq', 1, false);
--
-- Data for sequence public.provider_accounts_provacc_id_seq (OID = 28298)
--
SELECT pg_catalog.setval('provider_accounts_provacc_id_seq', 1, false);
--
-- Data for sequence public.required_docs_reqdoc_id_seq (OID = 28307)
--
SELECT pg_catalog.setval('required_docs_reqdoc_id_seq', 1, false);
--
-- Data for sequence public.scholarship_scholar_id_seq (OID = 28319)
--
SELECT pg_catalog.setval('scholarship_scholar_id_seq', 1, false);
--
-- Data for sequence public.schools_school_id_seq (OID = 28331)
--
SELECT pg_catalog.setval('schools_school_id_seq', 1, false);
--
-- Data for sequence public.student_account_staccount_id_seq (OID = 28359)
--
SELECT pg_catalog.setval('student_account_staccount_id_seq', 1, false);
--
-- Data for sequence public.student_healthcare_stuhcare_id_seq (OID = 28368)
--
SELECT pg_catalog.setval('student_healthcare_stuhcare_id_seq', 1, false);
--
-- Data for sequence public.student_monitoring_stmonit_id_seq (OID = 28377)
--
SELECT pg_catalog.setval('student_monitoring_stmonit_id_seq', 1, false);
--
-- Data for sequence public.student_retention_student_ret_id_seq (OID = 28389)
--
SELECT pg_catalog.setval('student_retention_student_ret_id_seq', 1, false);
--
-- Data for sequence public.subjects_subj_id_seq (OID = 28408)
--
SELECT pg_catalog.setval('subjects_subj_id_seq', 1, false);
--
-- Data for sequence public.subject_evaluation_subeval_id_seq (OID = 28420)
--
SELECT pg_catalog.setval('subject_evaluation_subeval_id_seq', 1, false);
--
-- Data for sequence public.tariffs_tariff_id_seq (OID = 28428)
--
SELECT pg_catalog.setval('tariffs_tariff_id_seq', 1, false);
--
-- Data for sequence public.tickets_ticket_id_seq (OID = 28442)
--
SELECT pg_catalog.setval('tickets_ticket_id_seq', 1, false);
--
-- Data for sequence public.ticket_details_tickdet_id_seq (OID = 28450)
--
SELECT pg_catalog.setval('ticket_details_tickdet_id_seq', 1, false);
--
-- Data for sequence public.ticket_details_types_tdtype_id_seq (OID = 28461)
--
SELECT pg_catalog.setval('ticket_details_types_tdtype_id_seq', 1, false);
--
-- Data for sequence public.tills_till_id_seq (OID = 28470)
--
SELECT pg_catalog.setval('tills_till_id_seq', 1, false);
--
-- Data for sequence public.tills_details_dettills_id_seq (OID = 28479)
--
SELECT pg_catalog.setval('tills_details_dettills_id_seq', 1, false);
--
-- Data for sequence public.till_transfers_tilltrans_id_seq (OID = 28489)
--
SELECT pg_catalog.setval('till_transfers_tilltrans_id_seq', 1, false);
--
-- Data for sequence public.titles_title_id_seq (OID = 28501)
--
SELECT pg_catalog.setval('titles_title_id_seq', 1, false);
--
-- Data for sequence public.units_unit_id_seq (OID = 28513)
--
SELECT pg_catalog.setval('units_unit_id_seq', 50, true);
--
-- Data for sequence public.workplace_wplace_id_seq (OID = 28525)
--
SELECT pg_catalog.setval('workplace_wplace_id_seq', 1, false);
--
-- Data for sequence public.carrera_mat_deetails_carmatdet_id_seq (OID = 29242)
--
SELECT pg_catalog.setval('carrera_mat_deetails_carmatdet_id_seq', 1, false);
--
-- Data for sequence public.countries_country_id_seq (OID = 29251)
--
SELECT pg_catalog.setval('countries_country_id_seq', 1, false);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
