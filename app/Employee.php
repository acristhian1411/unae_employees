<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Employee extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table = "employees";
  protected $primaryKey = "person_id";
  // protected $person_fillable=["person_id","homecity_id","birthplace_id","country_id","person_fname","person_lastname","person_birthdate","person_gender","person_idnumber","person_address", "person_bloodtype", "person_photo", "person_business_name","person_ruc"];
  protected $fillable = [
    "person_id", "homecity_id", "birthplace_id", "country_id",
    "person_fname", "person_lastname", "person_birthdate", "person_gender",
    "person_idnumber", "person_address", "person_bloodtype", "person_photo",
    "person_business_name", "person_ruc", "app_appointment_id",
    "emplo_startdate", "emplo_status", "emplo_observation", "emplo_salary", "has_ips",
    "porcent_ips", "till_id", "office_desc", "number_of_children", "civil_status"
  ];
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $hidden = ["created_at", "updated_at", "deleted_at"];
  //relaciones
  public function appointment()
  {
    return $this->belongsTo(Appointment::class, 'appoin_id', 'appoin_id');
  }
  public function tilldetails()
  {
    $this->hasMany(TillDetail::class, "person_id", "person_id");
  }
}
