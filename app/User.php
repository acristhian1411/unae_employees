<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\HasRoles;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable, HasApiTokens, SoftDeletes, HasRoles;
    use \OwenIt\Auditing\Auditable;

    const USUARIO_VERIFICADO = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const USUARIO_ADMINISTRADOR = 'true';
    const USUARIO_REGULAR = 'false';

    public $transformer = UserTransformer::class;

    protected $table = 'users';
    protected $dates = ['deleted_at', 'email_verified_at'];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $fillable = [
        'name',
        'email',
        'username',
        'person_id',
        'provider_name',
        'provider_id',
        'password',
        'verified',
        'verification_token',
        'admin',
        'reset_password'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'verification_token',
    ];
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtolower($value);
    }

    public function getNameAttribute($value)
    {
        return mb_strtolower($value);
    }
    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = mb_strtolower($value);
    }

    public function getUsernameAttribute($value)
    {
        return mb_strtolower($value);
    }
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value);
    }

    public function esVerificado()
    {
        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public function esAdministrador()
    {
        return $this->admin == User::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificationToken()
    {
        return Str::random(40);
    }
}
