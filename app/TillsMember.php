<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class TillsMember extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    protected $table="tills_members";
    protected $primaryKey="tills_member_id";
    protected $fillable=["tills_member_id","person_id", "tills_id", "tills_role", "expire", "start_date", "expiration_date"];
    protected $hidden = ["created_at","updated_at","deleted_at"];
    protected $dateFormat = 'Y-m-d H:i:s';

}
