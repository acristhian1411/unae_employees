<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Person extends Model implements Auditable
{
	use SoftDeletes;
	use \OwenIt\Auditing\Auditable;
	protected $table = "persons";
	protected $primaryKey = "person_id";
	protected $fillable = ["person_id", "homecity_id", "birthplace_id", "country_id", "person_fname", "person_lastname", "person_birthdate", "person_gender", "person_idnumber", "person_address", "person_bloodtype", "person_photo", "person_business_name", "person_ruc", "person_shirtsize"];
	protected $dateFormat = 'Y-m-d H:i:sO';
	protected $hidden = ["created_at", "updated_at", "deleted_at"];

	//relaciones
	public function workplace()
	{
		return $this->belongsToMany(Workplace::class, 'student_workplaces',  'person_id', 'wplace_id');
	}
	public function healthcare()
	{
		return $this->belongsToMany(Healthcare::class, 'student_healthcare', 'person_id', 'hcare_id');
	}
	public function studentScholarship()
	{
		return $this->hasMany(StudentScholarship::class, 'person_id', 'person_id');
	}
	public function scholarship()
	{
		return $this->hasMany(Scholarship::class, "student_scholarship", 'person_id', 'scholar_id');
	}
	public function country()
	{
		return $this->belongsTo(Country::class, 'country_id', 'country_id');
	}
	public function birthplace()
	{
		return $this->belongsTo(City::class, 'city_id', 'birthplace_id');
	}
	public function homecity()
	{
		return $this->belongsTo(City::class, 'city_id', 'homecity_id');
	}
}
