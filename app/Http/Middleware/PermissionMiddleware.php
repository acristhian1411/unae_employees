<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Role;
use App\User;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next, $permission)
     {
         if (Auth::guest()) {
             return redirect('/login');
         }
         // dd(auth()->user());
         // $role = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
         //              ->where('model_has_roles.model_id', '=', $request->user()->id)
         //              ->first();
         $user = User::find($request->user()->id);
        if ($user !== null) {
          // if (! $role->hasPermissionTo($permission)) {
            if (! $user->hasPermissionTo($permission)) {
              abort(403);
            }
          // }
        }
        else {
          abort(403);
        }
         return $next($request);
     }
}
