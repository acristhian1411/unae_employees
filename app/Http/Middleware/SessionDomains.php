<?php

namespace App\Http\Middleware;

use Closure;
class SessionDomains
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
        if($request->getHost() === env('SESSION_DOMAIN2')){
            config([
                'session.domain' => env('SESSION_DOMAIN2'),
                'sanctum.stateful' => env('SESSION_DOMAIN2'),
                'cors.allowed_origins' => env('SESSION_DOMAIN2')
            ]);
	}elseif($request->getHost() === env('SESSION_DOMAIN3')){
		config([
                'session.domain' => env('SESSION_DOMAIN3'),
                'sanctum.stateful' => env('SESSION_DOMAIN3'),
                'cors.allowed_origins' => env('SESSION_DOMAIN3')
            ]);
	}
        return $next($request);
    }
}
