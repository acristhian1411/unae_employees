<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Role;


class PermissionReportMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, $permission)
     {
       // dd("llega al Middleware");
       // dd(Auth::guest());
         if (Auth::guest()) {
             return false;
         }
         // dd(auth()->user());
         $role = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
                      ->where('model_has_roles.model_id', '=', $request->user()->id)
                      ->first();
        if ($role !== null) {
          if (! $role->hasPermissionTo($permission)) {
            return false;
          }else {
            return true;
          }
        }
        else {
          return false;
        }
     //     return $next($request);
     }
}
