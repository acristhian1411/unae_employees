<?php

namespace App\Http\Controllers\Persons;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ApiDBController;
use App\Http\Controllers\Persons\PersonsAdvancesFilters;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use App\Person;
use App\Student;
use App\Employee;
use App\Professor;
use App\Provider;
use App\Country;
use App\ContactPerson;
use EloquentBuilder;

class PersonController extends ApiDBController
{
  public function index()
  {
    //
    // $datos = Person::select('persons.*', 'countries.country_name', DB::raw("get_telephone(persons.person_id) as telephone"))
    //   ->join('countries', 'countries.country_id', '=', 'persons.country_id')
    //
    //   ->groupBy('persons.person_id', 'countries.country_name')
    //   ->get();
    $count = DB::select('select count(*) from only persons');
    $datos = "select p.*, c.country_name, get_telephone(p.person_id) as telephone from only persons p join countries c on c.country_id = p.country_id";
    // dd($datos);
    return $this->showAll($datos, $count, 200);
  }
  public function persontype($id)
  {
    $response = [];
    $student = Student::find($id);
    $employee = Employee::find($id);
    $professor = Professor::find($id);
    $provider = Provider::find($id);
    if ($student != null) {
      $response[] = 'student';
    }
    if ($employee != null) {
      $response[] = 'employee';
    }
    if ($professor != null) {
      $response[] = 'professor';
    }
    if ($provider != null) {
      $response[] = 'provider';
    }
    // dd($response);
    return response($response);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
    $request->validate([
      'homecity_id' => 'required',
      'birthplace_id' => 'required',
      'country_id' => 'required',
      'person_fname' => 'required',
      'person_lastname' => 'required',
      'person_birthdate' => 'required',
      'person_gender' => 'required',
      'person_idnumber' => 'required',
      'person_address' => 'required',
      'person_bloodtype' => 'required',
      'person_business_name' => 'required',
      'person_ruc' => 'required',
    ]);
    if ($request->get('person_photo') === null && $request->file('person_photo') === null) {
      $image = 'default.png';
      $request->request->add(['person_photo' => $image]);
    } elseif (($request->get('person_photo') === "default.png")) {
      $request->request->add(['person_photo' => "default.png"]);
    } else {
      $image = $request->get('person_photo');
      $imageName = Str::random(20) . '.png';
      $imagen = Image::make($image)->encode('png', 75);
      $destinationPath = public_path('img');
      $imagen->save($destinationPath . "/" . $imageName);
      $request->request->add(['person_photo' => $imageName]);
    }
    $dato = new Person([

      'homecity_id' => $request->get('homecity_id'),
      'birthplace_id' => $request->get('birthplace_id'),
      'country_id' => $request->get('country_id'),
      'person_fname' => $request->get('person_fname'),
      'person_lastname' => $request->get('person_lastname'),
      'person_birthdate' => $request->get('person_birthdate'),
      'person_gender' => $request->get('person_gender'),
      'person_idnumber' => $request->get('person_idnumber'),
      'person_address' => $request->get('person_address'),
      'person_bloodtype' => $request->get('person_bloodtype'),
      'person_photo' => $request->get('person_photo'),
      'person_business_name' => $request->get('person_business_name'),
      'person_ruc' => $request->get('person_ruc')

    ]);


    $dato->save();

    return response()->json([
      'message' => 'Persona se agrego con exito!',
      'dato' => $dato
    ]);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Person  $country
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //

    $dato = Person::where([['person_id', '=', $id], ['deleted_at', '=', null]])
      ->select(
        "persons.*",
        DB::raw("get_telephone(persons.person_id) as telephone"),
        DB::raw("get_email(persons.person_id) as email")
      )
      ->first();
    if ($dato === null) {
      return response()->json(false);
    }
    return $this->showOne($dato, 200);
  }

  // public function edit(Person $Person)
  // {
  //     //
  //     $Person = Person::find($Person);
  //     return response()->json($Person);
  // }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = Person::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Persona se restauro con exito!');
    } else {
      $request->validate([
        'homecity_id' => 'required',
        'birthplace_id' => 'required',
        'country_id' => 'required',
        'person_fname' => 'required',
        'person_lastname' => 'required',
        'person_birthdate' => 'required',
        'person_gender' => 'required',
        'person_idnumber' => 'required',
        'person_address' => 'required',
        'person_bloodtype' => 'required',
        'person_business_name' => 'required',
        'person_ruc' => 'required',
      ]);
      $dato = Person::find($id);
      $dato->homecity_id = $request->get('homecity_id');
      $dato->birthplace_id = $request->get('birthplace_id');
      $dato->country_id = $request->get('country_id');
      $dato->person_fname = $request->get('person_fname');
      $dato->person_lastname = $request->get('person_lastname');
      $dato->person_birthdate = $request->get('person_birthdate');
      $dato->person_gender = $request->get('person_gender');
      $dato->person_idnumber = $request->get('person_idnumber');
      $dato->person_address = $request->get('person_address');
      $dato->person_bloodtype = $request->get('person_bloodtype');
      // $dato->person_photo = $request->get('person_photo');
      $dato->person_business_name = $request->get('person_business_name');
      $dato->person_ruc = $request->get('person_ruc');
      if (!($request->get('person_photo') === $dato->person_photo)) {
        if ($request->get('person_photo') === null && $request->file('person_photo') === null) {
          $image = 'default.png';
          $request->request->add(['person_photo' => $image]);
        } elseif (($request->get('person_photo') === "default.png")) {
          $request->request->add(['person_photo' => "default.png"]);
        } else {
          $image = $request->get('person_photo');
          $imageName = Str::random(20) . '.png';
          $imagen = Image::make($image)->encode('png', 75);
          $destinationPath = public_path('img');
          $imagen->save($destinationPath . "/" . $imageName);
          $request->request->add(['person_photo' => $imageName]);
        }
      }
      $dato->person_photo = $request->get('person_photo');
      $dato->save();
      return response()->json('Persona se actualizo con exito!');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Person  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = Person::find($id);
    $dato->delete();
    return response()->json('Persona se elimino con exito!');
  }
  public function search(Request $request)
  {
    if (!$request->filter) {
      $word_filter = explode(" ", $request->search);
      $array_like = "";
      if (count($word_filter) == 2) {
        $array_like = "or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[1] . "%'))
          or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[0] . "%')) ";
      } elseif (count($word_filter) == 3) {
        $array_like = "or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[2] . "%'))
          or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[2] . "%'))
          or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[0] . "%'))
            or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[0] . "%')) ";
      } elseif (count($word_filter) == 4) {
        $array_like = "or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[3] . "%'))
          or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[3] . "%'))
          or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[3] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[0] . "%'))
            or (unaccent(p.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(p.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(p.person_lastname) ilike unaccent('%" . $word_filter[0] . "%')) ";
      }
      $count = DB::select("select count(*)
      from only persons p
      left join contact_persons cp on cp.person_id = p.person_id
      where (p.deleted_at is null) and ( p.person_business_name
      ilike " . '$$%' . $request->search . '%$$' . "
    or p.person_fname
    ilike " . '$$%' . $request->search . '%$$' . "
    or cp.cntper_data ilike " . '$$%' . $request->search . '%$$' . "
    or unaccent(p.person_lastname) ilike " . '$$%' . $request->search . '%$$' . "
    or p.person_idnumber ilike " . '$$%' . $request->search . '%$$' . "
    or p.person_ruc ilike " . '$$%' . $request->search . '%$$' . "
    or concat(p.person_idnumber, ' - ', unaccent(p.person_fname), ' ', unaccent(p.person_lastname))
      ilike unaccent('%" . $request->search . "%')
      " . $array_like . ")group by p.person_id");

      $data = "select  p.*, get_telephone(p.person_id) as telephone
        from only persons p left join contact_persons cp on cp.person_id = p.person_id
        where  (p.deleted_at is null) and (cp.cntper_data ilike " . '$$%' . $request->search . '%$$' .
        " or  unaccent(p.person_business_name) ilike " . 'unaccent($$%' . $request->search . '%$$)' .
        " or  unaccent(p.person_fname) ilike " . 'unaccent($$%' . $request->search . '%$$)' .
        " or unaccent(p.person_lastname) ilike " . 'unaccent($$%' . $request->search . '%$$)' .
        " or p.person_idnumber ilike " . '$$%' . $request->search . '%$$' .
        " or p.person_ruc ilike " . '$$%' . $request->search . '%$$' .
        " or (select concat(unaccent(person_fname), ' ', unaccent(person_lastname)) as full_name) ilike unaccent('%" . $request->search . "%')
        " . $array_like . " )group by p.person_id";


      // $data = Person::where('person_fname', 'ILIKE', '%' . $request->search . '%')
      //   ->orWhere('person_lastname', 'ILIKE', '%' . $request->search . '%')
      //   ->orWhere('person_idnumber', 'ILIKE', '%' . $request->search . '%')
      //   ->distinct()
      //   ->get();
      // dd(DB::select($data));
    } else {
      $filters = new PersonsAdvancesFilters;
      $applied_filter = $filters->applyFilter($request->filter);
      $count = DB::select("select count(persons.*) from only persons where deleted_at is null and " . $applied_filter);
      $data = "select p.*, get_telephone(persons.person_id) as telephone from only persons join contact_persons on contact_persons.person_id = persons.person_id where persons.deleted_at is null " . $applied_filter . ' group by persons.person_id';
    }


    return $this->showAll($data, $count, 200);
  }
}
