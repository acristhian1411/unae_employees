<?php
namespace App\Http\Controllers\Persons;

// use Illuminate\Support\Facades\DB;
/**
 * Se crea una clase aparte para tener mejor orden y entendimiento
 */
class PersonsAdvancesFilters {
  /*
  Filtros para la busqueda avanzada
  Retorna una cadena de caracteres con los filtros de busqueda para poder concatenar con la peticion sql
  */
  public function applyFilter($filters)
  {
    $string_where = "";

    // dd($filters);
    if (array_key_exists("person_idnumber", $filters) == true && $filters["person_idnumber"] !== null) {
      $string_where = "person_idnumber ilike ".'$$%'.$filters["person_idnumber"].'%$$';
    }
    if (array_key_exists('person_fname', $filters) == true && $filters["person_fname"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where." and person_fname ilike ".'$$%'.$filters["person_fname"].'%$$';
      }else {
        $string_where = "person_fname ilike ".'$$%'.$filters["person_fname"].'%$$';
      }
    }
    if (array_key_exists("person_lastname", $filters) == true && $filters["person_lastname"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where." and person_lastname ilike ".'$$%'.$filters["person_lastname"].'%$$';
      }else {
        $string_where = "person_lastname ilike ".'$$%'.$filters["person_lastname"].'%$$';
      }
    }
    return ($string_where);
    // return array_key_exists("person_fname", $filters);
  }

}
