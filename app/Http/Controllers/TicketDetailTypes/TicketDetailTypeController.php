<?php

namespace App\Http\Controllers\TicketDetailTypes;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\TicketDetailType;

class TicketDetailTypeController extends ApiController
{
  public function index()
  {
      //
      $t = TicketDetailType::query()->first();
      $query = TicketDetailType::query();
      $query = $this->filterData($query, $t);
      $datos = $query->get();
      return $this->showAll($datos,200);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store($request)
  {
    // $request->validate([
    // 'tdtype_desc' => 'required',
    // 'tdtype_obs' => 'required',
    // ]);
    $dato = new TicketDetailType([
      'tdtype_desc' => $request ->get('tdtype_desc'),
      'tdtype_obs' => $request ->get('tdtype_obs')
    ]);


    $dato->save();

    return response()->json('Tipo de detalle de factura se agrego con exito!');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\TicketDetailType  $country
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //

      $dato = TicketDetailType::find($id);

      return $this->showOne($dato,200);
  }

  // public function edit(TicketDetailType $TicketDetailType)
  // {
  //     //
  //     $TicketDetailType = TicketDetailType::find($TicketDetailType);
  //     return response()->json($TicketDetailType);
  // }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = TicketDetailType::find($id);
      $keys = collect($request->all())->keys();
      for ($i=0; $i < count($keys); $i++) {
        $dato[$keys[0]]= $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Tipo de detalle de factura se restauro con exito!');
    }
    else{
      $request->validate([
      'tdtype_desc' => 'required',
      'tdtype_obs' => 'required',
      ]);
      $dato = TicketDetailType::find($id);
      $dato->tdtype_desc = $request->get('tdtype_desc');
      $dato->tdtype_obs = $request->get('tdtype_obs');
      $dato->save();
      return response()->json('Tipo de detalle de factura actualizada con exito');
  }
}

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\TicketDetailType  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $dato = TicketDetailType::find($id);
      $dato->delete();
      return response()->json('Tipo de detalle de factura se elimino con exito');
  }

}
