<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ContactPerson;


class ContactPersonsController extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $datos = ContactPerson::join('contact_type', 'contact_type.cnttype_id', '=', 'contact_persons.cnttype_id')
      ->select('contact_persons.*', 'contact_type.cnttype_name')
      ->orderBy('contact_persons.cntper_id', 'asc')
      ->get();
    return $this->showAll($datos, 200);
  }

  public function store(Request $request)
  {
    $request->validate([
      'cnttype_id' => 'required',
      'person_id' => 'required',
      'cntper_description' => 'required',
      'cntper_data' => 'required',
    ]);

    $dato = new ContactPerson([
      'cnttype_id' => $request->get('cnttype_id'),
      'person_id' => $request->get('person_id'),
      'cntper_description' => $request->get('cntper_description'),
      'cntper_data' => $request->get('cntper_data')
    ]);
    $dato->save();
    return response()->json('Contacto agregado con exito');
  }

  public function storeFromPerson(Request $request)
  {

    $contacto =  $request->contacts;
    foreach ((array) $contacto as $key =>  $value) {
      if ($value['cntper_data'] != "") {
        $dato = new ContactPerson([
          'cnttype_id' => $value['cnttype_id'],
          'person_id' => $request->get('person_id'),
          'cntper_description' => $value['cnttype_name'],
          'cntper_data' => $value['cntper_data']
        ]);
        $dato->save();
      }
    }

    return response()->json('Contacto agregado con exito');
  }

  public function show($id)
  {
    //$dato = ContactPerson::find( $id);
    $dato = ContactPerson::join('contact_type', 'contact_type.cnttype_id', '=', 'contact_persons.cnttype_id')
      ->where('contact_persons.cntper_id', '=', $id)
      ->select('contact_persons.*', 'contact_type.cnttype_name')
      ->first();
    return $this->showOne($dato, 200);
  }

  public function showPerson($id)
  {
    $dato = ContactPerson::join('contact_type', 'contact_type.cnttype_id', '=', 'contact_persons.cnttype_id')
      ->where('contact_persons.person_id', '=', $id)
      ->select('contact_persons.*', 'contact_type.cnttype_name')
      ->orderBy('contact_persons.cntper_id', 'asc')
      ->get();
    if ($dato->isEmpty()) {
      return response()->json(false);
    } else {
      return $this->showAll($dato, 200);
    }
  }

  public function update(Request $request, $id)
  {
    $dato = ContactPerson::findOrFail($id);
    $request->validate([
      'cnttype_id' => 'required',
      'person_id' => 'required',
      'cntper_description' => 'required',
      'cntper_data' => 'required',

    ]);
    $dato->cnttype_id = $request->cnttype_id;
    $dato->person_id = $request->person_id;
    $dato->cntper_description = $request->cntper_description;
    $dato->cntper_data = $request->cntper_data;

    $dato->save();
    return response()->json('Contacto actualizado con exito');
  }

  public function updateFromArray(Request $request)
  {
    $contacts = $request->contacts;
    foreach ($contacts as $key => $value) {
     if(isset($value['cntper_id'])){
      $dato = ContactPerson::findOrFail($value['cntper_id']);
      $dato->cnttype_id = $value['cnttype_id'];
      $dato->person_id = $request->person_id;
      $dato->cntper_data = $value['cntper_data'];
      $dato->save();
     }else{
     	$dato = new ContactPerson([
	  'cnttype_id' => $value['cnttype_id'],
	  'person_id' => $request->person_id,
	  'cntper_description' => $value['cnttype_name'],
	  'cntper_data' => $value['cntper_data']
	]);
	$dato->save();
     }

    }
    return response()->json('Contactos actualizados con exito');
  }

  public function destroy($id)
  {
    $dato = ContactPerson::find($id);
    $dato->delete();
    return response()->json('Contacto se elimino con exito');
  }

  public function search($request)
  {
    $data = ContactPerson::where('cntper_description', 'ILIKE', '%' . $request . '%')
      ->get();
    return $this->showAll($data, 200);
  }
}
