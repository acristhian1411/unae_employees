<?php

namespace App\Http\Controllers\CareerSubjDetail;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\CareerSubjDetail;

class CareerSubjDetailController extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $datos = CareerSubjDetail::all();
    return $this->showAll($datos, 200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $details = $request->details;
    foreach ($details as $key => $value) {
      if (!isset($value['created'])) {
        $carsubjdetails = new CareerSubjDetail([
          'carsubj_id' => $request->carsubj_id,
          'carsubdet_day' => $value['value'],
          'carsubdet_range' => $value['range'],
          'carsubdet_hs_start' => $value['carsubdet_hs_start'],
          'carsubdet_hs_end' => $value['carsubdet_hs_end']
        ]);
        $carsubjdetails->save();
      }
    }
    return response()->json('Detalle se agrego con exito!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function getDays($num)
  {
    //  $date =  date($num);
    // $num1 = date("w");
    $month = [];
    $month[0] = "Domingo";
    $month[1] = "Lunes";
    $month[2] = "Martes";
    $month[3] = "Miercoles";
    $month[4] = "Jueves";
    $month[5] = "Viernes";
    $month[6] = "Sabado";

    return $month[$num];
  }

  public function show($id)
  {
    $dato = CareerSubjDetail::findOrFail($id);
    $dato->day = $this->getDays($dato->carsubdet_day);
    return $this->showOne($dato, 200);
  }

  public function showCareerSubjects($id)
  {
    $dato = CareerSubjDetail::where('carsubj_id', $id)->get();
    foreach ($dato as $key => $value) {
      $dato[$key]->day = $this->getDays($dato[$key]->carsubdet_day);
    }
    return $this->showAll($dato, 200);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $dato = [];
    if ($request->audit_restore) {
      $dato = CareerSubjDetail::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Registro se restauro con exito!');
    } else {
      $details = $request->details;

      foreach ($details as $key => $value) {
        if (isset($value['created'])) {
          $dato = CareerSubjDetail::findOrFail($value['carsubdet_id']);
          $dato->carsubdet_day = $value['value'];
          $dato->carsubdet_range = $value['range'];
          $dato->carsubdet_hs_start = $value['carsubdet_hs_start'];
          $dato->carsubdet_hs_end = $value['carsubdet_hs_end'];
          $dato->carsubj_id = $request->carsubj_id;
          $dato->save();
        } else {
          $dato = new CareerSubjDetail([
            'carsubj_id' => $request->carsubj_id,
            'carsubdet_day' => $value['value'],
            'carsubdet_range' => $value['range'],
            'carsubdet_hs_start' => $value['carsubdet_hs_start'],
            'carsubdet_hs_end' => $value['carsubdet_hs_end'],
          ]);
          $dato->save();
        }
      }
      return response()->json([
        'message' => 'Materia se actualizo con exito!',
        'dato' => $dato
      ]);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = CareerSubjDetail::findOrFail($id);
    $dato->delete();
    return response()->json([
      'message' => 'Registro se elimino con exito!'
    ]);
  }
}
