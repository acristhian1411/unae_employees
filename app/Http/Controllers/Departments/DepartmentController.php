<?php

namespace App\Http\Controllers\Departments;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Department;
use App\Country;

class DepartmentController extends ApiController
{
  public function index()
  {
      //
      $datos = Department::join('countries', 'countries.country_id', '=', 'departaments.country_id')
                                ->select('departaments.*','countries.country_name')
                                ->where('departaments.deleted_at', '=', null)
                                ->get();
      return $this->showAll($datos,200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function select($req)
  {
      //
      $datos = Department::join('countries', 'countries.country_id', '=', 'departaments.country_id')
                                ->select('departaments.*','countries.country_name')
                                ->where('departaments.country_id', '=', $req)
                                ->get();
      return $this->showForSelect($datos,200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
      $request->validate([
      'depart_name' => 'required',
      'country_id' => 'required',
      ]);
      $dato = new Department([
        'depart_name' => $request ->get('depart_name'),
        'country_id' => $request ->get('country_id')
      ]);


      $dato->save();

      return response()->json('Departamento agregado con exito!');
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Department  $country
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //

      $dato = Country::join('departaments', 'countries.country_id', '=', 'departaments.country_id')
                          ->where([
                            ['departaments.deleted_at', '=', null] ,
                             ['departaments.depart_id', '=', $id],
                          ])
                          ->select('departaments.*', 'countries.country_name')
                          ->first();

      return $this->showOne($dato,200);
  }

  // public function edit(Department $department)
  // {
  //     //
  //     $department = Department::find($department);
  //     return response()->json($department);
  // }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = Department::find($id);
      $keys = collect($request->all())->keys();
      for ($i=0; $i < count($keys); $i++) {
        $dato[$keys[0]]= $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Departamento se restauro con exito!');
    }
    else{
      $request->validate([
      'depart_name' => 'required',
      'country_id' => 'required',
      ]);
      $dato = Department::find($id);
      $dato->depart_name=$request->get('depart_name');
      $dato->country_id = $request->get('country_id');
      $dato->save();
      return response()->json('Departamento actualizado con exito');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Department  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $dato = Department::find($id);
      $dato->delete();
      return response()->json('Departamento se elimino con exito');
  }

public function country($id){
  $datos = Department::where('country_id', '=', $id)->get();

        return response()->json($datos);

}


  public function search($request){
      $data = Department::join('countries', 'countries.country_id', '=', 'departaments.country_id')
                    ->where('departaments.depart_name','ILIKE','%'.$request.'%')
                    ->orWhere('countries.country_name','ILIKE','%'.$request.'%')
                    ->get();
    return $this->showAll($data,200);
  //  return response()->json($data);
  }
}
