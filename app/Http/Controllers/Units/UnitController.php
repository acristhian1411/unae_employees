<?php

namespace App\Http\Controllers\Units;


use App\Unit;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UnitController extends ApiController
{
  public function index(Request $req)
  {
    // dd($req->unit_type);

      $t = Unit::query()->first();

      if ($req->unit_type != null) {
        $query = Unit::where([['unit_type_id', '=', $req->unit_type], ['deleted_at', '=', null]]);
      }
      else {
        $query = Unit::query();
      }
      $query = $this->filterData($query, $t);
      $datos = $query->get();

      return $this->showAll($datos,200);
      //return response()->json($units);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $request->validate([
    'unit_name' => 'required',
    'unit_code' => 'required|unique:units',
    ]);
      if($request->get('unit_logo')===null && $request->file('unit_logo')===null){
        $image = 'default.png';
        $request->request->add(['unit_logo'=>$image]);
      }
      elseif (($request->get('unit_logo')==="default.png")) {
        $request->request->add(['unit_logo'=>"default.png"]);
      }
      else {
          $image = $request->get('unit_logo');
          $imageName = Str::random(20).'.png';
          $imagen = Image::make($image)->encode('png', 75);
          $destinationPath = public_path('img');
          $imagen->resize(530, 470)->save($destinationPath."/".$imageName);
          $request->request->add(['unit_logo'=>$imageName]);
      }
      $dato = new Unit([
        'unit_name' => $request ->get('unit_name'),
        'unit_code' => $request ->get('unit_code'),
        'unit_logo' => $request ->get('unit_logo'),
        'unit_company' => $request ->get('unit_company'),
        'comp_det_id' => $request ->get('comp_det_id'),
        'unit_type_id' => $request ->get('unit_type_id'),
      ]);


       $dato->save();

      return response()->json('Institucion agregada con exito!');
  }
  public function show($id){
      //
      $unit = Unit::join('company_details', 'company_details.comp_det_id','=', 'units.comp_det_id')
      ->select('units.*', 'company_details.comp_det_bussiness_name', 'company_details.comp_det_ruc')
      ->where('unit_id',$id)
      ->first();
      //return response()->json($unit);
      return $this->showOne($unit,200);
  }

  public function update(Request $request, $id){
    if ($request->audit_restore) {
      $dato = Unit::find($id);
      $keys = collect($request->all())->keys();
      for ($i=0; $i < count($keys); $i++) {
        $dato[$keys[0]]= $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Institucion se restauro con exito!');
    }
    else{
    $request->validate([
    'unit_name' => 'required',
    'unit_code' => 'required',
    ]);
    $unit = Unit::find($id);
    $unit->unit_name=$request->get('unit_name');
      $unit->unit_code = $request->get('unit_code');
      //
      if(!($request->get('unit_logo')===$unit->unit_logo)){
        if($request->get('unit_logo')===null && $request->file('unit_logo')===null){
          $image = 'default.png';
          $request->request->add(['unit_logo'=>$image]);
        }
        elseif (($request->get('unit_logo')==="default.png")) {
          $request->request->add(['unit_logo'=>"default.png"]);
        }
        else {
            $image = $request->get('unit_logo');
            $imageName = Str::random(20).'.png';
            $imagen = Image::make($image)->encode('png', 75);
            $destinationPath = public_path('img');
            $imagen->resize(530, 470)->save($destinationPath."/".$imageName);
            $request->request->add(['unit_logo'=>$imageName]);
        }
      }
      $unit->unit_logo = $request->get('unit_logo');
      $unit->unit_company = $request->get('unit_company');
      $unit->comp_det_id = $request->get('comp_det_id');
      $unit->unit_type_id = $request->get('unit_type_id');
      $unit->save();
      return response()->json('Institucion actualizada con exito!');
  }
}

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Unit  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($unit_id)
  {
      $unit = Unit::find($unit_id);
      $unit->delete();
      return response()->json('Institucion se elimino con exito');
  }
  public function search($request){
      $data = Unit::where('unit_name','ILIKE','%'.$request.'%')
                    ->orWhere('unit_code','ILIKE','%'.$request.'%')
                    ->get();
    return response()->json($data);
  }
}
