<?php

namespace App\Http\Controllers\Employees;

// use Illuminate\Support\Facades\DB;
/**
 * Se crea una clase aparte para tener mejor orden y entendimiento
 */
class EmployeesAdvancesFilters
{
  /*
  Filtros para la busqueda avanzada
  Retorna una cadena de caracteres con los filtros de busqueda para poder concatenar con la peticion sql
  */
  public function applyFilter($filters)
  {
    $string_where = "";

    // dd($filters);
    if (array_key_exists("person_idnumber", $filters) == true && $filters["person_idnumber"] !== null) {
      $string_where = "person_idnumber ilike " . '$$%' . $filters["person_idnumber"] . '%$$';
    }
    if (array_key_exists('person_fname', $filters) == true && $filters["person_fname"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and person_fname ilike " . '$$%' . $filters["person_fname"] . '%$$';
      } else {
        $string_where = "person_fname ilike " . '$$%' . $filters["person_fname"] . '%$$';
      }
    }
    if (array_key_exists("person_lastname", $filters) == true && $filters["person_lastname"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and person_lastname ilike " . '$$%' . $filters["person_lastname"] . '%$$';
      } else {
        $string_where = "person_lastname ilike " . '$$%' . $filters["person_lastname"] . '%$$';
      }
    }
    if (array_key_exists("cntper_data", $filters) == true && $filters["cntper_data"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and cntper_data ilike " . '$$%' . $filters["cntper_data"] . '%$$';
      } else {
        $string_where = "cntper_data ilike " . '$$%' . $filters["cntper_data"] . '%$$';
      }
    }
    if (array_key_exists("nationality", $filters) == true && $filters["nationality"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and country_id = " . $filters["nationality"];
      } else {
        $string_where = "country_id = " . $filters["nationality"];
      }
    }
    if (array_key_exists("birthdate", $filters) == true && $filters["birthdate"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and person_birthdate = " . $filters["birthdate"];
      } else {
        $string_where = "person_birthdate = " . $filters["birthdate"];
      }
    }
    if (array_key_exists("school", $filters) == true && $filters["school"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and school_id = " . $filters["school"];
      } else {
        $string_where = "school_id = " . $filters["school"];
      }
    }
    return ($string_where);
    // return array_key_exists("person_fname", $filters);
  }
}
