<?php

namespace App\Http\Controllers\Employees;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiDBController;
use App\Http\Controllers\Employees\EmployeesAdvancesFilters;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use App\Employee;
use App\Person;
use App\EmployeeAccount;
use App\WorkingHour;
use App\ContactPerson;
use EloquentBuilder;

class EmployeesController extends ApiDBController
{
  public function index()
  {
    //
    // $datos = Employee::join('countries', 'countries.country_id', '=', 'employees.country_id')
    //   ->join('appointments', 'appointments.appoin_id', '=', 'employees.app_appointment_id')
    //   ->select('employees.*', 'countries.country_name', 'appointments.appoin_description', DB::raw("get_telephone(employees.person_id) as telephone"))
    //   ->get();
    //
    // return $this->showAll($datos, 200);

    $count = DB::select('select count(*) from employees e where e.deleted_at is null');
    $datos = "select e.*, c.country_name, get_telephone(e.person_id) as telephone, a.appoin_description
      from employees e
      join countries c on c.country_id = e.country_id
      join appointments a on a.appoin_id = e.app_appointment_id
      where e.deleted_at is null";
    // dd($datos);
    return $this->showAll($datos, $count, 200);
  }
  public function getWorkingHours($id)
  {
    $data = DB::select("select *, true saved, false deleted from working_hours wh where wh.deleted_at is null and wh.person_id=" . $id);
    return response()->json($data);
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
    if ($request->get('person_id')) {
      $person = Person::find($request->get('person_id'));
      $datos = new Employee([
        'till_id' =>  $person->till_id,
        'person_id' =>  $person->person_id,
        'homecity_id' => $person->homecity_id,
        'birthplace_id' => $person->birthplace_id,
        'country_id' => $person->country_id,
        'person_fname' => $person->person_fname,
        'person_lastname' => $person->person_lastname,
        'person_birthdate' => $person->person_birthdate,
        'person_gender' => $person->person_gender,
        'person_idnumber' => $person->person_idnumber,
        'person_address' => $person->person_address,
        'person_bloodtype' => $person->person_bloodtype,
        'person_photo' => $person->person_photo,
        'person_business_name' => $person->person_business_name,
        'person_ruc' => $person->person_ruc,
        'app_appointment_id' => $request->get('app_appointment_id'),
        'emplo_startdate' => $request->get('emplo_startdate'),
        'emplo_status' => $request->get('emplo_status'),
        'emplo_observation' => $request->get('emplo_observation'),
        'emplo_salary' => $request->get('emplo_salary'),
        'office_desc' => $request->get('office_desc'),
        'number_of_children' => $request->get('number_of_children'),
        'civil_status' => $request->get('civil_status')
      ]);
    } else {


      if ($request->get('person_photo') === null && $request->file('person_photo') === null) {
        $image = 'default.png';
        $request->request->add(['person_photo' => $image]);
      } elseif (($request->get('person_photo') === "default.png")) {
        $request->request->add(['person_photo' => "default.png"]);
      } else {
        $image = $request->get('person_photo');
        $imageName = Str::random(20) . '.png';
        $imagen = Image::make($image)->encode('png', 75);
        $destinationPath = public_path('img');
        $imagen->save($destinationPath . "/" . $imageName);
        $request->request->add(['person_photo' => $imageName]);
      }
      $dato = new Person([

        'homecity_id' => $request->get('homecity_id'),
        'birthplace_id' => $request->get('birthplace_id'),
        'country_id' => $request->get('country_id'),
        'person_fname' => $request->get('person_fname'),
        'person_lastname' => $request->get('person_lastname'),
        'person_birthdate' => $request->get('person_birthdate'),
        'person_gender' => $request->get('person_gender'),
        'person_idnumber' => $request->get('person_idnumber'),
        'person_address' => $request->get('person_address'),
        'person_bloodtype' => $request->get('person_bloodtype'),
        'person_photo' => $request->get('person_photo'),
        'person_business_name' => $request->get('person_business_name'),
        'person_ruc' => $request->get('person_ruc'),
      ]);

      $dato->save();

      $datos = new Employee([
        'person_id' =>  $dato->person_id,
        'till_id' =>  $request->till_id,
        'homecity_id' => $request->get('homecity_id'),
        'birthplace_id' => $request->get('birthplace_id'),
        'country_id' => $request->get('country_id'),
        'person_fname' => $request->get('person_fname'),
        'person_lastname' => $request->get('person_lastname'),
        'person_birthdate' => $request->get('person_birthdate'),
        'person_gender' => $request->get('person_gender'),
        'person_idnumber' => $request->get('person_idnumber'),
        'person_address' => $request->get('person_address'),
        'person_bloodtype' => $request->get('person_bloodtype'),
        'person_photo' => $request->get('person_photo'),
        'person_business_name' => $request->get('person_business_name'),
        'person_ruc' => $request->get('person_ruc'),
        'app_appointment_id' => $request->get('app_appointment_id'),
        'emplo_startdate' => $request->get('emplo_startdate'),
        'emplo_status' => $request->get('emplo_status'),
        'emplo_observation' => $request->get('emplo_observation'),
        'emplo_salary' => $request->get('emplo_salary'),
        'office_desc' => $request->get('office_desc'),
        'number_of_children' => $request->get('number_of_children'),
        'civil_status' => $request->get('civil_status')
      ]);
    }
    $datos->save();
    if ($request->whours && isset($request->whours)) {
      foreach ($request->whours as $value) {
        // aqui se debe insertar las horas crear el controller?
        $whour = new WorkingHour([
          "wh_day" => $value["wh_day"],
          "wh_start" => $value["wh_start"],
          "wh_end" => $value["wh_end"],
          "person_id" => $datos->person_id
        ]);
        $whour->save();
      }
    }
    return response()->json([
      'message' => 'Persona se agrego con exito!',
      'dato' => $datos
    ]);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Student  $country
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //

    $dato = Employee::join('countries', 'employees.country_id', '=', 'countries.country_id')
      ->join('appointments', 'appointments.appoin_id', '=', 'employees.app_appointment_id')
      ->where('employees.person_id', '=', $id)
      ->select('employees.*', 'countries.country_name', 'appointments.appoin_description')
      ->first();
    if ($dato === null) {
      return response()->json(false);
    }
    return $this->showOne($dato, 200);
  }
  public function laboral_data($id)
  {
    // $id = person id
    $datos = DB::select('select e.emplo_startdate, e.emplo_status, e.emplo_observation, e.emplo_salary, has_ips, porcent_ips from employees e where e.deleted_at is null and e.person_id = ' . $id);
    if (count($datos) > 0) {
      $cuentas = DB::select(
        'select * from employee_account ea 
        where ea.deleted_at is null and ea.is_discount is true and ea.person_id = ' . $id . "
        order by ea.emp_acc_expiration asc, ea.emp_acc_desc asc
        "
      );
      // dd($datos[0]->cuentas);
      return response()->json(["employ_detail" => $datos[0], "cuentas" => $cuentas]);
    } else {
      return response()->json(["employ_detail" => [], "cuentas" => []]);
    }

    // dd($datos);
  }
  public function updateSalary(Request $request, $id)
  {
    $dato = Employee::find($id);
    $dato->emplo_salary = $request->employ_detail['emplo_salary'];
    $dato->has_ips = $request->employ_detail['has_ips'];
    $dato->porcent_ips = $request->employ_detail['porcent_ips'];
    $dato->emplo_observation = $request->employ_detail['emplo_observation'];
    $dato->save();
    return response()->json('Se actualizo con exito');
  }

  public function addDiscount(Request $request, $id)
  {
    $len = count($request->emp_account);
    try {
      for ($i = 0; $i < $len; $i++) {
        $datos = new EmployeeAccount([
          'person_id' =>  $id,
          'emp_acc_desc' =>  $request->emp_account[$i]['description'],
          'emp_acc_amount' =>  $request->emp_account[$i]['amount'],
          'emp_acc_expiration' => $request->emp_account[$i]['fecha'],
          'is_discount' => true
        ]);
        $datos->save();
      }
      return response()->json("Se guardo con exito");
    } catch (Exception $e) {
      return response()->json($e);
    }
  }

  public function generateSalary()
  {
    $year = date("Y");
    $month = $year . "-" . date("m");
    $desc = "Salario mes " . date("m") . " anho " . $year;
    $fecha = $year . "-" . date("m") . "-" . "5";
    $emploAccount = DB::select("select gen_employee_salary('" . $month . "','" . $desc . "','" . $fecha . "') as salary");
    if ($emploAccount[0]->salary == true) {
      return response()->json('Salarios generados con exito');
    } else {
      return response()->json('Ya existen salarios para ' . $month);
    }
  }


  public function update(Request $request, $id)
  {
    //
    if ($request->audit_restore) {
      $dato = Employee::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Funcionario se restauro con exito!');
    } else {
      $dato = Employee::find($id);
      $dato->till_id = $request->get('till_id');
      $dato->homecity_id = $request->get('homecity_id');
      $dato->birthplace_id = $request->get('birthplace_id');
      $dato->country_id = $request->get('country_id');
      $dato->person_fname = $request->get('person_fname');
      $dato->person_lastname = $request->get('person_lastname');
      $dato->person_birthdate = $request->get('person_birthdate');
      $dato->person_gender = $request->get('person_gender');
      $dato->person_idnumber = $request->get('person_idnumber');
      $dato->person_address = $request->get('person_address');
      $dato->person_bloodtype = $request->get('person_bloodtype');
      $dato->person_business_name = $request->get('person_business_name');
      $dato->person_ruc = $request->get('person_ruc');
      $dato->app_appointment_id = $request->get('app_appointment_id');
      $dato->emplo_startdate = $request->get('emplo_startdate');
      $dato->emplo_status = $request->get('emplo_status');
      $dato->emplo_observation = $request->get('emplo_observation');
      $dato->emplo_salary = $request->get('emplo_salary');
      $dato->office_desc = $request->get('office_desc');
      $dato->number_of_children = $request->get('number_of_children');
      $dato->civil_status = $request->get('civil_status');

      if (!($request->get('person_photo') === $dato->person_photo)) {
        if ($request->get('person_photo') === null && $request->file('person_photo') === null) {
          $image = 'default.png';
          $request->request->add(['person_photo' => $image]);
        } elseif (($request->get('person_photo') === "default.png")) {
          $request->request->add(['person_photo' => "default.png"]);
        } else {
          $image = $request->get('person_photo');
          $imageName = Str::random(20) . '.png';
          $imagen = Image::make($image)->encode('png', 75);
          $destinationPath = public_path('img');
          $imagen->save($destinationPath . "/" . $imageName);
          $request->request->add(['person_photo' => $imageName]);
        }
      }
      $dato->person_photo = $request->get('person_photo');
      $dato->save();
      if ($request->whours && isset($request->whours)) {
        foreach ($request->whours as $value) {
          // aqui se debe insertar las horas crear el controller?
          if ($value["saved"] && $value["deleted"]) {
            $whour = WorkingHour::find($value["wh_id"]);
            $whour->delete();
          } elseif ($value["saved"]) {
            $whour = WorkingHour::find($value["wh_id"]);
            $whour->wh_day = $value["wh_day"];
            $whour->wh_start = $value["wh_start"];
            $whour->wh_end = $value["wh_end"];
            $whour->save();
          } else {
            $whour = new WorkingHour([
              "wh_day" => $value["wh_day"],
              "wh_start" => $value["wh_start"],
              "wh_end" => $value["wh_end"],
              "person_id" => $dato->person_id
            ]);
            $whour->save();
          }
        }
      }
      return response()->json('Funcionario se actualizo con exito');
    }
  }

  public function updateFromPerson(Request $request, $id)
  {
    //
    $dato = Employee::find($id);

    $dato->app_appointment_id = $request->get('app_appointment_id');
    $dato->till_id = $request->get('till_id');
    $dato->emplo_startdate = $request->get('emplo_startdate');
    $dato->emplo_status = $request->get('emplo_status');
    $dato->emplo_observation = $request->get('emplo_observation');
    $dato->emplo_salary = $request->get('emplo_salary');

    $dato->save();
    return response()->json('Funcionario se actualizo con exito');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Student  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = Employee::find($id);
    $dato->delete();
    return response()->json('Funcionario se elimino con exito');
  }
  public function search(Request $request)
  {
    if (!$request->filter) {
      $word_filter = explode(" ", $request->search);
      $array_like = "";
      if (count($word_filter) == 2) {
        $array_like = "or (unaccent(e.person_fname) ilike '%" . $word_filter[0] . "%' and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[1] . "%'))
          or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike '%" . $word_filter[0] . "%') ";
      } elseif (count($word_filter) == 3) {
        $array_like = "or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[2] . "%)')
          or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[2] . "%'))
          or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[0] . "%'))
            or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[0] . "%')) ";
      } elseif (count($word_filter) == 4) {
        $array_like = "or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[3] . "%'))
          or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[0] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[3] . "%'))
          or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[3] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[0] . "%'))
            or (unaccent(e.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(e.person_fname) ilike unaccent('%" . $word_filter[2] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[1] . "%') and" .
          " unaccent(e.person_lastname) ilike unaccent('%" . $word_filter[0] . "%')) ";
      }
      $count = DB::select("select  count( distinct e.person_id) from employees e left join contact_persons cp on cp.person_id = e.person_id
      where e.deleted_at is null and (cp.cntper_data ilike " . '$$%' . $request->search . '%$$' .
        " or  unaccent(e.person_fname) ilike unaccent(" . '$$%' . $request->search . '%$$' .
        ") or unaccent(e.person_lastname) ilike unaccent(" . '$$%' . $request->search . '%$$' .
        ") or e.person_idnumber ilike " . '$$%' . $request->search . '%$$' .
        " or (select concat(person_fname, ' ', person_lastname) as full_name) ilike '%" . $request->search . "%'
            " . $array_like . ")");
      $data = "select distinct e.person_id, e.*, c.country_name, ap.appoin_description, get_telephone(e.person_id) as telephone
      from employees e left join contact_persons cp on cp.person_id = e.person_id
      left join countries c on c.country_id = e.country_id
      left join appointments ap on ap.appoin_id = e.app_appointment_id
      where e.deleted_at is null and (cp.cntper_data ilike " . '$$%' . $request->search . '%$$' .
        " or  unaccent(e.person_fname) ilike unaccent(" . '$$%' . $request->search . '%$$' .
        ") or unaccent(e.person_lastname) ilike unaccent(" . '$$%' . $request->search . '%$$' .
        ") or e.person_idnumber ilike " . '$$%' . $request->search . '%$$' .
        " or (select concat(unaccent(person_fname), ' ', unaccent(person_lastname)) as full_name) ilike unaccent('%" . $request->search . "%')
            " . $array_like . ")";
    } else {
      $filters = new EmployeesAdvancesFilters;
      $applied_filter = $filters->applyFilter($request->filter);
      $count = DB::select("select count(distinct e.person_id) from employees e join contact_persons on contact_persons.person_id = e.person_id where e.deleted_at is null and (" . $applied_filter . ")");
      $data = "select distinct e.person_id, e.*, get_telephone(e.person_id) as telephone from employees e join contact_persons on contact_persons.person_id = e.person_id where e.deleted_at is null and (" . $applied_filter . ")";
      // $data = EloquentBuilder::setFilterNamespace('App\\EloquentFilters\\Persons\\')->to(Student::class, $request->filter)->get();
    }
    // dd($count);
    if (!isset($count[0]->count)) {
      // dd($data);
      return response()->json(false);
    } else {
      return $this->showAll($data, $count, 200);
    }
  }
}
