<?php
namespace App\Http\Controllers\Tariffs;

// use Illuminate\Support\Facades\DB;
/**
 * Se crea una clase aparte para tener mejor orden y entendimiento
 */
class TariffsAdvancesFilters {
  /*
  Filtros para la busqueda avanzada
  Retorna una cadena de caracteres con los filtros de busqueda para poder concatenar con la peticion sql
  */
  public function applyFilter($filters)
  {
    $string_where = "";

    // dd($filters);
    if (array_key_exists("tariff_caryear", $filters) == true && $filters["tariff_caryear"] !== null) {
      $string_where = "t.tariff_caryear = ".$filters["tariff_caryear"];
    }
    if (array_key_exists('career_name', $filters) == true && $filters["career_name"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where." and c.career_name ilike ".'$$%'.$filters["career_name"].'%$$';
      }else {
        $string_where = "c.career_name ilike ".'$$%'.$filters["career_name"].'%$$';
      }
    }
    if (array_key_exists("tariff_payamount", $filters) == true && $filters["tariff_payamount"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where." and t.tariff_payamount = ".$filters["tariff_payamount"];
      }else {
        $string_where = "t.tariff_payamount = ".$filters["tariff_payamount"];
      }
    }
    if (array_key_exists("fcaradm_year", $filters) == true && $filters["fcaradm_year"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where." and fa.fcaradm_year = ".$filters["fcaradm_year"];
      }else {
        $string_where = "fa.fcaradm_year = ".$filters["fcaradm_year"];
      }
    }
    return ($string_where);
    // return array_key_exists("person_fname", $filters);
  }

}
