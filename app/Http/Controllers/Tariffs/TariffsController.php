<?php

namespace App\Http\Controllers\Tariffs;

use App\Http\Controllers\ApiDBController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Tariff;
use App\Http\Controllers\Tariffs\TariffsAdvancesFilters;

class TariffsController extends ApiDBController
{
  public function index()
  {

    // $datos = Tariff::join ('facucar_adm', 'facucar_adm.fcaradm_id', '=', 'tariffs.fcaradm_id')
    //
    //   ->select('tariffs.*', 'facucar_adm.fcaradm_year')
    //   ->orderBy('tariffs.tariff_caryear', 'asc')
    //   ->get();
    //   return $this->showAll($datos,200);
    $count = DB::select('select count(*) from tariffs t join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                           join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                           join careers c on c.career_id = fc.career_id
                           where t.deleted_at is null');
    $datos = "select t.*, c.career_name, fa.fcaradm_year from tariffs t
                join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                join careers c on c.career_id = fc.career_id
                where t.deleted_at is null
                ";
    // dd($datos);
    return $this->showAll($datos, $count, 200);
  }

  public function store(Request $request)
  {
    // $request->validate([
    //   'fcaradm_id' => 'required',
    //   'tariff_payamount_mat' => 'required',
    //   'tariff_payamount_quot' => 'required',
    //   'tariff_caryear' => 'required',
    //   'tariff_regamount' => 'required',
    // ]);
    $datos = new Tariff($request->all());
    $datos->save();
    return response()->json([
      'message' => 'Tarifa se actualizo correctamente!',
      'dato' => $datos
    ]);
  }

  public function show($id)
  {
    $datos = Tariff::find($id);
    return $this->showOne($datos, 200);

    // $dato = Tariff::where( 'tariff_id', '=', $id)
    //                           ->first();
    // return $this->showOne($dato,200);
  }

  public function showFacucarAdm(Request $request, $id)
  {
    $currentDate = date("Y");
    if ($request->all == 'false') {
      $datos = Tariff::where([['fcaradm_id', $id], ['tariff_caryear', $currentDate]])
        ->first();
      if ($datos === null) {
        return response()->json(false);
      }
      return $this->showOne($datos, 200);
    } elseif ($request->all == 'true') {
      $count = DB::select("select count(*) from tariffs t where fcaradm_id = " . $id);
      $datos = "select * from tariffs t where fcaradm_id = " . $id . " order by tariff_caryear";
      // $datos = Tariff::where([['fcaradm_id', $id]])
      // ->orderBy('tariff_caryear')
      // ->get();
      // if ($datos->isEmpty()) {
      //   return response()->json(false);
      // }
      return $this->showAll($datos, $count, 200);
    }


    // $dato = Tariff::where( 'tariff_id', '=', $id)
    //                           ->first();
    // return $this->showOne($dato,200);
  }


  public function showCareer($id)
  {
    $currentDate = date("Y");
    // $datos = Tariff::join ('facucar_adm', 'facucar_adm.fcaradm_id', '=', 'tariffs.fcaradm_id')
    //   ->join ('facu_careers', 'facu_careers.facu_career_id', '=', 'facucar_adm.facu_career_id')
    //   ->where([['facu_careers.career_id', $id], ['tariffs.tariff_caryear', $currentDate]])
    //   ->get();
    $count = DB::select("select count(t.*) from tariffs t
                           join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                           join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                           where fc.facu_career_id = " . $id . " and t.tariff_caryear = " . $currentDate);
    $datos = "select t.* from tariffs t
                           join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                           join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                           where fc.facu_career_id = " . $id . " and t.tariff_caryear = " . $currentDate;
    // dd($count[0]->count);
    if (!$count[0]->count > 0) {
      return response()->json(false);
    }
    return $this->showAll($datos, $count, 200);
  }

  public function update(Request $request, $id)
  {
    $dato = Tariff::findOrFail($id);
    // $request->validate([
    //   'fcaradm_id' => 'required',
    //   'tariff_payamount_mat' => 'required',
    //   'tariff_payamount_quot' => 'required',
    //   'tariff_regamount' => 'required',
    // ]);
    $dato->fcaradm_id = $request->fcaradm_id;
    $dato->tariff_payamount_quot = $request->tariff_payamount_quot;
    $dato->tariff_payamount_mat = $request->tariff_payamount_mat;
    $dato->tariff_caryear = $request->tariff_caryear;
    $dato->tariff_regamount = $request->tariff_regamount;
    $dato->tariff_obs = $request->tariff_obs;
    $dato->save();
    return response()->json([
      'message' => 'Tarifa se actualizo correctamente!',
      'dato' => $dato
    ]);
  }
  public function destroy($id)
  {
    $dato = Tariff::find($id);
    $dato->delete();
    return response()->json([
      'message' => 'Tariff se elimino correctamente'
    ]);
  }
  public function search(Request $request)
  {
    if (!$request->filter) {
      $count = DB::select("select count(*) from tariffs t
                           join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                           join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                           join careers c on c.career_id = fc.career_id
                           where t.deleted_at is null and c.career_name ilike " . '$$%' . $request->search . '%$$');

      $data = "select t.*, c.career_name, fa.fcaradm_year from tariffs t
                           join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                           join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                           join careers c on c.career_id = fc.career_id
                           where t.deleted_at is null and c.career_name ilike " . '$$%' . $request->search . '%$$';

      // $data = Person::where('person_fname', 'ILIKE', '%' . $request->search . '%')
      //   ->orWhere('person_lastname', 'ILIKE', '%' . $request->search . '%')
      //   ->orWhere('person_idnumber', 'ILIKE', '%' . $request->search . '%')
      //   ->distinct()
      //   ->get();
      // dd(DB::select($data));
    } else {

      // dd("llega aca");
      $filters = new TariffsAdvancesFilters;
      $applied_filter = $filters->applyFilter($request->filter);
      $count = DB::select("select count(*) from tariffs t
                            join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                            join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                            join careers c on c.career_id = fc.career_id
                            where t.deleted_at is null and " . $applied_filter);
      $data = "select * from tariffs t
                            join facucar_adm fa on fa.fcaradm_id = t.fcaradm_id
                            join facu_careers fc on fc.facu_career_id = fa.facu_career_id
                            join careers c on c.career_id = fc.career_id
                            where t.deleted_at is null and " . $applied_filter;
    }


    return $this->showAll($data, $count, 200);
  }
}
