<?php

namespace App\Http\Controllers\Cities;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\City;
use Illuminate\Support\Facades\DB;

class CitiesController extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $datos = City::join('departaments', 'departaments.depart_id', '=', 'cities.depart_id')
      ->join('countries', 'countries.country_id', '=', 'departaments.country_id')
      ->select('cities.*', 'departaments.depart_name', 'countries.country_id', 'countries.country_name')
      ->get();


    return $this->showAll($datos, 200);
  }

  public function select($req)
  {
    $datos = City::join('departaments', 'departaments.depart_id', '=', 'cities.depart_id')
      ->join('countries', 'countries.country_id', '=', 'departaments.country_id')
      ->select('cities.*', 'departaments.depart_name')
      ->where('cities.depart_id', '=', $req)
      ->get();
    return $this->showForSelect($datos, 200);
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    $request->validate([
      'city_name' => 'required',
      'depart_id' => 'required',
    ]);

    $datos = new City($request->all());
    $datos->save();

    return response()->json('Ciudad se agrego con exito!');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $datos = City::join('departaments', 'departaments.depart_id', '=', 'cities.depart_id')
      ->join('countries', 'departaments.country_id', '=', 'countries.country_id')
      ->select('cities.*', 'departaments.depart_name', 'departaments.depart_id', 'countries.country_id', 'countries.country_name')
      ->where('cities.city_id', '=', $id)
      ->first();
    return $this->showOne($datos, 200);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = City::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Ciudad se restauro con exito!');
    } else {
      $dato = City::findOrFail($id);
      $request->validate([
        'city_name' => 'required',
        'depart_id' => 'required',
      ]);


      $dato->city_name = $request->city_name;
      $dato->depart_id = $request->depart_id;
      $dato->save();
      return response()->json([
        'message' => 'Ciudad se actualizo con exito!',
        'dato' => $dato
      ]);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = City::findOrFail($id);
    $dato->delete();
    return response()->json([
      'message' => 'Ciudad se elimino con exito!'
    ]);
  }
  public function search($request)
  {
    $data = City::join('departaments', 'departaments.depart_id', '=', 'cities.depart_id')
      ->join('countries', 'countries.country_id', '=', 'departaments.country_id')
      ->select('cities.*', 'departaments.depart_name', 'countries.country_id', 'countries.country_name')
      ->where('cities.city_name', 'ILIKE', '%' . $request . '%')
      ->get();
    return $this->showAll($data, 200);
  }
}
