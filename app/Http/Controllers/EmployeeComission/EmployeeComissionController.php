<?php

namespace App\Http\Controllers\EmployeeComission;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\EmployeeComission;
use Illuminate\Support\Facades\DB;

class EmployeeComissionController extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

    $datos = EmployeeComission::all();


    return $this->showAll($datos, 200);
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {


    $career_id = 0;
    $employee_id = 0;
    $porcentaje = DB::table('careers')->join('semesters', 'semesters.career_id', '=', 'careers.career_id')
      ->where('semesters.sems_id', '=', $request->sems_id)->first();
    if ($porcentaje->career_pid == 0) {
      $career_id = $porcentaje->career_id;
    } else {
      $career_id = $porcentaje->career_pid;
    }
    if (($request->employee_id)) {
      $employee_id = $request->employee_id;
    } else {
      $employee = DB::table('consult')->where([
        ['career_id', '=', $career_id],
        ['cons_obs', '=', 'Inscripcion confirmada'],
        ['student_id', '=', $request->student_id]
      ])->first();
      if (isset($employee)) {
        $employee_id = $employee->employee_id;
      }
    }
    if ($employee_id != 0) {

      $matricula = DB::table('student_account')->where([['enroll_id', '=', $request->enroll_id], ['staccount_type', '=', 'Matricula'], ['oper_id', '=', 0]])->first();
      $amount = $matricula->staccount_quot_amount * $porcentaje->career_comission;

      $datos = new EmployeeComission([
        'person_id' => $employee_id,
        'emp_com_date' => $request->emp_com_date,
        'enroll_id' => $request->enroll_id,
        'emp_com_amount' => $amount,
      ]);
      $datos->save();

      return response()->json('Comision se agrego con exito!');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $datos = EmployeeComission::where('emp_com_id', '=', $id)
      ->first();
    return $this->showOne($datos, 200);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function showEmployee($id)
  {
    $datos = EmployeeComission::where('person_id', '=', $id)
      ->get();
    return $this->showAll($datos, 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = EmployeeComission::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Se restauro con exito!');
    } else {
      $dato = EmployeeComission::findOrFail($id);

      $dato->person_id = $request->person_id;
      $dato->enroll_id = $request->enroll_id;
      $dato->emp_com_date = $request->emp_com_date;
      $dato->emp_com_amount = $request->emp_com_amount;
      $dato->emp_com_obs = $request->emp_com_obs;
      $dato->save();
      return response()->json([
        'message' => 'Se actualizo con exito!',
        'dato' => $dato
      ]);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = EmployeeComission::findOrFail($id);
    $dato->delete();
    return response()->json([
      'message' => 'Se elimino con exito!'
    ]);
  }
}
