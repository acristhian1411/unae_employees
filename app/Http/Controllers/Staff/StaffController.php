<?php

namespace App\Http\Controllers\Staff;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Staff;
use Illuminate\Support\Facades\DB;

class StaffController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $t = Staff::query()->first();
      $query = Staff::query();
      $query = $this->filterData($query, $t);
      $datos = $query->get();
        return $this->showAll($datos,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
      'person_id' => 'required',
      'facu_id' => 'required',
  ]);
 $datos = new Staff($request->all());
 $datos->save();
  return response()->json('Registro se agrego con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $dato = Staff::join('faculties', 'faculties.facu_id', '=', 'staff.facu_id')
      ->join('employees', 'employees.person_id', '=', 'staff.person_id')
                                ->select('staff.*','faculties.facu_name', DB::raw("CONCAT(employees.person_fname, ' ' ,employees.person_lastname) AS employee_name"))
                                ->where('staff.staff_id', '=', $id)
                                ->first();
      return $this->showOne($dato,200);
    }

    public function showFaculties($id){
      $dato = Staff::join('faculties', 'faculties.facu_id', '=', 'staff.facu_id')
      ->join('employees', 'employees.person_id', '=', 'staff.person_id')
      ->join('appointments', 'appointments.appoin_id', '=', 'employees.app_appointment_id')
                                ->select('staff.*','faculties.facu_name','appointments.appoin_description', DB::raw("CONCAT(employees.person_fname, ' ' ,employees.person_lastname) AS employee_name"))
                                ->where('staff.facu_id', '=', $id)
                                ->get();
      return $this->showAll($dato,200);
    }

    public function showEmployees($id){
      $dato = Staff::join('faculties', 'faculties.facu_id', '=', 'staff.facu_id')
      ->join('employees', 'employees.person_id', '=', 'staff.person_id')
      ->join('appointments', 'appointments.appoin_id', '=', 'employees.app_appointment_id')
                                ->select('staff.*','faculties.facu_name','appointments.appoin_description', DB::raw("CONCAT(employees.person_fname, ' ' ,employees.person_lastname) AS employee_name"))
                                ->where('staff.person_id', '=', $id)
                                ->first();
      return $this->showOne($dato,200);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if ($request->audit_restore) {
        $dato = Staff::find($id);
        $keys = collect($request->all())->keys();
        for ($i=0; $i < count($keys); $i++) {
          $dato[$keys[0]]= $request->get($keys[0]);
        }
        $dato->save();
        return response()->json('Registro se restauro con exito!');
      }
      else{
      $dato = Staff::findOrFail( $id);
            $request->validate([
              'facu_id' => 'required',
              'person_id' => 'required',

            ]);
            $dato->facu_id = $request->facu_id;
            $dato->person_id = $request->person_id;

            $dato->save();
            return response()->json([
                'message' => 'Registro se actualizo con exito!',
                'dato' => $dato
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dato = Staff::findOrFail($id);
        $dato->delete();
        return response()->json([
            'message' => 'Caja se elimino con exito!'
        ]);
      }
}
