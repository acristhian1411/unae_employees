<?php

namespace App\Http\Controllers\Roles;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Role;
use App\User;
use App\Permission;

class RoleController extends ApiController
{
  public function index(){
    $datos= Role::all();
    return $this->showAll($datos,200);
  }
  public function hasPermission(Request $request){

    $user = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
                 ->join('users', 'model_has_roles.model_id', '=', 'users.id')
                 ->select('users.*', 'roles.role_name', 'roles.role_id')
                 ->where('model_has_roles.model_id', '=', auth()->user()->id)
                 ->first();
    if ($user == null) {
      $user = $request->user();
      $user->role_id = null;
    }
    $permissions = $request->permission;

    $returnPermission = array();
    // dd($request->permission);
    // dd($user);
    if ($user->role_id==null) {
      return response('false');
    }
    else {
      // $role = Role::find($user->role_id);
      $user1 = User::find(auth()->user()->id);
      // dd( $user1->hasPermissionTo('users_index'));
      for ($i=0; $i < count($permissions); $i++) {
        if ($user1->hasPermissionTo($permissions[$i])) {
          // return response('true');
          $returnPermission[$permissions[$i]] = true;
        }
        else {
          // return response('false');
          $returnPermission[$permissions[$i]] = false;
        }
      }
      return $returnPermission;
    }
  }
  public function getPermissions($role){
      // $role = Role::find($role);
      // $datos = $role->permissions;
      $t = Permission::query()->first();
      $query = Permission::join('role_has_permissions', 'role_has_permissions.permission_id', '=', 'permissions.permission_id')
                          ->where('role_has_permissions.role_id', '=', $role)
                          ->select('permissions.*');
                          // ->get();
      $query = $this->filterData($query, $t);
      $datos = $query->get();
      // dd($datos);
      return $this->showAll($datos,200);
  }
  public function getPermissionsExclude($role){
    $t = Permission::query()->first();
    // $query = Permission::join('role_has_permissions', 'role_has_permissions.permission_id', '=', 'permissions.permission_id')
    //                     ->where('role_has_permissions.role_id', '!=', $role)
    //                     ->select('permissions.*');
                        // ->get();
    $in = DB::table('role_has_permissions')
                ->select('permission_id')
                ->where('role_id','=',$role);
    $query = Permission::select('*')
                        ->whereNotIn('permission_id', $in);

    $query = $this->filterData($query, $t);
    $datos = $query->get();
    // dd($datos);
    return $this->showAll($datos,200);
  }
  public function givePermissions(Request $request){
    $role = Role::find($request->role_id);
    $array_permissions = $request->permissions;
    try {
      for ($i=0; $i < count($array_permissions); $i++) {
        $role->givePermissionTo($array_permissions[$i]['permission_name']);
        // dd($array_permissions[$i]['permission_name']);
      }
      return response()->json('Permisos asignados con exito');
    } catch (\Exception $e) {
      return response()->json($e);
    }

  }
  public function revokePermissions(Request $request){
    $role = Role::find($request->role_id);
    $array_permissions = $request->permissions;
    try {
      for ($i=0; $i < count($array_permissions); $i++) {
        $role->revokePermissionTo($array_permissions[$i]['permission_name']);
        // dd($array_permissions[$i]['permission_name']);
      }
      return response()->json('Permisos removidos con exito');
    } catch (\Exception $e) {
      return response()->json($e);
    }

  }
  public function store(Request $request)
  {

        $request->validate([
        'role_name' => 'required',
    ]);

    $dato = new Role([
      'role_name' => $request ->get('role_name'),
      'guard_name' => 'web',
    ]);
    $dato->save();


    return response()->json(['message'=> 'Rol se agrego con exito!', 'dato'=>$dato]);
  }
  public function show($id)
  {
      $dato = Role::find($id);
      return $this->showOne($dato,200);
  }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = Role::find($id);
      $keys = collect($request->all())->keys();
      for ($i=0; $i < count($keys); $i++) {
        $dato[$keys[0]]= $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Rol se restauro con exito!');
    }
    else{
      $request->validate([
      'role_name' => 'required',
      ]);
      $dato = Role::find($id);
      $dato->role_name=$request->get('role_name');
      $dato->save();
      return response()->json('Rol se actualizo con exito');
  }
}
  public function destroy($id)
  {
      $dato = Role::find($id);
      $dato->delete();
      return response()->json('Rol se elimino con exito');
  }

}
