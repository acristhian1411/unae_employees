<?php

namespace App\Http\Controllers\TillTransfers;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\TillTransfer;
use App\TillDetail;
use App\DettillProofPayment;
use App\ProofPayment;
use Illuminate\Support\Facades\DB;

class TillTransfersController extends ApiController
{
  public function index()
  {
    $t = TillTransfer::query()->first();
    $query = TillTransfer::query();
    $query = $this->filterData($query, $t);
    $datos = $query->get();
    return $this->showAll($datos, 200);
  }

  public function store(Request $request)
  {
    $request->validate([
      'origin_till_id' => 'required',
      'destiny_till_id' => 'required',
      'tilltrans_amount' => 'required',
    ]);
    $today = date("Y-m-d");

    $year = date("Y");
    $month = date("m");
    $day = date("d");
    $fechas = $year . "-" . ($month) . "-" . $day;
    $dato = collect(\DB::select("select gettillamount(?) as suma ", [$request->get('origin_till_id')]))->first();
    if ($dato != null) {
      if ($request->get('tilltrans_amount') <= $dato->suma) {
        $transfer = new TillTransfer([
          'origin_till_id' => $request->get('origin_till_id'),
          'destiny_till_id' => $request->get('destiny_till_id'),
          'tilltrans_amount' => $request->get('tilltrans_amount'),
          'tilltrans_observation' => $request->get('tilltrans_observation'),
          'tilltrans_date' => $today,
        ]);
        $saved = $transfer->save();
        if ($saved) {
          $origin = new TillDetail([
            'till_id' => $transfer->origin_till_id,
            'factura_id' => 0,
            'description' => 'Transferencia de caja',
            'date' => $today,
            'amount' => $transfer->tilltrans_amount,
            'ref_id' => $transfer->tilltrans_id,
            'pay_type_id' => 1,
            'dettills_type' => false,
            'person_id' => auth()->user()->person_id,

          ]);
          $origin->save();

          $pay_type = ProofPayment::where('pay_type_id', 1)->first();

          $proof_origin = new DettillProofPayment();
          $proof_origin->dettills_id = $origin['dettills_id'];
          $proof_origin->pr_payment_id = $pay_type['pr_payment_id'];
          $proof_origin->dettills_pr_pay_desc = 'Ninguno';
          $proof_origin->save();

          $destiny = new TillDetail([
            'till_id' => $transfer->destiny_till_id,
            'factura_id' => 0,
            'description' => 'Transferencia de caja',
            'date' => $today,
            'pay_type_id' => 1,
            'amount' => $transfer->tilltrans_amount,
            'ref_id' => $transfer->tilltrans_id,
            'dettills_type' => true,
            'person_id' => auth()->user()->person_id,
          ]);
          $destiny->save();
          $proof_destiny = new DettillProofPayment();
          $proof_destiny->dettills_id = $destiny['dettills_id'];
          $proof_destiny->pr_payment_id = $pay_type['pr_payment_id'];
          $proof_destiny->dettills_pr_pay_desc = 'Ninguno';
          $proof_destiny->save();
        }
        return response()->json([
          'message' => 'Transferencia de Caja se registro con exito!',
          'dato' => true
        ]);
      } else {
        return response()->json([
          'message' => 'No hay suficiente saldo en caja',
          'dato' => false
        ]);
      }
    } else {
      return response()->json([
        'message' => 'No hay saldo en caja',
        'dato' => false
      ]);
    }
  }

  public function show($id)
  {
    //$dato = TillTransfer::find( $id);
    $dato = TillTransfer::join('tills as o', 'o.till_id', '=', 'till_transfers.origin_till_id')
      ->join('tills as d', 'd.till_id', '=', 'till_transfers.destiny_till_id')
      ->select('till_transfers.*', 'o.till_name As origin_name', 'd.till_name As destiny_name')
      ->where('till_transfers.tilltrans_id', '=', $id)
      ->first();
    return $this->showOne($dato, 200);
  }

  public function showTills(Request $request, $id)
  {
    if ($request->filter == 'false') {
      $datos = TillTransfer::join('tills as o', 'o.till_id', '=', 'till_transfers.origin_till_id')
        ->join('tills as d', 'd.till_id', '=', 'till_transfers.destiny_till_id')
        ->select('till_transfers.*', 'o.till_name As origin_name', 'd.till_name As destiny_name')
        ->where('till_transfers.origin_till_id', '=', $id)
        ->orWhere('till_transfers.destiny_till_id', '=', $id)
        ->get();
    } else {
      $year = date("Y");
      $month = date("m");
      $day = date("d");
      $fechas = $year . "-" . $month . "-" . ($day - 7);
      $datos = TillTransfer::join('tills as o', 'o.till_id', '=', 'till_transfers.origin_till_id')
        ->join('tills as d', 'd.till_id', '=', 'till_transfers.destiny_till_id')
        ->select('till_transfers.*', 'o.till_name As origin_name', 'd.till_name As destiny_name')
        // ->where('till_transfers.origin_till_id', '=', $id)
        ->where(function ($query) use ($id) {
          $query->where('till_transfers.origin_till_id', '=', $id)
            ->orWhere('till_transfers.destiny_till_id', '=', $id);
        })
        // ->orWhere('till_transfers.destiny_till_id', '=', $id)
        ->whereBetween('till_transfers.tilltrans_date', [$request->from_date, $request->to_date])
        ->get();
      // dd($datos);
    }
    return $this->showAll($datos, 200);
  }


  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = TillTransfer::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Transferencia de caja se restauro con exito!');
    } else {
      $dato = TillTransfer::findOrFail($id);
      $request->validate([
        'origin_till_id' => 'required',
        'destiny_till_id' => 'required',
        'tilltrans_date' => 'required',
        'tilltrans_amount' => 'required',
      ]);
      $dato->origin_till_id = $request->origin_till_id;
      $dato->destiny_till_id = $request->destiny_till_id;
      $dato->tilltrans_date = $request->tilltrans_date;
      $dato->tilltrans_amount = $request->tilltrans_amount;
      $dato->tilltrans_observation = $request->tilltrans_observation;

      $dato->save();
      return response()->json([
        'message' => 'Transferencia de caja se actualizo con exito!',
        'dato' => $dato
      ]);
    }
  }

  public function destroy($id)
  {
    $dato = TillTransfer::findOrFail($id);
    $dato->delete();
    return response()->json([
      'message' => 'Caja se elimino con exito!'
    ]);
  }
}
