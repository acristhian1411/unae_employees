<?php

namespace App\Http\Controllers\IvaType;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\IvaType;

class IvaTypeController extends ApiController
{
  public function index()
  {
    $t = IvaType::query()->first();
    $query = IvaType::query();
    $query = $this->filterData($query, $t);
    $datos = $query->get();
      return $this->showAll($datos,200);
  }

  public function store(Request $request)
  {
        $request->validate([
        'ivatype_description' => 'required',
        'ivatype_valor' => 'required',
    ]);
   $datos = new IvaType($request->all());
   $datos->save();
    return response()->json('Tipo de IVA se agrego correctamente!');
  }

  public function show($id)
  {
    //$dato = IvaType::find( $id);
    $dato = IvaType::where( 'ivatype_id', '=', $id)
                              ->first();
    return $this->showOne($dato,200);
  }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = IvaType::find($id);
      $keys = collect($request->all())->keys();
      for ($i=0; $i < count($keys); $i++) {
        $dato[$keys[0]]= $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Tipo de IVA se restauro con exito!');
    }
    else{
    $dato = IvaType::findOrFail( $id);
          $request->validate([
            'ivatype_description' => 'required',
            'ivatype_valor' => 'required',
          ]);
          $dato->ivatype_description = $request->ivatype_description;
          $dato->ivatype_valor = $request->ivatype_valor;
          $dato->save();
          return response()->json([
              'message' => 'Tipo de IVA se actualizo correctamente!',
              'dato' => $dato
          ]);
      }
    }

      public function destroy($id)
      {
          $dato = IvaType::findOrFail($id);
          $dato->delete();
          return response()->json([
              'message' => 'Tipo de IVA se elimino correctamente!'
          ]);
        }
}
