<?php

namespace App\Http\Controllers\Tickets;

// use Illuminate\Support\Facades\DB;
/**
 * Se crea una clase aparte para tener mejor orden y entendimiento
 */
class TicketsAdvancesFilters
{
  /*
  Filtros para la busqueda avanzada
  Retorna una cadena de caracteres con los filtros de busqueda para poder concatenar con la peticion sql
  */
  public function applyFilter($filters)
  {
    $string_where = "";

    // dd($filters);
    if (isset($filters["person_id"])  && $filters["person_id"] !== null && $filters["person_id"] !== 'null') {
      if ($string_where !== "") {
        $string_where = $string_where . " and t.person_id = '" . '' . $filters["person_id"] . "'";
      } else {
        $string_where = "t.person_id = '" .  $filters["person_id"] . "'";
      }
    }
    if (isset($filters["ticket_date"]) && $filters["ticket_date"] !== null && $filters["ticket_date"] !== 'null') {
      if ($string_where !== "") {
        $string_where = $string_where . " and ticket_date = '" . '' . $filters["ticket_date"] . "'";
      } else {
        $string_where = "t.ticket_date = '" .  $filters["ticket_date"] . "'";
      }
    }
    if (isset($filters["ticket_number"]) && $filters["ticket_number"] !== null && $filters["ticket_number"] !== 'null') {
      if ($string_where !== "") {
        $string_where = $string_where . " and ticket_number ilike " . '$$%' . $filters["ticket_number"] . '%$$';
      } else {
        $string_where = "t.ticket_number ilike " . '$$%' . $filters["ticket_number"] . '%$$';
      }
    }
    if (isset($filters["ticket_status"]) && $filters["ticket_status"] !== null && $filters["ticket_status"] !== 'null') {
      if ($string_where !== "") {
        $string_where = $string_where . " and ticket_status ilike " . '$$%' . $filters["ticket_status"] . '%$$';
      } else {
        $string_where = "t.ticket_status ilike " . '$$%' . $filters["ticket_status"] . '%$$';
      }
    }
    if (isset($filters["facu_id"]) && $filters["facu_id"] !== null && $filters["facu_id"] !== 'null') {
      if ($string_where !== "") {
        $string_where = $string_where . " and (select get_faculty_id_from_ticket(t.ticket_id))=" . '$$' . $filters["facu_id"] . '$$';
      } else {
        $string_where = "and (select get_faculty_id_from_ticket(t.ticket_id))=" . '$$' . $filters["facu_id"] . '$$';
      }
    }

    if (isset($filters["pay_type_id"]) == true && $filters["pay_type_id"] !== null && $filters["pay_type_id"] !== 'null') {
      if ($string_where !== "") {
        $string_where = $string_where . " and pp.pay_type_id in( "  . $filters["pay_type_id"] . ')';
      } else {
        $string_where = "and pp.pay_type_id in( "  . $filters["pay_type_id"] . ')';
      }
    }
    if (isset($filters["description"]) == true && $filters["description"] !== null && $filters["description"] !== 'null') {
      if ($string_where !== "") {
        $string_where = $string_where . " and td.tickdet_desciption ilike $$%"  . $filters["description"] . '%$$';
      } else {
        $string_where = "and td.tickdet_desciption ilike $$%"  . $filters["description"] . '%$$';
      }
    }
    return ($string_where);
    // return array_key_exists("person_fname", $filters);
  }
}
