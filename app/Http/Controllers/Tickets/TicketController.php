<?php

namespace App\Http\Controllers\Tickets;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ApiDBController;
use App\Ticket;
use App\TicketDetail;
use App\TillDetail;
use App\StudentAccount;
use App\EvalStudent;
use App\Enrolled;
use App\Http\Controllers\TillDetails\TillDetailsController;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Tickets\TicketsAdvancesFilters;

class TicketController extends ApiDBController
{
  public function index()
  {
    $count = DB::select("select count(t.ticket_id) from tickets t
        join persons p on p.person_id = t.person_id where t.deleted_at is null");
    $datos = "select distinct(t.ticket_id), t.*,
      concat(p.person_fname, ' ', p.person_lastname) as person_name
      from tickets t join persons p on p.person_id = t.person_id where t.deleted_at is null";
    $return_response = $this->showAll($datos, $count, 200);
    $response = json_decode(json_encode($return_response))->original;
    $data_len = count($response->data);
    for ($i = 0; $i < $data_len; $i++) {
      $sum = DB::select("select sum(td.tickdet_monto) as total
        from ticket_details td
        where td.ticket_id = " . $response->data[$i]->ticket_id .
        " and td.tickdet_desciption not ilike '%Descuento%' limit 1");
      $res = DB::select("select sum(td.tickdet_monto) as total
        from ticket_details td
        where td.ticket_id = " . $response->data[$i]->ticket_id .
        " and td.tickdet_desciption ilike '%Descuento%' limit 1");
      $details = DB::select("select * from ticket_details td where ticket_id = " . $response->data[$i]->ticket_id);
      $response->data[$i]->ticket_details = $details;
      $response->data[$i]->total = $sum[0]->total - $res[0]->total;
      // $response->data[$i]->sub_products = json_decode($response->data[$i]->sub_products, true);
    }
    return response()->json($response);
    // $datos = Ticket::where('tickets.deleted_at', '=', null)
    //   ->join('persons', 'persons.person_id', '=', 'tickets.person_id')
    //   ->select(
    //     'tickets.*',
    //     DB::raw("CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS person_name")
    //   )
    //   ->distinct('tickets.ticket_id')
    //   ->get();
    // for ($i = 0; $i < count($datos); $i++) {
    //   $sum = TicketDetail::select(DB::raw('sum(tickdet_monto) as total'))
    //     ->where([['ticket_details.ticket_id', '=', $datos[$i]->ticket_id], ['ticket_details.tickdet_desciption', 'not ilike', '%Descuento%']])
    //     ->first();
    //   $res = TicketDetail::select(DB::raw('sum(tickdet_monto) as total'))
    //     ->where([['ticket_details.ticket_id', '=', $datos[$i]->ticket_id], ['ticket_details.tickdet_desciption', 'ilike', '%Descuento%']])
    //     ->first();
    //   $datos[$i]->total = $sum->total - $res->total;
    // }
    // return $this->showAll($datos, 200);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function listTicketDetails(Request $request)
  {
    // dd($request);
    if (!$request->sort_by) {
      $sort_by = 'ticket_date';
      $order = 'desc';
      $request->sort_by = 'ticket_date';
      $request->order = 'desc';
    } else {
      $sort_by = $request->sort_by;
      $order = $request->order;
    }
    $totales = [];
    $cuotas = 0;
    $moras = 0;
    $derechos = 0;
    $gestion_titulo = 0;
    $certificado_estudio = 0;
    $defensa_mesa = 0;
    $productos = 0;
    $gestiones = 0;
    $otros = 0;
    // dd($request);
    if ($request->from == 'frontend' && (!isset($request->facu_id) and !isset($request->pay_type_id) and !isset($request->description) and !isset($request->person_id))) {
      // dd($request);

      $datos = "select distinct on(td.tickdet_id) td.*, t.*, cd.comp_det_bussiness_name comp_name,
      concat(p.person_fname, ' ', p.person_lastname) as person_name, p.person_idnumber,
      concat(p2.person_fname, ' ', p2.person_lastname) as cajero_name, p2.person_idnumber,
    pt.pay_type_desc, t2.till_name
    from tickets t 
    join ticket_details td on t.ticket_id = td.ticket_id 
    join tills_details td2 on td2.ref_id = t.ticket_id
    join tills t2 on t2.till_id = td2.till_id
    join dettill_proofpayment dp on dp.dettills_id = td2.dettills_id 
    join proof_payment pp on pp.pr_payment_id = dp.pr_payment_id 
    join payment_type pt on pt.pay_type_id = pp.pay_type_id 
    join only persons p on p.person_id = t.person_id 
    join only persons p2 on p2.person_id = td2.person_id 
    join company_details cd on cd.comp_det_id = t.comp_det_id     
    where t.created_at::date between '" . $request->from_date . "' and '" . $request->to_date . "'
              and t.deleted_at is null 
              and t.ticket_status != 'Anulado' and t.ticket_status != 'Rechazado'
    and td.tickdet_desciption not ilike '%descuento%'";
      $count = DB::select("select count(distinct(td.tickdet_id))
    from tickets t 
    join ticket_details td on t.ticket_id = td.ticket_id 
    join tills_details td2 on td2.ref_id = t.ticket_id
    join dettill_proofpayment dp on dp.dettills_id = td2.dettills_id 
    join proof_payment pp on pp.pr_payment_id = dp.pr_payment_id 
    join payment_type pt on pt.pay_type_id = pp.pay_type_id 
    join only persons p on p.person_id = t.person_id 
    join company_details cd on cd.comp_det_id = t.comp_det_id 
    where t.created_at::date between '" . $request->from_date . "' and '" . $request->to_date . "'
              and t.deleted_at is null 
              and t.ticket_status != 'Anulado' and t.ticket_status != 'Rechazado'
    and td.tickdet_desciption not ilike '%descuento%'");
    } else {
      // dd("else");
      $payment = 'null';
      if (isset($request->pay_type_id)) {
        if ($request->from == 'frontend') {
          $payment = implode(",", $request->pay_type_id);
        } else {
          $payment =  $request->pay_type_id;
        }
      }
      $filters = new TicketsAdvancesFilters;
      $req_filters = [
        'facu_id' => $request->facu_id, 'pay_type_id' => $payment, 'description' => $request->description, 'person_id' => $request->person_id
      ];
      $applied_filter = $filters->applyFilter($req_filters);
      $datos = "
      select distinct on(td.tickdet_id) td.*, t.*, cd.comp_det_bussiness_name comp_name,
      concat(p.person_fname, ' ', p.person_lastname) as person_name, p.person_idnumber,
      t.created_by as cajero_name,
      pt.pay_type_desc, t2.till_name
      from ticket_details td
      left join tickets t on t.ticket_id = td.ticket_id
      left join tills_details td2 on td2.ref_id = t.ticket_id
      left join tills t2 on t2.till_id = td2.till_id
      left join dettill_proofpayment dp on dp.dettills_id = td2.dettills_id 
      left join proof_payment pp on pp.pr_payment_id = dp.pr_payment_id 
      left join payment_type pt on pt.pay_type_id = pp.pay_type_id 
      left join only persons p on p.person_id = t.person_id
      left join company_details cd on cd.comp_det_id = t.comp_det_id
              where
               t.deleted_at is null and
               td.deleted_at is null and
              t.created_at::date between '" . $request->from_date . "' and '" . $request->to_date . "'
              and td.tickdet_desciption not ilike '%descuento%' 
              " . $applied_filter . "";
      // dd($datos);
      $count = DB::select("select count(distinct(td.tickdet_id))
              from ticket_details td
              join tickets t on t.ticket_id = td.ticket_id
              join student_account sa on td.tickdet_ref = sa.staccount_id
              join tills_details td2 on td2.ref_id = t.ticket_id
              join tills t2 on t2.till_id = td2.till_id
              join dettill_proofpayment dp on dp.dettills_id = td2.dettills_id 
              join proof_payment pp on pp.pr_payment_id = dp.pr_payment_id 
              join payment_type pt on pt.pay_type_id = pp.pay_type_id 
              join only persons p on p.person_id = t.person_id
              join company_details cd on cd.comp_det_id = t.comp_det_id
              where
              t.deleted_at is null and
             t.created_at::date between '" . $request->from_date . "' and '" . $request->to_date . "'
             and td.tickdet_desciption not ilike '%descuento%'
             " . $applied_filter . "
             ");
    }



    // $datos = TicketDetail::join('tickets', 'tickets.ticket_id', '=', 'ticket_details.ticket_id')
    //   ->join('persons', 'persons.person_id', '=', 'tickets.person_id')
    //   ->join('company_details', 'company_details.comp_det_id', '=', 'tickets.comp_det_id')
    //   ->select(
    //     'ticket_details.*',
    //     'tickets.*',
    //     'company_details.comp_det_bussiness_name as comp_name',
    //     'persons.person_fname',
    //     'persons.person_idnumber',
    //     DB::raw("get_faculty_from_ticket(tickets.ticket_id) as unit_faculty")
    //   )
    //   ->distinct()
    //   ->orderBy($sort_by, $order)
    //   ->whereBetween('tickets.ticket_date', [$request->from_date, $request->to_date])
    //   ->get();

    if ($request->from == 'frontend') {
      return $this->showAll($datos, $count, 200);
    } elseif ($request->from == 'frontend_balance') {
      $query = $datos;
      // dd($datos);
      $datos = DB::select($query);

      $array_gestiones = [];
      //dd($datos);
      for ($i = 0; $i < count($datos); $i++) {
        if (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == false) {
          $cuotas = $cuotas + $datos[$i]->tickdet_monto;
        } elseif (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == true) {
          $moras = $moras + $datos[$i]->tickdet_monto;
        } elseif (in_array($datos[$i]->tdtype_id, [2, 3, 4, 5, 6, 7, 8, 9, 15]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'oportunidad') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'prueba parcial') == true))) {
          $derechos = $derechos + $datos[$i]->tickdet_monto;
        } elseif (in_array($datos[$i]->tdtype_id, [10]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion de titulo') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == true))) {
          $gestion_titulo = $gestion_titulo + $datos[$i]->tickdet_monto;
        } elseif (in_array($datos[$i]->tdtype_id, [11, 16]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'certificado') == true))) {
          $certificado_estudio = $certificado_estudio + $datos[$i]->tickdet_monto;
        } elseif (in_array($datos[$i]->tdtype_id, [12, 13, 14]) || (in_array($datos[$i]->tdtype_id, [19]) && strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == true)) {
          $defensa_mesa = $defensa_mesa + $datos[$i]->tickdet_monto;
        } elseif (in_array($datos[$i]->tdtype_id, [18])) {
          $productos = $productos + $datos[$i]->tickdet_monto;
        } elseif (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion de titulo') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == false)) {
          // dd($datos[$i]);
          $gestiones_query = DB::select("select * from operations o where o.oper_description = '" . $datos[$i]->tickdet_desciption . "' limit 1");
          if (count($gestiones_query) === 0) {
            if (!array_key_exists(mb_strtolower(trim($datos[$i]->tickdet_desciption)), $array_gestiones)) {
              $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $datos[$i]->tickdet_monto;
            } else {
              $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] +  $datos[$i]->tickdet_monto;
            }
          } else {
            if (!array_key_exists(mb_strtolower(trim($gestiones_query[0]->oper_description)), $array_gestiones)) {
              $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $datos[$i]->tickdet_monto;
            } else {
              $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] +  $datos[$i]->tickdet_monto;
            }
          }



          // dd($array_gestiones);
          // $gestiones = $gestiones + $datos[$i]->tickdet_monto;
        } else {
          // dd($datos[$i]);
          $otros = $otros + $datos[$i]->tickdet_monto;
        }
      }
      $totales = [
        'cuotas' => $cuotas,
        'moras' => $moras,
        'derechos' => $derechos,
        'gestion de titulo' => $gestion_titulo,
        'certificados de estudio' => $certificado_estudio,
        'defensa de mesas' => $defensa_mesa,
        'productos' => $productos,
        'gestiones' => $gestiones,
        'otros' => $otros,
      ];
      $totales = array_merge($totales, $array_gestiones);

      arsort($totales);
      // dd($totales);
      return $totales;
    } else {
      // dd($datos);
      $query = $datos;
      //faltaria agregar un limite de todo
      $offset = 0;
      if (isset($request->index)) {
        // dd("$request->index = true");
        if ($request->index == 0) {
          $datos = DB::select($query);
          $len_j = count($datos);
          $array_gestiones = [];
          //dd($datos);
          for ($i = 0; $i < count($datos); $i++) {
            if (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == false) {
              $cuotas = $cuotas + $datos[$i]->tickdet_monto;
            } elseif (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == true) {
              $moras = $moras + $datos[$i]->tickdet_monto;
            } elseif (in_array($datos[$i]->tdtype_id, [2, 3, 4, 5, 6, 7, 8, 9, 15]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'oportunidad') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'prueba parcial') == true))) {
              $derechos = $derechos + $datos[$i]->tickdet_monto;
            } elseif (in_array($datos[$i]->tdtype_id, [10]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion de titulo') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == true))) {
              $gestion_titulo = $gestion_titulo + $datos[$i]->tickdet_monto;
            } elseif (in_array($datos[$i]->tdtype_id, [11, 16]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'certificado') == true))) {
              $certificado_estudio = $certificado_estudio + $datos[$i]->tickdet_monto;
            } elseif (in_array($datos[$i]->tdtype_id, [12, 13, 14]) || (in_array($datos[$i]->tdtype_id, [19]) && strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == true)) {
              $defensa_mesa = $defensa_mesa + $datos[$i]->tickdet_monto;
            } elseif (in_array($datos[$i]->tdtype_id, [18])) {
              $productos = $productos + $datos[$i]->tickdet_monto;
            } elseif (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion de titulo') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == false)) {
              // dd($datos[$i]);
              $gestiones_query = DB::select("select * from operations o where o.oper_description = '" . $datos[$i]->tickdet_desciption . "' limit 1");
              if (count($gestiones_query) === 0) {
                if (!array_key_exists(mb_strtolower(trim($datos[$i]->tickdet_desciption)), $array_gestiones)) {
                  $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $datos[$i]->tickdet_monto;
                } else {
                  $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] +  $datos[$i]->tickdet_monto;
                }
              } else {
                if (!array_key_exists(mb_strtolower(trim($gestiones_query[0]->oper_description)), $array_gestiones)) {
                  $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $datos[$i]->tickdet_monto;
                } else {
                  $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] +  $datos[$i]->tickdet_monto;
                }
              }



              // dd($array_gestiones);
              // $gestiones = $gestiones + $datos[$i]->tickdet_monto;
            } else {
              // dd($datos[$i]);
              $otros = $otros + $datos[$i]->tickdet_monto;
            }
          }
          $totales = [
            'cuotas' => $cuotas,
            'moras' => $moras,
            'derechos' => $derechos,
            'gestion de titulo' => $gestion_titulo,
            'certificados de estudio' => $certificado_estudio,
            'defensa de mesas' => $defensa_mesa,
            'productos' => $productos,
            'gestiones' => $gestiones,
            'otros' => $otros,
          ];
          $totales = array_merge($totales, $array_gestiones);

          arsort($totales);

          // dd(['datos' => $pag_res, 'totales' => $totales]);
          return [
            // 'datos' => $pag_res,
            'totales' => $totales
          ];
        } else {
          $offset = 500 * ($request->index - 1);
          $pag_res = DB::select($query . " limit 500 offset " . $offset);

          return [
            'datos' => $pag_res,
            'part' => $request->index
            // 'totales' => $totales
          ];
        }

        // $datos = $pag_res;
        // $len_j = count($datos);
        // for ($j = 0; $j < $len_j; $j++) {
        //   if (strstr($datos[$j]->tickdet_desciption, 'Cuota') && strstr($datos[$j]->tickdet_desciption, 'Mora') == false) {
        //     $cuotas = $cuotas + $datos[$j]->tickdet_monto;
        //   } elseif (strstr($datos[$j]->tickdet_desciption, 'Mora')) {
        //     $moras = $moras + $datos[$j]->tickdet_monto;
        //   } elseif (strstr($datos[$j]->tickdet_desciption, 'OPORTUNIDAD')) {
        //     $derechos = $derechos + $datos[$j]->tickdet_monto;
        //   } else {
        //     $aranceles = $aranceles + $datos[$j]->tickdet_monto;
        //   }
        // }
        // dd($pag_res);
      } else {
        $datos = DB::select($query);
        $array_gestiones = [];
        //dd($datos);
        for ($i = 0; $i < count($datos); $i++) {
          if (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == false) {
            $cuotas = $cuotas + $datos[$i]->tickdet_monto;
          } elseif (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == true) {
            $moras = $moras + $datos[$i]->tickdet_monto;
          } elseif (in_array($datos[$i]->tdtype_id, [2, 3, 4, 5, 6, 7, 8, 9, 15]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'oportunidad') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'prueba parcial') == true))) {
            $derechos = $derechos + $datos[$i]->tickdet_monto;
          } elseif (in_array($datos[$i]->tdtype_id, [10]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion de titulo') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == true))) {
            $gestion_titulo = $gestion_titulo + $datos[$i]->tickdet_monto;
          } elseif (in_array($datos[$i]->tdtype_id, [11, 16]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'certificado') == true))) {
            $certificado_estudio = $certificado_estudio + $datos[$i]->tickdet_monto;
          } elseif (in_array($datos[$i]->tdtype_id, [12, 13, 14]) || (in_array($datos[$i]->tdtype_id, [19]) && strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == true)) {
            $defensa_mesa = $defensa_mesa + $datos[$i]->tickdet_monto;
          } elseif (in_array($datos[$i]->tdtype_id, [18])) {
            $productos = $productos + $datos[$i]->tickdet_monto;
          } elseif (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion de titulo') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == false)) {
            // dd($datos[$i]);
            $gestiones_query = DB::select("select * from operations o where o.oper_description = '" . $datos[$i]->tickdet_desciption . "' limit 1");
            if (count($gestiones_query) === 0) {
              if (!array_key_exists(mb_strtolower(trim($datos[$i]->tickdet_desciption)), $array_gestiones)) {
                $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $datos[$i]->tickdet_monto;
              } else {
                $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] +  $datos[$i]->tickdet_monto;
              }
            } else {
              if (!array_key_exists(mb_strtolower(trim($gestiones_query[0]->oper_description)), $array_gestiones)) {
                $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $datos[$i]->tickdet_monto;
              } else {
                $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] +  $datos[$i]->tickdet_monto;
              }
            }



            // dd($array_gestiones);
            // $gestiones = $gestiones + $datos[$i]->tickdet_monto;
          } else {
            // dd($datos[$i]);
            $otros = $otros + $datos[$i]->tickdet_monto;
          }
        }
        $totales = [
          'cuotas' => $cuotas,
          'moras' => $moras,
          'derechos' => $derechos,
          'gestion de titulo' => $gestion_titulo,
          'certificados de estudio' => $certificado_estudio,
          'defensa de mesas' => $defensa_mesa,
          'productos' => $productos,
          'gestiones' => $gestiones,
          'otros' => $otros,
        ];
        $totales = array_merge($totales, $array_gestiones);

        arsort($totales);

        // dd(['datos' => $pag_res, 'totales' => $totales]);
        return [
          'datos' => $datos,
          'totales' => $totales
        ];
      }
      // $len_h = intval(($count[0]->count / 1000)+1);
      //
      // $pag_res = [];
      // // dd($len_h);
      // for ($h=0; $h < $len_h; $h++) {
      //   $array_temp = DB::select($query." limit 1000 offset ".$offset);
      //   $pag_res[] = $array_temp;
      //   $offset = $offset+count($array_temp);
      // }
      // $len_i = count($pag_res);
      // // dd($offset);
      // $cuotas = 0;
      // $moras = 0;
      // $derechos = 0;
      // $aranceles = 0;
      // for ($i=0; $i < $len_i; $i++) {
      //   $datos = $pag_res[$i];
      //   $len_j = count($datos);
      //   for ($j = 0; $j < $len_j; $j++) {
      //     if (strstr($datos[$j]->tickdet_desciption, 'Cuota') && strstr($datos[$j]->tickdet_desciption, 'Mora') == false) {
      //       $cuotas = $cuotas + $datos[$j]->tickdet_monto;
      //     } elseif (strstr($datos[$j]->tickdet_desciption, 'Mora')) {
      //       $moras = $moras + $datos[$j]->tickdet_monto;
      //     } elseif (strstr($datos[$j]->tickdet_desciption, 'OPORTUNIDAD')) {
      //       $derechos = $derechos + $datos[$j]->tickdet_monto;
      //     } else {
      //       $aranceles = $aranceles + $datos[$j]->tickdet_monto;
      //     }
      //   }
      // }
      // $datos = DB::select($query." limit ");
      // $count[0]->count
      // $len_j = count($datos);

      // // dd($datos);
      // $totales = [
      //   'cuotas' => $cuotas,
      //   'moras' => $moras,
      //   'derechos' => $derechos,
      //   'aranceles' => $aranceles,
      // ];
      // dd(['datos' => $pag_res, 'totales' => $totales]);

    }

    // return $this->showAll($datos,200);
    return response()->json($datos);
  }
  public function getTicket(Request $request)
  {
    // $datos = 'esto es una factura';
    // $datos = TicketDetail::join('tickets', 'tickets.ticket_id', '=', 'ticket_details.ticket_id')
    //   ->join('persons', 'persons.person_id', '=', 'tickets.person_id')
    //   ->join('company_details', 'company_details.comp_det_id', '=', 'tickets.comp_det_id')
    //   ->select(
    //     'ticket_details.*',
    //     'tickets.*',
    //     'company_details.comp_det_bussiness_name as comp_name',
    //     'persons.person_fname',
    //     'persons.person_idnumber',
    //     DB::raw("get_faculty_from_ticket(tickets.ticket_id) as unit_faculty")
    //   )
    //   ->distinct()
    //   ->where('tickets.ticket_id', '=', $request->id)
    //   ->get();
    $ticket = DB::select("select t.*, concat(p.person_lastname,' ', p.person_fname) as person_name,
    p.person_idnumber, p.person_ruc, p.person_business_name
    from tickets t join persons p on p.person_id = t.person_id
    where ticket_id = " . $request->id . " limit 1
     ");

    $ticketDetails = DB::select("Select td.*, tdp.tdtype_desc from ticket_details td join ticket_details_types tdp on td.tdtype_id = tdp.tdtype_id
      where td.ticket_id = " . $request->id . " and td.tickdet_desciption not ilike '%descuento%' and (td.is_references is false or td.is_references is null)
      order by td.tickdet_ref asc, td.tickdet_desciption asc
      ");

    $semester = DB::select("select case 
		when s.sems_name is not null then s.sems_name 
    when s3.sems_name is not null then s3.sems_name
		when se.subeval_name is not null and td.tdtype_id = 1 then concat(se.subeval_name,' ',s2.subj_name)
		when o.oper_description is not null then o.oper_description 
		when p.product_desc is not null then p.product_desc 
	end as sems_name,
  s.sems_name as s_name,
	sa.oper_id,
	sa.staccount_description,
	e.enroll_oper_type,
	td.* from tickets t join ticket_details td on td.ticket_id = t.ticket_id 
      left join student_account sa on sa.staccount_id = td.tickdet_ref and td.tdtype_id = 1
      left join enrolleds e on (e.enroll_id = sa.enroll_id) or (td.tdtype_id = 4 and e.enroll_id = td.tickdet_ref)
      left join semesters s on s.sems_id = e.sems_id and e.enroll_oper_type is null
      left join operations o on o.oper_id = sa.oper_id and e.enroll_oper_type = 'operation'
      left join product p on p.product_id = sa.oper_id and e.enroll_oper_type = 'product'
      left join eval_students es on (es.evstu_id = sa.oper_id and e.enroll_oper_type = 'exam') or (es.evstu_id = td.tickdet_ref and td.tdtype_id = 6)
      left join carsub_enrolled ce on (ce.carsub_en_id = es.carsub_en_id) 
      left join enrolleds en on en.enroll_id = ce.enroll_id
      left join semesters s3 on s3.sems_id = en.sems_id
      left join subject_evaluation se on se.subeval_id = es.subeval_id 
      left join careers_subjets cs on  cs.carsubj_id = ce.carsubj_id and cs.carsubj_id = se.carsubj_id
      left join subjects s2 on s2.subj_id = cs.materia_id 
    where t.ticket_id = " . $request->id . "
      limit 1");

    $sum = DB::select("select sum(tickdet_monto*tickdet_qty) as total from ticket_details td
      where td.ticket_id = " . $request->id . " and td.tickdet_desciption not ilike '%Descuento%' and (td.is_references is false or td.is_references is null)
      limit 1");

    $ex = DB::select("select sum(tickdet_monto*tickdet_qty) as total from ticket_details td
      where td.ticket_id = " . $request->id . " and td.tickdet_iva in(0,3) 
      and td.tickdet_desciption not ilike '%Descuento%' and (td.is_references is false or td.is_references is null)
      limit 1");
    $cinco = DB::select("select sum(tickdet_monto*tickdet_qty) as total from ticket_details td
      where td.ticket_id = " . $request->id . " and td.tickdet_iva = 5
      and td.tickdet_desciption not ilike '%Descuento%' and (td.is_references is false or td.is_references is null)
      limit 1");

    $diez = DB::select("select sum(tickdet_monto*tickdet_qty) as total from ticket_details td
      where td.ticket_id = " . $request->id . " and td.tickdet_iva = 10
      and td.tickdet_desciption not ilike '%Descuento%' and (td.is_references is false or td.is_references is null)
      limit 1");
    if ($semester == null) {
      $ticket[0]['semester'] = " ";
    } else {
      $ticket[0]->semester = $semester[0]->sems_name;
    }
    $ticket[0]->ticketDetails = $ticketDetails;
    $ticket[0]->total = $sum[0]->total;
    $ticket[0]->exentas = $ex[0]->total;
    $ticket[0]->cinco = $cinco[0]->total;
    $ticket[0]->diez = $diez[0]->total;

    // return $this->showOne($ticket, 200);

    return ['datos' => $ticket[0]];
  }
  public function getMonthFromExpiration($date)
  {
    // dd($date);
    $m = date('m', strtotime($date));
    // dd(intval($m));
    $month = [];
    $month[1] = "Enero";
    $month[2] = "Febrero";
    $month[3] = "Marzo";
    $month[4] = "Abril";
    $month[5] = "Mayo";
    $month[6] = "Junio";
    $month[7] = "Julio";
    $month[8] = "Agosto";
    $month[9] = "Septiembre";
    $month[10] = "Octubre";
    $month[11] = "Noviembre";
    $month[12] = "Diciembre";

    return $month[intval($m)];
  }
  public function store($request, $ticket, $company, $detailsMethod, $proofPayments, $list_discount, $ticket_date, $can_modify_date, $ticket_type, $cr_by = "", $ticket_pending = false)
  {
    // dump($detailsMethod);
    // dump($list_discount);
    // dd($proofPayments);

    // dd($request[0]['person_id']);
    $monto_movimiento = 0;
    $tilldet_message = "";
    $person_id = 0;

    if ($cr_by == "") {
      $created_by = auth()->user()->name;
    } else {
      $created_by = $cr_by;
    }

    $details['tdtype_id'] = 0;
    if ($request[0]['person_id'] == null) {
      $person = DB::table('carsub_enrolled')
        ->join('enrolleds', 'enrolleds.enroll_id', 'carsub_enrolled.enroll_id')
        ->where([['carsub_enrolled.carsub_en_id', '=', $request[0]['carsub_en_id']]])
        ->select('enrolleds.person_id')
        ->first();
      $person_id = $person->person_id;
    } else {
      $person_id = $request[0]['person_id'];
    }
    $type = true;
    if ($ticket_type == 'Factura') {
      $type = true;
    } else {
      $type = false;
    }
    $dato = new Ticket([
      'person_id' => $person_id,
      'ticket_date' => $ticket_date,
      'ticket_number' => $ticket,
      'comp_det_id' => $company,
      'ticket_is_invoice' => $type,
      'created_by' => $created_by,
      'updated_at' => \Carbon::now()
    ]);
    if ($ticket_pending) {
      $dato->ticket_status = "Pendiente";
    }
    $dato->save();

    try {

      for ($i = 0; $i < count($request); $i++) {
        $details['ticket_id'] = $dato->ticket_id;
        if (array_key_exists("operations", json_decode(json_encode($request[$i]), true))) {
          $ar_length = count($request[$i]['operations']);
          $detail_type = DB::table('ticket_details_types')->where('tdtype_desc', '=', 'Gestiones')
            ->select('tdtype_id')
            ->first();
          $details['tdtype_id'] = $detail_type->tdtype_id;
          for ($j = 0; $j < $ar_length; $j++) {
            $details['tickdet_monto'] = $request[$i]['operations'][$j]['oper_amount'];
            $details['tickdet_iva'] =  $request[$i]['operations'][$j]['ivatype_id'];
            $details['tickdet_desciption'] =  $request[$i]['operations'][$j]['oper_description'];
            $details['tickdet_qty'] = 1;
            $details['tickdet_ref'] = $request[$i]['operations'][$j]['oper_id'];
            $data = new TicketDetail($details);

            $data->save();
          }
          // dd($ar_length);
        } else {
          if (array_key_exists("products", json_decode(json_encode($request[$i]), true))) {
            $ar_length = count($request[$i]['products']);
            $detail_type = DB::table('ticket_details_types')->where('tdtype_desc', '=', 'Venta de Productos')
              ->select('tdtype_id')
              ->first();
            $details['tdtype_id'] = $detail_type->tdtype_id;
            for ($j = 0; $j < $ar_length; $j++) {
              $details['tickdet_monto'] = $request[$i]['products'][$j]['product_price'];
              $details['tickdet_iva'] =  $request[$i]['products'][$j]['ivatype_valor'];
              $details['tickdet_desciption'] =  $request[$i]['products'][$j]['product_desc'];
              if (array_key_exists("product_qty", $request[$i]['products'][$j])) {
                $details['tickdet_qty'] = $request[$i]['products'][$j]['product_qty'];
              } else {
                $details['tickdet_qty']  = 1;
              }

              $details['tickdet_ref'] = $request[$i]['products'][$j]['product_id'];
              $data = new TicketDetail($details);

              $data->save();
            }
            // dd($ar_length);
          } else {
            if ($request[$i]['oper_description'] == 'Prueba Parcial') {
              $detail_type = DB::table('ticket_details_types')->where('tdtype_desc', '=', 'Pruebas Parciales')
                ->select('tdtype_id')
                ->first();
              $details['tdtype_id'] = $detail_type->tdtype_id;
              $details['tickdet_monto'] = $request[$i]['oper_amount'];
              $details['tickdet_iva'] = $request[$i]['ivatype_valor'];
              $details['tickdet_qty'] = 1;
              // $details['tickdet_ref'] = $request[$i]['oper_id'];
              // $data = new TicketDetail($details);
              $subjects = DB::select("select s.subj_name from subjects s join careers_subjets cs on cs.materia_id = s.subj_id
            join carsub_enrolled ce on ce.carsubj_id = cs.carsubj_id
            where ce.enroll_id = " . $request[$i]['enroll_id'] . "
            ");
              $str_description = '';
              for ($k = 0; $k < count($subjects); $k++) {
                $str_description = $str_description . ", " . $subjects[$k]->subj_name;
              }
              $details['tickdet_desciption'] = $request[$i]['oper_description'] . " " . $str_description;
              // dd($details['tickdet_desciption']);

              $evstu = DB::select("select * from eval_students es
              join carsub_enrolled ce on ce.carsub_en_id = es.carsub_en_id
              join subject_evaluation se on se.subeval_id = es.subeval_id
              where ce.enroll_id = " . $request[$i]['enroll_id'] . " and se.subeval_name ilike '%parcial%'");

              $details['tickdet_ref'] = $request[$i]['enroll_id'];
              // dd($evstu);
              for ($j = 0; $j < count($evstu); $j++) {
                $eval = EvalStudent::find($evstu[$j]->evstu_id);
                $eval->evstu_paid = true;
                $eval->save();
              }
              $data = new TicketDetail($details);
              $data->save();
            } else {
              if ($request[$i]['staccount_amount_paid'] == null) {
                // dd('entra a staccount_amount_paid is null');

                // dd($request[$i]['carsub_en_id']);
                $data = DB::select("select ce.enroll_id, et.evaltype_name, se.*,get_exam_price(et.evaltype_name, s.sems_id, ce.enroll_id,se.subeval_id) as subeval_spprice from subject_evaluation se join eval_students es on es.subeval_id = se.subeval_id
                join carsub_enrolled ce on ce.carsub_en_id = es.carsub_en_id
                join evaluation_types et on et.evaltype_id = se.evaltype_id
                join careers_subjets cs on cs.carsubj_id = se.carsubj_id
                join subjects s on s.subj_id = cs.materia_id
                where ce.carsub_en_id = " . $request[$i]['carsub_en_id'] . " and se.subeval_id = " . $request[$i]['subeval_id'] . " limit 1");
                // dd($data[0]->subeval_spprice);
                // $data = DB::table('subject_evaluation')
                //   ->join('eval_students', 'eval_students.subeval_id', '=', 'subject_evaluation.subeval_id')
                //   ->join('carsub_enrolled', 'carsub_enrolled.carsub_en_id', '=', 'eval_students.carsub_en_id')
                //   ->join('evaluation_types', 'evaluation_types.evaltype_id', '=', 'subject_evaluation.evaltype_id')
                //   ->join('careers_subjets', 'careers_subjets.carsubj_id', '=', 'subject_evaluation.carsubj_id')
                //   ->join('subjects', 'subjects.subj_id', '=', 'careers_subjets.materia_id')
                //   ->where([['subeval_id', '=', $request[$i]['subeval_id']], ['carsub_enrolled.carsub_en_id', '=', $request[$i]['carsub_en_id']]])
                //   ->select(
                //     DB::raw('get_exam_price(evaluation_types.evaltype_name, subjects.sems_id, carsub_enrolled.enroll_id) as subeval_spprice'),
                //     'subject_evaluation.subeval_id',
                //     'subject_evaluation.subeval_name',
                //     'carsub_enrolled.enroll_id',
                //     'evaluation_types.evaltype_name'
                //   )
                //   ->distinct('subject_evaluation.subeval_id')
                //   ->first();
                // dd($data);
                $subeval_spprice = intval($data[0]->subeval_spprice);
                $detail_type = DB::table('ticket_details_types')->where('tdtype_desc', 'ilike', '%' . $data[0]->evaltype_name . '%')
                  ->select('tdtype_id')
                  ->first();
                // dd($detail_type);
                $details['tdtype_id'] = $detail_type->tdtype_id;
                $details['tickdet_monto'] = $subeval_spprice;
              } else {
                // dd("todavia sirve");
                // estira student_account existente y resta el new.staccount_amount_paid - old.staccount_amount_paid
                // $old_staccount = DB::table('student_account')
                //   ->where('staccount_id', '=', $request[$i]['staccount_id'])
                //   ->select('staccount_amount_paid', DB::raw("get_surcharge_from_date(staccount_id, '".$dato->ticket_date."')"), 'get_surcharge')
                //   ->first();
                $old_staccount = DB::select("select staccount_amount_paid,
                                            get_surcharge_from_date(staccount_id, '" . $dato->ticket_date . "')
                                            as get_surcharge, surcharge
                                            from student_account sa
                                            where staccount_id = " . $request[$i]['staccount_id'])[0];
                $detail_type = DB::table('ticket_details_types')->where('tdtype_desc', '=', 'Cuotas')
                  ->select('tdtype_id')
                  ->first();
                $details['tdtype_id'] = $detail_type->tdtype_id;
                $discount = json_decode(json_encode($list_discount), true);
                if ($old_staccount->get_surcharge > 0) {
                  // dump("linea 501");
                  // dump($request[$i]['surcharge']);
                  // dd("fin de  la funcion");
                  $details['tickdet_monto'] = $request[$i]['surcharge_paid'];
                  $details['tickdet_iva'] = 0;
                  $details['tickdet_desciption'] = "Mora por: {$request[$i]['staccount_description']}" . " - " . $this->getMonthFromExpiration($request[$i]['staccount_expiration']);
                  $details['tickdet_qty'] = 1;
                  $details['tickdet_ref'] = $request[$i]['staccount_id'];
                  // dump($details);
                  $data = new TicketDetail($details);
                  $data->save();
                  // dump($data);
                } elseif (in_array($request[$i]['staccount_id'], array_column($discount, 'id'))) {
                  $elementDiscount = $list_discount[array_keys(array_column($list_discount, 'id'), $request[$i]['staccount_id'])[0]];
                  // dd("llega al else if");
                  $details['tickdet_monto'] = $elementDiscount["discount"];
                  $details['tickdet_iva'] = 0;
                  $details['tickdet_desciption'] = "Descuento por: {$elementDiscount["description"]}" . " - " . $this->getMonthFromExpiration($request[$i]['staccount_expiration']);
                  $details['tickdet_qty'] = 1;
                  $details['tickdet_ref'] = $request[$i]['staccount_id'];
                  $data = new TicketDetail($details);
                  $data->save();
                }
                $details['tickdet_monto'] = ($request[$i]['staccount_amount_paid'] - $old_staccount->staccount_amount_paid);
              }
              /*Consultar como recuperar el iva*/
              if ($request[$i]['oper_id'] == 0) {
                $details['tickdet_iva'] = 0;
              } else {
                $details['tickdet_iva'] = 0;
              }
              $details['tickdet_qty'] = 1;
              if ($request[$i]['staccount_description'] == null) {
                $data = DB::table('subject_evaluation')->join('careers_subjets', 'careers_subjets.carsubj_id', '=', 'subject_evaluation.carsubj_id')
                  ->join('evaluation_types', 'evaluation_types.evaltype_id', '=', 'subject_evaluation.evaltype_id')
                  ->join('subjects', 'subjects.subj_id', '=', 'careers_subjets.materia_id')
                  ->where('subeval_id', '=', $request[$i]['subeval_id'])
                  ->select('subeval_name', 'evaluation_types.evaltype_name', 'subjects.subj_name')
                  ->first();
                $subeval_name = $data->evaltype_name . " " . $data->subj_name;
                $details['tickdet_desciption'] = $subeval_name;
              } else {
                $details['tickdet_desciption'] = $request[$i]['staccount_description'] . " - " . $this->getMonthFromExpiration($request[$i]['staccount_expiration']);
              }
              if ($request[$i]['staccount_id'] == null) {
                $details['tickdet_ref'] = $request[$i]['evstu_id'];
              } else {
                $details['tickdet_ref'] = $request[$i]['staccount_id'];
              }

              $data = new TicketDetail($details);

              $data->save();

              // aqui debe generar un nuevo movimiento
              $monto_movimiento = $monto_movimiento + $data->tickdet_monto;
            }
          }
        }
        // foreach ($detailsMethod as $key => $value) {
        //   $tills_details = new Request();
        //   $tills_details['factura_id'] = $dato->ticket_id;
        //   // $tills_details->till_id = se tiene que cargar
        //   // $tills_details->current_user.person_id
        //   $tills_details['description'] = 'Cobro de aranceles';
        //   $tills_details['dettills_type'] = true;
        //   $tills_details['ref_id'] = $dato->ticket_id;
        //   $tills_details['pay_type_id'] = $value['pay_type_id'];
        //   // $tills_details['dettills_receipt_number'] = $value['dettills_receipt_number'];
        //   // $tills_details->date = now()/
        //   $tills_details['amount'] = $value['value'];
        //   $tills_details['proofPayments'] = $proofPayments;
        //   // $tills_details['ticket_number'] = $dato->ticket_number;
        //   $till_det = new TillDetailsController;


        //   $tilldet_message = $till_det->store($tills_details);
        // }
      }
      $fecha = date('Y-m-d');
      if ($can_modify_date == true) {
        $fecha = $ticket_date;
      }
      foreach ($detailsMethod as $key => $value) {
        // dump($proofPayments);
        $tills_details = new Request();
        $tills_details['factura_id'] = $dato->ticket_id;
        // $tills_details->till_id = se tiene que cargar
        // $tills_details->current_user.person_id
        $tills_details['description'] = 'Cobro de aranceles';
        $tills_details['dettills_type'] = true;
        $tills_details['ref_id'] = $dato->ticket_id;
        $tills_details['pay_type_id'] = $value['pay_type_id'];
        // $tills_details['dettills_receipt_number'] = $value['dettills_receipt_number'];
        $tills_details->date = $fecha;
        $tills_details['person_id'] = $person_id;
        $tills_details['amount'] = $value['value'];
        $tills_details['proofPayments'] = $proofPayments;
        // $tills_details['ticket_number'] = $dato->ticket_number;
        $till_det = new TillDetailsController;
        $tilldet_message = $till_det->store($tills_details);
      }


      return response()->json($dato->ticket_id);
      // return response()->json('algo en este');

      // return response()->json('se genero un nuevo ticket con exito');
    } catch (\Exception $e) {
      return response()->json('ha ocurrido un error en ticket details controller');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Ticket  $country
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //

    $ticket = Ticket::join('persons', 'persons.person_id', '=', 'tickets.person_id')
      ->select('tickets.*', DB::raw("CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS person_name"), 'persons.person_idnumber')
      ->where('ticket_id', '=', $id)
      ->first();
    $ticketDetails = TicketDetail::join('ticket_details_types', 'ticket_details_types.tdtype_id', '=', 'ticket_details.tdtype_id')
      ->select('ticket_details.*', 'ticket_details_types.tdtype_desc')
      ->where('ticket_details.ticket_id', '=', $id)
      ->get();
    $sum = TicketDetail::select(DB::raw('sum(tickdet_monto) as total'))
      ->where([['ticket_details.ticket_id', '=', $id], ['ticket_details.tickdet_desciption', 'not ilike', '%Descuento%']])
      ->first();
    $res = TicketDetail::select(DB::raw('sum(tickdet_monto) as total'))
      ->where([['ticket_details.ticket_id', '=', $id], ['ticket_details.tickdet_desciption', 'ilike', '%Descuento%']])
      ->first();
    $ex = TicketDetail::select(DB::raw('sum(tickdet_monto) as total'))
      ->where([['ticket_details.ticket_id', '=', $id], ['ticket_details.tickdet_iva', '=', '0']])
      ->first();
    $cinco = TicketDetail::select(DB::raw('sum(tickdet_monto) as total'))
      ->where([['ticket_details.ticket_id', '=', $id], ['ticket_details.tickdet_iva', '=', '5']])
      ->first();
    $diez = TicketDetail::select(DB::raw('sum(tickdet_monto) as total'))
      ->where([['ticket_details.ticket_id', '=', $id], ['ticket_details.tickdet_iva', '=', '10']])
      ->first();
    $ticket->ticketDetails = $ticketDetails;
    $ticket->total = $sum->total - $res->total;
    $ticket->exentas = $ex->total;
    $ticket->cinco = $cinco->total;
    $ticket->diez = $diez->total;

    return $this->showOne($ticket, 200);
  }


  public function showPerson(Request $request, $id)
  {
    if (!property_exists('sort_by', $request)) {
      $request->sort_by = 'ticket_date';
      $request->order = 'desc';
    } else {
      // $sort_by = $request->sort_by;
      // $order = $request->order;
    }
    if (isset($request->type)) {
      $tickets = DB::select("select t.*, concat(p.person_fname, ' ', p.person_lastname)
        as person_name, p.person_idnumber, ticket_details(t.ticket_id) as details,
        get_sum_ticket_details(t.ticket_id) as sum_details,
        get_payment_types_from_tickets(t.ticket_id) as pay_types
          from tickets t join only persons p on p.person_id = t.person_id
          join ticket_details td on t.ticket_id = td.ticket_id
        where
          t.deleted_at is null and t.person_id = " . $request->person_id . "
          and t.ticket_status != 'Rechazado'
          and t.ticket_status != 'Anulado'
          and t.ticket_status != 'A'
        group by t.ticket_id, p.person_fname, p.person_lastname, p.person_idnumber
        order by t.ticket_date asc
         ");
      $data_len = count($tickets);
      for ($i = 0; $i < $data_len; $i++) {
        // $sum = DB::select("select sum(td.tickdet_monto) as total
        //   from ticket_details td
        //   where td.ticket_id = ".$response->data[$i]->ticket_id.
        //   " and td.tickdet_desciption not ilike '%Descuento%' limit 1");
        // $res = DB::select("select sum(td.tickdet_monto) as total
        //   from ticket_details td
        //   where td.ticket_id = ".$response->data[$i]->ticket_id.
        //   " and td.tickdet_desciption ilike '%Descuento%' limit 1");
        // $details = DB::select("select * from ticket_details td where ticket_id = ".$response->data[$i]->ticket_id);
        // $response->data[$i]->ticket_details = $details;
        // $response->data[$i]->total = $sum[0]->total - $res[0]->total;
        $tickets[$i]->details = json_decode($tickets[$i]->details);
        $tickets[$i]->pay_types = json_decode($tickets[$i]->pay_types);
        // $response->data[$i]->sub_products = json_decode($response->data[$i]->sub_products, true);
      }
      return $tickets;
    } else {
      $count = DB::select("select count(t.ticket_id)
        from tickets t join only persons p on p.person_id = t.person_id
        -- join ticket_details td on t.ticket_id = td.ticket_id
        where t.deleted_at is null and t.person_id = " . $id . "
         --group by t.ticket_id, p.person_fname, p.person_lastname, p.person_idnumber");
      // dd($count);
      $ticket = "select t.*, concat(p.person_fname, ' ', p.person_lastname)
        as person_name, p.person_idnumber, ticket_details(t.ticket_id) as details,
        get_sum_ticket_details(t.ticket_id) as sum_details,
        get_payment_types_from_tickets(t.ticket_id) as pay_types
          from tickets t join persons p on p.person_id = t.person_id
          join ticket_details td on t.ticket_id = td.ticket_id
        where t.deleted_at is null and t.person_id = " . $id . "
        group by t.ticket_id, p.person_fname, p.person_lastname, p.person_idnumber
         ";
      $return_response = $this->showAll($ticket, $count, 200);
      $response = json_decode(json_encode($return_response))->original;
      $data_len = count($response->data);
      for ($i = 0; $i < $data_len; $i++) {
        // $sum = DB::select("select sum(td.tickdet_monto) as total
        //   from ticket_details td
        //   where td.ticket_id = ".$response->data[$i]->ticket_id.
        //   " and td.tickdet_desciption not ilike '%Descuento%' limit 1");
        // $res = DB::select("select sum(td.tickdet_monto) as total
        //   from ticket_details td
        //   where td.ticket_id = ".$response->data[$i]->ticket_id.
        //   " and td.tickdet_desciption ilike '%Descuento%' limit 1");
        // $details = DB::select("select * from ticket_details td where ticket_id = ".$response->data[$i]->ticket_id);
        // $response->data[$i]->ticket_details = $details;
        // $response->data[$i]->total = $sum[0]->total - $res[0]->total;
        $response->data[$i]->details = json_decode($response->data[$i]->details);
        $response->data[$i]->pay_types = json_decode($response->data[$i]->pay_types);
        // $response->data[$i]->sub_products = json_decode($response->data[$i]->sub_products, true);
      }
      return response()->json($response);
    }

    // $ticket = Ticket::join('persons', 'persons.person_id', '=', 'tickets.person_id')
    //   ->join('ticket_details', 'ticket_details.ticket_id', '=', 'tickets.ticket_id')
    //   // ->join('tills_details', 'tills_details.factura_id', '=', 'tickets.ticket_id')
    //   // ->join('dettill_proofpayment', 'dettill_proofpayment.dettills_id', '=', 'tills_details.dettills_id')
    //   // ->join('proof_payment', 'proof_payment.pr_payment_id', '=', 'dettill_proofpayment.pr_payment_id')
    //   // ->join('payment_type', 'payment_type.pay_type_id', '=', 'proof_payment.pay_type_id')
    //   // ->join('employees', 'employees.person_id', '=', 'tills_details.person_id')
    //   ->select(
    //     'tickets.*',
    //     // DB::raw("CONCAT(employees.person_fname, ' ' ,employees.person_lastname) AS employee_name"),
    //     DB::raw("CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS person_name"),
    //     'persons.person_idnumber',
    //     DB::raw("ticket_details(tickets.ticket_id) as details"),
    //     DB::raw("get_sum_ticket_details(tickets.ticket_id) as sum_details"),
    //     DB::raw("get_payment_types_from_tickets(tickets.ticket_id) as pay_types")
    //     // 'payment_type.pay_type_desc'
    //   )
    //   ->where([
    //     ['tickets.person_id', '=', $id]
    //   ])
    //   ->distinct('tickets.ticket_id')
    //   ->orderBy('tickets.ticket_id', 'asc')
    //   ->orderBy('tickets.ticket_date', 'desc')
    //   ->get();
    // $len = count($ticket);
    // for ($i = 0; $i < $len; $i++) {
    //   $ticket[$i]->details = json_decode($ticket[$i]->details);
    //   $ticket[$i]->pay_types = json_decode($ticket[$i]->pay_types);
    // }
    //
    //
    // return $this->showAll($ticket, 200);
  }


  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = Ticket::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Factura se restauro con exito!');
    } else {
      $dato = Ticket::find($id);
      // $dato->person_id = $request->get('person_id');
      // $dato->ticket_date = $request->get('ticket_date');
      $dato->ticket_number = $request->get('ticket_number');
      if ($request->get('ticket_status') === 'Pendiente') {
        $dato->ticket_status = "Generado";
      }
      $dato->save();
      return response()->json('Factura actualizada con exito');
    }
  }

  public function updateTicketNumber(Request $request)
  {

    $dato = Ticket::find($request->ticket_id);
    $dato->ticket_number = $request->get('ticket_number');
    if ($request->get('ticket_status') === 'Pendiente') {
      $dato->ticket_status = "Generado";
    }
    $dato->ticket_is_invoice = $request->get('ticket_is_invoice');
    $dato->save();
    return response()->json('Factura actualizada con exito');
  }


  public function rollback($id)
  {
    // dd('responde desde el controller');
    $ticket = Ticket::join('persons', 'persons.person_id', '=', 'tickets.person_id')
      ->select('tickets.*', DB::raw("CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS person_name"), 'persons.person_idnumber')
      ->where('ticket_id', '=', $id)
      ->first();
    $ticketDetails = TicketDetail::select('ticket_details.*')
      ->where('ticket_details.ticket_id', '=', $id)
      ->get();
    $ticket->ticket_status = 'Anulado';
    $ticket->save();
    $tills_details = TillDetail::where('factura_id', '=', $id)->get();
    for ($i = 0; $i < count($tills_details); $i++) {
      $tills_details[0]->delete();
    }
    // $ticket->ticket_details = $ticketDetails;
    for ($i = 0; $i < count($ticketDetails); $i++) {
      if (strpos($ticketDetails[$i]->tickdet_desciption, 'Mora') !== false) {
        $staccount = StudentAccount::find($ticketDetails[$i]->tickdet_ref);
        $staccount->surcharge = $staccount->surcharge - $ticketDetails[$i]->tickdet_monto;
        $staccount->staccount_status = false;
        $staccount->save();
      } elseif (strpos($ticketDetails[$i]->tickdet_desciption, 'Descuento') !== false) {
        $staccount = StudentAccount::find($ticketDetails[$i]->tickdet_ref);
        $staccount->staccount_amount_paid = $staccount->staccount_amount_paid - $ticketDetails[$i]->tickdet_monto;
        $staccount->staccount_status = false;
        $staccount->save();
      } elseif (strpos($ticketDetails[$i]->tickdet_desciption, 'Cuota') !== false || strpos($ticketDetails[$i], 'Matricula') !== false) {
        $staccount = StudentAccount::find($ticketDetails[$i]->tickdet_ref);
        $staccount->staccount_amount_paid = $staccount->staccount_amount_paid - $ticketDetails[$i]->tickdet_monto;
        $staccount->staccount_status = false;
        $staccount->save();
        if (strpos($ticketDetails[$i], 'Matricula') !== false) {
          $enroll = Enrolled::find($staccount->enroll_id);
          $enroll->enroll_type = "Pre-Inscripcion";
          $enroll->save();
        }
      } elseif (strpos($ticketDetails[$i]->tickdet_desciption, 'Oportunidad') !== false) {
        $eval_students = EvalStudent::find($ticketDetails[$i]->tickdet_ref);
        $eval_students->evstu_paid = false;
        $eval_students->save();
      } elseif (strpos($ticketDetails[$i]->tickdet_desciption, 'Recuperatorio') !== false) {
        $eval_students = EvalStudent::find($ticketDetails[$i]->tickdet_ref);
        $eval_students->evstu_paid = false;
        $eval_students->save();
      } elseif ($ticketDetails[$i]->tdtype_id == 4) { //pruebas parciales
        $eval_students = EvalStudent::join('carsub_enrolled', 'carsub_enrolled.carsub_en_id', '=', 'eval_students.carsub_en_id')
          ->join('subject_evaluation', 'subject_evaluation.subeval_id', '=', 'eval_students.subeval_id')
          ->where([['carsub_enrolled.enroll_id', $ticketDetails[$i]->tickdet_ref], ['subject_evaluation.evaltype_id', 6]])->get();
        // dd($eval_students);
        foreach ($eval_students as $key => $value) {
          $ev = EvalStudent::find($value->evstu_id);
          $ev->evstu_paid = false;
          $ev->save();
        }
      }
    }
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Ticket  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = Ticket::find($id);
    $dato->delete();
    return response()->json('Factura se elimino con exito');
  }

  public function search(Request $req)
  {
    if (!isset($req->filter)) {
      $count = DB::select("select count(t.ticket_id)
      from tickets t join only persons p on p.person_id = t.person_id
     join ticket_details td on t.ticket_id = td.ticket_id
      where t.deleted_at is null and t.ticket_number ilike '%" . $req->search . "%'
     group by t.ticket_id, p.person_fname, p.person_lastname, p.person_idnumber");
      // dd($count);
      $ticket = "select t.*, concat(p.person_fname, ' ', p.person_lastname)
      as person_name, p.person_idnumber, ticket_details(t.ticket_id) as details,
      get_sum_ticket_details(t.ticket_id) as total,
      get_payment_types_from_tickets(t.ticket_id) as pay_types
        from tickets t join persons p on p.person_id = t.person_id
        join ticket_details td on t.ticket_id = td.ticket_id
      where t.deleted_at is null and t.ticket_number ilike '%" . $req->search . "%'
      group by t.ticket_id, p.person_fname, p.person_lastname, p.person_idnumber
       ";
    } else {
      $filters = new TicketsAdvancesFilters;
      $applied_filter = $filters->applyFilter($req->filter);
      // dd($applied_filter);
      $count = DB::select("select count(t.*) from  tickets t join persons p on p.person_id = t.person_id
      join ticket_details td on t.ticket_id = td.ticket_id where " . $applied_filter);
      $ticket = "select t.*, concat(p.person_fname, ' ', p.person_lastname)
      as person_name, p.person_idnumber, ticket_details(t.ticket_id) as details,
      get_sum_ticket_details(t.ticket_id) as total,
      get_payment_types_from_tickets(t.ticket_id) as pay_types
        from tickets t join persons p on p.person_id = t.person_id
        join ticket_details td on t.ticket_id = td.ticket_id where " . $applied_filter . ' group by p.person_id, t.ticket_id';
    }
    // dd($ticket);
    return $this->showAll($ticket, $count, 200);
  }
}
