<?php

namespace App\Http\Controllers\TillDetails;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\ApiDBController;
use Illuminate\Http\Request;
use App\TillDetail;
use App\Till;
use App\DettillProofPayment;
use App\ProofPayment;
use App\TillTransfer;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Tickets\TicketController;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\TillDetails\TillDetailsAdvancesFilters;

class TillDetailsController extends ApiDBController
{
  public function index()
  {
    // $t = TillDetail::query()->first();
    // $query = TillDetail::query();
    // $query = $this->filterData($query, $t);
    // $datos = $query->get();
    // return $this->showAll($datos, 200);
    $datos = "select * from tills_details td where td.deleted_at is null";
    $count = DB::select("select count(*) from tills_details td where td.deleted_at is null");
    return $this->showAll($datos, $count, 200);
  }
  public function listExpenses(Request $request)
  {
    if (!property_exists($request, 'sort_by')) {
      $sort_by = 'date';
      $order = 'desc';
      $request->sort_by = 'date';
      $request->order = 'desc';
    } else {
      $sort_by = $request->sort_by;
      $order = $request->order;
    }
    $tills = null;
    $persons = null;
    if ($request->tills != null and $request->tills[0] != null) {
      $tills = implode(',', $request->tills);
    }
    // dd($request->persons[0]);
    if ($request->person != null and $request->person != 'null') {
      // dd('entra');
      $persons = implode(',', $request->person);
    }
    $filter = [];
    $filter = ['persons' => $persons, 'tills' => $tills, 'amount' => $request->amount];
    $totales = ['salarios' => 0, 'proveedores' => 0];
    $filters = new TillDetailsAdvancesFilters;
    $applied_filter = $filters->applyFilter($filter);
    if ($request->tills != null or $request->person != null or $request->amount != null) {
      // dd($request);
      $egre_view = "select sa.*  from expenses_detail sa
      where
        (sa.date::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
      and sa.dettills_type = false and sa.dettill_status is true and sa.deleted_at is null 
      and (sa.description not ilike '%Cierre de caja%' and sa.description not ilike '%transferencia de caja%'))
       " . $applied_filter . "
    ";
      // dd($egre_view);
      $count = DB::select("select  count(distinct sa.dettills_id)  from expenses_detail sa
    where
      (sa.date::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
    and sa.dettills_type = false and sa.deleted_at is null  and sa.dettill_status is true 
    and (sa.description not ilike '%Cierre de caja%' and sa.description not ilike '%transferencia de caja%'))
       " . $applied_filter . "
      ");

      // $totales = DB::select("select sum(sa.amount)  from expenses_detail sa
      // where ");

      $totales = DB::select("select sum(case when  pa.provacc_id is not null then
      td.amount end)::bigint as proveedores,
    sum( case when  pa2.profeacc_id is not null or td.description ='Pago a docente' then
      td.amount end)::bigint docentes,
    sum( case when  ea.emp_acc_id is not null then
    td.amount end)::bigint as funcionarios
from tills_details td
left join provider_accounts pa on (pa.provacc_id  = td.ref_id and td.description = 'Pago a proveedor')
left join professor_accounts pa2 on (pa2.profeacc_id = td.ref_id and td.description = 'Pago a docente')
left join employee_account ea on (ea.emp_acc_id = td.ref_id and td.description = 'Pago a funcionario')
left join only persons sa on sa.person_id = pa.person_id or sa.person_id = pa2.person_id or sa.person_id = ea.person_id
where td.dettills_type = false and (td.date between '" . $request->from_date . "' and '" . $request->to_date . "')
and td.deleted_at is null
and td.dettill_status = true
" . $applied_filter . "
      and (td.description not ilike '%Cierre de caja%' and td.description not ilike '%transferencia de caja%')");
    } else {
      $egre_view = "select sa.* from expenses_detail sa
      where
        (sa.date::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
      and sa.dettills_type = false and sa.dettill_status is true and sa.deleted_at is null and (sa.description not ilike '%Cierre de caja%' and sa.description not ilike '%transferencia de caja%'))
    ";
      $count = DB::select("select  count(distinct sa.dettills_id)  from expenses_detail sa
    where
      (sa.date::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
    and sa.dettills_type = false and sa.dettill_status is true and sa.deleted_at is null and (sa.description not ilike '%Cierre de caja%' and sa.description not ilike '%transferencia de caja%'))
  ");
      // dd($count);
      $totales = DB::select("select sum(case when  pa.provacc_id is not null then
      td.amount end)::bigint as proveedores,
    sum( case when  pa2.profeacc_id is not null or td.description ='Pago a docente' then
      td.amount end)::bigint as docentes,
    sum( case when  ea.emp_acc_id is not null then
    td.amount end)::bigint as funcionarios
from tills_details td
left join provider_accounts pa on (pa.provacc_id  = td.ref_id and td.description = 'Pago a proveedor')
left join professor_accounts pa2 on (pa2.profeacc_id = td.ref_id and td.description = 'Pago a docente')
left join employee_account ea on (ea.emp_acc_id = td.ref_id and td.description = 'Pago a funcionario')
left join only persons prv on prv.person_id = pa.person_id
left join professors prf on prf.person_id = pa2.person_id
left join employees emp on emp.person_id = ea.person_id
where td.dettills_type = false and (td.date between '" . $request->from_date . "' and '" . $request->to_date . "')
and td.deleted_at is null
and td.dettill_status is true 
and (td.description not ilike '%Cierre de caja%' and td.description not ilike '%transferencia de caja%')");
    }

    // $datos = "select distinct td.*,
    //           prv.person_fname as provider_name, prv.person_lastname as provider_lastname, prv.person_idnumber as provider_idnumber, prv.person_ruc as provider_ruc,
    //           prf.person_fname as profe_fname, prf.person_lastname as profe_lastname, prf.person_idnumber as profe_idnumber, prf.person_ruc as profe_ruc,
    //           emp.person_fname as emplo_fname, emp.person_lastname as emplo_lastname, emp.person_idnumber as emplo_idnumber, emp.person_ruc as emplo_ruc,
    //           pa.provacc_description, pa.provacc_amountpaid,
    //           pa2.profeacc_desc, pa2.profeacc_month, pa2.profeacc_amountpaid,
    //           ea.emp_acc_desc , ea.emp_acc_month, ea.emp_acc_amountpaid
    //           from tills_details td
    //           left join provider_accounts pa on (pa.provacc_id  = td.ref_id and td.description = 'Pago a proveedor')
    //           left join professor_accounts pa2 on (pa2.profeacc_id = td.ref_id and td.description = 'Pago a docente')
    //           left join employee_account ea on (ea.emp_acc_id = td.ref_id and td.description = 'Pago a funcionario')
    //           left join persons prv on prv.person_id = pa.person_id
    //           left join professors prf on prf.person_id = pa2.person_id
    //           left join employees emp on emp.person_id = ea.person_id
    //           where td.dettills_type = false and (td.date between '" . $request->from_date . "' and '" . $request->to_date . "')
    //           and td.description <> 'Cierre de caja'";

    // $count = DB::select("select count(td.dettills_id)
    //                       from tills_details td
    //                       left join provider_accounts pa on (pa.provacc_id  = td.ref_id and td.description = 'Pago a proveedor')
    //                       left join professor_accounts pa2 on (pa2.profeacc_id = td.ref_id and td.description = 'Pago a docente')
    //                       left join employee_account ea on (ea.emp_acc_id = td.ref_id and td.description = 'Pago a funcionario')
    //                       left join persons prv on prv.person_id = pa.person_id
    //                       left join professors prf on prf.person_id = pa2.person_id
    //                       left join employees emp on emp.person_id = ea.person_id
    //                       where td.dettills_type = false and (td.date between '" . $request->from_date . "' and '" . $request->to_date . "')
    //                       and td.description <> 'Cierre de caja'");
    // // dd($datos);
    /*
      tiene que tener los totales de docentes, salarios, luz, internet
    */

    // dd($totales);
    // $datos = TillDetail::leftjoin('provider_accounts', 'provider_accounts.provacc_id', '=', 'tills_details.ref_id')
    //   ->leftjoin('persons', 'persons.person_id', '=', 'provider_accounts.person_id')
    //   ->where('tills_details.dettills_type', '=', false)
    //   ->select(
    //     'tills_details.*',
    //     'persons.person_fname',
    //     'persons.person_idnumber',
    //     'persons.person_ruc',
    //     'provider_accounts.provacc_description',
    //     'provacc_amountpaid'
    //   )
    //   ->whereBetween('date', [$request->from_date, $request->to_date])
    //   ->distinct()
    //   ->orderBy($sort_by, $order)
    //   ->get();
    if ($request->from == 'frontend') {
      return $this->showAll($egre_view, $count, 200);
    } elseif ($request->from == 'frontend_balance') {
      return $totales;
    } else {
      return ['datos' => DB::select($egre_view . " order by " . $sort_by . " " . $order), 'totales' => $totales];
    }
  }
  public function getBalance(Request $request)
  {
    ini_set('memory_limit','512M');
    set_time_limit(3000);
    $incomes_total = 0;
    $expenses_total = 0;
    $td = new TicketController;
    $tills_previous = DB::select("select round(gettillamountforbalance(false,t.till_id,'" . $request->from_date . "','" . $request->to_date . "')::decimal) as amount, t.* from tills t where t.till_type = 3 or t.till_name ilike '%rectorado%'");
    $till_actual = DB::select("select round(gettillamountforbalance(true,t.till_id,'" . $request->from_date . "','" . $request->to_date . "')::decimal) as amount, t.* from tills t where t.till_type = 3 or t.till_name ilike '%rectorado%'");

    // dd($till_actual);
    // $incomes = $this->report_to_incomes($request);
    $incomes = $td->listTicketDetails($request);
    $expenses = $this->listExpenses($request);
    // dd($incomes);
    foreach ($incomes as $key => $value) {
      $incomes_total = $incomes_total + $value;
    }
    // dd($expenses[0]);
    foreach ($expenses[0] as $key => $value) {
      $expenses_total = $expenses_total + $value;
    }

    if ($request->from == 'frontend_balance') {
      return [
        'till_actual' => $till_actual,
        'tills_previous' => $tills_previous,
        'incomes' => $incomes,
        'expenses' => $expenses[0],
        'incomes_total' => $incomes_total,
        'expenses_total' => $expenses_total,
        'balance_total' => $incomes_total - $expenses_total
      ];
      // return $this->showAll($datos,200);
    } else {
      return [
        'incomes' => $incomes,
        'expenses' => $expenses,
        'incomes_total' => $incomes_total,
        'expenses_total' => $expenses_total,
        'balance_total' => $incomes_total - $expenses_total
      ];
    }
  }

  public function get_till_to_close($id)
  {
    $data = "with payment_type_am as(select get_amount_payment_from_balance(pay_type_id, ''||now()::date||'', array_agg(" . $id . "), true) as amount, *
    from payment_type  group by pay_type_id ) select split_part(pay_type_desc,' -',1) as splited , *
     from payment_type_am
     where amount is not null";
    $count = DB::select("with payment_type_am as(select get_amount_payment_from_balance(pay_type_id, ''||now()::date||'', array_agg(" . $id . "), true) as amount, *
     from payment_type  group by pay_type_id ) select count(*)
      from payment_type_am
      where amount is not null");

    $return_response = $this->showAll($data, $count, 200);
    $response = json_decode(json_encode($return_response))->original;
    for ($i = 0; $i < count($response->data); $i++) {
      $till = DB::select("select * from tills t where t.till_name ilike '%" . $response->data[$i]->splited . "%' limit 1");

      if (isset($till[0])) {
        $response->data[$i]->till_id = $till[0]->till_id;
        $response->data[$i]->till_name = $till[0]->till_name;
        $response->data[$i]->time_out = false;
      } else {
        $response->data[$i]->till_id = 0;
        $response->data[$i]->till_name = "Seleccione una caja";
        $response->data[$i]->time_out = false;
      }
    }
    return response()->json($response);
  }

  public function store(Request $request)
  {
    // dd(isset($request['person_id']));
    // if (!isset($request['person_id'])) {
    //   $request['person_id'] = auth()->user()->person_id;
    // }

    if ($request->factura_id != 0) {
      $ticket = DB::table('tickets')
        ->select('tickets.ticket_number', DB::raw("SUM(ticket_details.tickdet_monto) as total"))
        ->join('ticket_details', 'ticket_details.ticket_id', '=', 'tickets.ticket_id')
        ->where('tickets.ticket_id', '=', $request->factura_id)
        ->groupBy('tickets.ticket_id')
        ->first();
      $request['ticket_number'] = $ticket->ticket_number;
      // $request['amount'] = $ticket->total;
      $numero_factura = $ticket->ticket_number;
      $today = $request->date;
      // try {
      $till = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
        ->select('tills.till_id')
        ->where([
          ['tills_details.description', 'Apertura de caja'],
          ['tills_details.person_id', '=', auth()->user()->person_id],
          ['tills.till_starting', '=', true],
        ])
        ->orderBy('tills_details.date', 'desc')
        ->first();
      if ($till != null) {
        $request['till_id'] = $till->till_id;
        $request->account_p_id = 0;
        $datos = new TillDetail(
          [
            'person_id' => auth()->user()->person_id,
            'till_id' => $till->till_id,
            'factura_id' => $request->factura_id,
            'ref_id' => $request->factura_id,
            'amount' => $request->amount,
            'description' => $request->description,
            'date' => $request->date,
            'account_p_id' => $request->account_p_id,
            'dettills_receipt_number' => $request->dettills_receipt_number,
            'dettills_type' => $request->dettills_type,
            'dettill_status' => true
          ]
        );

        $datos->save();
        // sleep(3);

        foreach ($request->proofPayments as $key => $value) {
          if ($value['pay_type_id'] == $request->pay_type_id) {
            $imageName =  'ninguno';
            if ($value['dettills_pr_pay_image'] != 'ninguno' && $value['pr_requires_image'] != false) {
              $image = $value['dettills_pr_pay_image'];
              $imageName =  $value['pay_type_desc'] . ' - ' . $numero_factura .  '.png';
              $imagen = Image::make($image[0])->encode('png', 90);
              $destinationPath = public_path('img');
              $imagen->save($destinationPath . "/" . $imageName);
            }

            $proof = new DettillProofPayment();
            $proof->dettills_id = $datos['dettills_id'];
            $proof->pr_payment_id = $value['pr_payment_id'];
            $proof->dettills_pr_pay_desc = $value['dettills_pr_pay_desc'];
            $proof->dettills_pr_pay_image = $imageName;
            $proof->save();
            // dd($proof->save());
          }
        }
      } else {
        $datos = 'No hay una caja abierta para este usuario';
      }
      return response()->json('Operación exitosa!');
      // } catch (\Exception $e) {
      //   return response()->json($e);
      // }


    } else {


      $datos = new TillDetail([
        'person_id' => $request->user()->person_id,
        'factura_id' => 0,
        'till_id' => $request->till_id,
        'amount' => $request->amount,
        'description' => $request->description,
        'account_p_id' => 0,
        'dettills_type' => $request->dettills_type
      ]);
      if ($datos->description == 'Apertura de caja') {
        $today = date("Y-m-d");
        $dato = DB::select("select td.description, td.date ,t.* from tills_details td join tills t on td.till_id = t.till_id
        where (t.till_starting = true and td.description = 'Apertura de caja' and td.person_id = " . auth()->user()->person_id . ")
        and
        (select count(*) from tills_details td2 join tills t2 on td2.till_id = t2.till_id
        where td2.description ilike '%Cierre de caja%' and td.till_id = td2.till_id and td.created_at < td2.created_at) = 0");
        // $dato = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
        //   ->select('tills.*')
        //   ->where([
        //     ['tills_details.person_id', '=', auth()->user()->person_id],
        //     // ['tills_details.till_id', '=', $request->till_id],
        //     ['tills.till_starting', '=', true],
        //     ['tills_details.description', '=', "Apertura de caja"]
        //     // , ['tills_details.date', $today]
        //   ])
        //   ->orderBy('tills_details.date', 'desc')
        //   ->first();

        $is_available = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
          ->select('tills.*')
          ->where([
            ['tills.till_id', '=', $request->till_id],
            ['tills.till_starting', '=', true],
            ['tills_details.description', '=', "Apertura de caja"]
            // , ['tills_details.date', $today]
          ])
          ->orderBy('tills_details.date', 'desc')
          ->first();
        // dd($dato);
        if (isset($dato)) {
          if ($is_available == null) {
            $datos->save();
            $pay_type = ProofPayment::where('pay_type_id', 1)->first();
            $proof = new DettillProofPayment();
            $proof->dettills_id = $datos['dettills_id'];
            $proof->pr_payment_id = $pay_type['pr_payment_id'];
            $proof->dettills_pr_pay_desc = 'Ninguno';
            $proof->save();
            $till = Till::findOrFail($datos->till_id);
            $till->till_starting = true;
            $till->save();
            return response()->json('Detalle de Caja se agrego con exito!');
          } else {
            return response()->json('Caja no disponible');
          }
        } else {
          return response()->json('No puede abrir mas de una caja');
        }
      } elseif ($datos->description == 'Cierre de caja') {

        if (isset($request->till_to_close)) {
          // dd($request->till_to_close);
          for ($i = 0; $i < count($request->till_to_close); $i++) {
            $till_detail = new TillDetail([
              'person_id' => $request->user()->person_id,
              'factura_id' => 0,
              'till_id' => $request->till_id,
              'amount' => $request->till_to_close[$i]['amount'],
              'description' => $request->description . " para " . $request->till_to_close[$i]['pay_type_desc'],
              'account_p_id' => 0,
              'dettills_type' => $request->dettills_type
            ]);
            $till_detail->save();
            $pay_type = ProofPayment::where('pay_type_id', $request->till_to_close[$i]['pay_type_id'])->first();
            $proof = new DettillProofPayment();
            $proof->dettills_id = $till_detail['dettills_id'];
            $proof->pr_payment_id = $pay_type['pr_payment_id'];
            $proof->dettills_pr_pay_desc = 'Ninguno';
            $proof->save();
            $today = date("Y-m-d");

            $transfer = new TillTransfer([
              'origin_till_id' => $request->get('till_id'),
              'destiny_till_id' => $request->till_to_close[$i]['till_id'],
              'tilltrans_amount' => $request->till_to_close[$i]['amount'],
              'tilltrans_date' => $today,
            ]);

            $saved = $transfer->save();

            if ($saved) {
              $origin = new TillDetail([
                'till_id' => $transfer->destiny_till_id,
                'factura_id' => 0,
                'description' => 'Transferencia por cierre de caja',
                'date' => $today,
                'amount' => $transfer->tilltrans_amount,
                'ref_id' => $transfer->tilltrans_id,
                'account_p_id' => 0,
                'dettills_type' => true,
                'person_id' => auth()->user()->person_id,
              ]);
              $origin->save();
              $pay_type = ProofPayment::where('pay_type_id', $request->till_to_close[$i]['pay_type_id'])->first();
              // $pay_type = ProofPayment::where('pay_type_id', 1)->first();
              $proof_origin = new DettillProofPayment();
              $proof_origin->dettills_id = $origin['dettills_id'];
              $proof_origin->pr_payment_id = $pay_type['pr_payment_id'];
              $proof_origin->dettills_pr_pay_desc = 'Ninguno';
              $proof_origin->save();
            }

            # code...
          }
        }


        // $datos->save();

        $till = Till::findOrFail($request->get('till_id'));
        $till->till_starting = false;
        $till->save();

        return response()->json('Detalle de Caja se agrego con exito!');
      } elseif ($datos->description == 'Ingreso de dinero') {
        $datos->save();
        $pay_type = ProofPayment::where('pay_type_id', 1)->first();

        $proof = new DettillProofPayment();
        $proof->dettills_id = $datos['dettills_id'];
        $proof->pr_payment_id = $pay_type['pr_payment_id'];
        $proof->dettills_pr_pay_desc = 'Ninguno';
        $proof->save();

        return response()->json('Detalle de Caja se agrego con exito!');
      }
    }



    // dd($request->all());



  }

  public function show($id)
  {
    //$dato = TillDetail::find( $id);
    $dato = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
      ->select('tills_details.*', 'tills.till_name')
      ->where('tills_details.dettills_id', '=', $id)
      ->first();
    return $this->showOne($dato, 200);
  }

  public function report_to_incomes(Request $request)
  {
    $totales = [];
    $cuotas = 0;
    $moras = 0;
    $derechos = 0;
    $gestion_titulo = 0;
    $certificado_estudio = 0;
    $defensa_mesa = 0;
    $productos = 0;
    $gestiones = 0;
    $otros = 0;
    $year = date("Y");
    $month = date("m");
    $day = date("d");
    $cont_derechos = 0;
    $cont_cuotas = 0;
    $fecha = $year . "-" . $month . "-" . $day;
    $payment = null;


    if ($request->from == 'frontend') {
      $out = explode(",", $request->tills);
      if (isset($request->pay_type_id)) {
        // $payment =  $request->pay_type_id;
        $payment = implode(",", $request->pay_type_id);
      }

      $filter = [];
      $filter = ['persons' => $request->person_id, 'pay_type_id' => $payment, 'tills' => $request->tills, 'description' => $request->description];
      $totales = ['salarios' => 0, 'proveedores' => 0];
      $filters = new TillDetailsAdvancesFilters;
      $applied_filter = $filters->applyFilter($filter);

      $datos = "select
      distinct on (tills_details.dettills_id) tills_details.dettills_id,
      alumno.person_idnumber,
      tills_details.dettills_type,
      CONCAT(alumno.person_fname, ' ', alumno.person_lastname) AS student_name,
      CONCAT(cajero.person_fname, ' ', cajero.person_lastname) AS cajero_name,
      get_semester_from_ticket(tickets.ticket_id) AS sems_name,
      tills_details.date,
      tickets.ticket_number,
      tills_details.created_at,
      tills_details.description,
      tills_details.amount,
      payment_type.pay_type_desc,
      payment_type.pay_type_id,
      tills.till_name,
      units.unit_code,
      ticket_details_types.tdtype_desc
    from
      tills_details
      inner join tills on tills.till_id = tills_details.till_id
      inner join units on units.unit_id = tills.unit_id
      inner join dettill_proofpayment on dettill_proofpayment.dettills_id = tills_details.dettills_id
      inner join proof_payment on proof_payment.pr_payment_id = dettill_proofpayment.pr_payment_id
      inner join payment_type on payment_type.pay_type_id = proof_payment.pay_type_id
      inner join tickets on tickets.ticket_id = tills_details.factura_id
      inner join ticket_details on ticket_details.ticket_id = tickets.ticket_id
      inner join ticket_details_types on ticket_details_types.tdtype_id = ticket_details.tdtype_id
      inner join only persons as alumno on alumno.person_id = tickets.person_id
      inner join only persons as cajero on cajero.person_id = tills_details.person_id
      where
        tills_details.created_at::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
        and (tills_details.description not ilike '%Apertura%' and tills_details.description not ilike '%cierre de caja%')
        and tills_details.dettills_type = true
        and tills_details.deleted_at is null " . $applied_filter;

      $count = DB::select("select
      count(distinct tills_details.dettills_id)
    from
      tills_details
      inner join tills on tills.till_id = tills_details.till_id
      inner join units on units.unit_id = tills.unit_id
      inner join dettill_proofpayment on dettill_proofpayment.dettills_id = tills_details.dettills_id
      inner join proof_payment on proof_payment.pr_payment_id = dettill_proofpayment.pr_payment_id
      inner join payment_type on payment_type.pay_type_id = proof_payment.pay_type_id
      inner join tickets on tickets.ticket_id = tills_details.factura_id
      inner join ticket_details on ticket_details.ticket_id = tickets.ticket_id
      inner join ticket_details_types on ticket_details_types.tdtype_id = ticket_details.tdtype_id
      inner join only persons as alumno on alumno.person_id = tickets.person_id
      inner join only persons as cajero on cajero.person_id = tills_details.person_id
      where
        tills_details.created_at::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
        and tills_details.dettills_type = true
        and (tills_details.description not ilike '%Apertura%' and tills_details.description not ilike '%cierre de caja%')
        and tills_details.deleted_at is null " . $applied_filter);
      return $this->showAll($datos, $count, 200);
    } elseif ($request->from == 'frontend_balance') {
      $out = explode(",", $request->tills);
      if (isset($request->pay_type_id)) {
        // dd($request->pay_type_id);
        $payment =  $request->pay_type_id;
        // $payment = implode(",", $request->pay_type_id);
      }
      $filter = [];
      $filter = ['persons' => $request->person_id, 'pay_type_id' => $payment, 'tills' => $request->tills, 'description' => $request->description];
      $totales = ['salarios' => 0, 'proveedores' => 0];
      $filters = new TillDetailsAdvancesFilters;
      $applied_filter = $filters->applyFilter($filter);
      // dd($request);
      $datos = "select
      distinct on (tills_details.dettills_id) tills_details.dettills_id,
      alumno.person_idnumber,
      tills_details.dettills_type,
      CONCAT(alumno.person_fname, ' ', alumno.person_lastname) AS student_name,
      CONCAT(cajero.person_fname, ' ', cajero.person_lastname) AS cajero_name,
      get_semester_from_ticket(tickets.ticket_id) AS sems_name,
      tills_details.date,
      ticket_details.tickdet_desciption,
      tickets.ticket_number,
      tills_details.created_at,
      tills_details.description,
      tills_details.amount,
      payment_type.pay_type_desc,
      payment_type.pay_type_id,
      tills.till_name,
      units.unit_code,
      ticket_details_types.tdtype_desc,
      ticket_details_types.tdtype_id
    from
      tills_details
      inner join tills on tills.till_id = tills_details.till_id
      inner join units on units.unit_id = tills.unit_id
      inner join dettill_proofpayment on dettill_proofpayment.dettills_id = tills_details.dettills_id
      inner join proof_payment on proof_payment.pr_payment_id = dettill_proofpayment.pr_payment_id
      inner join payment_type on payment_type.pay_type_id = proof_payment.pay_type_id
      inner join tickets on tickets.ticket_id = tills_details.factura_id
      inner join ticket_details on ticket_details.ticket_id = tickets.ticket_id
      inner join ticket_details_types on ticket_details_types.tdtype_id = ticket_details.tdtype_id
      inner join only persons as alumno on alumno.person_id = tickets.person_id
      inner join only persons as cajero on cajero.person_id = tills_details.person_id
      where
        tills_details.created_at::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
        and (tills_details.description not ilike '%Apertura%' and tills_details.description not ilike '%cierre de caja%')
        and tills_details.dettills_type = true
        and tills_details.deleted_at is null " . $applied_filter . "
        ";
      // dd($datos);

      $query = $datos;
      $datos = DB::select($query);

      $array_gestiones = [];
      //dd($datos);
      for ($i = 0; $i < count($datos); $i++) {
        if (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == false) {
          $cuotas = $cuotas + $datos[$i]->amount;
          $cont_cuotas = $cont_cuotas + 1;
        } elseif (in_array($datos[$i]->tdtype_id, [1, 17]) && strstr($datos[$i]->tickdet_desciption, 'Mora') == true) {
          $moras = $moras + $datos[$i]->amount;
        } elseif (in_array($datos[$i]->tdtype_id, [2, 3, 4, 5, 6, 7, 8, 9, 15]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'oportunidad') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'prueba parcial') == true))) {
          $derechos = $derechos + $datos[$i]->amount;
          $cont_derechos = $cont_derechos + 1;
        } elseif (in_array($datos[$i]->tdtype_id, [10]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion_titulo') == true || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == true))) {
          $gestion_titulo = $gestion_titulo + $datos[$i]->amount;
        } elseif (in_array($datos[$i]->tdtype_id, [11, 16]) || (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'certificado') == true))) {
          $certificado_estudio = $certificado_estudio + $datos[$i]->amount;
        } elseif (in_array($datos[$i]->tdtype_id, [12, 13, 14]) || (in_array($datos[$i]->tdtype_id, [19]) && strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == true)) {
          $defensa_mesa = $defensa_mesa + $datos[$i]->amount;
        } elseif (in_array($datos[$i]->tdtype_id, [18])) {
          $productos = $productos + $datos[$i]->amount;
        } elseif (in_array($datos[$i]->tdtype_id, [19]) && (strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'mesa') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestion_titulo') == false || strstr(mb_strtolower($datos[$i]->tickdet_desciption), 'gestión de titulo') == false)) {
          // dd($datos[$i]);
          $gestiones_query = DB::select("select * from operations o where o.oper_description = '" . $datos[$i]->tickdet_desciption . "' limit 1");
          if (count($gestiones_query) === 0) {
            if (!array_key_exists(mb_strtolower(trim($datos[$i]->tickdet_desciption)), $array_gestiones)) {
              $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $datos[$i]->amount;
            } else {
              $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] = $array_gestiones[mb_strtolower(trim($datos[$i]->tickdet_desciption))] +  $datos[$i]->amount;
            }
          } else {
            if (!array_key_exists(mb_strtolower(trim($gestiones_query[0]->oper_description)), $array_gestiones)) {
              $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $datos[$i]->amount;
            } else {
              $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] = $array_gestiones[mb_strtolower(trim($gestiones_query[0]->oper_description))] +  $datos[$i]->amount;
            }
          }



          // dd($array_gestiones);
          // $gestiones = $gestiones + $datos[$i]->tickdet_monto;
        } else {
          // dd($datos[$i]);
          $otros = $otros + $datos[$i]->amount;
        }
      }

      $totales = [
        'cuotas' => $cuotas,
        'moras' => $moras,
        'derechos' => $derechos,
        'gestion_titulo' => $gestion_titulo,
        'certificado_estudio' => $certificado_estudio,
        'defensa_mesa' => $defensa_mesa,
        'productos' => $productos,
        'gestiones' => $gestiones,
        'otros' => $otros,
      ];
      $totales = array_merge($totales, $array_gestiones);

      arsort($totales);
      // dd($totales);
      // dd('retorna desde linea 615');

      return $totales;
    } else {
      // dd($request->pay_type_id);
      if (isset($request->pay_type_id)) {
        $payment = $request->pay_type_id;
      }
      $filter = [];
      $filter = ['persons' => $request->person_id, 'pay_type_id' => $payment, 'tills' => $request->tills, 'description' => $request->description];
      $totales = ['salarios' => 0, 'proveedores' => 0];
      $filters = new TillDetailsAdvancesFilters;
      $applied_filter = $filters->applyFilter($filter);
      $datos = "select
      distinct on (tills_details.dettills_id) tills_details.dettills_id,
      alumno.person_idnumber,
      tills_details.dettills_type,
      CONCAT(alumno.person_fname, ' ', alumno.person_lastname) AS student_name,
      CONCAT(cajero.person_fname, ' ', cajero.person_lastname) AS cajero_name,
      get_semester_from_ticket(tickets.ticket_id) AS sems_name,
      tills_details.date,
      ticket_details.tickdet_desciption,
      tickets.ticket_number,
      tills_details.created_at,
      tills_details.description,
      tills_details.amount,
      payment_type.pay_type_desc,
      payment_type.pay_type_id,
      tills.till_name,
      units.unit_code,
      ticket_details_types.tdtype_desc,
      ticket_details_types.tdtype_id
    from
      tills_details
      inner join tills on tills.till_id = tills_details.till_id
      inner join units on units.unit_id = tills.unit_id
      inner join dettill_proofpayment on dettill_proofpayment.dettills_id = tills_details.dettills_id
      inner join proof_payment on proof_payment.pr_payment_id = dettill_proofpayment.pr_payment_id
      inner join payment_type on payment_type.pay_type_id = proof_payment.pay_type_id
      inner join tickets on tickets.ticket_id = tills_details.factura_id
      inner join ticket_details on ticket_details.ticket_id = tickets.ticket_id
      inner join ticket_details_types on ticket_details_types.tdtype_id = ticket_details.tdtype_id
      inner join only persons as alumno on alumno.person_id = tickets.person_id
      inner join only persons as cajero on cajero.person_id = tills_details.person_id
      where
        tills_details.created_at::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
        and (tills_details.description not ilike '%Apertura%' and tills_details.description not ilike '%cierre de caja%')
        and tills_details.dettills_type = true
        and tills_details.deleted_at is null " . $applied_filter . "
       ";

      // dd($datos);


      $query = $datos;
      //faltaria agregar un limite de todo
      $offset = 0;
      if (isset($request->index)) {
        // dd("$request->index = true");
        if ($request->index == 0) {
          $datos = DB::select($query);
          // dd($datos);
          $len_j = count($datos);
          for ($j = 0; $j < $len_j; $j++) {
            if (in_array($datos[$j]->tdtype_id, [1, 17]) && strstr($datos[$j]->tickdet_desciption, 'Mora') == false) {
              $cuotas = $cuotas + $datos[$j]->amount;
              $cont_cuotas = $cont_cuotas + 1;
            } elseif (in_array($datos[$j]->tdtype_id, [1, 17]) && strstr($datos[$j]->tickdet_desciption, 'Mora') == true) {
              $moras = $moras + $datos[$j]->amount;
            } elseif (in_array($datos[$j]->tdtype_id, [2, 3, 4, 5, 6, 7, 8, 9, 15])) {
              $derechos = $derechos + $datos[$j]->amount;
              $cont_derechos = $cont_derechos + 1;
            } elseif (in_array($datos[$j]->tdtype_id, [10])) {
              $gestion_titulo = $gestion_titulo + $datos[$j]->amount;
            } elseif (in_array($datos[$j]->tdtype_id, [11, 16])) {
              $certificado_estudio = $certificado_estudio + $datos[$j]->amount;
            } elseif (in_array($datos[$j]->tdtype_id, [12, 13, 14]) || (in_array($datos[$j]->tdtype_id, [19]) && strstr(strtolower($datos[$j]->tickdet_desciption), 'mesa') == true)) {
              $defensa_mesa = $defensa_mesa + $datos[$j]->amount;
            } elseif (in_array($datos[$j]->tdtype_id, [18])) {
              $productos = $productos + $datos[$j]->amount;
            } elseif (in_array($datos[$j]->tdtype_id, [19]) && strstr(strtolower($datos[$j]->tickdet_desciption), 'mesa') == false) {
              $gestiones = $gestiones + $datos[$j]->amount;
            } else {
              $otros = $otros + $datos[$j]->amount;
            }
          }
          $totales = [
            'cuotas' => $cuotas,
            'moras' => $moras,
            'derechos' => $derechos,
            'gestion_titulo' => $gestion_titulo,
            'certificado_estudio' => $certificado_estudio,
            'defensa_mesa' => $defensa_mesa,
            'productos' => $productos,
            'gestiones' => $gestiones,
            'otros' => $otros,
          ];
          // dd(['datos' => $pag_res, 'totales' => $totales]);
          //incomes_totales.blade.php(resumen)

          return [
            // 'datos' => $pag_res,
            'totales' => $totales
          ];
        } else {
          //incomes.blade.php (por partes)
          $offset = 500 * ($request->index - 1);
          $pag_res = DB::select($query . " limit 500 offset " . $offset);

          return [
            'datos' => $pag_res,
            'part' => $request->index
            // 'totales' => $totales
          ];
        }

        // $datos = $pag_res;
        // $len_j = count($datos);
        // for ($j = 0; $j < $len_j; $j++) {
        //   if (strstr($datos[$j]->tickdet_desciption, 'Cuota') && strstr($datos[$j]->tickdet_desciption, 'Mora') == false) {
        //     $cuotas = $cuotas + $datos[$j]->tickdet_monto;
        //   } elseif (strstr($datos[$j]->tickdet_desciption, 'Mora')) {
        //     $moras = $moras + $datos[$j]->tickdet_monto;
        //   } elseif (strstr($datos[$j]->tickdet_desciption, 'OPORTUNIDAD')) {
        //     $derechos = $derechos + $datos[$j]->tickdet_monto;
        //   } else {
        //     $aranceles = $aranceles + $datos[$j]->tickdet_monto;
        //   }
        // }
        // dd($pag_res);
      } else {
        $datos = DB::select($query);
        $len_j = count($datos);
        for ($j = 0; $j < $len_j; $j++) {
          if (in_array($datos[$j]->tdtype_id, [1, 17]) && strstr($datos[$j]->tickdet_desciption, 'Mora') == false) {
            $cuotas = $cuotas + $datos[$j]->amount;
            $cont_cuotas = $cont_cuotas + 1;
          } elseif (in_array($datos[$j]->tdtype_id, [1, 17]) && strstr($datos[$j]->tickdet_desciption, 'Mora') == true) {
            $moras = $moras + $datos[$j]->amount;
          } elseif (in_array($datos[$j]->tdtype_id, [2, 3, 4, 5, 6, 7, 8, 9, 15])) {
            $derechos = $derechos + $datos[$j]->amount;
            $cont_derechos = $cont_derechos + 1;
          } elseif (in_array($datos[$j]->tdtype_id, [10])) {
            $gestion_titulo = $gestion_titulo + $datos[$j]->amount;
          } elseif (in_array($datos[$j]->tdtype_id, [11, 16])) {
            $certificado_estudio = $certificado_estudio + $datos[$j]->amount;
          } elseif (in_array($datos[$j]->tdtype_id, [12, 13, 14]) || (in_array($datos[$j]->tdtype_id, [19]) && strstr(strtolower($datos[$j]->tickdet_desciption), 'mesa') == true)) {
            $defensa_mesa = $defensa_mesa + $datos[$j]->amount;
          } elseif (in_array($datos[$j]->tdtype_id, [18])) {
            $productos = $productos + $datos[$j]->amount;
          } elseif (in_array($datos[$j]->tdtype_id, [19]) && strstr(strtolower($datos[$j]->tickdet_desciption), 'mesa') == false) {
            $gestiones = $gestiones + $datos[$j]->amount;
          } else {
            $otros = $otros + $datos[$j]->amount;
          }
        }
        $totales = [
          'cuotas' => $cuotas,
          'moras' => $moras,
          'derechos' => $derechos,
          'gestion_titulo' => $gestion_titulo,
          'certificado_estudio' => $certificado_estudio,
          'defensa_mesa' => $defensa_mesa,
          'productos' => $productos,
          'gestiones' => $gestiones,
          'otros' => $otros,
        ];
        // dd(['datos' => $pag_res, 'totales' => $totales]);

        //incomes_with_totales.blade.php (lista de ingresos con totales)

        return [
          'datos' => $datos,
          'totales' => $totales
        ];
      }
      // $len_h = intval(($count[0]->count / 1000)+1);
      //
      // $pag_res = [];
      // // dd($len_h);
      // for ($h=0; $h < $len_h; $h++) {
      //   $array_temp = DB::select($query." limit 1000 offset ".$offset);
      //   $pag_res[] = $array_temp;
      //   $offset = $offset+count($array_temp);
      // }
      // $len_i = count($pag_res);
      // // dd($offset);
      // $cuotas = 0;
      // $moras = 0;
      // $derechos = 0;
      // $aranceles = 0;
      // for ($i=0; $i < $len_i; $i++) {
      //   $datos = $pag_res[$i];
      //   $len_j = count($datos);
      //   for ($j = 0; $j < $len_j; $j++) {
      //     if (strstr($datos[$j]->tickdet_desciption, 'Cuota') && strstr($datos[$j]->tickdet_desciption, 'Mora') == false) {
      //       $cuotas = $cuotas + $datos[$j]->tickdet_monto;
      //     } elseif (strstr($datos[$j]->tickdet_desciption, 'Mora')) {
      //       $moras = $moras + $datos[$j]->tickdet_monto;
      //     } elseif (strstr($datos[$j]->tickdet_desciption, 'OPORTUNIDAD')) {
      //       $derechos = $derechos + $datos[$j]->tickdet_monto;
      //     } else {
      //       $aranceles = $aranceles + $datos[$j]->tickdet_monto;
      //     }
      //   }
      // }
      // $datos = DB::select($query." limit ");
      // $count[0]->count
      // $len_j = count($datos);

      // // dd($datos);
      // $totales = [
      //   'cuotas' => $cuotas,
      //   'moras' => $moras,
      //   'derechos' => $derechos,
      //   'aranceles' => $aranceles,
      // ];
      // dd(['datos' => $pag_res, 'totales' => $totales]);

    }

    // else {
    //   $filter = [];
    //   $filter = ['persons' => $request->person_id, 'pay_type_id' => $payment, 'tills' => $request->tills, 'description' => $request->description];
    //   $totales = ['salarios' => 0, 'proveedores' => 0];
    //   $filters = new TillDetailsAdvancesFilters;
    //   $applied_filter = $filters->applyFilter($filter);
    //   $datos = "select
    //   distinct on (tills_details.dettills_id) tills_details.dettills_id,
    //   alumno.person_idnumber,
    //   tills_details.dettills_type,
    //   CONCAT(alumno.person_fname, ' ', alumno.person_lastname) AS student_name,
    //   CONCAT(cajero.person_fname, ' ', cajero.person_lastname) AS cajero_name,
    //   get_semester_from_ticket(tickets.ticket_id) AS sems_name,
    //   tills_details.date,
    //   ticket_details.tickdet_desciption,
    //   tickets.ticket_number,
    //   tills_details.created_at,
    //   tills_details.description,
    //   tills_details.amount,
    //   payment_type.pay_type_desc,
    //   payment_type.pay_type_id,
    //   tills.till_name,
    //   units.unit_code,
    //   ticket_details_types.tdtype_desc,
    //   ticket_details_types.tdtype_id
    // from
    //   tills_details
    //   inner join tills on tills.till_id = tills_details.till_id
    //   inner join units on units.unit_id = tills.unit_id
    //   inner join dettill_proofpayment on dettill_proofpayment.dettills_id = tills_details.dettills_id
    //   inner join proof_payment on proof_payment.pr_payment_id = dettill_proofpayment.pr_payment_id
    //   inner join payment_type on payment_type.pay_type_id = proof_payment.pay_type_id
    //   inner join tickets on tickets.ticket_id = tills_details.factura_id
    //   inner join ticket_details on ticket_details.ticket_id = tickets.ticket_id
    //   inner join ticket_details_types on ticket_details_types.tdtype_id = ticket_details.tdtype_id
    //   inner join persons as alumno on alumno.person_id = tickets.person_id
    //   inner join persons as cajero on cajero.person_id = tills_details.person_id
    //   where
    //     tills_details.created_at::date between '" . $request->from_date . "'::date and '" . $request->to_date . "'::date
    //     and (tills_details.description not ilike '%Apertura%' and tills_details.description not ilike '%cierre de caja%')
    //     and tills_details.dettills_type = true
    //     and tills_details.deleted_at is null " . $applied_filter;





  }

  public function generalTillsReport(Request $request)
  {

    $till_name = '';
    $total_ingreso = 0;
    $total_egreso = 0;
    $aranceles = 0;
    $transferencia = 0;
    $ingreso = 0;
    $apertura = 0;
    $docente = 0;
    $proveedor = 0;
    $funcionario = 0;
    $egre_date_condition = "";
    $date_condition = "";
    $hour_condition = "";
    $egre_hour_condition = "";
    $transfer_condition = "";
    $year = date("Y");
    $month = date("m");
    $day = date("d");
    $fecha = $year . "-" . $month . "-" . $day;
    $out = explode(",", $request->tills);
    // $till = Till::where('till_id', $id)->first();
    // $tillOpen = TillDetail::where([
    //   ['till_id', $id],
    //   ['tills_details.description', 'Apertura de caja']
    // ])
    //   ->orderBy('created_at', 'desc')->first();
    // $tillClose = TillDetail::where([
    //   ['till_id', $id],
    //   ['tills_details.description', 'Cierre de caja']
    // ])
    //   ->orderBy('created_at', 'desc')->first();
    // $till_name = $till->till_name;
    // $dato = collect(\DB::select("select now() as ahora"))->first();
    // $user = TillDetail::join('persons', 'persons.person_id', 'tills_details.person_id')
    //   ->where('tills_details.till_id', $id)
    //   ->select(DB::raw(
    //     "CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS cajero_name"
    //   ))->first();
    // $cajero_name = $user->cajero_name;

    // dd(count($out));
    // dd($request->tills);

    if (count($out) > 1) {
      // dump(count($out));
      $transfer_condition = "and sa.description != 'Transferencia por cierre de caja'";
    } else {
      // dump(count($out));
      $transfer_condition = "";
    }
    // dump($transfer_condition);
    if ($request->date_filter == 'true') {
      $date_condition = "sa.created_at::date between '" . $request->start_date . "'::date and '" . $request->end_date . "'::date ";
      $egre_date_condition = "sa.td_created_at::date between '" . $request->start_date . "'::date and '" . $request->end_date . "'::date ";

      $payments = DB::table('payment_type')->select(
        '*',
        DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $request->start_date . "' , '" .  $request->end_date . "','{" . $request->tills . "}', true) as amount_income"),
        DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $request->start_date . "' , '" .  $request->end_date . "','{" . $request->tills . "}', false) as amount_expenses"),
        DB::raw("get_qty_payment(payment_type.pay_type_id, '" . $request->start_date . "' , '" .  $request->end_date . "','{" . $request->tills . "}', true) as qty_income"),
        DB::raw("get_qty_payment(payment_type.pay_type_id, '" . $request->start_date . "' , '" .  $request->end_date . "','{" . $request->tills . "}', false) as qty_expenses")
      )
        ->where('payment_type.deleted_at', null)
        ->get();

      if ($request->turn != 'Ninguno') {
        $hr_start = 0;
        $hr_end = 0;
        if ($request->turn == "Mañana") {
          $hr_start = 7;
          $hr_end = 12;
          $hour_condition = "and(extract('hour' from sa.created_at) between '" . $hr_start . "' and '" . $hr_end . "')";
          $egre_hour_condition = "and(extract('hour' from sa.td_created_at) between '" . $hr_start . "' and '" . $hr_end . "')";
        } else if ($request->turn == "Tarde") {
          $hr_start = 14;
          $hr_end = 23;
          $hour_condition = "and(extract('hour' from sa.created_at) between '" . $hr_start . "' and '" . $hr_end . "')";
          $egre_hour_condition = "and(extract('hour' from sa.td_created_at) between '" . $hr_start . "' and '" . $hr_end . "')";
        }
      } else {
        $hour_condition = "";
        $egre_hour_condition = "";
      }
    } else {
      $date_condition = "sa.created_at between (select td2.created_at from tills_details td2
      where td2.till_id = sa.till_id and td2.description = 'Apertura de caja'
      order by td2.created_at desc limit 1) and now()";

      $egre_date_condition = "sa.td_created_at between (select td2.created_at from tills_details td2
      where td2.till_id = sa.till_id and td2.description = 'Apertura de caja'
      order by td2.created_at desc limit 1) and now()";
      $payments = DB::table('payment_type')->select(
        '*',
        DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $fecha . "' , '{" . $request->tills . "}', true) as amount_income"),
        DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $fecha . "' , '{" . $request->tills . "}', false) as amount_expenses"),
        DB::raw("get_qty_payment(payment_type.pay_type_id, '" . $fecha . "' , '{" . $request->tills . "}', true) as qty_income"),
        DB::raw("get_qty_payment(payment_type.pay_type_id, '" . $fecha . "' , '{" . $request->tills . "}', false) as qty_expenses")
      )
        ->where('payment_type.deleted_at', null)
        ->distinct('payment_type.pay_type_id')
        ->get();
    }
    $datos2 = DB::select("select
      distinct on (sa.dettills_id) sa.dettills_id,
      sa.dettills_type,
      CONCAT(alumno.person_fname, ' ', alumno.person_lastname) AS student_name,
      CONCAT(cajero.person_fname, ' ', cajero.person_lastname) AS cajero_name,
      get_semester_from_ticket(tickets.ticket_id) AS sems_name,
      sa.date,
      tickets.ticket_number,
      sa.created_at,
      sa.description,
      sa.amount,
      payment_type.pay_type_desc,
      payment_type.pay_type_id,
      tills.till_name,
      units.unit_code,
      ticket_details_types.tdtype_desc
    from
      tills_details sa
      inner join tills on tills.till_id = sa.till_id
      inner join units on units.unit_id = tills.unit_id
      inner join dettill_proofpayment on dettill_proofpayment.dettills_id = sa.dettills_id
      inner join proof_payment on proof_payment.pr_payment_id = dettill_proofpayment.pr_payment_id
      inner join payment_type on payment_type.pay_type_id = proof_payment.pay_type_id
      inner join tickets on tickets.ticket_id = sa.factura_id
      inner join ticket_details on ticket_details.ticket_id = tickets.ticket_id
      inner join ticket_details_types on ticket_details_types.tdtype_id = ticket_details.tdtype_id
      inner join persons as alumno on alumno.person_id = tickets.person_id
      inner join persons as cajero on cajero.person_id = sa.person_id
      where
      sa.till_id in (" . $request->tills . ")
      and (
        " . $date_condition . "
      )
      and sa.dettills_type = true
      and (sa.description != 'Apertura de caja' and sa.description not ilike '%Cierre de caja para%'
     " . $transfer_condition . "
      )
      " . $hour_condition . "
        and sa.deleted_at is null
        and tickets.ticket_status != 'Anulado'
    group by
      tickets.ticket_id,
      tickets.ticket_number,
      ticket_details_types.tdtype_desc,
      sa.dettills_id,
      sa.dettills_type,
      alumno.person_fname,
      alumno.person_lastname,
      cajero.person_fname,
      cajero.person_lastname,
      sa.date,
      sa.description,
      sa.amount,
      payment_type.pay_type_id,
      payment_type.pay_type_desc,
      tills.till_name,
      units.unit_code
    order by
      sa.dettills_id asc,
      sa.created_at asc");
    // dd($datos2);
    $egre_view = DB::select("select  sa.*  from expenses_detail sa
        where till_id = any('{" . $request->tills . "}')
        and(
          " . $egre_date_condition . "
          --sa.td_created_at::date between '" . $request->start_date . "'::date and '" . $request->end_date . "'::date
        )
          " . $egre_hour_condition . "
        and sa.description not ilike '%Cierre de caja%'
        and sa.dettills_type = false and sa.deleted_at is null
     ;");



    foreach ($datos2 as $key => $value) {
      // dd($value->description);
      if ($value->description == 'Transferencia de caja') {
        $transferencia = $transferencia + $value->amount;
      } elseif ($value->description == 'Cobro de aranceles') {
        $aranceles = $aranceles + $value->amount;
      } elseif ($value->description == 'Ingreso de dinero') {
        $ingreso = $ingreso + $value->amount;
      } elseif ($value->description == 'Apertura de caja') {
        $apertura = $apertura + $value->amount;
      }
      if ($value->dettills_type == true and $value->pay_type_id == 1) {
        $total_ingreso = $total_ingreso + $value->amount;
      }
    }
    foreach ($egre_view as $key => $value) {
      if ($value->description == 'Transferencia de caja') {
        $transferencia = $transferencia + $value->amount;
      } elseif ($value->description == 'Cobro de aranceles') {
        $aranceles = $aranceles + $value->amount;
      } elseif ($value->description == 'Ingreso de dinero') {
        $ingreso = $ingreso + $value->amount;
      } elseif ($value->description == 'Apertura de caja') {
        $apertura = $apertura + $value->amount;
      }

      if (!stripos($value->description, 'Cierre de caja')) {
        $total_egreso = $total_egreso + $value->amount;
      }
    }
    // } else {


    // dd($egre_view);





    // $payments = DB::table('payment_type')->select(
    //   '*',
    //   DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $fecha . "','{" . $request->tills . "}', true) as amount_income"),
    //   DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $fecha . "','{" . $request->tills . "}', false) as amount_expenses")
    // )->get();


    // }

    return [
      'ingresos' => $datos2,
      'egresos' => $egre_view,
      'date' => $fecha,
      'payments' => $payments,
      'total_ingreso' => $total_ingreso,
      // 'cajero' => $cajero_name,
      'till_name' => $till_name,
      'total_egreso' => $total_egreso,
      'cobro_aranceles' => $aranceles,
      'ingreso' => $ingreso,
      'apertura' => $apertura,
      'pago_docente' => $docente,
      'pago_funcionario' => $funcionario,
      'pago_proveedor' => $proveedor,
      'transferencia' => $transferencia,
    ];
    // return $this->showAll($datos);
  }

  public function showTills(Request $request, $id)
  {
    $datos = [];
    if ($request->response != 'report') {
      if ($request->filter == 'false') {
        // $datos = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
        //   ->select('tills_details.*', 'tills.till_name')
        //   ->where([['tills_details.till_id', '=', $id]])
        //   ->get();
        // $count = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
        //   ->select('count(tills_details.*)')
        //   ->where([['tills_details.till_id', '=', $id]])
        //   ->get();
        $datos = "select * from (select distinct on(td.dettills_id)td.*, t.till_name,
        case when pa.provacc_description is not null then pa.provacc_description else
        case when ea.emp_acc_desc  is not null then ea.emp_acc_desc  else td.description end end td_descrip,
        case when emp.person_fname is not null then     CONCAT(emp.person_fname, ' ', emp.person_lastname) else '-' end emp_name,
            CONCAT(cajero.person_fname, ' ', cajero.person_lastname) AS cajero_name,
            get_semester_from_ticket(tickets.ticket_id) AS sems_name,
            'pt.pay_type_id',
                dp.dettills_pr_pay_desc, pt.pay_type_desc from tills_details td
                join tills t on t.till_id = td.till_id
                join dettill_proofpayment dp on dp.dettills_id = td.dettills_id
                join proof_payment pp on pp.pr_payment_id  = dp.pr_payment_id
                join tickets on tickets.ticket_id = td.factura_id
                left join provider_accounts pa  on pa.provacc_id = td.ref_id and td.description = 'Pago a proveedor' 
                left join employee_account ea on ea.emp_acc_id = td.ref_id and td.description = 'Pago a funcionario' 
                left join only persons as emp on ((emp.person_id = ea.person_id or emp.person_id = pa.person_id ) and td.dettills_type is false)
                or (emp.person_id = tickets.person_id and td.dettills_type is true)
                join payment_type pt  on pt.pay_type_id  = pp.pay_type_id
                inner join persons as cajero on cajero.person_id = td.person_id
        where td.till_id = " . $id . " and td.deleted_at is null)t";
        $count = DB::select("select count(distinct td.dettills_id) from tills_details td
        join tills t on t.till_id = td.till_id
        join dettill_proofpayment dp on dp.dettills_id = td.dettills_id
        join proof_payment pp on pp.pr_payment_id  = dp.pr_payment_id
        join payment_type pt  on pt.pay_type_id  = pp.pay_type_id
        inner join persons as cajero on cajero.person_id = td.person_id
        where td.till_id = " . $id . " and td.deleted_at is null");
      } else {

        // $year = date("Y");
        // $month = date("m");
        // $day = date("d");
        // $fechas = $year . "-" . $month . "-" . ($day - 7);
        // $datos = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
        //   ->select('tills_details.*', 'tills.till_name')
        //   ->whereBetween('tills_details.date', [$request->from_date, $request->to_date])
        //   ->where(['tills_details.till_id', '=', $id])
        //   ->get();
        $datos = "select * from (select distinct on(td.dettills_id)td.*, t.till_name,
        case when pa.provacc_description is not null then pa.provacc_description else
        case when ea.emp_acc_desc  is not null then ea.emp_acc_desc  else td.description end end td_descrip,
            CONCAT(cajero.person_fname, ' ', cajero.person_lastname) AS cajero_name,
            case when emp.person_fname is not null then     CONCAT(emp.person_fname, ' ', emp.person_lastname) else '-' end emp_name,
            get_semester_from_ticket(tickets.ticket_id) AS sems_name,
            'pt.pay_type_id',
                dp.dettills_pr_pay_desc, pt.pay_type_desc from tills_details td
                join tills t on t.till_id = td.till_id
                join dettill_proofpayment dp on dp.dettills_id = td.dettills_id
                join proof_payment pp on pp.pr_payment_id  = dp.pr_payment_id
                join tickets on tickets.ticket_id = td.factura_id
                left join provider_accounts pa  on pa.provacc_id = td.ref_id and td.description = 'Pago a proveedor' 
                left join employee_account ea on ea.emp_acc_id = td.ref_id and td.description = 'Pago a funcionario' 
                left join only persons as emp on ((emp.person_id = ea.person_id or emp.person_id = pa.person_id ) and td.dettills_type is false)
                or (emp.person_id = tickets.person_id and td.dettills_type is true)
                join payment_type pt  on pt.pay_type_id  = pp.pay_type_id
                inner join persons as cajero on cajero.person_id = td.person_id
            where td.till_id = " . $id . " and td.deleted_at is null
            and td.dettills_type = " . $request->dettills_type . "
            and (td.date between '" . $request->from_date . "' and '" . $request->to_date . "' ))t
            ";
        $count = DB::select("select count(distinct td.dettills_id) from tills_details td
        join tills t on t.till_id = td.till_id
        join dettill_proofpayment dp on dp.dettills_id = td.dettills_id
        join proof_payment pp on pp.pr_payment_id  = dp.pr_payment_id
        join payment_type pt  on pt.pay_type_id  = pp.pay_type_id
        inner join persons as cajero on cajero.person_id = td.person_id
        where td.till_id = " . $id . " and td.deleted_at is null
        and td.dettills_type = " . $request->dettills_type . "
        and (td.date between '" . $request->from_date . "' and '" . $request->to_date . "' )");
      }
      return $this->showAll($datos, $count, 200);
    } else {
      $till_name = '';
      $total_ingreso = 0;
      $total_egreso = 0;
      $aranceles = 0;
      $transferenciaIn = 0;
      $transferenciaE = 0;
      $ingreso = 0;
      $apertura = 0;
      $docente = 0;
      $proveedor = 0;
      $funcionario = 0;
      $primera = 0;
      $segunda = 0;
      $tercera = 0;
      $parciales = 0;
      $recuperatorio = 0;
      $cuotas = 0;
      $extraordinario = 0;

      $year = date("Y");
      $month = date("m");
      $day = date("d");
      $fecha = $year . "-" . $month . "-" . $day;

      $till = Till::where('till_id', $id)->first();
      $tillOpen = TillDetail::where([
        ['till_id', $id],
        ['tills_details.description', 'Apertura de caja']
      ])
        ->orderBy('created_at', 'desc')->first();
      $tillClose = TillDetail::where([
        ['till_id', $id],
        ['tills_details.description', 'ilike', '%Cierre de caja para%']
      ])
        ->orderBy('created_at', 'desc')->first();
      $till_name = $till->till_name;
      $dato = collect(\DB::select("select now() as ahora"))->first();
      $user = TillDetail::join('persons', 'persons.person_id', 'tills_details.person_id')
        ->where('tills_details.till_id', $id)
        ->select(DB::raw(
          "CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS cajero_name"
        ))->first();
      $cajero_name = $user->cajero_name;
      $datos = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
        ->join('units', 'units.unit_id', '=', 'tills.unit_id')
        ->join('dettill_proofpayment', 'dettill_proofpayment.dettills_id', '=', 'tills_details.dettills_id')
        ->join('proof_payment', 'proof_payment.pr_payment_id', '=', 'dettill_proofpayment.pr_payment_id')
        ->join('payment_type', 'payment_type.pay_type_id', '=', 'proof_payment.pay_type_id')
        ->join('tickets', 'tickets.ticket_id', '=', 'tills_details.factura_id')
        ->join('ticket_details', 'ticket_details.ticket_id', '=', 'tills_details.factura_id')
        ->join('ticket_details_types', 'ticket_details_types.tdtype_id', '=', 'ticket_details.tdtype_id')
        ->join('persons as alumno', 'alumno.person_id', '=', 'tickets.person_id')
        ->select(
          'tills_details.dettills_id',
          'tills_details.dettills_type',
          DB::raw(
            "CONCAT(alumno.person_fname, ' ' ,alumno.person_lastname) AS student_name"
          ),
          DB::raw("get_semester_from_ticket(tickets.ticket_id) AS sems_name"),
          'alumno.person_fname',
          'alumno.person_lastname',
          'tickets.ticket_number',
          'ticket_details_types.tdtype_desc',
          'tills_details.date',
          'tills_details.created_at',
          'tills_details.description',
          'tills_details.amount',
          'payment_type.pay_type_desc',
          'payment_type.pay_type_id',
          'tills.till_name',
          'units.unit_code'
        )
        ->where([
          ['tills_details.till_id', '=', $id],
          ['tills_details.dettills_type',  true],
          ['tills_details.description', 'not like', '%Apertura de caja%'],
          ['tills_details.description', 'not like', '%Cierre de caja para%']
        ])
        ->whereBetween('tills_details.created_at', [$tillOpen->created_at, $tillClose->created_at])
        // ->whereBetween('tills_details.created_at', [$tillOpen->created_at, $dato->ahora])
        ->distinct('tills_details.created_at')
        ->orderBy('tills_details.created_at')
        // ->groupBy(
        //   'tills_details.dettills_id',
        //   'tills_details.dettills_type',
        //   // 'alumno.person_fname',
        //   // 'alumno.person_lastname',
        //   'tills_details.date',
        //   'tills_details.description',
        //   'tills_details.amount',
        //   'payment_type.pay_type_desc',
        //   'tills.till_name',
        //   'units.unit_code'
        // )
        ->get();

      $datos2 = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
        ->join('units', 'units.unit_id', '=', 'tills.unit_id')
        ->join('dettill_proofpayment', 'dettill_proofpayment.dettills_id', '=', 'tills_details.dettills_id')
        ->join('proof_payment', 'proof_payment.pr_payment_id', '=', 'dettill_proofpayment.pr_payment_id')
        ->join('payment_type', 'payment_type.pay_type_id', '=', 'proof_payment.pay_type_id')
        ->join('tickets', 'tickets.ticket_id', '=', 'tills_details.factura_id')
        ->join('ticket_details', 'ticket_details.ticket_id', '=', 'tills_details.factura_id')
        ->join('ticket_details_types', 'ticket_details_types.tdtype_id', '=', 'ticket_details.tdtype_id')
        ->join('persons as alumno', 'alumno.person_id', '=', 'tickets.person_id')
        ->select(
          'tills_details.dettills_id',
          'tills_details.dettills_type',
          DB::raw(
            "CONCAT(alumno.person_fname, ' ' ,alumno.person_lastname) AS student_name"
          ),
          'alumno.person_fname',
          'alumno.person_lastname',
          'tickets.ticket_number',
          'ticket_details_types.tdtype_desc',
          'tills_details.date',
          'tills_details.created_at',
          'tills_details.description',
          'tills_details.amount',
          'payment_type.pay_type_desc',
          'payment_type.pay_type_id',
          'tills.till_name',
          'units.unit_code'
        )
        ->where([
          ['tills_details.till_id', '=', $id],
          ['tills_details.dettills_type',  false],
          ['tills_details.description', 'not like', '%Apertura de caja%'],
          ['tills_details.description', 'not like', '%Cierre de caja%']
        ])
        ->whereBetween('tills_details.created_at', [$tillOpen->created_at, $tillClose->created_at])
        // ->whereBetween('tills_details.created_at', [$tillOpen->created_at, $dato->ahora])
        ->distinct('tills_details.created_at')
        ->orderBy('tills_details.created_at')
        // ->groupBy(
        //   'tills_details.dettills_id',
        //   'tills_details.dettills_type',
        //   // 'alumno.person_fname',
        //   // 'alumno.person_lastname',
        //   'tills_details.date',
        //   'tills_details.description',
        //   'tills_details.amount',
        //   'payment_type.pay_type_desc',
        //   'tills.till_name',
        //   'units.unit_code'
        // )
        ->get();

      // dump($tillClose->created_at);
      // dd($tillOpen->created_at);

      $paymentsIn = DB::table('payment_type')->select(
        '*',
        DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $tillOpen->created_at . "' , '{" . $id . "}', true) as amount_income"),
        DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $tillOpen->created_at . "' , '{" . $id . "}', false) as amount_expenses"),
        DB::raw("get_qty_payment(payment_type.pay_type_id, '" . $tillOpen->created_at . "' , '{" . $id . "}', true) as qty_income"),
        DB::raw("get_qty_payment(payment_type.pay_type_id, '" . $tillOpen->created_at . "' , '{" . $id . "}', false) as qty_expenses")
      )
        ->where('payment_type.deleted_at', null)
        // ->distinct('payment_type.pay_type_id')
        ->get();
      // dump("origen de pago");
      // dd($paymentsIn);

      $paymentsE = DB::table('payment_type')->select(
        '*',
        // DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $tillOpen->created_at . "' , '" . $dato->ahora . "','{" . $id . "}', false) as amount")
        DB::raw("get_amount_payment(payment_type.pay_type_id, '" . $tillOpen->created_at . "' , '" .  $tillClose->created_at . "','{" . $id . "}', false) as amount")
      )
        ->where('payment_type.deleted_at', null)
        ->get();

      foreach ($datos as $key => $value) {
        if ($value['description'] == 'Transferencia de caja') {
          $transferenciaIn = $transferenciaIn + $value['amount'];
        } elseif ($value['description'] == 'Ingreso de dinero') {
          $ingreso = $ingreso + $value['amount'];
        } elseif ($value['description'] == 'Apertura de caja') {
          $apertura = $apertura + $value['amount'];
        } elseif ($value['tdtype_desc'] == 'Examenes Finales primera oportunidad') {
          $primera = $primera + $value['amount'];
        } elseif ($value['tdtype_desc'] == 'Examenes Finales segunda oportunidad') {
          $segunda = $segunda + $value['amount'];
        } elseif ($value['tdtype_desc'] == 'Examenes Finales tercera oportunidad') {
          $tercera = $tercera + $value['amount'];
        } elseif ($value['tdtype_desc'] == 'Pruebas Parciales') {
          $parciales = $parciales + $value['amount'];
        } elseif ($value['tdtype_desc'] == 'Recuperatorio') {
          $recuperatorio = $recuperatorio + $value['amount'];
        } elseif ($value['tdtype_desc'] == 'Cuotas') {
          $cuotas = $cuotas + $value['amount'];
        } elseif ($value['tdtype_desc'] == 'Extraordinario') {
          $extraordinario = $extraordinario + $value['amount'];
        }

        if ($value['dettills_type'] == true && $value['pay_type_id'] == 1  && !stripos($value['description'], 'Cierre de caja')) {
          $total_ingreso = $total_ingreso + $value['amount'];
        }
      }
      foreach ($datos2 as $key => $value) {
        if ($value['description'] == 'Transferencia de caja') {
          $transferenciaE = $transferenciaE + $value['amount'];
        } elseif ($value['description'] == 'Pago a docente') {
          $docente = $docente + $value['amount'];
        } elseif ($value['description'] == 'Pago a proveedor') {
          $proveedor = $proveedor + $value['amount'];
        } elseif ($value['description'] == 'Pago a funcionario') {
          $funcionario = $funcionario + $value['amount'];
        }

        if ($value['dettills_type'] != true && $value['pay_type_id'] == 1 && !stripos($value['description'], 'Cierre de caja')) {
          $total_egreso = $total_egreso + $value['amount'];
        }
      }
      return [
        'report_complete' => $request->report_complete,
        'ingresos' => $datos,
        'egresos' => $datos2,
        'date' => $fecha,
        'total_ingreso' => $total_ingreso,
        'paymentsIn' => $paymentsIn,
        'paymentsE' => $paymentsE,
        'cajero' => $cajero_name,
        'till_name' => $till_name,
        'total_egreso' => $total_egreso,
        'ingreso' => $ingreso,
        'apertura' => $apertura,
        'primera' => $primera,
        'segunda' => $segunda,
        'tercera' => $tercera,
        'parciales' => $parciales,
        'recuperatorio' => $recuperatorio,
        'extraordinario' => $extraordinario,
        'cuotas' => $cuotas,
        'pago_docente' => $docente,
        'pago_funcionario' => $funcionario,
        'pago_proveedor' => $proveedor,
        'transferenciaIn' => $transferenciaIn,
        'transferenciaE' => $transferenciaE
      ];
    }
  }
  public function tillIsStarting()
  {
    //obtener el ultimo registro de caja con el person id = auth()->user()->person_id;
    //si fecha es igual a hoy
    $today = date("Y-m-d");
    $datos2 = DB::select("select t.till_id, t.unit_id, t.till_name, t.updated_at, t.till_starting, t.till_type
      from tills t join tills_details td on td.till_id = t.till_id
      left join (select max(td2.created_at) created_at2, td2.till_id
from tills_details td2 where td2.description  ilike '%cierre de caja%' and td2.description not ilike '%transferencia por cierre%'       group by td2.till_id) td3 on td3.till_id = td.till_id and td3.created_at2 > td.created_at
    where (td.person_id = " . auth()->user()->person_id . " and t.till_starting = true)
    and
     --(td.created_at::date = '" . $today . "') and
     (td.description = 'Apertura de caja')
     and td3.created_at2 is null
     --t.updated_at::date = td.updated_at::date
     order by td.created_at desc limit 1");
    $dato = collect($datos2);

    //  dd("select t.till_id, t.unit_id, t.till_name, t.updated_at, t.till_starting, t.till_type, td.dettills_id
    //    from tills t join tills_details td on td.till_id = t.till_id
    //  left join (select max(td2.created_at) created_at2, td2.till_id
    //  from tills_details td2 where td2.description ilike '%Cierre de caja%'
    // group by td2.till_id) td3 on td3.till_id = td.till_id and td3.created_at2 > td.created_at
    //  where (td.person_id = 0 and t.till_starting = true)
    //  and
    //   --(td.created_at::date = '" . $today . "') and
    //   (td.description = 'Apertura de caja')
    //   and td3.created_at2 is null
    //   --and
    //   --t.updated_at::date = td.updated_at::date
    //   order by td.created_at desc limit 1");

    // dd($dato);

    // $dato = TillDetail::join('tills', 'tills.till_id', '=', 'tills_details.till_id')
    //   ->select('tills.*')
    //   ->where([
    //     ['tills_details.person_id', '=', auth()->user()->person_id],
    //     ['tills.till_starting', '=', true]
    //   ])
    //   // ->whereBetween('tills_details.created_at', [DB::select("select td2.created_at from tills_details td2
    //   // where td2.till_id = tills_details.till_id and td2.description = 'Apertura de caja'
    //   // order by td2.created_at desc limit 1"), DB::select('now()')])
    //   ->orderBy('tills_details.date', 'desc')
    //   ->first();
    // dd($dato);
    if (count($dato) > 0) {
      return response()->json($dato);
    } else {
      return response()->json(false);
    }
  }

  public function process_check(Request $request, $id)
  {
    $fechas =  date("Y-m-d");

    $data = TillDetail::findOrFail($id);
    $data->dettill_status = $request->dettill_status;
    $data->date = $fechas;
    $data->save();
    return response()->json([
      'message' => 'Registro se actualizo con exito!',
      'dato' => $data
    ]);
  }

  public function tillStartingSum($id)
  {
    $year = date("Y");
    $month = date("m");
    $day = date("d");
    $fechas = $year . "-" . ($month) . "-" . $day;

    $dato = collect(\DB::select("select gettillamount(?) as suma ", [$id]))->first();
    return response()->json($dato);
  }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = TillDetail::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Detalle de Caja se restauro con exito!');
    } else {
      $dato = TillDetail::findOrFail($id);
      $request->validate([
        'till_id' => 'required',
        'factura_id' => 'required',
        'description' => 'required',
        'date' => 'required',
        'amount' => 'required',
        'ticket_number' => 'required',
      ]);
      $dato->till_id = $request->till_id;
      $dato->person_id = $request->person_id;
      $dato->factura_id = $request->factura_id;
      $dato->description = $request->description;
      $dato->date = $request->date;
      $dato->amount = $request->amount;

      $dato->save();
      return response()->json([
        'message' => 'Detalle de Caja se actualizo con exito!',
        'dato' => $dato
      ]);
    }
  }

  public function destroy($id)
  {
    $dato = TillDetail::findOrFail($id);
    $dato->delete();
    return response()->json([
      'message' => 'detalle de Caja se elimino con exito!'
    ]);
  }
}
