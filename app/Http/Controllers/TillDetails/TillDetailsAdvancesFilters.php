<?php

namespace App\Http\Controllers\TillDetails;

// use Illuminate\Support\Facades\DB;
/**
 * Se crea una clase aparte para tener mejor orden y entendimiento
 */
class TillDetailsAdvancesFilters
{
  /*
  Filtros para la busqueda avanzada
  Retorna una cadena de caracteres con los filtros de busqueda para poder concatenar con la peticion sql
  */
  public function applyFilter($filters)
  {
    $string_where = "";
    if (array_key_exists("persons", $filters) == true && $filters["persons"] !== null and $filters["persons"] !== 'null') {
      $string_where = " and person_id in( " . $filters["persons"] . ')';
    }
    if (array_key_exists('tills', $filters) == true && $filters["tills"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and till_id in (" .  $filters["tills"] . ")";
      } else {
        $string_where = " and till_id in(" .  $filters["tills"] . ")";
      }
    }
    if (array_key_exists('amount', $filters) == true && $filters["amount"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and amount = " .  $filters["amount"] . "";
      } else {
        $string_where = " and amount = " .  $filters["amount"] . "";
      }
    }
    if (array_key_exists('description', $filters) == true && $filters["description"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and tdtype_desc ilike '%" .  $filters["description"] . "%'";
      } else {
        $string_where = " and tdtype_desc ilike '%" .  $filters["description"] . "%'";
      }
    }
    if (array_key_exists('faculty_id', $filters) == true && $filters["faculty_id"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and (select get_faculty_id_from_ticket(tickets.ticket_id))= " .  $filters["faculty_id"] . "";
      } else {
        $string_where = "and (select get_faculty_id_from_ticket(tickets.ticket_id))= " .  $filters["faculty_id"] . "";
      }
    }
    if (array_key_exists('pay_type_id', $filters) == true && $filters["pay_type_id"] !== null) {
      if ($string_where !== "") {
        $string_where = $string_where . " and payment_type.pay_type_id in (" .  $filters["pay_type_id"] . ")";
      } else {
        $string_where = " and payment_type.pay_type_id in(" .  $filters["pay_type_id"] . ")";
      }
    }
    return ($string_where);
    // return array_key_exists("person_fname", $filters);
  }
}
