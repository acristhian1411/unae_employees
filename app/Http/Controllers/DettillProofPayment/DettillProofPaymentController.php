<?php

namespace App\Http\Controllers\DettillProofPayment;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\DettillProofPayment;
use Illuminate\Support\Facades\DB;
use EloquentBuilder;

class DettillProofPaymentController extends ApiController
{
    public function IfExist(Request $request)
    {
        foreach ($request->proofPayments as $key => $value) {
            $datos = DettillProofPayment::join('proof_payment', 'proof_payment.pr_payment_id', 'dettill_proofpayment.pr_payment_id')
                ->join('tills_details', 'tills_details.dettills_id', 'dettill_proofpayment.dettills_id')
                ->join('tickets', 'tickets.ticket_id', 'tills_details.factura_id')
                ->join('persons', 'persons.person_id', 'tickets.person_id')
                ->where([
                    ['dettill_proofpayment.dettills_pr_pay_desc', $value['dettills_pr_pay_desc']],
                    ['dettill_proofpayment.pr_payment_id', $value['pr_payment_id']],
                    ['proof_payment.pay_type_id', '!=', 1]
                ])
                ->select('dettill_proofpayment.*', DB::raw("CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS person_name"))
                ->first();
            if (!is_null($datos)) {
                return response()->json([
                    'message' => "Ya existe comprobante " . $value['dettills_pr_pay_desc'] . " pertenece a " . $datos->person_name,
                    'status' => true
                ]);
            }
        }
    }

    public function dettill_proff(Request $request)
    {
        $data = DettillProofPayment::join('proof_payment', 'proof_payment.pr_payment_id', 'dettill_proofpayment.pr_payment_id')
            ->join('tills_details', 'tills_details.dettills_id', 'dettill_proofpayment.dettills_id')
            ->join('payment_type', 'payment_type.pay_type_id', 'proof_payment.pay_type_id')
            ->join('tickets', 'tickets.ticket_id', 'tills_details.factura_id')
            ->join('persons', 'persons.person_id', 'tickets.person_id')
            ->where([['dettills_pr_pay_desc', 'not ilike', '%ninguno%'], ['payment_type.pay_type_id', '!=', 1]])
            ->select(
                'payment_type.pay_type_desc',
                'dettill_proofpayment.*',
                'tills_details.amount',
                'tills_details.date',
                DB::raw("CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS person_name")
            )
            ->distinct('dettill_proofpayment.dettills_id')
            ->get();
        if (isset($request->filter) and ($request->filter == 'false' or $request->filter == false)) {

            return $data;
        } else {
            return $this->showAll($data);
        }
    }

    public function search(Request $request)
    {
        $data = EloquentBuilder::setFilterNamespace('App\\EloquentFilters\\DettillProofPayment\\')->to(DettillProofPayment::class, $request->filter)->join('proof_payment', 'proof_payment.pr_payment_id', 'dettill_proofpayment.pr_payment_id')
            ->join('tills_details', 'tills_details.dettills_id', 'dettill_proofpayment.dettills_id')
            ->join('payment_type', 'payment_type.pay_type_id', 'proof_payment.pay_type_id')
            ->join('tickets', 'tickets.ticket_id', 'tills_details.factura_id')
            ->join('persons', 'persons.person_id', 'tickets.person_id')
            ->where('dettills_pr_pay_desc', 'not ilike', '%ninguno%')
            ->select(
                'payment_type.pay_type_desc',
                'dettill_proofpayment.*',
                'tills_details.amount',
                'tills_details.date',
                'persons.person_id',
                DB::raw("CONCAT(persons.person_fname, ' ' ,persons.person_lastname) AS person_name")
            )
            ->distinct('dettill_proofpayment.dettills_id')
            ->get();
        // dd($data[0]->pay_type_desc);
        if (isset($request->filters) and ($request->filters == 'true' or $request->filters == true)) {
            // dd('entra a if para filtro');
            // dd($data);
            // dd($request);
            return [
                'data' => $data,
                'filter' => $request->filter,
                'filter_label' => $request->filter_label
            ];
            // return $data;
        } else {
            // dd('entra a if sin filtro');
            return $this->showAll($data);
        }
        // return $this->showAll($data);
    }
}
