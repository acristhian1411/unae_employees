<?php

namespace App\Http\Controllers\Tills;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Till;
use App\TillsMember;
use Illuminate\Support\Facades\DB;

class TillsController extends ApiController
{
  public function index(Request $req)
  {
    $t = Till::query()->first();
    $query = Till::query();
    $query = $this->filterData($query, $t);
    if ($req->to_start == 'true') {
      $datos = $query
        ->where('tills.till_starting', '!=', true)
        ->select('tills.*', DB::raw("getcajerofromtill(tills.till_id) as cajero_name"), DB::raw("get_myrole_from_tills(tills.till_id, " . auth()->user()->person_id . ") as myrol "))
        ->get();
    } else {
      $datos = $query
        ->select('tills.*', DB::raw("getcajerofromtill(tills.till_id) as cajero_name"), DB::raw("get_myrole_from_tills(tills.till_id, " . auth()->user()->person_id . ") as myrol "))
        ->get();
    }

    return $this->showAll($datos, 200);
  }

  public function store(Request $request)
  {
    $request->validate([
      'unit_id' => 'required',
      'till_name' => 'required',
    ]);
    // dd($request->all());
    $datos = new Till($request->all());
    $datos->save();
    return response()->json('Caja se agrego con exito!');
  }

  public function show($id)
  {
    //$dato = Till::find( $id);
    $today = date("Y-m-d");
    $dato = Till::join('units', 'units.unit_id', '=', 'tills.unit_id')
      ->join('tills_details', 'tills.till_id', '=', 'tills_details.till_id')
      ->select('tills.*', 'units.unit_name', 'tills_details.person_id', DB::raw('getcajerofromtill(tills.till_id) as cajero_name'))
      ->where([['tills_details.description', '=', 'Apertura de caja'], ['tills.till_id', '=', $id], ['tills.till_starting', '=', true]])
      ->orderBy('tills_details.date', 'desc')
      ->first();
    if ($dato == null) {
      $dato = Till::join('units', 'units.unit_id', '=', 'tills.unit_id')
        ->select('tills.*', 'units.unit_name', DB::raw('getcajerofromtill(tills.till_id) as cajero_name'))
        ->where('tills.till_id', '=', $id)
        ->first();
      $dato->person_id = null;
      $dato->same_person = false;
      return $this->showOne($dato, 200);
    } else {
      if ($dato->person_id == auth()->user()->person_id) {
        $dato->same_person = true;
      } else {
        $dato->same_person = false;
      }
      return $this->showOne($dato, 200);
    }
  }

  public function isStarting()
  {
    $dato = Till::where('till_starting', true)->get();
    return $this->showAll($dato, 200);
  }

  public function tillContinue($id)
  {
    $till = DB::update("update tills t set updated_at=now() where t.till_id =" . $id);
    return response()->json([
      'message' => 'Caja se actualizo con exito!',
    ]);
  }
  public function tills_member($id)
  {
    $datos = DB::select(
      "select concat(p.person_fname, ' ', p.person_lastname) person_name, tm.* from tills_members tm 
      join only persons p on p.person_id = tm.person_id
      where tm.deleted_at is null and tm.tills_id =" . $id
    );
    // dd($datos);
    return response()->json($datos);
  }
  public function tills_member_create(Request $request, $id)
  {
    // dd($request);

    $tm_exist = DB::select(
      "select * from tills_members tm where tm.tills_id = " . $id . " and tm.person_id =" . $request->person_id . " limit 1"
    );
    $len_tm_exist = count($tm_exist);
    if ($len_tm_exist > 0) {
      if (is_null($tm_exist[0]->deleted_at)) {
        //es null
        $tm_update = TillsMember::find($tm_exist[0]->tills_member_id);
        $tm_update->tills_role = $request->tills_role;
        $tm_update->expire = $request->expire;
        $tm_update->start_date = $request->start_date;
        $tm_update->expiration_date = $request->expiration_date;
        $tm_update->save();
        return response()->json("guardado con exito");
        // dd($tm_update);
      } else {
        //no es null
        $expire = 'false';
        if ($request->expire) {
          $expire = 'true';
        }
        $tm_update = DB::update("update tills_members tm set deleted_at=null, tills_role='" . $request->tills_role . "', expire=" . $expire . ", start_date='" . $request->start_date . "', expiration_date='" . $request->expiration_date . "', updated_at=now() where tm.tills_member_id =" . $tm_exist[0]->tills_member_id);
        return response()->json("guardado con exito");
        // return response()->json([
        //   'message' => 'Caja se actualizo con exito!',
        // ]);
      }
    } else {
      $tm_create = new TillsMember([
        'tills_id' => $id,
        'person_id' => $request->person_id,
        'tills_role' => $request->tills_role,
        'expire' => $request->expire,
        'start_date' => $request->start_date,
        'expiration_date' => $request->expiration_date,
      ]);
      $tm_create->save();
      return response()->json("guardado con exito");
    }
    // $till = DB::update("update tills t set updated_at=now() where t.till_id =".$id);
    // return response()->json([
    //   'message' => 'Caja se actualizo con exito!',
    // ]);

  }
  public function tills_member_update(Request $request, $id)
  {
    $tm_update = TillsMember::find($id);
    $tm_update->tills_role = $request->tills_role;
    $tm_update->expire = $request->expire;
    $tm_update->start_date = $request->start_date;
    $tm_update->expiration_date = $request->expiration_date;
    $tm_update->save();
    return response()->json("guardado con exito");
  }
  public function tills_member_destroy(Request $request, $id)
  {
    $tm = TillsMember::find($id);
    $tm->delete();
    return response()->json("Se elimino con exito");
  }
  public function tills_current_member(Request $request, $id)
  {
    $person = $request->user()->person_id;
    $datos = DB::select(
      "select concat(p.person_fname, ' ', p.person_lastname) person_name, tm.* from tills_members tm 
      join only persons p on p.person_id = tm.person_id
      where tm.deleted_at is null and tm.tills_id =" . $id .
        " and tm.person_id = " . $person
    );
    return response()->json($datos);
  }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = Till::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Caja se restauro con exito!');
    } else {
      $dato = Till::findOrFail($id);
      $request->validate([
        'unit_id' => 'required',
        'till_name' => 'required',
      ]);
      $dato->unit_id = $request->unit_id;
      $dato->till_name = $request->till_name;
      $dato->till_type = $request->till_type;
      $dato->till_account_number = $request->till_account_number;
      $dato->save();
      return response()->json([
        'message' => 'Caja se actualizo con exito!',
        'dato' => $dato
      ]);
    }
  }

  public function destroy($id)
  {
    $dato = Till::findOrFail($id);
    $dato->delete();
    return response()->json([
      'message' => 'Caja se elimino con exito!'
    ]);
  }
  public function search(Request $req)
  {
    $datos = Till::where([['till_name', 'ilike', '%' . $req->till_name . '%'], ['till_type', '=', $req->till_type]])

      ->get();
    return $this->showAll($datos, 200);
  }
}
