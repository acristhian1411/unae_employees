<?php

namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Tickets\TicketController;
use App\Http\Controllers\StudentMonitoring\MonitoringReportController;
use App\Http\Controllers\Products\ProductController;
use App\Http\Controllers\Enrolleds\EnrolledController;
use App\Http\Controllers\EvalStudent\EvalStudentController;
use App\Http\Controllers\TillDetails\TillDetailsController;
use App\Http\Controllers\Student\StudentController;
use App\Http\Controllers\DocumentPerson\DocumentPersonController;
use App\Http\Controllers\EmployeeAccount\EmployeeAccountController;
use App\Http\Controllers\DettillProofPayment\DettillProofPaymentController;
use App\Http\Middleware\PermissionReportMiddleware;
use App\Http\Controllers\ProfessorAccount\ProfessorAccountController;

class ReportController extends Controller
{
  public function __construct()
  {
    //set_time_limit(-1);
    //ini_set('memory_limit', '-1');
  }
  public function returnReport(Request $request)
  {
    $authorizer = new PermissionReportMiddleware;
    
   if ($request->type == 'income') {
	set_time_limit(1000);
	if ($authorizer->handle($request, "income_report")) {
        $td = new TicketController;
        $data = $td->listTicketDetails($request);
        if (isset($data['totales']) && !isset($data['datos'])) {
          $pdf = \PDF::loadView('incomes_totales', compact('data'));
        } elseif (!isset($data['totales']) && isset($data['datos'])) {
          $pdf = \PDF::loadView('incomes', compact('data'));
        } else {
          $pdf = \PDF::loadView('incomes_with_totales', compact('data'));
        }
        // if ($data) {
        //   // code...
        // }

        // return view('incomes', ['data'=>$data]);
        // return $pdf->download('ingresos');
      } else {
        return view('unauthorized', ['permission_name' => 'income_report']);
      }
    } elseif ($request->type == 's_admin') {
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new MonitoringReportController;
      $data = $p->administrativo($request);
      $pdf = \PDF::loadView('monitoreo_administrativo', compact('data'))->setPaper('legal', 'landscape');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'correcaminata') {
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new MonitoringReportController;
      $data = $p->congress($request);
      $pdf = \PDF::loadView('congress', compact('data'))->setPaper('legal', 'landscape');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 's_congress') {
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new MonitoringReportController;
      $data = $p->congress($request);
      $pdf = \PDF::loadView('congress', compact('data'))->setPaper('legal', 'landscape');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'curdoc') {
      // if ($authorizer->handle($request, "enroll_report")) {
      // dd("en proceso de elaboración");
      $p = new DocumentPersonController;
      $data = $p->getReportFromSemester($request);
      $pdf = \PDF::loadView('curso_documentos', compact('data'))->setPaper('legal', 'landscape');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'stud_sems') {

      // dump("llega al report_controller");f
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new MonitoringReportController;
      $data = $p->curso_estudiantes($request);
      // dd($data);

      $pdf = \PDF::loadView('lista_estudiantes', compact('data'))->setPaper('legal', 'portrait');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'correcaminata1') {

      // dump("llega al report_controller");f
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new MonitoringReportController;
      $data = $p->listaCorrecaminata($request);
      // dd($data);

      $pdf = \PDF::loadView('lista_estudiantes', compact('data'))->setPaper('legal', 'portrait');

      // $pdf = \PDF::loadView('congress', compact('data'))->setPaper('legal', 'landscape');
      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'hpayment') {
      // if ($authorizer->handle($request, "enroll_report")) {
      // dd("se va mostrar un reporte dentro de poco");
      $t = new TicketController;
      $data = $t->showPerson($request, $request->person_id);
      // dd($data);
      $pdf = \PDF::loadView('historial_estudiante', compact('data'))->setPaper('legal', 'portrait');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'evstu') {
      // dump("llega al report_controller");
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new StudentController;
      $data = $p->student_scores($request, $request->person_id, $request->career_id);

      //  dd($data);
      // dd($data["person"]);

      $pdf = \PDF::loadView('calificaciones', compact('data'))->setPaper('legal', 'portrait');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'enroll') {
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new EnrolledController;
      $data = $p->get_document_enroll($request);
      // $data =[];
      // dd($data);
      // dd(array_values(array_filter($data['datos']['retention'], function($v) {return $v->student_ret_quet_id ==7;}))[0]);
      // dd(array_filter($data['datos']['retention'], function($v) {return $v['student_ret_quet_id'] == 7})[0]);
      $pdf = \PDF::loadView('form_inscripcion', compact('data'))->setPaper('legal', 'portrait');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'enroll2') {
      // if ($authorizer->handle($request, "enroll_report")) {
      $p = new EnrolledController;
      $data = $p->get_document_enroll($request);
      // $data =[];
      // dd($data);
      // dd(array_values(array_filter($data['datos']['retention'], function($v) {return $v->student_ret_quet_id ==7;}))[0]);
      // dd(array_filter($data['datos']['retention'], function($v) {return $v['student_ret_quet_id'] == 7})[0]);
      $pdf = \PDF::loadView('form_inscripcion1', compact('data'))->setPaper('legal', 'portrait');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'curso-eval') {
	    // if ($authorizer->handle($request, "enroll_report")) {
	    set_time_limit(25000); 
      $p = new EvalStudentController;
      $data = $p->getPlanilla($request);
      // $data =[];
      // dd($data);
      // dd(array_values(array_filter($data['datos']['retention'], function($v) {return $v->student_ret_quet_id ==7;}))[0]);
      // dd(array_filter($data['datos']['retention'], function($v) {return $v['student_ret_quet_id'] == 7})[0]);
      $pdf = \PDF::loadView('planilla', compact('data'))->setPaper('legal', 'landscape');

      // } else {
      //   return view('unauthorized', ['permission_name' => 'events_report']);
      // }
    } elseif ($request->type == 'event2') {
      if ($authorizer->handle($request, "events_report")) {
        $p = new ProductController;
        $data = $p->get_tdetails_product_report($request);

        // dd($data);

        $pdf = \PDF::loadView('events2', compact('data'))->setPaper('legal', 'landscape');
      } else {
        return view('unauthorized', ['permission_name' => 'events_report']);
      }
    } elseif ($request->type == 'event') {
      if ($authorizer->handle($request, "events_report")) {
        $p = new ProductController;
        $data = $p->get_tdetails_assistances($request);
        $pdf = \PDF::loadView('events', compact('data'))->setPaper('legal', 'landscape');
      } else {
        return view('unauthorized', ['permission_name' => 'events_report']);
      }
    } elseif ($request->type == 'expense') {
      if ($authorizer->handle($request, "expense_report")) {
        set_time_limit(500);
        ini_set('memory_limit', '-1');
        $td = new TillDetailsController;
        $data = $td->listExpenses($request);
        $pdf = \PDF::loadView('expenses', compact('data'));
      } else {
        return view('unauthorized', ['permission_name' => 'expense_report']);
      }
    } elseif ($request->type == 'ticket') {
      if ($authorizer->handle($request, "ticket_report")) {
        $td = new TicketController;
        $data = $td->getTicket($request);
        // dd($data['datos']);
        $pdf = \PDF::loadView('ticket', compact('data'))->setPaper('a4', 'portrait');
      } else {
        return view('unauthorized', ['permission_name' => 'ticket_report']);
      }
    } elseif ($request->type == 'ticket2') {
      if ($authorizer->handle($request, "ticket_report")) {
        $td = new TicketController;
        $data = $td->getTicket($request);
        // dd($data['datos']);
        // dd("Recibo");
        $pdf = \PDF::loadView('recibo', compact('data'))->setPaper('a4', 'portrait');
      } else {
        return view('unauthorized', ['permission_name' => 'ticket_report']);
      }
    } elseif ($request->type == 'balance') {
      if ($authorizer->handle($request, "balance_report")) {
        $td = new TillDetailsController;
        $data = $td->getBalance($request);
        $pdf = \PDF::loadView('balance', compact('data'));
        // return view('balance', compact('data'));
      } else {
        return view('unauthorized', ['permission_name' => 'balance_report']);
      }
    } elseif ($request->type == 'caja') {
      if ($authorizer->handle($request, "caja_report")) {
        set_time_limit(300);
        $td = new TillDetailsController;
        $data = $td->showTills($request, $request->till);
        $pdf = \PDF::loadView('caja', compact('data'))->setPaper('legal', 'landscape');
        // return view('balance', compact('data'));
      } else {
        return view('unauthorized', ['permission_name' => 'caja_report']);
      }
    } elseif ($request->type == 'caja_agrupado') {
      if ($authorizer->handle($request, "caja_agrupado_report")) {
        ini_set('memory_limit', '-1');
        $td = new TillDetailsController;
        $data = $td->generalTillsReport($request);
        // dd($data);
        $pdf = \PDF::loadView('caja_agrupado', compact('data'))->setPaper('legal', 'landscape');
        // return view('balance', compact('data'));
      } else {
        return view('unauthorized', ['permission_name' => 'caja_agrupado_report']);
      }
    } elseif ($request->type == 'habilitados') {
      if ($authorizer->handle($request, "habilitados_report")) {
        // $td = new TillDetailsController;
        $controller = new EvalStudentController;
        $data = $controller->getListEvalStudent($request);
        // dd($eval_students);

        // $data = "hola";
        $pdf = \PDF::loadView('habilitados', compact('data'));
        // return view('habilitados', compact('data'));
        // dd("tiene permiso para ver lista de habilitados");
      } else {
        return view('unauthorized', ['permission_name' => 'caja_agrupado_report']);
      }
    } elseif ($request->type == 'employees') {
      if ($authorizer->handle($request, "employee_account_report")) {

        $controller = new EmployeeAccountController;
        $data = $controller->search($request);
        // dd($eval_students);
        // dd($data);
        // $data = "hola";
        $pdf = \PDF::loadView('employee_account', compact('data'))->setPaper('legal', 'landscape');
      } else {
        return view('unauthorized', ['permission_name' => 'employee_account_report']);
      }
    } elseif ($request->type == 'professors') {
      if ($authorizer->handle($request, "professor_account_store")) {

        $controller = new ProfessorAccountController;
        $data = $controller->search($request);
        // dd($eval_students);
        // dd($data);
        // $data = "hola";
        $pdf = \PDF::loadView('professor_account', compact('data'))->setPaper('legal', 'landscape');
      } else {
        return view('unauthorized', ['permission_name' => 'professor_account_store']);
      }
    } elseif ($request->type == 'dettill_proof_payment') {
      $controller = new DettillProofPaymentController;
      if ($request->filters == 'true') {
        $data = $controller->search($request);
        // dd($data);
        $pdf = \PDF::loadView('dettill_proof_payment_with_filter', compact('data'));
      } else {
        $data = $controller->dettill_proff($request);
        // dd($data);
        $pdf = \PDF::loadView('dettill_proof_payment', compact('data'));
      }
      // dd($eval_students);
      // dd($data);
      // $data = "hola";
    } else {
      $data = [
        'titulo' => 'Unae'
      ];
      $pdf = \PDF::loadView('pruebaparapdf', compact('data'));
    }
    // dd($data);

    // dd($request);
    // $data = [
    //       'titulo' => 'Unae'
    //   ];
    return $pdf->stream();

    // return view('pruebaparapdf', compact('data'));
  }
}
