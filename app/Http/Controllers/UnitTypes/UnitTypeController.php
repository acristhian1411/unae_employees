<?php

namespace App\Http\Controllers\UnitTypes;


use App\UnitType;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class UnitTypeController extends ApiController
{
  public function index()
  {
      //
      $t = UnitType::query()->first();
      $query = UnitType::query();
      $query = $this->filterData($query, $t);
      $datos = $query->get();

      return $this->showAll($datos,200);
      //return response()->json($units);

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $request->validate([
    'unit_type_description' => 'required',
    ]);

      $dato = new UnitType([
        'unit_type_description' => $request ->get('unit_type_description'),
      ]);


       $dato->save();

      return response()->json('Tipo de unidad academica agregada con exito!');
  }
  public function show($id){
      //
      $unit = UnitType::find($id);
      //return response()->json($unit);
      return $this->showOne($unit,200);
  }

  public function update(Request $request, $id){
    if ($request->audit_restore) {
      $dato = UnitType::find($id);
      $keys = collect($request->all())->keys();
      for ($i=0; $i < count($keys); $i++) {
        $dato[$keys[0]]= $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Tipo de unidad academica se restauro con exito!');
    }
    else{
    $request->validate([
    'unit_type_description' => 'required',
    ]);
    $unit = UnitType::find($id);
    $unit->unit_type_description=$request->get('unit_type_description');
    $unit->save();
    return response()->json('Tipo de unidad academica actualizada con exito!');
  }
}

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\UnitType  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($unit_id)
  {
      $unit = UnitType::find($unit_id);
      $unit->delete();
      return response()->json('Tipo de unidad academica se elimino con exito');
  }
  public function search($request){
      $data = UnitType::where('unit_type_description','ILIKE','%'.$request.'%')
                    ->get();
    return response()->json($data);
  }
}
