<?php

namespace App\Http\Controllers\PaymentType;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\PaymentType;
use App\ProofPayment;

class PaymentTypeController extends ApiController
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $t = PaymentType::query()->first();
    $query = PaymentType::query();
    $query = $this->filterData($query, $t);
    $datos = $query->get();
    return $this->showAll($datos, 200);
  }

  public function store(Request $request)
  {
    $request->validate([
      'pay_type_desc' => 'required',
    ]);
    $datos = new PaymentType($request->all());
    $datos->save();
    return response()->json([
      'message' => 'Tipo de pago se agrego correctamente!',
      'dato' => $datos->pay_type_id
    ]);
  }

  public function showWhitProofPayments()
  {
    $dato = PaymentType::all();
    foreach ($dato as $key => $value) {
      $proofPayments = ProofPayment::join('payment_type', 'payment_type.pay_type_id', 'proof_payment.pay_type_id')
        ->where('proof_payment.pay_type_id', '=', $value['pay_type_id'])
        ->get();
      if ($proofPayments->isEmpty()) {
        $dato[$key]->proofPayments = [];
      } else {
        $dato[$key]->proofPayments = $proofPayments;
      }
    }
    return $this->showAll($dato, 200);
  }

  public function show($id)
  {
    //$dato = PaymentType::find( $id);
    $dato = PaymentType::where('pay_type_id', '=', $id)
      ->first();
    return $this->showOne($dato, 200);
  }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = PaymentType::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Tipo de pago se restauro con exito!');
    } else {
      $dato = PaymentType::findOrFail($id);
      $request->validate([
        'pay_type_desc' => 'required',
      ]);
      $dato->pay_type_desc = $request->pay_type_desc;
      $dato->save();
      return response()->json('Tipo de pago se actualizo correctamente!');
    }
  }

  public function destroy($id)
  {
    $dato = PaymentType::findOrFail($id);
    $dato->delete();
    return response()->json([
      'message' => 'Tipo de pago se elimino correctamente!'
    ]);
  }
  public function search($req)
  {
    $datos = PaymentType::where('pay_type_desc', 'ilike', '%' . $req . '%')->get();
    return $this->showAll($datos, 200);
  }
}
