<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use App\Http\Middleware\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * The user has been authenticated.
     *
     * @param  Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json($user);
        // return redirect('/');
    }
    /**
     * The user has logged out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    protected function loggedOut(Request $request)
    {
        // return redirect('/login');
        return response()->json('se cerro la sesion');
    }
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($driver)
    {
      return Socialite::driver($driver)->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        // $user = Socialite::driver('google')->stateless()->user();
        // $user->token;
        // // return response()->json($user);
        // return redirect('/');
        try {
             $user = Socialite::driver('google')->user();
         } catch (\Exception $e) {
             dd('ha ocurrido un error');
         }
         $existingUser = User::where('email', $user->getEmail())->first();
         if ($existingUser) {
             auth()->login($existingUser, true);
         } else {
             // $newUser                    = new User;
             // $newUser->provider_name     = 'google';
             // $newUser->provider_id       = $user->getId();
             // $newUser->name              = $user->getName();
             // $newUser->username          = $user->getName();
             // $newUser->email             = $user->getEmail();
             // $newUser->email_verified_at = now();
             // $newUser->avatar            = $user->getAvatar();
             // $newUser->person_id         = 47;
             // $newUser->save();
             //
             // auth()->login($newUser, true);
             //le debe agregar desde un usuario administrador
             abort(401);
         }

         return redirect('/');
    }
}
