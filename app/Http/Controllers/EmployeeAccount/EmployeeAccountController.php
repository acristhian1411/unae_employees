<?php

namespace App\Http\Controllers\EmployeeAccount;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\EmployeeAccount;
use App\TillDetail;
use App\ProofPayment;
use App\Employee;
use App\DettillProofPayment;
use Illuminate\Support\Facades\DB;

class EmployeeAccountController extends ApiController
{
  public function index()
  {
    //
    $datos = EmployeeAccount::all();
    return $this->showAll($datos, 200);
  }

  public function store(Request $request)
  {

    $dato = new EmployeeAccount([
      'person_id' => $request->get('person_id'),
      'emp_acc_desc' => $request->get('emp_acc_desc'),
      'emp_acc_status' => true,
      'emp_acc_amountpaid' => $request->get('emp_acc_amountpaid'),
      'emp_acc_amount' => $request->get('emp_acc_amountpaid'),
      'emp_acc_month' => $request->get('emp_acc_month'),
      'emp_acc_ticket_number' => $request->get('emp_acc_ticket_number'),
    ]);

    $today = date("Y-m-d");

    $till = DB::select("select td.description, td.date ,t.* from tills_details td join tills t on td.till_id = t.till_id 
    where (t.till_starting = true and td.description = 'Apertura de caja' and td.person_id = " . auth()->user()->person_id . ") 
    and 
    (select count(*) from tills_details td2 join tills t2 on td2.till_id = t2.till_id 
    where td2.description = 'Cierre de caja' and td.till_id = td2.till_id and td.created_at < td2.created_at) = 0");
    if (isset($till[0]->till_id)) {
      $year = date("Y");
      $month = date("m");
      $day = date("d");
      $fechas = $year . "-" . ($month) . "-" . $day;
      $amount = collect(\DB::select("select gettillamount(?) as suma ", [$till[0]->till_id]))->first();
      if ($amount != null) {
        if ($amount->suma >= $request->get('emp_acc_amountpaid')) {
          $saved = $dato->save();
          if ($saved) {
            $details = new TillDetail([
              'till_id' => $till[0]->till_id,
              'factura_id' => 0,
              'description' => 'Pago a funcionario',
              'date' => $today,
              'amount' => $dato->emp_acc_amountpaid,
              'ref_id' => $dato->emp_acc_id,
              'dettills_type' => false,
              'person_id' => auth()->user()->person_id,
            ]);
            $savedDetails = $details->save();
            if ($savedDetails) {
              $pay_type = ProofPayment::where('pay_type_id', 1)->first();
              $proof = new DettillProofPayment();
              $proof->dettills_id = $details['dettills_id'];
              $proof->pr_payment_id = $pay_type['pr_payment_id'];
              $proof->dettills_pr_pay_desc = 'Ninguno';
              $proof->save();
              return response()->json([
                'message' => 'Registro se agrego con exito!',
                'dato' => true
              ]);
            }
          }
        } else {
          return response()->json([
            'message' => 'No hay suficiente dinero en caja',
            'dato' => false
          ]);
        }
      } else {
        return response()->json([
          'message' => 'No hay dinero en caja',
          'dato' => false
        ]);
      }
    } else {

      return response()->json([
        'message' => 'No hay cajas abiertas!',
        'dato' => false
      ]);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\EmployeeAccount  $country
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //

    $dato = EmployeeAccount::join('employees', 'employees.person_id', '=', 'employee_account.person_id')
      ->select('employee_account.*', DB::raw("concat(employees.person_fname,' ',employees.person_lastname) AS employee_name"))
      ->where('emp_acc_id', $id)
      ->first();
    return $this->showOne($dato, 200);
  }

  public function showEmployee($id)
  {
    $dato = EmployeeAccount::where([['person_id', $id], ['emp_acc_status', false]])->get();
    return $this->showAll($dato, 200);
  }


  public function showEmployeesWithSalary(Request $req)
  {
    // dd($req->year . '-' . $req->month);
    // dd($req);
    $till_id = null;
    if (isset($req->till_id)) {
      $till = DB::select("select * from tills t where t.till_id = " . $req->till_id);
      if ($till[0]->till_type == 3) {
        $till_id = $till[0]->till_id;
      }
    }
    // dd($req);

    if ($req->filter_all == 'true') {

      $salarios = DB::select(
        "select e.person_id, e.person_lastname, e.person_fname, e.person_idnumber,e.till_id, e.emplo_salary, e.has_ips, e.porcent_ips, ea3.amount_loan, ea.* from employee_account ea 
      join employees e on e.person_id = ea.person_id 
      left join (select sum(ea2.emp_acc_amount) amount_loan, ea2.person_id from 
      employee_account ea2 where ea2.is_discount is true and 
      deleted_at is null and 
      extract(month from ea2.emp_acc_expiration)=" . $req->month . " and 
      extract(year from ea2.emp_acc_expiration) = " . $req->year . " group by ea2.person_id) ea3 on ea3.person_id = ea.person_id
      where extract(month from ea.emp_acc_expiration)=" . $req->month . " and 
      extract(year from ea.emp_acc_expiration) = " . $req->year . " and 
      ea.is_discount is false and ea.deleted_at is null and 
      e.deleted_at is null and e.emplo_status is true order by e.person_lastname"
      );
    } else {
      if ($till_id != null and $till_id != 0) {

        $salarios = DB::select(
          "select e.person_id, e.person_lastname, e.person_fname, e.person_idnumber,e.till_id, e.emplo_salary, e.has_ips, e.porcent_ips, ea3.amount_loan, ea.* from employee_account ea 
        join employees e on e.person_id = ea.person_id 
        left join (select sum(ea2.emp_acc_amount) amount_loan, ea2.person_id from 
        employee_account ea2 where ea2.is_discount is true and 
        deleted_at is null and 
        extract(month from ea2.emp_acc_expiration)=" . $req->month . " and 
        extract(year from ea2.emp_acc_expiration) = " . $req->year . " group by ea2.person_id) ea3 on ea3.person_id = ea.person_id
        where extract(month from ea.emp_acc_expiration)=" . $req->month . " and 
        extract(year from ea.emp_acc_expiration) = " . $req->year . " and 
        e.till_id = " . $req->till_id . " and 
        ea.is_discount is false and ea.deleted_at is null and 
        e.deleted_at is null and e.emplo_status is true order by e.person_lastname"
        );
      } else {
        $salarios = DB::select(
          "select e.person_id, e.person_lastname, e.person_fname, e.person_idnumber,e.till_id, e.emplo_salary, e.has_ips, e.porcent_ips, ea3.amount_loan,  ea.* from employee_account ea 
        join employees e on e.person_id = ea.person_id 
        left join (select sum(ea2.emp_acc_amount) amount_loan, ea2.person_id from 
        employee_account ea2 where ea2.is_discount is true and 
        deleted_at is null and 
        extract(month from ea2.emp_acc_expiration)=" . $req->month . " and 
        extract(year from ea2.emp_acc_expiration) = " . $req->year . " group by ea2.person_id) ea3 on ea3.person_id = ea.person_id
        where extract(month from ea.emp_acc_expiration)=" . $req->month . " and 
        extract(year from ea.emp_acc_expiration) = " . $req->year . " and 
        e.till_id is null and 
        ea.is_discount is false and ea.deleted_at is null and 
        e.deleted_at is null and e.emplo_status is true order by e.person_lastname"
        );
      }
    }
    // dd($req);
    if ($req->type == 'employees') {
      return ["data" => $salarios];
    } else {
      return response()->json(["data" => $salarios]);
    }


    // $dato = Employee::join('employee_account', 'employee_account.person_id', '=', 'employees.person_id')
    //   ->select(
    //     'employees.person_id',
    //     'employees.person_fname',
    //     'employees.person_lastname',
    //     'employees.person_idnumber',
    //     'employee_account.*'
    //   )
    //   ->where([['employees.emplo_status', true], ['emp_acc_month', 'ilike', $req->year . '-' . $req->month], ['emp_acc_status', false]])
    //   ->get();
    // return $this->showAll($dato, 200);
  }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = EmployeeAccount::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Registro se restauro con exito!');
    } else {
      $today = date("Y-m-d");

      $dato = EmployeeAccount::find($id);
      $dato->emp_acc_ticket_number = $request->get('emp_acc_ticket_number');
      $dato->emp_acc_amountpaid = floatval($request->get('emp_acc_amount_to_paid')) + floatval($dato->emp_acc_amountpaid);
      $dato->emp_acc_datepaid = $today;
      if (intval($request->emp_acc_amount_to_paid) == $request->emp_acc_amountpaid) {
        $dato->emp_acc_status = true;
      } else {
        $dato->emp_acc_status = false;
      }

      $till_id = 0;

      $till = DB::select("select td.description, td.date ,t.* from tills_details td join tills t on td.till_id = t.till_id 
      where (t.till_starting = true and td.description = 'Apertura de caja' and td.person_id = " . auth()->user()->person_id . ") 
      and 
      (select count(*) from tills_details td2 join tills t2 on td2.till_id = t2.till_id 
      where td2.description = 'Cierre de caja' and td.till_id = td2.till_id and td.created_at < td2.created_at) = 0");

      if (isset($request->till_id)) {
        $till_id = $request->till_id;
      } else {
        $till_id = $till[0]->till_id;
      }

      if ($till_id != 0) {
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $fechas = $year . "-" . ($month) . "-" . $day;
        $amount = collect(\DB::select("select gettillamount(?) as suma ", [$till_id]))->first();
        if ($amount != null) {
          if ($amount->suma >= $request->get('emp_acc_amount_to_paid')) {
            $saved = $dato->save();
            if ($saved == true) {
              $details = new TillDetail([
                'till_id' => $till_id,
                'factura_id' => 0,
                'description' => 'Pago a funcionario',
                'date' => $today,
                'amount' => $request->get('emp_acc_amount_to_paid'),
                'ref_id' => $dato->emp_acc_id,
                'dettills_type' => false,
                'person_id' => auth()->user()->person_id,
              ]);
              $savedDetails = $details->save();
              if ($savedDetails) {
                $pay_type = ProofPayment::where('pay_type_id', 1)->first();
                $proof = new DettillProofPayment();
                $proof->dettills_id = $details['dettills_id'];
                $proof->pr_payment_id = $pay_type['pr_payment_id'];
                $proof->dettills_pr_pay_desc = 'Ninguno';
                $proof->save();
                return response()->json([
                  'message' => 'Registro se agrego con exito!',
                  'dato' => true
                ]);
              }
            }
          } else {
            return response()->json([
              'message' => 'No hay suficiente dinero en caja',
              'dato' => false
            ]);
          }
        } else {
          return response()->json([
            'message' => 'No hay dinero en caja',
            'dato' => false
          ]);
        }
      } else {

        return response()->json([
          'message' => 'No hay cajas abiertas!',
          'dato' => false
        ]);
      }
    }
  }

  /**
   * It's a function that updates a record from an array of employee accounts
   * and make a payment or a partial payment if a till is starting and 
   * if there is enough money in it
   * @param Request request The request object.
   */
  public function updateFromArray(Request $request)
  {
    $saldo =  $request->emp_acc_amount_to_paid;
    $amount_pay = $request->emp_acc_amount_to_paid;
    $today = date("Y-m-d");
    foreach ($request->to_pay as $key => $value) {

      $dato = EmployeeAccount::find($value);


      // if (intval($request->emp_acc_amount_to_paid) == $request->emp_acc_amountpaid) {
      //   $dato->emp_acc_status = true;
      // } else {
      //   $dato->emp_acc_status = false;
      // }

      $till_id = 0;

      $till = DB::select("select td.description, td.date ,t.* from tills_details td join tills t on td.till_id = t.till_id 
        where (t.till_starting = true and td.description = 'Apertura de caja' and td.person_id = " . auth()->user()->person_id . ") 
        and 
        (select count(*) from tills_details td2 join tills t2 on td2.till_id = t2.till_id 
        where td2.description = 'Cierre de caja' and td.till_id = td2.till_id and td.created_at < td2.created_at) = 0");

      if (isset($request->till_id)) {
        $till_id = $request->till_id;
      } else {
        $till_id = $till[0]->till_id;
      }

      if ($till_id != 0) {
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $fechas = $year . "-" . ($month) . "-" . $day;
        $amount = collect(\DB::select("select gettillamount(?) as suma ", [$till_id]))->first();
        if ($amount != null) {

          if ($amount->suma >= $saldo) {
            // if()
            $amount_to_pay =  $dato->emp_acc_amount - $dato->emp_acc_amountpaid;

            // $saldo = $saldo - $amount_to_pay;

            $dato->emp_acc_ticket_number = $request->get('emp_acc_ticket_number');

            if (intval($saldo) >= $amount_to_pay) {
              $dato->emp_acc_status = true;
              // $dato->surcharge = intval($datos[$i]['surcharge']) + intval($datos[$i]['get_surcharge']);
              if ($dato->emp_acc_amountpaid == null || $dato->emp_acc_amountpaid == 0) {
                $dato->emp_acc_amountpaid = floatval($amount_to_pay);
                $saldo = floatval($saldo) - (floatval($amount_to_pay));
              } else {
                $saldo = floatval($saldo) - (floatval($amount_to_pay));
                $dato->emp_acc_amountpaid += floatval($amount_to_pay);
              }
            } else {

              $dato->emp_acc_status = false;
              if ($saldo >= (intval($amount_to_pay))) {
                $dato->emp_acc_amountpaid = intval($dato->emp_acc_amountpaid) + intval($amount_to_pay);
                $saldo = intval($saldo) - intval($amount_to_pay);
              } else {
                $dato->emp_acc_amountpaid = intval($dato->emp_acc_amountpaid) + intval($saldo);
                $saldo = 0;
              }
              // $dato->surcharge = intval($dato->surcharge']) + intval($saldo);
              $saldo = 0;
            }
            // $dato->emp_acc_amountpaid = floatval($request->get('emp_acc_amount_to_paid')) + floatval($dato->emp_acc_amountpaid);
            $dato->emp_acc_datepaid = $today;


            if ($amount->suma >= $saldo) {
              // if()
              $amount_to_pay =  $dato->emp_acc_amount - $dato->emp_acc_amountpaid;

              // $saldo = $saldo - $amount_to_pay;

              $dato->emp_acc_ticket_number = $request->get('emp_acc_ticket_number');

              if (intval($saldo) >= $amount_to_pay) {
                $dato->emp_acc_status = true;
                // $dato->surcharge = intval($datos[$i]['surcharge']) + intval($datos[$i]['get_surcharge']);
                if ($dato->emp_acc_amountpaid == null || $dato->emp_acc_amountpaid == 0) {
                  $dato->emp_acc_amountpaid = floatval($amount_to_pay);
                  $saldo = floatval($saldo) - (floatval($amount_to_pay));
                } else {
                  $saldo = floatval($saldo) - (floatval($amount_to_pay));
                  $dato->emp_acc_amountpaid += floatval($amount_to_pay);
                }
              } else {

                $dato->emp_acc_status = false;
                if ($saldo >= (intval($amount_to_pay))) {
                  $dato->emp_acc_amountpaid = intval($dato->emp_acc_amountpaid) + intval($amount_to_pay);
                  $saldo = intval($saldo) - intval($amount_to_pay);
                } else {
                  $dato->emp_acc_amountpaid = intval($dato->emp_acc_amountpaid) + intval($saldo);
                  $saldo = 0;
                }
                // $dato->surcharge = intval($dato->surcharge']) + intval($saldo);
                $saldo = 0;
              }
              // $dato->emp_acc_amountpaid = floatval($request->get('emp_acc_amount_to_paid')) + floatval($dato->emp_acc_amountpaid);
              $dato->emp_acc_datepaid = $today;


              $saved = $dato->save();
              // dd('entra a if de $amount->suma >= $request->get(emp_acc_amount_to_paid');

              // $saved = true;
              if ($saved == true) {
                $details = new TillDetail([
                  'till_id' => $till_id,
                  'factura_id' => 0,
                  'description' => 'Pago a funcionario',
                  'date' => $today,
                  'amount' => $dato->emp_acc_amountpaid,
                  'ref_id' => $dato->emp_acc_id,
                  'dettills_type' => false,
                  'person_id' => auth()->user()->person_id,
                  'dettill_status' => true,
                ]);
                $savedDetails = $details->save();
                // $savedDetails = true;
                if ($savedDetails) {
                  $pay_type = ProofPayment::where('pay_type_id', 1)->first();
                  $proof = new DettillProofPayment();
                  $proof->dettills_id = $details['dettills_id'];
                  $proof->pr_payment_id = $pay_type['pr_payment_id'];
                  $proof->dettills_pr_pay_desc = 'Ninguno';
                  $proof->save();
                  // return response()->json([
                  //   'message' => 'Registro se agrego con exito!',
                  //   'dato' => true
                  // ]);
                }
              }
            } else {
              return response()->json([
                'message' => 'No hay suficiente dinero en caja',
                'dato' => false
              ]);
            }
          } else {
            return response()->json([
              'message' => 'No hay dinero en caja',
              'dato' => false
            ]);
          }
        } else {

          return response()->json([
            'message' => 'No hay cajas abiertas!',
            'dato' => false
          ]);
        }
      }
      return response()->json([
        'message' => 'Registro se agrego con exito!',
        'dato' => true
      ]);
    }

    return response()->json([
      'message' => 'Registro se agrego con exito!',
      'dato' => true
    ]);
  }

  public function masiveUpdateFromArray(Request $request)
  {
    // dump($request->all());
    $validatedData = $request->validate(
      [
        'pay_type_id' => 'required|numeric|not_in:0',
        'comp_det_id' => 'required|numeric|not_in:0',
      ]
    );
    $to_report = [];
    $to_return = false;
    $month = 0;
    $company_detail = 0;
    $saldo =  $request->emp_acc_amountpaid;
    if ($request->pay_type_id) {
      $pay_type_id = $request->pay_type_id;
    } else {
      $pay_type_id = 1;
    }
    if ($request->comp_det_id) {
      $comp_det_id = $request->comp_det_id;
    } else {
      $comp_det_id = 0;
    }
    $company_detail = DB::select('select comp_det_account_number, comp_det_bussiness_name from company_details where comp_det_id = ' . $comp_det_id);
    $amount_pay = 0;
    $today = date("Y-m-d");
    foreach ($request->to_pay as $key => $value) {

      // if (intval($request->emp_acc_amount_to_paid) == $request->emp_acc_amountpaid) {
      //   $dato->emp_acc_status = true;
      // } else {
      //   $dato->emp_acc_status = false;
      // }
      // dump("value");
      // dump(gettype($value));
      // dd($value['emp_acc_id']);
      $dato = EmployeeAccount::find($value['emp_acc_id']);

      // $info_dato =  DB::select(
      //   "select e.person_id, e.person_lastname, e.person_fname, e.person_idnumber, e.emplo_salary, e.has_ips, e.porcent_ips, ea3.amount_loan, ea.* from employee_account ea 
      //   join employees e on e.person_id = ea.person_id 
      //   left join (select sum(ea2.emp_acc_amount) amount_loan, ea2.person_id from employee_account ea2 where ea2.is_discount is true and deleted_at is null and extract(month from ea2.emp_acc_expiration)= extract(month from '".$dato->emp_acc_expiration."'::date) and extract(year from ea2.emp_acc_expiration) = extract(year from '".$dato->emp_acc_expiration."'::date) group by ea2.person_id) ea3 on ea3.person_id = ea.person_id
      //   where ea.emp_acc_id = ".$value['emp_acc_id']." order by e.person_lastname"
      // );
      $value_ips = 0;
      $value_loan = 0;
      //dd($info_dato);
      $till_id = 0;

      $till = DB::select("select td.description, td.date ,t.* from tills_details td join tills t on td.till_id = t.till_id 
        where (t.till_starting = true and td.description = 'Apertura de caja' and td.person_id = " . auth()->user()->person_id . ") 
        and 
        (select count(*) from tills_details td2 join tills t2 on td2.till_id = t2.till_id 
        where td2.description = 'Cierre de caja' and td.till_id = td2.till_id and td.created_at < td2.created_at) = 0");
      // dd($request->till);
      if (isset($request->till_id) and $request->till_id != 0) {
        $till_id = $request->till_id;
      } else {
        $till_id = $till[0]->till_id;
      }

      if ($till_id != 0) {
        // dd('till_id != 0');
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $fechas = $year . "-" . ($month) . "-" . $day;
        $amount = collect(\DB::select("select t.till_name, gettillamount(t.till_id) as suma  from tills t where t.till_id = " . $till_id . ";"))->first();
        // dd($amount->till_name);
        if ($amount != null) {
          if ($amount->suma >= $saldo) {
            // dd($amount_to_pay);  
            if ($value['has_ips']) {
              $value_ips = ((floatval($value['porcent_ips']) * floatval($value['emplo_salary'])) / 100);
            } else {
              $value_ips = 0;
            }
            if (is_null($value['amount_loan'])) {
              $value_loan = 0;
            } else {
              $value_loan = floatval($value['amount_loan']);
            }
            if ($value['extra_hours'] > 0) {
              $dato->extra_hours = $value['extra_hours'];
            }
            $amount_to_pay = floatval($value['emplo_salary']) - floatval($dato->emp_acc_amountpaid) - $value_loan - $value_ips;

            $var = '"' . $value['person_idnumber'] . '","' . $company_detail[0]->comp_det_account_number . '","SUELDO/SAL/' . $request->year . '","' . (intval($amount_to_pay) + intval($value['extra_hours'])) . '.00","NO"';
            if ($amount->till_name == 'CAJA CONTINENTAL') {

              array_push($to_report, $var);
            }
            // dd($to_report);
            // dd($amount_to_pay);
            $saldo = $saldo - $amount_to_pay;
            // $dato->emp_acc_ticket_number = $request->get('emp_acc_ticket_number');

            // if (intval($amount_pay) >= $amount_to_pay) {
            $dato->emp_acc_status = true;
            // $dato->surcharge = intval($datos[$i]['surcharge']) + intval($datos[$i]['get_surcharge']);

            // $amount_pay = floatval($amount_pay) - (floatval($amount_to_pay));
            $dato->emp_acc_amountpaid += floatval($amount_to_pay);

            // }
            // $dato->emp_acc_amountpaid = floatval($request->get('emp_acc_amount_to_paid')) + floatval($dato->emp_acc_amountpaid);
            $dato->emp_acc_datepaid = $today;

            $dato->comp_det_id = $comp_det_id;

            $saved = $dato->save();

            if (!is_null($value['amount_loan'])) {
              $prestamos = DB::select(
                "select * from employee_account ea where ea.is_discount is true 
                and ea.deleted_at is null and extract(year from ea.emp_acc_expiration)= extract(year from '" . $dato->emp_acc_expiration . "'::date) and extract(month from ea.emp_acc_expiration) = extract(month from '" . $dato->emp_acc_expiration . "'::date) and ea.person_id = " . $dato->person_id
              );
              $len_prestamos = count($prestamos);
              for ($i = 0; $i < $len_prestamos; $i++) {
                DB::update("update employee_account ea set emp_acc_amountpaid = ea.emp_acc_amount, emp_acc_status = true, comp_det_id=" . $comp_det_id . ", updated_at = now() where emp_acc_id = " . $prestamos[$i]->emp_acc_id);
              }
            }


            // dd('entra a if de $amount->suma >= $request->get(emp_acc_amount_to_paid');

            // $saved = true;
            if ($saved == true) {
              $details = new TillDetail([
                'till_id' => $till_id,
                'factura_id' => 0,
                'description' => 'Pago a funcionario',
                'date' => $today,
                'amount' => ($dato->emp_acc_amountpaid - $value['emp_acc_amountpaid']) + $value['extra_hours'],
                'ref_id' => $dato->emp_acc_id,
                'dettills_type' => false,
                'dettill_status' => true,
                'person_id' => auth()->user()->person_id,
              ]);
              $savedDetails = $details->save();
              // $savedDetails = true;
              if ($savedDetails) {
                $pay_type = ProofPayment::where('pay_type_id', $pay_type_id)->first();
                $proof = new DettillProofPayment();
                $proof->dettills_id = $details['dettills_id'];
                $proof->pr_payment_id = $pay_type['pr_payment_id'];
                $proof->dettills_pr_pay_desc = 'Ninguno';
                $proof->save();
                // return response()->json([
                //   'message' => 'Registro se agrego con exito!',
                //   'dato' => true
                // ]);
                if (count($to_report) > 0) {
                  file_put_contents('salario_banco.txt', implode(PHP_EOL, $to_report));
                  $to_return = true;
                }
              } else {
                return response()->json([
                  'message' => 'No se pudo guardar el registro',
                  'dato' => false,
                  'show_file' => false
                ]);
              }
            }
          } else {
            // if (count($to_report) > 0) {
            //   file_put_contents('salario_banco.txt', implode(PHP_EOL, $to_report));
            //   $to_return = true;
            // }
            return response()->json([
              'message' => 'No hay suficiente dinero en caja',
              'dato' => false,
              'show_file' => $to_return
            ]);
          }
        } else {
          return response()->json([
            'message' => 'No hay dinero en caja',
            'dato' => false,
            'show_file' => false
          ]);
        }
      } else {
        return response()->json([
          'message' => 'No hay cajas abiertas!',
          'dato' => false,
          'show_file' => $to_return
        ]);
      }
    }
    file_put_contents('salario_banco.txt', implode(PHP_EOL, $to_report));
    // if($savedDetails == true){
    return response()->json([
      'message' => 'Registro se agrego con exito!',
      'dato' => true,
      'show_file' => $to_return
    ]);
    // }

  }

  public function search(Request $req)
  {
    // dd($req);
    $extra_filter = '';
    $extra_join = '';
    if (isset($req->filter['till']) and $req->filter['till'] != null) {
      if (isset($req->filter['paid']) and $req->filter['paid'] == 'true') {
        $extra_join = "join tills_details td on td.ref_id = ea.emp_acc_id and td.description = 'Pago a funcionario'";
        $extra_filter = $extra_filter . 'and td.till_id = ' . $req->filter['till'];
      } else {
        $extra_filter = $extra_filter . 'and e.till_id = ' . $req->filter['till'];
        $extra_join = '';
      }
    }

    if (isset($req->filter['paid'])) {
      // dd('da');
      if ($req->filter['paid'] == 'true') {
        // dd($req->filter['paid']);
        $extra_filter = $extra_filter . ' and ea.emp_acc_status is true ';
      } else {
        // dd('entra a else de paid');
        if (isset($req->filter['partial'])) {
          if ($req->filter['partial'] == 'true') {
            $extra_filter = $extra_filter . ' and ((ea.emp_acc_amountpaid < ea.emp_acc_amount and ea.emp_acc_amountpaid > 0) and ea.emp_acc_status is false) ';
          } else {
            $extra_filter = $extra_filter . ' and (ea.emp_acc_amountpaid = 0 and ea.emp_acc_status is false)';
          }
        }
      }
    }
    if (isset($req->filter['comp_det'])) {
      // dd('da');
      if ($req->filter['comp_det'] != null) {
        // dd($req->filter['paid']);
        $extra_filter = $extra_filter . ' and ea.comp_det_id = ' . $req->filter['comp_det'];
      }
    }
    // " . $extra_filter . "
    // dd($extra_filter);

    $search = DB::select("select e.person_id,cd.comp_det_bussiness_name, e.person_lastname, e.person_fname, e.person_idnumber,e.till_id, e.emplo_salary, e.has_ips, e.porcent_ips, ea3.amount_loan, ea.* from employee_account ea 
    join employees e on e.person_id = ea.person_id 
    join company_details cd on cd.comp_det_id = ea.comp_det_id 
    left join (select sum(ea2.emp_acc_amount) amount_loan, ea2.person_id from 
      employee_account ea2 where ea2.is_discount is true and 
      deleted_at is null and 
      extract(month from ea2.emp_acc_expiration)=" . $req->filter['month'] . " and 
      extract(year from ea2.emp_acc_expiration) = " . $req->filter['year'] . " group by ea2.person_id) ea3 on ea3.person_id = ea.person_id
    " . $extra_join . "
    where extract(month from ea.emp_acc_expiration)=" . $req->filter['month'] . " and 
    extract(year from ea.emp_acc_expiration) = " . $req->filter['year'] . " and 
    ea.is_discount is false and ea.deleted_at is null and 
    e.deleted_at is null 
    " . $extra_filter . " 
    order by e.person_lastname");
    // dd("select e.person_id, e.person_lastname, e.person_fname, e.person_idnumber,e.till_id, e.emplo_salary, 
    // e.has_ips, e.porcent_ips, ea3.amount_loan, ea.* from employee_account ea 
    // join employees e on e.person_id = ea.person_id 
    // left join (select sum(ea2.emp_acc_amount) amount_loan, ea2.person_id from 
    //   employee_account ea2 where ea2.is_discount is true and 
    //   deleted_at is null and 
    //   extract(month from ea2.emp_acc_expiration)=" . $req->filter['month'] . " and 
    //   extract(year from ea2.emp_acc_expiration) = " . $req->filter['year'] . " group by ea2.person_id) ea3 on ea3.person_id = ea.person_id
    // " . $extra_join . "
    // where extract(month from ea.emp_acc_expiration)=" . $req->filter['month'] . " and 
    // extract(year from ea.emp_acc_expiration) = " . $req->filter['year'] . " and 
    // ea.is_discount is false and ea.deleted_at is null and 
    // e.deleted_at is null 
    // " . $extra_filter . " 
    // order by e.person_lastname");
    // dd($search);
    return [
      'data' => $search,
      'filter' => $req->filter,
      'filter_label' => $req->filter_label
    ];
  }


  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\EmployeeAccount  $dato
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = EmployeeAccount::find($id);
    $dato->delete();
    return response()->json('Registro se elimino con exito!');
  }
}
