<?php

namespace App\Http\Controllers\Audits;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Audit;
use Illuminate\Support\Facades\DB;

class AuditController extends ApiController
{
  public function index()
  {
    $datos = Audit::all();
    return $this->showAll($datos,200);
    // return response()->json($datos);
  }

  // public function store(Request $request)
  // {
  //       $request->validate([
  //       'appoin_description' => 'required',
  //   ]);
  //  $datos = new Audit($request->all());
  //  $datos->save();
  //   return response()->json('Cargo se agrego con exito!');
  // }

  // public function show($id)
  // {
  //   //$dato = Audit::find( $id);
  //   $dato = Audit::where( 'appoin_id', '=', $id)
  //                             ->first();
  //   return $this->showOne($dato,200);
  // }

  // public function update(Request $request, $id)
  // {
  //   $dato = Audit::findOrFail( $id);
  //         $request->validate([
  //           'appoin_description' => 'required',
  //         ]);
  //         $dato->appoin_description = $request->appoin_description;
  //         $dato->save();
  //         return response()->json([
  //             'message' => 'Cargo se actualizo con exito!',
  //             'dato' => $dato
  //         ]);
  //     }

      public function destroy($id)
      {
          $dato = Audit::findOrFail($id);
          $dato->delete();
          return response()->json([
              'message' => 'Cargo se elimino con exito!'
          ]);
        }
}
