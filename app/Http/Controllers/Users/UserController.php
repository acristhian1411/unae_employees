<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiDBController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class UserController extends ApiDBController
{
  // $datos= Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
  //              ->rightjoin('users', 'model_has_roles.model_id', '=', 'users.id')
  //              ->select('users.*', 'roles.role_name')
  //              ->where('users.deleted_at','=', null)
  //              ->get();
  public function index()
  {
    $count = DB::select('select count(*) from users u where deleted_at is null');
    $query = "select * from users u where deleted_at is null";

    $return_response = $this->showAll($query, $count, 200);

    $response = json_decode(json_encode($return_response))->original;

    $data_len =count($response->data);
    for ($i=0; $i < $data_len; $i++) {
      $response->data[$i]->roles = DB::select("select r.*  from roles r
                                                join model_has_roles mhr on mhr.role_id = r.role_id
                                                where mhr.model_id = ".$response->data[$i]->id."
                                              ");
    }

    return response()->json($response);

    // $datos = User::all()
    //   ->where('deleted_at', '=', null);
    //
    // for ($i = 0; $i < count($datos); $i++) {
    //   $roles = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
    //     ->select('roles.*')
    //     ->where('model_has_roles.model_id', '=', $datos[$i]->id)
    //     ->get();
    //   $datos[$i]->roles = $roles;
    // }
    // // $roles = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
    // //              ->select('roles.*')
    // //              ->where('model_has_roles.model_id', '=', $datos->id)
    // //              ->get();
    // // $datos->roles = $roles;
    // return $this->showAll($datos, 200);
  }
  public function hasRoles(Request $request)
  {

    $user = auth()->user();

    return response()->json($user->hasRole($request->rol));
  }

  public function get_user(Request $request)
  {
    // $user = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
    //              ->join('users', 'model_has_roles.model_id', '=', 'users.id')
    //              ->select('users.*', 'roles.role_name', 'roles.role_id')
    //              ->where('model_has_roles.model_id', '=', $request->user()->id)
    //              ->first();
    $user = User::join('persons', 'persons.person_id', '=', 'users.person_id')
      ->select('users.*', 'persons.person_photo')
      ->where('id', '=', $request->user()->id)->first();
    $roles = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
      ->select('roles.*')
      ->where('model_has_roles.model_id', '=', $request->user()->id)
      ->get();
    $user->roles = $roles;

    // dd($user);
    return $user;
    // if ($user == null) {
    //   // $request->user()->request->add(['rol_name'=>"no definido"]);
    //   $user = $request->user();
    //   $user->rol_name = 'No definido';
    //   return $user;
    // }
    // else {
    //   return $user;
    // }


  }
  public function store(Request $request)
  {
    // dd($request);
    $request->validate([
      'person_id' => 'required',
      'email' => 'required',
    ]);

    $dato = new User([
      'person_id' => $request->get('person_id'),
      'email' => $request->get('email'),
      'name' => $request->get('name'),
      'username' => $request->get('username'),
      'password' => Hash::make('unaetest'),

    ]);
    $dato->save();
    if (property_exists('roles', $request) || $request->roles != null) {
      foreach ($request->roles as $key => $value) {
        $role = Role::find($value['role_id']);
        $dato->assignRole($role->role_name);
      }
    } else {
      $role = Role::find($request->role_id);
      $dato->assignRole($role->role_name);
    }


    return response()->json('Usuario se agrego con exito!');
  }
  public function update(Request $request, $id)
  {

    // return response()->json($request);
    if ($request->audit_restore) {
      $dato = User::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Usuario se restauro con exito!');
    } else {
      $dato = User::find($id);
      if ($request->new_password == null) {
        $request->validate([
          'person_id' => 'required',
          'email' => 'required',
        ]);
      } else {
        if ($request['new_password']['password'] == $request['new_password']['password_verified']) {
          $dato->password = Hash::make($request['new_password']['password']);
          $dato->reset_password = true;
        } else {
          return response()->json('las contraseñas no coinciden');
        }
      }


      $dato->person_id = $request['person_id'];
      $dato->email = $request['email'];
      $dato->name = $request['name'];
      $dato->username = $request['username'];
      $dato->save();


      foreach ($request->roles as $key => $value) {
        if ($value['edited'] == true) {
          $newRole = Role::find($value['new_role_id']);
          $oldRole = Role::find($value['old_role_id']);
          $dato->removeRole($oldRole->role_name);
          $dato->assignRole($newRole->role_name);
        } else {
          if ($value['created'] == false) {
            $role = Role::find($value['role_id']);
            $dato->assignRole($role->role_name);
          }
        }
      }
      return response()->json('Usuario se actualizo con exito');
    }
  }

  public function removeRole(Request $request, $id)
  {
    $dato = User::find($id);
    $role = Role::find($request->role_id);
    $dato->removeRole($role->role_name);
  }

  public function resetPassword(Request $request)
  {
    // dd($request['password']);
    $request->validate([
      'password' => [
        'required',
        'min:8'
      ]
    ]);

    if ($request['password'] == $request['password_verified']) {
      $dato = User::find(auth()->user()->id);
      $dato->password = Hash::make($request['password']);
      $dato->reset_password = true;
      $dato->save();
      return response()->json('se ha cambiado la contraseña correctamente', 200);
    } else {
      return abort(422, 'passwords no coinciden');
    }
  }
  public function resetPassword2(Request $request, $id)
  {
    // dd($request['password']);
    $request->validate([
      'new_password' => [
        'required',
        'min:8'
      ]
    ]);
    $roles = DB::select("select r.role_id, r.role_name from model_has_roles mhr 
        join roles r ON r.role_id = mhr.role_id 
        where mhr.model_id = ".$id." and mhr.role_id in(8, 9)");
    if (count($roles)>0) {
      $dato = User::find($request->id);
      $dato->password = Hash::make($request['new_password']);
      $dato->reset_password = false;
      $dato->save();
      return response()->json(["message"=>'se ha cambiado la contraseña correctamente', "type"=>1], 200);  
    }else{
      return response()->json(["message"=>'Solo se permite el reseteo en los roles de estudiantes y de docentes', "type"=>2], 200);  
    }
    

  }
  public function show($id)
  {
    $user = User::where('id', '=', $id)->first();
    $roles = Role::join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.role_id')
      ->select('roles.*')
      ->where('model_has_roles.model_id', '=', $id)
      ->get();
    $user->roles = $roles;

    return $this->showOne($user, 200);
  }
  public function showPerson($id)
  {
    $user = User::where('person_id', '=', $id)->get();

    return $this->showAll($user, 200);
  }
  public function showPerson2($id)
  {
    $data = DB::select("select p.person_lastname, p.person_fname, p.person_idnumber, p.person_birthdate, p.person_address from users u 
      join only persons p on p.person_id = u.person_id 
      where u.id = ".$id);
    if(count($data)>0){
      $data[0]->roles = DB::select("select r.role_id, r.role_name from model_has_roles mhr 
        join roles r ON r.role_id = mhr.role_id 
        where mhr.model_id = ".$id);
      return response()->json($data[0]);
    }else{
      return response()->json([]);
    }
  }
  public function destroy($id)
  {
    $dato = User::find($id);
    $dato->delete();
    return response()->json('Usuario se elimino con exito');
  }
  public function search($request){
    $word_filter = explode(" ",$request);
    $array_like = "";
    if (count($word_filter)==2) {
      $array_like = "or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[0]."%') and".
        " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[1]."%'))
        or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[1]."%') and".
          " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[0]."%')) ";
    }elseif (count($word_filter)==3) {
      $array_like = "or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[0]."%') and".
        " unaccent(p.person_fname) ilike unaccent('%".$word_filter[1]."%') and".
          " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[2]."%'))
        or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[0]."%') and".
          " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[1]."%') and".
          " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[2]."%'))
        or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[2]."%') and".
          " unaccent(p.person_fname) ilike unaccent('%".$word_filter[1]."%') and".
            " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[0]."%'))
          or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[2]."%') and".
            " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[1]."%') and".
            " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[0]."%')) ";
    }elseif (count($word_filter)==4) {
      $array_like = "or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[0]."%') and".
        " unaccent(p.person_fname) ilike unaccent('%".$word_filter[1]."%') and".
        " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[2]."%') and".
        " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[3]."%'))
        or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[0]."%') and".
          " unaccent(p.person_fname) ilike unaccent('%".$word_filter[1]."%') and".
          " unaccent(p.person_fname) ilike unaccent('%".$word_filter[2]."%') and".
          " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[3]."%'))
        or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[3]."%') and".
          " unaccent(p.person_fname) ilike unaccent('%".$word_filter[2]."%') and".
          " unaccent(p.person_fname) ilike unaccent('%".$word_filter[1]."%') and".
            " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[0]."%'))
          or (unaccent(p.person_fname) ilike unaccent('%".$word_filter[2]."%') and".
            " unaccent(p.person_fname) ilike unaccent('%".$word_filter[2]."%') and".
            " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[1]."%') and".
            " unaccent(p.person_lastname) ilike unaccent('%".$word_filter[0]."%')) ";
    }
    $count = DB::select("select count(*)
    from only persons p
    left join contact_persons cp on cp.person_id = p.person_id
    join users u on u.person_id = p.person_id
    where p.person_fname
      ilike " . '$$%' . $request . '%$$' . "
    or u.email ilike " . '$$%' . $request . '%$$' . "
    or unaccent(p.person_lastname) ilike " . '$$%' . $request . '%$$' . "
    or p.person_idnumber ilike " . '$$%' . $request . '%$$' . "
    or concat(p.person_idnumber, ' - ', unaccent(p.person_fname), ' ', unaccent(p.person_lastname))
      ilike unaccent('%" . $request . "%')
      ".$array_like."group by u.id");

      $data = "select  u.*
      from only persons p left join contact_persons cp on cp.person_id = p.person_id
      join users u on u.person_id = p.person_id
      where cp.cntper_data ilike " . '$$%' . $request . '%$$' .
      " or u.email ilike " . '$$%' . $request . '%$$' .
      " or  unaccent(p.person_fname) ilike " . 'unaccent($$%' . $request . '%$$)' .
      " or unaccent(p.person_lastname) ilike " . 'unaccent($$%' . $request . '%$$)' .
      " or p.person_idnumber ilike " . '$$%' . $request . '%$$'.
      " or (select concat(unaccent(person_fname), ' ', unaccent(person_lastname)) as full_name) ilike unaccent('%".$request. "%')
      ".$array_like."group by u.id";


    // $data = Person::where('person_fname', 'ILIKE', '%' . $request . '%')
    //   ->orWhere('person_lastname', 'ILIKE', '%' . $request . '%')
    //   ->orWhere('person_idnumber', 'ILIKE', '%' . $request . '%')
    //   ->distinct()
    //   ->get();
    // dd(DB::select($data));
    return $this->showAll($data, $count, 200);
  }
}
