<?php

namespace App\Http\Controllers\ProofPayment;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\ProofPayment;

class ProofPaymentController extends ApiController
{
  public function index()
  {
    $t = ProofPayment::query()->first();
    $query = ProofPayment::query();
    $query = $this->filterData($query, $t);
    $datos = $query->get();
    return $this->showAll($datos, 200);
  }

  public function store(Request $request)
  {
    if (count($request->proof_payment) > 0) {
      foreach ($request->proof_payment as $key =>  $value) {
        if ($value['created'] == false) {
          // dd($value['pr_requires_image']);
          $dato = new ProofPayment([
            'pay_type_id' => $request->pay_type_id,
            'pr_payment_desc' => $value['pr_payment_desc'],
            'pr_requires_image' => $value['pr_requires_image']
          ]);
          $dato->save();
        }
      }
    }
    return response()->json('Comprobante de pago se agrego correctamente!');
  }

  public function update(Request $request)
  {
    if (count($request->proof_payment) > 0) {
      $proof_payment = $request->proof_payment;
      foreach ($proof_payment as $key => $value) {
        if ($value['created'] == true) {
          $dato = ProofPayment::find($value['pr_payment_id']);
          $dato->pr_payment_desc = $value['pr_payment_desc'];
          $dato->pr_requires_image = $value['pr_requires_image'];
          $dato->save();
        }
      }
    }
    return response()->json('Comprobante de pago se agrego correctamente!');
  }

  public function show($id)
  {
    $dato = ProofPayment::where('pr_payment_id', '=', $id)
      ->first();
    return $this->showOne($dato, 200);
  }

  public function showPaymentType($id)
  {
    $dato = ProofPayment::where('pay_type_id', '=', $id)
      ->get();
    if ($dato->isEmpty()) {
      return response()->json(false);
    } else {
      return $this->showAll($dato, 200);
    }
  }

  public function destroy($id)
  {
    $dato = ProofPayment::findOrFail($id);
    $dato->delete();
    return response()->json(
      'Comprobante de pago se elimino correctamente!'
    );
  }
}
