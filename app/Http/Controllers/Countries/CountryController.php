<?php

namespace App\Http\Controllers\Countries;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;
use App\Country;

class CountryController extends ApiController
{
  public function index()
  {

      $t = Country::query()->first();
      $query = Country::query();
      $query = $this->filterData($query, $t);
      $datos = $query->get();
      return $this->showAll($datos,200);
  }



  public function store(Request $request)
  {
    $request->validate([
    'country_name' => 'required',
    'country_code' => 'required',
    ]);
      $dato = new Country([
        'country_name' => $request ->get('country_name'),
        'country_code' => $request ->get('country_code')
      ]);

      $dato->save();
      return response()->json('Pais se agrego con exito!');
  }

  public function show($id)
  {
      $datos = Country::find( $id);
        return $this->showOne($datos,200);
  }


  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = Country::find($id);
      $keys = collect($request->all())->keys();
      for ($i=0; $i < count($keys); $i++) {
        $dato[$keys[0]]= $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Pais se restauro con exito!');
    }
    else{
          $dato = Country::findOrFail( $id);
          $request->validate([
            'country_name' => 'required',
            'country_code' => 'required',
          ]);

          $dato->country_name = $request->country_name;
          $dato->country_code = $request->country_code;
          $dato->save();
/*           return response()->json([
              'message' => 'Country updated!',
              'dato' => $dato
          ]); */
          return response()->json('Pais se actualizo con exito!');
        }
  }


  public function destroy($id)
  {
      $dato = Country::find($id);
      $dato->delete();
//      return response()->json('Country Deleted Successfully');
      return response()->json('Pais se elimino con exito!');
  }

  public function search($request){
    $datos = Country::where('country_code','ILIKE','%'.$request.'%')
                  ->orWhere('country_name','ILIKE','%'.$request.'%')
                  ->get();
  //return response()->json($data);
  return $this->showAll($datos,200);
  }
}
