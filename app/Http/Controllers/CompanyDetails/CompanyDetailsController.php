<?php

namespace App\Http\Controllers\CompanyDetails;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\CompanyDetails;

class CompanyDetailsController extends ApiController
{
  public function index()
  {
    //
    $t = CompanyDetails::query()->first();
    $query = CompanyDetails::query();
    $query = $this->filterData($query, $t);
    $datos = $query
      ->select('comp_det_id', 'comp_det_bussiness_name')
      ->get();;
    return $this->showAll($datos, 200);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
    $request->validate([
      'comp_det_bussiness_name' => 'required',
      'comp_det_ruc' => 'required',
    ]);
    $dato = new CompanyDetails([
      'comp_det_bussiness_name' => $request->get('comp_det_bussiness_name'),
      'comp_det_ruc' => $request->get('comp_det_ruc'),
    ]);


    $dato->save();

    return response()->json([
      'message' => 'Dato registrado con exito',
      'dato' => $dato
    ]);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\CompanyDetails  $country
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //

    $dato = CompanyDetails::find($id);

    return $this->showOne($dato, 200);
  }

  // public function edit(CompanyDetails $department)
  // {
  //     //
  //     $department = CompanyDetails::find($department);
  //     return response()->json($department);
  // }

  public function update(Request $request, $id)
  {
    if ($request->audit_restore) {
      $dato = CompanyDetails::find($id);
      $keys = collect($request->all())->keys();
      for ($i = 0; $i < count($keys); $i++) {
        $dato[$keys[0]] = $request->get($keys[0]);
      }
      $dato->save();
      return response()->json('Datos se restauraron con exito!');
    } else {
      $request->validate([
        'comp_det_bussiness_name' => 'required',
        'comp_det_ruc' => 'required',
      ]);
      $dato = CompanyDetails::find($id);
      $dato->comp_det_bussiness_name = $request->get('comp_det_bussiness_name');
      $dato->comp_det_ruc = $request->get('comp_det_ruc');
      $dato->save();
      return response()->json('Datos se actualizaron con exito!');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\CompanyDetails  $country
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $dato = CompanyDetails::find($id);
    $dato->delete();
    return response()->json('Datos eliminados con exito!');
  }
}
