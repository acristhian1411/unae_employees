<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeAccount extends Model implements Auditable
{

  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table = "employee_account";
  protected $primaryKey = "emp_acc_id";
  protected $fillable = [
    "emp_acc_id", "person_id", "emp_acc_desc",
    "emp_acc_expiration", "emp_acc_status", "emp_acc_amount", "emp_acc_amountpaid", "emp_acc_ticket_number", "emp_acc_month", "emp_acc_datepaid", "is_discount", "extra_hours", "comp_det_id"
  ];
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $hidden = ["created_at", "updated_at", "deleted_at"];
  // relaciones

  public function  provider()
  {
    return $this->belongsTo(Employee::class, 'person_id', 'person_id');
  }
}
