<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class UnitType extends Model implements Auditable
{
    //
      use \OwenIt\Auditing\Auditable;
  protected $table ='unit_types';
  protected $primaryKey = 'unit_type_id';
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $fillable =['unit_type_id','unit_type_description'];
  protected $hidden = ["created_at","updated_at","deleted_at"];

  use SoftDeletes;


  // relascions
     Public function units()
     {
    return	$this->hasMany(Unit::class, "unit_type_id", "unit_type_id");
     }
//relación entre retentionQuestion y UnitTypes
     Public function StudentRetentionQuestions()
     {
    return  $this->hasMany(Unit::class, "unit_type_id", "unit_type_id");
     }
}
