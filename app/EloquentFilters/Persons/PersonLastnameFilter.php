<?php

namespace App\EloquentFilters\Persons;

use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class PersonLastnameFilter extends Filter
{
    /**
     * Apply the condition to the query.
     *
     * @param Builder $builder
     * @param mixed $value
     *
     * @return Builder
     */
    public function apply(Builder $builder, $value): Builder
    {
      return $builder->where('person_lastname', 'ILIKE', '%'.$value.'%');
    }
}
