<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use OwenIt\Auditing\Contracts\Auditable;


class Unit extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table ='units';
  protected $fillable =[
    'unit_id','unit_name','unit_code','unit_logo', 'unit_type_id','unit_company', 'comp_det_id'
  ];
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $primaryKey = 'unit_id';
  protected $hidden = ["created_at","updated_at","deleted_at"];

  // realacios
  // return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
  public function unit_type(){
    return $this->belongsTo(UnitType::class, 'unit_type_id', 'unit_type_id');
  }
  public function faculties() {
  	$this->hasMany(Faculties::class, "facu_unit_id", "unit_id" );
  }
  public function tills() {
    $this->hasMany(Till::class, "unit_id", "unit_id" );
  }
  //unit_logo
  // public static function setLogo($logo, $actual = false){
  //      if($logo){
  //        if ($actual) {
  //          Storage::disk('public')->delete("images/logos/$actual");
  //          }
  //          $imageName = Str::random(20).'.png';
  //          $imagen = Image::make($logo)->encode('png', 75);
  //          $imagen->resize(530, 470, function($constraint){
  //            $constraint->upsize();
  //          });
  //          Storage::disk('public')->put("images/logos/units/$imageName", $imagen->stream());
  //          return $imageName;
  //        }
  //        else{
  //          return false;
  //        }
  //      }

}
