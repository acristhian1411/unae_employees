<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Ticket extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table = 'tickets';
  protected $fillable = [
    'ticket_id', 'person_id', 'ticket_date', 'ticket_number',
    'comp_det_id', 'created_by', 'ticket_status', 'ticket_is_invoice'
  ];
  protected $primaryKey = 'ticket_id';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $hidden = ["created_at", "updated_at", "deleted_at"];
  //relaciones
  public function details()
  {
    $this->hasMany(TicketDetail::class, "ticket_id", "ticket_id");
  }
  public function tilldetails()
  {
    $this->hasMany(TicketDetail::class, "ticket_id", "ticket_id");
  }
  public function person()
  {
    $this->belongsTo(Person::class, "person_id", "person_id");
  }
}
