<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class TicketDetailType extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table ='ticket_details_types';
  protected $fillable =[ 'tdtype_id','tdtype_desc','tdtype_obs'];
  protected $primaryKey = 'tdtype_id';
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $hidden = ["created_at","updated_at","deleted_at"];
  //relaciones
  public function ticketdetails() {
    $this->hasMany(TicketDetail::class, "tdtype_id", "tdtype_id" );
  }
}
