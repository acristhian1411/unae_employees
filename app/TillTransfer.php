<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use App\Till;
class TillTransfer extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table ='till_transfers';
  protected $fillable =[ 'tilltrans_id','origin_till_id','destiny_till_id','tilltrans_date','tilltrans_amount','tilltrans_observation' ];
  protected $primaryKey = 'tilltrans_id';
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $hidden = ["created_at","updated_at","deleted_at"];

  // realaciones
  public function originTill() {
    $this->belongsTo(Till::class, "origin_till_id", "till_id" );
  }
  public function destTill() {
    $this->belongsTo(Till::class, "destiny_till_id", "till_id" );
  }
}
