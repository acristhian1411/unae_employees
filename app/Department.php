<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Department extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    //
    protected $table="departaments";
    protected $primaryKey="depart_id";
    protected $fillable=["depart_id","depart_name",  "country_id"];
    protected $dateFormat = 'Y-m-d H:i:sO';
    protected $hidden = ["created_at","updated_at","deleted_at"];
    // relaciones
    public function  country()
    {
    	 return $this->belongsTo(Country::class, 'country_id', 'country_id' );
    }
    public function  cities()
    {
    	 return $this->hasMany(City::class, 'depart_id', 'depart_id' );
    }
}
