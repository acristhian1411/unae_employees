<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class PaymentType extends Model implements Auditable
{
        protected $table="payment_type";
        protected $primaryKey="pay_type_id";
        protected $fillable=["pay_type_id","pay_type_desc"];
        protected $dateFormat = 'Y-m-d H:i:sO';
        protected $hidden = ["created_at","updated_at","deleted_at"];
        use SoftDeletes;
        use \OwenIt\Auditing\Auditable;

}
