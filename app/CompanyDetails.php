<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class CompanyDetails extends Model implements Auditable
{
    //
    protected $table="company_details";
    protected $primaryKey="comp_det_id";
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $fillable=["comp_det_id","comp_det_bussiness_name", "comp_det_ruc"];
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    // relaciones
    public function  units()
    {
    	 return $this->hasMany(Unit::class, 'comp_det_id', 'comp_det_id' );
    }

}
