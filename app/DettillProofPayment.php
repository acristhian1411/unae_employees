<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class DettillProofPayment extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $table = "dettill_proofpayment";
    protected $primaryKey = "dettills_pr_pay_id";
    protected $fillable = ["dettills_pr_pay_id", "dettills_id", "pr_payment_id", "dettills_pr_pay_desc", "dettills_pr_pay_image"];
    protected $dateFormat = 'Y-m-d H:i:sO';
    protected $hidden = ["created_at", "updated_at", "deleted_at"];
}
