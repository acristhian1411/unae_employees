<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class TillDetail extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;

  protected $table = 'tills_details';
  protected $fillable = [
    'dettills_id', 'till_id', 'person_id', 'factura_id', 'description',
    'date', 'amount', 'ticket_number', 'dettills_type', 'ref_id', "pay_type_id",
    "dettills_receipt_number", "account_p_id", 'dettill_status'
  ];
  protected $primaryKey = 'dettills_id';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $hidden = ["updated_at", "deleted_at"];

  //
  public function employee()
  {
    $this->belongsTo(Employee::class, "person_id", "person_id");
  }
  public function ticket()
  {
    $this->belongsTo(Ticket::class, "ticket_id", "ticket_id");
  }
  public function till()
  {
    $this->belongsTo(Till::class, "till_id", "till_id");
  }
}
