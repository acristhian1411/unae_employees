<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class ContactPerson extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $table="contact_persons";
    protected $primaryKey="cntper_id";
    protected $fillable=["cntper_id","person_id","cntper_description","cntper_data",'cnttype_id' ];
    protected $dateFormat = 'Y-m-d H:i:sO';
    protected $hidden = ["created_at","updated_at","deleted_at"];
    // relaciones
    public function  contactType()
    {
    	 return $this->belongsTo(ContactType::class, 'cnttype_id', 'cnttype_id' );
    }
        public function  person()
    {
    	 return $this->hasMany(Person::class, 'person_id', 'person_id' );
    }
}
