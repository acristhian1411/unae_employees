<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class TicketDetail extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table ='ticket_details';
  protected $fillable =[ 'ticket_id','tickdet_id','tdtype_id','tickdet_monto','tickdet_iva','tickdet_qty','tickdet_desciption','tickdet_ref'];
  protected $primaryKey = 'tickdet_id';
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $hidden = ["created_at","updated_at","deleted_at"];
  //relaciones
  public function ticket() {
    $this->belongsTo(Ticket::class, "ticket_id", "ticket_id" );
  }
  public function detTickType() {
    $this->belongsTo(TicketDetailType::class, "tdtype_id", "tdtype_id" );
  }
}
