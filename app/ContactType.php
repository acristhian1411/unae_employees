<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ContactType extends Model implements Auditable
{
        //
        use \OwenIt\Auditing\Auditable;
    protected $table="contact_type";
    protected $primaryKey="cnttype_id";
    protected $fillable=["cnttype_id","cnttype_name"];
    protected $dateFormat = 'Y-m-d H:i:sO';
    protected $hidden = ["created_at","updated_at","deleted_at"];
    use SoftDeletes;

    // relaciones
    public function  contactPersons()
    {
    	 return $this->hasMany(ContactPerson::class, 'cnttype_id', 'cnttype_id' );
    }
}
