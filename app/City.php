<?php

namespace App;

use App\School;
use App\Department;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class City extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table="cities";
    protected $primaryKey="city_id";
    protected $fillable=["depart_id","city_ip", "city_name"];
	  protected $dateFormat = 'Y-m-d H:i:sO';
    protected $hidden = ["created_at","updated_at","deleted_at"];

    use SoftDeletes;
    // relaciones
    public function  department()
    {
    	 return $this->belongsTo(Department::class, 'depart_id', 'depart_id' ); ;
    }
    public function  schools()
    {
    	 return $this->hasMany(School::class, 'city_id', 'city_id' );
    }
        public function  homecites()
    {
         return $this->hasMany(Person::class, 'homecity_id', 'city_id' );
    }
    public function  birthplaces()
    {
         return $this->hasMany(Person::class, 'birthplace_id', 'city_id' );
    }
}
