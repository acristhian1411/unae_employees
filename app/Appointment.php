<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Appointment extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;
    protected $table="appointments";
    protected $primaryKey="appoin_id";
    protected $fillable=["appoin_id","appoin_description"];
    protected $hidden = ["created_at","updated_at","deleted_at"];
    protected $dateFormat = 'Y-m-d H:i:sO';

    //relaciones
    public function employees()
    {
    	return $this->hasMany(Employee::class, 'appoin_id', 'appoin_id' );

    }

}
