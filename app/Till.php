<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Till extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table = 'tills';
  protected $fillable = ['unit_id', 'till_id', 'till_name', 'till_type', 'till_account_number'];
  protected $primaryKey = 'till_id';
  protected $dateFormat = 'Y-m-d H:i:sO';
  protected $hidden = ["created_at", "deleted_at"];

  // realaciones
  public function unit()
  {
    $this->belongsTo(Unit::class, "unit_id", "unit_id");
  }
  public function transfersOrigin()
  {
    $this->hasMany(TillTransfer::class, "origin_till_id", "till_id");
  }
  public function transfersDest()
  {
    $this->hasMany(TillTransfer::class, "destiny_till_id", "till_id");
  }
  public function details()
  {
    $this->hasMany(TillDetail::class, "till_id", "till_id");
  }
}
