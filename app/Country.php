<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class Country extends Model implements Auditable
{
    //
    protected $table="countries";
    protected $primaryKey="country_id";
    protected $dateFormat = 'Y-m-d H:i:sO';
    protected $fillable=["country_id","country_name", "country_code"];
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    // relaciones
    public function  departments()
    {
    	 return $this->hasMany(Department::class, 'country_id', 'country_id' );
    }
    public function  persons()
    {
    	 return $this->hasMany(Person::class, 'country_id', 'country_id' );
    }
}
