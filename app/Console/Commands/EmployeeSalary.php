<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Employee;
use App\Audit;
use App\EmployeeAccount;
class EmployeeSalary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employee:salary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate salary for every employees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
      $day = date("d");
      if ($day == 1) {
        $year = date("Y");
        $month = date("m")."-".$year;
        $desc = "Salario mes ".date("m")." anho ".$year;
        $fecha = $year."-".date("m")."-"."5";
        $employee = Employee::where('emplo_status',true)->get();
        foreach ($employee as $key => $value) {
        $account = new EmployeeAccount([
                  'person_id' => $value['person_id'],
                  'emp_acc_desc' => $desc,
                  'emp_acc_status' => false,
                  'emp_acc_amountpaid' => 0,
                  'emp_acc_expiration' => $fecha,
                  'emp_acc_amount' => $value['emplo_salary'],
                  'emp_acc_month' => $month,
                  'emp_acc_ticket_number' => '',
                  ]);
        $account->save();
            }
        }else {
          $audit = new Audit([
            'user_type' => 'App/User',
            'user_id' => 6,
            'event' => 'created',
            'auditable_type' => 'App\EmployeeAccount',
            'auditable_id' => 2,
          ]);
          $audit->save();
        }
        $this->info('Successfully generate salary to every employees.');
    }
}
