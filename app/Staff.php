<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Staff extends Model implements Auditable
{
  use SoftDeletes;
  use \OwenIt\Auditing\Auditable;
  protected $table = "staff";
  protected $primaryKey = "staff_id";
  protected $fillable = ["staff_id", "facu_id", "person_id"];
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $hidden = ["created_at", "updated_at", "deleted_at"];
}
