<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    //Permisos para usuarios
    Permission::firstOrCreate(['permission_name' => 'users_index', 'permission_description' => 'Ver Lista de usuarios']);
    Permission::firstOrCreate(['permission_name' => 'users_show', 'permission_description' => 'Ver detalles de usuarios']);
    Permission::firstOrCreate(['permission_name' => 'users_store', 'permission_description' => 'Crear nuevos usuarios']);
    Permission::firstOrCreate(['permission_name' => 'users_search', 'permission_description' => 'Buscar usuarios']);
    Permission::firstOrCreate(['permission_name' => 'users_update', 'permission_description' => 'Actualizar usuarios']);
    Permission::firstOrCreate(['permission_name' => 'users_destroy', 'permission_description' => 'Eliminar usuarios']);

    //Permisos para Categorias de Docentes
    Permission::firstOrCreate(['permission_name' => 'professor_categories_index', 'permission_description' => 'Ver Lista de Categorias de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_categories_show', 'permission_description' => 'Ver detalles de Categorias de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_categories_store', 'permission_description' => 'Crear nuevos Categorias de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_categories_search', 'permission_description' => 'Buscar Categorias de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_categories_update', 'permission_description' => 'Actualizar Categorias de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_categories_destroy', 'permission_description' => 'Eliminar Categorias de Docentes']);

    //Permisos para cheques
    Permission::firstOrCreate(['permission_name' => 'checks_index', 'permission_description' => 'Ver Lista de cheques']);
    Permission::firstOrCreate(['permission_name' => 'checks_show', 'permission_description' => 'Ver detalles de cheques']);
    Permission::firstOrCreate(['permission_name' => 'checks_store', 'permission_description' => 'Crear nuevos cheques']);
    Permission::firstOrCreate(['permission_name' => 'checks_search', 'permission_description' => 'Buscar cheques']);
    Permission::firstOrCreate(['permission_name' => 'checks_update', 'permission_description' => 'Actualizar cheques']);
    Permission::firstOrCreate(['permission_name' => 'checks_destroy', 'permission_description' => 'Eliminar cheques']);

    //Permisos para Asistencia de docentes
    Permission::firstOrCreate(['permission_name' => 'professor_assistances_index', 'permission_description' => 'Ver Lista de Asistencia de docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_assistances_show', 'permission_description' => 'Ver detalles de Asistencia de docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_assistances_store', 'permission_description' => 'Crear nuevos Asistencia de docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_assistances_search', 'permission_description' => 'Buscar Asistencia de docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_assistances_update', 'permission_description' => 'Actualizar Asistencia de docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_assistances_destroy', 'permission_description' => 'Eliminar Asistencia de docentes']);

    //Permisos para Controles para docentes
    Permission::firstOrCreate(['permission_name' => 'professor_controls_index', 'permission_description' => 'Ver Lista de Controles para docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_controls_show', 'permission_description' => 'Ver detalles de Controles para docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_controls_store', 'permission_description' => 'Crear nuevos Controles para docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_controls_search', 'permission_description' => 'Buscar Controles para docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_controls_update', 'permission_description' => 'Actualizar Controles para docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_controls_destroy', 'permission_description' => 'Eliminar Controles para docentes']);

    //Permisos para usuarios
    Permission::firstOrCreate(['permission_name' => 'dettill_proofpayment_index', 'permission_description' => 'Ver Lista de comprobantes']);
    Permission::firstOrCreate(['permission_name' => 'dettill_proofpayment_show', 'permission_description' => 'Ver detalles de comprobantes']);
    Permission::firstOrCreate(['permission_name' => 'dettill_proofpayment_store', 'permission_description' => 'Crear nuevos comprobantes']);
    Permission::firstOrCreate(['permission_name' => 'dettill_proofpayment_search', 'permission_description' => 'Buscar comprobantes']);
    Permission::firstOrCreate(['permission_name' => 'dettill_proofpayment_update', 'permission_description' => 'Actualizar comprobantes']);
    Permission::firstOrCreate(['permission_name' => 'dettill_proofpayment_destroy', 'permission_description' => 'Eliminar comprobantes']);


    //Permisos para usuarios
    Permission::firstOrCreate(['permission_name' => 'paymentTypes_index', 'permission_description' => 'Ver Lista de Formas de pago']);
    Permission::firstOrCreate(['permission_name' => 'paymentTypes_show', 'permission_description' => 'Ver detalles de Formas de pago']);
    Permission::firstOrCreate(['permission_name' => 'paymentTypes_store', 'permission_description' => 'Crear nuevas Formas de pago']);
    Permission::firstOrCreate(['permission_name' => 'paymentTypes_search', 'permission_description' => 'Buscar Formas de pago']);
    Permission::firstOrCreate(['permission_name' => 'paymentTypes_update', 'permission_description' => 'Actualizar Formas de pago']);
    Permission::firstOrCreate(['permission_name' => 'paymentTypes_destroy', 'permission_description' => 'Eliminar Formas de pago']);

    //Permisos para usuarios
    Permission::firstOrCreate(['permission_name' => 'promos_index', 'permission_description' => 'Ver Lista de Promociones']);
    Permission::firstOrCreate(['permission_name' => 'promos_show', 'permission_description' => 'Ver detalles de Promociones']);
    Permission::firstOrCreate(['permission_name' => 'promos_store', 'permission_description' => 'Crear nuevas Promociones']);
    Permission::firstOrCreate(['permission_name' => 'promos_search', 'permission_description' => 'Buscar Promociones']);
    Permission::firstOrCreate(['permission_name' => 'promos_update', 'permission_description' => 'Actualizar Promociones']);
    Permission::firstOrCreate(['permission_name' => 'promos_destroy', 'permission_description' => 'Eliminar Promociones']);

    //Permisos para usuarios
    Permission::firstOrCreate(['permission_name' => 'tdtypes_index', 'permission_description' => 'Ver Lista de Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'tdtypes_show', 'permission_description' => 'Ver detalles de Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'tdtypes_store', 'permission_description' => 'Crear nuevos Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'tdtypes_search', 'permission_description' => 'Buscar Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'tdtypes_update', 'permission_description' => 'Actualizar Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'tdtypes_destroy', 'permission_description' => 'Eliminar Tipos de Detalles de facturas']);

    //Permisos para proveedores
    Permission::firstOrCreate(['permission_name' => 'providers_index', 'permission_description' => 'Ver Lista de proveedores']);
    Permission::firstOrCreate(['permission_name' => 'providers_show', 'permission_description' => 'Ver detalles de proveedores']);
    Permission::firstOrCreate(['permission_name' => 'providers_store', 'permission_description' => 'Crear nuevos proveedores']);
    Permission::firstOrCreate(['permission_name' => 'providers_search', 'permission_description' => 'Buscar proveedores']);
    Permission::firstOrCreate(['permission_name' => 'providers_update', 'permission_description' => 'Actualizar proveedores']);
    Permission::firstOrCreate(['permission_name' => 'providers_destroy', 'permission_description' => 'Eliminar proveedores']);

    //Permisos para salarios de docentes
    Permission::firstOrCreate(['permission_name' => 'professor_account_index', 'permission_description' => 'Ver Lista de Salarios de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_show', 'permission_description' => 'Ver detalles de Salarios de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_store', 'permission_description' => 'Crear nuevos Salarios de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_search', 'permission_description' => 'Buscar Salarios de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_update', 'permission_description' => 'Actualizar Salarios de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_destroy', 'permission_description' => 'Eliminar Salarios de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_admin_authorize', 'permission_description' => 'Administracion Autorizar Pago de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_sec_gral_authorize', 'permission_description' => 'Sec. Gral. Autorizar Pago de Docentes']);
    Permission::firstOrCreate(['permission_name' => 'professor_account_sec_authorize', 'permission_description' => 'Secretaria Autorizar Pago de Docentes']);

    //Permisos para salario de funcionarios
    Permission::firstOrCreate(['permission_name' => 'employee_account_report', 'permission_description' => 'Ver Reporte de Salarios de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_account_index', 'permission_description' => 'Ver Lista de Salarios de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_account_show', 'permission_description' => 'Ver detalles de Salarios de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_account_store', 'permission_description' => 'Crear nuevos Salarios de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_account_search', 'permission_description' => 'Buscar Salarios de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_account_update', 'permission_description' => 'Actualizar Salarios de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_account_destroy', 'permission_description' => 'Eliminar Salarios de Funcionarios']);


    //Permisos para proveedores
    Permission::firstOrCreate(['permission_name' => 'provider_account_index', 'permission_description' => 'Ver Lista de Facturas de proveedores']);
    Permission::firstOrCreate(['permission_name' => 'provider_account_show', 'permission_description' => 'Ver detalles de Facturas de proveedores']);
    Permission::firstOrCreate(['permission_name' => 'provider_account_store', 'permission_description' => 'Crear nuevas Facturas de proveedores']);
    Permission::firstOrCreate(['permission_name' => 'provider_account_search', 'permission_description' => 'Buscar Facturas de proveedores']);
    Permission::firstOrCreate(['permission_name' => 'provider_account_update', 'permission_description' => 'Actualizar Facturas de proveedores']);
    Permission::firstOrCreate(['permission_name' => 'provider_account_destroy', 'permission_description' => 'Eliminar Facturas de proveedores']);

    //Permisos para planes de cuenta
    Permission::firstOrCreate(['permission_name' => 'accounting_plan_index', 'permission_description' => 'Ver Lista de Planes de cuenta']);
    Permission::firstOrCreate(['permission_name' => 'accounting_plan_show', 'permission_description' => 'Ver detalles de Planes de cuenta']);
    Permission::firstOrCreate(['permission_name' => 'accounting_plan_store', 'permission_description' => 'Crear nuevas Planes de cuenta']);
    Permission::firstOrCreate(['permission_name' => 'accounting_plan_search', 'permission_description' => 'Buscar Planes de cuenta']);
    Permission::firstOrCreate(['permission_name' => 'accounting_plan_update', 'permission_description' => 'Actualizar Planes de cuenta']);
    Permission::firstOrCreate(['permission_name' => 'accounting_plan_destroy', 'permission_description' => 'Eliminar Planes de cuenta']);


    //Permisos para evaluaciones por alumnos
    Permission::firstOrCreate(['permission_name' => 'evalstudent_index', 'permission_description' => 'Ver Lista de Evaluaciones por Alumno']);
    Permission::firstOrCreate(['permission_name' => 'evalstudent_show', 'permission_description' => 'Ver detalles de Evaluaciones por Alumno']);
    Permission::firstOrCreate(['permission_name' => 'evalstudent_store', 'permission_description' => 'Crear nuevas Evaluaciones por Alumno']);
    Permission::firstOrCreate(['permission_name' => 'evalstudent_update', 'permission_description' => 'Actualizar Evaluaciones por Alumno']);
    Permission::firstOrCreate(['permission_name' => 'evalstudent_authorise_paid', 'permission_description' => 'Autorizar pago de Evaluaciones por Alumno']);
    Permission::firstOrCreate(['permission_name' => 'evalstudent_destroy', 'permission_description' => 'Eliminar Evaluaciones por Alumno']);

    //Permisos para estado de cuenta de alumnos
    Permission::firstOrCreate(['permission_name' => 'staccount_index', 'permission_description' => 'Ver Lista de Estado de cuenta de alumnos']);
    Permission::firstOrCreate(['permission_name' => 'staccount_show', 'permission_description' => 'Ver detalles de Estado de cuenta de alumnos']);
    Permission::firstOrCreate(['permission_name' => 'staccount_store', 'permission_description' => 'Crear nuevas Estado de cuenta de alumnos']);
    Permission::firstOrCreate(['permission_name' => 'staccount_update', 'permission_description' => 'Actualizar Estado de cuenta de alumnos']);
    Permission::firstOrCreate(['permission_name' => 'staccount_destroy', 'permission_description' => 'Eliminar Estado de cuenta de alumnos']);

    //Permisos para facturas
    Permission::firstOrCreate(['permission_name' => 'ticket_index', 'permission_description' => 'Ver Lista de facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_show', 'permission_description' => 'Ver detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_store', 'permission_description' => 'Crear nuevas facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_update', 'permission_description' => 'Actualizar facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_destroy', 'permission_description' => 'Eliminar facturas']);

    //Permisos para detalles facturas
    Permission::firstOrCreate(['permission_name' => 'ticket_details_index', 'permission_description' => 'Ver Lista de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_details_show', 'permission_description' => 'Ver detalles de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_details_store', 'permission_description' => 'Crear nuevas Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_details_update', 'permission_description' => 'Actualizar Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'ticket_details_destroy', 'permission_description' => 'Eliminar Detalles de facturas']);

    //Permisos para detalles facturas
    Permission::firstOrCreate(['permission_name' => 'td_types_index', 'permission_description' => 'Ver Lista de Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'td_types_show', 'permission_description' => 'Ver detalles de Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'td_types_store', 'permission_description' => 'Crear nuevas Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'td_types_update', 'permission_description' => 'Actualizar Tipos de Detalles de facturas']);
    Permission::firstOrCreate(['permission_name' => 'td_types_destroy', 'permission_description' => 'Eliminar Tipos de Detalles de facturas']);

    //Permisos para detalles facturas
    Permission::firstOrCreate(['permission_name' => 'career_reqdoc_index', 'permission_description' => 'Ver Lista de Documentos Requeridos por Carreras']);
    Permission::firstOrCreate(['permission_name' => 'career_reqdoc_show', 'permission_description' => 'Ver detalles de Documentos Requeridos por Carreras']);
    Permission::firstOrCreate(['permission_name' => 'career_reqdoc_store', 'permission_description' => 'Crear nuevos Documentos Requeridos por Carreras']);
    Permission::firstOrCreate(['permission_name' => 'career_reqdoc_update', 'permission_description' => 'Actualizar Documentos Requeridos por Carreras']);
    Permission::firstOrCreate(['permission_name' => 'career_reqdoc_destroy', 'permission_description' => 'Eliminar Documentos Requeridos por Carreras']);


    //Permisos para roles
    Permission::firstOrCreate(['permission_name' => 'roles_index', 'permission_description' => 'Ver Lista de roles']);
    Permission::firstOrCreate(['permission_name' => 'roles_show', 'permission_description' => 'Ver detalles de roles']);
    Permission::firstOrCreate(['permission_name' => 'roles_store', 'permission_description' => 'Crear nuevos roles']);
    Permission::firstOrCreate(['permission_name' => 'roles_search', 'permission_description' => 'Buscar roles']);
    Permission::firstOrCreate(['permission_name' => 'roles_update', 'permission_description' => 'Actualizar roles']);
    Permission::firstOrCreate(['permission_name' => 'roles_destroy', 'permission_description' => 'Eliminar roles']);

    //Permisos para unit_types
    Permission::firstOrCreate(['permission_name' => 'unit_types_index', 'permission_description' => 'Ver Lista de tipos instituciones']);
    Permission::firstOrCreate(['permission_name' => 'unit_types_show', 'permission_description' => 'Ver detalles de tipos instituciones']);
    Permission::firstOrCreate(['permission_name' => 'unit_types_store', 'permission_description' => 'Crear nuevos tipos instituciones']);
    Permission::firstOrCreate(['permission_name' => 'unit_types_search', 'permission_description' => 'Buscar tipos instituciones']);
    Permission::firstOrCreate(['permission_name' => 'unit_types_update', 'permission_description' => 'Actualizar tipos instituciones']);
    Permission::firstOrCreate(['permission_name' => 'unit_types_destroy', 'permission_description' => 'Eliminar tipos instituciones']);

    //Permisos para units
    Permission::firstOrCreate(['permission_name' => 'units_index', 'permission_description' => 'Ver Lista de instituciones']);
    Permission::firstOrCreate(['permission_name' => 'units_show', 'permission_description' => 'Ver detalles de instituciones']);
    Permission::firstOrCreate(['permission_name' => 'units_store', 'permission_description' => 'Crear nuevas instituciones']);
    Permission::firstOrCreate(['permission_name' => 'units_search', 'permission_description' => 'Buscar instituciones']);
    Permission::firstOrCreate(['permission_name' => 'units_update', 'permission_description' => 'Actualizar instituciones']);
    Permission::firstOrCreate(['permission_name' => 'units_destroy', 'permission_description' => 'Eliminar instituciones']);

    //Permisos para careertypes
    Permission::firstOrCreate(['permission_name' => 'careertypes_index', 'permission_description' => 'Ver Lista de tipos de carreras']);
    Permission::firstOrCreate(['permission_name' => 'careertypes_show', 'permission_description' => 'Ver detalles de tipos de carreras']);
    Permission::firstOrCreate(['permission_name' => 'careertypes_store', 'permission_description' => 'Crear nuevos tipos de carreras']);
    Permission::firstOrCreate(['permission_name' => 'careertypes_search', 'permission_description' => 'Buscar tipos de carreras']);
    Permission::firstOrCreate(['permission_name' => 'careertypes_update', 'permission_description' => 'Actualizar tipos de carreras']);
    Permission::firstOrCreate(['permission_name' => 'careertypes_destroy', 'permission_description' => 'Eliminar tipos de carreras']);

    //Permisos para faculties
    Permission::firstOrCreate(['permission_name' => 'faculties_index', 'permission_description' => 'Ver Lista de facultades']);
    Permission::firstOrCreate(['permission_name' => 'faculties_show', 'permission_description' => 'Ver detalles de facultades']);
    Permission::firstOrCreate(['permission_name' => 'faculties_store', 'permission_description' => 'Crear nuevas facultades']);
    Permission::firstOrCreate(['permission_name' => 'faculties_search', 'permission_description' => 'Buscar facultades']);
    Permission::firstOrCreate(['permission_name' => 'faculties_update', 'permission_description' => 'Actualizar facultades']);
    Permission::firstOrCreate(['permission_name' => 'faculties_destroy', 'permission_description' => 'Eliminar facultades']);

    //Permisos para subjects
    Permission::firstOrCreate(['permission_name' => 'subjects_index', 'permission_description' => 'Ver Lista de materias']);
    Permission::firstOrCreate(['permission_name' => 'subjects_show', 'permission_description' => 'Ver detalles de materias']);
    Permission::firstOrCreate(['permission_name' => 'subj_price', 'permission_description' => 'Ver y editar precios por materia']);
    Permission::firstOrCreate(['permission_name' => 'subjects_store', 'permission_description' => 'Crear nuevas materias']);
    Permission::firstOrCreate(['permission_name' => 'subjects_search', 'permission_description' => 'Buscar materias']);
    Permission::firstOrCreate(['permission_name' => 'subjects_update', 'permission_description' => 'Actualizar materias']);
    Permission::firstOrCreate(['permission_name' => 'subjects_destroy', 'permission_description' => 'Eliminar materias']);

    //Permisos para prereqs
    Permission::firstOrCreate(['permission_name' => 'prereqs_index', 'permission_description' => 'Ver Lista de materias correlativas']);
    Permission::firstOrCreate(['permission_name' => 'prereqs_show', 'permission_description' => 'Ver detalles de materias correlativas']);
    Permission::firstOrCreate(['permission_name' => 'prereqs_store', 'permission_description' => 'Crear nuevas materias correlativas']);
    Permission::firstOrCreate(['permission_name' => 'prereqs_search', 'permission_description' => 'Buscar materias correlativas']);
    Permission::firstOrCreate(['permission_name' => 'prereqs_update', 'permission_description' => 'Actualizar materias correlativas']);
    Permission::firstOrCreate(['permission_name' => 'prereqs_destroy', 'permission_description' => 'Eliminar materias correlativas']);

    //Permisos para careers
    Permission::firstOrCreate(['permission_name' => 'careers_index', 'permission_description' => 'Ver Lista de carreras']);
    Permission::firstOrCreate(['permission_name' => 'careers_show', 'permission_description' => 'Ver detalles de carreras']);
    Permission::firstOrCreate(['permission_name' => 'careers_store', 'permission_description' => 'Crear nuevas carreras']);
    Permission::firstOrCreate(['permission_name' => 'careers_search', 'permission_description' => 'Buscar carreras']);
    Permission::firstOrCreate(['permission_name' => 'careers_update', 'permission_description' => 'Actualizar carreras']);
    Permission::firstOrCreate(['permission_name' => 'careers_destroy', 'permission_description' => 'Eliminar carreras']);

    //Permisos para scholarship
    Permission::firstOrCreate(['permission_name' => 'scholarship_index', 'permission_description' => 'Ver Lista de becas']);
    Permission::firstOrCreate(['permission_name' => 'scholarship_show', 'permission_description' => 'Ver detalles de becas']);
    Permission::firstOrCreate(['permission_name' => 'scholarship_store', 'permission_description' => 'Crear nuevas becas']);
    Permission::firstOrCreate(['permission_name' => 'scholarship_search', 'permission_description' => 'Buscar becas']);
    Permission::firstOrCreate(['permission_name' => 'scholarship_update', 'permission_description' => 'Actualizar becas']);
    Permission::firstOrCreate(['permission_name' => 'scholarship_destroy', 'permission_description' => 'Eliminar becas']);

    //Permisos para titles
    Permission::firstOrCreate(['permission_name' => 'titles_index', 'permission_description' => 'Ver Lista de titulos']);
    Permission::firstOrCreate(['permission_name' => 'titles_show', 'permission_description' => 'Ver detalles de titulos']);
    Permission::firstOrCreate(['permission_name' => 'titles_store', 'permission_description' => 'Crear nuevos titulos']);
    Permission::firstOrCreate(['permission_name' => 'titles_search', 'permission_description' => 'Buscar titulos']);
    Permission::firstOrCreate(['permission_name' => 'titles_update', 'permission_description' => 'Actualizar titulos']);
    Permission::firstOrCreate(['permission_name' => 'titles_destroy', 'permission_description' => 'Eliminar titulos']);

    //Permisos para hstitle
    Permission::firstOrCreate(['permission_name' => 'hstitle_index', 'permission_description' => 'Ver Lista de bachilleratos']);
    Permission::firstOrCreate(['permission_name' => 'hstitle_show', 'permission_description' => 'Ver detalles de bachilleratos']);
    Permission::firstOrCreate(['permission_name' => 'hstitle_store', 'permission_description' => 'Crear nuevos bachilleratos']);
    Permission::firstOrCreate(['permission_name' => 'hstitle_search', 'permission_description' => 'Buscar bachilleratos']);
    Permission::firstOrCreate(['permission_name' => 'hstitle_update', 'permission_description' => 'Actualizar bachilleratos']);
    Permission::firstOrCreate(['permission_name' => 'hstitle_destroy', 'permission_description' => 'Eliminar bachilleratos']);

    //Permisos para facuCarAdm
    Permission::firstOrCreate(['permission_name' => 'facuCarAdm_index', 'permission_description' => 'Ver Lista de carreras en administración']);
    Permission::firstOrCreate(['permission_name' => 'facuCarAdm_show', 'permission_description' => 'Ver detalles de carreras en administración']);
    Permission::firstOrCreate(['permission_name' => 'facuCarAdm_store', 'permission_description' => 'Crear nuevas carreras en administración']);
    Permission::firstOrCreate(['permission_name' => 'facuCarAdm_search', 'permission_description' => 'Buscar carreras en administración']);
    Permission::firstOrCreate(['permission_name' => 'facuCarAdm_update', 'permission_description' => 'Actualizar carreras en administración']);
    Permission::firstOrCreate(['permission_name' => 'facuCarAdm_destroy', 'permission_description' => 'Eliminar carreras en administración']);

    //Permisos para semesters
    Permission::firstOrCreate(['permission_name' => 'semesters_index', 'permission_description' => 'Ver Lista de semestres']);
    Permission::firstOrCreate(['permission_name' => 'semesters_show', 'permission_description' => 'Ver detalles de semestres']);
    Permission::firstOrCreate(['permission_name' => 'semesters_store', 'permission_description' => 'Crear nuevos semestres']);
    Permission::firstOrCreate(['permission_name' => 'semesters_search', 'permission_description' => 'Buscar semestres']);
    Permission::firstOrCreate(['permission_name' => 'semesters_update', 'permission_description' => 'Actualizar semestres']);
    Permission::firstOrCreate(['permission_name' => 'semesters_destroy', 'permission_description' => 'Eliminar semestres']);

    //Permisos para evaluationTypes
    Permission::firstOrCreate(['permission_name' => 'evaluationTypes_index', 'permission_description' => 'Ver Lista de tipos de evaluación']);
    Permission::firstOrCreate(['permission_name' => 'evaluationTypes_show', 'permission_description' => 'Ver detalles de tipos de evaluación']);
    Permission::firstOrCreate(['permission_name' => 'evaluationTypes_store', 'permission_description' => 'Crear nuevos tipos de evaluación']);
    Permission::firstOrCreate(['permission_name' => 'evaluationTypes_search', 'permission_description' => 'Buscar tipos de evaluación']);
    Permission::firstOrCreate(['permission_name' => 'evaluationTypes_update', 'permission_description' => 'Actualizar tipos de evaluación']);
    Permission::firstOrCreate(['permission_name' => 'evaluationTypes_destroy', 'permission_description' => 'Eliminar tipos de evaluación']);

    //Permisos para retentionStudent
    Permission::firstOrCreate(['permission_name' => 'retentionParameter_index', 'permission_description' => 'Ver Lista de parametros de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionParameter_show', 'permission_description' => 'Ver detalles de parametros de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionParameter_store', 'permission_description' => 'Crear nuevos parametros de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionParameter_search', 'permission_description' => 'Buscar parametros de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionParameter_update', 'permission_description' => 'Actualizar parametros de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionParameter_destroy', 'permission_description' => 'Eliminar parametros de restenciones']);

    //Permisos para retentionStudent
    Permission::firstOrCreate(['permission_name' => 'retentionQuestion_index', 'permission_description' => 'Ver Lista de Preguntas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionQuestion_show', 'permission_description' => 'Ver detalles de Preguntas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionQuestion_store', 'permission_description' => 'Crear nuevas Preguntas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionQuestion_search', 'permission_description' => 'Buscar Preguntas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionQuestion_update', 'permission_description' => 'Actualizar Preguntas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionQuestion_destroy', 'permission_description' => 'Eliminar Preguntas de restenciones']);

    //Permisos para retentionStudent
    Permission::firstOrCreate(['permission_name' => 'retentionAnswer_index', 'permission_description' => 'Ver Lista de Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionAnswer_show', 'permission_description' => 'Ver detalles de Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionAnswer_store', 'permission_description' => 'Crear nuevas Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionAnswer_search', 'permission_description' => 'Buscar Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionAnswer_update', 'permission_description' => 'Actualizar Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionAnswer_destroy', 'permission_description' => 'Eliminar Respuestas de restenciones']);

    //Permisos para holiday
    Permission::firstOrCreate(['permission_name' => 'holiday_index', 'permission_description' => 'Ver Lista de Feriados']);
    Permission::firstOrCreate(['permission_name' => 'holiday_show', 'permission_description' => 'Ver detalles de Feriados']);
    Permission::firstOrCreate(['permission_name' => 'holiday_store', 'permission_description' => 'Crear nuevas Feriados']);
    Permission::firstOrCreate(['permission_name' => 'holiday_search', 'permission_description' => 'Buscar Feriados']);
    Permission::firstOrCreate(['permission_name' => 'holiday_update', 'permission_description' => 'Actualizar Feriados']);
    Permission::firstOrCreate(['permission_name' => 'holiday_destroy', 'permission_description' => 'Eliminar Feriados']);

    //Permisos para holiday
    Permission::firstOrCreate(['permission_name' => 'objectives_index', 'permission_description' => 'Ver Lista de Objetivos']);
    Permission::firstOrCreate(['permission_name' => 'objectives_show', 'permission_description' => 'Ver detalles de Objetivos']);
    Permission::firstOrCreate(['permission_name' => 'objectives_store', 'permission_description' => 'Crear nuevas Objetivos']);
    Permission::firstOrCreate(['permission_name' => 'objectives_search', 'permission_description' => 'Buscar Objetivos']);
    Permission::firstOrCreate(['permission_name' => 'objectives_update', 'permission_description' => 'Actualizar Objetivos']);
    Permission::firstOrCreate(['permission_name' => 'objectives_destroy', 'permission_description' => 'Eliminar Objetivos']);


    //Permisos para retentionStudent
    Permission::firstOrCreate(['permission_name' => 'retentionStudent_index', 'permission_description' => 'Ver Lista de Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionStudent_show', 'permission_description' => 'Ver detalles de Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionStudent_store', 'permission_description' => 'Crear nuevas Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionStudent_search', 'permission_description' => 'Buscar Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionStudent_update', 'permission_description' => 'Actualizar Respuestas de restenciones']);
    Permission::firstOrCreate(['permission_name' => 'retentionStudent_destroy', 'permission_description' => 'Eliminar Respuestas de restenciones']);

    //Permisos para studentScholarship
    Permission::firstOrCreate(['permission_name' => 'studentScholarship_index', 'permission_description' => 'Ver Lista de becas por estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentScholarship_show', 'permission_description' => 'Ver detalles de becas por estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentScholarship_store', 'permission_description' => 'Crear nuevas becas por estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentScholarship_search', 'permission_description' => 'Buscar becas por estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentScholarship_update', 'permission_description' => 'Actualizar becas por estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentScholarship_destroy', 'permission_description' => 'Eliminar becas por estudiantes']);

    //Permisos para countries
    Permission::firstOrCreate(['permission_name' => 'countries_index', 'permission_description' => 'Ver Lista de paises']);
    Permission::firstOrCreate(['permission_name' => 'countries_show', 'permission_description' => 'Ver detalles de paises']);
    Permission::firstOrCreate(['permission_name' => 'countries_store', 'permission_description' => 'Crear nuevos paises']);
    Permission::firstOrCreate(['permission_name' => 'countries_search', 'permission_description' => 'Buscar paises']);
    Permission::firstOrCreate(['permission_name' => 'countries_update', 'permission_description' => 'Actualizar paises']);
    Permission::firstOrCreate(['permission_name' => 'countries_destroy', 'permission_description' => 'Eliminar paises']);

    //Permisos para departments
    Permission::firstOrCreate(['permission_name' => 'departments_index', 'permission_description' => 'Ver Lista de departamentos']);
    Permission::firstOrCreate(['permission_name' => 'departments_show', 'permission_description' => 'Ver detalles de departamentos']);
    Permission::firstOrCreate(['permission_name' => 'departments_store', 'permission_description' => 'Crear nuevos departamentos']);
    Permission::firstOrCreate(['permission_name' => 'departments_search', 'permission_description' => 'Buscar departamentos']);
    Permission::firstOrCreate(['permission_name' => 'departments_update', 'permission_description' => 'Actualizar departamentos']);
    Permission::firstOrCreate(['permission_name' => 'departments_destroy', 'permission_description' => 'Eliminar departamentos']);

    //Permisos para requiredDocs
    Permission::firstOrCreate(['permission_name' => 'requiredDocs_index', 'permission_description' => 'Ver Lista de documentos requeridos']);
    Permission::firstOrCreate(['permission_name' => 'requiredDocs_show', 'permission_description' => 'Ver detalles de documentos requeridos']);
    Permission::firstOrCreate(['permission_name' => 'requiredDocs_store', 'permission_description' => 'Crear nuevos documentos requeridos']);
    Permission::firstOrCreate(['permission_name' => 'requiredDocs_search', 'permission_description' => 'Buscar documentos requeridos']);
    Permission::firstOrCreate(['permission_name' => 'requiredDocs_update', 'permission_description' => 'Actualizar documentos requeridos']);
    Permission::firstOrCreate(['permission_name' => 'requiredDocs_destroy', 'permission_description' => 'Eliminar documentos requeridos']);

    //Permisos para healtCares
    Permission::firstOrCreate(['permission_name' => 'healtCares_index', 'permission_description' => 'Ver Lista de seguros medicos']);
    Permission::firstOrCreate(['permission_name' => 'healtCares_show', 'permission_description' => 'Ver detalles de seguros medicos']);
    Permission::firstOrCreate(['permission_name' => 'healtCares_store', 'permission_description' => 'Crear nuevos seguros medicos']);
    Permission::firstOrCreate(['permission_name' => 'healtCares_search', 'permission_description' => 'Buscar seguros medicos']);
    Permission::firstOrCreate(['permission_name' => 'healtCares_update', 'permission_description' => 'Actualizar seguros medicos']);
    Permission::firstOrCreate(['permission_name' => 'healtCares_destroy', 'permission_description' => 'Eliminar seguros medicos']);

    //Permisos para operations
    Permission::firstOrCreate(['permission_name' => 'operations_index', 'permission_description' => 'Ver Lista de gestiones']);
    Permission::firstOrCreate(['permission_name' => 'operations_show', 'permission_description' => 'Ver detalles de gestiones']);
    Permission::firstOrCreate(['permission_name' => 'operations_store', 'permission_description' => 'Crear nuevas gestiones']);
    Permission::firstOrCreate(['permission_name' => 'operations_search', 'permission_description' => 'Buscar gestiones']);
    Permission::firstOrCreate(['permission_name' => 'operations_update', 'permission_description' => 'Actualizar gestiones']);
    Permission::firstOrCreate(['permission_name' => 'operations_destroy', 'permission_description' => 'Eliminar gestiones']);

    //Permisos para workplaces
    Permission::firstOrCreate(['permission_name' => 'workplaces_index', 'permission_description' => 'Ver Lista de lugares de trabajo']);
    Permission::firstOrCreate(['permission_name' => 'workplaces_show', 'permission_description' => 'Ver detalles de lugares de trabajo']);
    Permission::firstOrCreate(['permission_name' => 'workplaces_store', 'permission_description' => 'Crear nuevos lugares de trabajo']);
    Permission::firstOrCreate(['permission_name' => 'workplaces_search', 'permission_description' => 'Buscar lugares de trabajo']);
    Permission::firstOrCreate(['permission_name' => 'workplaces_update', 'permission_description' => 'Actualizar lugares de trabajo']);
    Permission::firstOrCreate(['permission_name' => 'workplaces_destroy', 'permission_description' => 'Eliminar lugares de trabajo']);
    //Permisos para StudentWorkplaces
    Permission::firstOrCreate(['permission_name' => 'StudentWorkplaces_index', 'permission_description' => 'Ver Lista de lugares de trabajo por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'StudentWorkplaces_show', 'permission_description' => 'Ver detalles de lugares de trabajo por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'StudentWorkplaces_store', 'permission_description' => 'Crear nuevos lugares de trabajo por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'StudentWorkplaces_search', 'permission_description' => 'Buscar lugares de trabajo por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'StudentWorkplaces_update', 'permission_description' => 'Actualizar lugares de trabajo por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'StudentWorkplaces_destroy', 'permission_description' => 'Eliminar lugares de trabajo por estudiante']);

    //Permisos para documentPerson
    Permission::firstOrCreate(['permission_name' => 'documentPerson_index', 'permission_description' => 'Ver Lista de documentos por personas']);
    Permission::firstOrCreate(['permission_name' => 'documentPerson_show', 'permission_description' => 'Ver detalles de documentos por personas']);
    Permission::firstOrCreate(['permission_name' => 'documentPerson_store', 'permission_description' => 'Crear nuevos documentos por personas']);
    Permission::firstOrCreate(['permission_name' => 'documentPerson_search', 'permission_description' => 'Buscar documentos por personas']);
    Permission::firstOrCreate(['permission_name' => 'documentPerson_update', 'permission_description' => 'Actualizar documentos por personas']);
    Permission::firstOrCreate(['permission_name' => 'documentPerson_destroy', 'permission_description' => 'Eliminar documentos por personas']);

    //Permisos para persons
    Permission::firstOrCreate(['permission_name' => 'persons_index', 'permission_description' => 'Ver Lista de personas']);
    Permission::firstOrCreate(['permission_name' => 'persons_show', 'permission_description' => 'Ver detalles de personas']);
    Permission::firstOrCreate(['permission_name' => 'persons_store', 'permission_description' => 'Crear nuevas personas']);
    Permission::firstOrCreate(['permission_name' => 'persons_search', 'permission_description' => 'Buscar personas']);
    Permission::firstOrCreate(['permission_name' => 'persons_update', 'permission_description' => 'Actualizar personas']);
    Permission::firstOrCreate(['permission_name' => 'persons_destroy', 'permission_description' => 'Eliminar personas']);

    //Permisos para students
    Permission::firstOrCreate(['permission_name' => 'students_index', 'permission_description' => 'Ver Lista de estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'students_show', 'permission_description' => 'Ver detalles de estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'students_store', 'permission_description' => 'Crear nuevos estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'students_search', 'permission_description' => 'Buscar estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'students_update', 'permission_description' => 'Actualizar estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'students_destroy', 'permission_description' => 'Eliminar estudiantes']);

    //Permisos para employees
    Permission::firstOrCreate(['permission_name' => 'employees_index', 'permission_description' => 'Ver Lista de funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employees_show', 'permission_description' => 'Ver detalles de funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employees_store', 'permission_description' => 'Crear nuevos funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employees_search', 'permission_description' => 'Buscar funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employees_update', 'permission_description' => 'Actualizar funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employees_destroy', 'permission_description' => 'Eliminar funcionarios']);

    //Permisos para cities
    Permission::firstOrCreate(['permission_name' => 'cities_index', 'permission_description' => 'Ver Lista de ciudades']);
    Permission::firstOrCreate(['permission_name' => 'cities_show', 'permission_description' => 'Ver detalles de ciudades']);
    Permission::firstOrCreate(['permission_name' => 'cities_store', 'permission_description' => 'Crear nuevas ciudades']);
    Permission::firstOrCreate(['permission_name' => 'cities_search', 'permission_description' => 'Buscar ciudades']);
    Permission::firstOrCreate(['permission_name' => 'cities_update', 'permission_description' => 'Actualizar ciudades']);
    Permission::firstOrCreate(['permission_name' => 'cities_destroy', 'permission_description' => 'Eliminar ciudades']);

    //Permisos para contactTypes
    Permission::firstOrCreate(['permission_name' => 'contactTypes_index', 'permission_description' => 'Ver Lista de tipos de contacto']);
    Permission::firstOrCreate(['permission_name' => 'contactTypes_show', 'permission_description' => 'Ver detalles de tipos de contacto']);
    Permission::firstOrCreate(['permission_name' => 'contactTypes_store', 'permission_description' => 'Crear nuevos tipos de contacto']);
    Permission::firstOrCreate(['permission_name' => 'contactTypes_search', 'permission_description' => 'Buscar tipos de contacto']);
    Permission::firstOrCreate(['permission_name' => 'contactTypes_update', 'permission_description' => 'Actualizar tipos de contacto']);
    Permission::firstOrCreate(['permission_name' => 'contactTypes_destroy', 'permission_description' => 'Eliminar tipos de contacto']);

    //Permisos para facuCareers
    Permission::firstOrCreate(['permission_name' => 'facuCareers_index', 'permission_description' => 'Ver Lista de carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facuCareers_show', 'permission_description' => 'Ver detalles de carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facuCareers_store', 'permission_description' => 'Crear nuevas carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facuCareers_search', 'permission_description' => 'Buscar carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facuCareers_update', 'permission_description' => 'Actualizar carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facuCareers_destroy', 'permission_description' => 'Eliminar carreras por facultad']);

    //Permisos para profile_types
    Permission::firstOrCreate(['permission_name' => 'profile_types_index', 'permission_description' => 'Ver Lista de tipos de perfiles']);
    Permission::firstOrCreate(['permission_name' => 'profile_types_show', 'permission_description' => 'Ver detalles de tipos de perfiles']);
    Permission::firstOrCreate(['permission_name' => 'profile_types_store', 'permission_description' => 'Crear nuevos tipos de perfiles']);
    Permission::firstOrCreate(['permission_name' => 'profile_types_search', 'permission_description' => 'Buscar tipos de perfiles']);
    Permission::firstOrCreate(['permission_name' => 'profile_types_update', 'permission_description' => 'Actualizar tipos de perfiles']);
    Permission::firstOrCreate(['permission_name' => 'profile_types_destroy', 'permission_description' => 'Eliminar tipos de perfiles']);

    //Permisos para payplans
    Permission::firstOrCreate(['permission_name' => 'payplans_index', 'permission_description' => 'Ver Lista de planes de pagos']);
    Permission::firstOrCreate(['permission_name' => 'payplans_show', 'permission_description' => 'Ver detalles de planes de pagos']);
    Permission::firstOrCreate(['permission_name' => 'payplans_store', 'permission_description' => 'Crear nuevos planes de pagos']);
    Permission::firstOrCreate(['permission_name' => 'payplans_search', 'permission_description' => 'Buscar planes de pagos']);
    Permission::firstOrCreate(['permission_name' => 'payplans_update', 'permission_description' => 'Actualizar planes de pagos']);
    Permission::firstOrCreate(['permission_name' => 'payplans_destroy', 'permission_description' => 'Eliminar planes de pagos']);

    //Permisos para ivaType
    Permission::firstOrCreate(['permission_name' => 'ivaType_index', 'permission_description' => 'Ver Lista de tipos de iva']);
    Permission::firstOrCreate(['permission_name' => 'ivaType_show', 'permission_description' => 'Ver detalles de tipos de iva']);
    Permission::firstOrCreate(['permission_name' => 'ivaType_store', 'permission_description' => 'Crear nuevos tipos de iva']);
    Permission::firstOrCreate(['permission_name' => 'ivaType_search', 'permission_description' => 'Buscar tipos de iva']);
    Permission::firstOrCreate(['permission_name' => 'ivaType_update', 'permission_description' => 'Actualizar tipos de iva']);
    Permission::firstOrCreate(['permission_name' => 'ivaType_destroy', 'permission_description' => 'Eliminar tipos de iva']);

    //Permisos para appointments
    Permission::firstOrCreate(['permission_name' => 'appointments_index', 'permission_description' => 'Ver Lista de cargos']);
    Permission::firstOrCreate(['permission_name' => 'appointments_show', 'permission_description' => 'Ver detalles de cargos']);
    Permission::firstOrCreate(['permission_name' => 'appointments_store', 'permission_description' => 'Crear nuevos cargos']);
    Permission::firstOrCreate(['permission_name' => 'appointments_search', 'permission_description' => 'Buscar cargos']);
    Permission::firstOrCreate(['permission_name' => 'appointments_update', 'permission_description' => 'Actualizar cargos']);
    Permission::firstOrCreate(['permission_name' => 'appointments_destroy', 'permission_description' => 'Eliminar cargos']);
    //Permisos para Products
    Permission::firstOrCreate(['permission_name' => 'products_index', 'permission_description' => 'Ver Lista de productos']);
    Permission::firstOrCreate(['permission_name' => 'products_show', 'permission_description' => 'Ver detalles de productos']);
    Permission::firstOrCreate(['permission_name' => 'products_store', 'permission_description' => 'Crear nuevos productos']);
    Permission::firstOrCreate(['permission_name' => 'products_search', 'permission_description' => 'Buscar productos']);
    Permission::firstOrCreate(['permission_name' => 'products_update', 'permission_description' => 'Actualizar productos']);
    Permission::firstOrCreate(['permission_name' => 'products_destroy', 'permission_description' => 'Eliminar productos']);
    //Permisos para schools
    Permission::firstOrCreate(['permission_name' => 'schools_index', 'permission_description' => 'Ver Lista de colegios']);
    Permission::firstOrCreate(['permission_name' => 'schools_show', 'permission_description' => 'Ver detalles de colegios']);
    Permission::firstOrCreate(['permission_name' => 'schools_store', 'permission_description' => 'Crear nuevos colegios']);
    Permission::firstOrCreate(['permission_name' => 'schools_search', 'permission_description' => 'Buscar colegios']);
    Permission::firstOrCreate(['permission_name' => 'schools_update', 'permission_description' => 'Actualizar colegios']);
    Permission::firstOrCreate(['permission_name' => 'schools_destroy', 'permission_description' => 'Eliminar colegios']);

    //Permisos para tills
    Permission::firstOrCreate(['permission_name' => 'tills_index', 'permission_description' => 'Ver Lista de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_show', 'permission_description' => 'Ver detalles de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_store', 'permission_description' => 'Crear nuevas cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_search', 'permission_description' => 'Buscar cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_update', 'permission_description' => 'Actualizar cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_destroy', 'permission_description' => 'Eliminar cajas']);

    //Permisos para tillsDetails
    Permission::firstOrCreate(['permission_name' => 'tills_details_modify_date', 'permission_description' => 'Modificar fecha de movimientos de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_details_index', 'permission_description' => 'Ver Lista Detalles de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_details_show', 'permission_description' => 'Ver detalles de Detalles de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_details_store', 'permission_description' => 'Crear nuevos Detalles de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_details_search', 'permission_description' => 'Buscar Detalles de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_details_update', 'permission_description' => 'Actualizar Detalles de cajas']);
    Permission::firstOrCreate(['permission_name' => 'tills_details_destroy', 'permission_description' => 'Eliminar Detalles de cajas']);

    //Permisos para tillsTransfers
    Permission::firstOrCreate(['permission_name' => 'till_transfers_index', 'permission_description' => 'Ver Lista de Transferencias de cajas']);
    Permission::firstOrCreate(['permission_name' => 'till_transfers_show', 'permission_description' => 'Ver detalles de Transferencias de cajas']);
    Permission::firstOrCreate(['permission_name' => 'till_transfers_store', 'permission_description' => 'Crear nuevas Transferencias de cajas']);
    Permission::firstOrCreate(['permission_name' => 'till_transfers_search', 'permission_description' => 'Buscar Transferencias de cajas']);
    Permission::firstOrCreate(['permission_name' => 'till_transfers_update', 'permission_description' => 'Actualizar Transferencias de cajas']);
    Permission::firstOrCreate(['permission_name' => 'till_transfers_destroy', 'permission_description' => 'Eliminar Transferencias de cajas']);

    //Permisos para contactPersons
    Permission::firstOrCreate(['permission_name' => 'contactPersons_index', 'permission_description' => 'Ver Lista de contactos por personas']);
    Permission::firstOrCreate(['permission_name' => 'contactPersons_show', 'permission_description' => 'Ver detalles de contactos por personas']);
    Permission::firstOrCreate(['permission_name' => 'contactPersons_store', 'permission_description' => 'Crear nuevos contactos por personas']);
    Permission::firstOrCreate(['permission_name' => 'contactPersons_search', 'permission_description' => 'Buscar contactos por personas']);
    Permission::firstOrCreate(['permission_name' => 'contactPersons_update', 'permission_description' => 'Actualizar contactos por personas']);
    Permission::firstOrCreate(['permission_name' => 'contactPersons_destroy', 'permission_description' => 'Eliminar contactos por personas']);

    //Permisos para tariffs
    Permission::firstOrCreate(['permission_name' => 'tariffs_index', 'permission_description' => 'Ver Lista de tarifas']);
    Permission::firstOrCreate(['permission_name' => 'tariffs_show', 'permission_description' => 'Ver detalles de tarifas']);
    Permission::firstOrCreate(['permission_name' => 'tariffs_store', 'permission_description' => 'Crear nuevas tarifas']);
    Permission::firstOrCreate(['permission_name' => 'tariffs_search', 'permission_description' => 'Buscar tarifas']);
    Permission::firstOrCreate(['permission_name' => 'tariffs_update', 'permission_description' => 'Actualizar tarifas']);
    Permission::firstOrCreate(['permission_name' => 'tariffs_destroy', 'permission_description' => 'Eliminar tarifas']);

    //Permisos para edificios
    Permission::firstOrCreate(['permission_name' => 'edifice_index', 'permission_description' => 'Ver Lista de Edificios']);
    Permission::firstOrCreate(['permission_name' => 'edifice_show', 'permission_description' => 'Ver detalles de Edificios']);
    Permission::firstOrCreate(['permission_name' => 'edifice_store', 'permission_description' => 'Crear nuevas Edificios']);
    Permission::firstOrCreate(['permission_name' => 'edifice_search', 'permission_description' => 'Buscar Edificios']);
    Permission::firstOrCreate(['permission_name' => 'edifice_update', 'permission_description' => 'Actualizar Edificios']);
    Permission::firstOrCreate(['permission_name' => 'edifice_destroy', 'permission_description' => 'Eliminar Edificios']);

    //Permisos para tipo sala
    Permission::firstOrCreate(['permission_name' => 'classroomtype_index', 'permission_description' => 'Ver Lista de Tipos de Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroomtype_show', 'permission_description' => 'Ver detalles de Tipos de Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroomtype_store', 'permission_description' => 'Crear nuevas Tipos de Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroomtype_search', 'permission_description' => 'Buscar Tipos de Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroomtype_update', 'permission_description' => 'Actualizar Tipos de Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroomtype_destroy', 'permission_description' => 'Eliminar Tipos de Sala']);

    //Permisos para sala
    Permission::firstOrCreate(['permission_name' => 'classroom_index', 'permission_description' => 'Ver Lista de Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroom_show', 'permission_description' => 'Ver detalles de Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroom_store', 'permission_description' => 'Crear nuevas Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroom_search', 'permission_description' => 'Buscar Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroom_update', 'permission_description' => 'Actualizar Sala']);
    Permission::firstOrCreate(['permission_name' => 'classroom_destroy', 'permission_description' => 'Eliminar Sala']);

    //Permisos para secciones
    Permission::firstOrCreate(['permission_name' => 'section_index', 'permission_description' => 'Ver Lista de Secciones']);
    Permission::firstOrCreate(['permission_name' => 'section_show', 'permission_description' => 'Ver detalles de Secciones']);
    Permission::firstOrCreate(['permission_name' => 'section_store', 'permission_description' => 'Crear nuevas Secciones']);
    Permission::firstOrCreate(['permission_name' => 'section_search', 'permission_description' => 'Buscar Secciones']);
    Permission::firstOrCreate(['permission_name' => 'section_update', 'permission_description' => 'Actualizar Secciones']);
    Permission::firstOrCreate(['permission_name' => 'section_destroy', 'permission_description' => 'Eliminar Secciones']);

    //Permisos para secciones
    Permission::firstOrCreate(['permission_name' => 'reserve_index', 'permission_description' => 'Ver Lista de Reservas']);
    Permission::firstOrCreate(['permission_name' => 'reserve_show', 'permission_description' => 'Ver detalles de Reservas']);
    Permission::firstOrCreate(['permission_name' => 'reserve_store', 'permission_description' => 'Crear nuevas Reservas']);
    Permission::firstOrCreate(['permission_name' => 'reserve_search', 'permission_description' => 'Buscar Reservas']);
    Permission::firstOrCreate(['permission_name' => 'reserve_update', 'permission_description' => 'Actualizar Reservas']);
    Permission::firstOrCreate(['permission_name' => 'reserve_destroy', 'permission_description' => 'Eliminar Reservas']);

    //Permisos para secciones
    Permission::firstOrCreate(['permission_name' => 'category_index', 'permission_description' => 'Ver Lista de Categorias']);
    Permission::firstOrCreate(['permission_name' => 'category_show', 'permission_description' => 'Ver detalles de Categorias']);
    Permission::firstOrCreate(['permission_name' => 'category_store', 'permission_description' => 'Crear nuevas Categorias']);
    Permission::firstOrCreate(['permission_name' => 'category_search', 'permission_description' => 'Buscar Categorias']);
    Permission::firstOrCreate(['permission_name' => 'category_update', 'permission_description' => 'Actualizar Categorias']);
    Permission::firstOrCreate(['permission_name' => 'category_destroy', 'permission_description' => 'Eliminar Categorias']);

    //Permisos para secciones
    Permission::firstOrCreate(['permission_name' => 'employee_comission_index', 'permission_description' => 'Ver Lista de Comision de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_comission_show', 'permission_description' => 'Ver detalles de Comision de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_comission_store', 'permission_description' => 'Crear nuevas Comision de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_comission_search', 'permission_description' => 'Buscar Comision de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_comission_update', 'permission_description' => 'Actualizar Comision de Funcionarios']);
    Permission::firstOrCreate(['permission_name' => 'employee_comission_destroy', 'permission_description' => 'Eliminar Comision de Funcionarios']);

    //Permisos para secciones
    Permission::firstOrCreate(['permission_name' => 'consult_index', 'permission_description' => 'Ver Lista de Consultas']);
    Permission::firstOrCreate(['permission_name' => 'consult_show', 'permission_description' => 'Ver detalles de Consultas']);
    Permission::firstOrCreate(['permission_name' => 'consult_store', 'permission_description' => 'Crear nuevas Consultas']);
    Permission::firstOrCreate(['permission_name' => 'consult_search', 'permission_description' => 'Buscar Consultas']);
    Permission::firstOrCreate(['permission_name' => 'consult_update', 'permission_description' => 'Actualizar Consultas']);
    Permission::firstOrCreate(['permission_name' => 'consult_destroy', 'permission_description' => 'Eliminar Consultas']);


    //Permisos para facucars
    Permission::firstOrCreate(['permission_name' => 'facucars_index', 'permission_description' => 'Ver Lista de carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facucars_show', 'permission_description' => 'Ver detalles de carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facucars_store', 'permission_description' => 'Crear nuevas carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facucars_search', 'permission_description' => 'Buscar carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facucars_update', 'permission_description' => 'Actualizar carreras por facultad']);
    Permission::firstOrCreate(['permission_name' => 'facucars_destroy', 'permission_description' => 'Eliminar carreras por facultad']);

    //Permisos para carsubjs
    Permission::firstOrCreate(['permission_name' => 'carsubjs_index', 'permission_description' => 'Ver Lista de materias por carreras']);
    Permission::firstOrCreate(['permission_name' => 'carsubjs_show', 'permission_description' => 'Ver detalles de materias por carreras']);
    Permission::firstOrCreate(['permission_name' => 'carsubjs_store', 'permission_description' => 'Crear nuevas materias por carreras']);
    Permission::firstOrCreate(['permission_name' => 'carsubjs_search', 'permission_description' => 'Buscar materias por carreras']);
    Permission::firstOrCreate(['permission_name' => 'carsubjs_update', 'permission_description' => 'Actualizar materias por carreras']);
    Permission::firstOrCreate(['permission_name' => 'carsubjs_destroy', 'permission_description' => 'Eliminar materias por carreras']);

    //Permisos para enrolleds
    Permission::firstOrCreate(['permission_name' => 'enrolleds_index', 'permission_description' => 'Ver Lista de inscripciones']);
    Permission::firstOrCreate(['permission_name' => 'enrolleds_show', 'permission_description' => 'Ver detalles de inscripciones']);
    Permission::firstOrCreate(['permission_name' => 'enrolleds_store', 'permission_description' => 'Crear nuevas inscripciones']);
    Permission::firstOrCreate(['permission_name' => 'enrolleds_search', 'permission_description' => 'Buscar inscripciones']);
    Permission::firstOrCreate(['permission_name' => 'enrolleds_update', 'permission_description' => 'Actualizar inscripciones']);
    Permission::firstOrCreate(['permission_name' => 'enrolleds_destroy', 'permission_description' => 'Eliminar inscripciones']);

    //Permisos para professors
    Permission::firstOrCreate(['permission_name' => 'professors_index', 'permission_description' => 'Ver Lista de profesores']);
    Permission::firstOrCreate(['permission_name' => 'professors_show', 'permission_description' => 'Ver detalles de profesores']);
    Permission::firstOrCreate(['permission_name' => 'professors_store', 'permission_description' => 'Crear nuevos profesores']);
    Permission::firstOrCreate(['permission_name' => 'professors_search', 'permission_description' => 'Buscar profesores']);
    Permission::firstOrCreate(['permission_name' => 'professors_update', 'permission_description' => 'Actualizar profesores']);
    Permission::firstOrCreate(['permission_name' => 'professors_destroy', 'permission_description' => 'Eliminar profesores']);

    //Permisos para subevals
    Permission::firstOrCreate(['permission_name' => 'subevals_index', 'permission_description' => 'Ver Lista de evaluaciones por materias']);
    Permission::firstOrCreate(['permission_name' => 'subevals_show', 'permission_description' => 'Ver detalles de evaluaciones por materias']);
    Permission::firstOrCreate(['permission_name' => 'subevals_store', 'permission_description' => 'Crear nuevas evaluaciones por materias']);
    Permission::firstOrCreate(['permission_name' => 'subevals_search', 'permission_description' => 'Buscar evaluaciones por materias']);
    Permission::firstOrCreate(['permission_name' => 'subevals_update', 'permission_description' => 'Actualizar evaluaciones por materias']);
    Permission::firstOrCreate(['permission_name' => 'subevals_destroy', 'permission_description' => 'Eliminar evaluaciones por materias']);

    //Permisos para assistances
    Permission::firstOrCreate(['permission_name' => 'assistances_index', 'permission_description' => 'Ver Lista de asistencias']);
    Permission::firstOrCreate(['permission_name' => 'assistances_show', 'permission_description' => 'Ver detalles de asistencias']);
    Permission::firstOrCreate(['permission_name' => 'assistances_store', 'permission_description' => 'Crear nuevas asistencias']);
    Permission::firstOrCreate(['permission_name' => 'assistances_search', 'permission_description' => 'Buscar asistencias']);
    Permission::firstOrCreate(['permission_name' => 'assistances_update', 'permission_description' => 'Actualizar asistencias']);
    Permission::firstOrCreate(['permission_name' => 'assistances_destroy', 'permission_description' => 'Eliminar asistencias']);

    //Permisos para studenthealthcares
    Permission::firstOrCreate(['permission_name' => 'studenthealthcares_index', 'permission_description' => 'Ver Lista de seguros medicos por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'studenthealthcares_show', 'permission_description' => 'Ver detalles de seguros medicos por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'studenthealthcares_store', 'permission_description' => 'Crear nuevos seguros medicos por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'studenthealthcares_search', 'permission_description' => 'Buscar seguros medicos por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'studenthealthcares_update', 'permission_description' => 'Actualizar seguros medicos por estudiante']);
    Permission::firstOrCreate(['permission_name' => 'studenthealthcares_destroy', 'permission_description' => 'Eliminar seguros medicos por estudiante']);

    //Permisos para studentsmonitoring
    Permission::firstOrCreate(['permission_name' => 'studentsmonitoring_index', 'permission_description' => 'Ver Lista de monitoreos de estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentsmonitoring_show', 'permission_description' => 'Ver detalles de monitoreos de estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentsmonitoring_store', 'permission_description' => 'Crear nuevos monitoreos de estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentsmonitoring_search', 'permission_description' => 'Buscar monitoreos de estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentsmonitoring_update', 'permission_description' => 'Actualizar monitoreos de estudiantes']);
    Permission::firstOrCreate(['permission_name' => 'studentsmonitoring_destroy', 'permission_description' => 'Eliminar monitoreos de estudiantes']);

    //Permisos para proffesorstype
    Permission::firstOrCreate(['permission_name' => 'proffesorstype_index', 'permission_description' => 'Ver Lista de tipos de profesores']);
    Permission::firstOrCreate(['permission_name' => 'proffesorstype_show', 'permission_description' => 'Ver detalles de tipos de profesores']);
    Permission::firstOrCreate(['permission_name' => 'proffesorstype_store', 'permission_description' => 'Crear nuevos tipos de profesores']);
    Permission::firstOrCreate(['permission_name' => 'proffesorstype_search', 'permission_description' => 'Buscar tipos de profesores']);
    Permission::firstOrCreate(['permission_name' => 'proffesorstype_update', 'permission_description' => 'Actualizar tipos de profesores']);
    Permission::firstOrCreate(['permission_name' => 'proffesorstype_destroy', 'permission_description' => 'Eliminar tipos de profesores']);
    //Permisos para generacion de mallas
    Permission::firstOrCreate(['permission_name' => 'gen_studyplan', 'permission_description' => 'Generacion de mallas curriculares']);
    //Permisos para informes
    Permission::firstOrCreate(['permission_name' => 'income_report', 'permission_description' => 'Ver informes de ingresos']);
    Permission::firstOrCreate(['permission_name' => 'expense_report', 'permission_description' => 'Ver informes de egresos']);
    Permission::firstOrCreate(['permission_name' => 'ticket_report', 'permission_description' => 'Ver informes facturas']);
    Permission::firstOrCreate(['permission_name' => 'balance_report', 'permission_description' => 'Ver informes balances generales']);
    Permission::firstOrCreate(['permission_name' => 'caja_report', 'permission_description' => 'Ver informes cajas por cajera']);
    Permission::firstOrCreate(['permission_name' => 'caja_agrupado_report', 'permission_description' => 'Ver informes cajas en general']);
    Permission::firstOrCreate(['permission_name' => 'habilitados_report', 'permission_description' => 'Ver informes lista de habilitados']);
    Permission::firstOrCreate(['permission_name' => 'events_report', 'permission_description' => 'Ver informes eventos']);
    //permisos para usuarios
    Permission::firstOrCreate(['permission_name' => 'manage_tills', 'permission_description' => 'Configurar permisos de cajas']);
    Permission::firstOrCreate(['permission_name' => 'reset_password', 'permission_description' => 'Resetear contraseñas']);
  }
}
