<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Generator as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
      $user1 = User::create([
                  'person_id' => 0,
                  'name' => $faker->name,
                  'username'=> 'sistema',
                  'email' => 'sistema@unaetest.com',
                  'email_verified_at' => now(),
                  'password' => Hash::make('adminpass'),
                  'remember_token' => Str::random(10),
              ]);
      $user1->assignRole('Administrador del sistema');
      //
      $user2 = User::create([
                  'person_id' => 0,
                  'name' => $faker->name,
                  'username'=> 'academico',
                  'email' => 'academico@unaetest.com',
                  'email_verified_at' => now(),
                  'password' => Hash::make('adminpass'),
                  'remember_token' => Str::random(10),
              ]);
      $user2->assignRole('Administrador Academico');
      //
      $user3 = User::create([
                  'person_id' => 0,
                  'name' => $faker->name,
                  'username'=> 'administrativo',
                  'email' => 'administrativo@unaetest.com',
                  'email_verified_at' => now(),
                  'password' => Hash::make('adminpass'),
                  'remember_token' => Str::random(10),
              ]);
      $user3->assignRole('Administrador de parte Administrativa');
      //
      $user4 = User::create([
                  'person_id' => 0,
                  'name' => $faker->name,
                  'username'=> 'recepcion',
                  'email' => 'recepcion@unaetest.com',
                  'email_verified_at' => now(),
                  'password' => Hash::make('adminpass'),
                  'remember_token' => Str::random(10),
              ]);
      $user4->assignRole('Recepcion');
      //
      $user5 = User::create([
                  'person_id' => 0,
                  'name' => $faker->name,
                  'username'=> 'secretaria',
                  'email' => 'secretaria@unaetest.com',
                  'email_verified_at' => now(),
                  'password' => Hash::make('adminpass'),
                  'remember_token' => Str::random(10),
              ]);
      $user5->assignRole('Secretaria Academica');
      //
      $user6 = User::create([
                  'person_id' => 0,
                  'name' => $faker->name,
                  'username'=> 'caja',
                  'email' => 'caja@unaetest.com',
                  'email_verified_at' => now(),
                  'password' => Hash::make('adminpass'),
                  'remember_token' => Str::random(10),
              ]);
      $user6->assignRole('Cajas');
      //
      $user7 = User::create([
                  'person_id' => 0,
                  'name' => $faker->name,
                  'username'=> 'archivo',
                  'email' => 'archivo@unaetest.com',
                  'email_verified_at' => now(),
                  'password' => Hash::make('adminpass'),
                  'remember_token' => Str::random(10),
              ]);
      $user7->assignRole('Archivo');
      //

    }
}
