<?php

use Illuminate\Database\Seeder;
use App\Faculties;

class FacultyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Faculties::class, 10)->create();
    }
}
