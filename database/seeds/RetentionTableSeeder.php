<?php

use Illuminate\Database\Seeder;
use App\StudentRetentionQuestion;
use App\StudentRetentionParameters;

class RetentionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 1,'unit_type_id' => 2, 'quest_description' => '¿Con quién vives actualmente?']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 1,'student_ret_quet_id' => 1, 'parameter_description' => 'Otros compañeros de clase', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 2,'student_ret_quet_id' => 1, 'parameter_description' => 'Solo', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 3,'student_ret_quet_id' => 1, 'parameter_description' => 'Familia nuclear', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 4,'student_ret_quet_id' => 1, 'parameter_description' => 'Familia extensa', 'parameter_value' => 0]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 2,'unit_type_id' => 2, 'quest_description' => '¿Cuál es tu situación ocupacional en este momento?']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 5,'student_ret_quet_id' => 2, 'parameter_description' => 'Trab 1/2 dia', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 6,'student_ret_quet_id' => 2, 'parameter_description' => 'Trab Todo el dia', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 7,'student_ret_quet_id' => 2, 'parameter_description' => 'Solo Estudio', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 8,'student_ret_quet_id' => 2, 'parameter_description' => 'Trab. con cambio de turno', 'parameter_value' => 0]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 3,'unit_type_id' => 2, 'quest_description' => '¿Cuál es tu plan de sustento económico mientras estudias?']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 9,'student_ret_quet_id' => 3, 'parameter_description' => 'Familia', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 10,'student_ret_quet_id' => 3, 'parameter_description' => 'Becas', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 11,'student_ret_quet_id' => 3, 'parameter_description' => 'Beca parcial', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 12,'student_ret_quet_id' => 3, 'parameter_description' => 'Trabajo', 'parameter_value' => 0]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 4,'unit_type_id' => 2, 'quest_description' => 'Formación acádemica del Padre:']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 13,'student_ret_quet_id' => 4, 'parameter_description' => 'Básica', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 14,'student_ret_quet_id' => 4, 'parameter_description' => 'Media', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 15,'student_ret_quet_id' => 4, 'parameter_description' => 'Superior', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 16,'student_ret_quet_id' => 4, 'parameter_description' => 'Universitaria', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 17,'student_ret_quet_id' => 4, 'parameter_description' => 'Ninguna', 'parameter_value' => 0]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 5,'unit_type_id' => 2, 'quest_description' => 'Formación acádemica de la Madre:']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 18,'student_ret_quet_id' => 5, 'parameter_description' => 'Básica', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 19,'student_ret_quet_id' => 5, 'parameter_description' => 'Media', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 20,'student_ret_quet_id' => 5, 'parameter_description' => 'Superior', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 21,'student_ret_quet_id' => 5, 'parameter_description' => 'Universitaria', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 22,'student_ret_quet_id' => 5, 'parameter_description' => 'Ninguna', 'parameter_value' => 0]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 6,'unit_type_id' => 2, 'quest_description' => 'Formación acádemica de Hermanos:']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 23,'student_ret_quet_id' => 6, 'parameter_description' => 'Básica', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 24,'student_ret_quet_id' => 6, 'parameter_description' => 'Media', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 25,'student_ret_quet_id' => 6, 'parameter_description' => 'Superior', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 26,'student_ret_quet_id' => 6, 'parameter_description' => 'Universitaria', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 27,'student_ret_quet_id' => 6, 'parameter_description' => 'Ninguna', 'parameter_value' => 0]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 7,'unit_type_id' => 2, 'quest_description' => 'Estado Civil:']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 28,'student_ret_quet_id' => 7, 'parameter_description' => 'S', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 29,'student_ret_quet_id' => 7, 'parameter_description' => 'C', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 30,'student_ret_quet_id' => 7, 'parameter_description' => 'D', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 31,'student_ret_quet_id' => 7, 'parameter_description' => 'V', 'parameter_value' => 0]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 8,'unit_type_id' => 2, 'quest_description' => 'Número de hijos:']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 32,'student_ret_quet_id' => 8, 'parameter_description' => 'Ninguno', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 33,'student_ret_quet_id' => 8, 'parameter_description' => '1', 'parameter_value' => 1]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 34,'student_ret_quet_id' => 8, 'parameter_description' => '2', 'parameter_value' => 2]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 35,'student_ret_quet_id' => 8, 'parameter_description' => '3', 'parameter_value' => 3]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 36,'student_ret_quet_id' => 8, 'parameter_description' => '3+', 'parameter_value' => 5]);

      StudentRetentionQuestion::firstOrCreate(['student_ret_quet_id' => 9,'unit_type_id' => 2, 'quest_description' => 'Número de hermanos:']);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 37,'student_ret_quet_id' => 9, 'parameter_description' => '1', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 38,'student_ret_quet_id' => 9, 'parameter_description' => '2', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 39,'student_ret_quet_id' => 9, 'parameter_description' => '3', 'parameter_value' => 0]);
        StudentRetentionParameters::firstOrCreate(['student_ret_par_id' => 40,'student_ret_quet_id' => 9, 'parameter_description' => '3+', 'parameter_value' => 0]);

    }
}
