<?php

use Illuminate\Database\Seeder;
use App\ContactType;

class ContactTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ContactType::class, 10)->create();
    }
}
