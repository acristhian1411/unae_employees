<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Permission;
use App\Role;

class RolesAssignDocente extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role7 = Role::where('role_name', 'Docente')
                    ->first();
        //


    $permisos_docente = [ 'units_show', 'units_search',
                           'faculties_index', 'faculties_show', 'faculties_search',
                           'careers_index', 'careers_show', 'careers_search',
                           'facuCareers_index', 'facuCareers_show', 'facuCareers_search',
                           'subjects_index', 'subjects_show', 'subjects_search',
                           'carsubjs_index', 'carsubjs_show', 'carsubjs_search',
                           'students_index', 'students_show', 'students_search',
                           'persons_index', 'persons_show', 'persons_search',
                           'subevals_index', 'subevals_show', 'subevals_search',
                           'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search',
                           'enrolleds_index', 'enrolleds_show', 'enrolleds_search'
                         ];
   for ($i=0; $i < count($permisos_docente); $i++) {
     $role7->givePermissionTo($permisos_docente[$i]);
   }
    }
}
