<?php

use Illuminate\Database\Seeder;
use App\TicketDetailType;

class TicketDetailTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Matricula']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Pruebas de Nivelacion (1 curso)']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Pruebas Parciales']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Recuperatorio']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Examenes Finales 1 oportunidad']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Examenes Finales 2 oportunidad']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Examenes Finales 3 oportunidad']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Examenes Finales Extraordinario']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Gestiones de titulos']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Certificacion de Estudio Adicional']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => '1 Mesa de defensa']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => '2 Mesa de defensa']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => '3 Mesa de defensa']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Mesa Extraordinario']);
        TicketDetailType::firstOrCreate(['tdtype_desc' => 'Certificado de estudio']);
    }
}
