<?php

use Illuminate\Database\Seeder;
use App\Healthcare;

class HealthcareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Healthcare::class, 10)->create();
    }
}
