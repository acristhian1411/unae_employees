<?php

use Illuminate\Database\Seeder;
use App\Till;

class TillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Till::class, 10)->create();
    }
}
