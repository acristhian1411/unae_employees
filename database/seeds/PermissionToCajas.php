<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Permission;
use App\Role;

class PermissionToCajas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = Role::where('role_name', 'Cajas')
                    ->first();

    $permisos_caja = [
     'paymentTypes_index','paymentTypes_show','paymentTypes_store',
     'paymentTypes_search','paymentTypes_update',
     'promos_index', 'promos_show', 'promos_store', 'promos_search', 'promos_update',
     'tdtypes_index','tdtypes_show','tdtypes_store','tdtypes_search','tdtypes_update',
     'providers_index','providers_show','providers_store',
     'providers_search','providers_update',
     'professor_account_index', 'professor_account_show',
     'professor_account_store', 'professor_account_search', 'professor_account_update',
     'employee_account_index','employee_account_show','employee_account_store',
     'employee_account_search','employee_account_update',
     'provider_account_index','provider_account_show','provider_account_store',
     'provider_account_search','provider_account_update',
     'evalstudent_index','evalstudent_show',
     'staccount_index', 'staccount_show', 'staccount_store', 'staccount_update',
     'ticket_index', 'ticket_show', 'ticket_store', 'ticket_update',
     'ticket_details_index', 'ticket_details_show', 'ticket_details_store',
     'td_types_index', 'td_types_show', 'td_types_store', 'td_types_update',
     'unit_types_index', 'unit_types_show', 'unit_types_search',
     'units_index', 'units_show',  'units_search',
     'careertypes_index', 'careertypes_show', 'careertypes_search',
     'faculties_index', 'faculties_show', 'faculties_search',
     'subjects_index', 'subjects_show', 'subjects_search',
     'prereqs_index', 'prereqs_show', 'prereqs_search',
     'careers_index', 'careers_show', 'careers_search',
     'scholarship_index', 'scholarship_show', 'scholarship_store',
     'scholarship_search', 'scholarship_update',
     'titles_index', 'titles_show', 'titles_search',
     'hstitle_index', 'hstitle_show', 'hstitle_search',
     'semesters_index', 'semesters_show', 'semesters_search',
     'retentionParameter_index', 'retentionParameter_show', 'retentionParameter_search',
     'retentionQuestion_index', 'retentionQuestion_show', 'retentionQuestion_search',
     'retentionAnswer_index', 'retentionAnswer_show', 'retentionAnswer_search',
     'studentScholarship_index','studentScholarship_show','studentScholarship_store',
     'studentScholarship_search','studentScholarship_update',
     'countries_index', 'countries_show', 'countries_search',
     'departments_index', 'departments_show', 'departments_search',
     'requiredDocs_index', 'requiredDocs_show', 'requiredDocs_search',
     'healtCares_index', 'healtCares_show', 'healtCares_search',
     'operations_index', 'operations_show', 'operations_store',
     'operations_search', 'operations_update',
     'workplaces_index', 'workplaces_show', 'workplaces_search',
     'StudentWorkplaces_index', 'StudentWorkplaces_show', 'StudentWorkplaces_store',
     'StudentWorkplaces_search', 'StudentWorkplaces_update',
     'documentPerson_index', 'documentPerson_show', 'documentPerson_search',
     'persons_index','persons_show','persons_store',
     'persons_search','persons_update',
     'students_index', 'students_show', 'students_store',
     'students_search', 'students_update',
     'employees_index', 'employees_show', 'employees_search',
     'cities_index', 'cities_show', 'cities_search',
     'contactTypes_index', 'contactTypes_show', 'contactTypes_store',
     'contactTypes_search','contactTypes_update',
     'facuCareers_index', 'facuCareers_show', 'facuCareers_search',
     'profile_types_index', 'profile_types_show', 'profile_types_search',
     'ivaType_index', 'ivaType_show', 'ivaType_store', 'ivaType_search', 'ivaType_update',
     'appointments_index', 'appointments_show', 'appointments_search',
     'products_index', 'products_show', 'products_store', 'products_search', 'products_update',
     'schools_index', 'schools_show', 'schools_search',
     'tills_index', 'tills_show', 'tills_store', 'tills_search', 'tills_update',
     'tills_details_index', 'tills_details_show', 'tills_details_store',
     'tills_details_search', 'tills_details_update',
     'till_transfers_index', 'till_transfers_show', 'till_transfers_store',
     'till_transfers_search', 'till_transfers_update',
     'contactPersons_index', 'contactPersons_show', 'contactPersons_store',
     'contactPersons_search', 'contactPersons_update',
     'facucars_index', 'facucars_show', 'facucars_search',
     'subevals_index', 'subevals_show', 'subevals_search', 'subevals_update',
     'assistances_index', 'assistances_show', 'assistances_search',
     'studenthealthcares_index', 'studenthealthcares_show', 'studenthealthcares_store',
     'studenthealthcares_search', 'studenthealthcares_update',
     'studentsmonitoring_index', 'studentsmonitoring_show', 'studentsmonitoring_search',
     'payplans_index', 'payplans_show', 'payplans_search', 'payplans_store', 'payplans_update',
     'tariffs_index', 'tariffs_show', 'tariffs_search', 'tariffs_store', 'tariffs_update',
     'facuCarAdm_index', 'facuCarAdm_show', 'facuCarAdm_search', 'facuCarAdm_store',
     'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search',
     'carsubjs_index', 'carsubjs_show', 'carsubjs_search',
     'students_index', 'students_show', 'students_search',
     'persons_index', 'persons_show', 'persons_search',
     'professors_index', 'professors_show', 'professors_search',
     'proffesorstype_index', 'proffesorstype_show', 'proffesorstype_search',
     'enrolleds_index', 'enrolleds_show', 'enrolleds_search', 'enrolleds_store', 'enrolleds_update',
     'ticket_report', 'caja_report'
                   ];
     $this->command->getOutput()->progressStart(count($permisos_caja));
     for ($i=0; $i < count($permisos_caja); $i++) {
       $role->givePermissionTo($permisos_caja[$i]);
       $this->command->getOutput()->progressAdvance();
     }
     $this->command->getOutput()->progressFinish();
    }
}
