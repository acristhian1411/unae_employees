<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(Country::class, 10)->create();
        Country::create([
            'country_id' => 5,
            'country_name' => 'Argentina',
            'country_code' => 'AR',
        ]);
    }
}
