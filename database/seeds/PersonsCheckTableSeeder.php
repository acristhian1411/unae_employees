<?php

use Illuminate\Database\Seeder;
use App\PersonsCheck;

class PersonsCheckTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PersonsCheck::class, 3)->create();
    }
}
