<?php

use Illuminate\Database\Seeder;
use App\ProfileType;

class ProfileTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ProfileType::class, 10)->create();
    }
}
