<?php

use Illuminate\Database\Seeder;
use App\Tariff;

class TariffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Tariff::class, 10)->create();
    }
}
