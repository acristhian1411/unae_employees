<?php

use Illuminate\Database\Seeder;
use App\Title;

class TitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Title::class, 3)->create();
    }
}
