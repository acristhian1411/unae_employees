<?php

use Illuminate\Database\Seeder;
use App\CareerType;
class CareerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CareerType::class, 5)->create();
    }
}
