<?php

use Illuminate\Database\Seeder;
use App\StudentHealthcare;

class StudentHealthcareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(StudentHealthcare::class, 3)->create();

    }
}
