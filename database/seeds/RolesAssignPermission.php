<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Permission;
use App\Role;

class RolesAssignPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::where('role_name', 'Administrador Academico')
                    ->first();
        $role2 = Role::where('role_name', 'Administrador de parte Administrativa')
                    ->first();
        $role3 = Role::where('role_name', 'Recepcion')
                    ->first();
        $role4 = Role::where('role_name', 'Secretaria Academica')
                    ->first();
        $role5 = Role::where('role_name', 'Cajas')
                    ->first();
        $role6 = Role::where('role_name', 'Archivo')
                    ->first();
        $role7 = Role::where('role_name', 'Docente')
                    ->first();
        $role8 = Role::where('role_name', 'Estudiante')
                    ->first();
        //
        $permisos_academica = ['units_index', 'units_show', 'units_search', 'units_store', 'units_update', 'units_destroy',
                               'faculties_index', 'faculties_show', 'faculties_search', 'faculties_store', 'faculties_update', 'faculties_destroy',
                               'careers_index', 'careers_show', 'careers_search', 'careers_store', 'careers_update', 'careers_destroy',
                               'careertypes_index', 'careertypes_show', 'careertypes_store', 'careertypes_search', 'careertypes_update', 'careertypes_destroy',
                               'facuCareers_index', 'facuCareers_show', 'facuCareers_search', 'facuCareers_store', 'facuCareers_update', 'facuCareers_destroy',
                               'subjects_index', 'subjects_show', 'subjects_search', 'subjects_store', 'subjects_update', 'subjects_destroy',
                               'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search', 'evaluationTypes_store', 'evaluationTypes_update', 'evaluationTypes_destroy',
                               'carsubjs_index', 'carsubjs_show', 'carsubjs_search', 'carsubjs_store', 'carsubjs_update', 'carsubjs_destroy',
                               'employees_index', 'employees_show', 'employees_search', 'employees_store', 'employees_update', 'employees_destroy',
                               'students_index', 'students_show', 'students_search', 'students_store', 'students_update', 'students_destroy',
                               'persons_index', 'persons_show', 'persons_search', 'persons_store', 'persons_update', 'persons_destroy',
                               'professors_index', 'professors_show', 'professors_search', 'professors_store', 'professors_update', 'professors_destroy',
                               'proffesorstype_index', 'proffesorstype_show', 'proffesorstype_search', 'proffesorstype_store', 'proffesorstype_update', 'proffesorstype_destroy',
                               'profile_types_index', 'profile_types_show', 'profile_types_search', 'profile_types_store', 'profile_types_update', 'profile_types_destroy',
                               'enrolleds_index', 'enrolleds_show', 'enrolleds_search', 'users_index', 'users_show', 'users_search',
                               'semesters_index', 'semesters_show', 'semesters_store', 'semesters_search', 'semesters_update', 'semesters_destroy',
                               'contactTypes_index', 'contactTypes_show', 'contactTypes_store', 'contactTypes_search',
                               'contactPersons_index', 'contactPersons_show', 'contactPersons_store', 'contactPersons_search',
                               'workplaces_index', 'workplaces_show', 'workplaces_search',
                               'studenthealthcares_index', 'studenthealthcares_show', 'studenthealthcares_store', 'studenthealthcares_search',
                               'healtCares_index', 'healtCares_show', 'healtCares_search',
                               'countries_index', 'countries_show', 'countries_search',
                               'schools_index', 'schools_show', 'schools_search',
                               'hstitle_index', 'hstitle_show', 'hstitle_search',
                             ];
       for ($i=0; $i < count($permisos_academica); $i++) {
         $role1->givePermissionTo($permisos_academica[$i]);
       }
       //
       $permisos_administracion = ['payplans_index', 'payplans_show', 'payplans_search', 'payplans_store', 'payplans_update', 'payplans_destroy',
                              'scholarship_index', 'scholarship_show', 'scholarship_search', 'scholarship_store', 'scholarship_update', 'scholarship_destroy',
                              'ivaType_index', 'ivaType_show', 'ivaType_search', 'ivaType_store', 'ivaType_update', 'ivaType_destroy',
                              'tariffs_index', 'tariffs_show', 'tariffs_search', 'tariffs_store', 'tariffs_update', 'tariffs_destroy',
                              'tills_index', 'tills_show', 'tills_search', 'tills_store', 'tills_update', 'tills_destroy',
                              'operations_index', 'operations_show', 'operations_search', 'operations_store', 'operations_update', 'evaluationTypes_destroy',
                              'facuCarAdm_index', 'facuCarAdm_show', 'facuCarAdm_search', 'facuCarAdm_store', 'facuCarAdm_update', 'facuCarAdm_destroy',
                              'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search',
                              'carsubjs_index', 'carsubjs_show', 'carsubjs_search',
                              'students_index', 'students_show', 'students_search', 'students_store', 'students_update', 'students_destroy',
                              'persons_index', 'persons_show', 'persons_search', 'persons_store', 'persons_update', 'persons_destroy',
                              'professors_index', 'professors_show', 'professors_search', 'professors_store', 'professors_update', 'professors_destroy',
                              'proffesorstype_index', 'proffesorstype_show', 'proffesorstype_search', 'proffesorstype_store', 'proffesorstype_update', 'proffesorstype_destroy',
                              'profile_types_index', 'profile_types_show', 'profile_types_search',
                              'enrolleds_index', 'enrolleds_show', 'enrolleds_search', 'enrolleds_store', 'enrolleds_update', 'enrolleds_destroy',
                              'units_index', 'units_show', 'units_search',
                              'contactTypes_index', 'contactTypes_show', 'contactTypes_store', 'contactTypes_search',
                              'contactPersons_index', 'contactPersons_show', 'contactPersons_store', 'contactPersons_search',
                              'workplaces_index', 'workplaces_show', 'workplaces_search',
                              'healtCares_index', 'healtCares_show', 'healtCares_search',
                              'countries_index', 'countries_show', 'countries_search',
                              'schools_index', 'schools_show', 'schools_search',
                              'hstitle_index', 'hstitle_show', 'hstitle_search',
                            ];
      for ($i=0; $i < count($permisos_administracion); $i++) {
        $role2->givePermissionTo($permisos_administracion[$i]);
      }
      //
      $permisos_recepcion = ['units_index', 'units_show', 'units_search',
                             'faculties_index', 'faculties_show', 'faculties_search',
                             'careers_index', 'careers_show', 'careers_search',
                             'facuCareers_index', 'facuCareers_show', 'facuCareers_search',
                             'students_index', 'students_show', 'students_search', 'students_store', 'students_update',
                             'persons_index', 'persons_show', 'persons_search', 'persons_store', 'persons_update',
                             'enrolleds_index', 'enrolleds_show', 'enrolleds_search',
                           ];
     for ($i=0; $i < count($permisos_recepcion); $i++) {
       $role3->givePermissionTo($permisos_recepcion[$i]);
     }
     //
     $permisos_secretaria = ['units_index', 'units_show', 'units_search',
                            'faculties_index', 'faculties_show', 'faculties_search',
                            'careers_index', 'careers_show', 'careers_search',
                            'facuCareers_index', 'facuCareers_show', 'facuCareers_search',
                            'subjects_index', 'subjects_show', 'subjects_search',
                            'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search', 'evaluationTypes_update',
                            'carsubjs_index', 'carsubjs_show', 'carsubjs_search',
                            'students_index', 'students_show', 'students_search', 'students_update',
                            'persons_index', 'persons_show', 'persons_search', 'persons_update',
                            'professors_index', 'professors_show', 'professors_search', 'professors_update',
                            'proffesorstype_index', 'proffesorstype_show', 'proffesorstype_search', 'proffesorstype_update',
                            'enrolleds_index', 'enrolleds_show', 'enrolleds_search',
                          ];
    for ($i=0; $i < count($permisos_secretaria); $i++) {
      $role4->givePermissionTo($permisos_secretaria[$i]);
    }
    //
    $permisos_caja = ['payplans_index', 'payplans_show', 'payplans_search',
                     'scholarship_index', 'scholarship_show', 'scholarship_search',
                     'ivaType_index', 'ivaType_show', 'ivaType_search',
                     'tariffs_index', 'tariffs_show', 'tariffs_search',
                     'tills_index', 'tills_show', 'tills_search',
                     'tills_details_index', 'tills_details_show',  'tills_details_store',
                     'till_transfers_index', 'till_transfers_show', 'till_transfers_store', 
                     'operations_index', 'operations_show', 'operations_search', 'operations_update',
                     'facuCarAdm_index', 'facuCarAdm_show', 'facuCarAdm_search',
                     'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search',
                     'carsubjs_index', 'carsubjs_show', 'carsubjs_search',
                     'students_index', 'students_show', 'students_search',
                     'persons_index', 'persons_show', 'persons_search',
                     'professors_index', 'professors_show', 'professors_search',
                     'proffesorstype_index', 'proffesorstype_show', 'proffesorstype_search',
                     'enrolleds_index', 'enrolleds_show', 'enrolleds_search',
                   ];
     for ($i=0; $i < count($permisos_caja); $i++) {
       $role5->givePermissionTo($permisos_caja[$i]);
     }
     //
     $permisos_archivo = ['units_index', 'units_show', 'units_search',
                            'faculties_index', 'faculties_show', 'faculties_search',
                            'careers_index', 'careers_show', 'careers_search',
                            'facuCareers_index', 'facuCareers_show', 'facuCareers_search',
                            'subjects_index', 'subjects_show', 'subjects_search',
                            'carsubjs_index', 'carsubjs_show', 'carsubjs_search',
                            'students_index', 'students_show', 'students_search',
                            'persons_index', 'persons_show', 'persons_search',
                            'requiredDocs_index', 'requiredDocs_show', 'requiredDocs_search',
                            'documentPerson_index', 'documentPerson_show', 'documentPerson_search', 'documentPerson_store', 'documentPerson_update',
                          ];
    for ($i=0; $i < count($permisos_archivo); $i++) {
      $role6->givePermissionTo($permisos_archivo[$i]);
    }

    $permisos_docente = [ 'units_show', 'units_search',
                           'faculties_index', 'faculties_show', 'faculties_search',
                           'careers_index', 'careers_show', 'careers_search',
                           'facuCareers_index', 'facuCareers_show', 'facuCareers_search',
                           'subjects_index', 'subjects_show', 'subjects_search',
                           'carsubjs_index', 'carsubjs_show', 'carsubjs_search',
                           'students_index', 'students_show', 'students_search',
                           'persons_index', 'persons_show', 'persons_search',
                           'subevals_index', 'subevals_show', 'subevals_search',
                           'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search',
                           'enrolleds_index', 'enrolleds_show', 'enrolleds_search'
                         ];
   for ($i=0; $i < count($permisos_docente); $i++) {
     $role7->givePermissionTo($permisos_docente[$i]);
   }
    }
}
