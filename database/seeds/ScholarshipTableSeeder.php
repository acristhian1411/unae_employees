<?php

use Illuminate\Database\Seeder;
use App\Scholarship;

class ScholarshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Scholarship::class, 10)->create();
    }
}
