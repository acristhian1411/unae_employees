<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Permission;
use App\Role;

class RoleHasPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role = Role::where('role_name', 'Administrador del sistema')
                  ->first();
      $permisos = Permission::all();
      // $role->givePermissionTo("users_index");
      $this->command->getOutput()->progressStart(count($permisos));
      for ($i=0; $i < count($permisos); $i++) {
        $role->givePermissionTo($permisos[$i]->permission_name);
          $this->command->getOutput()->progressAdvance();
        // dd($permisos[$i]->permission_name);
      }
      $this->command->getOutput()->progressFinish();


    }
}
