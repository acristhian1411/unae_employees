<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(City::class, 10)->create();
      City::create([
        'city_id' =>3,
        'depart_id' => 1,
        'city_name' => 'Hohenau',
      ]);
    }
}
