<?php

use Illuminate\Database\Seeder;
use App\Operation;

class OperationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Primera Oportunidad - Grado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Segunda Oportunidad - Grado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Tercera Oportunidad - Grado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Recuperatorio - Grado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Extraordinario - Grado', 'oper_amount' => 0]);

        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Primera Oportunidad - Postgrado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Segunda Oportunidad - Postgrado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Tercera Oportunidad - Postgrado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Recuperatorio - Postgrado', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Extraordinario - Postgrado', 'oper_amount' => 0]);

        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Primera Oportunidad - Colegio', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Segunda Oportunidad - Colegio', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Tercera Oportunidad - Colegio', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Recuperatorio - Colegio', 'oper_amount' => 0]);
        Operation::firstOrCreate(['ivatype_id' => 3, 'oper_reference' => true, 'oper_description' => 'Extraordinario - Colegio', 'oper_amount' => 0]);
    }
}
