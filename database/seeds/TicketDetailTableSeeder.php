<?php

use Illuminate\Database\Seeder;
use App\TicketDetail;

class TicketDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(TicketDetail::class, 3)->create();
    }
}
