<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(CountryTableSeeder::class);
      $this->call(DepartamentTableSeeder::class);
      $this->call(CitiesTableSeeder::class);
      $this->call(PersonTableSeeder::class);
      $this->call(UserTableSeeder::class);
      $this->call(RolesAssignPermission::class);
      // $this->call(RoleTableSeeder::class);
      //$this->call(UnitTableSeeder::class);
      //$this->call(FacultyTableSeeder::class);
      //$this->call(CountryTableSeeder::class);
      //$this->call(ContactTypeTableSeeder::class);
      //$this->call(CareerTypeTableSeeder::class);
      //$this->call(CareerTableSeeder::class);
      //$this->call(DepartamentTableSeeder::class);
      //$this->call(CitiesTableSeeder::class);
      //$this->call(SemesterTableSeeder::class);
      //$this->call(FacuCareerTableSeeder::class);
    //$this->call(EvaluationTypesTableSeeder::class);
    //$this->call(PersonTableSeeder::class);
    //$this->call(IvaTypeTableSeeder::class);
    //$this->call(HschollTitleTableSeeder::class);
    //$this->call(HealthcareTableSeeder::class);
    //$this->call(OperationTableSeeder::class);
    //$this->call(ProfileTypeTableSeeder::class);
    //$this->call(RequiredDocTableSeeder::class);
    //$this->call(ScholarshipTableSeeder::class);
    //$this->call(SchoolTableSeeder::class);
    //$this->call(StudentHealthcareTableSeeder::class);
    //$this->call(StudentRetentionTableSeeder::class);
    //$this->call(SubjectTableSeeder::class);
    //$this->call(TicketTableSeeder::class);
    //$this->call(TicketDetailTypeTableSeeder::class);
    //$this->call(TicketDetailTableSeeder::class);
    //$this->call(TillTableSeeder::class);
    //$this->call(TitleTableSeeder::class);
    //$this->call(WorkplaceTableSeeder::class);
    //$this->call(StudentTableSeeder::class);
    //$this->call(ProfessorTableSeeder::class);
    //$this->call(AppointmentTableSeeder::class);
    //$this->call(FacucarAdmTableSeeder::class);
    //$this->call(PersonsCheckTableSeeder::class);
    //$this->call(TariffTableSeeder::class);
    }
}
