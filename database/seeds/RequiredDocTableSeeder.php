<?php

use Illuminate\Database\Seeder;
use App\RequiredDoc;

class RequiredDocTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(RequiredDoc::class, 10)->create();

    }
}
