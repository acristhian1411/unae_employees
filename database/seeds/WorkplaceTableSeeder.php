<?php

use Illuminate\Database\Seeder;
use App\Workplace;

class WorkplaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Workplace::class, 10)->create();
    }
}
