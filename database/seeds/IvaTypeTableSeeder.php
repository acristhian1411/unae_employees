<?php

use Illuminate\Database\Seeder;
use App\IvaType;

class IvaTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(IvaType::class, 5)->create();
    }
}
