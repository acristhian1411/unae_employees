<?php

use Illuminate\Database\Seeder;
use App\FacucarAdm;

class FacucarAdmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FacucarAdm::class, 10)->create();
    }
}
