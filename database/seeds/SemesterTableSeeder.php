<?php

use Illuminate\Database\Seeder;
use App\Semester;
class SemesterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Semester::class, 10)->create();
    }
}
