<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Permission;
use App\Role;

class PermissionToSecretary2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role = Role::where('role_name', 'Secretaria Academica.')
                    ->first();

                    $permisos = [
                      'units_show', 'units_search',
                      'paymentTypes_index', 'paymentTypes_show', 'paymentTypes_search',
                      'tdtypes_index', 'tdtypes_show',
                      'providers_index', 'providers_show',
                      'ticket_index', 'ticket_show', 'ticket_store',
                      'ticket_details_index', 'ticket_details_show', 'ticket_details_store',
                      'faculties_index', 'faculties_show', 'faculties_search',
                      'unit_types_index', 'unit_types_show', 'unit_types_search',
                      'careers_index', 'careers_show', 'careers_search', 'careers_store',
                      'facuCareers_index', 'facuCareers_show', 'facuCareers_search', 'facuCareers_store',
                      'careertypes_index', 'careertypes_show', 'careertypes_search',
                      'subjects_index', 'subjects_show', 'subjects_search', 'subjects_store',
                      'carsubjs_index', 'carsubjs_show', 'carsubjs_search', 'carsubjs_store',
                      'students_index', 'students_show', 'students_search', 'students_store',
                      'persons_index', 'persons_show', 'persons_search', 'persons_store',
                      'professors_index', 'professors_show', 'professors_search', 'professors_store',
                      'profile_types_index', 'profile_types_show', 'profile_types_search',
                      'subevals_index', 'subevals_show', 'subevals_search',
                      'evaluationTypes_index', 'evaluationTypes_show', 'evaluationTypes_search',
                      'enrolleds_index', 'enrolleds_show', 'enrolleds_search',
                      'users_index', 'users_show', 'users_search', 'users_store',
                      'semesters_index', 'semesters_show', 'semesters_store', 'semesters_search',  'semesters_destroy',
                      'contactTypes_index', 'contactTypes_show', 'contactTypes_store', 'contactTypes_search',
                      'contactPersons_index', 'contactPersons_show', 'contactPersons_store', 'contactPersons_search',
                      'workplaces_index', 'workplaces_show', 'workplaces_search',
                      'studenthealthcares_index', 'studenthealthcares_show', 'studenthealthcares_store', 'studenthealthcares_search',
                      'healtCares_index', 'healtCares_show', 'healtCares_search',
                      'countries_index', 'countries_show', 'countries_search',
                      'schools_index', 'schools_show', 'schools_search', 'schools_store',
                      'hstitle_index', 'hstitle_show', 'hstitle_search',
                      'titles_index', 'titles_show', 'titles_store',
                      'studentsmonitoring_index', 'studentsmonitoring_show', 'studentsmonitoring_store', 'studentsmonitoring_search', 'studentsmonitoring_update',
                      'assistances_index', 'assistances_show', 'assistances_store', 'assistances_search',
                      'evalstudent_index', 'evalstudent_show',
                      'staccount_index', 'staccount_show',
                      'career_reqdoc_index', 'career_reqdoc_show',
                      'prereqs_index', 'prereqs_show', 'prereqs_search',
                      'scholarship_index', 'scholarship_show', 'scholarship_search',
                      'studentScholarship_index', 'studentScholarship_show', 'studentScholarship_search',
                      'countries_index', 'countries_show', 'countries_search',
                      'departments_index', 'departments_show', 'departments_search',
                      'cities_index', 'cities_show', 'cities_search',
                      'requiredDocs_index', 'requiredDocs_show', 'requiredDocs_search',
                      'operations_index', 'operations_show', 'operations_search',
                      'workplaces_index', 'workplaces_show', 'workplaces_search',
                      'StudentWorkplaces_index', 'StudentWorkplaces_show', 'StudentWorkplaces_search',
                      'documentPerson_index', 'documentPerson_show', 'documentPerson_store', 'documentPerson_search',
                      'payplans_index', 'payplans_show', 'payplans_search',
                      'tariffs_index', 'tariffs_show', 'tariffs_search',
                      'enrolleds_index', 'enrolleds_show', 'enrolleds_store', 'enrolleds_search',
                      'studenthealthcares_index', 'studenthealthcares_show', 'studenthealthcares_search',
                      'proffesorstype_index', 'proffesorstype_show', 'proffesorstype_search',
                      'facuCarAdm_index', 'facuCarAdm_show',  'facuCarAdm_store', 'habilitados_report', 'events_report',
                      'ticket_report',
                      'products_index', 'products_show', 'products_store', 'products_search', 'products_update',
                      'careers_update',
                      'facuCareers_update',
                      'subjects_update',
                      'carsubjs_update',
                      'students_update',
                      'persons_update',
                      'professors_update',
                      'semesters_update',
                      'contactPersons_update',
                      'titles_update',
                      'studentsmonitoring_update',
                      'assistances_update',
                      'documentPerson_update',
                      'enrolleds_update',
                    ];
     $this->command->getOutput()->progressStart(count($permisos));
     for ($i=0; $i < count($permisos); $i++) {
       $role->givePermissionTo($permisos[$i]);
       $this->command->getOutput()->progressAdvance();
     }
     $this->command->getOutput()->progressFinish();
    }
}
