<?php

use Illuminate\Database\Seeder;
use App\HschollTitle;

class HschollTitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(HschollTitle::class, 10)->create();
    }
}
