<?php

use Illuminate\Database\Seeder;
use App\Department;
class DepartamentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          // factory(Department::class, 10)->create();
          Department::create([
            'depart_id' => 4,
            'country_id' => 1,
            'depart_name' => 'Pilar',
          ]);
    }
}
