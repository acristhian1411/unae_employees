<?php

use Illuminate\Database\Seeder;
use App\Career;

class CareerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Career::class, 10)->create();
    }
}
