<?php

use Illuminate\Database\Seeder;
use App\Person;
use Faker\Generator as Faker;

class PersonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // factory(Person::class, 10)->create();
        Person::create([
            'person_id' => 2,
            'homecity_id' => 2,
            'birthplace_id' => 2,
            'country_id' => 2,
            'person_fname' => $faker-> firstName,
            'person_lastname' => $faker-> lastName,
            'person_birthdate' => $faker-> date,
            'person_gender' => $faker-> title,
            'person_idnumber' => $faker-> latitude,
            'person_address' => $faker-> address,
            'person_bloodtype' => $faker-> word,
            'person_photo' => $faker-> word,
            'person_business_name' => $faker-> word,
            'person_ruc' => $faker-> postcode,

        ]);
    }
}
