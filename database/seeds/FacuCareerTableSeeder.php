<?php

use Illuminate\Database\Seeder;
use App\FacuCareer;

class FacuCareerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FacuCareer::class, 10)->create();
    }
}
