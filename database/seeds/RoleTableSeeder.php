<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Role::firstOrCreate(['role_name'=>'Administrador del sistema']);
      Role::firstOrCreate(['role_name'=>'Administrador Academico']);
      Role::firstOrCreate(['role_name'=>'Administrador de parte Administrativa']);
      Role::firstOrCreate(['role_name'=>'Recepcion']);
      Role::firstOrCreate(['role_name'=>'Secretaria Academica']);
      Role::firstOrCreate(['role_name'=>'Cajas']);
      Role::firstOrCreate(['role_name'=>'Archivo']);
      Role::firstOrCreate(['role_name'=>'Docente']);
      Role::firstOrCreate(['role_name'=>'Estudiante']);
    }
}
