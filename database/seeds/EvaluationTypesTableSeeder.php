<?php

use Illuminate\Database\Seeder;
use App\EvaluationTypes;

class EvaluationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(EvaluationTypes::class, 10)->create();
    }
}
