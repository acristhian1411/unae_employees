<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Department;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
//'user_id' => $faker->unique()->numberBetween(1, App\User::count()),

$factory->define(Department::class, function (Faker $faker) {
    return [
      'country_id' => $faker->unique()->numberBetween(1, App\Country::count()),
      'depart_name' => $faker->country,
    ];
});
