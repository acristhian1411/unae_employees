<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PersonsCheck;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(PersonsCheck::class, function (Faker $faker) {
    return [
        'person_id' => $faker-> numberBetween(6, 10),
        'perchek_id' => $faker-> unique()->numberBetween(5, 10),
        'percchek_time' => $faker-> time,
        'percchek_code_marc' => $faker-> word,
    ];
});
