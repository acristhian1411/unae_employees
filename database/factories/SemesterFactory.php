<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Semester;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
//'user_id' => $faker->unique()->numberBetween(1, App\User::count()),

$factory->define(Semester::class, function (Faker $faker) {
    return [
      'sems_id' => $faker-> unique()->numberBetween(1, 10),
      'sems_name' => $faker-> word,
      //'career_id' => $faker-> unique()->numberBetween(1, App\Career::count()),
      'career_id' => $faker-> unique()->numberBetween(12, 21),
      'sems_year' => $faker-> year($max = 'now'),
    ];
});
