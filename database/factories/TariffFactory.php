<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tariff;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Tariff::class, function (Faker $faker) {
    return [
        'fcaradm_id' => $faker-> unique()-> numberBetween(1, 10),
        'tariff_payamount' => $faker->numberBetween(100, 200),
        'tariff_caryear' => $faker-> numberBetween(10, 20),
        'tariff_regamount'=> $faker->numberBetween(300, 400),
        'tariff_obs'=>$faker-> word,
    ];
});
