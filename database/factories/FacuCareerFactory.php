<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\FacuCareer;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
//'user_id' => $faker->unique()->numberBetween(1, App\User::count()),

$factory->define(FacuCareer::class, function (Faker $faker) {
    return [
      'facu_career_id' => $faker-> unique()->numberBetween(1, 10),
      'fcar_name' => $faker-> company,
      //'career_id' => $faker-> unique()->numberBetween(1, App\Career::count()),
      'career_id' => $faker ->numberBetween(12, 21),
      'faculty_id' => $faker->numberBetween(1, 10),
      'tcareer_id' => $faker->numberBetween(1, 10),

    ];
});
