<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RequiredDoc;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(RequiredDoc::class, function (Faker $faker) {
    return [
        'proftype_id' => $faker-> numberBetween(1, 10),
        'reqdoc_description' => $faker-> company,
        'reqdoc_doctype' => $faker-> word,
        'reqdoc_qty' => $faker-> numberBetween(1, 10),
        'reqdoc_required' => $faker-> boolean
    ];
});
