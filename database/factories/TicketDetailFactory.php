<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TicketDetail;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(TicketDetail::class, function (Faker $faker) {
    return [
        'ticket_id' => $faker-> numberBetween(1, 3),
        'tdtype_id' => $faker-> numberBetween(1, 10),
        'tickdet_monto' => $faker-> numberBetween(1, 10),
        'tickdet_iva' => $faker-> numberBetween(1, 10),
        'tickdet_qty' => $faker-> numberBetween(1, 10),
        'tickdet_desciption' => $faker-> word,
        'tickdet_ref' => $faker-> numberBetween(1, 10),
    ];
});
