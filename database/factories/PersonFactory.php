<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Person;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Person::class, function (Faker $faker) {
    return [
        'person_id' => $faker-> unique()->numberBetween(1, 10),
        'homecity_id' => $faker-> numberBetween(1, 10),
        'birthplace_id' => $faker->numberBetween(1, 10),
        'country_id' => $faker->numberBetween(1, 10),
        'person_fname' => $faker-> firstName,
        'person_lastname' => $faker-> lastName,
        'person_birthdate' => $faker-> date,
        'person_gender' => $faker-> title,
        'person_idnumber' => $faker-> latitude,
        'person_address' => $faker-> address,
        'person_bloodtype' => $faker-> word,
        'person_photo' => $faker-> word,
        'person_business_name' => $faker-> word,
        'person_ruc' => $faker-> postcode,

    ];
});
