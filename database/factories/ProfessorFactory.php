<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Professor;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Professor::class, function (Faker $faker) {
    return [
        'professor_id' => $faker -> unique()->numberBetween(1, 10),
        'person_id' => $faker-> numberBetween(6, 10),
        'professor_year_start' => $faker-> year,
        'professor_observation' => $faker-> word,
        'professor_status' => $faker-> word,

    ];
});
