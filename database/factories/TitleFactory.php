<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Title;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Title::class, function (Faker $faker) {
    return [
        'person_id' => $faker->numberBetween(6, 10),
        'title_description' => $faker-> company,
        'title_type_title' => $faker-> word,
        //'title_status ' => $faker-> boolean,
        'title_doument' => $faker-> word,

    ];
});
