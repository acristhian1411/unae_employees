<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\StudentRetention;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(StudentRetention::class, function (Faker $faker) {
    return [
        'person_id' => $faker-> numberBetween(9, 10),
        'civil_status' => $faker-> suffix,
        'sons' => $faker-> numberBetween(1, 10),
        'sibling' => $faker-> numberBetween(1, 10),
        'emfermedad' => $faker-> word,

    ];
});
