<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Workplace;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Workplace::class, function (Faker $faker) {
    return [
        'wplace_description' => $faker-> company,
        'wplace_address' => $faker-> address,
        'wplace_telephone' => $faker-> phoneNumber,
    ];
});
