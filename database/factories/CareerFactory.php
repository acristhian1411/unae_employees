<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Career;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Career::class, function (Faker $faker) {
    return [
        'career_name' => $faker->company,
        'career_code' => $faker->postcode,
        'career_semsqity' => $faker->biasedNumberBetween($min = 1, $max = 9, $function = 'sqrt'),
        'career_logo' => $faker->imageUrl($width = 240, $height = 80) ,
    ];
});
