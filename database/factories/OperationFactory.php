<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Operation;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Operation::class, function (Faker $faker) {
    return [
        'ivatype_id' => $faker->numberBetween(5, 9),
        'oper_description' => $faker-> company,
        'oper_amount' => $faker->numberBetween(1, 10),
    ];
});
