<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Healthcare;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Healthcare::class, function (Faker $faker) {
    return [
        'hcare_description' => $faker-> company,
        'hcare_address' => $faker-> address,
        'hcare_contacto' => $faker-> firstName,
        'hcare_telephone' => $faker-> phoneNumber,
    ];
});
