<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\FacucarAdm;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
//'user_id' => $faker->unique()->numberBetween(1, App\User::count()),

$factory->define(FacucarAdm::class, function (Faker $faker) {
    return [
      'fcaradm_id' => $faker-> unique()->numberBetween(1, 10),
      'facu_career_id' => $faker->numberBetween(1, App\FacuCareer::count()),
      'fcaradm_fee1sem' => $faker-> numberBetween(1, 10),
      'fcaradm_fee2sem' => $faker-> numberBetween(1, 10),
      'fcaradm_dueday' => $faker-> numberBetween(1, 31),
      'fcaradm_year' => $faker->numberBetween(2000, 3500),
      //'career_id' => $faker-> unique()->numberBetween(1, App\Career::count()),

    ];
});
