<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Subject;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Subject::class, function (Faker $faker) {
    return [
        'subj_code' => $faker-> word,
        'subj_name' => $faker-> company,
        'subj_durationhs' => $faker-> numberBetween(1, 10),
        'subj_weeklyhs' => $faker-> numberBetween(1, 10),
        'sems_id' => $faker-> numberBetween(1, 10),
        'subj_mattertype' => $faker-> word,

    ];
});
