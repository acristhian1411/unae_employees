<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CareersSubject;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CareersSubject::class, function (Faker $faker) {
    return [
        'facucar_id' => $faker->numberBetween(1, 3),
        'materia_id' => $faker->numberBetween(1, 3),
        'person_id' => $faker->numberBetween(6, 10),
        'carsubj_year' => $faker->numberBetween(1, 3),
        'carsubj_shift' => $faker-> company,
        'carsubj_startdate' => $faker-> date,
    ];
});
