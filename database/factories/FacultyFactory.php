<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Faculties;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Faculties::class, function (Faker $faker) {
    return [
        'facu_name' => $faker->company,
        'facu_code' => $faker->postcode,
        'facu_unit_id' => $faker->biasedNumberBetween($min = 2, $max = 10, $function = 'sqrt'),
        'facu_logo' => $faker->imageUrl($width = 240, $height = 80) ,
    ];
});
