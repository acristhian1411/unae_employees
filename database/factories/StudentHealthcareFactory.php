<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\StudentHealthcare;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(StudentHealthcare::class, function (Faker $faker) {
    return [
        'person_id' => $faker-> numberBetween(6, 10),
        'hcare_id' => $faker-> numberBetween(1, 10),
        'stuhcare_carnet' => $faker-> word,
    ];
});
