<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('get_subjects', 'Student\StudentController@getSubjectFromStudentApp');
Route::get('get_semesters', 'Semesters\SemesterController@getSemestersFromStudentApp');
Route::get('get_enroll/{id}', 'Semesters\SemesterController@getEnrollFromStudentApp');
Route::get('history_payments','Student\StudentController@getHistoryPaymentFromStudentApp');
Route::get('get_profile','Student\StudentController@getProfileFromStudentApp');
Route::get('get_status_account', 'Student\StudentController@getStatusAccountFromStudentApp');
Route::get('search_subjects', 'Student\StudentController@searchSubjectFromStudentApp');
Route::get('search_semesters', 'Semesters\SemesterController@searchSemestersFromStudentApp');//falta agregar la funcion
Route::get('subj_details/{id}', 'CarsubEnrolled\CarsubEnrolledController@subjDetailFromStudentApp');
Route::get('get_all', 'Student\StudentController@getAllFromStudentApp');
