<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('enrolleds/{id}', 'Enrolleds\EnrolledController@show');
Route::get('student-last-enrolled', 'Enrolleds\EnrolledController@showLastEnrolledStudent');
Route::get('get-enrolled', 'Enrolleds\EnrolledController@get_career_enroll');
Route::post('enrolleds/', 'Enrolleds\EnrolledController@store');
Route::put('enrolleds/{id}', 'Enrolleds\EnrolledController@update');

Route::get('countries/', 'Countries\CountryController@index');

Route::get('countries/{id}', 'Countries\CountryController@show');

Route::get('countries/search/{req}', 'Countries\CountryController@search');

Route::get('departments/', 'Departments\DepartmentController@index');
Route::get('departments/select/{req}', 'Departments\DepartmentController@select');
Route::get('departments/{id}', 'Departments\DepartmentController@show');
Route::get('departments/search/{req}', 'Departments\DepartmentController@search');
Route::get('departCountry/{id}', 'Departments\DepartmentController@country');

Route::get('cities/', 'Cities\CitiesController@index');
Route::get('cities/select/{req}', 'Cities\CitiesController@select');
Route::get('cities/{id}', 'Cities\CitiesController@show');
Route::get('cities/search/{req}', 'Cities\CitiesController@search');

Route::get('facuCareers/', 'FacuCareers\FacuCareersController@index');
Route::get('facuCareers/{id}', 'FacuCareers\FacuCareersController@show');
Route::get('facuCareers/faculties/{id}', 'FacuCareers\FacuCareersController@showFaculties');
Route::get('facuCareers/careers/{id}', 'FacuCareers\FacuCareersController@showCareers');
Route::get('facuCareers/search/{req}', 'FacuCareers\FacuCareersController@search');

Route::get('careers', 'Careers\CareersController@index');
Route::get('careers/{id}', 'Careers\CareersController@showFromPreEnrolled');
// Route::get('careers/{id}', 'Careers\CareersController@showFromPreEnrolled');
Route::get('payplansCareers/{id}', 'Payplans\PayplanController@showCareer');
Route::get('careers-search', 'Careers\CareersController@search');
Route::get('semesters/career/{id}', 'Semesters\SemesterController@semester_career');

// Route::get('persons/', 'Persons\PersonController@index');
Route::get('persons/{id}', 'Persons\PersonController@show');
// Route::get('persons-search', 'Persons\PersonController@search');

Route::post('students-preenrolled', 'Student\StudentController@storeFromPreEnrolled');

Route::get('contactPersons-per/{id}', 'ContactPersonsController@showPerson');
Route::post('contactPersons-per/', 'ContactPersonsController@storeFromPerson');

Route::post('retentionAnswers/', 'StudentRetentionAnswers\StudentRetentionAnswersController@store');
Route::get('retentionQuestionParameters/{id}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@showWithParameters');
Route::put('students/{id}', 'Student\StudentController@update');
Route::put('contactPersons', 'ContactPersonsController@updateFromArray');

Route::get('hstitle', 'HschollTitle\HschollTitleController@index');
Route::get('hstitle/{id}', 'HschollTitle\HschollTitleController@show');

Route::get('schools/', 'Schools\SchoolController@index');
Route::get('schools/{id}', 'Schools\SchoolController@show');
Route::get('schools-search/{req}', 'Schools\SchoolController@search');

Route::get('contactTypes/', 'ContactTypes\ContactTypesController@index');
Route::get('contactTypes/{id}', 'ContactTypes\ContactTypesController@show');

Route::get('precareers', 'Careers\CareersController@edit');
Route::get('students-search', 'Student\StudentController@search');

Route::get('carsubject/semester/{id}', 'CareerSubjects\CareersSubjectController@showFromSemester');
