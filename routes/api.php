<?php
ini_set('max_execution_time', -1);

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::middleware('auth:sanctum')->get('/user', 'Users\UserController@get_user');
// Route::get('auth/google/url', 'Auth\LoginController@redirectToProvider');
// Route::get('auth/google/callback', 'Auth\LoginController@handleProviderCallback');

//rutas de usuarios
Route::get('users/', 'Users\UserController@index')->middleware('permission:users_index');
Route::get('users/{id}', 'Users\UserController@show');
//->middleware('permission:users_show');
Route::get('users_has_roles', 'Users\UserController@hasRoles');
Route::post('users/', 'Users\UserController@store')->middleware('permission:users_store');
Route::get('users/search/{req}', 'Users\UserController@search')->middleware('permission:users_search');
Route::put('users/{id}', 'Users\UserController@update')->middleware('permission:users_update');
Route::post('users/reset-password/{id}', 'Users\UserController@resetPassword2')->middleware('permission:users_update');
Route::put('reset-password', 'Users\UserController@resetPassword');
Route::delete('users/{id}', 'Users\UserController@destroy')->middleware('permission:users_destroy');
Route::get('users-person/{id}', 'Users\UserController@showPerson')->middleware('permission:users_show');
Route::get('users-details/{id}', 'Users\UserController@showPerson2')->middleware('permission:users_show');

#rutas para auditoria
Route::get('audit_logs/', 'Audits\AuditController@index');

//rutas de roles
Route::get('roles/', 'Roles\RoleController@index')->middleware('permission:roles_index');
Route::get('roles/{id}', 'Roles\RoleController@show')->middleware('permission:roles_show');
Route::post('roles/', 'Roles\RoleController@store')->middleware('permission:roles_store');
Route::get('roles/search/{req}', 'Roles\RoleController@search')->middleware('permission:roles_search');
Route::put('roles/{id}', 'Roles\RoleController@update')->middleware('permission:roles_update');
Route::delete('roles/{id}', 'Roles\RoleController@destroy')->middleware('permission:roles_destroy');
Route::get('roles_has_permission', 'Roles\RoleController@hasPermission');
Route::get('roles_get_permission/{id}', 'Roles\RoleController@getPermissions')->middleware('permission:roles_show');
Route::get('roles_get_permission/exclude/{id}', 'Roles\RoleController@getPermissionsExclude')->middleware('permission:roles_show');
Route::post('roles_give_permissions', 'Roles\RoleController@givePermissions')->middleware('permission:roles_show');
Route::post('roles_revoke_permissions', 'Roles\RoleController@revokePermissions')->middleware('permission:roles_show');


//rutas de units
Route::get('units/', 'Units\UnitController@index')->middleware('permission:units_index');
Route::get('units/{id}', 'Units\UnitController@show');
//->middleware('permission:units_show');
Route::get('units/search/{req}', 'Units\UnitController@search')->middleware('permission:units_search');
Route::post('units', 'Units\UnitController@store')->middleware('permission:units_store');
Route::put('units/{id}', 'Units\UnitController@update')->middleware('permission:units_update');
Route::delete('units/{id}', 'Units\UnitController@destroy')->middleware('permission:units_destroy');

//rutas de unit_types
Route::get('unit_types/', 'UnitTypes\UnitTypeController@index')->middleware('permission:units_index');
Route::get('unit_types/{id}', 'UnitTypes\UnitTypeController@show')->middleware('permission:units_show');
Route::get('unit_types/search/{req}', 'UnitTypes\UnitTypeController@search')->middleware('permission:units_search');
Route::post('unit_types', 'UnitTypes\UnitTypeController@store')->middleware('permission:units_store');
Route::put('unit_types/{id}', 'UnitTypes\UnitTypeController@update')->middleware('permission:units_update');
Route::delete('unit_types/{id}', 'UnitTypes\UnitTypeController@destroy')->middleware('permission:units_destroy');
// Route::group(['middleware' => ['permission:destroy_units']], function () {
//     Route::delete('units/{id}','Units\UnitController@destroy')->middleware('permission:careertypes_destroy');
// });
// Route::delete('units/{id}','Units\UnitController@destroy')->middleware('permission:careertypes_destroy');

//rutas de CareerType
Route::get('careertypes/', 'CareerTypes\CareerTypesController@index')->middleware('permission:careertypes_index');
Route::get('careertypes/{id}', 'CareerTypes\CareerTypesController@show')->middleware('permission:careertypes_show');
Route::get('enroll-careertype/{id}', 'CareerTypes\CareerTypesController@showByEnroll')->middleware('permission:careertypes_show');
Route::post('careertypes/', 'CareerTypes\CareerTypesController@store')->middleware('permission:careertypes_store');
Route::get('careertypes/search/{req}', 'CareerTypes\CareerTypesController@search')->middleware('permission:careertypes_search');
Route::put('careertypes/{career_type}', 'CareerTypes\CareerTypesController@update')->middleware('permission:careertypes_update');
Route::delete('careertypes/{career_type}', 'CareerTypes\CareerTypesController@destroy')->middleware('permission:careertypes_destroy');

//rutas de Facultades
Route::get('faculties/', 'Faculties\FacultiesController@index')->middleware('permission:faculties_index');
Route::get('faculties/{id}', 'Faculties\FacultiesController@show')->middleware('permission:faculties_show');
Route::get('demographic', 'Faculties\FacultiesController@demographic')->middleware('permission:faculties_show');
Route::get('faculties/unit/{id}', 'Faculties\FacultiesController@showUnit')->middleware('permission:faculties_show');
Route::get('faculties/search/{req}', 'Faculties\FacultiesController@search')->middleware('permission:faculties_search');
Route::post('faculties/', 'Faculties\FacultiesController@store')->middleware('permission:faculties_store');
Route::put('faculties/{id}', 'Faculties\FacultiesController@update')->middleware('permission:faculties_update');
Route::delete('faculties/{id}', 'Faculties\FacultiesController@destroy')->middleware('permission:faculties_destroy');

//rutas de Materias
Route::get('subjects/', 'Subject\SubjectController@index')->middleware('permission:subjects_index');
Route::get('subjects/{id}', 'Subject\SubjectController@show')->middleware('permission:subjects_show');
Route::get('subjects-search/{search}', 'Subject\SubjectController@search')->middleware('permission:subjects_search');
Route::get('subjects/career/{id}', 'Subject\SubjectController@carsub');
Route::get('subjects/semester/{id}', 'Subject\SubjectController@subjSems');
Route::get('subjsemesNull/', 'Subject\SubjectController@semesterIsNull');
Route::post('subjects/', 'Subject\SubjectController@store')->middleware('permission:subjects_store');
Route::post('workshops/', 'Subject\SubjectController@storeFromCareersForm')->middleware('permission:subjects_store');
Route::put('subjects/{id}', 'Subject\SubjectController@update')->middleware('permission:subjects_update');
Route::put('subjects/semester/add/{id}', 'Subject\SubjectController@addSemester');
Route::delete('subjects/{id}', 'Subject\SubjectController@destroy')->middleware('permission:subjects_destroy');
//rutas de PreRequirements
Route::get('prereqs/', 'PreRequirements\PreRequirementsController@index')->middleware('permission:prereqs_index');
Route::get('prereqs/{id}', 'PreRequirements\PreRequirementsController@show')->middleware('permission:prereqs_show');
Route::get('prereqs/subjects/{id}', 'PreRequirements\PreRequirementsController@subjects');
Route::post('prereqs/', 'PreRequirements\PreRequirementsController@store')->middleware('permission:prereqs_store');
Route::put('prereqs/{id}', 'PreRequirements\PreRequirementsController@update')->middleware('permission:prereqs_update');
Route::delete('prereqs/{id}', 'PreRequirements\PreRequirementsController@destroy')->middleware('permission:prereqs_destroy');

//rutas de Carreras
Route::get('precareers', 'Careers\CareersController@edit');
// ->middleware('permission:careers_index');
Route::get('careers', 'Careers\CareersController@index')->middleware('permission:careers_index');
Route::get('subject_control/', 'Careers\CareersController@subject_control')->middleware('permission:subjects_index');
Route::get('faculty_careers/{id}', 'Careers\CareersController@facultyCareers')->middleware('permission:careers_index');
Route::get('careers/{id}', 'Careers\CareersController@show')->middleware('permission:careers_show');
Route::get('careers-search', 'Careers\CareersController@search')->middleware('permission:careers_search');
Route::get('careers-students/{id}', 'Careers\CareersController@showStudentsFromCareer')->middleware('permission:careers_show');
Route::post('careers/', 'Careers\CareersController@store')->middleware('permission:careers_store');
Route::put('careers/{id}', 'Careers\CareersController@update')->middleware('permission:careers_update');
Route::delete('careers/{id}', 'Careers\CareersController@destroy')->middleware('permission:careers_destroy');
//rutas para congresos
Route::get('congress', 'Careers\CareersController@indexCongress')->middleware('permission:careers_index');
Route::get('congress-search', 'Careers\CareersController@search2')->middleware('permission:careers_search');
Route::get('congress-detail/{id}', 'Careers\CareersController@getCongressDetails')->middleware('permission:careers_index');
Route::post('update-enrolled_limit', 'CarsubEnrolled\CarsubEnrolledController@enrolledLimit');
Route::post('congress-to-save', 'CarsubEnrolled\CarsubEnrolledController@save_congress_data');

//rutas de Becas
Route::get('scholarship', 'Scholarship\ScholarshipController@index')->middleware('permission:scholarship_index');
Route::get('scholarship/{id}', 'Scholarship\ScholarshipController@show')->middleware('permission:scholarship_show');
Route::get('scholarship-search/{id}', 'Scholarship\ScholarshipController@search');
Route::post('scholarship/', 'Scholarship\ScholarshipController@store')->middleware('permission:scholarship_store');
Route::put('scholarship/{id}', 'Scholarship\ScholarshipController@update')->middleware('permission:scholarship_update');
Route::delete('scholarship/{id}', 'Scholarship\ScholarshipController@destroy')->middleware('permission:scholarship_destroy');

//rutas de Becas
Route::get('titles', 'Titles\TitlesController@index')->middleware('permission:titles_index');
Route::get('titles/{id}', 'Titles\TitlesController@show')->middleware('permission:titles_show');
Route::get('titles-person/{id}', 'Titles\TitlesController@showPerson');
Route::post('titles/', 'Titles\TitlesController@store')->middleware('permission:titles_store');
Route::put('titles/{id}', 'Titles\TitlesController@update')->middleware('permission:titles_update');
Route::delete('titles/{id}', 'Titles\TitlesController@destroy')->middleware('permission:titles_destroy');

//rutas de Titulos de bachillerato
Route::get('hstitle', 'HschollTitle\HschollTitleController@index')->middleware('permission:hstitle_index');
Route::get('hstitle/{id}', 'HschollTitle\HschollTitleController@show')->middleware('permission:hstitle_show');
Route::get('hstitle-search', 'HschollTitle\HschollTitleController@search')->middleware('permission:hstitle_search');
Route::post('hstitle/', 'HschollTitle\HschollTitleController@store')->middleware('permission:hstitle_store');
Route::put('hstitle/{id}', 'HschollTitle\HschollTitleController@update')->middleware('permission:hstitle_update');
Route::delete('hstitle/{id}', 'HschollTitle\HschollTitleController@destroy')->middleware('permission:hstitle_destroy');


//rutas de facuCarAdm
Route::get('facuCarAdm', 'FacucarAdm\FacucarAdmController@index')->middleware('permission:facuCarAdm_index');
Route::get('facuCarAdm/{id}', 'FacucarAdm\FacucarAdmController@show')->middleware('permission:facuCarAdm_show');
Route::get('facuCarAdm/career/{id}', 'FacucarAdm\FacucarAdmController@showCareer');
Route::get('facuCarAdm/careers/{id}', 'FacucarAdm\FacucarAdmController@showCareers');
Route::get('facuCarAdm-year', 'FacucarAdm\FacucarAdmController@showSemester');
Route::get('facuCarAdm/search/{req}', 'FacucarAdm\FacucarAdmController@search')->middleware('permission:facuCarAdm_search');
Route::post('facuCarAdm/', 'FacucarAdm\FacucarAdmController@store')->middleware('permission:facuCarAdm_store');
Route::post('facuCarAdms/', 'FacucarAdm\FacucarAdmController@storeFromArray')->middleware('permission:facuCarAdm_store');
Route::put('facuCarAdms/', 'FacucarAdm\FacucarAdmController@updateFromArray')->middleware('permission:facuCarAdm_update');
Route::put('facuCarAdm/{id}', 'FacucarAdm\FacucarAdmController@update')->middleware('permission:facuCarAdm_update');
Route::delete('facuCarAdm/{id}', 'FacucarAdm\FacucarAdmController@destroy')->middleware('permission:facuCarAdm_destroy');

//rutas de Semestres
Route::get('semesters/', 'Semesters\SemesterController@index')->middleware('permission:semesters_index');
Route::get('semesters/{id}', 'Semesters\SemesterController@show')->middleware('permission:semesters_show');
Route::get('semesters_enrolled', 'Semesters\SemesterController@showSemestersAndEnrolleds');
Route::get('semesters/career/{id}', 'Semesters\SemesterController@semester_career');
Route::get('semesters/students/{id}', 'Semesters\SemesterController@showStudents');
Route::get('semesters/studentaccount/{id}', 'Semesters\SemesterController@showStudentAccounts');
Route::get('semesters/search/{req}', 'Semesters\SemesterController@search');
// ->middleware('permission:semesters_search');
Route::post('semesters/', 'Semesters\SemesterController@store')->middleware('permission:semesters_store');
Route::put('semesters/{id}', 'Semesters\SemesterController@update')->middleware('permission:semesters_update');
Route::delete('semesters/{id}', 'Semesters\SemesterController@destroy')->middleware('permission:semesters_destroy');

//rutas de tipos de evaluacion
Route::get('evaluationTypes/', 'EvaluationTypes\EvaluationTypesController@index')->middleware('permission:evaluationTypes_index');
Route::get('evaluationTypes/{id}', 'EvaluationTypes\EvaluationTypesController@show')->middleware('permission:evaluationTypes_show');
Route::post('evaluationTypes/', 'EvaluationTypes\EvaluationTypesController@store')->middleware('permission:evaluationTypes_store');
Route::put('evaluationTypes/{id}', 'EvaluationTypes\EvaluationTypesController@update')->middleware('permission:evaluationTypes_update');
Route::delete('evaluationTypes/{id}', 'EvaluationTypes\EvaluationTypesController@destroy')->middleware('permission:evaluationTypes_destroy');

//rutas de retencion de estudiantes
Route::get('retentionStudent/', 'StudentRetention\StudentRetentionController@index');
// ->middleware('permission:retentionAnswer_index');
Route::get('retentionStudent/{id}', 'StudentRetention\StudentRetentionController@show')->middleware('permission:retentionStudent_show');
Route::get('retentionStudent-per/{id}', 'StudentRetention\StudentRetentionController@showStudent');
Route::post('retentionStudent/', 'StudentRetention\StudentRetentionController@store')->middleware('permission:retentionStudent_store');
Route::put('retentionStudent/{id}', 'StudentRetention\StudentRetentionController@update')->middleware('permission:retentionStudent_update');
Route::delete('retentionStudent/{id}', 'StudentRetention\StudentRetentionController@destroy')->middleware('permission:retentionStudent_destroy');

//rutas de becas de estudiantes
Route::get('studentScholarship/', 'StudentScholarship\StudentScholarshipController@index')->middleware('permission:studentScholarship_index');
Route::get('studentScholarship/{id}', 'StudentScholarship\StudentScholarshipController@showScholarship');
Route::get('studentScholarship-per/{id}', 'StudentScholarship\StudentScholarshipController@showStudent');
Route::post('studentScholarship/', 'StudentScholarship\StudentScholarshipController@store')->middleware('permission:studentScholarship_store');
Route::put('studentScholarship/{id}', 'StudentScholarship\StudentScholarshipController@update')->middleware('permission:studentScholarship_update');
Route::delete('studentScholarship/{id}', 'StudentScholarship\StudentScholarshipController@destroy')->middleware('permission:studentScholarship_destroy');

//rutas de lugares de trabajo de estudiantes
Route::get('studentWorkplaces/', 'StudentWorkplaces\StudentWorkplacesController@index')->middleware('permission:StudentWorkplaces_index');
Route::get('studentWorkplaces/{id}', 'StudentWorkplaces\StudentWorkplacesController@showWorkplaces');
Route::get('studentWorkplaces-per/{id}', 'StudentWorkplaces\StudentWorkplacesController@showStudent');
Route::post('studentWorkplaces/', 'StudentWorkplaces\StudentWorkplacesController@store')->middleware('permission:StudentWorkplaces_store');
Route::put('studentWorkplaces/{id}', 'StudentWorkplaces\StudentWorkplacesController@update')->middleware('permission:StudentWorkplaces_update');
Route::delete('studentWorkplaces/{id}', 'StudentWorkplaces\StudentWorkplacesController@destroy')->middleware('permission:StudentWorkplaces_destroy');


//rutas de countries
Route::get('countries/', 'Countries\CountryController@index')->middleware('permission:countries_index');
Route::get('countries/{id}', 'Countries\CountryController@show')->middleware('permission:countries_show');
Route::get('countries/search/{req}', 'Countries\CountryController@search')->middleware('permission:countries_search');
Route::post('countries/', 'Countries\CountryController@store')->middleware('permission:countries_store');
Route::put('countries/{id}', 'Countries\CountryController@update')->middleware('permission:countries_update');
Route::delete('countries/{id}', 'Countries\CountryController@destroy')->middleware('permission:countries_destroy');


//rutas de holiday
Route::get('holiday/', 'Holidays\HolidaysController@index')->middleware('permission:holiday_index');
Route::get('holiday/{id}', 'Holidays\HolidaysController@show')->middleware('permission:holiday_show');
Route::get('holiday/search/{req}', 'Holidays\HolidaysController@search')->middleware('permission:holiday_search');
Route::post('holiday/', 'Holidays\HolidaysController@store')->middleware('permission:holiday_store');
Route::put('holiday/{id}', 'Holidays\HolidaysController@update')->middleware('permission:holiday_update');
Route::delete('holiday/{id}', 'Holidays\HolidaysController@destroy')->middleware('permission:holiday_destroy');

//rutas de categories
Route::get('categories/', 'Categories\CategoryController@index')->middleware('permission:category_index');
Route::get('categories/{id}', 'Categories\CategoryController@show')->middleware('permission:category_show');
Route::get('categories/search/{req}', 'Categories\CategoryController@search')->middleware('permission:category_search');
Route::post('categories/', 'Categories\CategoryController@store')->middleware('permission:category_store');
Route::put('categories/{id}', 'Categories\CategoryController@update')->middleware('permission:category_update');
Route::delete('categories/{id}', 'Categories\CategoryController@destroy')->middleware('permission:category_destroy');

//rutas de departments
Route::get('departments/', 'Departments\DepartmentController@index')->middleware('permission:departments_index');
Route::get('departments/select/{req}', 'Departments\DepartmentController@select');
Route::get('departments/{id}', 'Departments\DepartmentController@show')->middleware('permission:departments_show');
Route::get('departments/search/{req}', 'Departments\DepartmentController@search')->middleware('permission:departments_search');
Route::get('departCountry/{id}', 'Departments\DepartmentController@country');
Route::post('departments', 'Departments\DepartmentController@store')->middleware('permission:departments_store');
Route::put('departments/{id}', 'Departments\DepartmentController@update')->middleware('permission:departments_update');
Route::delete('departments/{id}', 'Departments\DepartmentController@destroy')->middleware('permission:departments_destroy');

//rutas de RequiredDoc
Route::get('requiredDocs/', 'RequiredDocs\ReqDocsController@index')->middleware('permission:requiredDocs_index');
Route::get('requiredDocs/{id}', 'RequiredDocs\ReqDocsController@show')->middleware('permission:requiredDocs_show');
Route::get('requiredDocs-student/{req}', 'RequiredDocs\ReqDocsController@showStudent');
Route::get('requiredDocs/search/{req}', 'RequiredDocs\ReqDocsController@search')->middleware('permission:requiredDocs_search');
Route::post('requiredDocs', 'RequiredDocs\ReqDocsController@store')->middleware('permission:requiredDocs_store');
Route::put('requiredDocs/{id}', 'RequiredDocs\ReqDocsController@update')->middleware('permission:requiredDocs_update');
Route::delete('requiredDocs/{id}', 'RequiredDocs\ReqDocsController@destroy')->middleware('permission:requiredDocs_destroy');

//rutas de HealtCares
Route::get('healtCares/', 'HealtCares\HealtCaresController@index')->middleware('permission:healtCares_index');
Route::get('healtCares/{id}', 'HealtCares\HealtCaresController@show')->middleware('permission:healtCares_show');
Route::post('healtCares/', 'HealtCares\HealtCaresController@store')->middleware('permission:healtCares_store');
Route::put('healtCares/{id}', 'HealtCares\HealtCaresController@update')->middleware('permission:healtCares_update');
Route::delete('healtCares/{id}', 'HealtCares\HealtCaresController@destroy')->middleware('permission:healtCares_destroy');

//rutas de HealtCares
Route::get('operations/', 'Operations\OperationsController@index')->middleware('permission:operations_index');
Route::get('operations/{id}', 'Operations\OperationsController@show')->middleware('permission:operations_show');
Route::post('operations/', 'Operations\OperationsController@store')->middleware('permission:operations_store');
Route::put('operations/{id}', 'Operations\OperationsController@update')->middleware('permission:operations_update');
Route::delete('operations/{id}', 'Operations\OperationsController@destroy')->middleware('permission:operations_destroy');

//rutas de Workplace
Route::get('workplaces/', 'Workplace\WorkplaceController@index')->middleware('permission:workplaces_index');
Route::get('workplaces/{id}', 'Workplace\WorkplaceController@show')->middleware('permission:workplaces_show');
Route::post('workplaces/', 'Workplace\WorkplaceController@store')->middleware('permission:workplaces_store');
Route::put('workplaces/{id}', 'Workplace\WorkplaceController@update')->middleware('permission:workplaces_update');
Route::delete('workplaces/{id}', 'Workplace\WorkplaceController@destroy')->middleware('permission:workplaces_destroy');

//rutas de Workplace
Route::get('documentPerson/', 'DocumentPerson\DocumentPersonController@index')->middleware('permission:documentPerson_index');
Route::get('docpersons/semester/{id}', 'DocumentPerson\DocumentPersonController@fromSemester')->middleware('permission:documentPerson_index');
Route::get('documentPerson/{id}', 'DocumentPerson\DocumentPersonController@show')->middleware('permission:documentPerson_show');
Route::get('documentPerson-doc/', 'DocumentPerson\DocumentPersonController@person_show');
Route::post('documentPerson/', 'DocumentPerson\DocumentPersonController@store')->middleware('permission:documentPerson_store');
Route::put('documentPerson/{id}', 'DocumentPerson\DocumentPersonController@update')->middleware('permission:documentPerson_update');
Route::delete('documentPerson/{id}', 'DocumentPerson\DocumentPersonController@destroy')->middleware('permission:documentPerson_destroy');


//rutas de persona
Route::get('persons/', 'Persons\PersonController@index')->middleware('permission:persons_index');
Route::get('persontype/{id}', 'Persons\PersonController@persontype')->middleware('permission:persons_index');
Route::get('persons/{id}', 'Persons\PersonController@show')->middleware('permission:persons_show');
Route::get('persons-search', 'Persons\PersonController@search')->middleware('permission:persons_search');
Route::post('persons', 'Persons\PersonController@store')->middleware('permission:persons_store');
Route::put('persons/{id}', 'Persons\PersonController@update')->middleware('permission:persons_update');
Route::delete('persons/{id}', 'Persons\PersonController@destroy')->middleware('permission:persons_destroy');

//rutas de estudiante
Route::get('students/', 'Student\StudentController@index')->middleware('permission:students_index');
Route::get('students/{id}', 'Student\StudentController@show')->middleware('permission:students_show');
Route::get('students-search', 'Student\StudentController@search');
//->middleware('permission:students_search');
Route::post('students', 'Student\StudentController@store')->middleware('permission:students_store');
Route::get('student_careers/{id}', 'Student\StudentController@student_careers')->middleware('permission:students_index');
Route::get('student_scores/{person_id}/{careers}', 'Student\StudentController@student_scores')->middleware('permission:students_index');
Route::post('students-preenrolled', 'Student\StudentController@storeFromPreEnrolled')->middleware('permission:students_store');
Route::put('students/{id}', 'Student\StudentController@update')->middleware('permission:students_update');
Route::delete('students/{id}', 'Student\StudentController@destroy')->middleware('permission:students_destroy');

//rutas de funcionarios
Route::get('employees/', 'Employees\EmployeesController@index')->middleware('permission:employees_index');
Route::get('employees/{id}', 'Employees\EmployeesController@show')->middleware('permission:employees_show');
Route::get('employees-laboral-data/{id}', 'Employees\EmployeesController@laboral_data')->middleware('permission:employees_show');
Route::post('employees-add-discount/{id}', 'Employees\EmployeesController@addDiscount')->middleware('permission:employees_show');
Route::put('employees-update-salary/{id}', 'Employees\EmployeesController@updateSalary')->middleware('permission:employees_show');
Route::get('employeeSalary', 'Employees\EmployeesController@generateSalary');
Route::get('working_hours/{id}', 'Employees\EmployeesController@getWorkingHours')->middleware('permission:employees_index');
// ->middleware('permission:employee_account_store');
Route::get('employees-search', 'Employees\EmployeesController@search');
// ->middleware('permission:employees_search');
Route::post('employees', 'Employees\EmployeesController@store')->middleware('permission:employees_store');
Route::put('employees/{id}', 'Employees\EmployeesController@update')->middleware('permission:employees_update');
Route::put('employees-person/{id}', 'Employees\EmployeesController@updateFromPerson');
Route::delete('employees/{id}', 'Employees\EmployeesController@destroy')->middleware('permission:employees_destroy');

//rutas de proveedores
Route::get('providers/', 'Provider\ProviderController@index')->middleware('permission:providers_index');
Route::get('providers/{id}', 'Provider\ProviderController@show')->middleware('permission:providers_show');
Route::get('providers-search', 'Provider\ProviderController@search')->middleware('permission:providers_search');
Route::post('providers', 'Provider\ProviderController@store')->middleware('permission:providers_store');
Route::put('providers/{id}', 'Provider\ProviderController@update')->middleware('permission:providers_update');
Route::put('providers-person/{id}', 'Provider\ProviderController@updateFromPerson');
Route::delete('providers/{id}', 'Provider\ProviderController@destroy')->middleware('permission:providers_destroy');

//rutas de proveedores providers_account
Route::get('providers_account/', 'ProviderAccount\ProviderAccountController@index')->middleware('permission:provider_account_index');
Route::get('providers_account/{id}', 'ProviderAccount\ProviderAccountController@show')->middleware('permission:provider_account_show');
Route::get('providers_account-provider/{id}', 'ProviderAccount\ProviderAccountController@showProvider')->middleware('permission:provider_account_show');
Route::post('providers_account', 'ProviderAccount\ProviderAccountController@store')->middleware('permission:provider_account_store');
Route::put('providers_account/{id}', 'ProviderAccount\ProviderAccountController@update')->middleware('permission:provider_account_update');
Route::get('providers_account-search', 'ProviderAccount\ProviderAccountController@search')->middleware('permission:provider_account_index');
Route::delete('providers_account/{id}', 'ProviderAccount\ProviderAccountController@destroy')->middleware('permission:provider_account_destroy');

//rutas de Ciudades
Route::get('cities/', 'Cities\CitiesController@index')->middleware('permission:cities_index');
Route::get('cities/select/{req}', 'Cities\CitiesController@select');
Route::get('cities/{id}', 'Cities\CitiesController@show')->middleware('permission:cities_show');
Route::get('cities/search/{req}', 'Cities\CitiesController@search')->middleware('permission:cities_search');
Route::post('cities/', 'Cities\CitiesController@store')->middleware('permission:cities_store');
Route::put('cities/{id}', 'Cities\CitiesController@update')->middleware('permission:cities_update');
Route::delete('cities/{id}', 'Cities\CitiesController@destroy')->middleware('permission:cities_destroy');


Route::get('checks/', 'Checks\CheksController@index')->middleware('permission:checks_index');
Route::get('checks/select/{req}', 'Checks\CheksController@select')->middleware('permission:checks_index');
Route::get('checks/{id}', 'Checks\CheksController@show')->middleware('permission:checks_show');
Route::get('checks_detials/{id}', 'Checks\CheksController@show_details')->middleware('permission:checks_show');
Route::get('checks-search', 'Checks\CheksController@search')->middleware('permission:checks_search');
Route::post('checks/', 'Checks\CheksController@store')->middleware('permission:checks_store');
Route::put('checks/{id}', 'Checks\CheksController@update')->middleware('permission:checks_update');
Route::delete('checks/{id}', 'Checks\CheksController@destroy')->middleware('permission:checks_destroy');

Route::get('controls', 'ProfessorControls\ProfessorControlsController@index')->middleware('permission:professor_controls_index');
Route::get('controls/select/{req}', 'ProfessorControls\ProfessorControlsController@select')->middleware('permission:professor_controls_index');
Route::get('controls/{id}', 'ProfessorControls\ProfessorControlsController@show')->middleware('permission:professor_controls_show');
Route::get('controls/search/{req}', 'ProfessorControls\ProfessorControlsController@search')->middleware('permission:professor_controls_search');
Route::post('controls', 'ProfessorControls\ProfessorControlsController@store')->middleware('permission:professor_controls_store');
Route::put('controls/{id}', 'ProfessorControls\ProfessorControlsController@update')->middleware('permission:professor_controls_update');
Route::delete('controls/{id}', 'ProfessorControls\ProfessorControlsController@destroy')->middleware('permission:professor_controls_destroy');

Route::get('professorAssistances', 'ProfessorAssistances\ProfessorAssistancesController@index')->middleware('permission:professor_assistances_index');
Route::get('professorAssistances/select/{req}', 'ProfessorAssistances\ProfessorAssistancesController@select')->middleware('permission:professor_assistances_index');
Route::get('professorAssistances/{id}', 'ProfessorAssistances\ProfessorAssistancesController@show')->middleware('permission:professor_assistances_show');
Route::get('professorAssistancesForDay', 'ProfessorAssistances\ProfessorAssistancesController@showAssistancesForADay')->middleware('permission:professor_assistances_show');
Route::get('professorAssistancesGrouped', 'ProfessorAssistances\ProfessorAssistancesController@getAssistGrouped');
// ->middleware('permission:professor_assistances_index');
Route::get('professorAssistances/search/{req}', 'ProfessorAssistances\ProfessorAssistancesController@search')->middleware('permission:professor_assistances_search');
Route::post('professorAssistances', 'ProfessorAssistances\ProfessorAssistancesController@store')->middleware('permission:professor_assistances_store');
Route::put('professorAssistances/{id}', 'ProfessorAssistances\ProfessorAssistancesController@updateFromArray')->middleware('permission:professor_assistances_update');
Route::delete('professorAssistances/{id}', 'ProfessorAssistances\ProfessorAssistancesController@destroy')->middleware('permission:professor_assistances_destroy');


//rutas de Contact types
Route::get('contactTypes/', 'ContactTypes\ContactTypesController@index')->middleware('permission:contactTypes_index');
Route::get('contactTypes/{id}', 'ContactTypes\ContactTypesController@show')->middleware('permission:contactTypes_show');
Route::post('contactTypes/', 'ContactTypes\ContactTypesController@store')->middleware('permission:contactTypes_store');
Route::get('contactTypes/search/{req}', 'ContactTypes\ContactTypesController@search')->middleware('permission:contactTypes_search');
Route::put('contactTypes/{contactTypes}', 'ContactTypes\ContactTypesController@update')->middleware('permission:contactTypes_update');
Route::delete('contactTypes/{contactTypes}', 'ContactTypes\ContactTypesController@destroy')->middleware('permission:contactTypes_destroy');

//rutas de FacuCareers
Route::get('facuCareers/', 'FacuCareers\FacuCareersController@index')->middleware('permission:facuCareers_index');
Route::get('fcareer_inscript', 'FacuCareers\FacuCareersController@get_inscrips')->middleware('permission:facuCareers_index');
Route::get('facuCareers/{id}', 'FacuCareers\FacuCareersController@show')->middleware('permission:facuCareers_show');
Route::get('facuCareers/faculties/{id}', 'FacuCareers\FacuCareersController@showFaculties');
Route::get('facuCareers/careers/{id}', 'FacuCareers\FacuCareersController@showCareers')->middleware('permission:facuCareers_show');
Route::post('facuCareers/', 'FacuCareers\FacuCareersController@store')->middleware('permission:facuCareers_store');
Route::get('facuCareers/search/{req}', 'FacuCareers\FacuCareersController@search')->middleware('permission:facuCareers_search');
Route::put('facuCareers/{id}', 'FacuCareers\FacuCareersController@update')->middleware('permission:facuCareers_update');
Route::delete('facuCareers/{id}', 'FacuCareers\FacuCareersController@destroy')->middleware('permission:facuCareers_destroy');

//rutas de ProfileType
Route::get('profile_types/', 'ProfileTypes\ProfileTypeController@index')->middleware('permission:profile_types_index');
Route::get('profile_types/{id}', 'ProfileTypes\ProfileTypeController@show')->middleware('permission:profile_types_show');
Route::post('profile_types/', 'ProfileTypes\ProfileTypeController@store')->middleware('permission:profile_types_store');
Route::get('profile_types-search', 'ProfileTypes\ProfileTypeController@search')->middleware('permission:profile_types_search');
Route::put('profile_types/{id}', 'ProfileTypes\ProfileTypeController@update')->middleware('permission:profile_types_update');
Route::delete('profile_types/{id}', 'ProfileTypes\ProfileTypeController@destroy')->middleware('permission:profile_types_destroy');

//rutas de Payplans
Route::get('payplans/', 'Payplans\PayplanController@index')->middleware('permission:payplans_index');
Route::get('payplans/{id}', 'Payplans\PayplanController@show')->middleware('permission:payplans_show');
Route::get('payplansFacucarAdm/{id}', 'Payplans\PayplanController@showFacucarAdm');
Route::get('payplansCareers/{id}', 'Payplans\PayplanController@showCareer');
Route::post('payplans/', 'Payplans\PayplanController@store')->middleware('permission:payplans_store');
Route::get('payplans/search/{req}', 'Payplans\PayplanController@search')->middleware('permission:payplans_search');
Route::put('payplans/{id}', 'Payplans\PayplanController@update')->middleware('permission:payplans_update');
Route::delete('payplans/{id}', 'Payplans\PayplanController@destroy')->middleware('permission:payplans_destroy');


//rutas de ivaType
Route::get('ivaType', 'IvaType\IvaTypeController@index')->middleware('permission:ivaType_index');
Route::get('ivaType/{id}', 'IvaType\IvaTypeController@show')->middleware('permission:ivaType_show');
Route::post('ivaType/', 'IvaType\IvaTypeController@store')->middleware('permission:ivaType_store');
Route::put('ivaType/{id}', 'IvaType\IvaTypeController@update')->middleware('permission:ivaType_update');
Route::delete('ivaType/{id}', 'IvaType\IvaTypeController@destroy')->middleware('permission:ivaType_destroy');

//rutas de Cargos
Route::get('appointments/', 'Appointment\AppointmentController@index')->middleware('permission:appointments_index');
Route::get('appointments/{id}', 'Appointment\AppointmentController@show')->middleware('permission:appointments_show');
Route::get('appointments/search/{req}', 'Appointment\AppointmentController@search')->middleware('permission:appointments_search');
Route::post('appointments/', 'Appointment\AppointmentController@store')->middleware('permission:appointments_store');
Route::put('appointments/{id}', 'Appointment\AppointmentController@update')->middleware('permission:appointments_update');
Route::delete('appointments/{id}', 'Appointment\AppointmentController@destroy')->middleware('permission:appointments_destroy');

//rutas de Productos
Route::get('products/', 'Products\ProductController@index')->middleware('permission:products_index');
Route::get('events/', 'Products\ProductController@event_list')->middleware('permission:products_index');
Route::get('sub-items/{id}', 'Products\ProductController@getSubitems')->middleware('permission:products_index');
Route::get('products/{id}', 'Products\ProductController@show')->middleware('permission:products_show');
Route::get('products/search/{req}', 'Products\ProductController@search')->middleware('permission:products_search');
Route::post('products/', 'Products\ProductController@store')->middleware('permission:products_store');
Route::put('products/{id}', 'Products\ProductController@update')->middleware('permission:products_update');
Route::delete('products/{id}', 'Products\ProductController@destroy')->middleware('permission:products_destroy');

//rutas de Schools
Route::get('schools/', 'Schools\SchoolController@index')->middleware('permission:schools_index');
Route::get('schools/{id}', 'Schools\SchoolController@show')->middleware('permission:schools_show');
Route::post('schools/', 'Schools\SchoolController@store')->middleware('permission:schools_store');
Route::get('schools-search', 'Schools\SchoolController@search')->middleware('permission:schools_search');
Route::put('schools/{id}', 'Schools\SchoolController@update')->middleware('permission:schools_update');
Route::delete('schools/{id}', 'Schools\SchoolController@destroy')->middleware('permission:schools_destroy');

//rutas de professor_category
Route::get('professor_categories/', 'ProfessorCategories\ProfessorCategoriesController@index')->middleware('permission:professor_categories_index');
Route::get('professor_categories/{id}', 'ProfessorCategories\ProfessorCategoriesController@show')->middleware('permission:professor_categories_show');
Route::post('professor_categories/', 'ProfessorCategories\ProfessorCategoriesController@store')->middleware('permission:professor_categories_store');
Route::get('professor_categories-search/{req}', 'ProfessorCategories\ProfessorCategoriesController@search')->middleware('permission:professor_categories_search');
Route::put('professor_categories/{id}', 'ProfessorCategories\ProfessorCategoriesController@update')->middleware('permission:professor_categories_update');
Route::delete('professor_categories/{id}', 'ProfessorCategories\ProfessorCategoriesController@destroy')->middleware('permission:professor_categories_destroy');

//rutas de Cajas
Route::get('tills/', 'Tills\TillsController@index')->middleware('permission:tills_index');
Route::get('tills/{id}', 'Tills\TillsController@show')->middleware('permission:tills_show');
Route::get('tills-starting', 'Tills\TillsController@isStarting');
Route::get('tills-current-member/{id}', 'Tills\TillsController@tills_current_member')->middleware('permission:tills_show');
Route::get('tills-search', 'Tills\TillsController@search')->middleware('permission:tills_show');
//->middleware('permission:tills_show');
Route::post('tills/', 'Tills\TillsController@store')->middleware('permission:tills_store');
// ->middleware('permission:tills_store');
Route::put('tills/{id}', 'Tills\TillsController@update')->middleware('permission:tills_update');
Route::put('tills_continue/{id}', 'Tills\TillsController@tillContinue')->middleware('permission:tills_update');
Route::delete('tills/{id}', 'Tills\TillsController@destroy')->middleware('permission:tills_destroy');

//integrantes por caja
Route::get('tills-members/{id}', 'Tills\TillsController@tills_member')->middleware('permission:tills_show');
Route::post('tills-members_create/{id}', 'Tills\TillsController@tills_member_create')->middleware('permission:tills_store');
Route::put('tills-members_update/{id}', 'Tills\TillsController@tills_member_update')->middleware('permission:tills_update');
Route::delete('tills-members_delete/{id}', 'Tills\TillsController@tills_member_destroy')->middleware('permission:tills_destroy');

//rutas de Detalle de Cajas
Route::get('tillDetails/', 'TillDetails\TillDetailsController@index')->middleware('permission:tills_details_index');
Route::get('tillDetails/{id}', 'TillDetails\TillDetailsController@show');
Route::get('tills_close/{id}', 'TillDetails\TillDetailsController@get_till_to_close')->middleware('permission:tills_details_index');
Route::get('tillDetailsReport', 'TillDetails\TillDetailsController@generalTillsReport');
Route::get('tills_details-expenses/', 'TillDetails\TillDetailsController@listExpenses');
Route::get('tills_details-incomes/', 'TillDetails\TillDetailsController@report_to_incomes');
Route::get('tills_details-balance/', 'TillDetails\TillDetailsController@getBalance');
// ->middleware('permission:tills_details_show');
Route::get('till_is_starting/', 'TillDetails\TillDetailsController@tillIsStarting')->middleware('permission:tills_details_show');
Route::get('tillDetails-till/{id}', 'TillDetails\TillDetailsController@showTills');
// ->middleware('permission:tills_details_show');
Route::get('tillDetails-sum/{id}', 'TillDetails\TillDetailsController@tillStartingSum');
//->middleware('permission:tills_details_show');
Route::post('tillDetails', 'TillDetails\TillDetailsController@store')->middleware('permission:tills_details_store');
Route::put('tillDetails/{id}', 'TillDetails\TillDetailsController@update')->middleware('permission:tills_details_update');
Route::put('process_check/{id}', 'TillDetails\TillDetailsController@process_check')->middleware('permission:tills_details_update');
Route::delete('tillDetails/{id}', 'TillDetails\TillDetailsController@destroy')->middleware('permission:tills_details_destroy');

//rutas de Tranferencias de Cajas
Route::get('tillsTransfers/', 'TillTransfers\TillTransfersController@index')->middleware('permission:till_transfers_index');
Route::get('tillsTransfers/{id}', 'TillTransfers\TillTransfersController@show');
Route::get('tillsTransfers-till/{id}', 'TillTransfers\TillTransfersController@showTills');
//->middleware('permission:till_transfers_show');
Route::post('tillsTransfers/', 'TillTransfers\TillTransfersController@store')->middleware('permission:till_transfers_store');
Route::put('tillsTransfers/{id}', 'TillTransfers\TillTransfersController@update')->middleware('permission:till_transfers_update');
Route::delete('tillsTransfers/{id}', 'TillTransfers\TillTransfersController@destroy')->middleware('permission:till_transfers_destroy');

//rutas de ContactPerson
Route::get('contactPersons/', 'ContactPersonsController@index')->middleware('permission:contactPersons_index');
Route::get('contactPersons/{id}', 'ContactPersonsController@show')->middleware('permission:contactPersons_show');
Route::get('contactPersons-per/{id}', 'ContactPersonsController@showPerson');
Route::post('contactPersons/', 'ContactPersonsController@store')->middleware('permission:contactPersons_store');
Route::post('contactPersons-per/', 'ContactPersonsController@storeFromPerson');
Route::get('contactPersons/search/{req}', 'ContactPersonsController@search')->middleware('permission:contactPersons_search');
Route::put('contactPersons/{id}', 'ContactPersonsController@update')->middleware('permission:contactPersons_update');
Route::put('contactPersons', 'ContactPersonsController@updateFromArray')->middleware('permission:contactPersons_update');
Route::delete('contactPersons/{id}', 'ContactPersonsController@destroy')->middleware('permission:contactPersons_destroy');

//rutas de Tariffs
Route::get('tariffs/', 'Tariffs\TariffsController@index')->middleware('permission:tariffs_index');
Route::get('tariffs/{id}', 'Tariffs\TariffsController@show')->middleware('permission:tariffs_show');
Route::get('tariffsFacucarAdm/{id}', 'Tariffs\TariffsController@showFacucarAdm');
Route::get('tariffsCareers/{id}', 'Tariffs\TariffsController@showCareer');
Route::post('tariffs/', 'Tariffs\TariffsController@store')->middleware('permission:tariffs_store');
Route::get('tariffs-search/', 'Tariffs\TariffsController@search')->middleware('permission:tariffs_search');
Route::put('tariffs/{id}', 'Tariffs\TariffsController@update')->middleware('permission:tariffs_update');
Route::delete('tariffs/{id}', 'Tariffs\TariffsController@destroy')->middleware('permission:tariffs_destroy');

//rutas de facucar_adm
Route::get('facucars/', 'FacucarsAdmController@index')->middleware('permission:facucars_index');
Route::get('facucars/{id}', 'FacucarsAdmController@show')->middleware('permission:facucars_show');
Route::post('facucars/', 'FacucarsAdmController@store')->middleware('permission:facucars_store');
Route::get('facucars/search/{req}', 'FacucarsAdmController@search')->middleware('permission:facucars_search');
Route::put('facucars/{id}', 'FacucarsAdmController@update')->middleware('permission:facucars_update');
Route::delete('facucars/{id}', 'FacucarsAdmController@destroy')->middleware('permission:facucars_destroy');

//rutas de carsubjs
Route::get('carsubjs/', 'CareerSubjects\CareersSubjectController@index')->middleware('permission:carsubjs_index');
Route::get('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@show')->middleware('permission:carsubjs_show');
Route::get('carsubject/{id}', 'CareerSubjects\CareersSubjectController@showSubject');
Route::get('carsubjectControl/{id}', 'CareerSubjects\CareersSubjectController@subjectDetails');
Route::get('carsubsCareer/{id}', 'CareerSubjects\CareersSubjectController@showCareer');
Route::get('carsubsProfessor', 'CareerSubjects\CareersSubjectController@showProfessor');
Route::get('carsubsProfessors', 'CareerSubjects\CareersSubjectController@showProfessorSalary');
Route::get('carsubject/semester/{id}', 'CareerSubjects\CareersSubjectController@showFromSemester');
Route::post('carsubjs/', 'CareerSubjects\CareersSubjectController@store')->middleware('permission:carsubjs_store');
Route::put('careersubjets_shared/{id}', 'CareerSubjects\CareersSubjectController@addSharedId')->middleware('permission:carsubjs_update');
Route::get('carsubjs/search/{req}', 'CareerSubjects\CareersSubjectController@search')->middleware('permission:carsubjs_search');
Route::put('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@update')->middleware('permission:carsubjs_update');
Route::put('carsubj_permission/{id}', 'CareerSubjects\CareersSubjectController@change_permission')->middleware('permission:cierre_planilla_edit');
Route::delete('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@destroy')->middleware('permission:carsubjs_destroy');

//rutas de enrolleds
Route::get('enrolleds/', 'Enrolleds\EnrolledController@index')->middleware('permission:enrolleds_index');
Route::get('get-student-enrolled-nt/', 'Enrolleds\EnrolledController@getEnrolledNt')->middleware('permission:enrolleds_index');
Route::get('enrolleds/{id}', 'Enrolleds\EnrolledController@show');
// ->middleware('permission:enrolleds_show');
Route::get('enrolledStudent/{id}', 'Enrolleds\EnrolledController@showStudents');
Route::get('enrolledStudents/{id}', 'Enrolleds\EnrolledController@showAllEnronlledStudent');
Route::get('enrolleds_report/', 'Enrolleds\EnrolledController@getReportEnrolledData');
Route::get('student-last-enrolled', 'Enrolleds\EnrolledController@showLastEnrolledStudent')->middleware('permission:enrolleds_show');
// ->middleware('permission:enrolleds_show');
Route::get('enrolledFacucar/{id}', 'Enrolleds\EnrolledController@showFacucar');
Route::post('enrolleds/', 'Enrolleds\EnrolledController@store')->middleware('permission:enrolleds_store');
Route::get('enrolleds/search/{req}', 'Enrolleds\EnrolledController@search')->middleware('permission:enrolleds_search');
Route::put('enrolleds/{id}', 'Enrolleds\EnrolledController@update')->middleware('permission:enrolleds_update');
Route::get('enrolled/{id}', 'Enrolleds\EnrolledController@showEnrolled')->middleware('permission:enrolleds_show');
Route::put('enrolled_obs/{id}', 'Enrolleds\EnrolledController@updateObs')->middleware('permission:enrolleds_update');
Route::post('enrolledUp/', 'Enrolleds\EnrolledController@updateFromArray');
Route::delete('enrolleds/{id}', 'Enrolleds\EnrolledController@destroy')->middleware('permission:enrolleds_destroy');
Route::delete('enrolledsArray', 'Enrolleds\EnrolledController@destroyFromArray')->middleware('permission:enrolleds_destroy');

//rutas de Profesores
Route::get('professors/', 'ProfessorsController@index')->middleware('permission:professors_index');
// Route::get('professorSalary/', 'ProfessorsController@generateSalary')->middleware('permission:professor_account_store');
Route::get('professors/{id}', 'ProfessorsController@show')->middleware('permission:professors_show');
Route::post('professors/', 'ProfessorsController@store')->middleware('permission:professors_store');
Route::get('professors-search', 'ProfessorsController@search')->middleware('permission:professors_search');
Route::put('professors/{id}', 'ProfessorsController@update')->middleware('permission:professors_update');
Route::delete('professors/{id}', 'ProfessorsController@destroy')->middleware('permission:professors_destroy');

//rutas de Evaluacion de materia
Route::get('subevals/', 'SubjectEvaluations\SubjectEvaluationController@index')->middleware('permission:subevals_index');
Route::get('subevals/{id}', 'SubjectEvaluations\SubjectEvaluationController@show')->middleware('permission:subevals_show');
Route::get('subevals-sub/{id}', 'SubjectEvaluations\SubjectEvaluationController@showSubject')->middleware('permission:subevals_show');
Route::get('subevals-exams', 'SubjectEvaluations\SubjectEvaluationController@showExam')->middleware('permission:subevals_show');
Route::post('subevals/', 'SubjectEvaluations\SubjectEvaluationController@store')->middleware('permission:subevals_store');
Route::post('subevals-subject/', 'SubjectEvaluations\SubjectEvaluationController@storeFromSubject')->middleware('permission:subevals_store');
Route::get('subevals/search/{req}', 'SubjectEvaluations\SubjectEvaluationController@search')->middleware('permission:subevals_search');
Route::put('subevals/{id}', 'SubjectEvaluations\SubjectEvaluationController@update')->middleware('permission:subevals_update');
Route::delete('subevals/{id}', 'SubjectEvaluations\SubjectEvaluationController@destroy')->middleware('permission:subevals_destroy');

//rutas de Asistencia
Route::get('assistances/', 'Assistance\AssistanceController@index')->middleware('permission:assistances_index');
Route::get('assistances/{id}', 'Assistance\AssistanceController@show')->middleware('permission:assistances_show');
Route::get('assistancesSubject/{id}', 'Assistance\AssistanceController@showAssistancesPerSubject');
Route::get('assistances-grouped/{id}', 'Assistance\AssistanceController@showCareerSubjectsGrouped');
Route::get('assistances-per/{id}', 'Assistance\AssistanceController@showCareerSubjects');
Route::post('assistances/', 'Assistance\AssistanceController@store')->middleware('permission:assistances_store');
Route::post('assistances-multiple/', 'Assistance\AssistanceController@storeMultiple')->middleware('permission:assistances_store');
Route::put('assistances/{id}', 'Assistance\AssistanceController@update')->middleware('permission:assistances_update');
Route::delete('assistances/{id}', 'Assistance\AssistanceController@destroy')->middleware('permission:assistances_destroy');
Route::post('assistances-delete-classday/{id}', 'Assistance\AssistanceController@destroyClassDay')->middleware('permission:assistances_destroy');
Route::post('insert-congress-assist', 'Assistance\AssistanceController@insert_congress_assist');

//rutas de Seguros medicos por estudiantes
Route::get('studenthealthcares/', 'StudentHealthcare\StudentHealthcareController@index')->middleware('permission:studenthealthcares_index');
Route::get('studenthealthcares/{id}', 'StudentHealthcare\StudentHealthcareController@show')->middleware('permission:studenthealthcares_show');
Route::get('studenthealthcares-perStudent/{id}', 'StudentHealthcare\StudentHealthcareController@showStudent');
Route::get('studenthealthcares-perHealthcare/{id}', 'StudentHealthcare\StudentHealthcareController@showHealthcare');
Route::post('studenthealthcares/', 'StudentHealthcare\StudentHealthcareController@store')->middleware('permission:studenthealthcares_store');
Route::put('studenthealthcares/{id}', 'StudentHealthcare\StudentHealthcareController@update')->middleware('permission:studenthealthcares_update');
Route::delete('studenthealthcares/{id}', 'StudentHealthcare\StudentHealthcareController@destroy')->middleware('permission:studenthealthcares_destroy');

//rutas de Monitoreo de estudiantes studentsmonitoring
Route::get('studentsmonitoring/', 'StudentMonitoring\StudentMonitoringController@index')->middleware('permission:studentsmonitoring_index');
Route::get('studentsmonitoring/{id}', 'StudentMonitoring\StudentMonitoringController@show')->middleware('permission:studentsmonitoring_show');
Route::get('studentsmonitoring-perStudent/{id}', 'StudentMonitoring\StudentMonitoringController@showStudent');
Route::post('studentsmonitoring/', 'StudentMonitoring\StudentMonitoringController@store')->middleware('permission:studentsmonitoring_store');
Route::put('studentsmonitoring/{id}', 'StudentMonitoring\StudentMonitoringController@update')->middleware('permission:studentsmonitoring_update');
Route::delete('studentsmonitoring/{id}', 'StudentMonitoring\StudentMonitoringController@destroy')->middleware('permission:studentsmonitoring_destroy');

//rutas deTipo de profesores proffesorstype
Route::get('proffesorstype/', 'ProffesorType\ProffesorTypeController@index')->middleware('permission:proffesorstype_index');
Route::get('proffesorstype/{id}', 'ProffesorType\ProffesorTypeController@show')->middleware('permission:proffesorstype_show');
Route::post('proffesorstype/', 'ProffesorType\ProffesorTypeController@store')->middleware('permission:proffesorstype_store');
Route::put('proffesorstype/{id}', 'ProffesorType\ProffesorTypeController@update')->middleware('permission:proffesorstype_update');
Route::delete('proffesorstype/{id}', 'ProffesorType\ProffesorTypeController@destroy')->middleware('permission:proffesorstype_destroy');

//rutas de edificio
Route::get('edifice/', 'Edifice\EdificeController@index')->middleware('permission:edifice_index');
Route::get('edifice/{id}', 'Edifice\EdificeController@show')->middleware('permission:edifice_show');
Route::post('edifice/', 'Edifice\EdificeController@store')->middleware('permission:edifice_store');
Route::put('edifice/{id}', 'Edifice\EdificeController@update')->middleware('permission:edifice_update');
Route::delete('edifice/{id}', 'Edifice\EdificeController@destroy')->middleware('permission:edifice_destroy');

//rutas de comision de funcionarios
Route::get('employee_comission/', 'EmployeeComission\EmployeeComissionController@index')->middleware('permission:employee_comission_index');
Route::get('employee_comission/{id}', 'EmployeeComission\EmployeeComissionController@show')->middleware('permission:employee_comission_show');
Route::get('employee_comission-per/{id}', 'EmployeeComission\EmployeeComissionController@showEmployee');
// ->middleware('permission:employee_comission_show');
Route::post('employee_comission/', 'EmployeeComission\EmployeeComissionController@store')->middleware('permission:employee_comission_store');
Route::put('employee_comission/{id}', 'EmployeeComission\EmployeeComissionController@update')->middleware('permission:employee_comission_update');
Route::delete('employee_comission/{id}', 'EmployeeComission\EmployeeComissionController@destroy')->middleware('permission:employee_comission_destroy');

//rutas de consultas
Route::get('consult/', 'Consults\ConsultController@index');
// ->middleware('permission:consult_index');
Route::get('consult/{id}', 'Consults\ConsultController@show')->middleware('permission:consult_show');
Route::get('consult-student/{id}', 'Consults\ConsultController@showStudent')->middleware('permission:consult_show');
Route::get('consult-search', 'Consults\ConsultController@search');
Route::get('get-preinscripts/{id}', 'Consults\ConsultController@getPreInscript');

// ->middleware('permission:consult_show');
Route::post('consult/', 'Consults\ConsultController@store')->middleware('permission:consult_store');
Route::post('preconsult', 'Consults\ConsultController@storefromPreConsult')->middleware('permission:consult_store');
Route::put('consult/{id}', 'Consults\ConsultController@update')->middleware('permission:consult_update');
Route::put('preconsult', 'Consults\ConsultController@updatefromPreConsult')->middleware('permission:consult_update');
Route::delete('consult/{id}', 'Consults\ConsultController@destroy')->middleware('permission:consult_destroy');


//rutas de Tipo de salas
Route::get('classroomtype/', 'ClassRoomTypes\ClassRoomTypesController@index')->middleware('permission:classroomtype_index');
Route::get('classroomtype/search/{req}', 'ClassRoomTypes\ClassRoomTypesController@search')->middleware('permission:classroomtype_index');
Route::get('classroomtype/{id}', 'ClassRoomTypes\ClassRoomTypesController@show')->middleware('permission:classroomtype_show');
Route::post('classroomtype/', 'ClassRoomTypes\ClassRoomTypesController@store')->middleware('permission:classroomtype_store');
Route::put('classroomtype/{id}', 'ClassRoomTypes\ClassRoomTypesController@update')->middleware('permission:classroomtype_update');
Route::delete('classroomtype/{id}', 'ClassRoomTypes\ClassRoomTypesController@destroy')->middleware('permission:classroomtype_destroy');


//rutas de Tipo de salas
Route::get('objectives/', 'Objectives\ObjectivesController@index')->middleware('permission:objectives_index');
Route::get('objectives/{id}', 'Objectives\ObjectivesController@show')->middleware('permission:objectives_show');
Route::get('objectives-career/{id}', 'Objectives\ObjectivesController@showCareer')->middleware('permission:objectives_show');
Route::post('objectives/', 'Objectives\ObjectivesController@store')->middleware('permission:objectives_store');
Route::put('objectives/{id}', 'Objectives\ObjectivesController@update')->middleware('permission:objectives_update');
Route::delete('objectives/{id}', 'Objectives\ObjectivesController@destroy')->middleware('permission:objectives_destroy');


//rutas de salas
Route::get('classroom/', 'ClassRooms\ClassRoomsController@index')->middleware('permission:classroom_index');
Route::get('classroom/{id}', 'ClassRooms\ClassRoomsController@show')->middleware('permission:classroom_show');
Route::post('classroom/', 'ClassRooms\ClassRoomsController@store')->middleware('permission:classroom_store');
Route::put('classroom/{id}', 'ClassRooms\ClassRoomsController@update')->middleware('permission:classroom_update');
Route::get('classroom/search/{id}', 'ClassRooms\ClassRoomsController@search')->middleware('permission:classroom_update');
Route::delete('classroom/{id}', 'ClassRooms\ClassRoomsController@destroy')->middleware('permission:classroom_destroy');

//rutas de secciones
Route::get('section/', 'Sections\SectionsController@index')->middleware('permission:section_index');
Route::get('section/search/{req}', 'Sections\SectionsController@search')->middleware('permission:section_show');
Route::get('section/{id}', 'Sections\SectionsController@show')->middleware('permission:section_show');
Route::post('section/', 'Sections\SectionsController@store')->middleware('permission:section_store');
Route::put('section/{id}', 'Sections\SectionsController@update')->middleware('permission:section_update');
Route::delete('section/{id}', 'Sections\SectionsController@destroy')->middleware('permission:section_destroy');

//rutas de reservas
Route::get('classroomoc/{id}', 'Reserves\ReservesController@isOccupied');
// ->middleware('permission:reserve_index');
Route::get('reserve/', 'Reserves\ReservesController@index')->middleware('permission:reserve_index');
Route::get('reserve/search/{req}', 'Reserves\ReservesController@search')->middleware('permission:reserve_show');
Route::get('reserve/{id}', 'Reserves\ReservesController@show')->middleware('permission:reserve_show');
Route::post('reserve/', 'Reserves\ReservesController@store')->middleware('permission:reserve_store');
Route::put('reserve/{id}', 'Reserves\ReservesController@update')->middleware('permission:reserve_update');
Route::delete('reserve/{id}', 'Reserves\ReservesController@destroy')->middleware('permission:reserve_destroy');

//rutas deTipo de evaluaciones de estudiantes evalstudent
Route::get('evalstudent/', 'EvalStudent\EvalStudentController@index')->middleware('permission:evalstudent_index');
Route::get('evalstudent-careers', 'EvalStudent\EvalStudentController@enableds')->middleware('permission:evalstudent_index');
Route::get('evalstudent/{id}', 'EvalStudent\EvalStudentController@show')->middleware('permission:evalstudent_show');
Route::get('evalstudent-per', 'EvalStudent\EvalStudentController@showStudent');
Route::post('evalstudent/', 'EvalStudent\EvalStudentController@store');
// ->middleware('permission:evalstudent_store');
Route::put('evalstudent/{id}', 'EvalStudent\EvalStudentController@update')->middleware('permission:evalstudent_update');
Route::put('evalstudent_authorise_paiment/{id}', 'EvalStudent\EvalStudentController@authorize_paiment')->middleware('permission:evalstudent_authorise_paid');
Route::delete('evalstudent/{id}', 'EvalStudent\EvalStudentController@destroy')->middleware('permission:evalstudent_destroy');

//rutas de cuentas de estudiantes evalstudent
Route::get('studentaccount/', 'StudentAccount\StudentAccountController@index');
Route::get('genstudentaccount', 'StudentAccount\StudentAccountController@genAccount');
Route::get('studentaccount/{id}', 'StudentAccount\StudentAccountController@show');
Route::get('studentaccounts/{id}', 'StudentAccount\StudentAccountController@showStudent');
Route::get('studentaccount-enrolled/{id}', 'StudentAccount\StudentAccountController@showEnrolled');
Route::get('studentaccounts/', 'StudentAccount\StudentAccountController@showDues');
Route::post('studentaccount/', 'StudentAccount\StudentAccountController@store');
Route::post('studentaccount_pay/', 'StudentAccount\StudentAccountController@payment');
Route::get('quot_expiration_date', 'StudentAccount\StudentAccountController@quot_expiration_date');
Route::put('studentaccount/{id}', 'StudentAccount\StudentAccountController@update');
Route::put('studentaccounts', 'StudentAccount\StudentAccountController@updateFromArray');
Route::delete('studentaccount/{id}', 'StudentAccount\StudentAccountController@destroy');

//rutas de carsubEnrolled
Route::get('showEval/', 'CarsubEnrolled\CarsubEnrolledController@showEval');
Route::get('carsubEnrolled/', 'CarsubEnrolled\CarsubEnrolledController@index');
Route::get('carsubEnrolled/{id}', 'CarsubEnrolled\CarsubEnrolledController@show');
Route::get('carsubEnrolleds/{id}', 'CarsubEnrolled\CarsubEnrolledController@showEnrolled');
Route::get('carsubEnrolledPrerequirements/', 'CarsubEnrolled\CarsubEnrolledController@showPreRequirements');
Route::get('getfinal/{id}', 'CarsubEnrolled\CarsubEnrolledController@getFinal');
Route::get('carsubEnrolledSubject/{id}', 'CarsubEnrolled\CarsubEnrolledController@showSubject');
Route::get('carsubEnrolledSubjectEvaluation/{id}', 'CarsubEnrolled\CarsubEnrolledController@showSubjectEvaluations');
Route::get('carsubEnrolledMonitoring/{id}', 'CarsubEnrolled\CarsubEnrolledController@showMonitoring');
Route::post('carsubEnrolled/', 'CarsubEnrolled\CarsubEnrolledController@store');
Route::post('save-multiple-carsubenrolled/', 'CarsubEnrolled\CarsubEnrolledController@saveMultiple')->middleware('permission:enrolleds_store');
Route::post('subj_conv/', 'CarsubEnrolled\CarsubEnrolledController@subjConv');
Route::put('carsubEnrolled/{id}', 'CarsubEnrolled\CarsubEnrolledController@update');
Route::delete('carsubEnrolled/{id}', 'CarsubEnrolled\CarsubEnrolledController@destroy');

//rutas de careerreqdocs
Route::get('careerreqdocs/index', 'CareerReqdoc\CareerReqdocController@index');
Route::get('careerreqdocs/{id}', 'CareerReqdoc\CareerReqdocController@showReqDoc');
Route::get('careerreqdocs/', 'CareerReqdoc\CareerReqdocController@showCareer');
Route::post('careerreqdocs/', 'CareerReqdoc\CareerReqdocController@store');
Route::put('careerreqdocs/{id}', 'CareerReqdoc\CareerReqdocController@update');
Route::delete('careerreqdocs/{id}', 'CareerReqdoc\CareerReqdocController@destroy');

//rutas de Tickets
Route::get('tickets/', 'Tickets\TicketController@index')->middleware('permission:ticket_index');
Route::get('ticket_details/', 'Tickets\TicketController@listTicketDetails');
Route::get('ticket_details/events/', 'Products\ProductController@get_tdetails_assistances');
Route::get('tickets/{id}', 'Tickets\TicketController@show');
Route::get('ticketPerson/{id}', 'Tickets\TicketController@showPerson');
Route::get('ticketsearch', 'Tickets\TicketController@search');
Route::put('ticket-rollback/{id}', 'Tickets\TicketController@rollback')->middleware('permission:ticket_destroy');
Route::post('tickets/', 'Tickets\TicketController@store');
Route::put('tickets/{id}', 'Tickets\TicketController@update')->middleware('permission:ticket_update');
Route::put('ticket_number', 'Tickets\TicketController@updateTicketNumber')->middleware('permission:ticket_update');
Route::delete('tickets/{id}', 'Tickets\TicketController@destroy');

//rutas de TicketDetailTypes

Route::get('tdtypes', 'TicketDetailTypes\TicketDetailTypeController@index');
Route::get('tdtypes/{id}', 'TicketDetailTypes\TicketDetailTypeController@show');
Route::post('tdtypes', 'TicketDetailTypes\TicketDetailTypeController@store');
Route::put('tdtypes/{id}', 'TicketDetailTypes\TicketDetailTypeController@update');
Route::delete('tdtypes/{id}', 'TicketDetailTypes\TicketDetailTypeController@destroy');

Route::get('companyDetails', 'CompanyDetails\CompanyDetailsController@index');
Route::get('companyDetails/{id}', 'CompanyDetails\CompanyDetailsController@show');
Route::post('companyDetails/', 'CompanyDetails\CompanyDetailsController@store');
Route::put('companyDetails/{id}', 'CompanyDetails\CompanyDetailsController@update');
Route::delete('companyDetails/{id}', 'CompanyDetails\CompanyDetailsController@destroy');

Route::get('paymentTypes', 'PaymentType\PaymentTypeController@index')->middleware('permission:paymentTypes_index');
Route::get('paymentTypes/{id}', 'PaymentType\PaymentTypeController@show')->middleware('permission:paymentTypes_show');
Route::get('paymentTypesProofPayments', 'PaymentType\PaymentTypeController@showWhitProofPayments');
// ->middleware('permission:paymentTypes_show');
Route::post('paymentTypes/', 'PaymentType\PaymentTypeController@store')->middleware('permission:paymentTypes_store');
Route::put('paymentTypes/{id}', 'PaymentType\PaymentTypeController@update')->middleware('permission:paymentTypes_update');
Route::get('paymentTypes-search/{id}', 'PaymentType\PaymentTypeController@search')->middleware('permission:paymentTypes_index');
Route::delete('paymentTypes/{id}', 'PaymentType\PaymentTypeController@destroy')->middleware('permission:paymentTypes_destroy');


Route::get('employeeAccount', 'EmployeeAccount\EmployeeAccountController@index');
Route::get('employeeAccount/{id}', 'EmployeeAccount\EmployeeAccountController@show');
Route::get('employeeAccounts/{id}', 'EmployeeAccount\EmployeeAccountController@showEmployee');
Route::get('employeeAccounts', 'EmployeeAccount\EmployeeAccountController@showEmployeesWithSalary');
Route::post('employeeAccount/', 'EmployeeAccount\EmployeeAccountController@store');
Route::put('employeeAccount/{id}', 'EmployeeAccount\EmployeeAccountController@update');
Route::put('employeeAccounts', 'EmployeeAccount\EmployeeAccountController@updateFromArray');
Route::put('employeeAccounts-masive', 'EmployeeAccount\EmployeeAccountController@masiveUpdateFromArray');
Route::delete('employeeAccount/{id}', 'EmployeeAccount\EmployeeAccountController@destroy');

Route::get('professorAccount', 'ProfessorAccount\ProfessorAccountController@index');
Route::get('professorAccount/{id}', 'ProfessorAccount\ProfessorAccountController@show');
Route::get('professorAccounts/{id}', 'ProfessorAccount\ProfessorAccountController@showProfessor');
Route::get('professorAccountsToPay', 'ProfessorAccount\ProfessorAccountController@showProfessorsToPay');
// Route::get('professorAccountsAprovProfessor/{id}', 'ProfessorAccount\ProfessorAccountController@showSalaryAproved');
Route::post('professorAccounts-masivePay', 'ProfessorAccount\ProfessorAccountController@masivePay');
Route::post('professorAccounts-genexam', 'ProfessorAccount\ProfessorAccountController@gen_salary_for_exam');
Route::post('professorAccounts-genpostgrado', 'ProfessorAccount\ProfessorAccountController@gen_salary_for_postgrado');
Route::get('professorAccountsWorked/{id}', 'ProfessorAccount\ProfessorAccountController@showHsWorked');
Route::get('professorAccountsSubjects', 'ProfessorAccount\ProfessorAccountController@showSalariesWithSubjects');
Route::post('professorAccount/', 'ProfessorAccount\ProfessorAccountController@store');
Route::put('professorAccount', 'ProfessorAccount\ProfessorAccountController@update');
Route::put('professorAccountAuth', 'ProfessorAccount\ProfessorAccountController@updateAuth');
Route::delete('professorAccount/{id}', 'ProfessorAccount\ProfessorAccountController@destroy');

Route::get('promos', 'Promos\PromosController@index')->middleware('permission:promos_index');
Route::get('promos/{id}', 'Promos\PromosController@show')->middleware('permission:promos_show');
Route::get('promosActive', 'Promos\PromosController@showActive');
//->middleware('permission:promos_show');
Route::post('promos/', 'Promos\PromosController@store')->middleware('permission:promos_store');
Route::put('promos/{id}', 'Promos\PromosController@update')->middleware('permission:promos_update');
Route::delete('promos/{id}', 'Promos\PromosController@destroy')->middleware('permission:promos_destroy');

Route::get('staffs', 'Staff\StaffController@index');
Route::get('staffs/{id}', 'Staff\StaffController@show');
Route::get('facultyStaff/{id}', 'Staff\StaffController@showFaculties');
Route::get('employeeStaff/{id}', 'Staff\StaffController@showEmployees');
//->middleware('permission:staffs_show');
Route::post('staffs/', 'Staff\StaffController@store');
Route::put('staffs/{id}', 'Staff\StaffController@update');
Route::delete('staffs/{id}', 'Staff\StaffController@destroy');

Route::get('careersubjdetails', 'CareerSubjDetail\CareerSubjDetailController@index');
Route::get('careersubjdetails/{id}', 'CareerSubjDetail\CareerSubjDetailController@show');
Route::get('careersubjdetailsubject/{id}', 'CareerSubjDetail\CareerSubjDetailController@showCareerSubjects');
Route::post('careersubjdetails/', 'CareerSubjDetail\CareerSubjDetailController@store');
Route::put('careersubjdetails/{id}', 'CareerSubjDetail\CareerSubjDetailController@update');
Route::delete('careersubjdetails/{id}', 'CareerSubjDetail\CareerSubjDetailController@destroy');


//rutas de studentRetentionQuestion
Route::get('retentionQuestion/', 'StudentRetentionQuestions\StudentRetentionQuestionsController@index')->middleware('permission:retentionQuestion_index');
Route::get('retentionQuestion/select/{req}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@select');
Route::get('retentionQuestionParameters/{id}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@showWithParameters');
// ->middleware('permission:retentionQuestion_show');
Route::get('retentionQuestion/{id}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@show')->middleware('permission:retentionQuestion_show');
Route::get('retentionQuestion/search/{req}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@search')->middleware('permission:retentionQuestion_search');
Route::post('retentionQuestion/', 'StudentRetentionQuestions\StudentRetentionQuestionsController@store')->middleware('permission:retentionQuestion_store');
Route::put('retentionQuestion/{id}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@update')->middleware('permission:retentionQuestion_update');
Route::delete('retentionQuestion/{id}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@destroy')->middleware('permission:retentionQuestion_destroy');
//Route::get('studentRetentionQuestions/', 'StudentRetentionQuestions\StudentRetentionQuestionsController@index')->middleware('permission:retentionQuestion_index');
//Route::get('studentRetentionQuestions/{id}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@show')->middleware('permission:retentionQuestion_show');
//Route::get('studentRetentionQuestions/search/{req}', 'StudentRetentionQuestions\StudentRetentionQuestionsController@search')->middleware('permission:studentRetentionQuestions_search');
//Route::post('studentRetentionQuestions/', 'StudentRetentionQuestions\StudentRetentionQuestionsController@store')->middleware('permission:retentionQuestion_store');
//Route::put('studentRetentionQuestions/{id}','StudentRetentionQuestions\StudentRetentionQuestionsController@update')->middleware('permission:retentionQuestion_update');
//Route::delete('studentRetentionQuestions/{id}','StudentRetentionQuestions\StudentRetentionQuestionsController@destroy')->middleware('permission:retentionQuestion_destroy');

//rutas de studentRetentionParameters
Route::get('retentionParameters/', 'StudentRetentionParameters\StudentRetentionParametersController@index')->middleware('permission:retentionParameter_index');
Route::get('retentionParameters/{id}', 'StudentRetentionParameters\StudentRetentionParametersController@show')->middleware('permission:retentionParameter_show');
Route::get('retentionParametersQuestion/{id}', 'StudentRetentionParameters\StudentRetentionParametersController@showQuestions')->middleware('permission:retentionParameter_show');
//Route::get('studentRetentionParameters/search/{req}', 'StudentRetentionParameters\StudentRetentionParametersController@search');
Route::post('retentionParameters/', 'StudentRetentionParameters\StudentRetentionParametersController@store')->middleware('permission:retentionParameter_store');
Route::put('retentionParameters/{id}', 'StudentRetentionParameters\StudentRetentionParametersController@update')->middleware('permission:retentionParameter_update');
Route::delete('retentionParameters/{id}', 'StudentRetentionParameters\StudentRetentionParametersController@destroy')->middleware('permission:retentionParameter_destroy');

//rutas de studentRetentionAnswers
Route::get('retentionAnswers', 'StudentRetentionAnswers\StudentRetentionAnswersController@index')->middleware('permission:retentionAnswer_index');
Route::get('retentionAnswers/{id}', 'StudentRetentionAnswers\StudentRetentionAnswersController@show')->middleware('permission:retentionAnswer_show');
Route::get('retentionAnswerStudent/{id}', 'StudentRetentionAnswers\StudentRetentionAnswersController@showStudent');
// ->middleware('permission:retentionAnswer_show');
//Route::get('studentRetentionParameters/search/{req}', 'StudentRetentionParameters\StudentRetentionParametersController@search');
Route::post('retentionAnswers/', 'StudentRetentionAnswers\StudentRetentionAnswersController@store')->middleware('permission:retentionAnswer_store');
Route::put('retentionAnswers', 'StudentRetentionAnswers\StudentRetentionAnswersController@update')->middleware('permission:retentionAnswer_update');
Route::delete('retentionAnswers/{id}', 'StudentRetentionAnswers\StudentRetentionAnswersController@destroy')->middleware('permission:retentionAnswer_destroy');

Route::get('carsubProfessors/{id}', 'CarsubProfessor\CarsubProfessorController@showProfessors');
Route::get('carsubProfessorsforassist', 'CarsubProfessor\CarsubProfessorController@showProfesByDayToAssistance');
Route::delete('carsubProfessor/{id}', 'CarsubProfessor\CarsubProfessorController@delete');
Route::get('carsubProfessorSubjects/{id}', 'CarsubProfessor\CarsubProfessorController@showSubjects');
Route::get('subj_profe_search/{id}', 'CarsubProfessor\CarsubProfessorController@search');
Route::get('carsubProfessorAuth/', 'CarsubProfessor\CarsubProfessorController@salary_control');

Route::delete('usersRemove/{id}', 'Users\UserController@removeRole')->middleware('permission:roles_update');

Route::post('gen_studyplan', 'FacuCareers\FacuCareersController@genStudyPlan')->middleware('permission:gen_studyplan');

Route::post('gen_studiesplans', 'Careers\CareersController@genStudiesPlans')->middleware('permission:gen_studyplan');

Route::get('proofPayments', 'ProofPayment\ProofPaymentController@index');
Route::get('proofPayment/{id}', 'ProofPayment\ProofPaymentController@show');
Route::get('proofPayments/{id}', 'ProofPayment\ProofPaymentController@showPaymentType');
Route::post('proofPayments/', 'ProofPayment\ProofPaymentController@store');
Route::put('proofPayments', 'ProofPayment\ProofPaymentController@update');
Route::delete('proofPayments/{id}', 'ProofPayment\ProofPaymentController@destroy');

Route::post('proofPaymentsExist', 'DettillProofPayment\DettillProofPaymentController@IfExist');
Route::get('dettill_proof', 'DettillProofPayment\DettillProofPaymentController@dettill_proff');
Route::get('dettill_proof-search', 'DettillProofPayment\DettillProofPaymentController@search');

Route::get('accounting_plan', 'AccountingPlan\AccountingPlanController@index')->middleware('permission:accounting_plan_index');
Route::get('accounting_plan/{id}', 'AccountingPlan\AccountingPlanController@show')->middleware('permission:accounting_plan_show');
Route::post('accounting_plan/', 'AccountingPlan\AccountingPlanController@store')->middleware('permission:accounting_plan_store');
Route::put('accounting_plan/{id}', 'AccountingPlan\AccountingPlanController@update')->middleware('permission:accounting_plan_update');
Route::delete('accounting_plan/{id}', 'AccountingPlan\AccountingPlanController@destroy')->middleware('permission:accounting_plan_destroy');
Route::get('accounting_plans/', 'AccountingPlan\AccountingPlanController@search');
// ->middleware('permission:accounting_plan_index');

Route::get('subject-excel/export/', 'Subject\SubjectController@exportExcel');
