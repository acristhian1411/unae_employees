<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*crear un usuario y un rol de practipago y preguntar si el usuario
tiene ese rol retornar true, de lo contrario retornar false*/
Route::post('consult', 'Payments\PaymentController@consult');
Route::post('payment', 'Payments\PaymentController@payment');
Route::post('rollback', 'Payments\PaymentController@rollback');
