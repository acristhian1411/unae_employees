<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', 'Users\UserController@get_user');
//
// Route::get('users/', 'Users\UserController@index')->middleware('permission:users_index');
// Route::get('users/{id}', 'Users\UserController@show')->middleware('permission:users_show');
// Route::get('users_has_roles', 'Users\UserController@hasRoles');
// Route::post('users/', 'Users\UserController@store')->middleware('permission:users_store');
// Route::get('users/search/{req}', 'Users\UserController@search')->middleware('permission:users_search');
// Route::put('users/{id}', 'Users\UserController@update')->middleware('permission:users_update');
// Route::delete('users/{id}', 'Users\UserController@destroy')->middleware('permission:users_destroy');
// Route::get('users-person/{id}', 'Users\UserController@showPerson')->middleware('permission:users_show');

#rutas para auditoria
// Route::get('audit_logs/', 'Audits\AuditController@index');

// Route::get('roles/', 'Roles\RoleController@index')->middleware('permission:roles_index');
// Route::get('roles/{id}', 'Roles\RoleController@show')->middleware('permission:roles_show');
// Route::post('roles/', 'Roles\RoleController@store')->middleware('permission:roles_store');
// Route::get('roles/search/{req}', 'Roles\RoleController@search')->middleware('permission:roles_search');
// Route::put('roles/{id}', 'Roles\RoleController@update')->middleware('permission:roles_update');
// Route::delete('roles/{id}', 'Roles\RoleController@destroy')->middleware('permission:roles_destroy');
// Route::get('roles_has_permission', 'Roles\RoleController@hasPermission');
// Route::get('roles_get_permission/{id}', 'Roles\RoleController@getPermissions')->middleware('permission:roles_show');
// Route::get('roles_get_permission/exclude/{id}', 'Roles\RoleController@getPermissionsExclude')->middleware('permission:roles_show');
// Route::post('roles_give_permissions', 'Roles\RoleController@givePermissions')->middleware('permission:roles_show');
// Route::post('roles_revoke_permissions', 'Roles\RoleController@revokePermissions')->middleware('permission:roles_show');

// Route::get('evaluationTypes/', 'EvaluationTypes\EvaluationTypesController@index')->middleware('permission:evaluationTypes_index');
// Route::get('evaluationTypes/{id}', 'EvaluationTypes\EvaluationTypesController@show')->middleware('permission:evaluationTypes_show');
// Route::post('evaluationTypes/', 'EvaluationTypes\EvaluationTypesController@store')->middleware('permission:evaluationTypes_store');
// Route::put('evaluationTypes/{id}','EvaluationTypes\EvaluationTypesController@update')->middleware('permission:evaluationTypes_update');
// Route::delete('evaluationTypes/{id}','EvaluationTypes\EvaluationTypesController@destroy')->middleware('permission:evaluationTypes_destroy');

//rutas de facucar_adm
// Route::get('facucars/', 'FacucarsAdmController@index')->middleware('permission:facucars_index');
// Route::get('facucars/{id}', 'FacucarsAdmController@show')->middleware('permission:facucars_show');
// Route::post('facucars/', 'FacucarsAdmController@store')->middleware('permission:facucars_store');
// Route::get('facucars/search/{req}', 'FacucarsAdmController@search')->middleware('permission:facucars_search');
// Route::put('facucars/{id}', 'FacucarsAdmController@update')->middleware('permission:facucars_update');
// Route::delete('facucars/{id}', 'FacucarsAdmController@destroy')->middleware('permission:facucars_destroy');

//rutas de carsubjs
// Route::get('carsubjs/', 'CareerSubjects\CareersSubjectController@index')->middleware('permission:carsubjs_index');
// Route::get('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@show')->middleware('permission:carsubjs_show');
// Route::get('carsubject/{id}', 'CareerSubjects\CareersSubjectController@showSubject');
// Route::get('carsubsCareer/{id}', 'CareerSubjects\CareersSubjectController@showCareer');
// Route::get('carsubsProfessor', 'CareerSubjects\CareersSubjectController@showProfessor');
// Route::get('carsubject/semester/{id}', 'CareerSubjects\CareersSubjectController@showFromSemester');
// Route::post('carsubjs/', 'CareerSubjects\CareersSubjectController@store')->middleware('permission:carsubjs_store');
// Route::get('carsubjs/search/{req}', 'CareerSubjects\CareersSubjectController@search')->middleware('permission:carsubjs_search');
// Route::put('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@update')->middleware('permission:carsubjs_update');
// Route::delete('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@destroy')->middleware('permission:carsubjs_destroy');

//rutas de enrolleds
// Route::get('enrolleds/', 'Enrolleds\EnrolledController@index')->middleware('permission:enrolleds_index');
// Route::get('enrolleds/{id}', 'Enrolleds\EnrolledController@show');
// ->middleware('permission:enrolleds_show');
// Route::get('enrolledStudent/{id}', 'Enrolleds\EnrolledController@showStudents');
// ->middleware('permission:enrolleds_show');
// Route::get('enrolledFacucar/{id}', 'Enrolleds\EnrolledController@showFacucar');
// Route::post('enrolleds/', 'Enrolleds\EnrolledController@store')->middleware('permission:enrolleds_store');
// Route::get('enrolleds/search/{req}', 'Enrolleds\EnrolledController@search')->middleware('permission:enrolleds_search');
// Route::put('enrolleds/{id}', 'Enrolleds\EnrolledController@update')->middleware('permission:enrolleds_update');
// Route::post('enrolledUp/', 'Enrolleds\EnrolledController@updateFromArray');
// Route::delete('enrolleds/{id}', 'Enrolleds\EnrolledController@destroy')->middleware('permission:enrolleds_destroy');

//rutas de Profesores
// Route::get('professors/', 'ProfessorsController@index')->middleware('permission:professors_index');
// Route::get('professors/{id}', 'ProfessorsController@show')->middleware('permission:professors_show');
// Route::post('professors/', 'ProfessorsController@store')->middleware('permission:professors_store');
// Route::get('professors-search', 'ProfessorsController@search')->middleware('permission:professors_search');
// Route::put('professors/{id}', 'ProfessorsController@update')->middleware('permission:professors_update');
// Route::delete('professors/{id}', 'ProfessorsController@destroy')->middleware('permission:professors_destroy');

//rutas de Evaluacion de materia
// Route::get('subevals/', 'SubjectEvaluations\SubjectEvaluationController@index')->middleware('permission:subevals_index');
// Route::get('subevals/{id}', 'SubjectEvaluations\SubjectEvaluationController@show')->middleware('permission:subevals_show');
// Route::get('subevals-sub/{id}', 'SubjectEvaluations\SubjectEvaluationController@showSubject')->middleware('permission:subevals_show');
// Route::get('subevals-exams', 'SubjectEvaluations\SubjectEvaluationController@showExam')->middleware('permission:subevals_show');;
// Route::post('subevals/', 'SubjectEvaluations\SubjectEvaluationController@store')->middleware('permission:subevals_store');
// Route::post('subevals-subject/', 'SubjectEvaluations\SubjectEvaluationController@storeFromSubject')->middleware('permission:subevals_store');
// Route::get('subevals/search/{req}', 'SubjectEvaluations\SubjectEvaluationController@search')->middleware('permission:subevals_search');
// Route::put('subevals/{id}', 'SubjectEvaluations\SubjectEvaluationController@update')->middleware('permission:subevals_update');
// Route::delete('subevals/{id}', 'SubjectEvaluations\SubjectEvaluationController@destroy')->middleware('permission:subevals_destroy');

//rutas de Asistencia
// Route::get('assistances/', 'Assistance\AssistanceController@index')->middleware('permission:assistances_index');
// Route::get('assistances/{id}', 'Assistance\AssistanceController@show')->middleware('permission:assistances_show');
// Route::get('assistances-grouped/{id}', 'Assistance\AssistanceController@showCareerSubjectsGrouped');
// Route::get('assistances-per/{id}', 'Assistance\AssistanceController@showCareerSubjects');
// Route::post('assistances/', 'Assistance\AssistanceController@store')->middleware('permission:assistances_store');
// Route::put('assistances/{id}', 'Assistance\AssistanceController@update')->middleware('permission:assistances_update');
// Route::delete('assistances/{id}', 'Assistance\AssistanceController@destroy')->middleware('permission:assistances_destroy');

//rutas de Monitoreo de estudiantes studentsmonitoring
// Route::get('studentsmonitoring/', 'StudentMonitoring\StudentMonitoringController@index')->middleware('permission:studentsmonitoring_index');
// Route::get('studentsmonitoring/{id}', 'StudentMonitoring\StudentMonitoringController@show')->middleware('permission:studentsmonitoring_show');
// Route::get('studentsmonitoring-perStudent/{id}', 'StudentMonitoring\StudentMonitoringController@showStudent');
// Route::post('studentsmonitoring/', 'StudentMonitoring\StudentMonitoringController@store')->middleware('permission:studentsmonitoring_store');
// Route::put('studentsmonitoring/{id}', 'StudentMonitoring\StudentMonitoringController@update')->middleware('permission:studentsmonitoring_update');
// Route::delete('studentsmonitoring/{id}', 'StudentMonitoring\StudentMonitoringController@destroy')->middleware('permission:studentsmonitoring_destroy');

//rutas deTipo de profesores proffesorstype
// Route::get('proffesorstype/', 'ProffesorType\ProffesorTypeController@index')->middleware('permission:proffesorstype_index');
// Route::get('proffesorstype/{id}', 'ProffesorType\ProffesorTypeController@show')->middleware('permission:proffesorstype_show');
// Route::post('proffesorstype/', 'ProffesorType\ProffesorTypeController@store')->middleware('permission:proffesorstype_store');
// Route::put('proffesorstype/{id}', 'ProffesorType\ProffesorTypeController@update')->middleware('permission:proffesorstype_update');
// Route::delete('proffesorstype/{id}', 'ProffesorType\ProffesorTypeController@destroy')->middleware('permission:proffesorstype_destroy');
//rutas deTipo de evaluaciones de estudiantes evalstudent
// Route::get('evalstudent/', 'EvalStudent\EvalStudentController@index')->middleware('permission:evalstudent_index');
// Route::get('evalstudent/{id}', 'EvalStudent\EvalStudentController@show')->middleware('permission:evalstudent_show');
// Route::get('evalstudent-per', 'EvalStudent\EvalStudentController@showStudent');
// Route::post('evalstudent/', 'EvalStudent\EvalStudentController@store');
// ->middleware('permission:evalstudent_store');
// Route::put('evalstudent/{id}', 'EvalStudent\EvalStudentController@update');
//->middleware('permission:evalstudent_update');
// Route::delete('evalstudent/{id}', 'EvalStudent\EvalStudentController@destroy')->middleware('permission:evalstudent_destroy');

//rutas de carsubEnrolled
// Route::get('showEval/', 'CarsubEnrolled\CarsubEnrolledController@showEval');
// Route::get('carsubEnrolled/', 'CarsubEnrolled\CarsubEnrolledController@index');
// Route::get('carsubEnrolled/{id}', 'CarsubEnrolled\CarsubEnrolledController@show');
// Route::get('getfinal/{id}', 'CarsubEnrolled\CarsubEnrolledController@getFinal');
// Route::get('carsubEnrolleds/{id}', 'CarsubEnrolled\CarsubEnrolledController@showEnrolled');
// Route::get('carsubEnrolledPrerequirements/', 'CarsubEnrolled\CarsubEnrolledController@showPreRequirements');
// Route::get('carsubEnrolledSubject/{id}', 'CarsubEnrolled\CarsubEnrolledController@showSubject');
// Route::get('carsubEnrolledMonitoring/{id}', 'CarsubEnrolled\CarsubEnrolledController@showMonitoring');
// Route::post('carsubEnrolled/', 'CarsubEnrolled\CarsubEnrolledController@store');
// Route::put('carsubEnrolled/{id}', 'CarsubEnrolled\CarsubEnrolledController@update');
// Route::delete('carsubEnrolled/{id}', 'CarsubEnrolled\CarsubEnrolledController@destroy');

//rutas de carsubjs
// Route::get('carsubjs/', 'CareerSubjects\CareersSubjectController@index')->middleware('permission:carsubjs_index');
// Route::get('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@show')->middleware('permission:carsubjs_show');
// Route::get('carsubject/{id}', 'CareerSubjects\CareersSubjectController@showSubject');
// Route::get('carsubsCareer/{id}', 'CareerSubjects\CareersSubjectController@showCareer');
// Route::get('carsubsProfessor', 'CareerSubjects\CareersSubjectController@showProfessor');
// Route::get('carsubject/semester/{id}', 'CareerSubjects\CareersSubjectController@showFromSemester');
// Route::post('carsubjs/', 'CareerSubjects\CareersSubjectController@store')->middleware('permission:carsubjs_store');
// Route::get('carsubjs/search/{req}', 'CareerSubjects\CareersSubjectController@search')->middleware('permission:carsubjs_search');
// Route::put('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@update')->middleware('permission:carsubjs_update');
// Route::delete('carsubjs/{id}', 'CareerSubjects\CareersSubjectController@destroy')->middleware('permission:carsubjs_destroy');

// Route::get('carsubProfessors/{id}','CarsubProfessor\CarsubProfessorController@showProfessors');
Route::get('carsubProfessorSubjects', 'CarsubProfessor\CarsubProfessorController@showSubjectsProfes');

Route::get('get_assistances/{id}', 'Assistance\AssistanceController@getFromDate');

Route::post('insert_multiple_assist/{id}', 'Assistance\AssistanceController@insertMultiple');
Route::put('update_multiple_assist/{id}', 'Assistance\AssistanceController@updateMultiple');
Route::put('multiple_subeval', 'SubjectEvaluations\SubjectEvaluationController@multiple_subeval');
Route::put('multiple_eval', 'SubjectEvaluations\SubjectEvaluationController@multiple_eval');

Route::get('data_administrative', 'ProfessorAccount\ProfessorAccountController@showSalaryAproved');

Route::post('upload_finished/{id}', 'SubjectEvaluations\SubjectEvaluationController@upload_finished');

// Route::delete('carsubProfessor/{id}','CarsubProfessor\CarsubProfessorController@delete');
