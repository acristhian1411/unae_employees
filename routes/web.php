<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::view('/{path?}', 'app');
// Route::get('/{path?}', function () {
//     return view('app');
// })->middleware('auth');
Route::get('/{path?}', function () {
    return view('app');
});

Route::get('auth/login', function () {
    return view('auth.login');
})->name('login');
Route::post('auth/login', 'Auth\LoginController@login');
Route::post('auth/logout', 'Auth\LoginController@logout')->name('logout');

// Route::get('/home/{path?}', function () {
//     return view('app');
// })->name('home')->middleware('auth');

Route::get('redirect/{driver}', 'Auth\LoginController@redirectToProvider')
    ->name('login.provider');
Route::get('google/callback', 'Auth\LoginController@handleProviderCallback')
    ->name('login.callback');

Route::get('reportes/{route}', 'Reports\ReportController@returnReport');

Route::get('subject-excel/export/', 'Subject\SubjectController@exportExcel');

Route::get('recover_pdf/{name}', 'Reports\ReportController@returnPDF');


Route::get('testpdf/pdf', function () {
  // dd('llega algo');
  $data = [
        'titulo' => 'Styde.net'
    ];
    // $pdf = new PDF();
    // // $options = $pdf->getOptions();
    // $options->setOption('enable-javascript', true);
    // $options->setOption('javascript-delay', 13500);
    // $options->setOption('enable-smart-shrinking', true);
    // $options->setOption('no-stop-slow-scripts', true);
    // $options->setDefaultFont('Courier');
    // $pdf->setOptions($options);
    $pdf = \PDF::loadView('pruebaparapdf', compact('data'));


    // dd $pdf->download('archivo.pdf');
  // $pdf->loadView('pruebaparapdf');
  return $pdf->stream();
  // return view('pruebaparapdf', compact('data'));
});
